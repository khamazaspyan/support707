<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2012-2013 Techjoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 * @website    http://techjoomla.com
 */

// No direct access.
defined('_JEXEC') or die('Restricted access');

// Nothing to show here yet! Let it be an empty soul ;)
