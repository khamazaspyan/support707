<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2012-2013 Techjoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 * @website    http://techjoomla.com
 */

// No direct access.
defined('_JEXEC') or die('Restricted access');

$lang = JFactory::getLanguage();
$lang->load('com_jbolo');

$user = JFactory::getUser();

// Run only if user is logged in
if (!$user->id)
{
	return false;
}

// Check if JBOLO is installed
if (JFile::exists(JPATH_ROOT . '/components/com_jbolo/jbolo.php'))
{
	if (!JComponentHelper::isEnabled('com_jbolo', true))
	{
		return false;
	}
}
else
{
	return;
}

return true;

// @TODO - use this in future //require JModuleHelper::getLayoutPath('mod_jbolo');
