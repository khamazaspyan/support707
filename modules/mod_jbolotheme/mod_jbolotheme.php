<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2012-2013 Techjoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 * @website    http://techjoomla.com
 */

// No direct access.
defined('_JEXEC') or die('Restricted access');

$lang = JFactory::getLanguage();
$lang->load('com_jbolo');

$user = JFactory::getUser();

// Run only if user is logged in
if (!$user->id)
{
	echo JText::_('COM_JBOLO_LOGIN_CHAT');

	return;
}

// Return if Jbolo component not found or is disabled
if (!JFile::exists(JPATH_ROOT . '/components/com_jbolo/jbolo.php') || !JComponentHelper::isEnabled('com_jbolo', true))
{
	return false;
}

// Check if JBolo module is enabled
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('jbolo');

// If the module is disabled, return
if (!$module)
{
	return false;
}

// Get jbolo system plugin
$plugin = JPluginHelper::getPlugin('system', 'jbolo');

// If the plugin is disabled, return
if (!$plugin)
{
	return false;
}

// Load helper file
$jboloFrontendHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/helper.php';

if (!class_exists('jboloFrontendHelper'))
{
	JLoader::register('jboloFrontendHelper', $jboloFrontendHelperPath);
	JLoader::load('jboloFrontendHelper');
}

$jboloFrontendHelper = new jboloFrontendHelper;

if (!$jboloFrontendHelper->_validateUser())
{
	return;
}

require JModuleHelper::getLayoutPath('mod_jbolotheme');
