<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

jimport('joomla.application.component.model');
require_once JPATH_SITE . '/components/com_jgive/models/campaigns.php';

if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

// Get Params
$mod_data = array();

$mod_data['no_of_camp_show'] = $no_of_camp_show   = $params->get('no_of_camp_show');
$mod_data['campaigns_sort_by'] = $campaigns_sort_by = $params->get('campaigns_sort_by');
$mod_data['order_dir'] = $order_dir         = $params->get('order_dir');
$mod_data['campaigns_to_show'] = $campaigns_to_show = $params->get('campaigns_to_show');

// Load the helper
$helperPath = JPATH_SITE . "/components/com_jgive/helper.php";

if (!class_exists('jgiveFrontendHelper'))
{
	JLoader::register('jgiveFrontendHelper', $helperPath);
	JLoader::load('jgiveFrontendHelper');
}

$jgiveFrontendHelper = new jgiveFrontendHelper;

$otherData['createCampItemid'] = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=create');
$otherData['myCampaignsItemid'] = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');
$otherData['singleCampaignItemid'] = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');
$singleCampaignItemid = $otherData['allCampaignsItemid'] = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');

// Create instance of model class
$model       = JModelLegacy::getInstance('campaigns', 'JgiveModel');
$data = $model->getData($mod_data);

require JModuleHelper::getLayoutPath('mod_jgive_campaigns_pin');
