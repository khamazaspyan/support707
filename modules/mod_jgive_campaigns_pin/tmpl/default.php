<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.methods');
jimport('joomla.filesystem.file');

// Load Tj Strapper
$tjStrapperPath = JPATH_SITE . '/media/techjoomla_strapper/tjstrapper.php';

if (JFile::exists($tjStrapperPath))
{
	require_once $tjStrapperPath;
	TjStrapper::loadTjAssets('com_jgive');
}

// Load component css
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive.css');

$pin_width       = $params['pin_width']? $params['pin_width']: 240;
$pin_padding     = $params['pin_padding']? $params['pin_padding']: 5;
?>

<div class="tjBs3 <?php echo $params->get('moduleclass_sfx'); ?>">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div id="mod_jgive_pin_container<?php echo $module->id;?>">
			<?php
				if (empty($data))
				{
				?>
					<div class="alert alert-warning">
						<?php echo JText::_('MOD_JGIVE_PIN_NO_DATA_FOUND');?>
					</div>
				<?php
				}
				else
				{
					foreach ($data as $cdata)
					{
					?>
						<div class="col-sm-3 col-xs-12 jgive_module_pin_item">
							<style type="text/css">
								.jgive_module_pin_item { width: <?php echo $pin_width . 'px'; ?> !important; margin-right: <?php echo $pin_padding . 'px'; ?> !important; }
							</style>
							<?php
								$displayData['campaign'] = $cdata['campaign'];
								$displayData['images'] = $cdata['images'];
								$displayData['otherData'] = $otherData;
								$layout = new JLayoutFile('pin', $basePath = JPATH_SITE . '/components/com_jgive/layouts/campaigns');
								$html = $layout->render($displayData);
								echo $html;
							?>
						</div>
					<?php
					}
				}
			?>
		</div>
	</div>
</div>

<style>
#mod_jgive_pin_container<?php echo $module->id;?> .jgive_pin_item { width: <?php echo $pin_width . 'px'; ?> !important; }
</style>
