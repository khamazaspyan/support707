<?php
/**
 * @version     SVN: <svn_id>
 * @package     JMailAlerts
 * @subpackage  jma_mosets
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/css/jbolo.css');
?>

<script>
var groupid=<?php echo $groupId; ?>;

function setGroupChat(flag, reload)
{
	/*
	* Set heartbeat time to jb_minChatHeartbeat second, and poll,
	* as we want it to poll quick after adding new user
	*
	*/
	jb_chatHeartbeatTime = jb_minChatHeartbeat;

	poll_msg();

	techjoomla.jQuery.ajax({
		url: site_link + "index.php?option=com_jbolo&action=setGroupChat",
		cache: false,
		type: 'POST',
		dataType: "json",
		data: {
			gid: groupid,
			onOff: flag,
			client: "<?php echo $groupIntegrationClient; ?>"
		},
		beforeSend: function (){
			techjoomla.jQuery('#mod_jbolo_group .btn').prop('disabled', true);
		},
		success: function (data) {
			var jboloError = 0;

			if (data.validate.error)
			{
				jboloError = data.validate.error;
			}

			if (jboloError === 1)
			{
				alert(data.validate.error_msg);
				techjoomla.jQuery('#mod_jbolo_group .btn').prop('disabled', false);
				return 0;
			}
			else
			{
				nid = data.nodeinfo.nid;
				rec_name = data.nodeinfo.wt;
				var n = rec_name + "__" + nid;
				open_gc(n);

				if (reload === 1)
				{
					window.location.reload();
				}
				else
				{
					techjoomla.jQuery('#mod_jbolo_group .btn').prop('disabled', false);
				}
			}
		}
	});
}

function joinSocialGroupChat(groupid)
{
	/*
	* Set heartbeat time to jb_minChatHeartbeat second, and poll,
	* as we want it to poll quick after adding new user
	*
	*/
	jb_chatHeartbeatTime = jb_minChatHeartbeat;

	poll_msg();

	techjoomla.jQuery.ajax({
		url: site_link + "index.php?option=com_jbolo&action=joinSocialGroupChat",
		cache: false,
		type: 'POST',
		dataType: "json",
		data: {
			gid: groupid,
			client: "<?php echo $groupIntegrationClient; ?>"
		},
		success: function (data) {
			var jboloError = 0;

			if (data.validate.error)
			{
				jboloError = data.validate.error;
			}

			if (jboloError === 1)
			{
				alert(data.validate.error_msg);
				return 0;
			}
			nid = data.nodeinfo.nid;
			rec_name = data.nodeinfo.wt;
			var n = rec_name + "__" + nid;
			open_gc(n);
		}
	});
}
</script>

<div class="<?php echo $params->get('moduleclass_sfx'); ?> cModule app-box" id="mod_jbolo_group">
	<h3 class="app-box-header">
		<?php echo JText::_('MOD_JBOLO_GROUP_GROUP_CHAT'); ?>
	</h3>

	<div class="app-box-content">
		<?php
		$enabled = $disabled = '';

		if (isset($groupChatDetails->status))
		{
			if ($groupChatDetails->status)
			{
				$enabled = 'btn btn-success active';
			}
			else
			{
				$disabled = 'btn btn-danger disabled';
			}
		}
		?>

		<div class="">
			<fieldset id="social_groupchat" class="">
				<span class="jbolo-1click-gc-enabled">
					<?php if ($groupOwner == JFactory::getUser()->id): ?>
						<?php if (!isset($groupChatDetails->status) || !$groupChatDetails->status): ?>
							<p class="">
								<?php echo JText::_('MOD_JBOLO_GROUP_ENABLE_GROUP_CHAT'); ?>
							</p>
							<button type="button" class="btn btn-success jbolo-btn-wrapper" id="enable_groupchat" name="enable_groupchat" onclick="setGroupChat(1, 1);"
								title="<?php echo JText::_('MOD_JBOLO_GROUP_BTN_ENABLE_DESC'); ?>">
								<i class="icon-checkmark"></i> <?php echo JText::_('MOD_JBOLO_GROUP_BTN_ENABLE'); ?>
							</button>
						<?php else: ?>
							<button type="button" class="btn btn-primary jbolo-btn-wrapper" id="sync_groupchat" name="sync_groupchat" onclick="setGroupChat(1, 0);"
								title="<?php echo JText::_('MOD_JBOLO_GROUP_BTN_SYNC_DESC'); ?>">
								<i class="icon-wand"></i> <?php echo JText::_('MOD_JBOLO_GROUP_BTN_SYNC'); ?>
							</button>
						<?php endif; ?>

						<?php if (isset($groupChatDetails->status) && $groupChatDetails->status): ?>
							<button type="button" class="btn btn-danger jbolo-btn-wrapper" id="disable_groupchat" name="disable_groupchat" onclick="setGroupChat(0, 1);"
								title="<?php echo JText::_('MOD_JBOLO_GROUP_BTN_DISABLE_DESC'); ?>">
								<i class="icon-cancel-2"></i> <?php echo JText::_('MOD_JBOLO_GROUP_BTN_DISABLE'); ?>
							</button>
						<?php endif; ?>
					<?php endif; ?>
				</span>

				<span class="jbolo-1click-gc-disabled">
					<?php if (isset($groupChatDetails->status) && $groupChatDetails->status): ?>
						<button type="button" class="btn btn-success jbolo-btn-wrapper" id="join_groupchat" name="join_groupchat"
							onclick="joinSocialGroupChat(<?php echo $groupId; ?>);"
							title="<?php echo JText::_('MOD_JBOLO_GROUP_BTN_JOIN_GROUP_CHAT_DESC'); ?>">
								<i class="icon-comments-2"></i> <?php echo JText::_('MOD_JBOLO_GROUP_BTN_JOIN_GROUP_CHAT'); ?>
						</button>
					<?php endif; ?>
				</span>
			</fieldset>
		</div>
	</div>

	<!--div class="app-box-footer"></div>-->
</div>
