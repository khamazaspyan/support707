<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  mod_jbolo_group
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Return if Jbolo component not found or is disabled
if (!JFile::exists(JPATH_ROOT . '/components/com_jbolo/jbolo.php') || !JComponentHelper::isEnabled('com_jbolo', true))
{
	return false;
}

// Check if JBolo module is enabled
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('jbolo');

// If the module is disabled, return
if (!$module)
{
	return false;
}

// Get jbolo system plugin
$plugin = JPluginHelper::getPlugin('system', 'jbolo');

// If the plugin is disabled, return
if (!$plugin)
{
	return false;
}

// If not logged in, return from here.
if (!JFactory::getUser()->id)
{
	return false;
}

$jinput = JFactory::getApplication()->input;
$option = $jinput->get('option', '', 'string');
$view   = $jinput->get('view', '', 'string');

// Get group integration setting.
$groupIntegrationClient = $params->get('group_integration');

// Get social group/courss id also validate integration against option from URL.
switch ($groupIntegrationClient)
{
	case 'com_community.group':
	$groupId = $jinput->get('groupid', '', 'INT');

	if ($option !== 'com_community')
	{
		return false;
	}

	break;

	case 'com_tjlms.course':
	$groupId = $jinput->get('course_id', '', 'INT');

	if ($option !== 'com_tjlms')
	{
		return false;
	}

	break;

	default:
	return false;
	break;
}

// If no groupid found in URL, return from here.
if (! $groupId)
{
	return;
}

// Include tpdgroups helper
require_once JPATH_SITE . '/components/com_jbolo/helpers/tpdgroups.php';

// Get group owner id
$groupOwner = JBoloTpdGroupsHelper::getTpdGroupOwnerId($groupId, $groupIntegrationClient);

// Get group chat details
$groupChatDetails = JBoloTpdGroupsHelper::getTpdGroupChatDetails($groupId, $groupIntegrationClient);

// If no groupchat is created or active for this group & if user is not group admin, return from here.
if ($groupOwner != JFactory::getUser()->id )
{
	if (!isset($groupChatDetails->status) || !$groupChatDetails->status)
	{
		return false;
	}
}

require JModuleHelper::getLayoutPath('mod_jbolo_group');
