<?php
/**
* @package      EasySocial
* @copyright    Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>

<div id="es" class="mod-es mod-es-event <?php echo $lib->getSuffix();?>">

	<?php foreach ($events as $event) { ?>
		<div class="mod-fbstyle">
			<a href="<?php echo $event->getPermalink();?>" class="mod-fbstyle-image">
				<img src="<?php echo $event->getAvatar(SOCIAL_AVATAR_SQUARE);?>" alt="" data-avatar-image />
			</a>
			<div class="mod-fbstyle-content">
				<div class="mod-fbstyle-avatar-holder">
					<div class="mod-fbstyle-calendar-date">
						<div class="mod-fbstyle-calendar-mth">
							<?php echo $event->getEventStart()->format('M', true);?>
						</div>
						<div class="mod-fbstyle-calendar-day">
							<?php echo $event->getEventStart()->format('d', true);?>
						</div>
					</div>
				</div>
				<div class="mod-fbstyle-title">
					<a href="<?php echo $event->getPermalink();?>">
						<?php echo $event->getName();?>
					</a>
				</div>
				<div class="mod-fbstyle-time">
					<?php echo $event->getStartEndDisplay(array('end' => false)); ?>
				</div>

				<div class="mod-fbstyle-members">
					<span class="mod-fbstyle-cat"><?php echo $event->getCategory()->getTitle(); ?></span>
					<?php if ($params->get('display_member_counter', true)) { ?>
						<span role="presentation" aria-hidden="true" class="dot"> · </span>
						<?php echo JText::sprintf(ES::string()->computeNoun('MOD_EASYSOCIAL_EVENTS_GUEST_COUNT', $event->getTotalGuests()), $event->getTotalGuests());?>
					<?php } ?>
				</div>

				<?php if ($params->get('display_rsvp', true)) { ?>
				<div class="mod-fbstyle-action">
		            <?php echo ES::themes()->html('event.action', $event, 'right'); ?>
				</div>
				 <?php } ?>
			</div>
		</div>
	<?php } ?>

	<div class="mod-es-action mod-fbstyle-viewall">
		<a href="<?php echo ESR::events();?>" class="btn btn-es-default-o btn-sm"><?php echo JText::_('MOD_EASYSOCIAL_EVENTS_ALL_EVENT');?></a>
	</div>
</div>