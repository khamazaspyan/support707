<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

// Define DIRECTORY_SEPARATOR if not mostly for joomla 3.0
if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

// Load helper File
require_once dirname(__FILE__) . '/helper.php';
$modJGiveDonationHelper = new modJGiveDonationHelper;
$module_for             = $params->get('module_for');
$no_of_record_show      = $params->get('no_of_record_show');
$userid                 = JFactory::getUser()->id;

switch ($module_for)
{
	case 'last_donations':
		$result = $modJGiveDonationHelper->lastDonations($no_of_record_show);
		break;

	case 'top_donations':
		$result = $modJGiveDonationHelper->topDonations($no_of_record_show);
		break;

	default:
		$result = $modJGiveDonationHelper->myDonations($no_of_record_show);
		break;
}

require JModuleHelper::getLayoutPath('mod_jgive_donations');
