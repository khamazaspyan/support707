<?php
/**
* @package    jGive Donation
* @author     Techjoomla
* @copyright  Copyright 2013 - Techjoomla
* @license    http://www.gnu.org/licenses/gpl-3.0.html
**/

$document=JFactory::getDocument();

// No direct access
defined('_JEXEC') or die('Restricted access');

if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

$lang =  JFactory::getLanguage();
$lang->load('mod_jgive_donations', JPATH_ROOT);

// Load component css
$document->addStyleSheet(JUri::root(true).'/media/com_jgive/css/jgive.css');
$helperPath=JPATH_SITE.'/components/com_jgive/helper.php';

if (!class_exists('jgiveFrontendHelper'))
{
	JLoader::register('jgiveFrontendHelper', $helperPath );
	JLoader::load('jgiveFrontendHelper');
}

// Load Campaigns helper
$jgiveFrontendHelper = new jgiveFrontendHelper();
$singleCampaignItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');
?>

<div class="tjBs3 <?php echo $params->get('moduleclass_sfx'); ?>">
<?php
	$module_for=$params->get('module_for');

	if ($module_for=='my_donations')
	{
		if (!$userid)
		{
			echo JText::_('MOD_JGIVE_DONATION_LOGIN');
			echo '</div>';
			return;
		}
	}
?>

	<table class="table table-hover">
		<?php
		if (count($result) > 0 )
		{ ?>
			<thead>
				<tr>
					<th>
						<?php
							if ($module_for=='my_donations')
							{
								echo JText::_('MOD_JGIVE_DONATION_ORDER_ID');
							}
							elseif ($module_for=='last_donations' OR $module_for=='top_donations')
							{
								echo JText::_('MOD_JGIVE_DONATION_DONOR');
							}
						?>
					</th>
					<th>
						<?php echo JText::_('MOD_JGIVE_DONATION_CAMP_NAME');?>
					</th>
					<th>
						<?php echo JText::_('MOD_JGIVE_DONATION_AMOUNT_DONATED');?>
					</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$i = 0;
			$total_amount = 0;

				foreach($result as $row)
				{?>
				<tr>
					<td class="small">
						<div>
							<?php
								if ($module_for=='my_donations')
								{
									echo $row->order_id;
								}
								elseif ($module_for=='last_donations' OR $module_for=='top_donations')
								{
									if (!$row->avatar)
									{
										// If no avatar, use default avatar
										$row->avatar = JUri::root(true) . '/media/com_jgive/images/default_avatar.png';
									}

									if($row->donor_id != 0)
									{
										$title = $row->first_name;
							?>
										<a href="<?php echo !empty($row->profile_url) ? $row->profile_url : '';  ?>" target="_blank">
											<img class="img-circle" src="<?php echo $row->avatar; ?>" title="<?php echo $title; ?>" width="20" height="20" />
										</a>
							<?php
									}
									else
									{
							?>
										<img class="img-circle" src="<?php echo $row->avatar; ?>" title="<?php echo JText::_('MOD_JGIVE_DONATION_GUEST'); ?>" width="20" height="20"/>
							<?php
									}
								}
							?>
						</div>
					</td>
					<td class="small">
						<div>
						<?php
								if(strlen($row->title)>=15)
								{?>
									<a href="<?php echo JUri::root().substr(JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid='.$row->cid.'&Itemid='.$singleCampaignItemid),strlen(JUri::base(true))+1);?>">
										<?php echo substr($row->title,0,10).'...';?>
									</a>
						<?php
								}
								else
								{?>
									<a href="<?php echo JUri::root().substr(JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid='.$row->cid.'&Itemid='.$singleCampaignItemid),strlen(JUri::base(true))+1);?>">
										<?php echo $row->title;?>
									</a>
						<?php	}
						?>
						</div>
					</td>
					<td class = "small" style="word-break: break-word;">
						<div>
						<?php
							$com_jgive_params = JComponentHelper::getParams('com_jgive');
							$jgiveFrontendHelper = new jgiveFrontendHelper();
							echo $diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($row->amount);
							$total_amount = $total_amount + $row->amount;
						?>
						</div>
					</td>
				</tr>
			<?php
				}?>
			</tbody>
		<?php
		}?>
		<?php
		if (count($result) <= 0 )
		{ ?>
			<tbody>
				<tr>
					<td colspan = "3"  >
						<?php echo JText::_("COM_JGIVE_NO_DONATION");?>
					</td>
				</tr>
			</tbody>
		<?php
		}
		elseif ($module_for == 'my_donations')
		{
		?>
			<tbody>
				<tr>
					<td colspan="3" class="small" style="text-align:right;">
						<?php
							require_once JPATH_SITE . "/components/com_jgive/helper.php";
							$jgiveFrontendHelper = new jgiveFrontendHelper;

							$myDonationItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my'); ?>

							<a href="<?php echo JUri::root() . substr(JRoute::_('index.php?option=com_jgive&view=donations&layout=my&Itemid='. $myDonationItemid), strlen(JUri::base(true)) + 1)?>">
								<b><?php echo JText::_('MOD_JGIVE_DONATION_ALL');?></b>
							</a>
					</td>
				</tr>
			</tbody>
		<?php
		}?>
	</table>
</div>

