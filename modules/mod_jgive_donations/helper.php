<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Module Donation helper
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class ModJGiveDonationHelper
{
	/**
	 * Get Featured campaigns
	 *
	 * @param   INT  $no_of_record_show  The value that define How much record shown
	 *
	 * @return  result
	 *
	 * @since   1.7
	 */
	public function lastDonations($no_of_record_show)
	{
		$where = " ORDER BY i.mdate DESC ";

		return $result = $this->getData($where, $no_of_record_show);
	}

	/**
	 * Get Top Donation
	 *
	 * @param   INT  $no_of_record_show  The value that define How much record shown
	 *
	 * @return  result
	 *
	 * @since   1.7
	 */
	public function topDonations($no_of_record_show)
	{
		$where = " ORDER BY i.original_amount DESC ";

		return $result = $this->getData($where, $no_of_record_show);
	}

	/**
	 * Get Campaign owner Donation
	 *
	 * @param   INT  $no_of_record_show  The value that define How much record shown
	 *
	 * @return  result
	 *
	 * @since   1.7
	 */
	public function myDonations($no_of_record_show)
	{
		$userid = JFactory::getUser()->id;
		$where  = "AND d.user_id <> 0  AND d.user_id=" . $userid;

		return $result = $this->getData($where, $no_of_record_show);
	}

	/**
	 * Get data
	 *
	 * @param   String  $where              Condition
	 * @param   INT     $no_of_record_show  The value that define How much record shown
	 *
	 * @return  result
	 *
	 * @since   1.7
	 */
	public function getData($where, $no_of_record_show)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT i.id, i.order_id,i.amount,i.cdate, d.user_id AS donor_id, c.id AS cid, c.title,u.name, d.first_name, d.last_name
				FROM #__jg_orders AS i
				LEFT JOIN #__jg_campaigns AS c ON c.id     = i.campaign_id
				LEFT JOIN #__jg_donors AS d on d.id        = i.donor_id
				LEFT JOIN #__jg_donations AS don on don.id = i.donation_id
				LEFT JOIN #__users AS u  on d.user_id      = u.id
				WHERE don.annonymous_donation = 0 AND i.status='C'  " . $where . " LIMIT 0, " . $no_of_record_show . "";
		$db->setquery($query);

		$result = $db->loadobjectlist();

		// Get User avatar & profile URL
		$result = $this->_getDonorAvatar($result);

		return $result;
	}

	/**
	 * Get donor avatar
	 *
	 * @param   Array  $donars  Donors
	 *
	 * @return  donors
	 *
	 * @since   1.7
	 */
	public function _getDonorAvatar($donars)
	{
		$JgiveIntegrationsHelperPath = JPATH_SITE . '/components/com_jgive/helpers/integrations.php';

		// Load integrations helper file
		if (!class_exists('JgiveIntegrationsHelper'))
		{
			JLoader::register('JgiveIntegrationsHelper', $JgiveIntegrationsHelperPath);
			JLoader::load('JgiveIntegrationsHelper');
		}

		$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;

		foreach ($donars as $donar)
		{
			$donar->avatar      = $JgiveIntegrationsHelper->getUserAvatar($donar->donor_id);
			$donar->profile_url = $JgiveIntegrationsHelper->getUserProfileUrl($donar->donor_id);
		}

		return $donars;
	}

	/**
	 * Function For where condition .
	 *
	 * @param   Array  $array   Orderby
	 * @param   INT    $column  column
	 * @param   INT    $order   order
	 * @param   INT    $count   count
	 *
	 * @return  Query
	 *
	 * @since	1.8
	 */
	public function multi_d_sort($array, $column, $order, $count)
	{
		foreach ($array as $key => $row)
		{
			$orderby[$key] = $row->$column;
		}

		if ($order == 'ASC')
		{
			array_multisort($orderby, SORT_ASC, $array);
		}
		else
		{
			if (!empty($array))
			{
				array_multisort($orderby, SORT_DESC, $array);
			}
		}

		return $array;
	}
}
