<div id="es" style="    margin-top: -21px;" class="es-side-widget__bd">
    <ul class="o-tabs o-tabs--stacked feed-items" data-dashboard-feeds="">
        <li class="o-tabs__item <?php if($_REQUEST['d_filter'] == 'all'): ?>active<?php endif; ?>" data-filter-item="" data-type="donation_type" data-d_filter="all">
            <a href="<?php echo ESR::dashboard(array('type' => 'donation_type','d_filter' => 0));?>" class="o-tabs__link">
                All
            </a>
            <div class="o-loader o-loader--sm"></div>
        </li>
        <li class="o-tabs__item <?php if($_REQUEST['d_filter'] == 'new'): ?>active<?php endif; ?>" data-filter-item="" data-type="donation_type" data-d_filter="new">
            <a href="<?php echo ESR::dashboard(array('type' => 'donation_type','d_filter' => 0));?>" class="o-tabs__link">
                Newest
            </a>
            <div class="o-loader o-loader--sm"></div>
        </li>
        <li class="o-tabs__item <?php if($_REQUEST['d_filter'] == 'old'): ?>active<?php endif; ?>" data-filter-item="" data-type="donation_type" data-d_filter="old">
            <a href="<?php echo ESR::dashboard(array('type' => 'donation_type','d_filter' => 0));?>" class="o-tabs__link">
                Oldest
            </a>
            <div class="o-loader o-loader--sm"></div>
        </li>

        <?php foreach($types as $type): ?>
        <li class="o-tabs__item <?php if($_REQUEST['d_filter'] == $type->id): ?>active<?php endif; ?>" data-filter-item="" data-type="donation_type" data-d_filter="<?php echo $type->id; ?>">
            <a href="<?php echo ESR::dashboard(array('type' => 'donation_type','d_filter' => $type->id));?>" class="o-tabs__link">
                <?php echo $type->name; ?>
            </a>
            <div class="o-loader o-loader--sm"></div>
        </li>
        <?php endforeach; ?>
    </ul>
</div>