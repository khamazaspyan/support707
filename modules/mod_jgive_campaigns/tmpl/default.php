<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

$document = JFactory::getDocument();

// No direct access
defined('_JEXEC') or die('Restricted access');

$document->addStyleSheet(JUri::root() . 'modules/mod_jgive_campaigns/css/jgive_campaign.css');

// Load Campaigns helper
$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

if (!class_exists('campaignHelper'))
{
	JLoader::register('campaignHelper', $helperPath);
	JLoader::load('campaignHelper');
}

$params_jgive = JComponentHelper::getParams('com_jgive');

// Get the data to idetify which field to show
$show_selected_fields = $params_jgive->get('show_selected_fields');
$creatorfield = array();
$show_field = 0;
$show_goal_amount = 0;

if ($show_selected_fields)
{
	$creatorfield = $params_jgive->get('creatorfield');

	if (isset($creatorfield))
	{
		foreach ($creatorfield as $tmp)
		{
			switch ($tmp)
			{
				case 'goal_amount':
					$show_goal_amount = 1;
				break;
			}
		}
	}
}
else
{
	$show_field = 1;
}?>

<?php
// Get items ids create jgiveFrontendHelper object
require_once JPATH_SITE . "/components/com_jgive/helper.php";
$jgiveFrontendHelper = new jgiveFrontendHelper;

// Use campaign helper
require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
$campaignHelper = new campaignHelper;

$allCampaignsItemid = $itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');

if (empty($itemid))
{
	$itemid = $allCampaignsItemid;
}

$cdata = array();

// Modifiy the data
foreach ($result as $d)
{
	// Get campaign amounts
	$amounts = $campaignHelper->getCampaignAmounts($d->id);
	$d->amount_received = $amounts['amount_received'];
	$d->remaining_amount = $amounts['remaining_amount'];
	$d->donor_count = $campaignHelper->getCampaignDonorsCount($d->id);
}

if ($orderby == 'amount_received' or $orderby = 'remaining_amount')
{
	$Modulehelper = JPATH_SITE . '/modules/mod_jgive_campaigns/helper.php';

	if (!class_exists('modJGiveHelper'))
	{
		JLoader::register('modJGiveHelper', $Modulehelper);
		JLoader::load('modJGiveHelper');
	}

	$filter = $orderby;
	$modJGiveHelper = new modJGiveHelper;

	if (!empty($result))
	$result = $modJGiveHelper->multi_d_sort($result, $filter, $orderby_dir, $count);
}

// Get currency from component config to do change component parameter variable name
$com_params = JComponentHelper::getParams('com_jgive');
$currency_code = $com_params->get('currency');
?>

<div class="tjBs3 <?php echo $params->get('moduleclass_sfx'); ?>">
<?php
$arraycnt = count($result);
$arraycnt < $count ? $count = $arraycnt:$count;

if ($count <= 0)
{
?>
	<div class="alert alert-warning">
		<?php echo JText::_('COM_JGIVE_NO_DATA_FOUND');?>
	</div>
<?php
}
else
{
	for ($i = 0; $i < $count; $i++)
	{
	?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php
				// Show the star to know the campaigns marked as Featured
					$title = JText::_('MOD_JGIVE_FEATURED');
					$featured = $campaignHelper->isFeatured($result[$i]->id);
				?>
				<h3 class="panel-title">
					<a target="_blank"
					href="<?php echo JUri::root() . substr(
					JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $result[$i]->id . '&Itemid=' . $itemid),
					strlen(JUri::base(true)) + 1
					);?>">
						<?php echo $featured ? $imgpath = '<img src="' . JUri::root() . 'media/com_jgive/images/featured.png"  title="' . $title . '">':'';?>
						<b><?php echo $result[$i]->title;?></b>
					</a>
				</h3>
			</div>
			<div class="panel-body mod_camp">
				<div class="center col-sm-12 col-md-12 col-lg-12">
					<a target="_blank"
					href="<?php echo JUri::root() . substr(
					JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $result[$i]->id . '&Itemid=' . $itemid),
					strlen(JUri::base(true)) + 1
					);?>">
						<?php
						if ($result[$i]->path)
						{
							$path = 'images/jGive/';
							$fileParts = pathinfo($result[$i]->path);

							if (file_exists($path . $fileParts['basename']))
							{
								$img_path = JUri::base() . $path . $fileParts['basename'];
							}
							else
							{
								$img_path = JUri::base() . $path . 'S_' . $fileParts['basename'];
							}

							echo "<img class='img-thumbnail' src='" . $img_path . "' />";
						}
						?>
					</a>
				</div>
				<?php
				// Check if exeeding goal amount is allowed
				$flag = 0;

				if ($result[$i]->allow_exceed == 0)
				{
					if ($result[$i]->amount_received >= $result[$i]->goal_amount)
					{
						$flag = 1;
					}
				}

				if ($result[$i]->max_donors > 0)
				{
					if ($result[$i]->donor_count >= $result[$i]->max_donors)
					{
						$flag = 1;
					}
				}

				/* If both start date, and end date are present
				The(int) typecasting is important*/
				if ((int) $result[$i]->start_date && (int) $result[$i]->end_date)
				{
					$result[$i]->start_date;
					$result[$i]->end_date;

					$start_date = JFactory::getDate($result[$i]->start_date)->Format(JText::_('Y-m-d H:i:s'));
					$curr_date  = JFactory::getDate()->Format(JText::_('Y-m-d H:i:s'));
					$end_date   = JFactory::getDate($result[$i]->end_date)->Format(JText::_('Y-m-d H:i:s'));

					if ($curr_date > $end_date)
					{
						// Camp closed
						$flag = 0;
					}
					elseif ($curr_date < $start_date)
					{
						// Campaign Not yet started
						$flag = -1;
					}
					else
					{
						$flag = 1;
					}
				}
				?>
				<?php
				// Calculate progress bar data
				$recPer = 0;
				$goal_amount = (float) $result[$i]->goal_amount;

				if (!empty($result[$i]->amount_received) && $goal_amount > 0)
				{
					$recPer = intval((100 * $result[$i]->amount_received) / $result[$i]->goal_amount);
				}

				if ($recPer > 100)
				{
					$recPer = 100;
					$progresslabel = JText::_('MOD_JGIVE_MORE_THAN_HUNDRED') . ' %';
				}
				else
				{
					$progresslabel = $recPer . '%';
				}

				if ($show_field == 1 OR $show_goal_amount == 0 )
				{
				?>
				<div class="clearfix">&nbsp;</div>
					<div class="col-sm-12 col-md-12 col-lg-12">
						<div class="progress" >
							<div class="progress-bar-success" style="width:<?php echo $recPer;?>%;min-width: 2em;">
								<b class="com_jgive_progress_text"><?php echo $progresslabel;?></b>
							</div>
						</div>
					</div>

				<?php
				}

				if ($result[$i]->success_status == 1)
				{
				?>
					<div class="mod_rubber_stamp">
						<span class="mod_campaign_rubber_stamp_text">
							<?php echo JText::_('COM_JGIVE_SUCCESS_STAMP'); ?>
						</span>
					</div>
				<?php
				}

				if ($params->get('show_goal_remaining') != 0 || $params->get('show_received'))
				{
				?>
					<table class="table table-hover">
					<?php
						// Conditions added by Sneha
						if ($params->get('show_goal_remaining'))
						{
						?>
							<tr>
								<td class="text-center">
									<?php echo JText::_('MOD_JGIVE_GOAL_AMOUNT');?>
								</td>
								<td class="text-center">
									<?php
									/*$jgiveFrontendHelper = new jgiveFrontendHelper();*/
									echo $diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($result[$i]->goal_amount);
									?>
								</td>
							</tr>
						<?php
						}

						if ($params->get('show_received'))
						{
						?>
							<tr>
								<td class="text-center">
									<?php echo JText::_('MOD_JGIVE_RECEIVED_AMOUNT');?>
								</td>
								<td class="text-center">
									<?php
									$jgiveFrontendHelper = new jgiveFrontendHelper;
									echo $diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($result[$i]->amount_received);
									?>
								</td>
							</tr>
						<?php
								// Donate button style whene show amount is on
								$donate_button_style = 'show_amount_on';
						}
						else
						{
							// Donate button style whene show amount is off
							$donate_button_style = 'show_amount_off';
						}

						if ($params->get('show_goal_remaining'))
						{
						?>
							<tr>
								<td class="text-center">
									<?php echo JText::_('MOD_JGIVE_REMAINING_AMOUNT');?>
								</td>
								<td class="text-center">
									<?php
										if ($result[$i]->amount_received > $result[$i]->goal_amount)
										{
											echo JText::_('COM_JGIVE_GOAL_ACHIEVED');
										}
										else
										{
											$jgiveFrontendHelper = new jgiveFrontendHelper;
											echo $diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($result[$i]->remaining_amount);
										}?>
								</td>
							</tr>
					<?php
						}?>
				</table>
			<?php
				}?>

			<div class="center col-sm-12 col-md-12 col-lg-12">
				<?php
				if ($flag == 1)
				{
				?>
					<form action="" method="post" name="campaignForm" id="campaignForm">
						<input type="hidden" name="cid" value="<?php echo $result[$i]->id;?>">
						<button class="btn btn-medium btn-primary " type="submit">
							<?php $result[$i]->type == "donation" ? $donate = JText::_('MOD_JGIVE_DONATE') :$donate = JText::_('MOD_JGIVE_INVEST');
							echo $donate;	?>
						</button>
						<input type="hidden" name="option" value="com_jgive">
						<input type="hidden" name="task" value="donations.donate">
					</form>
				<?php
				}
				elseif ($flag == -1)
				{
				?>
					<input type="button" class="btn btn-mini disabled" value="<?php echo JText::_('COM_JGIVE_WILL_START_SOON');?>" />
				<?php
				}
				elseif ($flag == 0)
				{
				?>
					<input type="button" class="btn btn-mini disabled" value="<?php echo JText::_('MOD_JGIVE_DONATIONS_CLOSED');?>" />
				<?php
				}?>
			</div>
		</div>
			<div class="clearfix"></div>
		</div>
	<?php
	}
}?>
</div>

<div class="row">
	<div class="pull-right col-sm-6 col-md-6 col-lg-6">
		<a href="<?php echo JUri::root() . substr(
			JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&Itemid=' . $allCampaignsItemid), strlen(JUri::base(true)) + 1
			);?>">
			<b><?php echo JText::_('MOD_JGIVE_CAMPAIGNS_ALL_PROJECT'); ?></b>
		</a>
	</div>
	<div class="clearfix"></div>
</div>
