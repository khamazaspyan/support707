<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

// Load helper File
require_once dirname(__FILE__) . '/helper.php';
$db             = JFactory::getDBO();

// Get Params
$orderby        = $params->get('campaigns_sort_by');
$orderby_dir    = $params->get('order_dir');
$count          = $params->get('no_of_camp_show');
$image          = $params->get('image');

// Model helper object
$modJGiveHelper = new modJGiveHelper;
$featured_camp  = $params->get('featured_camp');

// For group campaign module
$module_for = $params->get('module_for');

$groupid    = '';
$group_page = 0;

if ($module_for == 'js_group_camp')
{
	// Get js group id
	$input = JFactory::getApplication()->input;

	if ($input->get('task') == 'viewgroup')
	{
		$group_page = 1;
		$groupid    = $input->get('groupid', '', 'INT');
	}
	else
	{
		return;
	}
}

// Sort by Remaing Amount
if ($params->get('campaigns_sort_by') == 'amount_remaining')
{
	// Get Campaigns data
	$result = $modJGiveHelper->getData($featured_camp, $group_page, $groupid);

	foreach ($result as $key => $d)
	{
		// Get Amount for each campaigns
		$amounts = $modJGiveHelper->getCampaignAmounts($d->id, $image);
		$d->path = $amounts['path'];
	}
}
else
// Other Sort, get query for Campaigns Data
{
	$where = $modJGiveHelper->getwhere($groupid, $group_page, $orderby, $orderby_dir, $count, $image, $featured_camp);
	$db->setQuery($where);
	$result = $db->loadobjectlist();

	foreach ($result as $key => $d)
	{
		// Get Amount for each campaigns
		$amounts = $modJGiveHelper->getCampaignAmounts($d->id, $image);
		$d->path = $amounts['path'];
	}
}

// Call default.php
require JModuleHelper::getLayoutPath('mod_jgive_campaigns');
