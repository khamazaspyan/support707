<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * JGive Campaigns module helper class.
 *
 * @package  JGive
 * @since    1.8
 */
class ModJGiveHelper
{
	/**
	 * Function Get Data.
	 *
	 * @param   string  $group_page     Group_page
	 * @param   string  $groupid        Groupid
	 * @param   string  $featured_camp  Featured_camp
	 *
	 * @return  Goal Amount.
	 *
	 * @since	1.8
	 */
	public function getData($group_page, $groupid, $featured_camp = '')
	{
		// Module js group
		$for_group_camp = " ";

		if (($group_page == 1) && (!empty($groupid)))
		{
			$for_group_camp = " AND c.js_groupid= " . $groupid . " ";
		}

		$featured_camp ? $featured_camp = " WHERE c.featured = 1 AND c.published=1 AND i.gallery=0 " .
		$for_group_camp : $featured_camp = " WHERE c.published=1  AND i.gallery=0 " . $for_group_camp;
		$db    = JFactory::getDBO();
		$query = "SELECT c.max_donors,COUNT(c.id) AS total,
				c.id,o.status, c.title,c.goal_amount,c.start_date,c.end_date,
				c.allow_exceed,c.type , SUM(o.amount) AS amount_received
				FROM #__jg_campaigns as c
				LEFT JOIN #__jg_orders as o ON o.campaign_id=c.id
				LEFT JOIN #__jg_campaigns_images as i ON i.campaign_id=c.id
				$featured_camp
				GROUP BY c.id";
		$db->setQuery($query);
		$goal_amount = $db->loadobjectlist();

		return $goal_amount;
	}

	/**
	 * Function For get campaign Amount .
	 *
	 * @param   INT  $cid    Campaign Id
	 * @param   INT  $image  Image
	 *
	 * @return  Query
	 *
	 * @since	1.8
	 */
	public function getCampaignAmounts($cid, $image)
	{
		$db      = JFactory::getDBO();
		$amounts = array();

		if ($image)
		{
			$query = $db->getQuery(true);
			$query->select('i.path');
			$query->from($db->qn('#__jg_campaigns_images', 'i'));
			$query->where($db->qn('i.campaign_id') . ' = ' . $db->quote($cid));
			$query->where($db->qn('i.gallery') . ' = ' . $db->quote('0'));

			$db->setQuery($query);
			$amounts['path'] = $db->loadResult();
		}
		else
		{
			$amounts['path'] = 0;
		}

		$query = $db->getQuery(true);
		$query->select('c.max_donors');
		$query->select('COUNT(c.id) AS total');
		$query->select('c.id');
		$query->select('c.goal_amount');
		$query->select('c.type');
		$query->select('c.allow_exceed');
		$query->from($db->qn('#__jg_campaigns', 'c'));
		$query->where($db->qn('c.id') . ' = ' . $db->quote($cid));
		$db->setQuery($query);
		$data = $db->loadObjectList();

		$query = $db->getQuery(true);
		$query->select('Sum(o.amount) AS AS amount_received');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->where($db->qn('o.campaign_id') . ' = ' . $db->quote($cid));
		$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));
		$db->setQuery($query);

		// If no donations, set received amount as zero

		// Calculate remaining amount
		$amounts['goal_amount']  = $data[0]->goal_amount;
		$amounts['id']           = $data[0]->id;
		$amounts['type']         = $data[0]->type;
		$amounts['allow_exceed'] = $data[0]->allow_exceed;
		$amounts['total']        = $data[0]->total;
		$amounts['max_donors']   = $data[0]->max_donors;

		return $amounts;
	}

	/**
	 * Function For where condition .
	 *
	 * @param   INT  $groupid        Groupid
	 * @param   INT  $group_page     Group_page
	 * @param   INT  $orderby        Orderby
	 * @param   INT  $orderby_dir    Orderby_dir
	 * @param   INT  $count          Count
	 * @param   INT  $image          Image
	 * @param   INT  $featured_camp  Campaign Id
	 *
	 * @return  Query
	 *
	 * @since	1.8
	 */
	public function getwhere($groupid, $group_page, $orderby = '', $orderby_dir = '', $count = '', $image = '', $featured_camp = '')
	{
		// Module js group
		$for_group_camp = " ";

		if (($group_page == 1) && (!empty($groupid)))
		{
			$for_group_camp = " AND c.js_groupid= " . $groupid . " ";
		}

		$featured_camp ? $featured_camp = " WHERE c.featured = 1 AND c.published=1 AND i.gallery=0 " .
		$for_group_camp : $featured_camp = " WHERE c.published=1  AND i.gallery=0 " . $for_group_camp;
		$select = ',';

		if ($image)
		{
			$select = ",i.path,";
		}

		$query = "SELECT c.max_donors,COUNT(c.id) AS total,
				c.id,o.status,c.title,c.goal_amount,c.start_date,c.end_date,
				c.allow_exceed,c.type $select SUM(o.amount) AS amount_received,c.success_status
				FROM #__jg_campaigns as c
				LEFT JOIN #__jg_orders as o ON o.campaign_id=c.id
				LEFT JOIN #__jg_campaigns_images as i ON i.campaign_id=c.id
				$featured_camp
				GROUP BY c.id
				ORDER BY $orderby $orderby_dir LIMIT 0, $count";

		return $query;
	}

	/**
	 * Function For where condition .
	 *
	 * @param   Array  $array   Orderby
	 * @param   INT    $column  column
	 * @param   INT    $order   order
	 * @param   INT    $count   count
	 *
	 * @return  Query
	 *
	 * @since	1.8
	 */
	public function multi_d_sort($array, $column, $order, $count)
	{
		foreach ($array as $key => $row)
		{
			$orderby[$key] = $row->$column;
		}

		if ($order == 'ASC')
		{
			array_multisort($orderby, SORT_ASC, $array);
		}
		else
		{
			if (!empty($array))
			{
				array_multisort($orderby, SORT_DESC, $array);
			}
		}

		return $array;
	}
}
