EasySocial.module("story/blog", function($){

    var module = this;

    EasySocial.Controller("Story.Blog", {
            defaultOptions: {
                "{title}" : "[data-blog-title]",
                "{content}" : "[data-blog-content]",
                "{category}": "[data-story-blog-category]",
                "{sourceId}": "[data-blog-source-id]",
                "{sourceType}": "[data-blog-source-type]"
            }
        }, function(self) {
            return {

            init: function() {
            },

            resetForm: function() {
                self.title().val('');
                self.content().val('');
            },

            "{story} save": function(element, event, save) {

                if (save.currentPanel != 'blog') {
                    return;
                }
                
                // Add the task for uploading video
                self.savePost = save.addTask('savePost');

                self.save(save);
            },

            save: function(save) {

                var savePost = self.savePost;


                if (!savePost) {
                    return;
                }

                var title = self.title().val();

                if (!title) {
                    self.clearMessage();
                    save.reject('<?php echo $errorTitle;?>');
                    return false;
                }

                var content = self.content().val();

                if (!content) {
                    self.clearMessage();
                    save.reject('<?php echo $errorContent;?>');
                    return false;
                }

                // Perform validation checks here.
                var data = {
                    "categoryId": self.category().val(),
                    "title": self.title().val(),
                    "content": self.content().val(),
                    "sourceId": self.sourceId().val(),
                    "sourceType": self.sourceType().val()
                };

                self.resetForm();

                save.addData(self, data);

                savePost.resolve();

                delete self.savePost;

            }
        }}
    );

    // Resolve module
    module.resolve();

});


EasySocial.require()
.script("story/blog")
.done(function($) {
    var plugin = story.addPlugin("blog");
});

