<?php
/**
 * @package		JBolo
 * @version		$versionID$
 * @author		TechJoomla
 * @author mail	extensions@techjoomla.com
 * @website		http://techjoomla.com
 * @copyright	Copyright © 2009-2013 TechJoomla. All rights reserved.
 * @license		GNU General Public License version 2, or later
*/
//no direct access
if ( ! ( defined( '_VALID_CB' ) || defined( '_JEXEC' ) || defined( '_VALID_MOS' ) ) ) { die( 'Direct Access to this location is not allowed.' ); }
class cbonlineTab  extends cbTabHandler
{
	function cbonlineTab(){
		$this->cbTabHandler();
	}

	function getDisplayTab($tab,$user,$ui)
	{
		$params=JComponentHelper::getParams('com_jbolo');
		$fonly=$params->get('fonly');

 		$lang=JFactory::getLanguage();
		$lang->load('com_jbolo');
		$my=JFactory::getUser();
		$db=JFactory::getDBO();
		global $return;

		//load nodes helper file
		$nodesHelperPath=JPATH_SITE.DS.'components'.DS.'com_jbolo'.DS.'helpers'.DS.'nodes.php';
		if(!class_exists('nodesHelper'))
		{
			JLoader::register('nodesHelper',$nodesHelperPath );
			JLoader::load('nodesHelper');
		}

		$nodesHelper=new nodesHelper();

		$my=JFactory::getUser();

		$isBlocked=$nodesHelper->isBlockedBy($user->id,$my->id);
		if(!$isBlocked)
		{
			$isBlocked=$nodesHelper->isBlockedBy($my->id,$user->id);
		}

		//Not to show user status if one of both, blocked one to one chat
		if($isBlocked)
			return;

		if($fonly)//if jbolo config is set to friends only chat
		{
			$accepted=0;
			//chk if connection request sent  by logged in user to other user whose profile he is on
			$query="SELECT m.accepted
			FROM #__comprofiler_members AS m
			WHERE m.pending=0
			AND m.referenceid=".$my->id."
			AND m.memberid=".$user->id;
			$db->setQuery($query);
			$acceptedLIU=$db->loadResult();

			//chk if connection request sent by logged in user is accepted by other user
			$query="SELECT m.accepted
			FROM #__comprofiler_members AS m
			WHERE m.pending=0
			AND m.referenceid=".$user->id."
			AND m.memberid=".$my->id;
			$db->setQuery($query);
			$acceptedOU=$db->loadResult();
			//acceptedLIU => acceptedByLoggedInUser
			//acceptedOU => acceptedByOtherUser
			if($acceptedLIU && $acceptedOU){
				$accepted=1;
			}
		}
		else//jbolo config is set to chat with everyone
		{
			$accepted=0;
			if($user->jboloallowall)//cb privacy private
			{
				//connection request sent  by logged in user
				$query="SELECT m.accepted
				FROM #__comprofiler_members AS m
				WHERE m.pending=0
				AND m.referenceid=".$my->id."
				AND m.memberid=".$user->id;
				$db->setQuery($query);
				$acceptedLIU=$db->loadResult();

				//connection request accepted by other user
				$query="SELECT m.accepted
				FROM #__comprofiler_members AS m
				WHERE m.pending=0
				AND m.referenceid=".$user->id."
				AND m.memberid=".$my->id;
				$db->setQuery($query);
				$acceptedOU=$db->loadResult();

				if($acceptedLIU && $acceptedOU){
					$accepted=1;
				}
				else{
					$accepted=0;
				}
			}
			else //cb privacy public
			{
				$accepted=1;
			}
		}

		if($ui && $accepted)//if logged in and if conncection
		{
			$my=JFactory::getUser();
			if($my->id!=$user->id)//not on own profile
			{
				if($params->get('chatusertitle')){
					$chattitle='username';
				}else{
					$chattitle='name';
				}

				$db->setQuery("SELECT userid FROM #__session WHERE userid = $user->id");
				if($db->loadResult())
				{
					$return="<table width='100%'>
						<tr>
							<td class='titleCell'>
								<label for='Chat'>".JText::_('COM_JBOLO_CB_PLG_CHAT').":</label>
							</td>
							<td class='fieldCell'>
								<a style='text-decoration:none;' href='javascript:void(0)' onclick=\"javascript:chatFromAnywhere(".$my->id.",".$user->id.")\">
									<div class='statusicon_1'></div>
										<span>".
											sprintf(JText::_('COM_JBOLO_CB_PLG_ONLINE_MSG'),$user->username)."
										</span>
								</a>
							</td>
						</tr>
					</table>";
				}
				else
				{
					$return="
					<table width=100% >
						<tr>
							<td class='titleCell'>
								<label for='Chat'>".JText::_('COM_JBOLO_CB_PLG_CHAT').":</label>
							</td>
							<td class='fieldCell'>
								<div class='statusicon_4'></div>
									<span>".
										sprintf(JText::_('COM_JBOLO_CB_PLG_OFFLINE_MSG'),$user->username)."
									</span>
							</td>
						</tr>
					</table>";
				}
			}
		}
		else if ($user->id!=$my->id)//if logged in and if NO conncection and IF not on own profile
		{
			$return="
			<table width=100% >
				<tr>
					<td class='titleCell'>
						<label for='Chat'>".JText::_('COM_JBOLO_CB_PLG_CHAT').":</label>
					</td>
					<td class='fieldCell'>".JText::_('COM_JBOLO_CB_PLG_FRND_MSG')."</td>
				</tr>
			</table>";
		}
		return $return;
	}

	//***********
	function getEditTab($tab,$user,$ui)
	{
		$lang=JFactory::getLanguage();
		$lang->load('com_jbolo');
		$public='';
		$private='';
		if($user->jboloallowall)
			$private='checked';
		else
			$public='checked';
		if($ui)
		{
			$html='<table cellspacing="0" cellpadding="0"><tr class="sectiontableentry2" >';
			$html.='<td class="titleCell">
				<label for="pcode">'.JText::_('COM_JBOLO_CB_PLG_CHAT_PRIVACY').':</label>
			</td>';
			$html.='<td class="fieldCell" >
				<input name="jboloallowall" id="jboloallowall" '.$public.'  value=0 class="inputbox" type="radio">'.JText::_('COM_JBOLO_CB_PLG_PUBLIC').
				'<input name="jboloallowall" id="jboloallowall" '.$private.' value=1 class="inputbox" type="radio">'.JText::_('COM_JBOLO_CB_PLG_PRIVATE').
			'</td>
			</tr>
			</table>';
			return $html;
		}
	}

	function saveEditTab($tab,&$user,$ui,$postdata)
	{
		$user->jboloallowall=cbGetParam($_POST, 'jboloallowall');
	}

}//end class
?>
