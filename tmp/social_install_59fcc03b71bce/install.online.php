<?php
/**
 * @package		JBolo
 * @version		$versionID$
 * @author		TechJoomla
 * @author mail	extensions@techjoomla.com
 * @website		http://techjoomla.com
 * @copyright	Copyright © 2009-2013 TechJoomla. All rights reserved.
 * @license		GNU General Public License version 2, or later
*/
//no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
function plug_cb_online_install()
{
	$database=JFactory::getDBO();
	$chk_qry="DESCRIBE #__comprofiler";
	$database->setQuery($chk_qry);
	$cb=$database->loadColumn();
	if(!in_array('jboloallowall',$cb))
	{
		$installqry="ALTER  TABLE `#__comprofiler` ADD `jboloallowall` INT( 2 ) NOT NULL ;";
		$database->setQuery($installqry);
		$database->execute();
	}
	return 1;
}
?>