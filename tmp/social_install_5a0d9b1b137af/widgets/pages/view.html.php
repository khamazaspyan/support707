<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

class SupportfamilyWidgetsGroups extends SocialAppsWidgets
{
    public function exists()
    {
        $file = JPATH_ROOT . '/administrator/components/com_easyblog/includes/easyblog.php';

        if (!JFile::exists($file)) {
            return false;
        }

        require_once($file);

        return true;
    }

    /**
     * Renders the new post button for the group
     *
     * @since   5.0
     * @access  public
     * @param   string
     */
	public function groupAdminStart($group)
	{
        if (!$this->exists()) {
            return;
        }

        // @since 4.0
        // Attach the theme's css
        $config = EB::config();

        $stylesheet = EB::stylesheet('site', $config->get('theme_site'));
        $stylesheet->attach();

		// Render the scripts on the page.
		EB::init('composer');

		$theme = ES::themes();
		$theme->set('group', $group);
		$theme->set('app', $this->app);

		echo $theme->output('themes:/apps/group/blog/widgets/widget.menu');
	}

    /**
     * Renders a list of blog posts created in the group
     *
     * @since   1.0
     * @access  public
     * @param   string
     * @return
     */
    public function sidebarBottom($groupId)
    {
        // Set the max length of the item
        $params = $this->app->getParams();
        $enabled = $params->get('widget', true);

        if (!$enabled) {
            return;
        }

        $theme = FD::themes();

        // Get the group
        $group = FD::group($groupId);

        $limit = (int) $params->get('widget_total', 5);
        $options = array('limit' => $limit);

        // Retrieve the model
        $model = $this->getModel('Blog');
        $posts = $model->getItems($group->id, $options);

        $pagination = $model->getPagination();

        $theme->set('group', $group);
        $theme->set('app', $this->app);
        $theme->set('posts', $posts);

        echo $theme->output('themes:/apps/group/blog/widgets/widget.posts' );
    }
}
