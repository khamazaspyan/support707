<?php/*** @version		1.0* @package		DJ Classifieds* @subpackage	DJ Classifieds and EasySocial integration* @copyright	Copyright (C) 2010 DJ-Extensions.com LTD, All rights reserved.* @license		http://www.gnu.org/licenses GNU/GPL* @autor url    http://design-joomla.eu* @autor email  contact@design-joomla.eu* @Developer    Lukasz Ciastek - lukasz.ciastek@design-joomla.eu* * * DJ Classifieds is free software: you can redistribute it and/or modify* it under the terms of the GNU General Public License as published by* the Free Software Foundation, either version 3 of the License, or* (at your option) any later version.** DJ Classifieds is distributed in the hope that it will be useful,* but WITHOUT ANY WARRANTY; without even the implied warranty of* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the* GNU General Public License for more details.** You should have received a copy of the GNU General Public License* along with DJ Classifieds. If not, see <http://www.gnu.org/licenses/>.* */
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

if(!defined("DS")){define('DS',DIRECTORY_SEPARATOR);}
require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_djclassifieds'.DS.'lib'.DS.'djseo.php');
require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_djclassifieds'.DS.'lib'.DS.'djtheme.php');require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_djclassifieds'.DS.'lib'.DS.'djimage.php');

class djcfitemsWidgetsProfile extends SocialAppsWidgets
{

    public function sidebarTop( $user ){
    	
		//$params 	= $this->getUserParams( $user->id );
		$params = $this->getParams();

		// User might not want to show this app in their profile.
		/*	if( !$params->get( 'widget-profile' , true ) )
		{
			return;
		}*/
		
		if($params->get( 'widget-position','sidebartop')=='sidebartop'){
			echo $this->getLatestAdverts( $user , $params );	
		}else{
			return;	
		} 					
	}
	
    public function aboveHeader( $user ){
    	// Get the user params
		$params = $this->getParams();
		
		if($params->get( 'widget-position','sidebartop')=='aboveheader'){
			echo $this->getLatestAdverts( $user , $params );	
		}else{
			return;	
		} 					
	}	

	public function sidebarBottom ( $user ){
    	// Get the user params
		$params = $this->getParams();
		if($params->get( 'widget-position','sidebartop')=='sidebarbottom'){
			echo $this->getLatestAdverts( $user , $params );	
		}else{
			return;	
		} 					
	}	
	
	public function afterBadges ( $user ){
    	// Get the user params
		$params = $this->getParams();
		
		if($params->get( 'widget-position','sidebartop')=='afterbadges'){
			echo $this->getLatestAdverts( $user , $params );	
		}else{
			return;	
		} 					
	}	
	
	public function beforeBadges( $user ){
    	// Get the user params
		$params = $this->getParams();
		
		if($params->get( 'widget-position','sidebartop')=='beforebadges'){
			echo $this->getLatestAdverts( $user , $params );	
		}else{
			return;	
		} 					
	}	
	
	public function afterActions( $user ){
    	// Get the user params
		$params = $this->getParams();
		
		if($params->get( 'widget-position','sidebartop')=='afteractions'){
			echo $this->getLatestAdverts( $user , $params );	
		}else{
			return;	
		} 					
	}	
	
	public function beforeActions( $user ){
    	// Get the user params
		$params = $this->getParams();
		
		if($params->get( 'widget-position','sidebartop')=='beforeactions'){
			echo $this->getLatestAdverts( $user , $params );	
		}else{
			return;	
		} 					
	}	
		
	public function afterInfo( $user ){
    	// Get the user params
		$params = $this->getParams();
		
		if($params->get( 'widget-position','sidebartop')=='afterinfo'){
			echo $this->getLatestAdverts( $user , $params );	
		}else{
			return;	
		} 					
	}	

	public function getLatestAdverts( $user, $params  )
    {
    
		if( !$params->get( 'widget-enabled' , true ) ){
			return;
		}		
		
		if($user->id){
				    	
			$document= JFactory::getDocument();
			DJClassifiedsTheme::includeCSSfiles();
			
			$language = JFactory::getLanguage();	
			$c_lang = $language->getTag();
				if($c_lang=='pl-PL' || $c_lang=='en-GB'){
					$language->load('com_djclassifieds', JPATH_SITE.'/components/com_djclassifieds', null, true);	
				}else{
					if(!$language->load('com_djclassifieds', JPATH_SITE, null, true)){
						$language->load('com_djclassifieds', JPATH_SITE.'/components/com_djclassifieds', null, true);
					}			
				}												$model 	= $this->getModel( 'Djcfitems' );				$items = $model->getItems( $user->id,$params->get( 'widget-adverts-limit') );

				$cfpar = JComponentHelper::getParams( 'com_djclassifieds' );									
					
				
				$this->set( 'items'	, $items );
				$this->set( 'user'	, $user );
				$this->set( 'params', $params );
				$this->set( 'cfpar'	, $cfpar );
		
				return parent::display( 'widgets/profile/default' );		
		}
    }
}