<?php/*** @version		1.0* @package		DJ Classifieds* @subpackage	DJ Classifieds and EasySocial integration* @copyright	Copyright (C) 2010 DJ-Extensions.com LTD, All rights reserved.* @license		http://www.gnu.org/licenses GNU/GPL* @autor url    http://design-joomla.eu* @autor email  contact@design-joomla.eu* @Developer    Lukasz Ciastek - lukasz.ciastek@design-joomla.eu* * * DJ Classifieds is free software: you can redistribute it and/or modify* it under the terms of the GNU General Public License as published by* the Free Software Foundation, either version 3 of the License, or* (at your option) any later version. ** DJ Classifieds is distributed in the hope that it will be useful,* but WITHOUT ANY WARRANTY; without even the implied warranty of* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the* GNU General Public License for more details.** You should have received a copy of the GNU General Public License* along with DJ Classifieds. If not, see <http://www.gnu.org/licenses/>.* */
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
// We want to import our app library
Foundry::import( 'admin:/includes/apps/apps' );if(!defined("DS")){define('DS',DIRECTORY_SEPARATOR);}require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_djclassifieds'.DS.'lib'.DS.'djseo.php');require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_djclassifieds'.DS.'lib'.DS.'djtheme.php');require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_djclassifieds'.DS.'lib'.DS.'djimage.php');
class SocialUserAppDjcfitems extends SocialAppItem{
	/**
	 * Class constructor.
	 *
	 * @since	1.0
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Triggers the preparation of stream.
	 *
	 * If you need to manipulate the stream object, you may do so in this trigger.
	 *
	 * @since	1.0
	 * @access	public
	 * @param	SocialStreamItem	The stream object.
	 * @param	bool				Determines if we should respect the privacy
	 */	public function onPrepareStream( SocialStreamItem &$item, $includePrivacy = true )	{
		//echo '<pre>';print_r($item);echo '</pre>';die();		// You should be testing for app context		if( $item->type == 'djclassifieds' )		{			$item->content = $item->content_raw;			return;		}	}
	/**	 * Triggers the preparation of activity logs which appears in the user's activity log.	 *	 * @since	1.0	 * @access	public	 * @param	SocialStreamItem	The stream object.	 * @param	bool				Determines if we should respect the privacy	 */
	public function onPrepareActivityLog( SocialStreamItem &$item, $includePrivacy = true )
	{
	}	/**	 * Triggers after a like is saved.	 *	 * This trigger is useful when you want to manipulate the likes process.	 *	 * @since	1.0	 * @access	public	 * @param	SocialTableLikes	The likes object.	 *	 * @return	none	 */
	public function onAfterLikeSave( &$likes )
	{
	}	/**	 * Triggered when a comment save occurs.	 *	 * This trigger is useful when you want to manipulate comments.	 *	 * @since	1.0	 * @access	public	 * @param	SocialTableComments	The comment object	 * @return	 */
	public function onAfterCommentSave( &$comment )
	{
	}  
	/**
	 * Renders the notification item that is loaded for the user.
	 *
	 * This trigger is useful when you want to manipulate the notification item that appears
	 * in the notification drop down.
	 *	 * @since	1.0	 * @access	public	 * @param	string	 * @return	 */
	public function onNotificationLoad( SocialTableNotification &$item )
	{
	}
}