<?php
/**
* @version		1.0
* @package		DJ Classifieds
* @subpackage	DJ Classifieds and EasySocial integration
* @copyright	Copyright (C) 2010 DJ-Extensions.com LTD, All rights reserved.
* @license		http://www.gnu.org/licenses GNU/GPL
* @autor url    http://design-joomla.eu
* @autor email  contact@design-joomla.eu
* @Developer    Lukasz Ciastek - lukasz.ciastek@design-joomla.eu
* 
* 
* DJ Classifieds is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DJ Classifieds is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DJ Classifieds. If not, see <http://www.gnu.org/licenses/>.
* 
*/

defined( '_JEXEC' ) or die( 'Unauthorized Access' );
$cols = $params->get( 'view-columns-limit',1);
$uid_slug = $user->id.':'.DJClassifiedsSEO::getAliasName($user->name);	
//echo '<pre>';print_r($user);die();
?>
<div class="profile-adverts">
	<legend>
		<div class="row">
			<div class="col-md-12">						
				<span class="">
					<?php 
					if( $user->isViewer() ){
						echo  JText::_('APP_DJCFITEMS_YOUR_ADVERTS') ;
					}else{
						echo  JText::_('APP_DJCFITEMS_ADVERTS_FOR').' '.$user->getName() ;
					}?>
				</span>
			</div>
		</div>
	</legend>
	<?php if( $items ){ ?>
	<div class="djcf_items items-cols<?php echo $cols; ?>" >
		<?php foreach( $items as $item ){ ?>
			<div class="item">
				<?php if($params->get( 'view-display-image') || $params->get( 'view-display-name')  ){ ?>
					<div class="title">
					<?php if(count($item->images) && $params->get( 'view-display-image') ){																						
							echo '<a class="title_img" href="'.JRoute::_(DJClassifiedsSEO::getItemRoute($item->id.':'.$item->alias,$item->cat_id.':'.$item->c_alias)).'">';
							echo '<img style="margin-right:3px;" src="'.JURI::base().$item->images[0]->thumb_s.'" alt="'.str_ireplace('"', "'", $item->name).'" title="'.str_ireplace('"', "'", $item->images[0]->caption).'" />';
							echo '</a>';
						} ?>
						<?php if($params->get( 'view-display-name')  ){ 																											
							echo '<a class="title" href="'.JRoute::_(DJClassifiedsSEO::getItemRoute($item->id.':'.$item->alias,$item->cat_id.':'.$item->c_alias)).'">'.$item->name.'</a>';
						} ?>	
					</div>				
				<?php } ?>		
				<div class="date_cat">
					<?php if($params->get( 'view-display-pdate')  ){ ?>								
						<span class="date">							
							<?php if($cfpar->get('date_format_type_modules',0)){
								echo DJClassifiedsTheme::dateFormatFromTo(strtotime($item->date_start));	
							}else{
								echo date($cfpar->get('date_format','Y-m-d H:i:s'),strtotime($item->date_start));	
							}?>
						</span>
					<?php } ?>											
					<?php if($params->get( 'view-display-pdate')  ){ ?>					
						<span class="category">																			
							<?php echo '<a class="title_cat" href="'.JRoute::_(DJClassifiedsSEO::getCategoryRoute($item->cat_id.':'.$item->c_alias)).'">'.$item->c_name.'</a>'; ?>												
						</span>
					<?php } ?>											
					<?php if($params->get( 'view-display-region')  ){ ?>																														
						<span class="region">
							<?php echo $item->r_name; ?>
						</span>
					<?php } ?>											
											
					<?php if($item->price && $params->get( 'view-display-price')){ ?>
						<span class="price">
							<?php echo DJClassifiedsTheme::priceFormat($item->price,$item->currency); ?>
						</span>
					<?php } ?>
				</div>
				<?php if($params->get( 'view-display-desc')  ){
						$desc_c = $params->get('view-desc-chars-limit');
						if($desc_c!=0 && $item->intro_desc!='' && strlen($item->intro_desc)>$desc_c){
							$item_desc = mb_substr($item->intro_desc, 0, $desc_c,'utf-8').' ...';
						}else{
							$item_desc = $item->intro_desc;
						}
					 ?>																												
					<div class="desc">																	
						<?php echo $item_desc; ?>		
					</div>
				<?php } ?>							
			</div>							
		<?php } ?>
		<div style="clear:both"></div>
	</div>
	
	<div class="alladverts_link">		
		<?php echo '<a class="profile_name" href="index.php?option=com_djclassifieds&view=profile&uid='.$uid_slug.DJClassifiedsSEO::getMainAdvertsItemid().'">'.JText::_('APP_DJCFITEMS_SEE_ALL_ADVERTS').'</a>'; ?>
	</div>	
<?php } else { ?>
	<p><?php echo JText::_('APP_DJCFITEMS_WIDGET_NO_ADVERTS_FOUND'); ?></p>
<?php } ?>
	
</div>