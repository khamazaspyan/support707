EasySocial.module("story/requestfound", function($){

    var module = this;

    EasySocial.Controller("Story.Requestfound", {
            defaultOptions: {
                "{title}" : "[data-requestfound-title]",
                "{content}" : "[data-requestfound-content]",
                "{category}": "[data-story-requestfound-category]",
                "{sourceId}": "[data-requestfound-source-id]",
                "{sourceType}": "[data-requestfound-source-type]"
            }
        }, function(self) {
            return {

            init: function() {
            },

            resetForm: function() {
                self.title().val('');
                self.content().val('');
            },

            "{story} save": function(element, event, save) {

                if (save.currentPanel != 'requestfound') {
                    return;
                }
                
                // Add the task for uploading video
                self.savePost = save.addTask('savePost');

                self.save(save);
            },

            save: function(save) {

                var savePost = self.savePost;


                if (!savePost) {
                    return;
                }

                var title = self.title().val();

                if (!title) {
                    self.clearMessage();
                    save.reject('<?php echo $errorTitle;?>');
                    return false;
                }

                var content = self.content().val();

                if (!content) {
                    self.clearMessage();
                    save.reject('<?php echo $errorContent;?>');
                    return false;
                }

                // Perform validation checks here.
                var data = {
                    "categoryId": self.category().val(),
                    "title": self.title().val(),
                    "content": self.content().val(),
                    "sourceId": self.sourceId().val(),
                    "sourceType": self.sourceType().val()
                };

                self.resetForm();

                save.addData(self, data);

                savePost.resolve();

                delete self.savePost;

            }
        }}
    );

    // Resolve module
    module.resolve();

});


EasySocial.require()
.script("story/requestfound")
.done(function($) {
    var plugin = story.addPlugin("requestfound");
});

