
EasyBlog.require()
.script('site/posts/posts')
.library('dialog')
.done(function($) {
    $('[data-requestfound-posts]').implement(EasyBlog.Controller.Posts);

    $('[data-requestfound-delete]').on('click', function(){
    	var id = $(this).data('post-id');
    	var returnUrl = $(this).data('return-url');

    	EasyBlog.dialog({
    		content: EasyBlog.ajax('site/views/entry/confirmDelete', {"id" : id, "return": returnUrl})
    	});
    });

})