<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

class RequestfoundViewPages extends SocialAppsView
{
	public function exists()
	{
		$file = JPATH_ROOT . '/administrator/components/com_easyblog/includes/easyblog.php';

		if (!JFile::exists($file)) {
			return false;
		}

		require_once($file);

		return true;
	}

	public function display($pageId = null, $docType = null)
	{
		if (!$this->exists()) {
			return;
		}

		$params = $this->app->getParams();

		// @since 4.0
		// Attach the theme's css
		$config = EB::config();

		$stylesheet = EB::stylesheet('site', $config->get('theme_site'));
		$stylesheet->attach();

		// Render the scripts on the page.
		EB::init('composer');

		// Load easyblog's language file
		EB::loadLanguages();

		// Get the current logged in user.
		$my = ES::user();
		
		// Load up the group
		$page = ES::page($pageId);

		$limit = (int) $params->get('listing_total', 5);
		$options = array('limit' => $limit);
		
		// Retrieve the model
		$model = $this->getModel('Requestfound');
		$posts = $model->getItems($page->id, $options);

		if ($posts) {
			foreach ($posts as $post) {
				$content = $post->getIntro('', true, 'all', null, $options = array('triggerPlugins' => false));

				// truncate the content
				$post->content = $this->truncateStreamContent($content, $params->get('listing_maxlength'));

				// Get the image
				$post->image = $post->getImage('medium');
			}
		}
		
		$pagination = $model->getPagination();
		$pagination->setVar('option' , 'com_easysocial');
		$pagination->setVar('view' , 'pages');
		$pagination->setVar('layout' , 'item');
		$pagination->setVar('id' , $page->getAlias());
		$pagination->setVar('appId' , $this->app->getAlias());        

		// Generate the return url
		$return = ESR::pages(array('layout' => 'item', 'id' => $page->getAlias(), 'appId' => $this->app->getAlias()));
		$return = base64_encode($return);

		$composeLink = EB::composer()->getComposeUrl(array('source_id' => $page->id, 'source_type' => 'easysocial.page', 'returnUrl' => $return, 'return' => $return));

		$this->set('return', $return);
		$this->set('composeLink', $composeLink);
		$this->set('posts', $posts);
		$this->set('page', $page);
		$this->set('params', $params);
		$this->set('my', $my);
		$this->set('pagination', $pagination);

		echo parent::display('pages/default');
	}

	/**
	 * Truncate the stream item
	 *
	 * @since   1.0
	 * @access  public
	 */
	public function truncateStreamContent($content, $contentLength)
	{
		// Get the app params
		$params = $this->getParams();
		$truncateType = $params->get('truncation');

		if ($truncateType == 'chars') {

			// Remove uneccessary html tags to avoid unclosed html tags
			$content = strip_tags($content);

			// Remove blank spaces since the word calculation should not include new lines or blanks.
			$content = trim($content);

			// @task: Let's truncate the content now.
			$content = JString::substr(strip_tags($content), 0, $contentLength) . JText::_('COM_EASYSOCIAL_ELLIPSES');

		} else {

			$tag = false;
			$count = 0;
			$output = '';

			// Remove uneccessary html tags to avoid unclosed html tags
			$content = strip_tags($content);

			$chunks = preg_split("/([\s]+)/", $content, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

			foreach($chunks as $piece) {

				if (!$tag || stripos($piece, '>') !== false) {
					$tag = (bool) (strripos($piece, '>') < strripos($piece, '<'));
				}

				if (!$tag && trim($piece) == '') {
					$count++;
				}

				if ($count > $contentLength && !$tag) {
					break;
				}

				$output .= $piece;
			}

			unset($chunks);
			$content = $output . JText::_('COM_EASYSOCIAL_ELLIPSES');
		}


		return $content;
	}
}
