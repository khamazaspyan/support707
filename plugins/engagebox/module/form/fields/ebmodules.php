<?php

/**
 * @package         Engage Box
 * @version         3.3.3 Pro
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2016 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('_JEXEC') or die('Restricted access');
JFormHelper::loadFieldClass('modules');

class JFormFieldEBModules extends JFormFieldModules
{
    protected function getInput()
    {
        $html[] = parent::getInput();
        $html[] = '
        	<a class="btn btn-small editModule" href="#">
        		<span class="icon-edit"></span> ' . JText::_('JLIB_HTML_EDIT_MODULE') . '
        	</a>
        ';

        JFactory::getDocument()->addScriptDeclaration('
        	jQuery(function($) {
        		$(".editModule").click(function() {
					moduleID = $(this).parent().find("select").val();
					url = "' . JURI::base() . '/index.php?option=com_modules&view=module&task=module.edit&id=" + moduleID;
					SqueezeBox.open(url, { handler: "iframe", size: {x: 1100, y: 635}});
        			return false;
        		})
        	})
        ');

        return implode("", $html);
    }
}