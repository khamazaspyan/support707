<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.file');
jimport('joomla.application.application');

/**
 * PlgSystemJgive_api class.
 *
 * @package  JGive
 * @since    1.8
 */
class PlgSystemJgive_Api extends JPlugin
{
	/**
	 *Function constructor.
	 *
	 * @param   Array  $options  Options
	 *
	 * @since	1.0
	 * @access	public
	 */
	public function __construct($options = array())
	{
		// Define DIRECTORY_SEPARATOR if not mostly for joomla 3.0
		if (!defined('DS'))
		{
			define('DS', DIRECTORY_SEPARATOR);
		}

		parent::__construct($options);
	}

	/**
	 * Function OnAfterJGiveCampaignSave
	 *
	 * @param   INT  $cid   Campaign Id
	 * @param   INT  $post  Post
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function OnAfterJGiveCampaignSave($cid, $post)
	{
		// Check the campaign is published.
		$published = $this->checkCampaignIsPublished($cid);

		if (!$published)
		{
			return false;
		}

		// Push activity to various activity streams.
		// Set some of the data for activity.
		$act_subtype     = 'campaign';
		$act_description = JText::_('COM_JGIVE_ACTIVITY_CREATED_CAMPAIGN') . ' ';
		$result          = $this->pushActivity($cid, $act_subtype, $act_description);

		if (!$result)
		{
			return false;
		}

		return true;
	}

	/**
	 * Function OnAfterJGiveCampaignDelete
	 *
	 * @param   INT  $cids  Campaign Id
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function OnAfterJGiveCampaignDelete($cids)
	{
	}

	/**
	 * Function OnAfterJGiveCampaignEdit
	 *
	 * @param   INT     $cid         Campaign Id
	 * @param   String  $post        Post
	 * @param   String  $newDetails  New Details
	 * @param   String  $oldDetails  Old Details
	 *
	 * @return  void.
	 *
	 * @since  1.8
	 */
	public function OnAfterJGiveCampaignEdit($cid, $post, $newDetails, $oldDetails)
	{
	}

	/**
	 * Function OnAfterJGivePaymentStatusChange
	 *
	 * @param   INT     $order_id_key  Order Id Key
	 * @param   String  $status        Status
	 * @param   String  $comment       Comment
	 * @param   String  $send_mail     Send mail
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function OnAfterJGivePaymentStatusChange($order_id_key, $status, $comment, $send_mail)
	{
	}

	/**
	 * Function OnAfterJGivePaymentStatusChange
	 *
	 * @param   INT     $orderid      Order Id
	 * @param   String  $orderStatus  Order Status
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function OnAfterJGivePaymentStatusProcess($orderid, $orderStatus)
	{
	}

	/**
	 * Function OnAfterJGivePaymentSuccess
	 *
	 * @param   INT  $orderid  Order Id
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function OnAfterJGivePaymentSuccess($orderid)
	{
		// Push activity to various activity streams.
		// Set some of the data for activity.
		$path = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$donationsHelper     = new donationsHelper;
		$cid                 = $donationsHelper->getCidFromOrderId($orderid);
		$uid                 = $donationsHelper->getDonorIdFromOrderId($orderid);
		$isAnonymousDonation = $donationsHelper->isAnonymousDonation($orderid);

		// If is not anonymous donation then only push activity stream
		if (!$isAnonymousDonation)
		{
			// Set some of the data for activity
			$act_subtype     = 'payment';
			$act_description = JText::_('COM_JGIVE_ACTIVITY_DONATED') . ' ';
			$result          = $this->pushActivity($cid, $act_subtype, $act_description, $uid);

			if (!$result)
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Function pushActivity
	 *
	 * @param   INT     $cid              Id
	 * @param   String  $act_subtype      Subtype
	 * @param   String  $act_description  Description
	 * @param   INT     $uid              UID
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function pushActivity($cid, $act_subtype, $act_description, $uid = 0)
	{
		$params              = JComponentHelper::getParams('com_jgive');
		$integration_option  = $params->get('integration');
		$jgiveFrontendHelper = new jgiveFrontendHelper;

		// Set activity data to be pushed
		$user                = JFactory::getUser();

		$actor_id = $user->get('id');

		if (!$actor_id)
		{
			$actor_id = $uid;
		}

		$act_type = 'jgive';

		$singleCampaignItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all', 1);
		$act_link  = JUri::root() . substr(
		JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cid . '&Itemid=' . $singleCampaignItemid), strlen(JUri::base(true)) + 1
		);

		$path = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $path);
			JLoader::load('campaignHelper');
		}

		$campaignHelper = new campaignHelper;

		$act_title = $campaignHelper->getCampaignTitleFromCid($cid);

		if ($integration_option == 'joomla')
		{
			return true;
		}
		elseif ($integration_option == 'cb')
		{
			$result = $this->pushToCBActivity($actor_id, $act_type, $act_subtype, $act_description, $act_link, $act_title);

			if (!$result)
			{
				return false;
			}
		}
		elseif ($integration_option == 'jomsocial')
		{
			$result = $this->pushToJomsocialActivity($actor_id, $act_type, $act_subtype, $act_description, $act_link, $act_title);

			if (!$result)
			{
				return false;
			}
		}
		elseif ($integration_option == 'jomwall')
		{
			$result = $this->pushToJomwallActivity($actor_id, $act_type, $act_subtype, $act_description, $act_link, $act_title);

			if (!$result)
			{
				return false;
			}
		}
		elseif ($integration_option == 'easySocial')
		{
			$result = $this->pushToEasySocialActivity($actor_id, $act_type, $act_subtype, $act_description, $act_link, $act_title);

			if (!$result)
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Function pushToCBActivity
	 *
	 * @param   INT     $actor_id         Id
	 * @param   String  $act_type         Type
	 * @param   String  $act_subtype      Subtype
	 * @param   String  $act_description  Description
	 * @param   String  $act_link         Link
	 * @param   String  $act_title        Title
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function pushToEasySocialActivity($actor_id, $act_type = '', $act_subtype = '', $act_description = '', $act_link = '', $act_title = '')
	{
		require_once JPATH_ROOT . '/administrator/components/com_easysocial/includes/foundry.php';

		$linkHTML = '<a href="' . $act_link . '">' . $act_title . '</a>';

		if ($actor_id != 0)
		{
			$myUser = Foundry::user($actor_id);
		}

		$stream   = Foundry::stream();
		$template = $stream->getTemplate();
		$template->setActor($actor_id, SOCIAL_TYPE_USER);
		$template->setContext($actor_id, 'all');
		$template->setVerb('jgive_campaign');
		$template->setType(SOCIAL_STREAM_DISPLAY_MINI);

		if ($actor_id != 0)
		{
			$userProfileLink = '<a href="' . $myUser->getPermalink() . '">' . $myUser->getName() . '</a>';
			$title           = ($userProfileLink . " " . $act_description . "" . $linkHTML);
		}
		else
		{
			$title = ("A guest " . $act_description);
		}

		$template->setTitle($title);
		$template->setAggregate(false);

		$template->setPublicStream('core.view');
		$stream->add($template);

		return true;
	}

	/**
	 * Function pushToCBActivity
	 *
	 * @param   INT     $actor_id         Id
	 * @param   String  $act_type         Type
	 * @param   String  $act_subtype      Subtype
	 * @param   String  $act_description  Description
	 * @param   String  $act_link         Link
	 * @param   String  $act_title        Title
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function pushToCBActivity($actor_id, $act_type, $act_subtype, $act_description, $act_link, $act_title)
	{
		// Load CB framework
		global $_CB_framework, $mainframe;

		if (defined('JPATH_ADMINISTRATOR'))
		{
			if (!file_exists(JPATH_ADMINISTRATOR . '/components/com_comprofiler/plugin.foundation.php'))
			{
				echo 'CB not installed!';

				return false;
			}

			include_once JPATH_ADMINISTRATOR . '/components/com_comprofiler/plugin.foundation.php';
		}
		else
		{
			if (!file_exists($mainframe->getCfg('absolute_path') . '/administrator/components/com_comprofiler/plugin.foundation.php'))
			{
				echo 'CB not installed!';

				return false;
			}

			include_once $mainframe->getCfg('absolute_path') . '/administrator/components/com_comprofiler/plugin.foundation.php';
		}

		cbimport('cb.plugins');
		cbimport('cb.html');
		cbimport('cb.database');
		cbimport('language.front');
		cbimport('cb.snoopy');
		cbimport('cb.imgtoolbox');

		global $_CB_framework, $_CB_database, $ueConfig;

		// Load cb activity plugin class
		$cbactivity_path = JPATH_SITE . "/components/com_comprofiler/plugin/user/plug_cbactivity/cbactivity.class.php";

		if (!file_exists($cbactivity_path))
		{
			return false;
		}

		require_once JPATH_SITE . "/components/com_comprofiler/plugin/user/plug_cbactivity/cbactivity.class.php";

		// Push activity
		$linkHTML = '<a href="' . $act_link . '">' . $act_title . '</a>';

		$activity = cbactivityData::getActivity(array('id', '=', $id), null, null, false);
		$activity->set('user_id', $actor_id);
		$activity->set('type', $act_type);
		$activity->set('subtype', $act_subtype);
		$activity->set('title', $act_description . ' ' . $linkHTML);
		$activity->set('icon', 'nameplate');
		$activity->set('date', cbactivityClass::getUTCDate());
		$activity->store();

		return true;
	}

	/**
	 * Function pushToJomsocialActivity
	 *
	 * @param   INT     $actor_id         Id
	 * @param   String  $act_type         Type
	 * @param   String  $act_subtype      Subtype
	 * @param   String  $act_description  Description
	 * @param   String  $act_link         Link
	 * @param   String  $act_title        Title
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function pushToJomsocialActivity($actor_id, $act_type, $act_subtype, $act_description, $act_link, $act_title)
	{
		/*load Jomsocial core*/
		$jspath = JPATH_ROOT . '/components/com_community';

		if (file_exists($jspath))
		{
			include_once $jspath . '/libraries/core.php';
		}

		// Push activity
		$linkHTML     = '<a href="' . $act_link . '">' . $act_title . '</a>';
		$act          = new stdClass;
		$act->cmd     = 'wall.write';
		$act->actor   = $actor_id;
		$act->target  = 0;
		$act->title   = '{actor} ' . $act_description . ' ' . $linkHTML;
		$act->content = '';
		$act->app     = 'wall';
		$act->cid     = 0;
		$jspath       = JPATH_ROOT . '/components/com_community';

		if (file_exists($jspath))
		{
			CFactory::load('libraries', 'activities');
			CActivityStream::add($act);

			return true;
		}

		return false;
	}

	/**
	 * Function pushToJomwallActivity
	 *
	 * @param   INT     $actor_id         Id
	 * @param   String  $act_type         Type
	 * @param   String  $act_subtype      Subtype
	 * @param   String  $act_description  Description
	 * @param   String  $act_link         Link
	 * @param   String  $act_title        Title
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function pushToJomwallActivity($actor_id, $act_type, $act_subtype, $act_description, $act_link, $act_title)
	{
		/*load jomwall core*/
		if (!class_exists('AwdwallHelperUser'))
		{
			require_once JPATH_SITE . '/components/com_awdwall/helpers/user.php';
		}

		$linkHTML   = '<a href="' . $act_link . '">' . $act_title . '</a>';
		$comment    = $act_description . ' ' . $linkHTML;
		$attachment = $act_link;
		$type       = 'text';
		$imgpath    = null;
		$params     = array();

		AwdwallHelperUser::addtostream($comment, $attachment, $type, $actor_id, $imgpath, $params);

		return true;
	}

	/**
	 * Function to Check campaign is published.
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function checkCampaignIsPublished($cid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT c.published FROM #__jg_campaigns as c where c.id=" . $cid;
		$db->setQuery($query);
		$campaignStatus = $db->LoadResult();

		if ($campaignStatus)
		{
			return 1;
		}

		return 0;
	}
}
