<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');
jimport('joomla.filesystem.file');

// Load language file for plugin
$lang = JFactory::getLanguage();
$lang->load('plg_system_jgive_invitex_email', JPATH_ADMINISTRATOR);

// This is needed when JMA integration plugin is used on sites where JMA is not installed
$jma_integration_helper = JPATH_SITE . '/plugins/system/plg_sys_jma_integration/plg_sys_jma_integration/plugins.php';

if (!class_exists('JmaPluginHelper'))
{
	// Require_once $path;
	JLoader::register('JmaPluginHelper', $jma_integration_helper);
	JLoader::load('JmaPluginHelper');
}

/**
 * Invitex System plugin  class.
 *
 * @package  JGive
 *
 * @since    1.7
 **/
class PlgSystemjgive_Invitex_Email extends JPlugin
{
	/**
	 * Trigger on prepare invitex email
	 *
	 * @param   string  $message_body     Email Message body
	 * @param   Object  $connection_data  Inviter & invitee info
	 *
	 * @return  void
	 */
	public function onPrepareInvitexEmail($message_body, $connection_data = null)
	{
		$helperPath = JPATH_SITE . '/components/com_invitex/helper.php';

		if (!class_exists('cominvitexHelper'))
		{
			JLoader::register('cominvitexHelper', $helperPath);
			JLoader::load('cominvitexHelper');
		}

		$cominvitexHelper = new cominvitexHelper;
		$invite_type      = $cominvitexHelper->geTypeId_By_InernalName('jgive_email');

		if (!empty($invite_type) && !empty($connection_data->invite_type) && ( $invite_type == $connection_data->invite_type))
		{
			$invite_type_tag = explode("|", $connection_data->invite_type_tag);

			$campId = null;

			if (isset($invite_type_tag[1]))
			{
				$invite_type_tag = str_replace("cid=", "", $invite_type_tag[1]);
				$campId          = str_replace("]", "", $invite_type_tag);
			}

			// To get campaign id
			if (!$campId)
			{
				return false;
			}

			// Add campaign Id to plugin
			$message_body = $this->_jGiveCampaignEmail($campId, $message_body, $connection_data);
		}

		return $message_body;
	}

	/**
	 * Trigger on prepare email content
	 *
	 * @param   INT    $campId           ToDo
	 * @param   Date   $message_body     Todo
	 * @param   Array  $connection_data  User params array
	 *
	 * @return  void
	 *
	 * @since   1.0.0
	 */
	public function _jGiveCampaignEmail($campId, $message_body, $connection_data)
	{
		$areturn = array();

		// If no userid/or no guest user return blank array for html and css

		JLoader::import('campaign', JPATH_SITE . '/components/com_jgive/models');
		$JgiveModelCampaign = new JgiveModelCampaign;

		$cdata = $JgiveModelCampaign->getCampaign($campId);
		$html  = '';

		if (!empty($cdata))
		{
			// Create object for helper class
			$helper = new JmaPluginHelper;

			// Call helper function to get plugin layout
			$data                  = new stdclass;
			$data->message_body    = $message_body;
			$data->cdata           = $cdata;
			$data->connection_data = $connection_data;

			// @param should be $layout,$vars=false,$plugin_params,$plugin='',$group='emailalerts'
			$html = $helper->getLayout($this->_name, $data, '', '', 'system');
		}

		return $html;
	}

	/**
	 * Trigger on After invite added to queue
	 *
	 * @return  void
	 *
	 * @since   1.0.0
	 */
	public function onAfterinvitesqueued()
	{
		return;
	}

	/**
	 * Trigger on After invite sent
	 *
	 * @param   INT     $inviter_id    Inviter user id
	 * @param   String  $invitee_mail  Invitee mail
	 *
	 * @return  void
	 *
	 * @since   1.0.0
	 */
	public function onAfterinvitesent($inviter_id, $invitee_mail)
	{
		return;
	}
}
