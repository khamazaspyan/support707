<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  jbolo
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Load integrations helper file
$integrationsHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/integrations.php';

if (!class_exists('integrationsHelper'))
{
	JLoader::register('integrationsHelper', $integrationsHelperPath);
	JLoader::load('integrationsHelper');
}

/**
 * Class for JBolo system plugin
 *
 * @package     JBolo
 * @subpackage  jbolo
 * @since       3.1.4
 */
class PlgSystemJbolo extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @param   string  &$subject  Name
	 * @param   array   $config    Array of config options
	 *
	 * @since   3.0
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);

		/*// Load assets
		jimport('joomla.filesystem.file');
		$tjStrapperPath = JPATH_ROOT . '/media/techjoomla_strapper/tjstrapper.php';

		if (JFile::exists($tjStrapperPath))
		{
			require_once $tjStrapperPath;
			TjStrapper::loadTjAssets('com_jbolo');
		}*/

		// Get component params
		$params = JComponentHelper::getParams('com_jbolo');
		$this->community         = $params->get('community');
		$this->sync_social_inbox = $params->get('sync_social_inbox');
		$this->sync_group_chats  = $params->get('sync_group_chats');
	}

	/**
	 *  Uses this sytem trigger to catch Easysocial messages
	 *
	 * @return  boolean
	 *
	 * @since   3.1.4
	 */
	public function onAfterRoute()
	{
		// Check if JBolo module is enabled
		jimport('joomla.application.module.helper');
		$module = JModuleHelper::getModule('jbolo');

		if (!$module)
		{
			return false;
		}

		// Load JBolo assets
		jimport('joomla.filesystem.file');
		$tjStrapperPath = JPATH_ROOT . '/media/techjoomla_strapper/tjstrapper.php';

		if (JFile::exists($tjStrapperPath))
		{
			require_once $tjStrapperPath;
			TjStrapper::loadTjAssets('com_jbolo');
		}

		// If integration is not set to Easysocial, return
		if ($this->community !== 'EasySocial')
		{
			return false;
		}

		$input  = JFactory::getApplication()->input;
		$option = $input->post->get('option', '', 'STRING');

		// If url does not contain option = com_easysocial, return
		if ($option !== 'com_easysocial')
		{
			return false;
		}

		// If social message sync is on
		if ($this->sync_social_inbox)
		{
			// Get namespace
			$namespace = $input->post->get('namespace', '', 'STRING');
			$myId      = JFactory::getUser()->id;

			// Store message replies send via ajax posted from conversation read view
			if (!empty($namespace) && $namespace == 'site/controllers/conversations/reply')
			{
				// Get message id and msg
				$msgId = $input->post->get('id', '', 'INT');
				$msg   = $input->post->get('message', '', 'STRING');

				// Initialize variables.
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				// Get *active (state=1) conversation participants
				$query->select('scp.user_id')
					->from($db->quoteName('#__social_conversations_participants') . ' AS scp')
					->join('INNER', $db->quoteName('#__social_conversations') . ' AS sc ON sc.id=scp.conversation_id')
					->where($db->quoteName('scp.state') . ' =1')
					->where($db->quoteName('scp.conversation_id') . ' = ' . $msgId)
					->where($db->quoteName('scp.user_id') . ' != ' . $myId);

				// Set the query and load the result.
				$db->setQuery($query);

				try
				{
					$participants = array();
					$participants = $db->loadColumn();
				}
				catch (RuntimeException $e)
				{
					JError::raiseWarning(500, $e->getMessage());

					return false;
				}

				// If it's a group chat check if group conversations are to be synced
				if ((count($participants) >= 2) && (!$this->sync_group_chats))
				{
					// Do nothing
				}
				elseif (count($participants))
				{
					$query = $db->getQuery(true);

					// Get conversation owner
					$query->select('sc.created_by')
						->from($db->quoteName('#__social_conversations') . ' AS sc')
						->where($db->quoteName('sc.id') . ' = ' . $msgId);

					// Set the query and load the result.
					$db->setQuery($query);

					try
					{
						$owner = $db->loadResult();

						/*if ($owner != $myId)
						{

						}*/
					}
					catch (RuntimeException $e)
					{
						JError::raiseWarning(500, $e->getMessage());

						return false;
					}

					// Push message to Jbolo
					$pushMessagesToJboloResult = $this->pushMessagesToJbolo($uid = $myId, $participants, $msg, $nodeOwner = $owner);
				}
			}

			// Store messages send via ajax posted from other user's profile
			// Or sent from all user view
			if (!empty($namespace) && $namespace == 'site/controllers/conversations/store')
			{
				// Get userid and msg
				if (is_array($input->post->get('uid', '')))
				{
					$uid = $input->post->get('uid', '', 'ARRAY');

					// @TODO - this might need change in future
					$uid = $uid[0];
				}
				else
				{
					$uid = $input->post->get('uid', '', 'INT');
				}

				$msg = $input->post->get('message', '', 'STRING');

				$participants = array();
				$participants[] = $uid;

				if (count($participants))
				{
					// Push message to JBolo
					$pushMessagesToJboloResult = $this->pushMessagesToJbolo($uid = $myId, $participants, $msg, $nodeOwner = $myId);
				}
			}

			// Store new message posted from compose form
			$controller = $input->post->get('controller', '', 'STRING');
			$task       = $input->post->get('task', '', 'STRING');

			if (!empty($controller) && $controller == 'conversations' && !empty($task) && $task == 'store')
			{
				$msg          = $input->post->get('message', '', 'STRING');
				$participants = $input->post->get('uid', '', 'ARRAY');

				// If it's a group chat check if group conversations are to be synced
				if ((count($participants) >= 2) && (!$this->sync_group_chats))
				{
					// Do nothing
				}
				elseif (count($participants))
				{
					// Push message to JBolo
					$pushMessagesToJboloResult = $this->pushMessagesToJbolo($uid = $myId, $participants, $msg, $nodeOwner = $myId);
				}
			}
		}

		return true;
	}

	/**
	 * Returns the latest conversation id for given participants for a social integration
	 *
	 * @param   integer  $uid           Sender's user id
	 * @param   array    $participants  Participants array
	 * @param   string   $msg           Message
	 * @param   integer  $nodeOwner     Group owner
	 *
	 * @return  object  $pushChatToNodeResult  Object with message details
	 *
	 * @since   3.1.4
	 */
	public function pushMessagesToJbolo($uid, $participants, $msg, $nodeOwner)
	{
		// Load nodes helper file
		$nodesHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/nodes.php';

		if (!class_exists('nodesHelper'))
		{
			JLoader::register('nodesHelper', $nodesHelperPath);
			JLoader::load('nodesHelper');
		}

		// Load users helper file
		$jbolousersHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/users.php';

		if (!class_exists('jbolousersHelper'))
		{
			JLoader::register('jbolousersHelper', $jbolousersHelperPath);
			JLoader::load('jbolousersHelper');
		}

		// Load integrations helper file
		$integrationsHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/integrations.php';

		if (!class_exists('integrationsHelper'))
		{
			JLoader::register('integrationsHelper', $integrationsHelperPath);
			JLoader::load('integrationsHelper');
		}

		// Load jbolo helper file
		$chatBroadcastHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/chatBroadcast.php';

		if (!class_exists('chatBroadcastHelper'))
		{
			JLoader::register('chatBroadcastHelper', $chatBroadcastHelperPath);
			JLoader::load('chatBroadcastHelper');
		}

		// Check if node exists
		$nodesHelper = new nodesHelper;

		// If only 1 participant, check for 1to1 node
		if (count($participants) == 1)
		{
			$node_id_found = $nodesHelper->checkNodeExists($uid, $participants[0]);
		}
		// If more than 1 participants, check for group chat node
		elseif (count($participants) > 1)
		{
			$node_id_found = $nodesHelper->checkGroupChatNodeExists($uid, $nodeOwner, $participants);
		}

		// If no node found, create one node
		if ($node_id_found == 0)
		{
			// If only 1 participant, create 1to1 node
			if (count($participants) == 1)
			{
				$ini_node = $nodesHelper->initiateNode($uid, $participants);
			}
			// If more than 1 participants, create group chat node
			elseif (count($participants) > 1)
			{
				$ini_node = $nodesHelper->initiateGroupNode($nodeOwner, $participants);
			}

			if (isset($ini_node['nodeinfo']))
			{
				$node_id_found = $ini_node['nodeinfo']->nid;
			}
		}

		// Push message to node
		if ($node_id_found)
		{
			// Add logged in user as participant
			$participants[] = $uid;

			// Push chat to jbolo node
			$pushChatToNodeResult = $nodesHelper->pushChatToNode($uid, $participants, $node_id_found, $msg);

			return $pushChatToNodeResult;
		}

		return false;
	}
}
