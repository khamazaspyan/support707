<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive_Activities
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
defined('_JEXEC') or die('Direct Access to this location is not allowed.');

jimport('joomla.plugin.plugin');
jimport('joomla.application.component.model');
jimport('techjoomla.jsocial.jsocial');

$lang = JFactory::getLanguage();
$lang->load('plg_activitystream_jgiveactivities', JPATH_ADMINISTRATOR);

/**
 * jgive_activities
 *
 * @package     Jgive_Activities
 * @subpackage  site
 * @since       1.0
 */
class PlgSystemJgiveActivitiesHelper
{
	/**
	 * Constructor
	 *
	 */
	public function __construct()
	{
		// Load activity component models
		JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_activitystream/models');

		// Load activity component models
		JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_activitystream/models');

		// Load activity component tables
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_activitystream/tables');
	}

	/**
	 * Method to get actor data
	 *
	 * @param   OBJECT  $user  user object
	 *
	 * @return  null
	 *
	 * @since   1.0
	 */
	public function getActorData($user)
	{
		$userData = array();
		$userData['type'] = 'person';
		$userData['id'] = $user->id;
		$userData['name'] = $user->get('name');
		$params = JComponentHelper::getParams('com_jgive');
		$integration = $params->get('integration');
		$avatarsetting = $params->get('gravatar');

		switch (strtolower($integration))
		{
			case 'jomsocial':
				jimport('techjoomla.jsocial.jomsocial');
				$sociallibraryclass = new JSocialJomsocial;
				break;
			case 'easysocial':
				jimport('techjoomla.jsocial.easysocial');
				$sociallibraryclass = new JSocialEasysocial;
				break;
			case 'joomla':
				jimport('techjoomla.jsocial.joomla');
				$sociallibraryclass = new JSocialJoomla;
				break;
			case 'cb':
				jimport('techjoomla.jsocial.cb');
				$sociallibraryclass = new JSocialCB;
				break;
			case 'jomwall':
				jimport('techjoomla.jsocial.jomwall');
				$sociallibraryclass = new JSocialJomwall;
				break;
			default:
				jimport('techjoomla.jsocial.joomla');
				$sociallibraryclass = new JSocialJoomla;
				break;
		}

		/*Added By Deepa*/
		$helperPath = JPATH_SITE . '/components/com_jgive/helpers/integrations.php';

		if (!class_exists('JgiveIntegrationsHelper'))
		{
			JLoader::register('JgiveIntegrationsHelper', $helperPath);
			JLoader::load('JgiveIntegrationsHelper');
		}

		$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
		$user->avatar      = $JgiveIntegrationsHelper->getUserAvatar($user->id);
		/*End here*/

		$userData['url'] = $sociallibraryclass->getProfileUrl($user);
		$imageData = array();
		$imageData['type'] = "link";

		/* Commented by Deepa*/
		if (!$user->avatar)
		{
			// If no avatar, use default avatar
			$user->avatar = JUri::root(true) . '/media/com_jgive/images/default_avatar.png';
		}

		$imageData['avatar'] = $user->avatar;
		$userData['image'] = json_encode($imageData);

		return $userData;
	}

	/**
	 * Method to add avtivity for adding images to the campaign
	 *
	 * @param   ARRAY  $newAddedImages  array of newly added images
	 *
	 * @param   INT    $cid             campaign id
	 *
	 * @return  boolean
	 *
	 * @since   1.0
	 */
	public function addImageActivity($newAddedImages, $cid)
	{
		if (!empty($newAddedImages))
		{
			foreach ($newAddedImages as $k => $newAddedImage)
			{
				$newAddedImages[$k] = JUri::root() . $newAddedImage;
			}

			$user = JFactory::getUser();
			$data = array();
			$data['id'] = '';
			$actorData = $this->getActorData($user);
			$data['actor'] = json_encode($actorData);

			// Get campaign-object data
			$objectData = array();
			$objectData['type'] = 'image';
			$objectData['url'] = json_encode($newAddedImages);
			$objectData['count'] = count($newAddedImages);
			$data['object'] = json_encode($objectData);

			$data['actor_id'] = $user->id;
			$data['object_id'] = "image";
			$data['type'] = 'campaign.addimage';
			$data['template'] = 'image.mustache';

			// Load component models
			JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_jgive/models');

			$jgiveCampaignModel = JModelLegacy::getInstance('Campaign', 'JgiveModel');
			$campaignData = $jgiveCampaignModel->getCampaignDetails($cid);

			$data['target_id'] = $campaignData->id;
			$targetData = array();
			$targetData['id'] = $campaignData->id;
			$targetData['name'] = $campaignData->title;
			$targetData['url'] = JRoute::_(JUri::root() . 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $campaignData->id);

			$targetData['type'] = 'campaign';
			$data['target'] = json_encode($targetData);
			$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');
			$result = $activityStreamModelActivity->save($data);

			return $result;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Method to add avtivity for adding video to the campaign
	 *
	 * @param   ARRAY  $newAddedVideos  array of newly added videos
	 *
	 * @param   INT    $cid             campaign id
	 *
	 * @return  boolean
	 *
	 * @since   1.0
	 */
	public function addVideoActivity($newAddedVideos, $cid)
	{
		$path = JPATH_SITE . '/components/com_jgive/helpers/media.php';

		if (!class_exists('JgiveMediaHelper'))
		{
			JLoader::register('JgiveMediaHelper', $path);
			JLoader::load('JgiveMediaHelper');
		}

		$JgiveMediaHelper = new JgiveMediaHelper;

		if (!empty($newAddedVideos))
		{
			// Load component models
			JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_jgive/models');
			$jgiveCampaignModel = JModelLegacy::getInstance('Campaign', 'JgiveModel');

			$link = array();
			$thumbSrc = array();
			$playIcon = array();
			$VideoActivityData = array();

			foreach ($newAddedVideos as $k => $newAddedVideo)
			{
				$videoDetails = array();

				if (strpos($newAddedVideo, 'media/com_jgive') !== false)
				{
					$videoData = $jgiveCampaignModel->getCampaignVideoId($newAddedVideo, 'path');
					$videoId = $videoData['id'];
					$videoType = $videoData['type'];
					$videoDetails['thumbSrc'] = JUri::root(true) . '/media/com_jgive/images/no_thumb.png';
					$videoDetails['url'] = JUri::root() . substr($newAddedVideo, 1);
				}
				else
				{
					$videoData = $jgiveCampaignModel->getCampaignVideoId($newAddedVideo, 'url');
					$videoId = $videoData['id'];
					$videoType = $videoData['type'];

					$videoIdThumb = $JgiveMediaHelper->videoId($videoType, $newAddedVideo);
					$videoDetails['thumbSrc'] = $JgiveMediaHelper->videoThumbnail($videoType, $videoIdThumb);
					$videoDetails['url'] = $newAddedVideo;
				}

				$videoDetails['link'] = JRoute::_(
					JUri::root() . "index.php?option=com_jgive&view=campaign&layout=single_playvideo&vid="
					. $videoId . "&type=" . trim($videoType) . "&tmpl=component"
					);

				$videoDetails['playIcon'] = JUri::root(true) . '/media/com_jgive/images/play_icon.png';

				$VideoActivityData[] = $videoDetails;
			}

			$user = JFactory::getUser();
			$data = array();
			$data['id'] = '';
			$actorData = $this->getActorData($user);
			$data['actor'] = json_encode($actorData);

			$link = JRoute::_(
			JUri::root() . "index.php?option=com_jgive&view=campaign&layout=single_playvideo&vid=" . $videoId . "&type=" . trim($videoType) . "&tmpl=component"
			);
			$thumbSrc = JUri::root(true) . '/media/com_jgive/images/no_thumb.png';
			$playIcon = JUri::root(true) . '/media/com_jgive/images/play_icon.png';

			// Get campaign-object data
			$objectData = array();
			$objectData['videos'] = json_encode($VideoActivityData);
			$objectData['count'] = count($VideoActivityData);

			$data['object'] = json_encode($objectData);
			$data['actor_id'] = $user->id;
			$data['object_id'] = "video";
			$data['type'] = 'campaign.addvideo';
			$data['template'] = 'video.mustache';

			$campaignData = $jgiveCampaignModel->getCampaignDetails($cid);

			$data['target_id'] = $campaignData->id;
			$targetData = array();
			$targetData['id'] = $campaignData->id;
			$targetData['name'] = $campaignData->title;

			$targetData['url'] = JRoute::_(JUri::root() . '/index.php?option=com_jgive&view=campaign&layout=single&cid=' . $campaignData->id);

			$targetData['type'] = 'campaign';
			$data['target'] = json_encode($targetData);
			$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');
			$result = $activityStreamModelActivity->save($data);

			return $result;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Method to add avtivity for adding give back to the campaign
	 *
	 * @param   ARRAY  $newAddedGivebacks  array of newly added give backs
	 *
	 * @param   INT    $cid                campaign id
	 *
	 * @return  boolean
	 *
	 * @since   1.0
	 */
	public function addGivebackActivity($newAddedGivebacks, $cid)
	{
		if (!empty($newAddedGivebacks))
		{
			foreach ($newAddedGivebacks as $k => $newAddedGiveback)
			{
				if (!empty($newAddedGiveback))
				{
					if (!empty($newAddedGiveback['giveback_image']))
					{
						$newAddedGivebacks[$k]['giveback_image'] = JUri::root() . $newAddedGiveback['giveback_image'];
					}
					else
					{
						$newAddedGivebacks[$k]['giveback_image'] = '';
					}
				}
			}

			$user = JFactory::getUser();
			$data = array();
			$data['id'] = '';
			$actorData = $this->getActorData($user);
			$data['actor'] = json_encode($actorData);

			// Get campaign-object data
			$objectData = array();
			$objectData['type'] = 'giveback';
			$objectData['url'] = json_encode($newAddedGivebacks);
			$objectData['count'] = count($newAddedGivebacks);
			$data['object'] = json_encode($objectData);

			$data['actor_id'] = $user->id;
			$data['object_id'] = "giveback";
			$data['type'] = 'campaign.addgiveback';
			$data['template'] = 'giveback.mustache';

			// Load component models
			JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_jgive/models');

			$jgiveCampaignModel = JModelLegacy::getInstance('Campaign', 'JgiveModel');
			$campaignData = $jgiveCampaignModel->getCampaignDetails($cid);

			$data['target_id'] = $campaignData->id;
			$targetData = array();
			$targetData['id'] = $campaignData->id;
			$targetData['name'] = $campaignData->title;

			$targetData['url'] = JRoute::_(JUri::root() . 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $campaignData->id);

			$targetData['type'] = 'campaign';
			$data['target'] = json_encode($targetData);
			$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');
			$result = $activityStreamModelActivity->save($data);

			return $result;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Method to add avtivity for campaign date extension
	 *
	 * @param   OBJECT  $newDetails  campaign details
	 *
	 * @param   OBJECT  $oldDetails  campaign details
	 *
	 * @param   INT     $cid         campaign id
	 *
	 * @return  boolean
	 *
	 * @since   1.0
	 */
	public function endDateChangeActivity($newDetails, $oldDetails, $cid)
	{
		if (!empty($newDetails->end_date) && !empty($oldDetails->end_date))
		{
			$date_diff = date_diff(date_create($oldDetails->end_date), date_create($newDetails->end_date));

			if ($date_diff->days > 0)
			{
				$user = JFactory::getUser();
				$data = array();
				$data['id'] = '';
				$actorData = $this->getActorData($user);
				$data['actor_id'] = $user->id;
				$data['actor'] = json_encode($actorData);

				// Load component models
				JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_jgive/models');

				$jgiveCampaignModel = JModelLegacy::getInstance('Campaign', 'JgiveModel');
				$campaignData = $jgiveCampaignModel->getCampaignDetails($cid);

				// Get date difference in
				$dateDiff = (strtotime($newDetails->end_date) - strtotime($oldDetails->end_date));

				// Get campaign-object data
				$objectData = array();
				$objectData['type'] = 'campaign';

				$objectData['newenddate'] = date("d M Y", strtotime($newDetails->end_date));

				$baseUrl = "index.php?option=com_jgive&view=campaign&layout=single&cid=";
				$objectData['url'] = JUri::root() . substr(
				JRoute::_($baseUrl . $cdata['campaign']->id . '&Itemid=' . $this->singleCampaignItemid), strlen(JUri::base(true)) + 1
				);
				$data['object'] = json_encode($objectData);
				$data['object_id'] = $campaignData->id;

				$targetData = array();
				$targetData['type'] = 'campaign';
				$targetData['name'] = $campaignData->title;
				$targetData['id'] = $campaignData->id;
				$targetData['url'] = JRoute::_(JUri::root() . 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $campaignData->id);
				$data['target'] = json_encode($targetData);
				$data['target_id'] = $campaignData->id;

				$data['type'] = 'campaign.extended';

				// If campaign end date is extended then use extended template else use datechange template
				if ($date_diff->invert == 0)
				{
					$data['template'] = 'extended.mustache';
				}
				else
				{
					$data['template'] = 'datechange.mustache';
				}

				$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');
				$result = $activityStreamModelActivity->save($data);

				return $result;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * Method to add avtivity for campaign complition
	 *
	 * @param   ARRAY  $donationDetails  donation details
	 *
	 * @return  boolean
	 *
	 * @since   1.0
	 */
	public function addCampaignCompleteActivity($donationDetails)
	{
		if ($donationDetails['campaign']->remaining_amount <= 0)
		{
			if ($donationDetails['campaign']->donation_amount >= abs($donationDetails['campaign']->remaining_amount))
			{
				$user = JFactory::getUser($donationDetails['donor']->user_id);
				$data = array();

				$data['id'] = '';
				$actorData = $this->getActorData($user);
				$data['actor'] = json_encode($actorData);
				$data['actor_id'] = $user->id;

				$data['object_id'] = $donationDetails['campaign']->id;
				$objectData = array();
				$objectData['id'] = $donationDetails['campaign']->id;
				$objectData['name'] = $donationDetails['campaign']->title;
				$objectData['url'] = JUri::root() . '/index.php?option=com_jgive&view=campaign&layout=single&cid=' . $donationDetails['campaign']->id;
				$objectData['type'] = 'campaign';
				$data['object'] = json_encode($objectData);
				$data['type'] = 'campaign.completed';
				$data['template'] = 'completed.mustache';

				$targetData = array();
				$targetData['type'] = 'campaign';
				$targetData['name'] = $donationDetails['campaign']->title;
				$targetData['id'] = $donationDetails['campaign']->id;
				$targetData['url'] = JRoute::_(JUri::root() . 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $donationDetails['campaign']->id);
				$data['target'] = json_encode($targetData);
				$data['target_id'] = $donationDetails['campaign']->id;

				$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');

				$result = $activityStreamModelActivity->save($data);

				return $result;
			}
		}

		return false;
	}

	/**
	 * Method to add avtivity for campaign donation
	 *
	 * @param   ARRAY  $donationDetails  donation details319
	 *
	 * @return  boolean
	 *
	 * @since   1.0
	 */
	public function addDonationActivity($donationDetails)
	{
		$path = JPATH_SITE . '/components/com_jgive/helper.php';

		if (!class_exists('JgiveFrontendHelper'))
		{
			JLoader::register('JgiveFrontendHelper', $path);
			JLoader::load('JgiveFrontendHelper');
		}

		$jGiveFrontendHelper = new JgiveFrontendHelper;

		$user = JFactory::getUser($donationDetails['donor']->user_id);
		$activityData = array();
		$activityData['id'] = '';

		if ($donationDetails['payment']->annonymous_donation == 1)
		{
			// For annonymous donation
			$imageData = array();
			$imageData['type'] = "link";
			$imageData['avatar'] = JUri::root(true) . '/media/com_jgive/images/default_avatar.png';
			$activityData['actor'] = json_encode(array('image' => json_encode($imageData)));
			$activityData['actor_id'] = $user->id;
		}
		else
		{
			$actorData = $this->getActorData($user);
			$activityData['actor'] = json_encode($actorData);
			$activityData['actor_id'] = $user->id;
		}

		$objectData = array();
		$objectData['type'] = $donationDetails['campaign']->type;

		$objectData['amount'] = str_replace("&nbsp;", "", strip_tags($jGiveFrontendHelper->getFormattedPrice($donationDetails['payment']->amount)));
		$activityData['object'] = json_encode($objectData);
		$activityData['object_id'] = 'donation';

		// Get campaign-target data
		$targetData = array();
		$targetData['id'] = $donationDetails['campaign']->id;
		$targetData['type'] = 'campaign';
		$targetData['url'] = JUri::root() . 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $donationDetails['campaign']->id;
		$targetData['name'] = $donationDetails['campaign']->title;
		$activityData['target'] = json_encode($targetData);
		$activityData['target_id'] = $donationDetails['campaign']->id;

		$activityData['type'] = 'jgive.donation';

		// For annonymous donation
		if ($objectData['type'] == 'donation')
		{
			if ($donationDetails['payment']->annonymous_donation == 1)
			{
				$activityData['template'] = 'anonymousdonation.mustache';
			}
			else
			{
				$activityData['template'] = 'donation.mustache';
			}
		}
		elseif ($objectData['type'] == 'investment')
		{
			if ($donationDetails['payment']->annonymous_donation == 1)
			{
				$activityData['template'] = 'anonymousinvestment.mustache';
			}
			else
			{
				$activityData['template'] = 'investment.mustache';
			}
		}

		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');
		$result = $activityStreamModelActivity->save($activityData);

		return $result;
	}

	/**
	 * Function to add activity for new campaign added
	 *
	 * @param   INT     $cid   Campaign Id
	 * @param   String  $post  Post
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function addCampaignActivity($cid, $post)
	{
		$user = JFactory::getUser();
		$activityData = array();
		$activityData['id'] = '';
		$actorData = $this->getActorData($user);
		$activityData['actor'] = json_encode($actorData);
		$activityData['actor_id'] = $user->id;

		$objectData = array();
		$objectData['type'] = 'campaign';
		$objectData['name'] = $post->get('title', '', 'STRING');
		$objectData['id'] = $cid;
		$objectData['url'] = JRoute::_(JUri::root() . '/index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cid);
		$activityData['object'] = json_encode($objectData);
		$activityData['object_id'] = $cid;

		$targetData = array();
		$targetData['type'] = 'campaign';
		$targetData['name'] = $post->get('title', '', 'STRING');
		$targetData['id'] = $cid;
		$targetData['url'] = JRoute::_(JUri::root() . 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cid);
		$activityData['target'] = json_encode($targetData);
		$activityData['target_id'] = $cid;

		$activityData['type'] = 'jgive.addcampaign';
		$activityData['template'] = 'addcampaign.mustache';
		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');
		$result = $activityStreamModelActivity->save($activityData);

		return $result;
	}

	/**
	 * Function to Check campaign is published.
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function checkCampaignIsPublished($cid)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->quoteName('published'));
		$query->from($db->quoteName('#__jg_campaigns'));
		$query->where($db->quoteName('id') . ' = ' . $cid);
		$db->setQuery($query);
		$campaignStatus = $db->LoadResult();

		if ($campaignStatus)
		{
			return 1;
		}

		return 0;
	}

	/**
	 * Function postActivity
	 *
	 * @param   MIXED  $data  data
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function postActivity($data)
	{
		$user = JFactory::getUser();

		if (empty($user->id))
		{
			$result = array();

			$result['error'] = JText::_('COM_JGIVE_TEXT_ACTIVITY_POST_GUEST_ERROR_MSG');

			return $result;
		}

		$activityData = array();
		$activityData['id'] = '';
		$actorData = $this->getActorData($user);
		$activityData['actor'] = json_encode($actorData);
		$activityData['actor_id'] = $user->id;

		$cid = $data['cid'];

		// Load component models
		JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_jgive/models');

		$jgiveCampaignModel = JModelLegacy::getInstance('Campaign', 'JgiveModel');
		$campaignData = $jgiveCampaignModel->getCampaignDetails($cid);

		$objectData = array();
		$objectData['type'] = 'text';
		$objectData['postData'] = $data['postData'];
		$activityData['object'] = json_encode($objectData);
		$activityData['object_id'] = 'text';

		$targetData = array();
		$targetData['id'] = $campaignData->id;
		$targetData['name'] = $campaignData->title;

		$targetData['url'] = JRoute::_(JUri::root() . 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $campaignData->id);

		$targetData['type'] = 'campaign';
		$activityData['target'] = json_encode($targetData);
		$activityData['target_id'] = $cid;

		$activityData['type'] = 'jgive.textpost';
		$activityData['template'] = 'textpost.mustache';

		if ($data['cdate'])
		{
			$activityData['created_date'] = $data['cdate'];
		}

		if ($data['mdate'])
		{
			$activityData['updated_date'] = $data['mdate'];
		}

		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');
		$result = $activityStreamModelActivity->save($activityData);

		return $result;
	}

	/**
	 * Function to get deleted images
	 *
	 * @param   String  $newDetails  New Details
	 * @param   String  $oldDetails  Old Details
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function removeImageActivity($newDetails, $oldDetails)
	{
		foreach ($oldDetails->images as $j => $img)
		{
			$imageIsRemoved = 1;

			foreach ($newDetails->images as $k => $image)
			{
				if ($oldDetails->images[$j]->path == $newDetails->images[$k]->path)
				{
					$imageIsRemoved = 0;
					break;
				}
				else
				{
					continue;
				}
			}

			if ($imageIsRemoved)
			{
				$imagePath = $oldDetails->images[$j]->path;

				$this->removeImgActivity($imagePath, $newDetails->id, 'campaign.addimage');
			}
		}
	}

	/**
	 * Function to delete/update activities related to deleted images
	 *
	 * @param   String  $deletedImagePath  path of deleted image
	 * @param   String  $cid               campaign Id
	 * @param   String  $type              activity type
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function removeImgActivity($deletedImagePath, $cid, $type)
	{
		$deletedImagePath = str_replace('images/jGive/', '', $deletedImagePath);

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__tj_activities'));
		$query->where($db->quoteName('target_id') . ' = ' . $cid);
		$query->where($db->quoteName('type') . " = '" . $type . "'");

		$db->setQuery($query);
		$activities = $db->loadAssocList();

		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');

		if (!empty($activities))
		{
			foreach ($activities as $activity)
			{
				$objectData = json_decode($activity['object']);

				$images = json_decode($objectData->url);

				foreach ($images as $k => $image)
				{
					if ($objectData->count == '1')
					{
						if (strpos($image, $deletedImagePath) !== false)
						{
							$activityStreamModelActivity->delete($activity['id']);
						}
					}
					else
					{
						if (strpos($image, $deletedImagePath) !== false)
						{
							unset($images[$k]);
							$activityImages = array();

							foreach ($images as $img)
							{
								$activityImages[] = $img;
							}

							$objectData->url = json_encode($activityImages);
							$objectData->count -= 1;
							$activity['object'] = json_encode($objectData);
							$activityStreamModelActivity->save($activity);
						}
					}
				}
			}
		}
	}

	/**
	 * Function to delete/update activities related to deleted videos
	 *
	 * @param   String  $videoId  video Id
	 * @param   String  $type     video type
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function removeVidActivity($videoId, $type)
	{
		if ($type == 'video')
		{
			$col = 'path';
		}
		else
		{
			$col = 'url';
		}

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->quoteName(array($col, 'content_id')));
		$query->from($db->quoteName('#__jg_campaigns_media'));
		$query->where($db->quoteName('id') . ' = ' . $videoId);
		$db->setQuery($query);
		$mediaData = $db->loadAssoc();

		if (!empty($mediaData['content_id']))
		{
			$cid = $mediaData['content_id'];
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from($db->quoteName('#__tj_activities'));
			$query->where($db->quoteName('target_id') . ' = ' . $cid);
			$query->where($db->quoteName('type') . " = 'campaign.addvideo'");

			$db->setQuery($query);
			$activities = $db->loadAssocList();
		}

		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');

		if (!empty($activities))
		{
			foreach ($activities as $activity)
			{
				$objectData = json_decode($activity['object']);

				$videos = json_decode($objectData->videos);

				foreach ($videos as $k => $video)
				{
					if ($objectData->count == '1')
					{
						if (strpos($video->url, $mediaData[$col]) !== false)
						{
							$activityStreamModelActivity->delete($activity['id']);
						}
					}
					else
					{
						if (strpos($video->url, $mediaData[$col]) !== false)
						{
							unset($videos[$k]);

							$activityVideos = array();

							foreach ($videos as $vid)
							{
								$activityVideos[] = $vid;
							}

							$objectData->videos = json_encode($activityVideos);
							$objectData->count -= 1;
							$activity['object'] = json_encode($objectData);
							$activityStreamModelActivity->save($activity);
						}
					}
				}
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Function to get deleted givebacks
	 *
	 * @param   String  $newDetails  New Details
	 * @param   String  $oldDetails  Old Details
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function removeGiveBackActivity($newDetails, $oldDetails)
	{
		foreach ($oldDetails->givebacks as $j => $img)
		{
			$givebackIsRemoved = 1;

			foreach ($newDetails->givebacks as $k => $image)
			{
				if ($oldDetails->givebacks[$j]->id == $newDetails->givebacks[$k]->id)
				{
					$givebackIsRemoved = 0;
					break;
				}
				else
				{
					continue;
				}
			}

			if ($givebackIsRemoved)
			{
				$givebackId = $oldDetails->givebacks[$j]->id;

				$this->removeGiveAwayActivity($givebackId, $newDetails->id, 'campaign.addgiveback');
			}
		}
	}

	/**
	 * Function to delete/update activities related to giveback
	 *
	 * @param   String  $deletedGivebackId  path of deleted image
	 * @param   String  $cid                campaign Id
	 * @param   String  $type               activity type
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function removeGiveAwayActivity($deletedGivebackId, $cid, $type)
	{
		$deletedGivebackId = str_replace('images/jGive/', '', $deletedGivebackId);

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__tj_activities'));
		$query->where($db->quoteName('target_id') . ' = ' . $cid);
		$query->where($db->quoteName('type') . " = '" . $type . "'");

		$db->setQuery($query);
		$activities = $db->loadAssocList();

		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');

		if (!empty($activities))
		{
			foreach ($activities as $activity)
			{
				$objectData = json_decode($activity['object']);

				$images = json_decode($objectData->url);

				foreach ($images as $k => $image)
				{
					if ($objectData->count == '1')
					{
						if (strpos($image->giveback_id, $deletedGivebackId) !== false)
						{
							$activityStreamModelActivity->delete($activity['id']);
						}
					}
					else
					{
						if (strpos($image->giveback_id, $deletedGivebackId) !== false)
						{
							unset($images[$k]);
							$activityImages = array();

							foreach ($images as $img)
							{
								$activityImages[] = $img;
							}

							$objectData->url = json_encode($activityImages);
							$objectData->count -= 1;
							$activity['object'] = json_encode($objectData);
							$activityStreamModelActivity->save($activity);
						}
					}
				}
			}
		}
	}
}
