<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive_Activities
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
defined('_JEXEC') or die('Direct Access to this location is not allowed.');

jimport('joomla.plugin.plugin');
jimport('joomla.application.component.model');
jimport('techjoomla.jsocial');

$lang = JFactory::getLanguage();
$lang->load('plg_activitystream_jgiveactivities', JPATH_ADMINISTRATOR);

/**
 * jgive_activities
 *
 * @package     Jgive_Activities
 * @subpackage  site
 * @since       1.0
 */
class PlgSystemJgiveActivities extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @param   string  &$subject  subject
	 *
	 * @param   string  $config    config
	 */
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);

		require_once dirname(__FILE__) . '/helper.php';

		$this->plgSystemJgiveActivitiesHelper = new PlgSystemJgiveActivitiesHelper;
	}

	/**
	 * Method to include required scripts for activity streams
	 *
	 * @param   string  $theme  Theme used
	 *
	 * @return  null
	 *
	 * @since   1.0
	 */
	public function getActivityScript($theme)
	{
		$document = JFactory::getDocument();
		$document->addScriptDeclaration('var root_url = \'' . JUri::base() . '\'');
		$document->addScript(JUri::root() . '/media/com_activitystream/scripts/mustache.min.js');
		$document->addScript(JUri::root() . '/media/com_activitystream/scripts/activities.jQuery.js');
		$document->addStyleSheet(JUri::root() . '/media/com_activitystream/themes/' . $theme . '/css/theme.css');
	}

	/**
	 * Method to add activity streams for changes done in campaign
	 *
	 * @param   INT     $orderId   order id
	 * @param   STRING  $status    order status
	 * @param   STRING  $comment   order status change comment
	 * @param   INT     $sendMail  send mail flag
	 *
	 * @return  null
	 *
	 * @since   1.0
	 */
	public function OnAfterJGivePaymentStatusChange($orderId, $status, $comment, $sendMail)
	{
		if ($status == 'C')
		{
			$path = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

			if (!class_exists('donationsHelper'))
			{
				JLoader::register('donationsHelper', $path);
				JLoader::load('donationsHelper');
			}

			$donationsHelper  = new donationsHelper;
			$donationDetails = $donationsHelper->getSingleDonationInfo($orderId);

			// Activity for campaign complition - start
			if (($donationDetails['campaign']->amount_received >= $donationDetails['campaign']->goal_amount))
			{
				$result = $this->plgSystemJgiveActivitiesHelper->addCampaignCompleteActivity($donationDetails);
			}
			// Activity for campaign complition - end

			// Activity for donation done in campaign - start

			$result = $this->plgSystemJgiveActivitiesHelper->addDonationActivity($donationDetails);

			// Activity for donation done in campaign - end

			return $result;
		}

		return false;
	}

	/**
	 * Function OnAfterJGiveCampaignEdit
	 *
	 * @param   INT     $cid         Campaign Id
	 * @param   String  $post        Post
	 * @param   String  $newDetails  New Details
	 * @param   String  $oldDetails  Old Details
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function OnAfterJGiveCampaignEdit($cid, $post, $newDetails, $oldDetails)
	{
		// Activity for adding images to the campaign - start
		$newAddedImages = array();
		$newMainImages = array();

		$result = false;

		foreach ($newDetails->images as $j => $img)
		{
			$imageIsNew = 1;

			foreach ($oldDetails->images as $k => $image)
			{
				if ($newDetails->images[$j]->path == $oldDetails->images[$k]->path)
				{
					$imageIsNew = 0;
					break;
				}
				else
				{
					continue;
				}
			}

			if ($imageIsNew)
			{
				$galleryImage = $newDetails->images[$j]->gallery;

				// If image is not gallery image i.e the image is main image then use medium size image
				if (!$galleryImage)
				{
					$imagePath = str_replace('images/jGive/', 'images/jGive/M_', $newDetails->images[$j]->path);
					$newMainImages[] = $imagePath;
				}
				else
				{
					$newAddedImages[] = $newDetails->images[$j]->path;
				}
			}
		}

		// Activity for gallery images
		if (!empty($newAddedImages))
		{
			$result = $this->plgSystemJgiveActivitiesHelper->addImageActivity($newAddedImages, $cid);
		}

		// Activity for main images
		if (!empty($newMainImages))
		{
			$result = $this->plgSystemJgiveActivitiesHelper->addImageActivity($newMainImages, $cid);
		}

		// Remove/update activites related to the deleted images
		$this->plgSystemJgiveActivitiesHelper->removeImageActivity($newDetails, $oldDetails);

		// Activity for adding images to the campaign - end

		// Activity for adding video to the campaign - start
		$newAddedVideos = array();

		// For uploaded video
		foreach ($newDetails->video as $j => $vid)
		{
			$videoIsNew = 1;

			foreach ($oldDetails->video as $k => $video)
			{
				if ($newDetails->video[$j]->path == $oldDetails->video[$k]->path)
				{
					$videoIsNew = 0;

					break;
				}
				else
				{
					continue;
				}
			}

			if (!empty($videoIsNew))
			{
				$newAddedVideos[] = $newDetails->video[$j]->path;
			}
		}

		// For external link
		foreach ($newDetails->video as $j => $vid)
		{
			$videoIsNew = 1;

			foreach ($oldDetails->video as $k => $video)
			{
				if ($newDetails->video[$j]->url == $oldDetails->video[$k]->url)
				{
					$videoIsNew = 0;

					break;
				}
				else
				{
					continue;
				}
			}

			if (!empty($videoIsNew))
			{
				$newAddedVideos[] = $newDetails->video[$j]->url;
			}
		}

		$newAddedVideos = array_filter($newAddedVideos);

		if (!empty($newAddedVideos))
		{
			$result = $this->plgSystemJgiveActivitiesHelper->addVideoActivity($newAddedVideos, $cid);
		}

		// Activity for adding video to the campaign - end

		// Activity for adding give away - start
		$newAddedGivebacks = array();

		foreach ($newDetails->givebacks as $j => $img)
		{
			$givebackIsNew = 1;

			foreach ($oldDetails->givebacks as $k => $givebacks)
			{
				if ($newDetails->givebacks[$j]->id == $oldDetails->givebacks[$k]->id)
				{
					$givebackIsNew = 0;

					break;
				}
				else
				{
					continue;
				}
			}

			if ($givebackIsNew)
			{
				$givebackDetails = array();
				$givebackDetails['giveback_image'] = $newDetails->givebacks[$j]->image_path;
				$givebackDetails['giveback_id'] = $newDetails->givebacks[$j]->id;
				$newAddedGivebacks[] = $givebackDetails;
			}
		}

		if (!empty($newAddedGivebacks))
		{
			$result = $this->plgSystemJgiveActivitiesHelper->addGivebackActivity($newAddedGivebacks, $cid);
		}

		// Remove/update activites related to the deleted images
		$this->plgSystemJgiveActivitiesHelper->removeGiveBackActivity($newDetails, $oldDetails);

		// Activity for adding give away - end

		// Activity for extending campaign date or end date changed- start
		if (!empty($newDetails->end_date) && !empty($oldDetails->end_date))
		{
			if ($newDetails->end_date != $oldDetails->end_date)
			{
				$result = $this->plgSystemJgiveActivitiesHelper->endDateChangeActivity($newDetails, $oldDetails, $cid);
			}
		}

		// Activity for extending campaign date - end

		return $result;
	}

	/**
	 * Function OnAfterJGiveCampaignSave
	 *
	 * @param   INT     $cid   Campaign Id
	 * @param   String  $post  Post
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function OnAfterJGiveCampaignSave($cid, $post)
	{
		if (!empty($cid))
		{
			$result = $this->plgSystemJgiveActivitiesHelper->addCampaignActivity($cid, $post);

			return $result;
		}
	}

	/**
	 * Function postActivity
	 *
	 * @param   MIXED  $data  data
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function postActivity($data)
	{
		if (!empty($data))
		{
			$result = $this->plgSystemJgiveActivitiesHelper->postActivity($data);

			return $result;
		}
	}

	/**
	 * Function onAfterCampaignVideoDelete
	 *
	 * @param   INT     $videoId  video Id
	 * @param   String  $type     video type
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function onAfterCampaignVideoDelete($videoId, $type)
	{
		if (!empty($videoId))
		{
			$result = $this->plgSystemJgiveActivitiesHelper->removeVidActivity($videoId, $type);

			return $result;
		}
	}
}
