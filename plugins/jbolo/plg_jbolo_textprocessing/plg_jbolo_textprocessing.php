<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  plg_jbolo_textprocessing
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.registry.registry');
jimport('joomla.filesystem.file');
jimport('joomla.html.parameter');

if (! JFile::exists(JPATH_ROOT . '/components/com_jbolo/jbolo.php'))
{
	return false;
}

// Load com_jbolo frontend language file
$lang = JFactory::getLanguage();
$lang->load('com_jbolo', JPATH_SITE);

/**
 * Class for JBolo text processing plugin
 *
 * @package     JBolo
 * @subpackage  plg_jbolo_textprocessing
 * @since       3.1.4
 */
class PlgJboloPlg_Jbolo_Textprocessing extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @param   string  &$subject  Name
	 * @param   array   $config    Array of config options
	 *
	 * @since   3.0
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);

		// Get component params
		$params = JComponentHelper::getParams('com_jbolo');
		$this->community         = $params->get('community');
		$this->sync_social_inbox = $params->get('sync_social_inbox');
		$this->sync_group_chats  = $params->get('sync_group_chats');
	}

	/**
	 * Converts text-url found in a chat message into an actual url with html
	 *
	 * @param   string  $text  Chat message
	 *
	 * @return  string  $text  Chat message with URL HTML
	 *
	 * @since   3.0
	 */
	public function processUrls($text)
	{
		/*$text = preg_replace("#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie",
			"'<a href=\"$1\" target=\"_blank\">$1</a>$4'",
			$text);*/

		$text = preg_replace_callback(
			"#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#i",
			function($matches)
			{
				return "<a href='" . $matches[1] . "' target=\"_blank\">" . $matches[1] . "</a>";
			},
			$text
		);

		return $text;
	}

	/**
	 * Converts a chat message into an actual download url with html
	 *
	 * @param   string  $text           Chat message
	 * @param   string  $particularUID  User id
	 *
	 * @return  string  $text  Chat message with download link for file
	 *
	 * @since   3.0
	 */
	public function processDownloadLink($text, $particularUID)
	{
		/*$text = preg_replace("#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie",
		"'<a href=\"$1\" target=\"_blank\">".JText::_('COM_JBOLO_CLICK_TO_DOWNLOAD')."</a>$4'",
		$text);*/

		// Message to be shown to file sender
		if ($particularUID)
		{
			$msg          = JText::_('COM_JBOLO_YOU_SENT_FILE') . ' ';
			$downloadLink = JUri::root(true) . '/index.php?option=com_jbolo&controller=sendfile&action=downloadFile&f=' . $text;
			$text         = $msg . '<a href="' . $downloadLink . '" target="">' . JText::_('COM_JBOLO_CLICK_TO_DOWNLOAD') . '</a>';
		}
		// Message to be shown to file receivers
		else
		{
			$msg          = JText::_('COM_JBOLO_I_SENT_FILE') . ' ';
			$downloadLink = JUri::root(true) . '/index.php?option=com_jbolo&controller=sendfile&action=downloadFile&f=' . $text;
			$text         = $msg . '<a href="' . $downloadLink . '" target="">' . JText::_('COM_JBOLO_CLICK_TO_DOWNLOAD') . '</a>';
		}

		return $text;
	}

	/**
	 * Converts a smiley found in a chat message into an actual smiley with html
	 *
	 * @param   string  $text  Chat message
	 *
	 * @return  string  $text  Chat message with HTML for smiley
	 *
	 * @since   3.0
	 */
	/*public function processSmilies($text)
	{
		$params      = JComponentHelper::getParams('com_jbolo');
		$template    = $params->get('template');
		$smiliesfile = file_get_contents(JPATH_SITE . '/components/com_jbolo/jbolo/assets/smileys.txt');
		$smilies     = explode("\n", $smiliesfile);

		foreach ($smilies as $smiley)
		{
			if (trim($smiley) == '')
			{
				continue;
			}

			$pcs    = explode('=', $smiley);
			$img    = JUri::root(true) . '/components/com_jbolo/jbolo/view/' . $template . '/images/smileys/default/' . $pcs[1];
			$imgsrc = '<img src="' . $img . '" border="0" />';
			$text   = str_replace($pcs[0], $imgsrc, $text);
		}

		return $text;
	}*/

	/**
	 * Converts the bad words found in a chat message into ***
	 *
	 * @param   string  $text  Chat message
	 *
	 * @return  string  $text  Chat message with bad words replaced with ***
	 *
	 * @since   3.0
	 */
	public function processBadWords($text)
	{
		$params   = JComponentHelper::getParams('com_jbolo');
		$badwords = $params->get('badwords');
		$badwords = str_replace(' ', '', $badwords);

		if ($badwords !== '')
		{
			$badwords = explode(",", $badwords);
			$count    = count($badwords);

			for ($i = 0; $i < $count; $i++)
			{
				// Replace all ouccrances
				// $badwords[$i] = '/' . $badwords[$i] . '/i';

				// Replace only full words
				$badwords[$i] = '/\b' . $badwords[$i] . '\b/i';
			}

			$replacement = '***';
			$text        = preg_replace($badwords, $replacement, $text);

			return $text;
		}

		return $text;
	}

	/**
	 * Trigger event for onAfterSendMessage
	 * - here we sync messages to selected social network
	 *
	 * @param   object  $msg           Message details
	 * @param   object  $node          Node details
	 * @param   array   $participants  Participants array
	 *
	 * @return  void
	 *
	 * @since   3.1.4
	 */
	public function onJboloAfterSendMessage($msg, $node, $participants)
	{
		// If social message sync is not on
		if (! $this->sync_social_inbox)
		{
			return false;
		}

		// If it's a group chat check if group conversations are to be synced
		if ((count($participants) >= 2) && (!$this->sync_group_chats))
		{
			return false;
		}

		$db = JFactory::getDbo();

		// Get integration
		$socialIntegration = $this->community;

		// Get old conversation id for these chat participants
		$oldConversationId = $this->getSocialConversationId(JFactory::getUser()->id, $participants, $node->owner, $socialIntegration);

		switch ($socialIntegration)
		{
			case 'EasySocial':
				// Set conversation object
				$conversation              = new stdClass;
				$conversation->id          = null;
				$conversation->lastreplied = $msg->time;

				// Set the parent for existing conversation
				if ($oldConversationId)
				{
					// Update existing conversation for setting last replied timesamp
					$conversation->id  = $oldConversationId;

					// Update entry for new message
					$db->updateObject('#__social_conversations', $conversation, 'id');

					// Imp** We need to consider sender as participant in easysocial conversations
					$participants[] = $msg->from;
				}
				// Set the parent as new msg id itself for new conversation
				else
				{
					// Set conversation object
					$conversation->created    = $msg->time;
					$conversation->created_by = $msg->from;

					if (count($participants) >= 2)
					{
						$conversation->type = 2;
					}
					else
					{
						$conversation->type = 1;
					}

					// Add entry for new conversation
					$db->insertObject('#__social_conversations', $conversation, 'id');

					// Imp** We need to consider sender as participant in easysocial conversations
					$participants[] = $msg->from;

					// Add entry for all participants
					foreach ($participants as $participant)
					{
						// Set participant object
						$p = new stdClass;

						$p->conversation_id = $conversation->id;
						$p->user_id         = $participant;
						$p->state           = 1;

						$db->insertObject('#__social_conversations_participants', $p);
					}
				}

				// Set conversation message object
				$conversation_msg = new stdClass;
				$conversation_msg->conversation_id = $conversation->id;

				// Type = message for texts, leave for when user leaves converasation
				$conversation_msg->type            = 'message';
				$conversation_msg->message         = $msg->msg;
				$conversation_msg->created         = $msg->time;
				$conversation_msg->created_by      = $msg->from;

				// Add entry for new conversation message
				$db->insertObject('#__social_conversations_message', $conversation_msg, 'id');

				// Add entry for all participants in conversation message mapping
				foreach ($participants as $participant)
				{
					// Set participant mapping object
					$pMap = new stdClass;

					$pMap->user_id         = $participant;
					$pMap->conversation_id = $conversation->id;
					$pMap->message_id      = $conversation_msg->id;
					$pMap->isread          = 1;
					$pMap->state           = 1;

					$db->insertObject('#__social_conversations_message_maps', $pMap, 'id');
				}

			break;

			case 'jomsocial':
				$obj            = new stdClass;
				$obj->id        = null;
				$obj->from      = $msg->from;
				$obj->posted_on = $msg->time;
				$obj->from_name = JFactory::getUser($msg->from)->name;

				// If only 1 participant,  subject is 'Chat'
				if (count($participants) < 2)
				{
					$obj->subject = 'Chat';
				}
				// If more than 1 participants, subject is 'Group Chat'
				elseif (count($participants >= 2))
				{
					$obj->subject = 'Group Chat';
				}

				// Process message body
				$body = new JRegistry;
				$body->set('content', strip_tags($msg->msg));
				$obj->body = $body->toString();

				// Set the parent for existing conversation
				if ($oldConversationId)
				{
					// Add the parent for existing conversation
					$obj->parent = $oldConversationId;

					// Set RE:subject
					$obj->subject = 'RE:' . $obj->subject;

					// Add entry for new message
					$db->insertObject('#__community_msg', $obj, 'id');
				}
				// Set the parent as new msg id itself for new conversation
				else
				{
					// Add entry for new message in jomsocial #__community_msg table
					$db->insertObject('#__community_msg', $obj, 'id');

					$obj->parent = $obj->id;

					// Update msg entry for parent
					$db->updateObject('#__community_msg', $obj, 'id');
				}

				// Add entry for all participants
				foreach ($participants as $participant)
				{
					$p = new stdClass;
					$p->msg_id     = $obj->id;
					$p->msg_parent = $obj->parent;
					$p->msg_from   = $obj->from;
					$p->to         = $participant;
					$p->bcc        = 0;
					$p->is_read    = 1;
					$p->deleted    = 0;

					$db->insertObject('#__community_msg_recepient', $p);
				}
			break;

			default:
			return 0;
			break;
		}
	}

	/**
	 * Returns the latest conversation id for given participants for a social integration
	 *
	 * @param   integer  $from               Sender's user id
	 * @param   array    $participants       Participants array
	 * @param   array    $owner              Conversation owner
	 * @param   array    $socialIntegration  Social integration easysocial or jomsocial
	 *
	 * @return  integer  $conversationId  Conversation id or 0
	 *
	 * @since   3.1.4
	 */
	public function getSocialConversationId($from, $participants, $owner, $socialIntegration)
	{
		$db = JFactory::getDbo();
		$to = $participants[0];

		switch ($socialIntegration)
		{
			case 'EasySocial':
				// Get query object and write query
				if (count($participants) < 2)
				{
					// Get the LATEST conversation id for participants, where from and to are participants
					// Get query object and write query
					$query = $db->getQuery(true)
						->select('MAX(scp1.conversation_id)')
						->from($db->quoteName('#__social_conversations_participants') . ' AS scp1')
						->join(' LEFT', $db->quoteName('#__social_conversations_participants') . ' AS scp2 ON scp1.conversation_id=scp2.conversation_id')
						->join(' LEFT', $db->quoteName('#__social_conversations') . ' AS sc ON sc.id=scp1.conversation_id')
						->where("sc.type=1")
						->where("scp1.user_id=" . $from)
						->where("scp2.user_id=" . $to);

					$db->setQuery($query);
					$conversationId = $db->loadResult();
				}
				elseif (count($participants >= 2))
				{
					// Get the LATEST conversation id for participants, where from and to are participants
					// Get query object and write query
					$query = $db->getQuery(true)
						->select('MAX(scp1.conversation_id)')
						->from($db->quoteName('#__social_conversations_participants') . ' AS scp1')
						->join(' LEFT', $db->quoteName('#__social_conversations_participants') . ' AS scp2 ON scp1.conversation_id=scp2.conversation_id')
						->join(' LEFT', $db->quoteName('#__social_conversations') . ' AS sc ON sc.id=scp1.conversation_id')
						->where("sc.type=2")
						->where("scp1.user_id=" . $from)
						->where("scp2.user_id=" . $to);

					$db->setQuery($query);
					$conversationId = $db->loadResult();
				}

			break;

			case 'jomsocial':
				// Get query object and write query
				if (count($participants) < 2)
				{
					// Get the LATEST conversation id for participants, Where from=$to and to=$to or vice-versa and subject is 'Chat'
					$query = $db->getQuery(true)
						->select('MAX(cm.parent) ')
						->from($db->quoteName('#__community_msg') . ' AS cm')
						->join(' LEFT', $db->quoteName('#__community_msg_recepient') . ' AS cmr ON cmr.msg_id=cm.id')
						->where(" ((cmr.msg_from=" . $from . " AND cmr.to=" . $to . ") OR (cmr.msg_from=" . $to . " AND cmr.to=" . $from . "))")
						->where(' cm.subject=' . $db->quote('Chat'))
						->where(' cm.deleted=0');

					// Set query and get result
					$db->setQuery($query);
					$conversationId = $db->loadResult();
				}
				elseif (count($participants >= 2))
				{
					// Get the LATEST conversation id for participants, Where from=$owner and to=$owner or vice-versa and subject is 'Group Chat'

					/*
					 * $conversations
					 *
					 * parent   users
					 * 6        64,65
					 */

					// Check if participants have shared a conversation already
					// Make array of all participants
					$participantsAll   = (array) $participants;
					$participantsAll[] = $from;
					$participantsAll[] = $owner;
					$participantsAll   = array_unique($participantsAll);

					$query = $db->getQuery(true);

					$query = "SELECT cmp.msg_parent AS parent, cm.from, cmp.msg_from, GROUP_CONCAT(DISTINCT(cmp.to)) AS users
					 FROM
					 (
						 SELECT DISTINCT(cmp2.to), cmp2.msg_parent, cmp2.msg_from
						 FROM `#__community_msg_recepient` AS cmp2
						 WHERE cmp2.deleted=0
						 ORDER BY cmp2.msg_parent, cmp2.to ASC
					 ) AS cmp
					 LEFT JOIN `#__community_msg` AS cm ON cm.id = cmp.msg_parent
					 WHERE `cm`.`subject` = " . $db->quote('Group Chat') .
					" AND cm.from IN (" . implode(',', $participantsAll) . ")
					 GROUP BY cm.id";

					// Set query and get result
					$db->setQuery($query);
					$conversations = $db->loadObjectList();

					// Sort array => 63 64 65 66
					asort($participantsAll);

					// Make array of all participants except msg sender => 63 64 65 66
					$participantsExceptFrom = $participantsAll;

					// Search msg sender in array and remove if found => 63 65 66
					$key = array_search($from, $participantsAll);

					if ($key !== false)
					{
						unset($participantsExceptFrom[$key]);
					}

					// Get comma separated pid list => 63,65,66
					$participantsExceptFromCS = implode(',', $participantsExceptFrom);

					$conversationId  = 0;
					$count           = count($conversations);

					for ($i = 0; $i < $count; $i++)
					{
						// @TODO chk line below
						// $conversations[$i]->users . '-' . $conversations[$i]->parent;

						/*
						* $conversations
						* from 66
							p f   users
							2 64  66,63,64,65
							3 65  63,64,65,66

							from 64
							p f   users
							2 64  66,63,64,65
							3 65  63,64,65,66
						*/

						// $participantsAll 65 66 63 64

						// If node found for 2 users, exit loop
						// If current conversation owner is the current message sender
						if ($conversations[$i]->from == $from)
						{
							// Convert CS list to array 66 63 64 65
							$conversations[$i]->users = explode(',', $conversations[$i]->users);

							// Sort array => 63 64 65 66
							asort($conversations[$i]->users);

							// Search conversation owner in array and remove if found => 63 65 66
							$key = array_search($conversations[$i]->from, $conversations[$i]->users);

							if ($key !== false)
							{
								unset($conversations[$i]->users[$key]);
							}

							// Convert array to CS list => 63,65,66
							$conversations[$i]->users = implode(',', $conversations[$i]->users);

							// If node found for 2 users, exit loop
							if ($conversations[$i]->users == $participantsExceptFromCS)
							{
								$conversationId = $conversations[$i]->parent;
								break;
							}
						}
						else
						{
							/*
							* $conversations
							* from 66
							*
								p f   users
								2 64  66,63,64,65
								3 65  63,64,65,66
							*/

							// Make array of all participants except conversations[$i]->from => 63 64 65 66
							$participantsExceptFromTemp   = $participantsAll;
							$participantsExceptFromTemp[] = $from;
							$participantsExceptFromTemp   = array_unique($participantsExceptFromTemp);

							// Remove conversations[$i]->from from array => 63 64 65 66 => 63 65 66
							$key = array_search($conversations[$i]->from, $participantsExceptFromTemp);

							if ($key !== false)
							{
								unset($participantsExceptFromTemp[$key]);
							}

							// Sort array 63 65 66
							asort($participantsExceptFromTemp);

							// Get comma separated pid list => 63,65,66
							$participantsExceptFromTempCS = implode(',', $participantsExceptFromTemp);

							// Convert CS list to array 66 63 64 65
							$conversations[$i]->users = explode(',', $conversations[$i]->users);
							$conversations[$i]->users = array_unique($conversations[$i]->users);

							// Remove conversations[$i]->from from $conversations[$i]->users array => 66 63 64
							$key = array_search($conversations[$i]->from, $conversations[$i]->users);

							if ($key !== false)
							{
								unset($conversations[$i]->users[$key]);
							}

							// Sort 63 65 66
							asort($conversations[$i]->users);

							// Convert array to CS list => 63,65,66
							$conversations[$i]->users = implode(',', $conversations[$i]->users);

							if ($conversations[$i]->users == $participantsExceptFromTempCS)
							{
								// If node found for 2 users, exit loop
								$conversationId = $conversations[$i]->parent;
								break;
							}
						}
					}
				}

			break;

			default:
			return 0;
			break;
		}

		// Return old ceonversation id OR 0
		if ($conversationId)
		{
			return $conversationId;
		}
		else
		{
			return 0;
		}
	}
}
