<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  plg_js_jbolo_online
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Load language file for plugin frontend
$lang = JFactory::getLanguage();
$lang->load('plg_community_plg_js_jbolo_online', JPATH_ADMINISTRATOR);

require_once JPATH_ROOT . '/components/com_community/libraries/core.php';

/**
 * Class for JBolo commumity plugin
 *
 * @package     JBolo
 * @subpackage  plg_js_jbolo_online
 * @since       3.0
 */
class PlgCommunityplg_Js_Jbolo_Online extends CApplications
{
	protected $name = 'plg_js_jbolo_online';

	protected $path = '';

	protected $cache = null;

	/**
	 * Constructor
	 *
	 * @param   string  &$subject  Name
	 * @param   array   $config    Array of config options
	 *
	 * @since   3.0
	 */
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);

		// Get component params
		$params = JComponentHelper::getParams('com_jbolo');
		$this->community         = $params->get('community');
		$this->sync_social_inbox = $params->get('sync_social_inbox');
		$this->sync_group_chats  = $params->get('sync_group_chats');
	}

	/**
	 * Jomsocial's onProfileDisplay trigger
	 *
	 * @return  string  $content  HTML string
	 *
	 * @since   3.0
	 */
	public function onProfileDisplay()
	{
		// Return if Jbolo component not found or is disabled
		if (!JFile::exists(JPATH_ROOT . '/components/com_jbolo/jbolo.php') || !JComponentHelper::isEnabled('com_jbolo', true))
		{
			return false;
		}

		// Check if JBolo module is enabled
		jimport('joomla.application.module.helper');
		$module = JModuleHelper::getModule('jbolo');

		// If the module is disabled, return
		if (!$module)
		{
			return false;
		}

		// Get jbolo system plugin
		$plugin = JPluginHelper::getPlugin('system', 'jbolo');

		// If the plugin is disabled, return
		if (!$plugin)
		{
			return false;
		}

		$content = '';

		if ($this->community !== 'jomsocial')
		{
			return $content;
		}

		$model = CFactory::getModel('profile');
		$my    = CFactory::getUser();
		$user  = CFactory::getRequestUser();
		$this->loadUserParams();

		$db = JFactory::getDBO();

		$lang = JFactory::getLanguage();
		$lang->load('com_jbolo');

		$accepted = 0;

		// Load nodes helper file
		$nodesHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/nodes.php';

		if (!class_exists('nodesHelper'))
		{
			JLoader::register('nodesHelper', $nodesHelperPath);
			JLoader::load('nodesHelper');
		}

		$nodesHelper = new nodesHelper;

		$my = JFactory::getUser();

		$isBlocked = $nodesHelper->isBlockedBy($user->id, $my->id);

		if (!$isBlocked)
		{
			$isBlocked = $nodesHelper->isBlockedBy($my->id, $user->id);
		}

		// Not to show user status if one of both, blocked one to one chat
		if ($isBlocked)
		{
			return;
		}

		$jboloallowall = $this->userparams->get('jboloallowall', 1);

		if (!$jboloallowall)
		{
			$db = JFactory::getDBO();

			$query = $db->getQuery(true)
					->select('status')
					->from($db->quoteName('#__community_connection'))
					->where($db->quoteName('connect_from') . ' = ' . $my->id)
					->where($db->quoteName('connect_to') . ' = ' . $user->id);

			$db->setQuery($query);
			$accepted = $db->loadResult();
		}
		else
		{
			$accepted = 1;
		}

		if ($accepted)
		{
			$query = $db->getQuery(true)
				->select('userid')
				->from($db->quoteName('#__session'))
				->where($db->quoteName('userid') . ' = ' . $user->id);

			$db->setQuery($query);

			if ($my->id !== $user->id)
			{
				if ($db->loadResult())
				{
					$content = "<table width='100%'>
						<tr>
							<td class='fieldCell'>
								<a style='text-decoration:none;' href='javascript:void(0)' onclick=\"javascript:chatFromAnywhere(" . $my->id . ", " . $user->id . ")\">
									<div class='statusicon_1'></div>
										<span>" .
											sprintf(JText::_('COM_JBOLO_COMMUNITY_PLG_ONLINE_MSG'), $user->username) . "
										</span>
								</a>
							</td>
						</tr>
					</table>";
				}
				else
				{
					$content = "<table width='100%'>
						<tr>
							<td class='fieldCell'>
								<div class='statusicon_4'></div>
									<span>" .
										sprintf(JText::_('COM_JBOLO_COMMUNITY_PLG_OFFLINE_MSG'), $user->username) . "
									</span>
							</td>
						</tr>
					</table>";
				}
			}
		}
		elseif ($user->id !== $my->id)
		{
			$content = "<table width='100%'>
				<tr>
					<td class='fieldCell'>" . JText::_('COM_JBOLO_COMMUNITY_PLG_PRIVACY') . "</td>
				</tr>
			</table>";
		}

		return $content;
	}

	/**
	 * Jomsocial's onFormSave trigger
	 * Refer to Refer to http://documentation.jomsocial.com/wiki/OnFormSave
	 * We will catch - Jomsocial's New Message form here
	 *
	 * @param   string  $form_name  Form name
	 *
	 * @return  boolean
	 *
	 * @since   3.1.4
	 */
	public function onFormSave($form_name)
	{
		if ($this->community !== 'jomsocial')
		{
			return false;
		}

		// If social sync is on and if it is a jomsocial's new message form
		if ($this->sync_social_inbox && $form_name == 'jsform-inbox-write')
		{
			$input = JFactory::getApplication()->input;
			$myId  = CFactory::getUser()->id;

			// Get participants, subject and message
			$participants = $input->post->get('friends', '', 'ARRAY');
			$subject      = $input->post->get('subject', '', 'STRING');
			$msg          = $input->post->get('body', '', 'STRING');

			// Add validations for all inputs
			if (!$participants[0] || $subject == '' || $msg == '')
			{
				return false;
			}

			// If it's a group chat check if group conversations are to be synced
			if ((count($participants) >= 2) && (!$this->sync_group_chats))
			{
				// Do nothing
				return true;
			}
			else
			{
				// Since the message subject might not be 'Chat' all the time, we need to concat subject & msg
				$msg = $subject . ': ' . $msg;

				// Push message to JBolo
				$pushMessagesToJboloResult = $this->pushMessagesToJbolo($uid = $myId, $participants, $msg, $nodeOwner = $myId);
			}
		}

		return true;
	}

	/**
	 * Jomsocial's onAjax call trigger
	 * Refer to http://documentation.jomsocial.com/wiki/OnAjaxCall
	 * We will catch - Jomsocial's inbox reply form [ajaxAddReply], and,
	 * Jomsocial's New Message form (sent from other user's profile) here [ajaxSend]
	 *
	 * @param   string  $arrItems  ajax function name
	 *
	 * @return  boolean
	 *
	 * @since   3.1.4
	 */
	public function onAjaxCall($arrItems)
	{
		if ($this->community !== 'jomsocial')
		{
			return false;
		}

		$input = JFactory::getApplication()->input;

		// $input = json_decode($input);

		$myId  = CFactory::getUser()->id;

		// Get message id and mesage
		$task = $input->post->getString('task', '');
		$func = $input->post->getString('func', '');

		/*  [option] => community
			[no_html] => 1
			[task] => azrul_ajax
			[func] => inbox,ajaxSend
			[0418a59bf54cf36e514a00a8c037cfd6] => 1
			[arg2] => [[subject,Chat],[body,jujuj jujj],[to,66]]
			[view] => article
			[id] => 4
			[Itemid] => 101
			[basedata] => null*/

		// If social sync is on and if it is a jomsocial's New Message form from other user's profile
		if ($this->sync_social_inbox && $task == 'azrul_ajax' && $func == 'inbox,ajaxSend')
		{
			$msgData = $input->post->getString('arg2', '');
			$msgData = json_decode($msgData);

			/*
			[0] => Array
				(
					[0] => subject
					[1] => Chat
				)

			[1] => Array
				(
					[0] => body
					[1] => hi
				)

			[2] => Array
				(
					[0] => to
					[1] => 66
				)
			*/

			$participants = array();

			// Get participants, subject and message
			$subject         = $msgData[0][1];
			$msg             = $msgData[1][1];
			$participants[0] = $msgData[2][1];

			// Needed from jomsocial 4.0
			$msg = urldecode($msg);

			// Add validations for all inputs
			if (!$participants[0] || $subject == '' || $msg == '')
			{
				return false;
			}

			// Since the message subject might not be 'Chat' all the time, we need to concat subject & msg
			$msg = $subject . ': ' . $msg;

			// Push message to JBolo
			$pushMessagesToJboloResult = $this->pushMessagesToJbolo($uid = $myId, $participants, $msg, $nodeOwner = $myId);
		}

		/*
		 * If present arg4 - will be file id
		 *
			[option] => community
			[no_html] => 1
			[task] => azrul_ajax
			[func] => inbox,ajaxAddReply
			[35b5899d3fc6db677c69d51c0a53a43f] => 1
			[arg2] => [_d_,1]
			[arg3] => [_d_,asdsadsa]
			[arg4] => [_d_,]
			[view] => article
			[id] => 4
			[Itemid] => 101
			[basedata] => null*/

		// If social sync is on and if it is a jomsocial's inbox reply form
		elseif ($this->sync_social_inbox && $arrItems == 'inbox,ajaxAddReply')
		{
			$input = JFactory::getApplication()->input;

			// Get message id and mesage
			$msgId = $input->post->getInt('arg2', '');
			$msg   = $input->post->getString('arg3', '');

			// Get the actual posted message
			// Let's do st(R)ing operations ;)
			$msg = explode(',', $msg);
			$msg = rtrim($msg[1], ']');
			$msg = rtrim($msg, '"');
			$msg = ltrim($msg, '"');

			// Needed from jomsocial 4.0
			$msg = urldecode($msg);

			// Use jomsocial model to get recepient
			jimport('joomla.application.component.model');
			JModelLegacy::addIncludePath(JPATH_BASE . DS . 'components' . DS . 'com_community' . DS . 'models');
			$model = JModelLegacy::getInstance('Inbox', 'CommunityModel');

			// $messageRecepient = $model->getUserMessage($msgId);

			// If user has access to reply
			// if (!empty($messageRecepient) && $model->canReply($myId, $msgId))
			if ($model->canReply($myId, $msgId))
			{
				$param             = array();
				$param['reply_id'] = $msgId;
				$participants      = $model->getMultiRecepientID($param);

				// Add validations for all inputs
				if (!$participants[0] || $msg == '')
				{
					return false;
				}

				// If it's a group chat check if group conversations are to be synced
				if ((count($participants) >= 2) && (!$this->sync_group_chats))
				{
					// Do nothing
					return true;
				}
				else
				{
					// Initialize variables.
					$db    = JFactory::getDbo();
					$query = $db->getQuery(true);

					// Get conversation owner
					$query->select('cm.from, cm.subject')
					->from($db->quoteName('#__community_msg') . ' AS cm')
					->where($db->quoteName('cm.id') . ' = ' . $msgId);

					// Set the query and load the result.
					$db->setQuery($query);

					try
					{
						$cmsg  = $db->loadObject();
						$owner = $cmsg->from;
						$msg   = $cmsg->subject . ': ' . $msg;
					}
					catch (RuntimeException $e)
					{
						JError::raiseWarning(500, $e->getMessage());

						return false;
					}

					// Push message to Jbolo
					$pushMessagesToJboloResult = $this->pushMessagesToJbolo($uid = $myId, $participants, $msg, $nodeOwner = $owner);
				}
			}
		}

		return true;
	}

	/**
	 * Returns the latest conversation id for given participants for a social integration
	 *
	 * @param   integer  $uid           Sender's user id
	 * @param   array    $participants  Participants array
	 * @param   string   $msg           Message
	 * @param   integer  $nodeOwner     Group owner
	 *
	 * @return  object  $pushChatToNodeResult  Object with message details
	 *
	 * @since   3.1.4
	 */
	public function pushMessagesToJbolo($uid, $participants, $msg, $nodeOwner)
	{
		// Load nodes helper file
		$nodesHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/nodes.php';

		if (!class_exists('nodesHelper'))
		{
			JLoader::register('nodesHelper', $nodesHelperPath);
			JLoader::load('nodesHelper');
		}

		// Load users helper file
		$jbolousersHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/users.php';

		if (!class_exists('jbolousersHelper'))
		{
			JLoader::register('jbolousersHelper', $jbolousersHelperPath);
			JLoader::load('jbolousersHelper');
		}

		// Load integrations helper file
		$integrationsHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/integrations.php';

		if (!class_exists('integrationsHelper'))
		{
			JLoader::register('integrationsHelper', $integrationsHelperPath);
			JLoader::load('integrationsHelper');
		}

		// Load jbolo helper file
		$chatBroadcastHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/chatBroadcast.php';

		if (!class_exists('chatBroadcastHelper'))
		{
			JLoader::register('chatBroadcastHelper', $chatBroadcastHelperPath);
			JLoader::load('chatBroadcastHelper');
		}

		// Check if node exists
		$nodesHelper = new nodesHelper;

		// If only 1 participant, check for 1to1 node
		if (count($participants) == 1)
		{
			$node_id_found = $nodesHelper->checkNodeExists($uid, $participants[0]);
		}
		// If more than 1 participants, check for group chat node
		elseif (count($participants) > 1)
		{
			$node_id_found = $nodesHelper->checkGroupChatNodeExists($uid, $nodeOwner, $participants);
		}

		// If no node found, create one node
		if ($node_id_found == 0)
		{
			// If only 1 participant, create 1to1 node
			if (count($participants) == 1)
			{
				$ini_node = $nodesHelper->initiateNode($uid, $participants);
			}
			// If more than 1 participants, create group chat node
			elseif (count($participants) > 1)
			{
				$ini_node = $nodesHelper->initiateGroupNode($nodeOwner, $participants);
			}

			if (isset($ini_node['nodeinfo']))
			{
				$node_id_found = $ini_node['nodeinfo']->nid;
			}
		}

		// Push message to node
		if ($node_id_found)
		{
			// Add logged in user as participant
			$participants[] = $uid;

			// Push chat to jbolo node
			$pushChatToNodeResult = $nodesHelper->pushChatToNode($uid, $participants, $node_id_found, $msg);

			return $pushChatToNodeResult;
		}

		return false;
	}
}
