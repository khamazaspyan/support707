<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

$lang = JFactory::getLanguage();
$lang->load('plg_community_jgive', JPATH_ADMINISTRATOR);

// Load Campaigns helper
$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

if (!class_exists('campaignHelper'))
{
	JLoader::register('campaignHelper', $helperPath);
	JLoader::load('campaignHelper');
}

/**
 * Campaign plgCommunityjgive class.
 *
 * @package  JGive
 * @since    1.8
 */
class PlgCommunityjgive extends CApplications
{
	/**
	 * Function onProfileDisplay
	 *
	 * @return  void
	 *
	 * @since   1.8
	 */
	public function onProfileDisplay()
	{
		// Payee user
		$user = CFactory::getRequestUser();

		return $this->_getHTML($user->id);
	}

	/**
	 * Function _getHTML
	 *
	 * @param   string  $user_id  User information
	 *
	 * @return  html
	 *
	 * @since   1.8
	 */
	public function _getHTML($user_id)
	{
		$params_jgive = JComponentHelper::getParams('com_jgive');

		// Get the data to idetify which field to show
		$show_selected_fields = $params_jgive->get('show_selected_fields');
		$creatorfield         = array();
		$show_field           = 0;
		$goal_amount          = 0;

		if ($show_selected_fields)
		{
			$creatorfield = $params_jgive->get('creatorfield');

			if (isset($creatorfield))
			{
				foreach ($creatorfield as $tmp)
				{
					switch ($tmp)
					{
						case 'goal_amount':
							$goal_amount = 1;
							break;
					}
				}
			}
		}
		else
		{
			$show_field = 1;
		}

		$db = JFactory::getDBO();

		// Get count of campaigns by profile user
		$query = $db->getQuery(true);
		$query->select('COUNT(c.id) AS total');
		$query->from($db->qn('#__jg_campaigns', 'c'));
		$query->where($db->qn('c.published') . ' = 1' . ' AND ' . $db->qn('c.creator_id') . ' = ' . $user_id);
		$db->setQuery($query);
		$total_campaigns = $db->loadResult();

		// Get campaigns data by profile user
		$query = $db->getQuery(true);
		$query->select(
		array('c.id', 'c.category_id', 'c.org_ind_type', 'c.creator_id', 'c.title', 'c.type',
		'c.goal_amount', 'c.allow_exceed', 'c.start_date', 'c.end_date', 'c.max_donors', 'c.success_status', 'c.featured'
		)
		);
		$query->from($db->qn('#__jg_campaigns', 'c'));
		$query->where($db->qn('c.published') . ' = 1 AND ' . $db->qn('c.creator_id') . ' = ' . $user_id);
		$query->setLimit((int) $this->params->get('count'));

		$db->setQuery($query);
		$data = $db->loadObjectList();

		if ($data)
		{
			// Load css
			$document = JFactory::getDocument();
			$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');
			$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive.css');
			$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/bs3/css/bootstrap.min.css');

			// Get JGive component params
			$jgive_params = JComponentHelper::getParams('com_jgive');

			// Load jgive front end helper
			$helperFrontPath = JPATH_SITE . "/components/com_jgive/helper.php";

			if (!class_exists('jgiveFrontendHelper'))
			{
				// Require_once $path;
				JLoader::register('jgiveFrontendHelper', $helperFrontPath);
				JLoader::load('jgiveFrontendHelper');
			}

			$jgiveFrontendHelper = new jgiveFrontendHelper;

			// Get items ids
			$this->allCampaignsItemid   = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');

			// Use campaign helper
			$campaignHelper = new campaignHelper;
			$cdata          = array();

			// Modifiy the data
			foreach ($data as $d)
			{
				$camp_startdate = JFactory::getDate($d->start_date)->Format(JText::_('Y-m-d H:i:s'));
				$camp_enddate = JFactory::getDate($d->end_date)->Format(JText::_('Y-m-d H:i:s'));
				$curr_date = JFactory::getDate()->Format(JText::_('Y-m-d H:i:s'));

				if ($curr_date > $camp_enddate)
				{
					// Camp closed
					$d->donteButtonStatusFlag = 0;
				}
				elseif ($curr_date < $camp_startdate)
				{
					// Campaign Not yet started
					$d->donteButtonStatusFlag = -1;
				}
				else
				{
					$d->donteButtonStatusFlag = 1;
				}

				// Get campaign amounts
				$amounts             = $campaignHelper->getCampaignAmounts($d->id);
				$d->amount_received  = $amounts['amount_received'];
				$d->remaining_amount = $amounts['remaining_amount'];
				$d->donor_count      = $campaignHelper->getCampaignDonorsCount($d->id);

				// Push modified data in cdata
				$cdata[$d->id]['campaign'] = $d;

				// Get campaign images
				$cdata[$d->id]['images']   = $campaignHelper->getCampaignImages($d->id);
			}

			// Get currency from component config
			$params              = JComponentHelper::getParams('com_jgive');
			$this->currency_code = $params->get('currency');
			?>

			<div class="tjBs3">
				<?php
				$html = "";

				foreach ($cdata as $displayData)
				{
					$displayData['otherData']['singleCampaignItemid'] = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');
					$displayData['otherData']['allCampaignsItemid'] = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');

					$layout = new JLayoutFile('pin', $basePath = JPATH_SITE . '/components/com_jgive/layouts/campaigns');
					$html .= $layout->render($displayData);
				}

				// Show link if all campaigns are not shown
				if ($total_campaigns && $total_campaigns > $this->params->get('count'))
				{
					$viewmorelink = JUri::root() . substr(
						JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&user_filter=' . $user_id . '&Itemid=' . $this->allCampaignsItemid),
						strlen(JUri::base(true)) + 1);

					$html .= '<div class="col-sm-12">
								<a href="'. $viewmorelink .'">' . JText::_('PLG_JGIVE_VIEW_ALL_CAMPAIGNS_FROM_USER') . '(' . $total_campaigns . ')' . '</a>
							</div>';
				}?>
				<!--end of bootstrap div-->
			</div>
		<?php
			$result = $html;
		}

		return $result;
	}
}
