<?php
/**
 *
 * @copyright   Webemus All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

 
class PlgUserWbs_autologin extends JPlugin
{
	
	public function onUserAfterSave($user, $isnew, $success, $msg)
	{

		// If the user wasn't stored we don't login
		if (!$success)
		{
			return false;
		}

		// If the user isn't new we don't login
		if (!$isnew)
		{
			return false;
		}

		// Ensure the user id is really an int
		$user_id = (int) $user['id'];

		// If the user id appears invalid then bail out just in case
		if (empty($user_id))
		{
			return false;
		}

		$app 	= JFactory::getApplication();
	
		//Set return URL
		$item 	= $app->getMenu()->getItem($this->params->get('login'));
		
		// Go to home page
		$return = JUri::base();

		if ($item)
		{
			$lang = '';

			if (JLanguageMultilang::isEnabled() && $item->language !== '*')
			{
				$lang = '&lang=' . $item->language;
			}

			$return = 'index.php?Itemid=' . $item->id . $lang;
		}
		
		
		
		// Set the return URL in the user state to allow modification by plugins
		$app->setUserState('users.login.form.return', $return);

		// Get the log in options.
		$options = array();
		$options['remember'] 	= false;
		$options['return'] 		= JURI::base();

		// Get the log in credentials.
		$credentials = array();
		$credentials['username'] = $user['username'];
		$credentials['password'] = $user['password1'];





		//Send Notification mail to administrators before redirecting to login page
		$params = JComponentHelper::getParams('com_users');
		
		
		//load language package
		$language 	= JFactory::getLanguage();
		$base_dir 	= JPATH_SITE . '/language';
		if(!$language->load('com_users', $base_dir, $language->getTag(), true))
		{
			 $language->load('com_users', $base_dir, 'en-GB', true);
		}

		
		$config = JFactory::getConfig();
		
		// Compile the notification mail values.
		$data['name']		= $user['name'];
		$data['username'] 	= $user['username'];
		$data['fromname'] 	= $config->get('fromname');
		$data['mailfrom'] 	= $config->get('mailfrom');
		$data['sitename'] 	= $config->get('sitename');
		$data['siteurl'] 	= JUri::root();
		
		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1))
		{
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBodyAdmin = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
				$data['name'],
				$data['username'],
				$data['siteurl']
			);

			// Get all admin users
			$db 	= JFactory::getDbo();
			$query 	= $db->getQuery(true);

			$query->clear()
				->select($db->quoteName(array('name', 'email', 'sendEmail')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('sendEmail') . ' = ' . 1);

			$db->setQuery($query);

			try
			{
				$rows = $db->loadObjectList();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);

				return false;
			}

			// Send mail to all superadministrators id
			foreach ($rows as $row)
			{
				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);

				// Check for an error.
				if ($return !== true)
				{
					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

					return false;
				}
			}
		} 
		
		//Send Notification mail to administrators before redirecting to login page - ENDs




		// Perform the log in.
		$app->login($credentials, $options);
		$app->setUserState('users.login.form.data', array());
		$app->redirect(JRoute::_($app->getUserState('users.login.form.return'), false));
	}


	
	
}
