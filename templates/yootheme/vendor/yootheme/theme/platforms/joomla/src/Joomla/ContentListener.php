<?php

namespace YOOtheme\Theme\Joomla;

use YOOtheme\EventSubscriber;
use YOOtheme\Theme\Builder;

class ContentListener extends EventSubscriber
{
    const PATTERN = '/^<!-- (\{.*\}) -->/';

    protected $user;
    protected $edit;
    protected $isArticle;
    protected $inject = [
        'db' => 'app.db',
        'admin' => 'app.admin',
        'routes' => 'app.routes',
        'customizer' => 'theme.customizer',
        'session' => 'JFactory::getSession',
        'application' => 'JFactory::getApplication',
    ];

    public function onInit($theme)
    {
        $this->user = \JFactory::getUser();
        $this->routes->post('/page', [$this, 'savePage']);
    }

    public function onSite($theme)
    {
        $input = $this->application->input;
        $statement = 'SELECT userid FROM @session WHERE session_id = :session_id';

        $this->isArticle = $input->getCmd('option') == 'com_content' && $input->getCmd('view') == 'article' && $input->getCmd('task') == null;

        if ($this->isArticle
            AND $this->customizer->isActive()
            AND $theme->params->get('admin')
            AND $session_id = $input->cookie->get(md5(\JApplicationHelper::getHash('administrator')))
            AND $session = $this->db->fetchAssoc($statement, compact('session_id'))
            AND $session['userid']
        ) {
            $this->session->set('user', \JFactory::getUser($session['userid']));
        }
    }

    public function onDispatch($document, $input)
    {
        $this->session->set('user', $this->user);

        if ($this->admin || !$this->isArticle) {
            return;
        }

        if (!$article = \JControllerLegacy::getInstance('Content')->getView('article', 'html')->get('Item') OR !$article->params->get('access-view')) {
            return;
        }

        $edit = '';
        $content = preg_match(self::PATTERN, $article->fulltext, $matches) ? json_decode($matches[1], true) : null;

        if ($article->params->get('access-edit')) {

            if ($this->customizer->isActive()) {

                if ($page = $this->theme->params->get('page')) {
                    $content = $page['content'];
                }

                if ($content) {
                    $content = Builder::encode($content, false);
                }

                $data = [
                    'id' => $article->id,
                    'catid' => $article->catid,
                    'title' => $article->title,
                    'content' => $content,
                    'modified' => !empty($page),
                ];

                $this->customizer->addData('page', $data);

            } else {

                $url = \JRoute::_(\ContentHelperRoute::getFormRoute($article->id).'&return='.base64_encode(\JUri::getInstance()));

                $edit = "<a style=\"position: fixed!important\" class=\"uk-position-medium uk-position-bottom-right uk-button uk-button-primary\" href=\"{$url}\">".\JText::_('JACTION_EDIT')."</a>";
            }

        }

        $this->theme->set('builder', $content !== null ? compact('content', 'edit') : null);
    }

    public function savePage($page = [])
    {
        jimport('legacy.model.legacy');

        $data = [
            'id' => $page['id'],
            'catid' => $page['catid'],
            'title' => $page['title'],
            'introtext' => Builder::content($page['content']),
            'fulltext' => '<!-- '.Builder::encode($page['content']).' -->',
        ];

        if (!$this->user->authorise('core.edit', "com_content.article.{$data['id']}")) {
            $this->app->abort(403, 'Insufficient User Rights.');
        }

        if ($tags = (new \JHelperTags)->getTagIds($page['id'], 'com_content.article')) {
            $data['tags'] = explode(',', $tags);
        }

        \JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_content/models', 'ContentModel');
        \JModelLegacy::getInstance('Article', 'ContentModel', ['ignore_request' => true])->save($data);

        return 'success';
    }

    public static function getSubscribedEvents()
    {
        return [
            'theme.init' => 'onInit',
            'theme.site' => 'onSite',
            'dispatch' => ['onDispatch', 10],
        ];
    }
}
