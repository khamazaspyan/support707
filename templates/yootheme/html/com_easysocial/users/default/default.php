<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<?php echo $this->html('responsive.toggle'); ?>
<div class="es-container  uk-container" data-es-users data-es-container>
    <div class="es-sidebar-sticky">

        <script>
            jQuery(document).ready(function($) {
                var height = jQuery('.es-sidebar-sticky .es-sidebar').height();
                var header = jQuery('.es-profile-header').height();
                jQuery(window).scroll(function() {
                    var scrool = jQuery(this).scrollTop();
                    var d = jQuery(this).scrollTop();
                    var e = jQuery(".es-profile-header").height()+ height;
                    if(d >= e){

                        if(height<550){
                            if(scrool>=header){
                                jQuery('.es-sidebar-sticky').addClass('fixed');
                                jQuery('.es-sidebar-sticky .es-sidebar').css('bottom', '0px');
                            }
                        }
                        else{
                            jQuery('.es-sidebar-sticky').addClass('fixed');
                            jQuery('.es-sidebar-sticky .es-sidebar').css('bottom','0');
                        }
                    }
                    else{
                        jQuery('.es-sidebar-sticky').removeClass('fixed');
                        jQuery('.es-sidebar-sticky .es-sidebar').css('bottom','initial');
                    }
                });

            });
        </script>
	<div class="es-sidebar" data-sidebar>
		<?php echo $this->render('module', 'es-users-sidebar-top', 'site/dashboard/sidebar.module.wrapper'); ?>

		<?php if ($createCustomFilter || $searchFilters) { ?>
		<div class="es-side-widget">
			<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_USERS_BROWSE_BY_FILTERS', $createCustomFilter); ?>

			<div class="es-side-widget__bd">
				<?php if ($searchFilters) { ?>
				<ul class="o-tabs o-tabs--stacked">
					<?php foreach ($searchFilters as $searchFilter) { ?>
					<li class="o-tabs__item filter-item<?php echo $filter == 'search' && $fid == $searchFilter->id ? ' active' : '';?>" data-filter-item data-type="search" data-id="<?php echo $searchFilter->id;?>">
						<a href="<?php echo ESR::users(array('filter' => 'search', 'id' => $searchFilter->getAlias()));?>" class="o-tabs__link" title="<?php echo $searchFilter->get('title'); ?>">
							<?php echo $this->html('string.escape', $searchFilter->get('title')); ?>
						</a>
						<div class="o-loader o-loader--sm"></div>
					</li>
					<?php } ?>
				</ul>
				<?php } else { ?>
				<div class="t-text--muted"><?php echo JText::_('COM_EASYSOCIAL_ADVANCED_SEARCH_EMPTY_FILTERS');?></div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>

		<?php echo $this->render('module', 'es-users-sidebar-bottom', 'site/dashboard/sidebar.module.wrapper'); ?>

        <div class="profile-ads-position">
            <?php echo $this->render('module', 'es-profile-sidebar-ads-position'); ?>
        </div>

	</div>
    </div>

	<div class="es-content" style="margin-top: 16px;">
		<?php echo $this->render('module', 'es-users-before-contents'); ?>

		<div class="fbstfrnds">
			<div class="fbstfrnds_hd">
				<div class="fbstfrnds_hd_title">
					<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_USERS'); ?>
				</div>
				<div class="fbstfrnds_hd_subtitle">
					<ul class="o-tabs o-tabs--stacked">
						<li class="o-tabs__item <?php echo !$filter || $filter == 'all' ? 'active' : '';?>" data-filter-item data-type="users" data-id="all">
							<a href="<?php echo ESR::users();?>" class="o-tabs__link" title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_USERS', true);?>">
								<?php echo JText::_('COM_EASYSOCIAL_USERS_FILTER_USERS_ALL_USERS');?>
							</a>
							<div class="o-loader o-loader--sm"></div>
						</li>
					</ul>
				</div>

			</div>
			<div data-contents>
				<div class="fbstfrnds_bd">
					<div class="fbstfrnds_bd1">
						<?php echo $this->includeTemplate('site/users/default/wrapper'); ?>
					</div>
					<?php echo $this->html('html.loading'); ?>
				</div>
			</div>
		</div>

		<?php echo $this->render('module', 'es-users-after-contents'); ?>
	</div>


</div>
