<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<link rel="stylesheet" type="text/css" href="<?php echo JUri::base();?>/components/com_djclassifieds/themes/default/css/style.css">

<script>
    jQuery(document).ready(function($) {
        var height = jQuery('.es-sidebar .sidebar').height();
        jQuery(window).scroll(function() {

            var d = jQuery(this).scrollTop() + jQuery(window).height();
            var e =  height;
            var w = jQuery('.content').height();
            if(d >= e && w>e){
                jQuery('.es-sidebar .sidebar').addClass('es-sidebar_fixed');
                jQuery('.es-sidebar .sidebar.es-sidebar_fixed').css('bottom','0');
            }
            else{
                jQuery('.es-sidebar .sidebar').removeClass('es-sidebar_fixed');
            }
        });

    });
</script>
<style>
	.es-right-sidebar .es-right-sidebar-fixed.fixed_pos{position: fixed;bottom: 0%;width:278.88px; }
	.content{width: 100%}
</style>

<div class="es-dashboard uk-container" data-es-dashboard>
                   
	<?php if ($this->config->get('users.dashboard.sidebar') != 'hidden') { ?>
		<?php echo $this->html('responsive.toggle'); ?>
	<?php } ?>

	<div id="es_sidebar_data_container_toolbar" class="djclsf es-container <?php echo $this->config->get('users.dashboard.sidebar') == 'right' ? 'es-sidebar-right' : '';?>" data-es-container>

	<div class="es-sidebar" data-sidebar>
			<div class="sidebar">

                <div class="listing-home-menu">
                    <h2>
                        <a href="/free-items">
                            <i class="listing"></i>
                            <span>Free Listings</span>
                        </a>
                    </h2>
                </div>

                <div class="dj_jm-module button-post-ad">
                    <div class="jm-module-in">
                        <div class="jm-module-content clearfix notitle">

<?php  $current_url = parse_url($_SERVER["REQUEST_URI"])['path']; ?>
                            <div class="custombutton-post-ad">
                                <p><a class="add_new-free_listing" href="<?php echo $current_url.'?page='?>new">+ Post a free listing</a></p></div>

                        </div>
                    </div>
                </div>
                <div class="dj_jm-module dj_filter_search dj_cat_menu" style="    margin-top: 13px;">
                    <div class="jm-module-in">
                        <h3 class="jm-title "><span>Location</span></h3>
                        <div class="jm-module-content clearfix ">
                            <div id="mod_djcf_search206" class="dj_cf_search cat_lvl2">
                                <form action="" method="get" name="form-search206" id="form-search-listing">


                                    <div class="search_word djcf_se_row">
                                        <i class="location_icon fa fa-location-arrow"></i>
                                        <input style="width: 100%!important;" type="text" id="input_location" size="12" name="location" class="inputbox first_input"  placeholder="your location..." autocomplete="off" autocorrect="off" autocapitalize="off" value="<?php if(isset($_GET['search'])): echo $_GET['search']; endif; ?>">
                                    </div>
                                    <div class="search_word djcf_se_row">
                                        <select name="distance">
                                            <option value="5">Within 5 miles</option>
                                            <option value="10">Within 10 miles</option>
                                            <option value="15">Within 15 miles</option>
                                            <option value="20">Within 20 miles</option>
                                            <option value="30">Within 30 miles</option>
                                            <option value="50">Within 50 miles</option>
                                            <option value="100">Within 100 miles</option>
                                        </select>
                                    </div>


                                </form>

                                <div style="clear:both"></div>
                            </div>




                        </div>
                    </div>

                </div>
                <div class="dj_jm-module _menu white-ms dj_cat_menu">
                    <div class="jm-module-in">
                        <h3 class="jm-title "><span>Categories</span></h3>
                        <div class="jm-module-content clearfix ">
                            <div class="djcf_menu">

                                <ul class="menu nav _menu white-ms">
                                    <?php
                                    $pageURL = parse_url($_SERVER["REQUEST_URI"])['path'];
                                    $i=0;
                                    ?>
                                    <li class=""><a <?php if($_REQUEST['cat']==''): ?>class="active"<?php endif; ?> href="<?php echo $pageURL.'?cat='.$cat->alias;?>">All Categories<span><?php if(sizeof($items)>0): ?>(<?php echo sizeof($items);?>)<?php endif; ?></span></a></li>
                                    <?php foreach ($cats as $cat): ?>
                                    <li class=""><a <?php if($_REQUEST['cat']==$cat->alias): ?>class="active"<?php endif; ?> href="<?php echo $pageURL.'?cat='.$cat->alias;?>"><?php echo $cat->name;?><span><?php if($cat->items_count): ?>(<?php echo $cat->items_count;?>)<?php endif; ?></span></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>




                        </div>
                    </div>
                </div>

			</div>
		</div>

		<div class="content" data-wrapper style="    padding-left: 17px;">
			<?php echo $this->html('html.loading'); ?>

			<?php echo $this->render('module', 'es-dashboard-before-contents'); ?>

			<div data-contents>
				<?php if ($_GET['id']) {
					echo $this->includeTemplate('site/classifieds/default/single-ad');
				}elseif($_GET['page']) {
					echo $this->includeTemplate('site/classifieds/default/new-ad');
				}elseif($_GET['cat']){
					 echo $this->includeTemplate('site/classifieds/default/cat');
				}
				elseif($_GET['edit']){
                    echo $this->includeTemplate('site/classifieds/default/edit-ad');
                }
                elseif($_GET['delete']){
                    echo $this->includeTemplate('site/classifieds/default/delete-ad');
                }
                elseif ($_GET['search']){
                    echo $this->includeTemplate('site/classifieds/default/search');
                }
				else{
                    echo $this->includeTemplate('site/classifieds/default/feeds');
                }
                ?>
			</div>

			<?php echo $this->render('module', 'es-dashboard-after-contents'); ?>
		</div>

	</div>
</div>
