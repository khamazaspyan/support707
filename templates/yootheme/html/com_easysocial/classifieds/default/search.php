<link rel="stylesheet" type="text/css" href="<?php echo JUri::base();?>/components/com_djclassifieds/themes/default/css/style.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">
    .item_title a i {
        margin-right: 5px;
    }
</style>
<div id="dj-classifieds">

    <?php
        $db = JFactory::getDBO();
        $items_s = $db->setQuery("SELECT a.*,c.name as c_name,c.alias as c_alias FROM #__djcf_items as a
                                  INNER JOIN #__djcf_categories as c on c.id = a.cat_id
                                  WHERE a.name LIKE '%".$_GET['search']."%' OR a.description LIKE '%".$_GET['search']."%'")->loadObjectList();
        foreach($items_s as $item){
            $item->images = $db->setQuery("SELECT * FROM #__djcf_images WHERE item_id = '".$item->id."'")->loadObjectList();
            foreach($item->images as $img){
                $img->thumb_s = $img->path.$img->name.'_ths.'.$img->ext;
                $img->thumb_m = $img->path.$img->name.'_thm.'.$img->ext;
                $img->thumb_b = $img->path.$img->name.'_thb.'.$img->ext;
            }
        }
    ?>
    <?php  echo $this->includeTemplate('site/classifieds/default/search-box'); ?>

    <div class="dj-items">
        <div class="dj-items-table-smart">

            <div class="dj-items-rows">
                <?php
                //$geturl = & JFactory::getURI();
                //$current_url = $geturl->toString();
                $current_url = parse_url($_SERVER["REQUEST_URI"])['path'];
                if($_GET['cat']){

                    foreach ($items_s as $item) {


                        if ($item->c_alias == $_GET['cat']) {  ?>
                            <?php $image_link = ($item->images[0]->thumb_m) ? $item->images[0]->thumb_m : '/components/com_djclassifieds/assets/images/no-image-big.png' ;?>


                        <?php }}}else{
                    foreach ($items_s as $row=>$item) { ?>
                        <?php $image_link = ($item->images[0]->thumb_m) ? $item->images[0]->thumb_m : '/components/com_djclassifieds/assets/images/no-image-big.png' ;?>
                        <div class="item_row item_row0"><div class="item_row_in">


                                <div class="item_outer">
                                    <div class="item_outer_in">
                                        <div class="item_img_box">
                                            <div class="item_img_box_in">
                                                <a href="<?php echo $current_url.'?id='.$item->id;?>">
                                                    <img src="<?php echo JURI::base().'/'.$image_link;?>" alt="<?php echo $item->name;?>">			</a>									</a>
                                            </div>
                                        </div>
                                        <div class="item_content">
                                            <div class="item_content_in">
                                                <div class="item_title">
                                                    <h3>
                                                        <a class="title Tips1" href="<?php echo $current_url.'?id='.$item->id;?>" title="<?php echo $item->name;?>" ><?php echo $item->name;?></a>
                                                    </h3>
                                                </div>

                                                <div class="item_cat_region_outer">
                                                    <div class="item_date">
                                                        <i class="fa fa-clock-o "></i>
                                                        <span><?php echo JHTML::_('date', $item->date_start , JText::_('DATE_FORMAT_LC3')); ?></span>
                                                    </div>
                                                    <div class="item_category">
                                                        <a href="/free-items?cat=<?php echo $item->c_alias;?>"><i style="padding-right: 4px;" class="fa fa-list-ul"></i><?php echo $item->c_name;?></a>														</div>
                                                    <?php if($item->address): ?>
                                                        <div class="item_region">
                                                            <a class="" target="_blank" href="https://www.google.com/maps/?q=<?php echo $item->latitude;?>,<?php echo $item->longitude;?>"><i style="padding-right: 6px;" class="fa fa-location-arrow"></i><?php echo $item->address; ?></a>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    <?php }} ?>
            </div>
        </div>
    </div>
</div>

