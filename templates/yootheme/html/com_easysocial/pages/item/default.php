<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-profile uk-container" data-es-page data-id="<?php echo $page->id;?>">

	<?php echo $this->html('cover.page', $page, $layout); ?>

	<?php echo $this->html('responsive.toggle'); ?>

	<div class="es-container" data-es-container>

		<?php if ($layout == 'timeline' || $layout == 'moderation' || $layout == 'filterForm') { ?>
        <div class="es-sidebar-sticky">
		<div class="es-sidebar" data-sidebar>

            <script>
                jQuery(document).ready(function($) {
                    var height = jQuery('.es-sidebar-sticky .es-sidebar').height();
                    var header = jQuery('.es-profile-header').height();
                    jQuery(window).scroll(function() {
                        var scrool = jQuery(this).scrollTop();
                        var d = jQuery(this).scrollTop();
                        var e = jQuery(".es-profile-header").height()+ height;
                        if(d >= e){

                            if(height<550){
                                if(scrool>=header){
                                    jQuery('.es-sidebar-sticky').addClass('fixed');
                                    jQuery('.es-sidebar-sticky .es-sidebar').css('bottom', '0px');
                                }
                            }
                            else{
                                jQuery('.es-sidebar-sticky').addClass('fixed');
                                jQuery('.es-sidebar-sticky .es-sidebar').css('bottom','0');
                            }
                        }
                        else{
                            jQuery('.es-sidebar-sticky').removeClass('fixed');
                            jQuery('.es-sidebar-sticky .es-sidebar').css('bottom','initial');
                        }
                    });

                });
            </script>


			<?php echo $this->render('module', 'es-pages-item-sidebar-top', 'site/dashboard/sidebar.module.wrapper'); ?>
			<?php echo $this->render('widgets', SOCIAL_TYPE_PAGE, 'pages', 'sidebarTop', array('uid' => $page->id, 'page' => $page)); ?>

			<div class="es-side-widget">
				<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_PAGES_INTRO'); ?>

				<div class="es-side-widget__bd">
					<?php if (!$this->isMobile() && $this->config->get('pages.layout.description')) { ?>
						<?php echo $this->html('string.truncate', $page->getDescription(), 120, '', false, false, false, true);?>
						<a href="<?php echo $aboutPermalink;?>"><?php echo JText::_('COM_EASYSOCIAL_READMORE');?></a>
					<?php } ?>

					<ul class="o-nav o-nav--stacked t-lg-mt--sm">
						<li class="o-nav__item t-text--muted t-lg-mb--sm">
							<a class="o-nav__link t-text--muted" href="<?php echo $page->getCreator()->getPermalink();?>">
								<i class="es-side-widget__icon fa fa-user t-lg-mr--md"></i>
								<?php echo $page->getCreator()->getName();?>
							</a>
						</li>

						<li class="o-nav__item t-text--muted t-lg-mb--sm">
							<a class="o-nav__link t-text--muted" href="<?php echo $page->getAppPermalink('followers');?>">
								<i class="es-side-widget__icon fa fa-thumbs-o-up t-lg-mr--md"></i>
								<?php
                                if($page->category_id == 17 || $page->category_id == 18){
                                    echo JText::sprintf(ES::string()->computeNoun('COM_ES_SUPPORT_COUNT', $page->getTotalMembers()), $page->getTotalMembers());

                                }
                                else{
                                    echo JText::sprintf(ES::string()->computeNoun('COM_ES_LIKES_COUNT', $page->getTotalMembers()), $page->getTotalMembers());

                                }

                                ?>
							</a>
						</li>
					</ul>
				</div>
				<?php if (!$displayFeedsFilter) { ?>
					<input type="hidden" class="active" data-filter-item="feeds" data-type="feeds" data-id="<?php echo $page->id; ?>" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>" />
				<?php } ?>
			</div>

			<?php echo $this->trigger('fields', 'page', 'item', 'sidebarTop', $page); ?>



			<?php echo $this->render('widgets', SOCIAL_TYPE_PAGE, 'pages', 'sidebarMiddle', array('uid' => $page->id, 'page' => $page)); ?>

			<?php echo $this->render('widgets', SOCIAL_TYPE_PAGE, 'pages', 'sidebarBottom', array('uid' => $page->id, 'page' => $page)); ?>

			<?php echo $this->render('module', 'es-pages-item-sidebar-bottom', 'site/dashboard/sidebar.module.wrapper'); ?>

            <div class="profile-ads-position">
                <?php echo $this->render('module', 'es-profile-sidebar-ads-position'); ?>
            </div>
		</div>
        </div>
		<?php } ?>

		<div class="es-content">

			<?php echo $this->render('module', 'es-pages-before-contents'); ?>

			<?php if ($layout != 'info') { ?>
			<div class="es-content-wrap" data-wrapper>
				<?php echo $this->html('html.loading'); ?>

				<div data-contents>
					<?php if ($contents) { ?>
						<?php echo $contents; ?>
					<?php } else { ?>
						<?php echo $this->includeTemplate('site/pages/item/content'); ?>
					<?php } ?>
				</div>
			</div>
			<?php } ?>

			<?php if ($layout == 'info') { ?>
			<div class="es-profile-info">
				<?php if ($steps) { ?>
					<?php echo $this->output('site/fields/about/default', array('steps' => $steps, 'canEdit' => $page->isAdmin(), 'objectId' => $page->id, 'routerType' => 'pages')); ?>
				<?php } ?>
			</div>
			<?php } ?>

			<?php echo $this->render('module', 'es-pages-after-contents'); ?>
		</div>
	</div>
</div>
