<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
if($profile->id == 4)
{
  	$img_path = 'images/the_survivor_image.jpg';
    $profile->ttl = "In need of assistance";
    $txt = "choose this option if you have been througha disaster or are in need of assistance";
}  
if($profile->id == 3)
{
  	$img_path = 'images/the_supporter_image.jpg';
    $profile->ttl = "Here to support";
    $txt = "Choose this option if you would like to donate orhelp support people in need of assistance";
}  
  
  
?>
<style>
  .support_707_registration_profile_id_3 {float:right;width:45%;margin-left:2.5%;}
  .support_707_registration_profile_id_4 {float:left;width:45%;margin-right:2.5%;}
  .o-flag {
    position: relative;
}
#es .o-flag__image.o-flag--top.t-lg-pr--lg {
    width: 100% !important;
    display: inline-table !important;
    float: left;
    margin: 0px !important;
    padding: 0px !important;
}
  .o-flag__image.o-flag--top.t-lg-pr--lg img {
    width: 100%;
    margin: 0px;
}
  #es .o-flag__body {
    width: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
}
  .support_707_registration_button_div {
    position: absolute;
    bottom: 20px;
    left: 0%;
    width: 100%;
    text-align: center !important;
  }
  .support_707_registration_button_div a.support_707_registration_button {
    float: none !important;
    display: inline-table;
}
  ul.list-profiles.g-list-unstyled {
    width: 80%;
}
  ul.list-profiles.g-list-unstyled li{
    margin:0px !important; width:48% !important
}
  #es .es-card__bd {padding: 0px !important;}
  #es.es-main {background: #fff !important;}
  .support_707_registration_profile_data {position: absolute;bottom: 75px;width: 90%;left: 10%;color:#fff;}
</style>  

<li class="support_707_registration_profile_id_<?php echo $profile->id;?>">
	<div class="es-card">
		<div class="es-card__bd">
			<div class="o-flag" data-behavior="sample_code">
				
				<div class="o-flag__body" style="text-align:center;">
					<b class=" t-mb--sm" style="font-size:18px;color:#fff;"><?php echo $profile->ttl;?></b>
					<!--<div class=" t-mb--sm"><?php /* echo $profile->get('description'); */ ?></div>-->

					<?php /* if ($profile->getRegistrationType() == 'approvals' || $profile->getRegistrationType() == 'verify') { ?>
					<!-- <div>* <?php echo $profile->getRegistrationType(SOCIAL_TRANSLATE_REGISTRATION); ?></div>
					<?php } ?>

					<?php  if ($profile->getMembersCount() && $this->config->get('registrations.layout.users')) { ?>
					 <div class="profile-members">
						<div>
							<hr />
							<div class="list-profile-type-peep t-fs--sm">
								<?php echo JText::sprintf('COM_EASYSOCIAL_REGISTRATIONS_OTHER_PROFILE_MEMBERS', $profile->getMembersCount()); ?>
							</div>

							<?php  if ($profile->users) { ?>
							<ul class="g-list-inline profile-users">
								<?php foreach ($profile->users as $user ){ ?>
								<li data-es-provide="tooltip" data-original-title="<?php echo $this->html( 'string.escape' , $user->getName() );?>" class="t-lg-mb--md t-lg-mr--md">
									<?php echo $this->html('avatar.user', $user, 'sm', true, false); ?>
								</li>
								<?php } ?>
							</ul> -->
							<?php } ?>
						</div>
					</div> --->
					<?php } */?>
				</div>
              <div class="o-flag__image o-flag--top t-lg-pr--lg">
					
											
							<img src="<?php echo $img_path ;?>" title="<?php echo $this->html('string.escape', $profile->ttl);?>" />
											
					
				</div>
			</div>
		</div>
      <div class="support_707_registration_profile_data">
        <?php echo $txt; ?>
      </div> 
		<div class="support_707_registration_button_div" style="margin:0 auto;text-align:center;">
			<a href="<?php echo FRoute::registration( array( 'controller' => 'registration' , 'task' => 'selectType' , 'profile_id' => $profile->id ) );?>" class="support_707_registration_button" style="background: #E83F38;color: #fff;padding: 13px;border-radius: 6px;min-width: 150px;float: left;">
				<?php /* echo JText::_( 'COM_EASYSOCIAL_JOIN_NOW_BUTTON' );  */?> Join Now
			</a>
		</div>
	</div>
</li>