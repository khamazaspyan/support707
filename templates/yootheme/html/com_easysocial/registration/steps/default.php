<?php
/**
* @package      EasySocial
* @copyright    Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet"> 

<style>
#es label {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 17px !important;
}
.o-help-block{font-family: 'Roboto Condensed', sans-serif;}

.es-container {
    background-image: url(https://support707.org/images/easysocial_login/login_background.png);
    background-size: cover;
}
  #es .es-container .es-content {margin: 0 auto;}
 /* .view-registration .o-form-horizontal {
    min-height: 540px;
  }*/
.es-forms__content {
    width: 100%;
    /* float: left; */
    box-shadow: 2px 3px 10px #b7b7b7;
    max-width: 1200px;
    width: 100%;
    background: #fff;
    margin: auto;
    display: block;
}

.o-form-horizontal {
    padding: 30px;
    margin: 25px auto 30px;
}
button#checkUsername , button.btn.btn-es-default-o{
    line-height: 4px!important;
    padding: 20px!important;
}
.o-form-horizontal input {
    border: 1px solid rgb(163, 163, 163)!important;
    padding: 10px;
    background: #f9f9f9!important;
}
#es .chzn-container-single .chzn-single {    height: 40px;    line-height: 40px;}
#es .o-form-control {height: auto!important; padding: 8px; line-height: 25px!important;}
#es .btn-es-default-o {border-color:rgb(163, 163, 163)!important }
#es .es-snackbar {display: block!important; text-align: center; background: transparent;}
#es .es-snackbar__title {font-size: 40px; font-weight: normal;}
#es .es-snackbar + p {
    text-align: center;
    margin-bottom: 50px;
}
#es .es-field-avatar .avatar-frame , #es .es-field-avatar .avatar-wrap-frame{border-radius: 50%; margin-bottom: 20px;}
#es .o-input-group__btn>.btn {    height: 43px;    line-height: 36px;}
#es .btn-file:before{    top: 14px!important;}

#joomla_password .o-help-block {width: 30%;}
#es .chzn-container-single .chzn-single div {top: 5px;}
#es .chzn-container-single .chzn-single {
    height: 35px;
    line-height: 35px;
}
.es-forms__actions {
    max-width: 1200px;
    margin: 0 auto 50px;
    
}
#es .btn-primary-o {
    color: #fff!important;
    background-color: rgb(232, 63, 56)!important;
    border-color: rgb(232, 63, 56)!important;
}
#es .btn-primary-o:hover {color: #fff!important;}
#es .o-form-actions {background: transparent; border: none; padding: 0;}
div#joomla_password .es-fields-error-note {
    width: 100%;
    position: absolute;
    left: 0;
    top: 70px;
}
div#joomla_password ul li {width: 49%!important; float: left;}
div#joomla_password ul li:last-child {float: right;}

div#joomla_password ul li:first-child input {
    
    float: left;
}
div#joomla_password ul li:last-child input {
  
    float: right;
}
/*#joomla_password {width: 100%; float: left;}*/
#joomla_password input,div#joomla_username, #joomla_email , div#joomla_fullname, div#textbox {width: 49%; display: inline-block;}
div#joomla_email, div#gender , div#birthday {
    width: 49%;
    float: right;
}
div#joomla_fullname input {
    margin-bottom: 6px!important;
}
#es .o-form-horizontal .o-form-group {display: block!important;}
button.btn.btn-primary-o.pull-right {height: 45px!important; letter-spacing: 1px;}

#es .t-lg-mb--lg {
    margin-bottom: 0px !important;
    text-align: center;
}
.divider-vertical-last {
    display: none;
}
.o-grid__cell.o-grid__cell--auto-size {
    float: left;
    text-align: center !important;
    position: relative;
    left: 40%;
    padding-top: 20px;
}
.profile-selected {
    background: none !important;
}
.profile-selected::after {
    visibility: visible;
    content: "You are currently registering as a Supporter. Would you like to switch?";
    position: absolute;
    left: 0;
    text-align: center;
    width: 100%;
}
.profile-selected {
    visibility: hidden;
    position: relative;
}
/*#es.view-registration.layout-completed .es-complete-wrap .es-login-form {
    margin-bottom: 20px !important;
}
#es h1.t-text--center {
    margin-top: 10px !important;
}*/
/*#dropdown .o-form-group {
    float: right;
    width: 49%;
}*/
.o-control-input #es-fields-380 {
    padding: 11px 10px !important;
}
#dropdown .o-form-group::before {
    border-style: solid;
    border-width: .1em .1em 0 0;
    content: '';
    display: inline-block;
    width: .5em;
    height: .5em;
    position: relative;
    left: 97% !important;
    vertical-align: top;
    top: 43px;
    transform: rotate(135deg);
}

.btn.btn-es-default-o.is-loading::before {
    position: absolute;
    content: '';
    top: 12px;
    left: 30%;
    width: 20px;
    height: 20px;
    border-radius: 500rem;
    border: 4px solid rgba(0, 0, 0, 0.1);
}
.btn.btn-es-default-o.is-loading::after {
    position: absolute !important;
    content: '' !important;
    top: 17px !important;
    left: 50% !important;
    width: 19px !important;
height: 22px !important;
margin: 0 0 0 -10px !important;
    animation: fd-loader .55s linear !important;
    animation-iteration-count: infinite !important;
    border-radius: 500rem !important;
    border-color: #999 transparent transparent !important;
    border-style: solid !important;
    border-width: 4px !important;
    box-shadow: 0 0 0 1px transparent !important;
}
div#dropdown {
    display: inline-block;
    width: 49%;
}
/*#textbox {
    float: right;
}*/
#textbox[data-name=es-fields-375] {
    float: right;
}
/*#textbox[data-name=es-fields-298] {
    float: left;
}*/
#textbox[data-name=es-fields-297] {
    float: right;
}
#textbox[data-name=es-fields-267] {
    float: right;
}
#textbox[data-id="299"] {
    float: right;
}
#es .es-stepbar .es-stepbar__lists > li > a{background-color: #fff !important;}
#es .fa.fa-users.t-lg-mr--sm{display: none;}
.profile-selected.es-bg-shade.es-island.t-lg-p--lg.t-lg-mb--lg {
    font-size: 16px;
}
#textbox .o-control-label {
    width: 100% !important;
}
#es .es-snackbar__title {
    font-family: 'Josefin Sans', sans-serif;
}
#es .es-snackbar + p {
    font-family: 'Josefin Sans', sans-serif;
    margin-bottom: 30px;
}
#es .es-snackbar{margin-bottom: 0px;}
#es .btn.is-loading {
    min-width: 50px !important;
}
.password_strength.password_strength_2.text-warning.small.help-inline {
    margin-top: 16px;
}
#es .es-stepbar .es-stepbar__lists > li.active a {
    background: #E84239 !important;
}
#es .es-stepbar .es-stepbar__lists > li.divider-vertical.active {
    background: #E84239;
}
#es .es-stepbar .es-stepbar__lists > li > a:hover {
    background: #E84239 !important;
}
#es .es-stepbar .es-stepbar__lists > li > a {
    display: block;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    line-height: 29px;
    padding: 0 !important;
    text-align: center;
    background: #d6d6d6;
    color: #888;
    text-shadow: none;
}
#es .es-stepbar .es-stepbar__lists > li > a .fa {
    display: none;
    line-height: 29px;
}
#es .es-stepbar .es-stepbar__lists > li.divider-vertical {
    width: 30px;
    height: 3px;
    background: #d6d6d6;
    top: 14px;
    margin: 0 2px;
    position: relative;
}
#es .o-flag__image > img {
display: block;
max-width: none;
height: 100% !important;
min-height: 492px !important;
}
.es-dialog-modal .es-dialog-title{color: #E84239;}
.es-dialog-modal .es-dialog-content{text-align: center;}
#joomla_password .es-fields-error-note {
    margin-top: 9px;
}


/********** NEED SUPPORT CSS ************/
#checkbox[data-name="es-fields-294"] {
    text-align: center;
}
#checkbox[data-name="es-fields-294"] label {
    text-align: center !important;
}
#checkbox .o-control-label {
    /*text-align: center !important;*/
    float: left;
    width: 100% !important;
    font-size: 26px;
}
#checkbox .o-help-block {
    /*text-align: center;*/
}
#checkbox .o-checkbox {
    display: inline-block;
    margin-left: 8px;
}
#checkbox .es-checkbox-group-es-fields-294 {
    float: none;
    margin: 0 auto;
    width: 143px;
}
#es .o-checkbox label::before {
    content: "";
    display: inline-block;
    position: absolute;
    width: 16px;
    height: 16px;
    left: 0;
    top: 6px;
    margin-left: -16px;
    border: 1px solid #ccc;
    border-radius: 2px;
    background-color: #fff;
    cursor: pointer;
}
#es .o-checkbox label::after {
    display: inline-block;
    position: absolute;
    width: 16px;
    height: 16px;
    line-height: 16px;
    left: 0;
    top: 5px;
    margin-left: -16px;
    padding-left: 2px;
    padding-top: 0;
    font-size: 13px;
    color: #fff;
    cursor: pointer;
}
b.yes{color: #5EC6C2;}
b.noo{color: #E84239;}



/*********** Responsive ***********/

@media screen and (max-width: 1366px) {
.o-grid__cell.o-grid__cell--auto-size {
    left: 35%;
}
}
@media screen and (max-width: 1192px) {
.o-grid__cell.o-grid__cell--auto-size {
    left: 33%;
}
}
@media screen and (max-width: 800px) {
    #joomla_password input, div#joomla_username, #joomla_email, div#joomla_fullname, div#textbox,div#joomla_email, div#gender, div#birthday {width: 100%;}
    .es-forms__content{max-width: 100%!important;}
    #es .es-container .es-content {width: 100%;}
    .es-forms__actions {width: 90%;}
    #joomla_password .o-help-block{width: 100%; float: left;}
    div#joomla_password .es-fields-error-note {position: relative;}
    #es .o-form-horizontal .o-control-label{width: 100%;}
    .o-grid__cell.o-grid__cell--auto-size {left: 30%;}
    #dropdown .o-form-group {width: 100%;}
    #dropdown .o-form-group::before {left: 98% !important; top: 62px;}
    div#dropdown {
    width: 100%;
}
#textbox[data-name="es-fields-375"] {
    float: none;
}
div#joomla_email, div#gender, div#birthday {
    float: none;
}
}
@media screen and (max-width: 680px) {
.o-grid__cell.o-grid__cell--auto-size {
    left: 27%;
}
}

@media screen and (max-width: 580px) {
.o-grid__cell.o-grid__cell--auto-size {
    left: 23%;
}
}
@media screen and (max-width: 420px) {
.o-grid__cell.o-grid__cell--auto-size {
    left: 13%;
}
#dropdown .o-form-group::before {left: 96% !important;}
}
@media screen and (max-width: 375px) {
.o-grid__cell.o-grid__cell--auto-size {
    left: 8%;
}
}
@media screen and (max-width: 320px) {
.o-grid__cell.o-grid__cell--auto-size {
    left: 1%;
}
#dropdown .o-form-group::before {left: 95% !important;}
}
}
</style>  
<div class="es-container">
    <div class="es-content">

        <?php if ($this->config->get('registrations.steps.progress')) { ?>
            <?php echo $this->html('html.steps', $steps, $currentStep, 
                            array('link' => ESR::registration(array('profile_id' => '0')), 'tooltip' => 'COM_EASYSOCIAL_REGISTRATIONS_SELECT_A_PROFILE'), 
                            array('tooltip' => 'COM_EASYSOCIAL_REGISTRATIONS_REGISTRATION_COMPLETE')
                        ); ?>
        <?php } ?>



        <form action="<?php echo JRoute::_('index.php');?>" method="post" enctype="multipart/form-data" class="es-forms has-privacy" data-registration-form>

            <div class="es-forms__group">
                <div class="es-forms__content">
                    <div class="o-form-horizontal">

                        <?php if ($fields) { ?>
                            <?php foreach ($fields as $field) { ?>
                                <?php echo $this->loadTemplate('site/registration/steps/field', array('field' => $field, 'errors' => $errors)); ?>
                            <?php } ?>
                        <?php } ?>

                        <div class="o-form-group">
                            <div class="o-row">
                                <div class="o-col-sm--8">
                                    <?php echo JText::_('COM_EASYSOCIAL_REGISTRATIONS_REQUIRED');?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="es-forms__actions">
                <div class="o-form-actions">
                    <?php if ($currentStep != 1) { ?>
                    <button class="btn btn-es-default-o pull-left" type="button" data-registration-previous><?php echo JText::_('COM_EASYSOCIAL_PREVIOUS_BUTTON'); ?></button>
                    <?php } ?>
                    <button class="btn btn-primary-o pull-right" type="button" data-registration-submit>
                        <div class="o-loader o-loader--sm"></div> <?php echo $currentIndex === $totalSteps || $totalSteps < 2 ? JText::_('COM_EASYSOCIAL_SUBMIT_BUTTON') : JText::_('COM_EASYSOCIAL_CONTINUE_BUTTON');?>
                    </button>
                </div>
            </div>

            <?php echo JHTML::_('form.token'); ?>
            <input type="hidden" name="conditionalRequired" value="<?php echo ES::string()->escape($conditionalFields); ?>" data-conditional-check />
            <input type="hidden" name="currentStep" value="<?php echo $currentIndex; ?>" />
            <input type="hidden" name="controller" value="registration" />
            <input type="hidden" name="task" value="saveStep" />
            <input type="hidden" name="option" value="com_easysocial" />
            <input type="hidden" name="profileId" value="<?php echo $profile->id; ?>" />
        </form>

    </div>
</div>
<script>
jQuery(document).ready(function(){
    jQuery("#joomla_username").keypress(function (e){
        //alert("hello");
    var code =e.keyCode || e.which;
       if((code<65 || code>90)
       &&(code<97 || code>122)&&code!=32&&code!=46)  
      {
       alert("Only alphabates are allowed");
       return false;
      }
    });
});
</script>

<style>
    .avatar-upload-field span.o-input-group__btn{
        position: absolute!important;
        opacity: 0;
        top: -156px;
        width: 134px;
        height: 134px;
        border-radius: 50%;
    }
    .avatar-upload-field span.o-input-group__btn a{
        width: 100%!important;
        height: 100%!important;
    }
    .avatar-upload-field input.o-form-control{
        display: none!important;
    }
    .es-field-cover span.o-input-group__btn{
        position: absolute!important;
        width: 100%;
        height: 379px;
        top: -409px;
        opacity: 0;
    }
    .es-field-cover span.o-input-group__btn span{
        width: 100%!important;
        height: 100%!important;
    }
    .es-field-cover input.o-form-control{
        display: none!important;
    }
</style>