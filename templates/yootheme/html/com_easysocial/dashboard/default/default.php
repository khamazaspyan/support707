<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<?php if (!($this->isMobile())) { ?>
<script>
	jQuery(document).ready(function($) {
		if(jQuery('.es-right-sidebar .sidebar_widget').height() <= jQuery(window).height()){
			jQuery('.es-right-sidebar .es-right-sidebar-fixed').addClass('position', 'fixed');
		}else{
			jQuery(window).scroll(function() {
				var d = jQuery(this).scrollTop() + jQuery(window).height();
				var e = jQuery(".es-right-sidebar .sidebar_widget").height() + jQuery(".es-right-sidebar").offset().top;
				if(d >= e){
					if(jQuery(".es-right-sidebar .es-right-sidebar-fixed").hasClass('fixed_pos') == false){
						jQuery('.es-right-sidebar .es-right-sidebar-fixed').addClass('fixed_pos');
					}
					//jQuery('.es-right-sidebar .sidebar_widget').css('margin-top', (d-e)+'px');
				}else if(jQuery('.es-right-sidebar .es-right-sidebar-fixed').hasClass('fixed_pos')){
					jQuery('.es-right-sidebar .es-right-sidebar-fixed').removeClass('fixed_pos');
				}
			});
		}
	});
</script>
<style>
	.es-right-sidebar .es-right-sidebar-fixed.fixed_pos{position: fixed;bottom: 0%;width:278.88px; }
</style>
<?php } ?>
<div class="es-dashboard uk-container" data-es-dashboard>

	<?php if ($this->config->get('users.dashboard.sidebar') != 'hidden') { ?>
		<?php echo $this->html('responsive.toggle'); ?>
	<?php } ?>

	<div id="es_sidebar_data_container_toolbar" class="es-container <?php echo $this->config->get('users.dashboard.sidebar') == 'right' ? 'es-sidebar-right' : '';?>" data-es-container>

		<?php if ($this->config->get('users.dashboard.sidebar') != 'hidden') { ?>
        <?php if (!($this->isMobile())) { ?>
            <script>
                jQuery(document).ready(function($) {
                    var height = jQuery('.es-sidebar .es_sidebar_check_fix').height();
                    jQuery(window).scroll(function() {

                        var d = jQuery(this).scrollTop() + jQuery(window).height();
                        var e =  height;
                        if(d >= e){
                            jQuery('.es-sidebar .es_sidebar_check_fix').addClass('es-sidebar_fixed');
                           //jQuery('.es-sidebar .es_sidebar_check_fix.es-sidebar_fixed').css('bottom','0');
                        }
                        else{
                            jQuery('.es-sidebar .es_sidebar_check_fix').removeClass('es-sidebar_fixed');
                        }
                    });

                });
            </script>
        <?php } ?>
		<div class="es-sidebar" data-sidebar>
			<div class="es_sidebar_check_fix">
                <?php if ($this->my->id) { ?>
                <div class="es-left-sidebar-user-avatar">

                    <?php echo $this->html('avatar.user', $this->my, 'sm', false, false);?>
                    <a class="user-link" href="<?php echo $this->my->getPermalink();?>"> <span><?php echo $this->my->name; ?></span></a>

                </div>
                <?php } ?>
			<?php echo $this->render('module', 'es-dashboard-sidebar-top', 'site/dashboard/sidebar.module.wrapper'); ?>

			<?php echo $this->render('widgets', SOCIAL_TYPE_USER, 'dashboard', 'sidebarTop'); ?>

			<div class="es-side-widget" data-type="feeds">
				<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_DASHBOARD_SIDEBAR_NEWSFEEDS', $createCustomFilter); ?>

				<div class="es-side-widget__bd">
					<ul class="o-tabs o-tabs--stacked feed-items" data-dashboard-feeds>
						<?php
						if ($this->my->profile_id == 1) {
							?>
							<li class="o-tabs__item " data-filter-item data-type="everyone" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
								<a href="<?php echo ESR::dashboard(array('type' => 'everyone'));?>" class="o-tabs__link">
									<?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_SIDEBAR_NEWSFEEDS_EVERYONE');?>
									<div class="o-tabs__bubble" data-counter>0</div>
								</a>
								<div class="o-loader o-loader--sm"></div>
							</li>
						<?php }
						?>



						<li class="o-tabs__item <?php echo $filter == 'donations' ? ' active' : '';?>" data-filter-item data-type="donations" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
							<a href="<?php echo ESR::dashboard(array('type' => 'me'));?>" class="o-tabs__link">
								<?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_FEEDS_DONATIONS'); ?>
								<div class="o-tabs__bubble" data-counter>0</div>
							</a>
							<div class="o-loader o-loader--sm"></div>
						</li>

                        <li class="o-tabs__item <?php echo $filter == 'support_i_follow' ? ' active' : '';?>" data-filter-item data-type="support_i_follow" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
                            <a href="<?php echo ESR::dashboard(array('type' => 'support_i_follow'));?>" class="o-tabs__link">
                                <?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_FEEDS_SUPPORT_I_FOLLOW'); ?>
                                <div class="o-tabs__bubble" data-counter>0</div>
                            </a>
                            <div class="o-loader o-loader--sm"></div>
                        </li>

                        <li class="o-tabs__item <?php echo $filter == 'recoveries_i_follow' ? ' active' : '';?>" data-filter-item data-type="recoveries_i_follow" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
                            <a href="<?php echo ESR::dashboard(array('type' => 'recoveries_i_follow'));?>" class="o-tabs__link">
                                <?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_FEEDS_RECOVERIES_I_FOLLOW'); ?>
                                <div class="o-tabs__bubble" data-counter>0</div>
                            </a>
                            <div class="o-loader o-loader--sm"></div>
                        </li>

                        <li class="o-tabs__item <?php echo $filter == 'all_recoveries' ? ' active' : '';?>" data-filter-item data-type="all_recoveries" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
                            <a href="<?php echo ESR::dashboard(array('type' => 'all_recoveries'));?>" class="o-tabs__link">
                                <?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_FEEDS_ALL_RECOVERIES'); ?>
                                <div class="o-tabs__bubble" data-counter>0</div>
                            </a>
                            <div class="o-loader o-loader--sm"></div>
                        </li>

						<li class="o-tabs__item " data-filter-item data-type="me" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
							<a href="<?php echo ESR::dashboard(array('type' => 'me'));?>" class="o-tabs__link">
								<?php if ($this->config->get('friends.enabled')) { ?>
								<?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_SIDEBAR_ME_AND_FRIENDS');?>
								<?php } else {  ?>
								<?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_SIDEBAR_MY_UPDATES');?>
								<?php } ?>
								<div class="o-tabs__bubble" data-counter>0</div>
							</a>
							<div class="o-loader o-loader--sm"></div>
						</li>

						<?php //if ($this->config->get('followers.enabled')) { ?>
						<!--<li class="o-tabs__item <?php echo $filter == 'following' ? ' active' : '';?>" data-filter-item data-type="following" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
							<a href="<?php echo ESR::dashboard(array('type' => 'following'));?>" class="o-tabs__link">
								<?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_FEEDS_FOLLOWING');?>
								<div class="o-tabs__bubble" data-counter>0</div>
							</a>
							<div class="o-loader o-loader--sm"></div>
						</li>-->
						<?php //} ?>

						<?php if ($this->config->get('stream.bookmarks.enabled')) { ?>
							<li class="o-tabs__item <?php echo $filter == 'bookmarks' ? ' active' : '';?>" data-filter-item data-type="bookmarks" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
								<a href="<?php echo ESR::dashboard(array('type' => 'bookmarks'));?>" class="o-tabs__link"><?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_FEEDS_BOOKMARKS'); ?></a>
								<div class="o-loader o-loader--sm"></div>
							</li>
						<?php } ?>

						<?php if ($this->config->get('stream.pin.enabled')) { ?>
							<!--<li class="o-tabs__item <?php echo $filter == 'sticky' ? ' active' : '';?>" data-filter-item data-type="sticky" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
								<a href="<?php echo ESR::dashboard(array('type' => 'sticky'));?>" class="o-tabs__link"><?php echo JText::_('COM_EASYSOCIAL_DASHBOARD_FEEDS_STICKY'); ?></a>
								<div class="o-loader o-loader--sm"></div>
							</li>-->
						<?php } ?>

						<?php if ($friendLists && count($friendLists) > 0) { ?>
							<?php foreach ($friendLists as $friendList) { ?>
							<!--<li class="o-tabs__item <?php echo $listId == $friendList->id ? ' active' : '';?>" data-filter-item data-type="list" data-id="<?php echo $friendList->id;?>" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
								<a href="<?php echo ESR::dashboard(array('type' => 'list', 'listId' => $friendList->id));?>" class="o-tabs__link">
									<?php echo $friendList->_('title'); ?>
									<div class="o-tabs__bubble" data-counter>0</div>
								</a>
								<div class="o-loader o-loader--sm"></div>
							</li>-->
							<?php } ?>
						<?php } ?>

						<?php if ($filter == 'hashtag' && isset($hashtag) && $hashtag) { ?>
                        <!--<li class="o-tabs__item active" style="display:none;" data-filter-item data-type="hashtag" data-tag="<?php echo $hashtag ?>" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
								<div class="o-loader o-loader--sm"></div>
							</li>-->
						<?php } ?>

						<?php if ($this->config->get('users.dashboard.customfilters') && ($filterList && count($filterList) > 0)) { ?>
							<?php foreach ($filterList as $filter) { ?>
							<!--<li class="o-tabs__item <?php echo $filterId == $filter->id ? ' active' : '';?>" class="o-tabs__item" data-filter-item data-type="custom" data-id="<?php echo $filter->id; ?>" data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
								<a href="<?php echo ESR::dashboard(array('type' => 'filter', 'filterid' => $filter->getAlias()));?>" class="o-tabs__link">
									<?php echo $filter->_('title'); ?>
								</a>
								<div class="o-loader o-loader--sm"></div>
							</li>-->
							<?php } ?>
						<?php } ?>
					</ul>
				</div>
			</div>
                <?php
                    $my_pages = JFactory::getDBO()->setQuery("SELECT id FROM #__social_clusters WHERE cluster_type='page' AND creator_uid = {$this->my->id} AND state='1'")->loadObjectList();
                    if(!empty($my_pages)): ?>
                        <div class="es-side-widget"  data-type="feeds">
                            <?php echo $this->html('widget.title', 'COM_EASYSOCIAL_DASHBOARD_SIDEBAR_SHORTCUTS', $createCustomFilter); ?>
                            <div class="es-side-widget__bd">
                                <ul class="o-tabs o-tabs--stacked feed-items">
                            <?php foreach($my_pages as $my_page):
                                $r_page = ES::page($my_page->id);
                                $page_avatar = $r_page->getAvatar();
                                $page_link = JUri::base() . '/community/pages/' . $r_page->id . '-' . $r_page->alias;

                                ?>

                                <li class="o-tabs__item  es-left-sidebar-user-avatar">
                                    <a href="<?php echo $page_link; ?>" class="o-avatar o-avatar--sm">
                                        <img src="<?php echo $page_avatar; ?>" alt="<?php echo $my_page->page->title; ?>" width="24" height="24">
                                    </a>
                                    <a class="user-link" href="<?php echo $page_link; ?>"> <span><?php echo $r_page->title; ?></span></a>
                                </li>

                    <?php endforeach; ?>
                                </ul>
                            </div>

                        </div>
<?php                    endif; ?>



			<div class="es-side-widget"  data-type="feeds">
				<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_DASHBOARD_SIDEBAR_EXPLORE', $createCustomFilter); ?>
				<div class="es-side-widget__bd">
					<ul class="o-tabs o-tabs--stacked feed-items">
						<?php if ($dashboard) { ?>
							<li class="<?php echo $highlight == 'dashboard' ? 'is-active' : '';?> is-home">
								<a href="<?php echo ESR::dashboard();?>" class="es-navbar__footer-link">
									<i class="fa fa-home"></i>
									<span>
							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_HOME'); ?>
						</span>
								</a>
							</li>
						<?php } ?>

                        <li class="o-tabs__item  ">
                            <a href="/resource" class="o-tabs__link o-tabs__link2">
                                <i class="es-explore__icons1 es-explore__resources"></i>
                                Resources
                            </a>
                        </li>


						<li class="o-tabs__item  ">
								<a href="/free-items" class="o-tabs__link o-tabs__link2">
									<i class="es-explore__icons1 es-explore__free_items"></i>
									Free items
								</a>
							</li>


						<li class="o-tabs__item  ">
								<a href="/community/pages/categories/16-support-profiles/all/latest" class="o-tabs__link o-tabs__link2">
									<i class="es-explore__icons1 es-explore__support_profiles"></i>
                                    Support Profiles
								</a>
							</li>

						<?php if ($this->my->id) { ?>
							<?php if ($this->isMobile()) { ?>


								<?php if ($showVerificationLink) { ?>
									<li>
										<a href="<?php echo ESR::profile(array('layout' => 'submitVerification'));?>" class="es-navbar__footer-link">
											<i class="fa fa-check-circle-o"></i>
											<span><?php echo JText::_('COM_ES_SUBMIT_VERIFICATION');?></span>
										</a>
									</li>
								<?php } ?>

							<?php } ?>


						<?php } ?>

						<?php if ($this->config->get('pages.enabled')) { ?>
							<li class="o-tabs__item <?php echo $highlight == 'pages' ? 'is-active' : '';?>">
								<a href="/community/pages/categories/6-local-business-or-place/all/latest" class="o-tabs__link">
									<i class="es-explore__icons es-explore__pages"></i>
							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_PROFILE_PAGES'); ?>
								</a>
							</li>
						<?php } ?>

						<?php if ($this->config->get('groups.enabled')) { ?>
							<li class="o-tabs__item <?php echo $highlight == 'groups' ? 'is-active' : '';?>">
								<a href="<?php echo ESR::groups();?>" class="o-tabs__link">
									<i class="es-explore__icons es-explore__groups"></i>

							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_PROFILE_GROUPS'); ?>

								</a>
							</li>
						<?php } ?>

						<?php if ($this->config->get('events.enabled')) { ?>
							<li class="o-tabs__item <?php echo $highlight == 'events' ? 'is-active' : '';?>">
								<a href="<?php echo ESR::events();?>" class="o-tabs__link">
									<i class="es-explore__icons es-explore__events"></i>

							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_PROFILE_EVENTS'); ?>

								</a>
							</li>
						<?php } ?>

						<?php if ($this->my->id) { ?>

							<?php if ($this->config->get('friends.enabled')) { ?>
								<li class="o-tabs__item <?php echo $highlight == 'friends' ? 'is-active' : '';?>">
									<a href="<?php echo ESR::friends();?>" class="o-tabs__link">
										<i class="es-explore__icons es-explore__friends"></i>

								<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_FRIENDS'); ?>

									</a>
								</li>
							<?php } ?>

							<?php if ($this->config->get('friends.invites.enabled') && $this->isMobile()) { ?>
								<li class="o-tabs__item">
									<a href="<?php echo ESR::friends(array('layout' => 'invite'));?>" class="o-tabs__link">
										<i class="es-explore__icons es-explore__friends"></i>

								<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_INVITE_FRIENDS'); ?>

									</a>
								</li>
							<?php } ?>

							<?php if ($this->config->get('followers.enabled')) { ?>
								<li class="o-tabs__item <?php echo $highlight == 'followers' ? 'is-active' : '';?>">
									<a href="<?php echo ESR::followers();?>" class="o-tabs__link">
										<i class="es-explore__icons es-explore__followers"></i>

								<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_FOLLOWERS'); ?>

									</a>
								</li>
							<?php } ?>
						<?php } ?>

						<?php if ($this->config->get('video.enabled')) { ?>
							<li class="o-tabs__item <?php echo $highlight == 'videos' ? 'is-active' : '';?>">
								<a href="<?php echo ESR::videos();?>" class="o-tabs__link">
									<i class="es-explore__icons es-explore__videos"></i>

							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_VIDEOS'); ?>

								</a>
							</li>
						<?php } ?>

						<?php if ($this->config->get('audio.enabled')) { ?>
							<li class="o-tabs__item <?php echo $highlight == 'audios' ? 'is-active' : '';?>">
								<a href="<?php echo ESR::audios();?>" class="o-tabs__link">
									<i class="es-explore__icons es-explore__audios"></i>

							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_AUDIOS'); ?>

								</a>
							</li>
						<?php } ?>



						<li class="o-tabs__item <?php echo $highlight == 'users' ? 'is-active' : '';?>">
							<a href="<?php echo ESR::users();?>" class="o-tabs__link">
								<i class="es-explore__icons es-explore__people"></i>

							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_PEOPLE');?>

							</a>
						</li>

						<?php if ($this->config->get('polls.enabled')) { ?>
							<li class="o-tabs__item <?php echo $highlight == 'polls' ? 'is-active' : '';?>">
								<a href="<?php echo ESR::polls();?>" class="o-tabs__link">
									<i class="es-explore__icons es-explore__polls"></i>

							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_POLLS');?>

								</a>
							</li>
						<?php } ?>

						<?php if ($this->isMobile() && $this->my->id) { ?>
							<li class="<?php echo $view == 'conversations' ? 'is-active' : '';?>">
								<a href="<?php echo ESR::conversations();?>" class="es-navbar__footer-link">
									<i class="fa fa-comment"></i>
									<span>
							<?php echo JText::_('COM_EASYSOCIAL_TOOLBAR_CONVERSATIONS');?>
						</span>
								</a>
							</li>

							<li class="<?php echo $view == 'conversations' ? 'is-active' : '';?>" data-es-logout>
								<a href="javascript:void(0);" class="es-navbar__footer-link" data-es-logout-button>
									<i class="fa fa-power-off"></i>
									<span>
							<?php echo JText::_('Logout');?>
						</span>
								</a>

								<form class="logout-form" action="<?php echo JRoute::_('index.php');?>" data-es-logout-form method="post">
									<input type="hidden" name="return" value="<?php echo $logoutReturn;?>" />
									<input type="hidden" name="option" value="com_easysocial" />
									<input type="hidden" name="controller" value="account" />
									<input type="hidden" name="task" value="logout" />
									<input type="hidden" name="view" value="" />
									<?php echo $this->html('form.token'); ?>
								</form>
							</li>
						<?php } ?>
					</ul>

				</div>

			</div>
			<?php echo $this->render('module', 'es-dashboard-sidebar-before-newsfeeds', 'site/dashboard/sidebar.module.wrapper'); ?>

			<?php if ($appFilters && $this->config->get('users.dashboard.appfilters')) { ?>
			<div class="es-side-widget" data-section data-type="appfilters">
				<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_FILTER_BY_APPS'); ?>

				<div class="es-side-widget__bd">
					<ul class="o-tabs o-tabs--stacked feed-items">
						<?php $i = 1; ?>
						<?php foreach ($appFilters as $appFilter) { ?>
							<li class="o-tabs__item <?php echo $i > 5 ? ' t-hidden' : '';?><?php echo $filterId == $appFilter->alias ? ' active' : '';?>"
								data-filter-item
								data-type="appFilter"
								data-id="<?php echo $appFilter->alias;?>"
								data-stream-identifier="<?php echo $stream->getIdentifier(); ?>">
								<a href="<?php echo ESR::dashboard(array('type' => 'appFilter', 'filterid' => $appFilter->alias));?>" class="o-tabs__link">
									<?php echo $appFilter->title;?>
								</a>
								<div class="o-loader o-loader--sm"></div>
							</li>
							<?php $i++; ?>
						<?php } ?>
					</ul>
				</div>

				<?php if (count($appFilters) > 5) { ?>
				<div class="es-side-widget__ft">
					<a href="javascript:void(0);" data-section-showall>
						<?php echo JText::_('COM_ES_VIEW_ALL');?>
					</a>
				</div>
				<?php } ?>
			</div>
			<?php } ?>

			<?php echo $this->render('module', 'es-dashboard-sidebar-after-newsfeeds', 'site/dashboard/sidebar.module.wrapper'); ?>

			<?php if ($groups && $this->config->get('users.dashboard.groups') && $this->config->get('groups.enabled')) { ?>
			<div class="es-side-widget" data-section-clusters data-type="groups">
				<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_DASHBOARD_SIDEBAR_GROUPS'); ?>

				<div class="es-side-widget__bd">
					<?php echo $this->includeTemplate('site/dashboard/default/filter.groups', array('groups' => $groups)); ?>
				</div>

				<?php if ($showMoreGroups) { ?>
				<div class="es-side-widget__ft">
					<a href="javascript:void(0);" data-clusters-showall>
						<?php echo JText::_('COM_ES_VIEW_ALL');?>
					</a>
				</div>
				<?php } ?>
			</div>
			<?php } ?>

			<?php if ($events && $this->config->get('users.dashboard.events') && $this->config->get('events.enabled')) { ?>
			<div class="es-side-widget" data-section-clusters data-type="events">
				<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_DASHBOARD_SIDEBAR_EVENTS'); ?>

				<div class="es-side-widget__bd">
					<?php echo $this->includeTemplate('site/dashboard/default/filter.events', array('events' => $events)); ?>
				</div>

				<?php if ($showMoreEvents) { ?>
				<div class="es-side-widget__ft">
					<a href="javascript:void(0);" data-clusters-showall>
						<?php echo JText::_('COM_ES_VIEW_ALL');?>
					</a>
				</div>
				<?php } ?>
			</div>
			<?php } ?>

			<?php if ($pages && $this->config->get('users.dashboard.pages', true) && $this->config->get('pages.enabled')) { ?>
			<div class="es-side-widget" data-section-clusters data-type="pages">
				<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_DASHBOARD_SIDEBAR_PAGES'); ?>

				<div class="es-side-widget__bd">
					<?php echo $this->includeTemplate('site/dashboard/default/filter.pages', array('pages' => $pages)); ?>
				</div>

				<?php if ($showMorePages) { ?>
				<div class="es-side-widget__ft">
					<a href="javascript:void(0);" data-clusters-showall>
						<?php echo JText::_('COM_ES_VIEW_ALL');?>
					</a>
				</div>
				<?php } ?>
			</div>
			<?php } ?>

			<div class="widgets-wrapper">
				<?php //echo $this->render('widgets' , SOCIAL_TYPE_USER , 'dashboard' , 'sidebarBottom'); ?>
			</div>

			<?php echo $this->render('module', 'es-dashboard-sidebar-bottom', 'site/dashboard/sidebar.module.wrapper'); ?>
		</div>
		</div>
		<?php } else { ?>
			<input type="hidden" class="active" data-filter-item="feeds" data-type="<?php echo $filter ?>"<?php echo ($filter == 'appFilter') ? ' data-id="' . $filterId . '"' : ''; ?> data-stream-identifier="<?php echo $stream->getIdentifier(); ?>" />
		<?php } ?>

		<div class="es-content <?php if(!($this->isMobile())): ?>es_desktop<?php else: ?>es_mobile<?php endif; ?>" data-wrapper>
			<?php echo $this->html('html.loading'); ?>

			<?php echo $this->render('module', 'es-dashboard-before-contents'); ?>

			<div data-contents>

				<?php if ($contents) { ?>
					<?php echo $contents; ?>
				<?php } else { ?>
					<?php echo $this->includeTemplate('site/dashboard/default/feeds'); ?>
				<?php } ?>
			</div>

			<?php echo $this->render('module', 'es-dashboard-after-contents'); ?>
		</div>

		<div class="es-right-sidebar" style="position: relative;       min-height: 1500px;" data-sidebar>
			<div class="es-right-sidebar-fixed">
				<?php/*
					$document = &JFactory::getDocument();
					$renderer   = $document->loadRenderer('modules');
					$position   = 'es_right_sidebar';
					$options   = array('style' => 'raw');
					echo "<div class=\"sidebar_widget\">".$renderer->render($position, $options, null)."</div>";
				*/?>
				<div class="sidebar_widget">
					<?php echo $this->renderModule('es_right_sidebar'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
