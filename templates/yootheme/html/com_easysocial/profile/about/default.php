<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-profile userProfile uk-container" data-id="<?php echo $user->id;?>" data-es-profile>

	<?php echo $this->render('widgets', 'user', 'profile', 'aboveHeader', array($user)); ?>

	<?php echo $this->render('module', 'es-profile-about-before-header'); ?>

	<?php if ($this->my->isSiteAdmin() && $user->isBlock()) { ?>
	<div class="es-user-banned alert alert-danger">
		<?php echo JText::_('COM_EASYSOCIAL_PROFILE_USER_IS_BANNED');?>
	</div>
	<?php } ?>

	<?php echo $this->html('cover.user', $user, 'about'); ?>

	<?php echo $this->render('module', 'es-profile-about-after-header'); ?>

	<?php echo $this->html('responsive.toggle'); ?>

	<div class="es-container" data-es-container>
        <div class="es-sidebar-sticky">
            <script>
                jQuery(document).ready(function($) {
                    var height = jQuery('.es-sidebar-sticky .es-sidebar').height();
                    var header = jQuery('.es-profile-header').height();
                    jQuery(window).scroll(function() {
                        var scrool = jQuery(this).scrollTop();
                        var d = jQuery(this).scrollTop();
                        var e = jQuery(".es-profile-header").height()+ height;
                        if(d >= e){

                            if(height<550){
                                if(scrool>=header){
                                    jQuery('.es-sidebar-sticky').addClass('fixed');
                                    jQuery('.es-sidebar-sticky .es-sidebar').css('bottom', '0px');
                                }
                            }
                            else{
                                jQuery('.es-sidebar-sticky').addClass('fixed');
                                jQuery('.es-sidebar-sticky .es-sidebar').css('bottom','0');
                            }
                        }
                        else{
                            jQuery('.es-sidebar-sticky').removeClass('fixed');
                            jQuery('.es-sidebar-sticky .es-sidebar').css('bottom','initial');
                        }
                    });

                });
            </script>
		<div class="es-sidebar" data-sidebar>
			<?php echo $this->render('module', 'es-profile-about-sidebar-top' , 'site/dashboard/sidebar.module.wrapper'); ?>

			<?php echo $this->output('site/profile/about/stats', array('user' => $user)); ?>

			<?php echo $this->render('module', 'es-profile-about-sidebar-bottom' , 'site/dashboard/sidebar.module.wrapper'); ?>
            <div class="profile-ads-position">
                <?php echo $this->render('module', 'es-profile-sidebar-ads-position'); ?>
            </div>
        </div>
        </div>

		<div class="es-content">
			<div class="es-profile-info">
				<?php echo $this->output('site/fields/about/default', array('steps' => $steps, 'canEdit' => $user->id == $this->my->id, 'routerType' => 'profile')); ?>
			</div>

			<?php echo $this->render('module', 'es-profile-about-after-contents'); ?>
		</div>
	</div>
</div>

