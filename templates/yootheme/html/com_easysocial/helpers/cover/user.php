<?php
/**
 * @package		EasySocial
 * @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * EasySocial is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Unauthorized Access');
$viewer = JFactory::getUser();
?>
<div class="es-profile-header t-lg-mb--lg " data-profile-header data-id="<?php echo $user->id;?>" data-name="<?php echo $this->html('string.escape' , $user->getName());?>" data-avatar="<?php echo $user->getAvatar();?>">
    <div class="es-profile-header__hd <?php echo $this->config->get('users.layout.cover') ? ' with-cover' : ' without-cover';?>">
        <?php if ($this->config->get('users.layout.cover') && (!isset($showCover) || $showCover)) { ?>
            <div
                <?php if ($cover->photo_id && $cover->getPhoto()->album_id) { ?>
                    data-es-photo-group="album:<?php echo $cover->getPhoto()->album_id;?>"
                <?php } ?>
            >
                <div data-profile-cover
                    <?php echo $cover->photo_id ? 'data-es-photo="' . $cover->photo_id . '"' : '';?>
                     class="es-profile-header__cover es-flyout <?php echo $user->hasCover() ? '' : 'no-cover'; ?> <?php echo !empty($newCover) ? "editing" : ""; ?>"
                     style="background-image   : url(<?php echo $cover->getSource();?>);background-position: <?php echo $cover->getPosition();?>;">

                    <div class="es-cover-container">
                        <div class="es-cover-viewport">
                            <div
                                data-cover-image
                                class="es-cover-image"
                                <?php if (!empty($newCover)) { ?>
                                    data-photo-id="<?php echo $newCover->id; ?>"
                                    style="background-image: url(<?php echo $newCover->getSource('large'); ?>);"
                                <?php } ?>

                                <?php if ($cover->id) { ?>
                                    data-photo-id="<?php echo $cover->getPhoto()->id; ?>"
                                    style="background-image: url(<?php echo $cover->getSource(); ?>);"
                                <?php } ?>
                            >
                            </div>

                            <div class="es-cover-hint">
							<span>
								<span class="o-loader o-loader--sm o-loader--inline with-text"><?php echo JText::_('COM_EASYSOCIAL_PHOTOS_COVER_LOADING'); ?></span>
								<span class="es-cover-hint-text"><?php echo JText::_('COM_EASYSOCIAL_PHOTOS_COVER_DRAG_HINT'); ?></span>
							</span>
                            </div>

                            <div class="es-cover-loading-overlay"></div>

                            <?php if( $user->id == $this->my->id ){ ?>
                                <div class="es-flyout-content">
                                    <div class="dropdown_ pull-right es-cover-menu" data-cover-menu>
                                        <a href="javascript:void(0);" data-bs-toggle="dropdown" class="dropdown-toggle_ es-flyout-button">
                                            <?php echo JText::_('COM_EASYSOCIAL_PHOTOS_EDIT_COVER');?>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li data-cover-upload-button>
                                                <a href="javascript:void(0);"><?php echo JText::_("COM_EASYSOCIAL_PHOTOS_UPLOAD_COVER"); ?></a>
                                            </li>
                                            <li data-cover-select-button>
                                                <a href="javascript:void(0);"><?php echo JText::_('COM_EASYSOCIAL_PHOTOS_SELECT_COVER'); ?></a>
                                            </li>
                                            <li data-cover-edit-button>
                                                <a href="javascript:void(0);"><?php echo JText::_('COM_EASYSOCIAL_PHOTOS_REPOSITION_COVER'); ?></a>
                                            </li>
                                            <li class="divider for-cover-remove-button"></li>
                                            <li data-cover-remove-button>
                                                <a href="javascript:void(0);"><?php echo JText::_("COM_EASYSOCIAL_PHOTOS_REMOVE_COVER"); ?></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <a href="javascript:void(0);" class="es-cover-done-button es-flyout-button" data-cover-done-button>
                                        <i class="fa fa-check"></i>&nbsp; <?php echo JText::_("COM_EASYSOCIAL_PHOTOS_COVER_DONE"); ?>
                                    </a>

                                    <a href="javascript:void(0);" class="es-cover-cancel-button es-flyout-button" data-cover-cancel-button>
                                        <i class="fa fa-remove"></i>&nbsp; <?php echo JText::_("COM_ES_CANCEL"); ?>
                                    </a>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="es-profile-header__avatar-wrap es-flyout" data-profile-avatar
            <?php if ($showAvatar) { ?>
                data-es-photo-group="album:<?php echo $user->getAvatarPhoto()->album_id;?>"
            <?php } ?>
        >
            <a href="<?php echo $user->getAvatarPhoto() ? 'javascript:void(0);' : $user->getPermalink();?>" class=""
                <?php if ($showAvatar) { ?>
                    data-es-photo="<?php echo $user->getAvatarPhoto()->id;?>"
                <?php } ?>
            >
                <img src="<?php echo $user->getAvatar(SOCIAL_AVATAR_SQUARE);?>" alt="<?php echo $this->html('string.escape' , $user->getName() );?>" data-avatar-image />

                <?php echo $this->loadTemplate('site/utilities/user.online.state', array('online' => $user->isOnline(), 'size' => 'small')); ?>
            </a>

            <?php if ($user->isViewer()) { ?>
                <div class="es-flyout-content">
                    <div class="dropdown_ es-avatar-menu" data-avatar-menu>
                        <a href="javascript:void(0);"
                           class="es-flyout-button dropdown-toggle_"
                           data-bs-toggle="dropdown"><?php echo JText::_('COM_EASYSOCIAL_PHOTOS_EDIT_AVATAR');?></a>
                        <ul class="dropdown-menu">
                            <li data-avatar-upload-button>
                                <a href="javascript:void(0);"><?php echo JText::_("COM_EASYSOCIAL_PHOTOS_UPLOAD_AVATAR"); ?></a>
                            </li>

                            <li data-avatar-select-button>
                                <a href="javascript:void(0);"><?php echo JText::_('COM_EASYSOCIAL_PHOTOS_SELECT_AVATAR'); ?></a>
                            </li>

                            <?php if ($this->config->get('users.avatarWebcam') && !$this->isMobile()) { ?>
                                <li class="divider"></li>
                                <li data-avatar-webcam>
                                    <a href="javascript:void(0);"><?php echo JText::_("COM_EASYSOCIAL_PHOTOS_TAKE_PHOTO"); ?></a>
                                </li>
                            <?php } ?>

                            <?php if ($user->hasAvatar()) { ?>
                                <li class="divider"></li>
                                <li data-avatar-remove-button>
                                    <a href="javascript:void(0);">
                                        <?php echo JText::_("COM_EASYSOCIAL_PHOTOS_REMOVE_AVATAR"); ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>

            <?php echo $this->render('module', 'es-profile-avatar'); ?>
        </div>

        <?php echo $this->render('widgets', 'user', 'profile', 'afterAvatar', array($user)); ?>
    </div>

    <div class="es-profile-header__bd">
        <div class="es-profile-header__info-wrap">
            <?php echo $this->render('module', 'es-profile-before-name'); ?>
            <?php echo $this->render('widgets', 'user', 'profile', 'beforeName' , array($user)); ?>

            <h1 class="es-profile-header__title">
                <?php echo $this->html('html.user', $user); ?>
            </h1>

            <?php echo $this->render('widgets', 'user', 'profile', 'afterName', array($user)); ?>
            <?php echo $this->render('module', 'es-profile-after-name'); ?>



        </div>

        <div class="es-profile-header__action-wrap">

            <?php echo $this->render('widgets', 'user', 'profile', 'beforeActions', array($user)); ?>
            <?php echo $this->render('module', 'es-profile-before-actions'); ?>


            <div class="btn-toolbar" role="toolbar">

                <?php if (!$user->isViewer()) { ?>
                    <div class="btn-group btn-group--viewer" role="group">
                        <?php echo $this->html('user.friends', $user); ?>

                        <?php echo $this->html('user.subscribe', $user); ?>

                        <?php echo $this->html('user.conversation', $user); ?>
                    </div>
                <?php } ?>

                <div class="btn-group">
                    <?php echo $this->html('user.bookmark', $user); ?>
                </div>

                <?php echo $this->html('user.actions', $user); ?>
            </div>

            <?php echo $this->render('module', 'es-profile-after-actions'); ?>
            <?php echo $this->render('widgets', 'user', 'profile', 'afterActions' , array($user)); ?>
        </div>
    </div>

    <div class="es-profile-header-nav">
        <div class="es-profile-header-nav__item <?php echo $active == 'timeline' ? 'is-active' : '';?>">
            <a href="<?php echo $timelinePermalink;?>" class="es-profile-header-nav__link"><span><?php echo JText::_('COM_ES_TIMELINE');?></span></a>
        </div>

        <div class="es-profile-header-nav__item <?php echo $active == 'about' ? 'is-active' : '';?>">
            <a href="<?php echo $aboutPermalink;?>" class="es-profile-header-nav__link"><span><?php echo JText::_('COM_ES_ABOUT');?></span></a>
        </div>

        <?php if ($this->config->get('friends.enabled')) { ?>
            <div class="es-profile-header-nav__item <?php echo $active == 'friends' ? 'is-active' : '';?> <?php echo $pendingFriends > 0 ? 'has-notice' : '';?>">
                <a href="<?php echo ESR::friends(array('userid' => $user->getAlias()));?>" class="es-profile-header-nav__link">
                    <span><?php echo JText::_('COM_EASYSOCIAL_FRIENDS');?></span>
                    <span class="es-profile-header-nav__link-bubble"></span>
                </a>
            </div>
        <?php } ?>



        <?php if (!$this->isMobile()) { ?>

            <?php if ($this->config->get('followers.enabled')) { ?>
                <div class="es-profile-header-nav__item <?php echo $active == 'followers' ? 'is-active' : '';?>">
                    <a href="<?php echo ESR::followers(array('userid' => $user->getAlias()));?>" class="es-profile-header-nav__link">
                        <span><?php echo JText::_('COM_EASYSOCIAL_FOLLOWERS');?></span>
                    </a>
                </div>
            <?php } ?>



            <?php if ($user->canCreateVideos()) { ?>
                <div class="es-profile-header-nav__item <?php echo $active == 'videos' ? 'is-active' : '';?>">
                    <a href="<?php echo ESR::videos(array('uid' => $user->getAlias(), 'type' => SOCIAL_TYPE_USER));?>" class="es-profile-header-nav__link"><span><?php echo JText::_('COM_EASYSOCIAL_VIDEOS');?></span></a>
                </div>
            <?php } ?>

            <?php if ($user->canCreateAudios()) { ?>
                <div class="es-profile-header-nav__item <?php echo $active == 'audios' ? 'is-active' : '';?>">
                    <a href="<?php echo ESR::audios(array('uid' => $user->getAlias(), 'type' => SOCIAL_TYPE_USER));?>" class="es-profile-header-nav__link"><span><?php echo JText::_('COM_ES_AUDIOS');?></span></a>
                </div>
            <?php } ?>

            <?php if ($this->config->get('groups.enabled')) { ?>
                <div class="es-profile-header-nav__item <?php echo $active == 'groups' ? 'is-active' : '';?>">
                    <a href="<?php echo ESR::groups(array('userid' => $user->getAlias()));?>" class="es-profile-header-nav__link"><span><?php echo JText::_('COM_EASYSOCIAL_GROUPS');?></span></a>
                </div>
            <?php } ?>

            <?php if ($this->config->get('events.enabled')) { ?>
                <div class="es-profile-header-nav__item <?php echo $active == 'events' ? 'is-active' : '';?>">
                    <a href="<?php echo ESR::events(array('userid' => $user->getAlias()));?>" class="es-profile-header-nav__link"><span><?php echo JText::_('COM_EASYSOCIAL_EVENTS');?></span></a>
                </div>
            <?php } ?>

            <?php if ($this->config->get('pages.enabled')) { ?>
                <div class="es-profile-header-nav__item <?php echo $active == 'pages' ? 'is-active' : '';?>">
                    <a href="<?php echo ESR::pages(array('userid' => $user->getAlias()));?>" class="es-profile-header-nav__link"><span><?php echo JText::_('COM_EASYSOCIAL_PAGES');?></span></a>
                </div>
            <?php } ?>


            <?php if ($apps && $this->config->get('users.layout.sidebarapps')) { ?>
                <?php foreach ($apps as $app) {
                    if($app->id == 256 && $user->id != $viewer->id){
                        continue;
                    }
                    ?>
                    <div class="es-profile-header-nav__item <?php echo $active == 'apps.' . $app->element ? 'is-active' : '';?>">
                        <a href="<?php echo ESR::profile(array('id' => $user->getAlias(), 'appId' => $app->getAlias()));?>" class="es-profile-header-nav__link"><span><?php echo $app->getPageTitle(); ?></span></a>
                    </div>

                <?php } ?>
            <?php } ?>
        <?php } ?>


    </div>
</div>