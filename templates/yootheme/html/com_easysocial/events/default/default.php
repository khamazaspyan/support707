<?php
/**
* @package      EasySocial
* @copyright    Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="uk-container">
<?php if (!$browseView) { ?>
	<?php if ($cluster) { ?>
		<?php echo $this->html('cover.' . $cluster->getType(), $cluster, 'events'); ?>
	<?php } else { ?>
		<?php echo $this->html('cover.user', $activeUser, 'events'); ?>
	<?php } ?>
<?php } ?>

<?php echo $this->html('responsive.toggle'); ?>

<div class="es-container es-events  <?php echo $delayed ? 'is-detecting-location' : '';?>" data-es-events data-filter="<?php echo $activeCategory ? 'category' : $filter; ?>"
	data-categoryid="<?php echo $activeCategory ? $activeCategory->id : 0; ?>"
	data-es-container
>

	<?php echo $this->includeTemplate('site/events/default/sidebar'); ?>


	<div class="es-content" >

		<div class="fbstfrnds">
			<div class="fbstfrnds_hd">
				<div class="fbstfrnds_hd_title">
					<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_EVENTS'); ?>
				</div>
				<div class="fbstfrnds_hd_subtitle">
					<ul class="o-tabs o-tabs--stacked">
						<?php if ($browseView) { ?>
							<li class="o-tabs__item has-notice <?php echo $filter == 'all'? 'active' : ''; ?>" data-filter-item data-type="all">
								<a href="<?php echo $filtersLink->all; ?>"
								title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_ALL', true); ?>"
								class="o-tabs__link">
									<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_ALL'); ?>
									<span class="o-tabs__bubble" data-counter><?php echo $counters->all; ?></span>
									<div class="o-loader o-loader--sm"></div>
								</a>
							</li>

							<li class="o-tabs__item has-notice <?php echo $filter == 'featured' ? 'active' : ''; ?>" data-filter-item data-type="featured">
								<a href="<?php echo $filtersLink->featured; ?>"
								title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_FEATURED', true); ?>"
								class="o-tabs__link">
									<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_FEATURED'); ?>
									<span class="o-tabs__bubble" data-counter><?php echo $counters->featured; ?></span>
									<div class="o-loader o-loader--sm"></div>
								</a>
							</li>
						<?php } else { ?>
							<li class="o-tabs__item has-notice <?php echo $filter == 'created'? 'active' : ''; ?>" data-filter-item data-type="created">
								<a href="<?php echo $filtersLink->created; ?>"

								title="<?php echo JText::_('COM_ES_CREATED_EVENTS', true); ?>"
								class="o-tabs__link">
									<?php echo JText::_('COM_ES_CREATED_EVENTS'); ?>
									<span class="o-tabs__bubble" data-counter><?php echo $counters->created; ?></span>
									<div class="o-loader o-loader--sm"></div>
								</a>
							</li>
							<?php if (!$cluster) { ?>
							<li class="o-tabs__item has-notice <?php echo $filter == 'participated'? 'active' : ''; ?>" data-filter-item data-type="participated">
								<a href="<?php echo $filtersLink->participated; ?>"

								title="<?php echo JText::_('COM_ES_PARTICIPATED_EVENTS', true); ?>"
								class="o-tabs__link">
									<?php echo JText::_('COM_ES_PARTICIPATED_EVENTS'); ?>
									<span class="o-tabs__bubble" data-counter><?php echo $counters->participated; ?></span>
									<div class="o-loader o-loader--sm"></div>
								</a>
							</li>
							<?php } ?>
						<?php } ?>

						<?php if ($showMyEvents) { ?>
							<li class="o-tabs__item has-notice <?php echo $filter == 'mine' ? 'active' : ''; ?>" data-filter-item data-type="mine">
								<a href="<?php echo ES::event()->getFilterPermalink(array('filter' => 'mine', 'cluster' => $cluster)); ?>"
								title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_MINE', true); ?>"
								class="o-tabs__link">
									<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_MINE'); ?>
									<span class="o-tabs__bubble" data-counter><?php echo $counters->created; ?></span>
									<div class="o-loader o-loader--sm"></div>
								</a>
							</li>
						<?php } ?>

						<?php if ($showPendingEvents) { ?>
							<li class="o-tabs__item has-notice <?php echo $filter == 'review' ? 'active' : '';?>" data-filter-item data-type="review">
								<a href="<?php echo $filtersLink->pending;?>"
								title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_REVIEW', true);?>"
								class="o-tabs__link">
									<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_REVIEW');?>
									<span class="o-tabs__bubble" data-counter><?php echo $counters->totalPendingEvents;?></span>
									<div class="o-loader o-loader--sm"></div>
								</a>
							</li>
						<?php } ?>

						<?php if ($showTotalInvites) { ?>
							<li class="o-tabs__item has-notice <?php echo $filter == 'invited' ? 'active' : ''; ?>" data-filter-item data-type="invited">
								<a href="<?php echo $filtersLink->invited; ?>"
								title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_INVITED', true); ?>"
								class="o-tabs__link">
									<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_INVITED'); ?>
									<span class="o-tabs__bubble" data-counter><?php echo $counters->invited; ?></span>
									<div class="o-loader o-loader--sm"></div>
								</a>
							</li>
						<?php } ?>

						<?php if ($browseView) { ?>
							<li class="o-tabs__item has-notice <?php echo $filter == 'nearby' ? 'active' : ''; ?>" data-filter-item data-type="nearby">
								<a href="<?php echo ES::event()->getFilterPermalink(array('filter' => 'nearby', 'cluster' => $cluster)); ?>"
								title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_NEARBY', true); ?>"
								class="o-tabs__link">
									<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_NEARBY'); ?>
									<div class="o-loader o-loader--sm"></div>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="fbstfrnds_bd">
				<div class="fbstfrnds_bd1" data-wrapper>
					<?php echo $this->html('html.loading'); ?>

					<div class="es-detecting-location es-island" data-fetching-location>
						<i class="fa fa-globe fa-spin"></i> <span data-detecting-location-message><?php echo JText::_('COM_EASYSOCIAL_EVENTS_DETERMINING_LOCATION'); ?></span>
					</div>

					<?php echo $this->render('module', 'es-events-before-contents'); ?>
					<div class="events-content-wrapper" data-contents>
						<?php echo $this->includeTemplate('site/events/default/wrapper'); ?>
					</div>
					<?php echo $this->render('module', 'es-events-after-contents'); ?>

				</div>
			</div>
		</div>



	</div>
</div>
</div>
