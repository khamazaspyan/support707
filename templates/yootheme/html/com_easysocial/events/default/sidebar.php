<?php
/**
* @package      EasySocial
* @copyright    Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-sidebar" data-sidebar>

	<?php echo $this->render('module', 'es-events-sidebar-top' , 'site/dashboard/sidebar.module.wrapper'); ?>

	<?php if ((!$cluster && $this->my->canCreateEvents()) || ($cluster && $cluster->canCreateEvent())) { ?>
	<a href="<?php echo ESR::events($createUrl); ?>" class="btn btn-primary btn-create btn-block t-lg-mb--xl">
		<?php echo JText::_('COM_EASYSOCIAL_EVENTS_CREATE_EVENT'); ?>
	</a>
	<?php } ?>

	<?php if ($browseView) { ?>
		<div class="es-side-widget">
			<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_EVENTS_CALENDAR_WIDGET_TITLE'); ?>

			<div class="es-side-widget__bd">
				<div class="es-side-widget-events-calendar" data-events-calendar-wrapper>
					<?php echo $this->html('html.loading'); ?>
					<div data-events-calendar></div>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if ($browseView) { ?>
		<hr class="es-hr" />
		<div class="es-side-widget">
			<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_EVENTS_FILTER_BY_DATE_SIDEBAR_TITLE'); ?>

			<div class="es-side-widget__bd">
				<ul class="o-tabs o-tabs--stacked">
					<li class="o-tabs__item has-notice <?php echo $filter == 'date' && $activeDateFilter == 'today' ? 'active' : ''; ?>" data-filter-item data-type="today">
						<a href="<?php echo $dateLinks->today; ?>" class="o-tabs__link" title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_TODAY'); ?>">
							<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_TODAY'); ?>
						</a>
						<span class="o-tabs__bubble" data-counter><?php echo $counters->today; ?></span>
						<div class="o-loader o-loader--sm"></div>
					</li>
					<li class="o-tabs__item has-notice <?php echo $filter == 'date' && $activeDateFilter == 'tomorrow' ? 'active' : ''; ?>" data-filter-item data-type="tomorrow">
						<a href="<?php echo $dateLinks->tomorrow; ?>" title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_TOMORROW'); ?>" class="o-tabs__link">
							<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_TOMORROW'); ?>
						</a>
						<span class="o-tabs__bubble" data-counter><?php echo $counters->tomorrow; ?></span>
						<div class="o-loader o-loader--sm"></div>
					</li>

					<li class="o-tabs__item has-notice <?php echo $filter == 'week1' ? 'active' : ''; ?>" data-filter-item data-type="week1">
						<a href="<?php echo ES::event()->getFilterPermalink(array('filter' => 'week1', 'cluster' => $cluster)); ?>" title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_UPCOMING_1WEEK', true); ?>" class="o-tabs__link">
							<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_WEEK1'); ?>
						</a>
						<span class="o-tabs__bubble" data-counter><?php echo $counters->week1; ?></span>
						<div class="o-loader o-loader--sm"></div>
					</li>

					<li class="o-tabs__item has-notice <?php echo $filter == 'week2' ? 'active' : ''; ?>" data-filter-item data-type="week2">
						<a href="<?php echo ES::event()->getFilterPermalink(array('filter' => 'week2', 'cluster' => $cluster)); ?>" title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_UPCOMING_2WEEK', true); ?>" class="o-tabs__link">
							<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_WEEK2'); ?>
						</a>
						<span class="o-tabs__bubble" data-counter><?php echo $counters->week2; ?></span>
						<div class="o-loader o-loader--sm"></div>
					</li>

					<li class="o-tabs__item has-notice <?php echo $filter == 'date' && $activeDateFilter == 'month' ? 'active' : ''; ?>" data-filter-item data-type="month">
						<a href="<?php echo $dateLinks->month; ?>" class="o-tabs__link" title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_MONTH', true); ?>">
							<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_MONTH'); ?>
						</a>
						<span class="o-tabs__bubble" data-counter><?php echo $counters->month; ?></span>
						<div class="o-loader o-loader--sm"></div>
					</li>
					<li class="o-tabs__item has-notice <?php echo $filter == 'date' && $activeDateFilter == 'year' ? 'active' : ''; ?>" data-filter-item data-type="year">
						<a href="<?php echo $dateLinks->year; ?>" class="o-tabs__link" title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_YEAR', true); ?>">
							<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_YEAR'); ?>
						</a>
						<span class="o-tabs__bubble" data-counter><?php echo $counters->year; ?></span>
						<div class="o-loader o-loader--sm"></div>
					</li>
					<li class="o-tabs__item has-notice <?php echo $filter == 'past' ? 'active' : ''; ?>" data-filter-item data-type="past">
						<a class="o-tabs__link" href="<?php echo ES::event()->getFilterPermalink(array('filter' => 'past', 'cluster' => $cluster)); ?>" title="<?php echo JText::_('COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_PAST', true); ?>">
							<?php echo JText::_('COM_EASYSOCIAL_EVENTS_FILTER_PAST'); ?>
						</a>
						<span class="o-tabs__bubble" data-counter><?php echo $counters->past; ?></span>
						<div class="o-loader o-loader--sm"></div>
					</li>
				</ul>
			</div>
		</div>

		<hr class="es-hr" />
		<div class="es-side-widget">
			<?php echo $this->html('widget.title', 'COM_EASYSOCIAL_EVENTS_CATEGORIES_SIDEBAR_TITLE'); ?>

			<div class="es-side-widget__bd">
				<?php echo $this->html('cluster.categoriesSidebar', SOCIAL_TYPE_EVENT, $activeCategory) ?>
			</div>
		</div>
	<?php } ?>

	<?php echo $this->render('module', 'es-events-sidebar-bottom' , 'site/dashboard/sidebar.module.wrapper'); ?>
</div>