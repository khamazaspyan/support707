EasySocial.require()
.script("site/story/links")
.done(function($) {
	var plugin = story.addPlugin("links", {
					validateUrl: <?php echo $this->config->get('links.parser.validate') ? 'true' : 'false';?>,
					"link": {
						<?php if ($link->url) { ?>
						"title": "<?php echo ES::string()->escape($link->title);?>",
						"description": "<?php echo ES::string()->escape($link->description);?>",
						"url": "<?php echo ES::string()->escape($link->url);?>",
						"image": "<?php echo isset($link->images[0]) ? $link->images[0] : ''; ?>"
						<?php } ?>
					},
					"isEdit": <?php echo $isEdit ? 'true' : 'false'; ?>,
					"errors": {
						"messages": {
							"insert": "<?php echo JText::_('COM_ES_LINKS_STORY_PLEASE_INSERT_LINK', true);?>"
						}
					}
				});
});
