<?php
/**
 * @package      EasySocial
 * @copyright    Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
 * @license      GNU/GPL, see LICENSE.php
 * EasySocial is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-story-fastfounding-form" data-story-fastfounding-base>

    <div data-story-fastfounding-form class="t-lg-mt--md">




        <div class="o-form-group">
            <div class="o-row">
                <div class="o-col--6 t-lg-pr--md t-xs-pr--no t-xs-pb--lg">
                    <input type="text" class="o-form-control" placeholder="<?php echo JText::_('APP_PAGE_FASTFOUNDING_PRICE');?>"  data-fastfounding-price/>

                </div>


                <div  data-fastfounding-datetime-form data-yearfrom="2016" data-yearto="2100" data-allowtime="1" data-allowtimezone="0" data-dateformat="DD-MM-YYYY hh:mm A" data-disallowpast="<?php echo $disallowPast; ?>" data-minutestepping="15">
                    <div class="o-row">
                        <div style="display: none" class="o-col--6 t-lg-pr--md t-xs-pr--no t-xs-pb--lg">
                            <div id="datetimepicker4" class="o-input-group" data-fastfounding-datetime="start">
                                <input type="text" class="o-form-control" placeholder="<?php echo JText::_('FIELDS_EVENT_STARTEND_START_DATETIME'); ?>" data-picker />
                                <input type="hidden" data-datetime />
                                <span class="o-input-group__addon" data-picker-toggle>
                    <i class="fa fa-calendar"></i>
                </span>
                            </div>
                        </div>

                        <div class="o-col--6">
                            <div id="datetimepicker4" class="o-input-group" data-fastfounding-datetime-end>
                                <input type="text" class="o-form-control" placeholder="<?php echo JText::_('FIELDS_EVENT_STARTEND_END_DATETIME'); ?>" data-picker />
                                <input type="hidden" data-datetime />
                                <span class="o-input-group__addon" data-picker-toggle>
                    <i class="fa fa-calendar"></i>
                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="o-form-group">
            <input type="text" class="o-form-control" placeholder="<?php echo JText::_('APP_PAGE_FASTFOUNDING_SYORY_TITLE');?>" data-fastfounding-title />
        </div>


        <div class="o-form-group">
            <textarea name="content" id="content" class="o-form-control"  placeholder="<?php echo JText::_('APP_PAGE_FASTFOUNDING_STORY_DESCRIPTION');?>" data-fastfounding-description></textarea>
        </div>

        <input type="hidden" name="source_id" value="<?php echo $page->id;?>" data-fastfounding-source-id />
        <input type="hidden" name="source_type" value="easysocial.page" data-fastfounding-source-type />
    </div>
</div>