<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>

<div data-items>
	<?php if ($friends) { ?>
		<ul class="fbstfrnds_ul">
			<?php foreach ($friends as $friend) { ?>
				<li class="es-users-item es-island fbstfrnds_li" data-item data-id="<?php echo $friend->id;?>">
					<?php echo $this->html('listing.user', $filter == 'suggest' ? $friend->friend : $friend, $filter == 'list'); ?>
				</li>
			<?php } ?>
		</ul>
	<?php } ?>

	<?php echo $this->html('html.loading'); ?>
</div>

<div class="<?php echo !$friends ? ' is-empty' : '';?>">

	<?php if ($filter == 'pending') { ?>
		<?php echo $this->html('html.emptyBlock', 'COM_EASYSOCIAL_FRIENDS_NO_PENDING_APPROVALS', 'fa-users'); ?>
	<?php } ?>

	<?php if ($filter == 'list') { ?>
		<?php echo $this->html('html.emptyBlock', 'COM_EASYSOCIAL_FRIENDS_NO_FRIENDS_IN_LIST', 'fa-users'); ?>
	<?php } ?>

	<?php if ($filter == 'suggest') { ?>
		<?php echo $this->html('html.emptyBlock', 'COM_EASYSOCIAL_FRIENDS_REQUEST_NO_FRIEND_SUGGESTION', 'fa-users'); ?>
	<?php } ?>

	<?php if ($filter == 'all') { ?>
		<?php echo $this->html('html.emptyBlock', 'COM_EASYSOCIAL_FRIENDS_NO_FRIENDS_YET', 'fa-users'); ?>
	<?php } ?>

	<?php if ($filter == 'request') { ?>
		<?php echo $this->html('html.emptyBlock', 'COM_EASYSOCIAL_FRIENDS_NO_FRIENDS_REQUEST_SENT', 'fa-users'); ?>
	<?php } ?>

	<?php if ($filter == 'mutual') { ?>
		<?php echo $this->html('html.emptyBlock', ($user->isViewer()) ?  JText::sprintf('COM_EASYSOCIAL_FRIENDS_NO_MUTUAL_FRIENDS_WITH', $user->getName()) : JText::_('COM_EASYSOCIAL_FRIENDS_NO_MUTUAL_FRIENDS'), 'fa-users'); ?>
	<?php } ?>
</div>

<div data-pagination>
	<?php echo $pagination->getListFooter('site');?>
</div>
