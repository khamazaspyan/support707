<?php
defined('_JEXEC') or die();

$theme = JHtml::_('theme');

$site = $theme->get('site', []);

// Boxed Page Layout
$boxed = $theme->get('site.boxed', []);
$boxed_class = ['tm-page'];
$boxed_class[] = $boxed['padding'] ? 'tm-page-padding' : '';
$boxed_style[] = $boxed['media'] ? "background-image: url('{$boxed['media']}');" : '';

$user = JFactory::getUser();
$component = $_REQUEST['option'];
$social_page = 0;

if($component=='com_easysocial' || $component == 'com_easydiscuss'){
    $social_page = 1;
}
if($component!='com_easysocial' && $component!='com_easydiscuss'){
    $social_page = 0;
}

?>
<!DOCTYPE html>
<html lang="<?= $this->language ?>" dir="<?= $this->direction ?>" vocab="http://schema.org/">
    <head>

        <meta charset="<?= $this->getCharset() ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?= $theme->get('favicon') ?>">
        <link rel="apple-touch-icon-precomposed" href="<?= $theme->get('touchicon') ?>">
        <jdoc:include type="head" />
        <?php
        if($social_page==1){
            $document = JFactory::getDocument();
            $document->addScript(JUri::root(true) . '/media/com_jgive/javascript/donation_social.js');
            $document->addScript(JUri::root(true) . '/media/com_easysocial/apps/page/requestfound/popup.js');
        }
        ?>
        <script>
            jQuery(document).ready(function($) {
                if(jQuery("#es .es-profile-header").siblings('.es-container').length == 1 ||
                    jQuery("#es .albums-mini-heading").siblings('.es-container').length == 1 ){
                    jQuery("#es .es-sidebar").addClass('sidebar323');
                    jQuery("#es .es-content").addClass('es-content323');
                }

                if ( jQuery("#es").length > 0 &&
                    (jQuery("#es .es-profile-header").siblings('.es-container').length == 0
                        && jQuery("#es .es-profile-header").siblings('.es-app-wrapper').length == 0
                        && jQuery("#es .uk-container").children('.albums-mini-heading').length == 0)
                ) {
                    jQuery("#es").children(".uk-container").css('margin-top', '20px');
                }
            });
        </script>
    </head>
    <body class="<?= $theme->get('body_class')->join(' ') ?>">

        <?php if (strpos($theme->get('header.layout'), 'offcanvas') === 0 || $theme->get('mobile.animation') == 'offcanvas') : ?>
        <div class="uk-offcanvas-content">
        <?php endif ?>

        <?php if ($site['layout'] == 'boxed') : ?>
        <div<?= JHtml::_('attrs', ['class' => $boxed_class, 'style' => $boxed_style]) ?>>
            <div <?= $boxed['alignment'] ? 'class="uk-margin-auto"' : '' ?>>
        <?php endif ?>

            <div class="tm-header-mobile uk-hidden@<?= $theme->get('mobile.breakpoint') ?>">
            <?= JHtml::_('render', 'header-mobile') ?>
            </div>

            <?php if ($social_page==0 && ($this->countModules('toolbar-left') || $this->countModules('toolbar-right'))) : ?>
            <div class="tm-toolbar uk-visible@<?= $theme->get('mobile.breakpoint') ?>">
                <div class="uk-container uk-flex uk-flex-middle <?= $site['toolbar_fullwidth'] ? 'uk-container-expand' : '' ?>">

                    <?php if ($this->countModules('toolbar-left')) : ?>
                    <div>
                        <div class="uk-grid-medium uk-child-width-auto uk-flex-middle" uk-grid="margin: uk-margin-small-top">
                            <jdoc:include type="modules" name="toolbar-left" style="cell" />
                        </div>
                    </div>
                    <?php endif ?>

                    <?php if ($this->countModules('toolbar-right')) : ?>
                    <div class="uk-margin-auto-left">
                        <div class="uk-grid-medium uk-child-width-auto uk-flex-middle" uk-grid="margin: uk-margin-small-top">
                            <jdoc:include type="modules" name="toolbar-right" style="cell" />
                        </div>
                    </div>
                    <?php endif ?>

                </div>
            </div>
            <?php endif ?>

            <?= JHtml::_('render', 'header') ?>

            <jdoc:include type="modules" name="top" style="section" />

            <?php if (!$theme->get('builder') && $social_page == 0 && $component!='com_easysocial') : ?>

            <div id="tm-main" class="tm-main uk-section uk-section-default" uk-height-viewport="expand: true">
                <div class="uk-container">

                    <?php
                        $grid = ['uk-grid']; $sidebar = $theme->get('sidebar', []);
                        $grid[] = $sidebar['gutter'] ? "uk-grid-{$sidebar['gutter']}" : '';
                        $grid[] = $sidebar['divider'] ? "uk-grid-divider" : '';
                    ?>

                    <div<?= JHtml::_('attrs', ['class' => $grid, 'uk-grid' => true]) ?>>
                        <div class="uk-width-expand@<?= $theme->get('sidebar.breakpoint') ?>">

                            <?php if ($site['breadcrumbs']) : ?>
                            <div class="uk-margin-medium-bottom">
                                <?= JHtml::_('section', 'breadcrumbs') ?>
                            </div>
                            <?php endif ?>

            <?php endif ?>

            <jdoc:include type="message" />
            <jdoc:include type="component" />

            <?= JHtml::_('section', 'builder') ?>

            <?php if (!$theme->get('builder')) : ?>

                        </div>

                        <?php if ($this->countModules('sidebar')) : ?>
                        <?= JHtml::_('render', 'sidebar') ?>
                        <?php endif ?>

                    </div>

                </div>
            </div>
            <?php endif ?>

            <jdoc:include type="modules" name="bottom" style="section" />
<?php if($social_page!=1): ?>
            <?= JHtml::_('builder', $theme->get('footer.content'), 'footer') ?>
<?php endif; ?>
        <?php if ($site['layout'] == 'boxed') : ?>
            </div>
        </div>
        <?php endif ?>

        <?php if (strpos($theme->get('header.layout'), 'offcanvas') === 0 || $theme->get('mobile.animation') == 'offcanvas') : ?>
        </div>
        <?php endif ?>

        <jdoc:include type="modules" name="debug" />

    </body>
</html>
