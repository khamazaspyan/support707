EasySocial.require()
.script(
	'site/api/data',
	'site/api/admin',
	'site/api/popbox',
	'site/api/likes',
	'site/api/repost',
	'site/api/photos',
	'site/api/oauth',
	'site/api/share',
	'site/api/stream',
	'site/locations/popbox',

	// Layouts
	'shared/responsive',
	'shared/elements',
	'shared/popdown',
	'shared/privacy'
)
.library('history', 'dialog').done(function(){

	if (window.es.mobile) {
		EasySocial.require()
		.library('swiper')
		.done(function($) {

			var swiper = new Swiper('.swiper-container', {
				"freeMode": true,
				"slidesPerView": 'auto',
				"visibilityFullFit": true,
				"freeModeFluid": true,
				"slidesOffsetAfter": 88
				
			});
		});
	}
});
