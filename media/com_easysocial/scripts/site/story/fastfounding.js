EasySocial.module('site/story/fastfounding', function($) {

    var module = this;
    var lang = EasySocial.options.momentLang;

    EasySocial.require()
        .library('datetimepicker', 'moment/' + lang)
        .done(function() {

            
            EasySocial.Controller('Story.Fastfounding', {
                defaultOptions: {
                    '{base}': '[data-story-fastfounding-base]',
                    '{form}': '[data-story-fastfounding-form]',
                    '{datetimeForm}': '[data-fastfounding-datetime-form]',
                    '{datetime}': '[data-fastfounding-datetime]',
                    '{datetimeend}': '[data-fastfounding-datetime-end]',
                    '{title}': '[data-fastfounding-title]',
                    '{description}': '[data-fastfounding-description]',
                    '{picker}': '[data-picker]',
                    '{toggle}': '[data-picker-toggle]',
                    '{datetime}': '[data-datetime]',
                    '{price}': '[data-fastfounding-price]'
                }
            }, function(self, opts, base) { return {

                init: function() {
                    var minDate = new $.moment();
                    var yearto = new Date().getFullYear() + 10;
                    var dateFormat = 'DD-MM-YYYY';

                    // Minus 1 on the date to allow today
                    minDate.date(minDate.date() - 1);

                    self.picker()._datetimepicker({
                        component: "es",
                        useCurrent: false,
                        format: dateFormat,
                        minDate: minDate,
                        maxDate: new $.moment({y: yearto}),
                        icons: {
                            time: 'fa fa-time',
                            date: 'fa fa-calendar',
                            up: 'fa fa-chevron-up',
                            down: 'fa fa-chevron-down'
                        },
                        sideBySide: false,
                        pickTime: false,
                        minuteStepping: 1,
                        language: lang
                    });

                    var curActiveDateTime = self.element.data('value');
                    if (curActiveDateTime != '') {
                        var dateObj = $.moment(curActiveDateTime);
                       // self.datetimepicker('setDate', dateObj);
                    }
                    var data = self.base().htmlData();
                    opts.error = data.error || {};
                },

                datetimepicker: function(name, value) {
                    return self.picker().data('DateTimePicker')[name](value);
                },

                '{toggle} click': function() {
                    self.picker().focus();
                },

                '{picker} dp.change': function(el, ev) {
                    self.setDateValue(ev.date.toDate());

                    //self.parent.element.trigger('event' + $.String.capitalize(self.options.type), [ev.date]);
                },

                setDateValue: function(date) {
                    // Convert the date object into sql format and set it into the input
                    self.datetime().val(date.getFullYear() + '-' +
                        ('00' + (date.getMonth()+1)).slice(-2) + '-' +
                        ('00' + date.getDate()).slice(-2) + ' ' +
                        ('00' + date.getHours()).slice(-2) + ':' +
                        ('00' + date.getMinutes()).slice(-2) + ':' +
                        ('00' + date.getSeconds()).slice(-2));
                },

                '{self} datetimeExport': function(el, ev, data) {
                    data['dateend'] = self.datetime().val();
                },

                loadStoryForm: $.memoize(function(id) {
                    return EasySocial.ajax('apps/user/fastfounding/controllers/fastfounding/loadStoryForm', {
                        id: id
                    });
                }),

                '{story} save': function(element, event, save) {

                    if (save.currentPanel != 'fastfounding') {
                        return;
                    }

                    var data = {
                        title: self.title().val(),
                        description: self.description().val(),
                        price: self.price().val()
                    };
                    
                    if (self.options.allowTimezone) {
                        data.timezone = self.timezone().val()
                    }

                    self.datetime().trigger('datetimeExport', [data]);

                    self.options.name = 'fastfounding';

                    var task = save.addTask('validateFastfoundingForm');

                    self.save(task, data);
                },

                save: function(task, data) {

                    if ($.isEmpty(data.title) || $.isEmpty(data.price) || $.isEmpty(data.dateend)) {
                        return task.reject(opts.error.empty);
                    }

                    if (!$.isEmpty(data.dateend) && !$.isEmpty(data.dateend) && data.end < data.dateend) {
                        return task.reject(opts.error.datetime);
                    }
                    task.save.addData(self, data);
                    task.resolve();
                }
            }});


            module.resolve();
        });
});
