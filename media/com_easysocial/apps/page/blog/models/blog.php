<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

ES::import('admin:/includes/model');

class BlogModel extends EasySocialModel
{
	public function exists()
	{
		jimport('joomla.filesystem.file');

		$file = JPATH_ADMINISTRATOR . '/components/com_easyblog/includes/easyblog.php';

		if (JFile::exists($file)) {
			require_once($file);
			return true;
		}

		return false;
	}

	/**
	 * Retrieves a list of blog posts created in a page
	 *
	 * @since	5.0
	 * @access	public
	 */
	public function getItems($pageId = null, $options = array())
	{
		if (!$this->exists()) {
			return array();
		}
		
		$db = ES::db();
		$query = array();

		$query[] = 'SELECT * FROM ' . $db->qn('#__easyblog_post');
		$query[] = 'WHERE ' . $db->qn('source_type') . '=' . $db->Quote('easysocial.page');
		$query[] = 'AND ' . $db->qn('source_id') . '=' . $db->Quote($pageId);
		$query[] = 'AND ' . $db->qn('published') . '=' . $db->Quote(EASYBLOG_POST_PUBLISHED);
		$query[] = 'AND ' . $db->qn('state') . '=' . $db->Quote(EASYBLOG_POST_NORMAL);

		$query[] = 'ORDER BY ' . $db->qn('created') . ' DESC';

		if (isset($options['limit'])) {
			// Set total
			$this->setTotal(implode(' ', $query), true);
			$this->setState('limit', $options['limit']);
			$this->setState('limitstart', $this->getUserStateFromRequest('limitstart', 0));

			// $query[] = 'LIMIT '. $options['limit'];
		}
		
		$query = implode(' ', $query);

		// $db->setQuery($query);

		// $result = $db->loadObjectList();

		$result = $this->getData($query);

		if (!$result) {
			return $result;
		}

		// Format the blog post
		$result = EB::formatter('list', $result);

		return $result;
	}

    public function getItem($pageId = null, $options = array(), $filter = null)
    {
        if (!$this->exists()) {
            return array();
        }

        $db = ES::db();
        $query = array();

        $query[] = 'SELECT * FROM ' . $db->qn('#__easyblog_post');
        $query[] = 'WHERE ' . $db->qn('source_type') . '=' . $db->Quote('easysocial.page');
        $query[] = 'AND ' . $db->qn('source_id') . '=' . $db->Quote($pageId);
        $query[] = 'AND ' . $db->qn('id') . '=' . $db->Quote($filter);
        $query[] = 'AND ' . $db->qn('published') . '=' . $db->Quote(EASYBLOG_POST_PUBLISHED);
        $query[] = 'AND ' . $db->qn('state') . '=' . $db->Quote(EASYBLOG_POST_NORMAL);

        $query[] = 'ORDER BY ' . $db->qn('created') . ' DESC';

        if (isset($options['limit'])) {
            // Set total
            $this->setTotal(implode(' ', $query), true);
            $this->setState('limit', $options['limit']);
            $this->setState('limitstart', $this->getUserStateFromRequest('limitstart', 0));

            // $query[] = 'LIMIT '. $options['limit'];
        }

        $query = implode(' ', $query);

        // $db->setQuery($query);

        // $result = $db->loadObjectList();

        $result = $this->getData($query);

        if (!$result) {
            return $result;
        }

        // Format the blog post
        $result = EB::formatter('list', $result);

        return $result;
    }
}