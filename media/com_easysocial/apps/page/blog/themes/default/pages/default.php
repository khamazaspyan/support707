<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-container" data-blog>


	<div class="es-content">
		<div class="app-contents<?php echo !$posts ? ' is-empty' : '';?>" data-blog-posts>
			<div class="fbstfrnds">
				<div class="fbstfrnds_bd">
					<div class="fbstfrnds_bd1">
						<div class="app-contents-data">
							<div class="" data-blog-lists>
								<?php if ($posts) { ?>
									<?php foreach ($posts as $post) { ?>
										<?php echo $this->loadTemplate('themes:/apps/page/blog/pages/item', array('post' => $post, 'page' => $page, 'my' => $my, 'return' => $return)); ?>
									<?php } ?>
								<?php } ?>
							</div>
						</div>

						<div class="<?php echo !$posts ? 'is-empty' : '';?>">
							<div class="o-empty es-island">
								<div class="o-empty__content">
									<i class="o-empty__icon fa fa-book"></i>
									<div class="o-empty__text"><?php echo JText::_('APP_PAGE_BLOG_NO_POSTS'); ?></div>
									<div class="o-empty__action">
										<a class="btn btn-es-primary btn-sm " href="<?php echo $composeLink;?>" data-eb-composer><?php echo JText::_('APP_USER_BLOG_NEW_POST_BUTTON'); ?></a>
									</div>
								</div>
							</div>
						</div>

						<div class="mt-20 pagination-wrapper text-center">
							<?php echo $pagination->getListFooter( 'site' ); ?>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>