<?php
/**
 * @package      EasyBlog
 * @copyright    Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
 * @license      GNU/GPL, see LICENSE.php
 * EasySocial is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-apps-item es-island es-stream-apps es-stream-apps__hd">
    <div class="es-apps-item__hd es-stream-meta">
        <div class="es-stream-title"><a href="<?php echo $page->getPermalink() . $post->url;?>" class="es-apps-item__title"><?php echo $post->title; ?></a></div>

        <?php if ($page->isAdmin() || $my->isSiteAdmin() || $post->created_by == $this->my->id) { ?>
            <div class="es-apps-item__action">
                <div class="btn-group">
                    <button type="button" class="dropdown-toggle_ btn btn-es-default-o btn-xs" data-bs-toggle="dropdown">
                        <i class="fa fa-caret-down"></i>
                    </button>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="<?php echo EB::composer()->getComposeUrl(array('uid' => $post->id . '.' . $post->revision_id, 'return' => $return));?>"><?php echo JText::_('APP_PAGE_BLOG_EDIT');?></a>
                        </li>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="es-apps-item__ft">
        <div class="o-grid">
            <div class="o-grid__cell">
                <div class="es-apps-item__meta">
                    <div class="es-apps-item__meta-item">
                        <ol class="g-list-inline g-list-inline--dashed">
                            <li>
                                <i class="fa fa-calendar"></i>&nbsp; <?php echo $this->html('string.date', $post->created, JText::_('DATE_FORMAT_LC1')); ?>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="es-apps-item__bd">

        <div class="o-grid-sm">
            <div class="o-grid-sm__cell">
                <div class="es-apps-item__desc">
                    <?php echo $post->content;?>
                </div>
            </div>


        </div>
        
    </div>

</div>