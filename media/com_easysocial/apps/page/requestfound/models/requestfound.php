<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

ES::import('admin:/includes/model');

class RequestfoundModel extends EasySocialModel
{
	public function exists()
	{
		jimport('joomla.filesystem.file');

		$file = JPATH_ADMINISTRATOR . '/components/com_easyblog/includes/easyblog.php';

		if (JFile::exists($file)) {
			require_once($file);
			return true;
		}

		return false;
	}

	/**
	 * Retrieves a list of blog posts created in a page
	 *
	 * @since	5.0
	 * @access	public
	 */
	public function getItems($pageId = null, $options = array())
	{

		if (!$this->exists()) {
			return array();
		}

        $db = JFactory::getDbo();

        $query = "SELECT s.id,i.context_id,c.* FROM #__social_stream as s
                  INNER JOIN #__social_stream_item as i
                  ON s.id = i.uid
                  INNER JOIN #__jg_campaigns as c
                  on i.context_id = c.id
                  WHERE s.context_type='requestfound' AND s.cluster_id = '".$pageId."'";
        $result = $db->setQuery($query)->loadObjectList();


		return $result;
	}
}