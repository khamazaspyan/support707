<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>

<div class="es-container" data-requestfound>
    <div class="es-sidebar-sticky" style="padding-top: 0px;">
	<div class="es-sidebar sidebar323" style="padding-top: 0px;" data-sidebar="">

		<div class="es-side-widget">
			<ul id="requests_filter" class="o-tabs o-tabs--stacked feed-items" data-dashboard-feeds="">

				<li class="o-tabs__item  active" data-list="0">
					<a href="javascript:void(0);" class="o-tabs__link">
						All Requests
						<div style="display: block;" class="o-tabs__bubble" data-counter=""><?php echo sizeof($posts); ?></div>
					</a>
					<div class="o-loader o-loader--sm"></div>
				</li>

				<li class="o-tabs__item" data-list="1">
					<a href="javascript:void(0);" class="o-tabs__link">
						Finished Requests
						<div style="display: block;" class="o-tabs__bubble" data-counter=""><?php echo $finished; ?></div>
					</a>
					<div class="o-loader o-loader--sm"></div>
				</li>

				<li class="o-tabs__item " data-list="2">
					<a href="javascript:void(0);" class="o-tabs__link">Not Finished Requests
						<div style="display: block;" class="o-tabs__bubble" data-counter=""><?php echo $not_finished; ?></div>
					</a>
					<div class="o-loader o-loader--sm"></div>
				</li>

			</ul>
		</div>

		<div class="es-side-widget is-module">
			<div class="es-side-widget__hd">
				<div class="es-side-widget__title">
					Donors
					<span>(<?php echo sizeof($donors); ?>)</span>
				</div>
			</div>

			<div class="es-side-widget__bd">
				<ul class="g-list-inline">
					<?php foreach($donors as $donor): ?>
						<li class="t-lg-mb--md t-lg-mr--md">
							<?php echo $this->html('avatar.user', ES::user($donor), 'md'); ?>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>


		</div>

	</div>
    </div>
	<div class="es-content">

		<div class="app-contents<?php echo !$posts ? ' is-empty' : '';?>" data-requestfound-posts>
			<div class="app-contents-data">
				<div class="" data-requestfound-lists>
					<?php if ($posts) { ?>
						<?php foreach ($posts as $post) { ?>
							<div style="margin-top: 0px;   margin-bottom: 10px; " class="es-stream-apps type-requestfound <?php echo $post->class_filrer ?>">
								<div class="es-stream-apps__hd">
									<div class="es_reuqest_found_title_panel">
										<a href="<?php echo $post->link; ?>" target="_blank" class="es-stream-apps__title">
											<?php echo $post->title; ?>
										</a>
									</div>
									<div class="es-stream-apps__bd es-stream-apps--border es-requestfound-content">
										<div class="es-stream-apps__desc">
											<?php echo strip_tags($post->short_description);?>
										</div>
									</div>
									<div class="es-stream-apps__meta t-fs--sm">
										<div class="es-request_found_compaign_details">
											<div class="after">
												<div class="price">
													<div class="raised">Raised<br><span>$<?php echo $post->raised; ?></span></div>
													<div class="goal">Goal<br><span>$<?php echo $post->goal_amount; ?></span></div>
												</div>
												<div class="donors">
													<div class="number"><?php echo $post->total_donors; ?></div>
													<div class="title">Donors</div>
												</div>
												<div class="days">
													<div class="number"><?php echo $post->diff; ?></div>
													<div class="title">Days left</div>
												</div>
												<div class="donated">
													<div class="c100 p<?php echo $post->amount_precent; ?>">
														<span><?php echo $post->amount_precent; ?>%</span>
														<div class="slice">
															<div class="bar"></div>
															<div class="fill"></div>
														</div>
														<span class="donated_txt_1">Donated</span>
													</div>
												</div>
												<div class="slide">
												</div>
											</div>
											<div class="donate_now"><button data-amount-raised="<?php echo $post->raised; ?>" data-amount-goal="<?php echo $post->goal_amount; ?>" data-precent="<?php echo $post->amount_precent; ?>" data-donors="<?php echo $post->total_donors; ?>" data-days="<?php echo $post->diff; ?>" data-title="<?php echo $post->title; ?>" data-id="<?php echo $post->id; ?>" data-donate-action>Donate Now!</button></div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>

			<div class="<?php echo !$posts ? 'is-empty' : '';?>">
				<div class="o-empty es-island">
					<div class="o-empty__content">
						<i class="o-empty__icon fa fa-book"></i>
						<div class="o-empty__text"><?php echo JText::_('APP_PAGE_REQUESTFOUNDS_NO_POSTS'); ?></div>

					</div>
				</div>
			</div>

			<div class="mt-20 pagination-wrapper text-center">
				<?php echo $pagination->getListFooter( 'site' ); ?>
			</div>
		</div>
	</div>
</div>

<script>
	jQuery('#requests_filter li').click(function(){
		var filter = jQuery(this).attr('data-list');
		if(filter == 1){
			jQuery(this).addClass('active');
			jQuery(this).siblings('li').removeClass('active');
			jQuery('.app-contents-data .type-requestfound.filter_1').show();
			jQuery('.app-contents-data .type-requestfound.filter_2').hide();
		}
		if(filter == 2){
			jQuery(this).addClass('active');
			jQuery(this).siblings('li').removeClass('active');
			jQuery('.app-contents-data .type-requestfound.filter_2').show();
			jQuery('.app-contents-data .type-requestfound.filter_1').hide();
		}
		if(filter == 0){
			jQuery(this).addClass('active');
			jQuery(this).siblings('li').removeClass('active');
			jQuery('.app-contents-data .type-requestfound.filter_1').show();
			jQuery('.app-contents-data .type-requestfound.filter_2').show();
		}
	});
</script>