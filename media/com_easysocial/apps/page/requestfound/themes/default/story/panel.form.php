<?php
/**
* @package      EasySocial
* @copyright    Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-story-requestfound-form" data-story-requestfound-form>
	<div class="o-form-group mb-0" style="display: none">
		<input type="hidden" value="9"   data-story-requestfound-category/>

	</div>
	<div data-story-requestfound-form class="t-lg-mt--md">
        <div class="o-form-group mb-0">
            <select name="categoryid" id="categoryid" class="form-control" data-story-requestfound-type="">
                <option value=""  selected="selected">Select Type</option>
                <?php foreach($page->d_types as $d_type): ?>
                <option value="<?php echo $d_type->id; ?>"><?php echo $d_type->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="o-form-group">
            <textarea style="resize: none;" name="content" maxlength="120" id="content" class="o-form-control"  placeholder="<?php echo JText::_('APP_PAGE_FASTFOUNDING_STORY_DESCRIPTION');?>" data-requestfound-content></textarea>
            <div id="content_max_count" class="content_max_count"></div>
        </div>

		<div class="o-form-group">
			<div class="o-row">
				<div class="o-col--6 t-lg-pr--md t-xs-pr--no t-xs-pb--lg">
					<input type="number" class="o-form-control" placeholder="<?php echo JText::_('APP_PAGE_FASTFOUNDING_PRICE');?>"  data-requestfound-price/>

				</div>


				<div  data-requestfound-datetime-form data-yearfrom="2016" data-yearto="2100" data-allowtime="1" data-allowtimezone="0" data-dateformat="DD-MM-YYYY hh:mm A" data-disallowpast="<?php echo $disallowPast; ?>" data-minutestepping="15">
					<div class="o-row">
						<div style="display: none" class="o-col--6 t-lg-pr--md t-xs-pr--no t-xs-pb--lg">
							<div id="datetimepicker4" class="o-input-group" data-fastfounding-datetime="start">
								<input type="text" class="o-form-control" placeholder="<?php echo JText::_('FIELDS_EVENT_STARTEND_START_DATETIME'); ?>" data-picker />
								<input type="hidden" data-datetime />
								<span class="o-input-group__addon" data-picker-toggle>
                    <i class="fa fa-calendar"></i>
                </span>
							</div>
						</div>

						<div class="o-col--6">
							<div id="datetimepicker4" class="o-input-group" data-fastfounding-datetime-end>
								<input type="text" class="o-form-control" placeholder="<?php echo JText::_('FIELDS_EVENT_STARTEND_END_DATETIME'); ?>" data-picker />
								<input type="hidden" data-datetime />
								<span class="o-input-group__addon" data-picker-toggle>
                    <i class="fa fa-calendar"></i>
                </span>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="o-form-group">
			<input type="text" class="o-form-control" placeholder="<?php echo JText::_('APP_PAGE_FASTFOUNDING_SYORY_TITLE');?>" data-requestfound-title />
		</div>



		<input type="hidden" name="source_id" value="<?php echo $page->id;?>" data-requestfound-source-id />
		<input type="hidden" name="paypalEmail" value="<?php echo $page->paypal;?>" data-story-requestfound-paypalemail/>
		<input type="hidden" name="source_type" value="easysocial.page" data-requestfound-source-type />
	</div>
</div>
<style>
	#content_max_count{
		position: absolute;
		right: 7px;
		top: 26px;
		width: 26px;
		height: 26px;
		text-align: center;
		border-radius: 50%;
		border: 1px solid #e9ebee;
		padding: 2px 0;
		color: #5fc6c2;
	}
	#content_max_count.oops{
		color: #e61212;
		border-color: #5fc6c2;
	}
    #es .plugin-requestfound .es-story-text{
        display: none;
    }
</style>