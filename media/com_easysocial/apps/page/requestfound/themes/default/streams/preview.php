<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-stream-apps type-requestfound <?php echo $this->isMobile() ? 'is-mobile' : 'is-desktop';?>">

	<div class="es-stream-apps__hd">
		<div class="es_reuqest_found_title_panel">
			<span><?php echo $post->title; ?></span>
		</div>
		<div class="es-stream-apps__bd es-stream-apps--border es-requestfound-content">
			<div class="es-stream-apps__desc">
				<?php echo strip_tags($content);?>
			</div>
		</div>
		<div class="es-stream-apps__meta t-fs--sm">
			<div class="es-request_found_compaign_details">
				<div class="after">
					<div class="price">
						<div class="raised">Raised<br><span>$<?php echo $post->raised; ?></span></div>
						<div class="goal">Goal<br><span>$<?php echo $post->goal_amount; ?></span></div>
					</div>
					<div class="donors">
						<div class="number"><?php echo $post->total_donors; ?></div>
						<div class="title">Donors</div>
					</div>
					<div class="days">
						<div class="number"><?php echo $post->diff; ?></div>
						<div class="title">Days left</div>
					</div>
					<div class="donated">
						<div class="c100 p<?php echo $post->amount_precent; ?>">
							<span><?php echo $post->amount_precent; ?>%</span>
							<div class="slice">
								<div class="bar"></div>
								<div class="fill"></div>
							</div>
							<span class="donated_txt_1">Donated</span>
						</div>
					</div>
					<div class="slide">
					</div>
				</div>
				<div class="donate_now"><button data-clusterid="<?php echo $post->page->id; ?>" data-amount-raised="<?php echo $post->raised; ?>" data-amount-goal="<?php echo $post->goal_amount; ?>" data-precent="<?php echo $post->amount_precent; ?>" data-donors="<?php echo $post->total_donors; ?>" data-days="<?php echo $post->diff; ?>" data-title="<?php echo $post->title; ?>" data-id="<?php echo $post->id; ?>" data-donate-action>Donate Now!</button></div>
			</div>
		</div>
	</div>
</div>
