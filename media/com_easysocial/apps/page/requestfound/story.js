EasySocial.module("story/requestfound", function($){

    var module = this;
    var lang = EasySocial.options.momentLang;



    maxCharacters = 120;

    $('#content_max_count').text(maxCharacters);

    $('textarea#content').bind('keyup keydown', function() {
        var count = $('#content_max_count');
        var characters = $(this).val().length;
        if (characters == maxCharacters) {
            $('#content_max_count').addClass('oops');
        } else {
            $('#content_max_count').removeClass('oops');
        }

        $('#content_max_count').text(maxCharacters - characters);
    });

    EasySocial.require()
        .library('datetimepicker', 'moment/' + lang)
        .done(function() {

    EasySocial.Controller("Story.Requestfound", {
            defaultOptions: {
                "{title}" : "[data-requestfound-title]",
                "{content}" : "[data-requestfound-content]",
                "{category}": "[data-story-requestfound-category]",
                "{sourceId}": "[data-requestfound-source-id]",
                '{picker}': '[data-picker]',
                '{toggle}': '[data-picker-toggle]',
                '{datetime}': '[data-datetime]',
                '{price}': '[data-requestfound-price]',
                "{sourceType}": "[data-requestfound-source-type]",
                "{paypalEmail}": "[data-story-requestfound-paypalemail]",
                "{type}": "[data-story-requestfound-type]"
            }
        }, function(self) {
            return {

            init: function() {
                var minDate = new $.moment();
                var yearto = new Date().getFullYear() + 10;
                var dateFormat = 'DD-MM-YYYY';

                // Minus 1 on the date to allow today
                minDate.date(minDate.date() - 1);

                self.picker()._datetimepicker({
                    component: "es",
                    useCurrent: false,
                    format: dateFormat,
                    minDate: minDate,
                    maxDate: new $.moment({y: yearto}),
                    icons: {
                        time: 'fa fa-time',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down'
                    },
                    sideBySide: false,
                    pickTime: false,
                    minuteStepping: 1,
                    language: lang
                });

                var curActiveDateTime = self.element.data('value');
                if (curActiveDateTime != '') {
                    var dateObj = $.moment(curActiveDateTime);
                    // self.datetimepicker('setDate', dateObj);
                }
            },

                datetimepicker: function(name, value) {
                    return self.picker().data('DateTimePicker')[name](value);
                },

                '{toggle} click': function() {
                    self.picker().focus();
                },

                '{picker} dp.change': function(el, ev) {
                    self.setDateValue(ev.date.toDate());

                    //self.parent.element.trigger('event' + $.String.capitalize(self.options.type), [ev.date]);
                },

                setDateValue: function(date) {
                    // Convert the date object into sql format and set it into the input
                    self.datetime().val(date.getFullYear() + '-' +
                        ('00' + (date.getMonth()+1)).slice(-2) + '-' +
                        ('00' + date.getDate()).slice(-2) + ' ' +
                        ('00' + date.getHours()).slice(-2) + ':' +
                        ('00' + date.getMinutes()).slice(-2) + ':' +
                        ('00' + date.getSeconds()).slice(-2));
                },

                '{self} datetimeExport': function(el, ev, data) {
                    data['dateend'] = self.datetime().val();
                },

            resetForm: function() {
                self.title().val('');
                self.content().val('');
                self.price().val('');
                self.datetime().val('');
            },

            "{story} save": function(element, event, save) {

                if (save.currentPanel != 'requestfound') {
                    return;
                }

                // Add the task for uploading video
                self.savePost = save.addTask('savePost');

                self.save(save);
            },

            save: function(save) {

                var savePost = self.savePost;


                if (!savePost) {
                    return;
                }

                var paypalEmail = self.paypalEmail().val();

                if (!paypalEmail) {
                    self.clearMessage();
                    save.reject('<?php echo $errorPayPalEmail;?>');
                    return false;
                }

                var type = self.type().val();

                if (!type) {
                    self.clearMessage();
                    save.reject('<?php echo $errorType;?>');
                    return false;
                }

                var title = self.title().val();

                if (!title) {
                    self.clearMessage();
                    save.reject('<?php echo $errorTitle;?>');
                    return false;
                }

                var content = self.content().val();

                if (!content) {
                    self.clearMessage();
                    save.reject('<?php echo $errorContent;?>');
                    return false;
                }

                var price = self.price().val();

                if (!price) {
                    self.clearMessage();
                    save.reject('<?php echo $errorPrice;?>');
                    return false;
                }

                var datetime = self.datetime().val();

                if (!datetime) {
                    self.clearMessage();
                    save.reject('<?php echo $errorDatetime;?>');
                    return false;
                }

                // Perform validation checks here.
                var data = {
                    "categoryId": self.category().val(),
                    "title": self.title().val(),
                    "content": self.content().val(),
                    "sourceId": self.sourceId().val(),
                    "price": self.price().val(),
                    "date": self.datetime().val(),
                    "paypalEmail": self.paypalEmail().val(),
                    "type": self.type().val(),
                    "sourceType": self.sourceType().val()
                };

                self.resetForm();

                save.addData(self, data);

                savePost.resolve();

                delete self.savePost;

            }
        }}
    );

    // Resolve module
    module.resolve();
        });
});


EasySocial.require()
.script("story/requestfound")
.done(function($) {
    var plugin = story.addPlugin("requestfound");
});

