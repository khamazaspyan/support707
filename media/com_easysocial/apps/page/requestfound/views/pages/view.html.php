<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

class RequestfoundViewPages extends SocialAppsView
{
	public function exists()
	{
		$file = JPATH_ROOT . '/administrator/components/com_easyblog/includes/easyblog.php';

		if (!JFile::exists($file)) {
			return false;
		}

		require_once($file);

		return true;
	}

	public function display($pageId = null, $docType = null)
	{
		if (!$this->exists()) {
			return;
		}

        $db = JFactory::getDbo();

		$params = $this->app->getParams();

		// @since 4.0
		// Attach the theme's css
		$config = EB::config();

		$stylesheet = EB::stylesheet('site', $config->get('theme_site'));
		$stylesheet->attach();

		// Render the scripts on the page.
		EB::init('composer');

		// Load easyblog's language file
		EB::loadLanguages();

		// Get the current logged in user.
		$my = ES::user();
		
		// Load up the group
		$page = ES::page($pageId);

		$limit = (int) $params->get('listing_total', 5);
		$options = array('limit' => $limit);
		
		// Retrieve the model
		$model = $this->getModel('Requestfound');
		$posts = $model->getItems($page->id, $options);
        $appParams = $this->getParams();
        $contentLength  = $appParams->get('maxlength');
        $donors = [];
        $finished = 0;
        $not_finished = 0;
		if ($posts) {
			foreach ($posts as $post) {
                $amount_query = "SELECT SUM(amount) as amount FROM #__jg_orders WHERE campaign_id='".$post->id."' && status='C'";
                $post->raised = $db->setQuery($amount_query)->loadResult();
                $post->raised = ($post->raised)? $post->raised : 0;
                if ($contentLength > 0) {
                    // truncate the content
                    $post->short_description = $this->truncateStreamContent($post->short_description, $contentLength);
                }
                $total_donors_query = "SELECT COUNT(id) as orders FROM #__jg_orders WHERE campaign_id='".$post->id."' && status='C'";
                $post->total_donors = $db->setQuery($total_donors_query)->loadResult();

                $donors_query = "SELECT b.user_id FROM #__jg_orders as a
                                 INNER JOIN #__jg_donors as b ON a.donor_id = b.id
                                 INNER JOIN #__jg_donations as j ON a.donor_id = j.donor_id
                                 WHERE a.campaign_id = '".$post->id."' AND status='C' AND j.annonymous_donation = '0'
                                 GROUP BY b.user_id";
                $donor = $db->setQuery($donors_query)->loadObjectList();
                array_push($donors,$donor);

                $post->amount_precent = ($post->raised*100)/$post->goal_amount ;
                $post->amount_precent = (int)$post->amount_precent;

                $date1 = date_create($post->stard_date);
                $date2 = date_create($post->end_date);
                $diff=date_diff($date1,$date2);
                $post->diff = $diff->format("%a");
                if($diff->invert == 1){
                    $post->diff = 0;
                    $finished ++ ;
                    $post->class_filrer = "filter_1";
                }
                else{
                    $not_finished++;
                    $post->class_filrer = "filter_2";
                }


                $post->link = '/index.php/component/jgive/'.$post->id.'/details/campaign?Itemid=0';
                $post->donation_link = '/index.php/component/jgive/donations/payment/'.$post->id.'?Itemid=0';
			}
		}
		$donors_unic = [];
		foreach ($donors as $donor){
            foreach ($donor as $don){
                array_push($donors_unic,$don->user_id);
            }
        }
        $donors_unic = array_unique($donors_unic);
		$pagination = $model->getPagination();
		$pagination->setVar('option' , 'com_easysocial');
		$pagination->setVar('view' , 'pages');
		$pagination->setVar('layout' , 'item');
		$pagination->setVar('id' , $page->getAlias());
		$pagination->setVar('appId' , $this->app->getAlias());        

		// Generate the return url
		$return = ESR::pages(array('layout' => 'item', 'id' => $page->getAlias(), 'appId' => $this->app->getAlias()));
		$return = base64_encode($return);

		$composeLink = EB::composer()->getComposeUrl(array('source_id' => $page->id, 'source_type' => 'easysocial.page', 'returnUrl' => $return, 'return' => $return));

		$this->set('return', $return);
		$this->set('composeLink', $composeLink);
		$this->set('posts', $posts);
		$this->set('donors', $donors_unic);
		$this->set('finished', $finished);
		$this->set('not_finished', $not_finished);
		$this->set('page', $page);
		$this->set('params', $params);
		$this->set('my', $page);
		$this->set('pagination', $pagination);

		echo parent::display('pages/default');
	}

	/**
	 * Truncate the stream item
	 *
	 * @since   1.0
	 * @access  public
	 */
	public function truncateStreamContent($content, $contentLength)
	{
		// Get the app params
		$params = $this->getParams();
		$truncateType = $params->get('truncation');

		if ($truncateType == 'chars') {

			// Remove uneccessary html tags to avoid unclosed html tags
			$content = strip_tags($content);

			// Remove blank spaces since the word calculation should not include new lines or blanks.
			$content = trim($content);

			// @task: Let's truncate the content now.
			$content = JString::substr(strip_tags($content), 0, $contentLength);

		} else {

			$tag = false;
			$count = 0;
			$output = '';

			// Remove uneccessary html tags to avoid unclosed html tags
			$content = strip_tags($content);

			$chunks = preg_split("/([\s]+)/", $content, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

			foreach($chunks as $piece) {

				if (!$tag || stripos($piece, '>') !== false) {
					$tag = (bool) (strripos($piece, '>') < strripos($piece, '<'));
				}

				if (!$tag && trim($piece) == '') {
					$count++;
				}

				if ($count > $contentLength && !$tag) {
					break;
				}

				$output .= $piece;
			}

			unset($chunks);
			$content = $output . JText::_('COM_EASYSOCIAL_ELLIPSES');
		}


		return $content;
	}
}
