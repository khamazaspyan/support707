<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

ES::import('admin:/includes/apps/apps');

class SocialPageAppRequestfound extends SocialAppItem
{
	/**
	 * Check if EasyBlog exists on the site.
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function exists()
	{
		$file = JPATH_ROOT . '/administrator/components/com_easyblog/includes/easyblog.php';

		if (!JFile::exists($file)) {
			return false;
		}

		require_once($file);

		return true;
	}

	/**
	 * Prepares the story panel for page story form
	 *
	 * @since	2.0
	 * @access	public
	 */
	public function onPrepareStoryPanel($story)
	{
		if (!$this->exists()) {
			return;
		}

		// Ensure that it's a page cluster
		if ($story->clusterType != SOCIAL_TYPE_PAGE) {
			return;
		}

        $page = ES::page($story->cluster);

        if (!$page->canCreateFastfounding() || !$page->canAccessFastdfounding()) {
            return;
        }

		// check for acl to see if this user has the acl to blog or not
		$acl = EB::acl();

		if (!$acl->get('add_entry')) {
			return;
		}

		// Load the page

		$params = $this->getParams();

		// Create plugin object
		$plugin = $story->createPlugin('requestfound', 'panel');
        $this->getApp()->loadCss();

		// Get the theme class
		$theme = ES::themes();

		// Get the available blog category
		$model = EB::model('Category');
		$defaultCategory = $model->getDefaultCategory();
		$categories = EB::populateCategories('', '', 'select', 'categoryid', $defaultCategory->id, false, true, true, array(), 'data-story-requestfound-category', 'APP_PAGE_BLOG_SELECT_A_CATEGORY');

        $db = JFactory::getDbo();

        $paypalEmail = $db->setQuery("SELECT raw FROM #__social_fields_data WHERE field_id='390' AND type='page' AND uid='".$page->id."'")->loadResult();

        $page->paypal = $paypalEmail;

        $page->d_types = $db->setQuery("SELECT id,name FROM #__0_donation_types WHERE status = '1'")->loadObjectList();

		$theme->set('page', $page);
		$theme->set('categories', $categories);

		$plugin->button->html = $theme->output('apps/page/requestfound/story/panel.button');
		$plugin->content->html = $theme->output('apps/page/requestfound/story/panel.form');

		$script = ES::get('Script');
		$script->set('errorTitle', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_TITLE'));
		$script->set('errorContent', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_CONTENT'));
		$script->set('errorPrice', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_PRICE'));
		$script->set('errorDatetime', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_DATE'));
		$script->set('errorPayPalEmail', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_PAYPAL'));
		$script->set('errorType', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_TYPE'));
		$plugin->script = $script->output('apps:/page/requestfound/story');

		return $plugin;
	}

	/**
	 * Before saving the blog post, perform validation checks here.
	 *
	 * @since	2.0
	 * @access	public
	 */
	public function onBeforeStorySave(&$template, &$stream, &$content)
	{

		if (!$this->exists() || $template->context_type != 'requestfound') {
			return;
		}

		$author = ES::user();

		// Retrieve the post data
		$title = addslashes($this->input->get('requestfound_title', '', 'default'));
		$content = addslashes($this->input->get('requestfound_content', '', 'default'));
		$category = $this->input->get('requestfound_categoryId', 0, 'int');
		$sourceId = $this->input->get('requestfound_sourceId', 0, 'int');
		$sourceType = $this->input->get('requestfound_sourceType', '', 'default');
		$price = $this->input->get('requestfound_price', '', 'default');
        $paypalEmail = $this->input->get('requestfound_paypalEmail', '', 'default');
        $type = $this->input->get('requestfound_type', '', 'default');
		$date = $this->input->get('requestfound_date', '', 'default');

		if (!$title) {
			return false;
		}

		$post = EB::post();

		$data = new stdClass();
		$data->title = $title;
		$data->content = $content;
		$data->category_id = $category;
		$data->created_by = $author->id;
		$data->page_id = $sourceId;
		$data->price = sprintf("%.2f", $price);
		$data->date = $date;
		$data->paypalEmail = $paypalEmail;
		$data->type = $type;

		// Get the current date
		$current = ES::date();

		$data->published = 1;
		$data->created = $current->toSql();
		$data->modified = $current->toSql();
		$data->publish_up = $current->toSql();
		$data->publish_down	= '0000-00-00 00:00:00';
		$data->frontpage = 1;
		$data->allowcomment = 1;
		$data->subscription = 1;
		$data->source_id = $sourceId;
		$data->source_type = $sourceType;
        $now_time = date("Y-m-d,h:m:s");

        $db = JFactory::getDbo();


        $insert_jgive_quert ="INSERT INTO `#__jg_campaigns`
(`category_id`, `org_ind_type`, `creator_id`, `title`, `created`, `modified`, `type`, `max_donors`, `minimum_amount`, `short_description`, `long_description`, `goal_amount`,`start_date`, `end_date`, `allow_exceed`, `allow_view_donations`, `published`, `internal_use`, `featured`, `js_groupid`, `success_status`, `processed_flag`, `video_on_details_page`, `meta_data`, `meta_desc`,`paypal_email`, `donation_type`) VALUES 

('".$data->category_id."','individuals','".$data->created_by."','".$data->title."','".$now_time."','0000-00-00 00:00:00','donation','0','0','".$data->content."','','".$data->price."','".$now_time."','".$data->date."','1','1','1','','0','0','0','','0','','','".$data->paypalEmail."', '".$data->type."')";

        $db->setQuery($insert_jgive_quert)->query();
        $insertedCampaignId = $db->insertid();


        $insertQuery = "INSERT INTO `#__social_request_found`
(`page_id`, `title`, `price`, `end`, `description`,`campaign_id`,`user_id`) VALUES 
('".$data->page_id."','".$data->title."','".$data->price."','".$data->date."','".$data->content."','".$insertedCampaignId."','".$data->created_by."')";
        $db->setQuery($insertQuery)->query();


		//$post->create(array('overrideDoctType' => 'legacy'));

		// Retrieve the uid
		$data->uid = $data->created_by;
		//$data->revision_id = $post->revision->id;

		// binding
		//$post->bind($data, array());

		/*$saveOptions = array(
						'applyDateOffset' => false,
						'validateData' => false,
						'useAuthorAsRevisionOwner' => true,
						'saveFromEasysocialStory' => true
					);*/

		//$post->save($saveOptions);

		$template->context_type = 'requestfound';

		$template->context_id = $insertedCampaignId;

		return;
	}

	/**
	 * Prepare stream item
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function onPrepareStream(SocialStreamItem &$item, $includePrivacy = true)
	{
		// Ensure that we're in the correct context
		if ($item->context != 'requestfound' || !$this->exists()) {
			return;
		}

		// Get the page
		$page = ES::page($item->cluster_id);

		// Check if the viewer can really view the item
		if (!$page->canViewItem()) {
			return;
		}

		// Attach our own stylesheet
		$this->getApp()->loadCss();

		// Define standard stream looks
		$item->display = SOCIAL_STREAM_DISPLAY_FULL;

		// New blog post
		if ($item->verb == 'create') {
			$this->prepareNewBlogStream($item);
		}

		// Updated blog post
		if ($item->verb == 'update') {
			$this->prepareUpdateBlogStream($item);
		}
	}

	/**
	 * Generates the stream for updated stream item
	 *
	 * @since   2.0
	 * @access  public
	 */
	private function prepareUpdateBlogStream(&$item)
	{
		$post = EB::post($item->contextId);

		// Post could be deleted from the site by now.
		if (!$post->id) {
			return;
		}

		if (!$post->getPrimaryCategory()) {
			return;
		}



		// Format the likes for the stream
		$likes = ES::likes();
		$likes->get($item->contextId, 'requestfound', 'update');
		$item->likes = $likes;
		$url = EBR::_('index.php/component/jgive/'.$post->id.':newtestrequest/edit/campaign?Itemid=0');

		// Apply comments on the stream
		$item->comments = ES::comments($item->contextId, 'requestfound', 'update', SOCIAL_APPS_GROUP_PAGE, array('url' => $url));

		// We might want to use some javascript codes.
		EB::init('site');

		$date = EB::date($post->created);

		$content = $post->getIntro(true, true, 'all', null, $options = array('triggerPlugins' => false));

		$appParams = $this->getParams();
		$alignment = 'pull-' . $appParams->get('imagealignment', 'right');
		$this->set('alignment', $alignment);

		$contentLength  = $appParams->get('maxlength');


		if ($contentLength > 0) {
			// truncate the content
			$content = $this->truncateStreamContent($content, $contentLength);
		}

		

		// See if there's any audio files to process.
		$audios = EB::audio()->getItems($content);

		// Remove videos from the source
		$content = EB::videos()->strip($content);

		// Remove audios from the content
		$content = EB::audio()->strip($content);

		$catUrl = EBR::_('index.php?option=com_easyblog&view=categories&layout=listings&id=' . $post->category_id, true, null, false, true);

		$this->set('categorypermalink', $catUrl);
		$this->set('audios', $audios);
		$this->set('date', $date);
		$this->set('permalink', $url);
		$this->set('post', $post);
		$this->set('actor', $item->actor);
		$this->set('content', $content);

		$item->title = parent::display('streams/' . $item->verb . '.title');
		$item->content = parent::display('streams/preview');

		// Append the opengraph tags
		if ($post->getImage()) {
			$item->addOgImage($post->getImage('thumbnail'));
		}

		$item->addOgDescription($content);
	}


	/**
	 * Displays the stream item for new blog post
	 *
	 * @since	2.0
	 * @access	public
	 */
	private function prepareNewBlogStream(&$item)
	{

		// Load the blog post
        $db = JFactory::getDbo();
        $post = $db->setQuery("SELECT * FROM #__jg_campaigns WHERE id='".$item->contextId."'")->loadObject();
		//$post = EB::post($item->contextId);


		if (!$post->id) {
			return;
		}

        $document = JFactory::getDocument();
        $document->addScript(JUri::root(true) . '/media/com_jgive/javascript/donation_social.js');



        $script = ES::get('Script');
        $script->set('errorTitle', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_TITLE'));
        $script->set('errorContent', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_CONTENT'));
        $script->set('errorPrice', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_PRICE'));
        $script->set('errorDatetime', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_DATE'));
        $script->set('errorPayPalEmail', JText::_('APP_PAGE_REQUEST_FOUND_INVALID_PAYPAL'));
        $script->output('apps:/page/requestfound/story');

        $amount_query = "SELECT SUM(amount) as amount FROM #__jg_orders WHERE campaign_id='".$post->id."' && status='C'";
		$post->raised = $db->setQuery($amount_query)->loadResult();
        $post->raised = ($post->raised)? $post->raised : 0;

        $total_donors_query = "SELECT COUNT(id) as orders FROM #__jg_orders WHERE campaign_id='".$post->id."' && status='C'";
        $post->total_donors = $db->setQuery($total_donors_query)->loadResult();

        $post->amount_precent = ($post->raised*100)/$post->goal_amount ;
        $post->amount_precent = (int)$post->amount_precent;

        $date1 = date_create($post->stard_date);
        $date2 = date_create($post->end_date);
        $diff=date_diff($date1,$date2);
        $post->diff = $diff->format("%a");

        $post->link = '/index.php/component/jgive/'.$post->id.'/details/campaign?Itemid=0';
        $post->donation_link = '/index.php/component/jgive/donations/payment/'.$post->id.'?Itemid=0';


        // Format the likes for the stream
		$likes = ES::likes();
		$likes->get($item->contextId, 'requestfound', 'create');

		$item->likes = $likes;

		// Get the permalink to the blog post
		$permalink = '#';

		// Apply comments on the stream
		$item->comments = ES::comments($item->contextId, 'requestfound', 'create', SOCIAL_APPS_GROUP_PAGE, array('url' => $permalink));

		// Get the app params
		$params = $this->getParams();

		// Get the alignment that should be used for the image
		$alignment = 'pull-' . $params->get('imagealignment', 'right');

		// Get the content
		$content = $post->short_description;

		$contentLength  = $params->get('maxlength');

		if ($contentLength > 0){
			// truncate the content
			$content = $this->truncateStreamContent($content, $contentLength);
		}

		$post->page = ES::page($item->cluster_id);
		$access = $post->page->getAccess();
		if ($this->my->isSiteAdmin() || $post->page->isAdmin() || ($access->get('stream.edit', 'admins') == 'members' && $item->actor->id == $this->my->id)) {
			$item->edit_link = 	'index.php/component/jgive/'.$post->id.':newtestrequest/edit/campaign?Itemid=0';
			//$item->edit_link = EB::composer()->getComposeUrl(array('uid' => $post->id));
		}

		// See if there's any audio files to process.
		$audios = EB::audio()->getItems($content);

		$this->set('alignment', $alignment);
		$this->set('audios', $audios);
		$this->set('post', $post);
		$this->set('actor', $item->actor);
		$this->set('content', $content);

		$item->title = parent::display('streams/' . $item->verb . '.title');
		$item->content = parent::display('streams/preview');


		// Add image to the og:image
		$item->addOgImage('');
		$item->addOgDescription($content);
        
	}

	/**
	 * Truncate stream item
	 *
	 * @since   2.0
	 * @access  public
	 */

	public function truncateStreamContent($content, $contentLength)
	{
		// Get the app params
		$params = $this->getParams();
		$truncateType = $params->get('truncation');

		if ($truncateType == 'chars') {

			// Remove uneccessary html tags to avoid unclosed html tags
			$content = strip_tags($content);

			// Remove blank spaces since the word calculation should not include new lines or blanks.
			$content = trim($content);

			// @task: Let's truncate the content now.
			$content = JString::substr(strip_tags($content), 0, $contentLength);

		} else {

			$tag = false;
			$count = 0;
			$output = '';

			// Remove uneccessary html tags to avoid unclosed html tags
			$content = strip_tags($content);

			$chunks = preg_split("/([\s]+)/", $content, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

			foreach($chunks as $piece) {

				if (!$tag || stripos($piece, '>') !== false) {
					$tag = (bool) (strripos($piece, '>') < strripos($piece, '<'));
				}

				if (!$tag && trim($piece) == '') {
					$count++;
				}

				if ($count > $contentLength && !$tag) {
					break;
				}

				$output .= $piece;
			}

			unset($chunks);
			$content = $output;
		}

		return $content;
	}
}
