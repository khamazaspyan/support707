<?php
/**
 * @package		EasySocial
 * @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * EasySocial is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
$jgiveFrontendHelper = new jgiveFrontendHelper;
$user = JFactory::getUser();
$input = JFactory::getApplication()->input;

$session       = JFactory::getSession();
$this->session = $session;

$this->params          = $params = JComponentHelper::getParams('com_jgive');
$show_selected_fields_on_donate = $this->params->get('show_selected_fields_on_donation');
$donationfield = array();
$show_field = 0;
$donation_anonym = 0;

if ($show_selected_fields_on_donate)
{
    $donationfield = $this->params->get('donationfield');

    if (isset($donationfield))
    {
        if (in_array("donation_anonym", $donationfield))
        {
            $donation_anonym = 1;
        }
    }
}
else
{
    $show_field = 1;
}
// Fetched data


$lang = JFactory::getLanguage();
$extension = 'com_jgive';
$base_dir = JPATH_SITE;
$language_tag = 'en-GB';
$reload = true;
$lang->load($extension, $base_dir, $language_tag, $reload);
$db = JFactory::getDBO();
$cdata_array = $db->setQuery("SELECT * FROM #__jg_campaigns WHERE id='".$campaign_id."'")->loadObject();
$cdata['campaign'] = $cdata_array;
$cdata['images'] = array();
$cdata['givebacks'] = array();


$dispatcher = JDispatcher::getInstance();
JPluginHelper::importPlugin('payment');

$params = JComponentHelper::getParams('com_jgive');

if (!is_array($params->get('gateways')))
{
    $gateway_param[] = $params->get('gateways');
}
else
{
    $gateway_param = $params->get('gateways');
}

if (!empty($gateway_param))
{
    $gateways = $dispatcher->trigger('onTP_GetInfo', array($gateway_param));
}

$this->gateways = $gateways;




jimport('joomla.filesystem.file');

jimport('joomla.filesystem.folder');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.modal', 'a.modal');
$input=JFactory::getApplication()->input;


$campaign_title=$this->cdata['campaign']->title;
$pagetitle=JText::sprintf('COM_JGIVE_CHECKOUT_TITLE',$campaign_title);
$document=JFactory::getDocument();
$document->addScript(JUri::root(true).'/media/com_jgive/javascript/jgive.js');
$document->addStyleSheet(JUri::root(true).'/media/com_jgive/css/jgive_bs3.css');
$document->addStyleSheet(JUri::root(true).'/media/techjoomla_strapper/bs3/css/bootstrap.css');
$document->addScript(JUri::root(true).'/media/com_jgive/javascript/fields_validation.js');
$document->addScript(JUri::root(true).'/media/com_jgive/javascript/donations.js');

$document->setTitle($pagetitle);
$baseurl=JRoute::_ (JUri::root().'index.php');

$params=JComponentHelper::getParams('com_jgive');

$user = JFactory::getuser();
$es_user = ES::user($user->id);
$isguest=$user->id;

// Get the data to idetify which field to show on donation view
$show_selected_fields_on_donate=$params->get('show_selected_fields_on_donation');
$donationfield=array();

$show_field=0;
$first_name=1;
$last_name=1;
$email=1;
$address=1;
$address2=1;
$hide_country=1;
$state=1;
$city=1;
$zip=1;
$phone_no=1;
$donation_type=0;
$donation_anonym=0;

if ($show_selected_fields_on_donate)
{
    $donationfield=$params->get('donationfield');

    if(isset($donationfield))
        foreach($donationfield as $tmp)
        {
            switch($tmp)
            {
                case 'first_name':
                    $first_name=1;
                    break;

                case 'last_name':
                    $last_name=1;
                    break;

                case 'email':
                    $email=1;
                    break;

                case 'address':
                    $address=1;
                    break;

                case 'address2':
                    $address2=1;
                    break;

                case 'country':
                    $hide_country=1;
                    break;

                case 'state':
                    $state=1;
                    break;

                case 'city':
                    $city=1;
                    break;

                case 'zip':
                    $zip=1;
                    break;


                case 'donation_type':
                    $donation_type=1;
                    break;


            }
        }
}
else
{
    $show_field=1;
}

if($user->id)
{
    $registr_field_display="display:none";
}
else
{
    $registr_field_display="display:display";
}

if(empty($isguest))
{
    $isguest=1;
}
else
{
    $isguest=0;
}

$js = "
var isgst=".$isguest.";
var jgive_baseurl='".$baseurl."';

	function jGive_submitbutton(pressbutton)
	{
		var form = document.jGivePaymentForm;

		if(pressbutton)
			{
				techjoomla.jQuery('#payment-info-tab').hide();
				techjoomla.jQuery('#system-message').remove();
				if (document.formvalidator.isValid(document.id('jGivePaymentForm')))
				{
					techjoomla.jQuery('#system-message').remove();
				}
				else
				{
					techjoomla.jQuery('#payment-info-tab').hide();
					var msg = 'Some values are not acceptable.  Please retry.';
					alert(msg);
					return false;
				}

				values=techjoomla.jQuery('#jGivePaymentForm').serialize();
				var order_id=techjoomla.jQuery('#order_id').val();
				techjoomla.jQuery.ajax({
					url: jgive_baseurl+'?option=com_jgive&task=donations.placeOrder&tmpl=component',
					type: 'POST',
					data:values,
					dataType: 'json',
					beforeSend: function()
					{
						techjoomla.jQuery('#confirm-order').after('<div class=\"com_jgive_ajax_loading\"><div class=\"com_jgive_ajax_loading_text\">".JText::_('COM_JGIVE_LOADING_PAYMET_FORM_MSG')."</div><img class=\"com_jgive_ajax_loading_img\" src=\"".JUri::base()."media/com_jgive/images/ajax.gif\"></div>');

						// CODE TO HIDE EDIT LINK
						jgive_hideAllEditLinks();
					},
					complete: function()
					{
						techjoomla.jQuery('#jgive_order_details_tab').show()
						techjoomla.jQuery('.com_jgive_ajax_loading').remove();
						jgive_showAllEditLinks();
					},
					success: function(data)
					{
						if(data['success'] == 1)
						{
							if(isgst==1)
							{
								techjoomla.jQuery('#user-info').html('');
							}
							techjoomla.jQuery('#payment-info-tab').hide();
							techjoomla.jQuery('#order_id').val(data['order_id']);
							techjoomla.jQuery('#payment-info .checkout-heading a').remove();
							addEditLink(techjoomla.jQuery('#payment-info'+' .checkout-heading'),'payment-info');

							techjoomla.jQuery('#payment_tab_table_html').html(data['payhtml']);
							techjoomla.jQuery('#order_summary_tab_table_html').html(data['orderHTML']);
							techjoomla.jQuery('#payment_tab').show();
							techjoomla.jQuery('#payment_tab_table').show();
							techjoomla.jQuery('#order_summary_tab').show();
							techjoomla.jQuery('#order_summary_tab_table').show();
							techjoomla.jQuery('#payment-info-tab').hide();
							techjoomla.jQuery('#payment_tab_table_html').html(data['gatewayhtml']);
							techjoomla.jQuery('#payment_tab').show();
							techjoomla.jQuery('#payment_tab_table').show();
							techjoomla.jQuery('#payment_tab_table_html').show();
						}
						else
						{
							techjoomla.jQuery('#payment-info').hide();
							techjoomla.jQuery('#payment-info-tab').hide();
							techjoomla.jQuery('#payment_tab').hide();
							techjoomla.jQuery('#payment_tab_table').html();
							techjoomla.jQuery('#order_summary_tab_table_html').html();
							techjoomla.jQuery('#payment_tab_table').hide();
							techjoomla.jQuery('#order_summary_tab_table').hide();
						}
					}
				});
				return;
			}
		return;
	}
";

$document->addScriptDeclaration($js);
?>

<script type="text/javascript">

    //get Commission for identifying minimum donation amount
    var minimum_amount="<?php echo $cdata['campaign']->minimum_amount;?>";
    minimum_amount=parseInt(minimum_amount);
    var send_payments_to_owner="<?php echo$params->get('send_payments_to_owner');?>";
    var commission_fee="<?php echo$params->get('commission_fee');?>";
    var fixed_commissionfee="<?php echo$params->get('fixed_commissionfee');?>";

    function validatedatainsteps(thistepobj,nextstepid,stepno,currentstepname)
    {
        if (parseInt(stepno)==2)
        {
            GotonextStep(thistepobj,nextstepid,currentstepname);
        }

        if (parseInt(stepno)==3) //donation form
        {
            //check donation amount is valid
            var donation_amount=document.getElementById('donation_amount').value;
            donation_amount=parseFloat(donation_amount);

            if(donation_amount)
            {
                if(!send_payments_to_owner)
                {
                    if(commission_fee)
                    {
                        total_commission_amount=((donation_amount*commission_fee)/100)+fixed_commissionfee;
                    }
                    else
                    {
                        total_commission_amount=fixed_commissionfee;
                    }
                }
                else
                {
                    total_commission_amount=0;
                }

                if(total_commission_amount<minimum_amount)
                {
                    total_commission_amount=minimum_amount; //trikey don't bother it
                }

                if(total_commission_amount>donation_amount)
                {
                    alert("<?php echo JText::_('COM_JGIVE_MINIMUM_DONATION_AMOUNT');?>"+total_commission_amount);
                    return false;
                }

                var response = validateGiveBackAmount(donation_amount);

                if(!response)
                {
                    return false;
                }
            }
            else
            {
                alert("<?php echo JText::_('COM_JGIVE_ENTER_DONATION_AMOUNT');?>");
                return false;
            }

            if(document.formvalidator.isValid(document.id('jGivePaymentForm')))
            {
                techjoomla.jQuery('#payment-info-tab').hide();
                techjoomla.jQuery('#system-message').remove();
            }
            else
            {
                techjoomla.jQuery('#payment-info-tab').hide();
                var msg = "<?php echo JText::_('COM_JGIVE_FORM_INVALID')?>";
                alert(msg);
                return false;
            }
            <?php
            if($params->get('terms_condition') && ($params->get('payment_terms_article')!=0))
            { ?>
            if(document.jGivePaymentForm.terms_condition.checked==false)
            {
                var check="<?php echo JText::_('COM_JGIVE_CHECK_TERMS');?>";
                alert(check);
                return false;
            }
            <?php
            } ?>
            GotonextStep(thistepobj,nextstepid,currentstepname);
        }
    }

    function GotonextStep(thistepobj,nextstepid,currentstepname){
        techjoomla.jQuery('.checkout-content').hide();
        techjoomla.jQuery('#'+currentstepname.toString()+' .checkout-heading a').remove();

        var order_id=techjoomla.jQuery('#order_id').val();
        var finalamt=techjoomla.jQuery('#net_amt_pay_inputbox').val();
        if(parseFloat(finalamt)<=0)
            techjoomla.jQuery('#payment_info-tab-method').hide();
        else
            techjoomla.jQuery('#payment_info-tab-method').show();

        if(parseInt(order_id)>=1)
        {
            jGive_submitbutton(thistepobj.id)
            addEditLink(techjoomla.jQuery('#'+currentstepname.toString()+' .checkout-heading'),currentstepname.toString());
            return;
        }

        techjoomla.jQuery('#'+nextstepid).slideDown('slow');
        //MOVE CURSOR TO CURRENT STEP
        var parentid=techjoomla.jQuery('#'+nextstepid).parent().attr('id');
        goToByScroll(parentid);
        addEditLink(techjoomla.jQuery('#'+currentstepname.toString()+' .checkout-heading'),currentstepname.toString());
        jgive_showAllEditLinks()
    }

    function jgive_hideAllEditLinks()
    {
        techjoomla.jQuery(".jgive_editTab").hide();
    }
    function jgive_showAllEditLinks()
    {
        techjoomla.jQuery(".jgive_editTab").show();
    }
    // This is a functions that scrolls to #{blah}link
    function goToByScroll(id){
        techjoomla.jQuery('html,body').animate({
            scrollTop: techjoomla.jQuery("#"+id).offset().top},'slow');
    }

    function addEditLink(selectorObj,currentstepname)
    {
        techjoomla.jQuery('#'+currentstepname.toString()+' .checkout-heading .badge').remove();
        techjoomla.jQuery(selectorObj).append('<span class=" badge badge-success pull-right" id="jgive_success_icon"><i class="fa fa-check"></i></span><a class="jgive_editTab" onclick="jgive_hideshowTabs(this)"><?php echo JText::_('COM_JGIVE_EDIT'); ?>&nbsp;&nbsp;</a>');
    }

    techjoomla.jQuery(document).ready(function()
    {
        //techjoomla.jQuery(".checkout-content").hide();
        var userid=techjoomla.jQuery('#userid').val();
        if(parseInt(userid)==0)
        {
            techjoomla.jQuery(".checkout-first-step-billing-info").hide();
            techjoomla.jQuery(".checkout-first-step-user-info").show();
        }
        else
        {
            techjoomla.jQuery(".checkout-first-step-billing-info").show();
        }
        techjoomla.jQuery('#payment-info-tab').hide();

        var DBuserbill="<?php echo (isset($this->userbill->state_code))?$this->userbill->state_code:''; ?>";

        var state='',city='',category='';
        <?php
        $JGIVE_state = $this->session->get('JGIVE_state');
        if(!empty($JGIVE_state)) { ?>
        state="<?php echo $this->session->get('JGIVE_state');?>";
        <?php } ?>

        <?php

        $JGIVE_city = $this->session->get('JGIVE_city');
        if(!empty($JGIVE_city)) { ?>
        city="<?php echo $this->session->get('JGIVE_city');?>";
        <?php } ?>

        //generateState("country",DBuserbill) ;
        generateState('country',state,city);
        populateGiveback();

        //payment gateways html
        jGive_RecurDonation(0);

    });
    /*
    To generate State list according to selected Country
    @param id of select list
    */
    function generateState(countryId,state,city)
    {
        generateCity(countryId,city);
        var country=jQuery('#'+countryId).val();
        techjoomla.jQuery.ajax(
            {
                url:'<?php echo JUri::root();?>'+'index.php?option=com_jgive&task=loadState&country='+country+'&tmpl=component',
                type:'GET',
                dataType:'json',
                success:function(data)
                {
                    if (data === undefined || data == null || data.length <= 0)
                    {
                        var op='<option value="">'+"<?php echo JText::_('COM_JGIVE_STATE');?>"+'</option>';
                        select=techjoomla.jQuery('#state');
                        select.find('option').remove().end();
                        select.append(op);
                    }
                    else{
                        generateoption(data,countryId,state);
                    }
                }
            });
    }

    /*
    TO generate option
    @param: data=list of state/region in Json format
    countryID=called country select list
    Source ID which generate Option list
    */
    function generateoption(data,countryId,state)
    {
        var options, index, select, option;
        if(countryId=='country'){
            select = techjoomla.jQuery('#state');
        }
        select.find('option').remove().end();
        options=data.options;
        for(index = 0; index < data.length; ++index)
        {
            var region=data[index];
            if(state==region['id'])
                var op="<option value="  +region['id']+  " selected='selected'>"  +region['region']+   '</option>'     ;
            else
                var op="<option value="  +region['id']+  ">"  +region['region']+   '</option>'     ;
            if(countryId=='country'){
                techjoomla.jQuery('#state').append(op);
            }
        }
    }
    function generateCity(countryId,city)
    {
        var country=techjoomla.jQuery('#'+countryId).val();
        techjoomla.jQuery.ajax(
            {
                url:'<?php echo JUri::root();?>'+'index.php?option=com_jgive&task=loadCity&country='+country+'&tmpl=component',
                type:'GET',
                dataType:'json',
                success:function(data)
                {
                    if (data === undefined || data == null || data.length <= 0)
                    {
                        var op='<option value="">'+"<?php echo JText::_('COM_JGIVE_CITY');?>"+'</option>';
                        select=techjoomla.jQuery('#city');
                        select.find('option').remove().end();
                        select.append(op);
                    }
                    else{
                        generateoptioncity(data,countryId,city);
                    }
                }
            });
    }
    function generateoptioncity(data,countryId,citydeafult)
    {
        var options, index, select, option;
        if(countryId=='country'){
            select = techjoomla.jQuery('#city');
        }
        select.find('option').remove().end();
        options=data.options;
        for(index = 0; index < data.length; ++index)
        {
            var city=data[index];
            if(citydeafult==city['id'])
            {
                var op="<option value="  +city['id']+  " selected='selected'>"  +city['city']+   '</option>'     ;
            }
            else
            {
                var op="<option value="  +city['id']+  ">"  +city['city']+   '</option>'     ;
            }
            if(countryId=='country'){
                techjoomla.jQuery('#city').append(op);
            }
        }
    }
    function chkmail(email){
        techjoomla.jQuery.ajax({
            url: '?option=com_jgive&task=donations.chkmail&email='+email+'&tmpl=component',
            type: 'GET',
            dataType: 'json',
            success: function(data)
            {

                if(data[0] == 1){
                    techjoomla.jQuery('#email_reg').html(data[1]);
                    techjoomla.jQuery("#button-billing-info").attr("disabled", "disabled");
                }
                else{
                    techjoomla.jQuery('#email_reg').html('');
                    techjoomla.jQuery("#button-billing-info").removeAttr("disabled");
                }
            }
        });
    }

    function jgive_hideshowTabs(obj)
    {
        jgive_hideAllEditLinks()
        techjoomla.jQuery('.checkout-content').slideUp('slow');
        techjoomla.jQuery(obj).hide();
        techjoomla.jQuery('#payment_tab_table_html').html();
        techjoomla.jQuery(obj).parent().parent().find('.checkout-content').slideDown('slow');
    }

    function jgive_toggleOrder(selecteddiv){
        techjoomla.jQuery('#'+selecteddiv).toggle();
    }

    function jgive_login()
    {
        techjoomla.jQuery.ajax({
            url: jgive_baseurl+'?option=com_jgive&task=donations.login_validate',
            type: 'post',
            data: techjoomla.jQuery('#user-info-tab #login :input'),
            dataType: 'json',
            beforeSend: function() {
                techjoomla.jQuery('#button-login').attr('disabled', true);
                techjoomla.jQuery('#button-login').after('<span class="wait">&nbsp;Loading..</span>');
            },
            complete: function() {
                techjoomla.jQuery('#button-login').attr('disabled', false);
                techjoomla.jQuery('.wait').remove();
            },
            success: function(json) {
                techjoomla.jQuery('.warning, .j2error').remove();

                if (json['error']) {
                    techjoomla.jQuery('#login').prepend('<div class="warning alert alert-danger" >' + json['error']['warning'] + '<button data-dismiss="alert" class="close" type="button">×</button></div>');
                    techjoomla.jQuery('.warning').fadeIn('slow');
                }
                else if (json['redirect']) {
                    location = json['redirect'];
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
            }
        });
    }

    function jGive_RecurDonation(radio_option)
    {
        if(radio_option == 0)
        {
            var pg_list = <?php
                $select   = array();
                $gateways = array_merge($select, $this->gateways);
                $gateways = array_filter($gateways);

                // If only one geteway then keep it as selected
                if(count($gateways) >= 1)
                {
                    // Id and value is same
                    $default = $gateways[0]->id;
                }

                if(empty($this->gateways))
                {
                    echo 0;
                }
                else
                {
                    $plg_html = '';
                    $cheched = 'checked="chcked"';

                    foreach ($gateways as $gateway)
                    {
                        $plg_html .=

                            '<div class="radio">
					  <label>
						<input type="radio" name="gateways" id="'.$gateway->id.'" value="'.$gateway->id.'" aria-label="..." ' . $cheched . '>
							'.$gateway->name.'
					  </label>
					</div>';

                        $cheched = '';
                    }

                    $html = array();
                    $html[0] = $plg_html;
                    echo json_encode($html);
                }
                ?>;

            if(pg_list[0]==0)
            {
                pg_list[0]="<?php echo JText::_( 'COM_JGIVE_NO_PAYMENT_GATEWAY' ); ?>"
            }

            techjoomla.jQuery( "div#gatewaysContent" ).html(pg_list[0]);
            techjoomla.jQuery('#recurring_freq_div').hide();
            techjoomla.jQuery('#recurring_count_div').hide();
            techjoomla.jQuery('#recurring_count').removeClass('required');
        }
        else if(radio_option==1)
        {
            var pg_list = <?php
                $select=array();
                $gateways = array_merge($select, $this->recurringGateways);
                $gateways=array_filter($gateways);

                // If only one geteway then keep it as selected
                if(count($gateways)>=1)
                {
                    // Id and value is same
                    $default = $gateways[0]->id;
                }

                if(empty($this->recurringGateways))
                {
                    echo 0;
                }
                else
                {
                    $plg_html = '';

                    $i = 0;

                    $cheched = 'checked="chcked"';

                    foreach ($gateways as $gateway)
                    {
                        $plg_html .=

                            '<div class="radio">
					  <label>
						<input type="radio" name="gateways" id="'.$gateway->id.'" value="'.$gateway->id.'" aria-label="..." ' . $cheched . '>
							'.$gateway->name.'
					  </label>
					</div>';

                        $cheched = '';
                    }

                    $html = array();

                    $html[0] = $plg_html;

                    echo json_encode($html);
                }
                ?>;

            if(pg_list[0]==0)
            {
                pg_list[0]="<?php echo JText::_( 'COM_JGIVE_NO_PAYMENT_GATEWAY' ); ?>"

            }
            techjoomla.jQuery( "div#gatewaysContent" ).html(pg_list[0]);
            techjoomla.jQuery('#recurring_count').addClass('required');
            techjoomla.jQuery('#recurring_freq_div').show();
            techjoomla.jQuery('#recurring_count_div').show();
        }
    }

    function otherCity()
    {
        if(document.jGivePaymentForm.other_city_check.checked===true)
        {
            jQuery("#other_city").show();
            jQuery("#hide_city").hide();
        }
        else
        {
            jQuery("#hide_city").show();
            jQuery("#other_city").hide();
        }
    }

    //Populate selectd giveback amount in donation amount field

    var givebackDetails=<?php
        if(!empty($this->campaignGivebacks))
        {
            echo json_encode($this->campaignGivebacks,1);
        }
        else
        {
            echo 0;
        }?>;

    function populateGiveback()
    {
        var giveBackid= techjoomla.jQuery('#givebacks').val();

        if(giveBackid=='edit_amount')
        {
            techjoomla.jQuery('#giveback_des').html("<?php echo JText::_('COM_JGIVE_GIVE_NOT_AVIL_FOR_THIS_AMOUNT'); ?>");
            //techjoomla.jQuery('#donation_amount').removeAttr('readOnly');
        }

        for(index = 0; index < givebackDetails.length; index++)
        {
            if(givebackDetails[index]['id']==giveBackid)
            {
                //techjoomla.jQuery('#donation_amount').attr('readOnly','readOnly');
                //update amount in donation amount field
                techjoomla.jQuery('#donation_amount').attr('value',givebackDetails[index]['amount']);
                techjoomla.jQuery('#giveback_des').html(givebackDetails[index]['description']);
                //update giveback description
            }
        }
    }

    function noGiveBack()
    {
        var no_giveback=techjoomla.jQuery( "input:checkbox[name=no_giveback]:checked" ).val();

        if(no_giveback==undefined)
        {
            techjoomla.jQuery("#hide_giveback").show("slow");
            techjoomla.jQuery("#hide_giveback_desc").show("slow");
        }
        else
        {
            techjoomla.jQuery("#hide_giveback").hide("slow");
            techjoomla.jQuery("#hide_giveback_desc").hide("slow");
            //techjoomla.jQuery('#donation_amount').removeAttr('readOnly');
        }
    }

    function validateGiveBackAmount(donation_amount)
    {

        var no_giveback=techjoomla.jQuery( "input:checkbox[name=no_giveback]:checked" ).val();

        // Get the value from a dropdown select
        var givebackId = techjoomla.jQuery( "#givebacks").val();

        if(no_giveback==undefined)
        {
            for(index = 0; index < givebackDetails.length; index++)
            {
                if(givebackDetails[index]['id']==givebackId)
                {
                    var givebackAmount = givebackDetails[index]['amount'];

                    if(donation_amount>=givebackAmount)
                    {
                        return true;
                    }
                    else
                    {
                        alert("<?php echo JText::_('COM_JGIVE_AMOUNT_SHOULD_BE'); ?>"+givebackAmount);
                        return false;
                    }
                }
            }
        }
        return true;
    }
</script>
<?php
if(isset($this->jsheader))
{
    echo $this->jsheader;
}
?>
<?php
if(isset($this->jsfooter))
{
    echo $this->jsfooter;
}
//jomsocial toolprogress-bar
echo $this->jomsocailToolbarHtml;
?>
<div class="es-container" data-requestfound>
    <div class="es-sidebar-sticky" style="padding-top: 0px;">
        <div class="es-sidebar sidebar323" style="padding-top: 0px;" data-sidebar="">


            <div class="es-side-widget is-module">
                <div class="es-side-widget__hd">
                    <div class="es-side-widget__title">
                        Sponsors
                        <span>(<?php echo sizeof($donors); ?>)</span>
                    </div>
                </div>

                <div class="es-side-widget__bd">
                    <ul class="g-list-inline">
                        <?php foreach($donors as $donor): ?>
                            <li class="t-lg-mb--md t-lg-mr--md">
                                <?php echo $this->html('avatar.user', ES::user($donor), 'md'); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>


            </div>

        </div>
    </div>
    <div class="es-content support_a_family_page" style="margin-bottom: 20px;">
        <div class="tjBs3 jgive" id="jgive-checkout">
            <div class="page-header">
                <h1>
                    <?php
                    if($cdata['campaign']->type == 'donation')
                    {
                        echo JText::sprintf('COM_JGIVE_CHECKOUT_TITLE',$campaign_title);
                    }
                    else
                    {
                        echo JText::sprintf('COM_JGIVE_CHECKOUT_TITLE_INVESTMENT',$campaign_title);
                    }
                    ?>
                </h1>
            </div>

            <div class="container-fluid">


                <form action="" method="post" name="jGivePaymentForm" id="jGivePaymentForm"	class="form-validate">

                    <?php
                    $user = JFactory::getUser();
                    if (!$user->id)
                    { ?>
                        <div id="user-info" class="jgive-checkout-steps row">
                            <div class="checkout-heading">
                                <?php echo JText::_('COM_JGIVE_USER_INFO');?>
                            </div>
                            <div class="checkout-content checkout-first-step-user-info" id="user-info-tab">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <h2><?php echo JText::_('COM_JGIVE_CHECKOUT_NEW_DONOR'); ?></h2>
                                    <h4><?php echo JText::_('COM_JGIVE_CHECKOUT_OPTIONS'); ?></h4>
                                    <!-- registration -->
                                    <?php if($this->guest_donation == '1'): ?>
                                        <label label-default for="register">
                                            <input type="radio" name="account" value="register" id="register" checked="checked" />
                                            <b><?php echo JText::_('COM_JGIVE_CHECKOUT_REGISTER'); ?></b>
                                        </label>
                                        <br />
                                    <?php endif; ?>

                                    <!-- guest -->
                                    <?php if($this->guest_donation == '1'): ?>
                                        <label label-default for="guest">
                                            <input type="radio" name="account" value="guest" id="guest"  />
                                            <b><?php echo JText::_('COM_JGIVE_CHECKOUT_GUEST'); ?></b>
                                        </label>
                                    <?php endif; ?>
                                    <br />

                                    <?php if($this->guest_donation == '1'): ?>
                                        <p class="text-info"><?php echo JText::_('COM_JGIVE_CHECKOUT_REGISTER_ACCOUNT_HELP_TEXT'); ?></p>
                                    <?php endif; ?>
                                    <input type="button" class="btn btn-primary" id="button-user-info" value="<?php echo JText::_('COM_JGIVE_CONTINUE');?>" onclick="validatedatainsteps(this,'<?php echo "billing-info-tab" ?>',2,'user-info')">
                                    <br/>

                                </div>
                                <div id="login" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <h2><?php echo JText::_('COM_JGIVE_CHECKOUT_RETURNING_DONOR'); ?></h2>
                                    <h4><?php echo JText::_('COM_JGIVE_CHECKOUT_RETURNING_DONOR_WELCOME'); ?></h4>
                                    <b><?php echo JText::_('COM_JGIVE_CHECKOUT_USERNAME'); ?></b><br />
                                    <input type="text" name="email" value="" />
                                    <b><?php echo JText::_('COM_JGIVE_CHECKOUT_PASSWORD'); ?></b><br />
                                    <input type="password" name="password" value="" />
                                    <br />
                                    <input type="button" value="<?php echo JText::_('COM_JGIVE_CHECKOUT_LOGIN'); ?>" id="button-login" onclick="jgive_login()" class="btn btn-primary" /><br />
                                    <br />
                                </div>
                                <div class="span5 pull-left">
                                </div>
                            </div>
                        </div>

                        <?php
                    }?>

                    <!-- Start OF billing_info_tab-->
                    <div id="billing-info" class="jgive-checkout-steps row">
                        <div class="checkout-heading">
                            <?php //echo JText::_('COM_JGIVE_DONOR_DONATION_DETAILS');

                            if($cdata['campaign']->type == 'donation')
                            {
                                echo JText::_('COM_JGIVE_DONOR_DONATION_DETAILS');
                            }
                            else
                            {
                                echo JText::_('COM_JGIVE_DONOR_INVESTMENT_DETAILS');
                            }
                            ?>
                        </div>
                        <div class="checkout-content  checkout-first-step-billing-info" id="billing-info-tab">
                            <div class="form-horizontal">
                                <div class="form-group" id="jgive_billmail_msg_div">
                                    <span class="help-inline jgive_removeBottomMargin" id="billmail_msg"></span>
                                </div>

                                <!--Added by Sneha-->
                                <?php if($first_name==0 ): ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="first_name">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_FIRST_NAME_TOOLTIP'),
                                                JText::_('COM_JGIVE_FIRST_NAME'),
                                                '',
                                                JText::_('COM_JGIVE_FIRST_NAME') . '*');?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <input type="text" id="first_name" name="first_name" class="validate-name required form-control"
                                                   placeholder="<?php echo JText::_('COM_JGIVE_FIRST_NAME_PH');?>" value="<?php echo $this->session->get('JGIVE_first_name');?>">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <!--Added by Sneha-->
                                <?php if($last_name==0 ): ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="last_name">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_LAST_NAME_TOOLTIP'),
                                                JText::_('COM_JGIVE_LAST_NAME'),
                                                '',
                                                JText::_('COM_JGIVE_LAST_NAME') . '*');?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <input type="text" id="last_name" name="last_name" class="required form-control"
                                                   placeholder="<?php echo JText::_('COM_JGIVE_LAST_NAME_PH');?>" value="<?php echo $this->session->get('JGIVE_last_name');?>">
                                        </div>
                                    </div>
                                <?php endif; ?>


                                        <input type="hidden" id="paypal_email" <?php echo ( (!$user->id) ?  'onchange="chkmail(this.value);"':''); ?> name="paypal_email" class=""
                                               placeholder="<?php echo JText::_('COM_JGIVE_EMAIL_PH');?>" value="<?php echo $user->email;?>"
                                               onblur='validateCheckoutFields(document.getElementById("paypal_email").value)'>

                                <!--Added by Sneha-->
                                <?php if($address==0 ): ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="address" title="<?php echo JText::_('COM_JGIVE_ADDRESS_TOOLTIP');?>">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_ADDRESS_TOOLTIP'),
                                                JText::_('COM_JGIVE_ADDRESS'),
                                                '',
                                                JText::_('COM_JGIVE_ADDRESS') . '*');?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <input type="text" id="address" name="address" class="required form-control"
                                                   placeholder="<?php echo JText::_('COM_JGIVE_ADDRESS_PH');?>" value="<?php echo $this->session->get('JGIVE_address');?>">
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <!--Added by Sneha-->
                                <?php if($address2==0 ): ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="address2">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_ADDRESS2_TOOLTIP'),
                                                JText::_('COM_JGIVE_ADDRESS2'),
                                                '',
                                                JText::_('COM_JGIVE_ADDRESS2'));?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <input type="text" id="address2"
                                                   name="address2"
                                                   placeholder="<?php echo JText::_('COM_JGIVE_ADDRESS2');?>"
                                                   value="<?php echo $this->session->get('JGIVE_address2');?>"
                                                   class="form-control"
                                            >
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <!--Added by Sneha-->
                                <?php if($hide_country==0 ): ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="country">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_COUNTRY_TOOLTIP'),
                                                JText::_('COM_JGIVE_COUNTRY'),
                                                '',
                                                JText::_('COM_JGIVE_COUNTRY') . '*');?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <?php
                                            $sc = $this->session->get('JGIVE_country');//@TODO use directly below
                                            $countries=$this->countries;
                                            $default=NULL;
                                            if(isset($sc)){
                                                $default=$this->session->get('JGIVE_country');
                                            }else{
                                                $default=$this->default_country;
                                            }
                                            $options=array();
                                            $options[]=JHtml::_('select.option',"",JText::_('COM_JGIVE_COUNTRY'));
                                            foreach($countries as $key=>$value)
                                            {
                                                $country=$countries[$key];
                                                $id=$country['id'];
                                                $value=$country['country'];
                                                $options[]=JHtml::_('select.option', $id, $value);
                                            }
                                            echo $this->dropdown=JHtml::_('select.genericlist',$options,'country','required="required" aria-invalid="false" size="1" class="input-large" onchange="generateState(id,\''.$this->session->get('JGIVE_state').'\',\''.$this->session->get('JGIVE_city').'\')"','value','text',$default,'country');
                                            ?>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <!--Added by Sneha-->
                                <?php if(($hide_country==0 AND $state==0 )): ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="state">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_STATE_TOOLTIP'),
                                                JText::_('COM_JGIVE_STATE'),
                                                '',
                                                JText::_('COM_JGIVE_STATE') . '*'
                                            );?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <select name="state" id="state" class="input-large"></select>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <!--Added by Sneha-->
                                <?php if(($hide_country==0 AND $city==0 )): ?>
                                    <div class="form-group" id="hide_city">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="city">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_CITY_TOOLTIP'),
                                                JText::_('COM_JGIVE_CITY'),
                                                '',
                                                JText::_('COM_JGIVE_CITY') . '*');?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <select name="city" id="city" class="input-large"></select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="other_city_check">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_OTHER_CITY_TOOLTIP'),
                                                JText::_('COM_JGIVE_OTHER_CITY'),
                                                '',
                                                JText::_('COM_JGIVE_OTHER_CITY'));?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <input type="checkbox" name="other_city_check" id="other_city_check" onchange="otherCity()"/>
                                            <?php echo JText::_('COM_JGIVE_CHECK_OTHER_CITY_MSG'); ?> <br/><br/>
                                            <input type="text" name="other_city" id="other_city" placeholder="<?php echo JText::_('COM_JGIVE_ENTER_OTHER_CITY');?>" class="form-control" value="" style="display:none;" >
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <!--Added by Sneha-->
                                <?php if($zip==0 ): ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="zip">
                                            <?php echo JHtml::tooltip(
                                                JText::_('COM_JGIVE_ZIP_TOOLTIP'),
                                                JText::_('COM_JGIVE_ZIP'),
                                                '',
                                                JText::_('COM_JGIVE_ZIP') . '*');?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <input type="text" id="zip" name="zip" class="required form-control"
                                                   placeholder="<?php echo JText::_('COM_JGIVE_ZIP_PH');?>" value="<?php echo $this->session->get('JGIVE_zip');?>">
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <!--Added by Sneha-->
                                <?php if($phone_no==0 ): ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="phone">
                                            <?php echo JHtml::tooltip(JText::_('COM_JGIVE_PHONE_TOOLTIP'),
                                                JText::_('COM_JGIVE_PHONE'),'',
                                                JText::_('COM_JGIVE_PHONE'));?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <input type="text" id="phone"
                                                   name="phone"
                                                   placeholder="<?php echo JText::_('COM_JGIVE_PHONE_PH');?>"
                                                   class="form-control validate-numeric"
                                                   value="<?php echo $this->session->get('JGIVE_phone');?>">
                                        </div>
                                    </div>
                                <?php endif; ?>



                                <div class="form-group">
                                    <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="donation_amount" title="<?php
                                    echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_AMOUNT_TOOLTIP') : JText::_('COM_JGIVE_INVESTMENT_AMOUNT_TOOLTIP'));
                                    ?>">
                                        <?php
                                        echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_AMOUNT') : JText::_('COM_JGIVE_INVESTMENT_AMOUNT'));
                                        ?> *
                                    </label>
                                    <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                        <div class="input-group input-large">

                                            <?php
                                            if (!empty($this->giveback_id))
                                            {
                                                $predefineAmount='';
                                                foreach($this->campaignGivebacks as $giveback)
                                                {
                                                    if($this->giveback_id == $giveback->id)
                                                    {
                                                        $predefineAmount=$giveback->amount;
                                                    }
                                                }
                                                //print_r( (array)$this->campaignGivebacks);die;
                                                ?>
                                                <input type="text"
                                                       id="donation_amount"
                                                       name="donation_amount" class="required validate-numeric form-control"
                                                       onblur='validateAmount(id,"<?php echo JText::_('COM_JGIVE_INVALID_DONATION_AMOUNT'); ?>")'
                                                       placeholder="<?php
                                                       echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_AMOUNT_PH') : JText::_('COM_JGIVE_INVESTMENT_AMOUNT_PH'));
                                                       ?>"  value="<?php echo $predefineAmount; ?>" >
                                                <?php
                                            }
                                            else
                                            {?>
                                                <input type="text" id="donation_amount" name="donation_amount" class="required validate-numeric"
                                                       onblur='validateAmount(id,"<?php echo JText::_('COM_JGIVE_INVALID_DONATION_AMOUNT'); ?>")'
                                                       placeholder="<?php
                                                       echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_AMOUNT_PH') : JText::_('COM_JGIVE_INVESTMENT_AMOUNT_PH'));
                                                       ?>" >

                                                <?php
                                            } ?>
                                            <span class="input-group-addon">USD</span>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                if (count($this->campaignGivebacks))
                                {
                                    $givebackDescription = '';
                                    $firstItem = 1;
                                    $end_select = 0;

                                    foreach ($this->campaignGivebacks as $giveback)
                                    {
                                        // Only unsold giveback available to buy
                                        if ($giveback->sold == 0 )
                                        {
                                            if ($firstItem==1)
                                            {
                                                $firstItem++;
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label " for="no_giveback">
                                                        <?php echo JHtml::tooltip(
                                                            JText::_('COM_JGIVE_NO_GIVEBACK'),
                                                            JText::_('COM_JGIVE_NO_GIVEBACK'),
                                                            '',
                                                            JText::_('COM_JGIVE_NO_GIVEBACK'));?>
                                                    </label>
                                                    <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12 ">
                                                        <input type="checkbox" id="no_giveback" name="no_giveback" value="1" onchange="noGiveBack()" > &nbsp; <?php echo JText::_('COM_JGIVE_THANKS_MSG'); ?>
                                                    </div>
                                                </div>

                                                <div id="hide_giveback" class="form-group">
                                                <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="givebacks">
                                                    <?php echo JHtml::tooltip(
                                                        JText::_('COM_JGIVE_GIVEBACKS_TOOLTIP'),
                                                        JText::_('COM_JGIVE_GIVEBACK'),
                                                        '',
                                                        JText::_('COM_JGIVE_GIVEBACK')); ?>
                                                </label>
                                                <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                                <select name="givebacks" id="givebacks" onchange="populateGiveback()" >
                                                <?php
                                                $end_select=1;
                                            }
                                            if($this->giveback_id == $giveback->id)
                                            {
                                                $givebackDescription=$giveback->description;
                                                ?>
                                                <option value="<?php echo $giveback->id; ?>"  selected="selected" ><?php echo $giveback->amount; ?>+ </option>
                                                <?php
                                            }
                                            else
                                            { ?>
                                                <option value="<?php echo $giveback->id; ?>"><?php echo $giveback->amount;?>+</option>
                                                <?php
                                            }
                                        }
                                    }

                                    if ($end_select == 1)
                                    { ?>
                                        </select>

                                        <!--<div id="giveback_des" class="well">
									<?php //echo $givebackDescription; ?>
								</div>-->
                                        </div>
                                        </div>
                                        <div id="hide_giveback_desc" class="form-group">
                                            <label class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="givebacks">
                                                <?php echo JHtml::tooltip(
                                                    JText::_('COM_JGIVE_GIVEBACK_DESC_TOOLTIP'),
                                                    JText::_('COM_JGIVE_GIVEBACK_DESC'),
                                                    '',
                                                    JText::_('COM_JGIVE_GIVEBACK_DESC')); ?>
                                            </label>
                                            <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                                <div id="giveback_des" class="well">
                                                    <?php echo $givebackDescription;?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } ?>

                                <?php if($params->get('recurring_donation'))
                                { ?>
                                    <!--Added by Sneha-->
                                    <?php if($show_field==1 OR $donation_type==0 ): ?>
                                    <!--Donation type -->
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label"  title="<?php
                                        echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_TYPE_TOOLTIP') : JText::_('COM_JGIVE_INVESTMENT_TYPE_TOOLTIP'));
                                        ?>">
                                            <?php
                                            echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATATION_TYPE') : JText::_('COM_JGIVE_INVESTMENT_TYPE'));
                                            ?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <label label-default class="radio-inline">
                                                <input type="radio" name="donation_type" id="donation_one_time" value="0" checked onclick="jGive_RecurDonation(0)">
                                                <?php echo JText::_('COM_JGIVE_ONE_TIME');?>
                                            </label>
                                            <label label-default class="radio-inline">
                                                <input type="radio" name="donation_type" id="donation_recurring" value="1" onclick="jGive_RecurDonation(1)" >
                                                <?php echo JText::_('COM_JGIVE_RECURRING');?>
                                            </label>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                    <!--recurring type -->
                                    <div id="recurring_freq_div" class="form-group jgive_display_none">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="recurring_freq" title="<?php
                                        echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATE_RECR_TYPE_TOOLTIP') : JText::_('COM_JGIVE_INVEST_RECR_TYPE_TOOLTIP'));
                                        ?>">
                                            <?php
                                            echo JText::_('COM_JGIVE_RECURRING_TYPE');
                                            ?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <select name="recurring_freq" id="recurring_freq">
                                                <option value="DAY"><?php echo JText::_('COM_JGIVE_RECUR_DAILY'); ?></option>
                                                <option value="WEEK"><?php echo JText::_('COM_JGIVE_RECUR_WEEKLY'); ?></option>
                                                <option value="MONTH"><?php echo JText::_('COM_JGIVE_RECUR_MONTHLY'); ?></option>
                                                <option value="QUARTERLY"><?php echo JText::_('COM_JGIVE_RECUR_QUARTERLY'); ?></option>
                                                <option value="YEAR"><?php echo JText::_('COM_JGIVE_RECUR_ANNUALLY'); ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <!--recurring times -->
                                    <div id="recurring_count_div" class="form-group jgive_display_none">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="recurring_count" title="<?php
                                        echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_RECUR_TIMES_TOOLTIP') : JText::_('COM_JGIVE_INVESTMENT_RECUR_TIMES_TOOLTIP'));
                                        ?>">
                                            <?php
                                            echo JText::_('COM_JGIVE_RECUR_TIMES');
                                            ?> *
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <div class="input-group input-group">
                                                <input type="number" id="recurring_count" name="recurring_count"
                                                       onblur='validateAmount(id,"<?php echo JText::_('COM_JGIVE_INVALID_RECURRING_TIMES'); ?>")'
                                                       class="validate-numeric"
                                                       placeholder="<?php
                                                       echo JText::_('COM_JGIVE_RECUR_TIMES');
                                                       ?>" min="2">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!-- vat number -->
                                <?php
                                $params=JComponentHelper::getParams('com_jgive');
                                if($params->get('vat_for_donor'))
                                {
                                    ?>
                                    <div class="form-group">
                                        <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label" for="vat_number" title="<?php
                                        echo JText::_('COM_JGIVE_VAT_NUMBER_TOOLTIP') ?>" >
                                            <?php
                                            echo JText::_('COM_JGIVE_VAT_NUMBER');
                                            ?>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                            <div class="input-group input-group">
                                                <input type="text" id="vat_number" name="vat_number"
                                                       placeholder="<?php
                                                       echo JText::_('COM_JGIVE_VAT_NUMBER'); ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                } ?>
                                <!--Added by Sneha-->
                                <?php
                                if ($this->cdata['campaign']->type == "donation")
                                {
                                    if ($show_field == 1 || $donation_anonym == 0 ): ?>
                                        <div class="form-group">
                                            <label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label"  title="<?php
                                            echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATE_ANONYMOUSLY_TOOLTIP') : JText::_('COM_JGIVE_INVEST_ANONYMOUSLY_TOOLTIP'));
                                            ?>">
                                                <?php
                                                echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATE_ANONYMOUSLY') : JText::_('COM_JGIVE_INVEST_ANONYMOUSLY'));
                                                ?>
                                            </label>
                                            <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                                <label label-default class="radio-inline">
                                                    <input type="radio" name="annonymousDonation" id="annonymousDonation1" value="1" >
                                                    <?php echo JText::_('COM_JGIVE_YES');?>
                                                </label>
                                                <label label-default class="radio-inline">
                                                    <input type="radio" name="annonymousDonation" id="annonymousDonation2" value="0" checked>
                                                    <?php echo JText::_('COM_JGIVE_NO');?>
                                                </label>
                                            </div>
                                        </div>
                                    <?php endif;
                                }?>
                                <?php

                                if($params->get('terms_condition'))
                                {
                                    $link='';
                                    if($params->get('payment_terms_article'))
                                        $link = JRoute::_(JUri::root()."index.php?option=com_content&view=article&id=".$params->get('payment_terms_article')."&tmpl=component" );
                                    if($link)
                                    {
                                        ?>
                                        <div class="form-group">
                                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label">
                                                <a rel="{handler: 'iframe', size: {x: 600, y: 600}}" href="<?php echo $link; ?>" class="modal jgive-bs3-modal">
                                                    <?php echo JText::_( 'COM_JGIVE_ACCEPT_TERMS' ); ?>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                                <div class="input-group">
                                                    <input  type="checkbox"  id="terms_condition" size="30"/>&nbsp;&nbsp;<?php echo JText::_( 'COM_JGIVE_YES' ); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="form-group">
                                    <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12">
                                        <input type="button" class="btn btn-primary" id="button-billing-info" value="<?php echo JText::_('COM_JGIVE_CONTINUE');?>" onclick="validatedatainsteps(this,'<?php echo "payment-info-tab" ?>',3,'billing-info')">
                                    </div>
                                </div>	<!-- END OF row-->
                            </div>
                        </div>
                    </div>
                    <!-- END OF billing_info_tab-->

                    <!--Start of Select Payment method  -->
                    <div id="payment-info" class="jgive-checkout-steps form-horizontal row">
                        <div class="checkout-heading">
                            <?php echo JText::_('COM_JGIVE_PAY_METHODS_TAB');?>
                        </div>

                        <div class="checkout-content" id="payment-info-tab">
                            <div id="payment_info-tab-method">
                                <div class="form-group">
                                    <label label-default for="state"
                                           class="col-lg-3 col-md-3 col-sm-3 col-xs-12 control-label">
                                        <?php echo JHtml::tooltip(JText::_('COM_JGIVE_PAY_METHODS_TAB'), JText::_('COM_JGIVE_PAY_METHODS'), '', JText::_('COM_JGIVE_PAY_METHODS'));?>
                                    </label>

                                    <div id="gatewaysContent" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    </div>
                                </div>

                                <input  type="button" class="btn btn-primary"  name="save" id="save"
                                        value="<?php echo JText::_('COM_JGIVE_CONTINUE_CONFIRM_FREE');?>"
                                        onclick="jGive_submitbutton('save');">

                                <input type="hidden" name="cid" value="<?php echo $this->cid;?>" />
                                <input type="hidden" name="option" value="com_jgive" />
                                <input type="hidden" name="view" value="donations" />
                                <input type="hidden" name="Itemid" value="<?php echo $input->get('Itemid','','INT');?>" />
                                <input type="hidden" name="order_id" id="order_id" value="0" />
                                <input type="hidden" name="task" value="donations.placeOrder" />
                                <input type="hidden" name="userid" id="userid" value="<?php if($user->id) echo $user->id;else echo '0';?>"/>

                                <div class="clearfix">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                    <!--EOF Select Payment method  -->
                </form>


                <!-- start confirm order -->
                <div id="confirm-order" class="jgive-checkout-steps row">
                    <div class="checkout-heading">
                        <?php echo JText::_('COM_JGIVE_PAYMENT_INFO');?>
                        <span class="pull-right" id="jgive_order_details_tab">
					<a href="javascript:void('0');" onclick="jgive_toggleOrder('order_summary_tab_table')"></a>
				</span>
                    </div>
                    <div class="checkout-content"  id="payment_tab">
                        <div   id="order_summary_tab_table"  class="table table-striped- table-hover">
                            <div  id="order_summary_tab_table_html" >
                            </div>
                        </div>

                        <!--start of payment tab-->
                        <div   id="payment_tab_table">
                            <div class="checkout-heading">
                                <?php echo JText::_('COM_JGIVE_PAY_FORM'); ?>
                            </div>
                            <div  id="payment_tab_table_html" class="">
                            </div>
                        </div>
                        <!--end of payment tab-->
                    </div>
                </div>
                <!-- EOF of confirm order -->
            </div>
        </div>
        <!-- EOF of tjBs3 -->

        <script>

            // Javascript global variable
            var decimal_separator = "<?php echo $params->get('amount_separator'); ?>";

        </script>

    </div>

</div>

<script>
    jQuery("#donation_amount").keyup(function(){
        var val = this.value;
        if (val.length > 0) {
            var fees = (val*2.9)/100 + 0.3;
            var fee = fees.toFixed(2);
            jQuery('#paypal_fees .fees').show();
            jQuery('#paypal_fees .fees span').html("$" + fee);
        }
    });


    var jgive_baseurl = "<?php echo JUri::root(); ?>";

    var minimum_amount = "<?php echo $cdata['campaign']->minimum_amount;?>";
    minimum_amount = parseInt(minimum_amount);

    var send_payments_to_owner = "<?php echo $this->params->get('send_payments_to_owner');?>";
    var commission_fee = "<?php echo $this->params->get('commission_fee');?>";
    var fixed_commissionfee = "<?php echo $this->params->get('fixed_commissionfee');?>";

    var givebackDetails = <?php echo !empty($this->campaignGivebacks) ? json_encode($this->campaignGivebacks, 1) : 0;?>;
</script>
<style>
    #es .support_a_family_page{
        background: #fff;
        padding: 20px;
    }
    #es .page-header{
        display: none;
    }
    #es .support_a_family_page .row{
        margin-left: 0px;
    }
    #es .support_a_family_page .enter_amount_label{
        float: left;
        width: 20%;
        margin-top: 10px;
    }
    #es .support_a_family_page .enter_amount_input{
        width: 80%;
        float: left;
    }
    #es .support_a_family_page #payment_tab_table_html .form-actions{
        padding: 18px 0;
        width: 100%;
        text-align: center;
    }
</style>