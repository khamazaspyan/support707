<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

ES::import('admin:/includes/apps/apps');

class SocialPageAppSupportfamily extends SocialAppItem
{
	/**
	 * Check if EasyBlog exists on the site.
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function exists()
	{
		$file = JPATH_ROOT . '/administrator/components/com_easyblog/includes/easyblog.php';

		if (!JFile::exists($file)) {
			return false;
		}

		require_once($file);

		return true;
	}

	/**
	 * Prepares the story panel for page story form
	 *
	 * @since	2.0
	 * @access	public
	 */
	
	/**
	 * Before saving the blog post, perform validation checks here.
	 *
	 * @since	2.0
	 * @access	public
	 */
	public function onBeforeStorySave(&$template, &$stream, &$content)
	{
		if (!$this->exists() || $template->context_type != 'blog') {
			return;
		}

		$author = ES::user();

		// Retrieve the post data
		$title = $this->input->get('blog_title', '', 'default');
		$content = $this->input->get('blog_content', '', 'default');
		$category = $this->input->get('blog_categoryId', 0, 'int');
		$sourceId = $this->input->get('blog_sourceId', 0, 'int');
		$sourceType = $this->input->get('blog_sourceType', '', 'default');

		if (!$title) {
			return false;
		}

		$post = EB::post();

		$data = new stdClass();
		$data->title = $title;
		$data->content = $content;
		$data->category_id = $category;
		$data->created_by = $author->id;

		// Get the current date
		$current = ES::date();

		$data->published = 1;
		$data->created = $current->toSql();
		$data->modified = $current->toSql();
		$data->publish_up = $current->toSql();
		$data->publish_down	= '0000-00-00 00:00:00';
		$data->frontpage = 1;
		$data->allowcomment = 1;
		$data->subscription = 1;
		$data->source_id = $sourceId;
		$data->source_type = $sourceType;

		$post->create(array('overrideDoctType' => 'legacy'));

		// Retrieve the uid
		$data->uid = $post->uid;
		$data->revision_id = $post->revision->id;

		// binding
		$post->bind($data, array());

		$saveOptions = array(
						'applyDateOffset' => false,
						'validateData' => false,
						'useAuthorAsRevisionOwner' => true,
						'saveFromEasysocialStory' => true
					);

		$post->save($saveOptions);

		$template->context_type = 'blog';

		$template->context_id = $post->id;

		return;
	}

	/**
	 * Prepare stream item
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function onPrepareStream(SocialStreamItem &$item, $includePrivacy = true)
	{
		// Ensure that we're in the correct context
		if ($item->context != 'blog' || !$this->exists()) {
			return;
		}

		// Get the page
		$page = ES::page($item->cluster_id);

		// Check if the viewer can really view the item
		if (!$page->canViewItem()) {
			return;
		}

		// Attach our own stylesheet
		$this->getApp()->loadCss();

		// Define standard stream looks
		$item->display = SOCIAL_STREAM_DISPLAY_FULL;

		// New blog post
		if ($item->verb == 'create') {
			$this->prepareNewBlogStream($item);
		}

		// Updated blog post
		if ($item->verb == 'update') {
			$this->prepareUpdateBlogStream($item);
		}
	}

	/**
	 * Generates the stream for updated stream item
	 *
	 * @since   2.0
	 * @access  public
	 */
	private function prepareUpdateBlogStream(&$item)
	{
		$post = EB::post($item->contextId);

		// Post could be deleted from the site by now.
		if (!$post->id) {
			return;
		}

		if (!$post->getPrimaryCategory()) {
			return;
		}

		// Format the likes for the stream
		$likes = ES::likes();
		$likes->get($item->contextId, 'blog', 'update');
		$item->likes = $likes;

		$url = EBR::_('index.php?option=com_easyblog&view=entry&id=' . $post->id);

		// Apply comments on the stream
		$item->comments = ES::comments($item->contextId, 'blog', 'update', SOCIAL_APPS_GROUP_USER, array('url' => $url));

		// We might want to use some javascript codes.
		EB::init('site');

		$date = EB::date($post->created);

		$content = $post->getIntro(true, true, 'all', null, $options = array('triggerPlugins' => false));

		$appParams = $this->getParams();
		$alignment = 'pull-' . $appParams->get('imagealignment', 'right');
		$this->set('alignment', $alignment);

		$contentLength  = $appParams->get('maxlength');

		if ($contentLength > 0) {
			// truncate the content
			$content = $this->truncateStreamContent($content, $contentLength);
		}

		

		// See if there's any audio files to process.
		$audios = EB::audio()->getItems($content);

		// Remove videos from the source
		$content = EB::videos()->strip($content);

		// Remove audios from the content
		$content = EB::audio()->strip($content);

		$catUrl = EBR::_('index.php?option=com_easyblog&view=categories&layout=listings&id=' . $post->category_id, true, null, false, true);

		$this->set('categorypermalink', $catUrl);
		$this->set('audios', $audios);
		$this->set('date', $date);
		$this->set('permalink', $url);
		$this->set('post', $post);
		$this->set('actor', $item->actor);
		$this->set('content', $content);

		$item->title = parent::display('streams/' . $item->verb . '.title');
		$item->content = parent::display('streams/preview');

		// Append the opengraph tags
		if ($post->getImage()) {
			$item->addOgImage($post->getImage('thumbnail'));
		}

		$item->addOgDescription($content);
	}


	/**
	 * Displays the stream item for new blog post
	 *
	 * @since	2.0
	 * @access	public
	 */
	private function prepareNewBlogStream(&$item)
	{
		// Load the blog post
		$post = EB::post($item->contextId);

		if (!$post->id) {
			return;
		}

		// Format the likes for the stream
		$likes = ES::likes();
		$likes->get($item->contextId, 'blog', 'create');

		$item->likes = $likes;

		// Get the permalink to the blog post
		$permalink = $post->getExternalPermalink();

		// Apply comments on the stream
		$item->comments = ES::comments($item->contextId, 'blog', 'create', SOCIAL_APPS_GROUP_PAGE, array('url' => $permalink));

		// Get the app params
		$params = $this->getParams();

		// Get the alignment that should be used for the image
		$alignment = 'pull-' . $params->get('imagealignment', 'right');

		// Get the content
		$content = $post->getIntro(true, true, 'all', null, $options = array('triggerPlugins' => false));

		$contentLength  = $params->get('maxlength');

		if ($contentLength > 0){
			// truncate the content
			$content = $this->truncateStreamContent($content, $contentLength);
		}
		
		$page = ES::page($item->cluster_id);
		$access = $page->getAccess();
		if ($this->my->isSiteAdmin() || $page->isAdmin() || ($access->get('stream.edit', 'admins') == 'members' && $item->actor->id == $this->my->id)) {
			$item->edit_link = EB::composer()->getComposeUrl(array('uid' => $post->id));
		}

		// See if there's any audio files to process.
		$audios = EB::audio()->getItems($content);

		$this->set('alignment', $alignment);
		$this->set('audios', $audios);
		$this->set('post', $post);
		$this->set('actor', $item->actor);
		$this->set('content', $content);

		$item->title = parent::display('streams/' . $item->verb . '.title');
		$item->content = parent::display('streams/preview');

		// Add image to the og:image
		$item->addOgImage($post->getImage('thumbnail'));
		$item->addOgDescription($content);
	}

	/**
	 * Truncate stream item
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function truncateStreamContent($content, $contentLength)
	{
		// Get the app params
		$params = $this->getParams();
		$truncateType = $params->get('truncation');

		if ($truncateType == 'chars') {

			// Remove uneccessary html tags to avoid unclosed html tags
			$content = strip_tags($content);

			// Remove blank spaces since the word calculation should not include new lines or blanks.
			$content = trim($content);

			// @task: Let's truncate the content now.
			$content = JString::substr(strip_tags($content), 0, $contentLength) . JText::_('COM_EASYSOCIAL_ELLIPSES');

		} else {

			$tag = false;
			$count = 0;
			$output = '';

			// Remove uneccessary html tags to avoid unclosed html tags
			$content = strip_tags($content);

			$chunks = preg_split("/([\s]+)/", $content, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

			foreach($chunks as $piece) {

				if (!$tag || stripos($piece, '>') !== false) {
					$tag = (bool) (strripos($piece, '>') < strripos($piece, '<'));
				}

				if (!$tag && trim($piece) == '') {
					$count++;
				}

				if ($count > $contentLength && !$tag) {
					break;
				}

				$output .= $piece;
			}

			unset($chunks);
			$content = $output;
		}

		return $content;
	}
}
