<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div data-field-currency>
	<div class="o-grid">
		<div class="o-grid__cell t-lg-pr--md">
			<div class="o-input-group">
				<input type="text" class="o-form-control" name="<?php echo $inputName; ?>[dollar]" value="<?php echo $dollar; ?>" />
				<span class="o-input-group__addon"><?php echo $dollarsLabel;?></span>
			</div>
		</div>

		<div class="o-grid__cell">
			<div class="o-input-group">
				<input type="text" class="o-form-control" name="<?php echo $inputName; ?>[cent]" value="<?php echo $cent; ?>" />
				<span class="o-input-group__addon"><?php echo $centsLabel; ?></span>
			</div>
		</div>
	</div>
</div>
