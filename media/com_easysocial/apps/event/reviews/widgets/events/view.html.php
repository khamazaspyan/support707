<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

class ReviewsWidgetsEvents extends SocialAppsWidgets
{
	/**
	 * Display admin actions for the event
	 *
	 * @since	2.1
	 * @access	public
	 */
	public function eventAdminStart($event)
	{
		if (!$this->app->hasAccess($event->category_id) || !$event->getParams()->get('reviews', true)) {
			return;
		}

		$theme = ES::themes();
		$theme->set('app', $this->app);
		$theme->set('cluster', $event);

		echo $theme->output('themes:/site/reviews/widgets/menu');
	}

	/**
	 * Display additional meta in header of the event
	 *
	 * @since   2.1
	 * @access  public
	 */
	public function headerMeta($event)
	{
		if (!$this->app->hasAccess($event->category_id) || !$event->getParams()->get('reviews', true)) {
			return;
		}

		$theme = ES::themes();
		$theme->set('cluster', $event);

		echo $theme->output('themes:/site/reviews/widgets/header');
	}

	/**
	 * Display user photos on the side bar
	 *
	 * @since	2.1
	 * @access	public
	 */
	public function sidebarBottom($eventId)
	{
		// Set the max length of the item
		$params = $this->app->getParams();
		$enabled = $params->get('widget', true);
		$event = ES::event($eventId);

		if (!$enabled || !$this->app->hasAccess($event->category_id)) {
			return;
		}

		$theme = ES::themes();

		$options = array('limit' => (int) $params->get('widgets_total', 5));

		$model = ES::model('Reviews');
		$items = $model->getReviews($event->id, SOCIAL_TYPE_EVENT, $options);
		$total = $model->getTotalReviews($event->id, SOCIAL_TYPE_EVENT);

		if (!$items) {
			return;
		}
		
		$theme->set('total', $total);
		$theme->set('cluster', $event);
		$theme->set('app', $this->app);
		$theme->set('items', $items);

		echo $theme->output('themes:/site/reviews/widgets/reviews');
	}
}
