<?php
/**
* @package      EasySocial
* @copyright    Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="app-members app-events" data-es-event-guests data-id="<?php echo $event->id; ?>" data-return="<?php echo $returnUrl;?>">

	<div class="es-container" data-es-container>

		<div class="es-content">
			<div class="app-contents-wrap">

				<div class="fbstfrnds">
					<div class="fbstfrnds_hd">
						<div class="fbstfrnds_hd_title">
							<?php echo $this->html('widget.title', 'COM_ES_FILTERS'); ?>
						</div>
						<div class="fbstfrnds_hd_subtitle">
							<ul class="o-tabs o-tabs--stacked feed-items">

								<li class="o-tabs__item has-notice <?php echo $active == 'going' ? ' active' : '';?>" data-filter data-type="going">
									<a href="javascript:void(0);" class="o-tabs__link">
										<?php echo JText::_('APP_EVENT_GUESTS_FILTER_GOING');?>
										<div class="o-tabs__bubble" data-counter><?php echo $counters['going'];?></div>
									</a>
								</li>

								<?php if ($event->getParams()->get('allowmaybe', true)) { ?>
								<li class="o-tabs__item has-notice <?php echo $active == 'maybe' ? ' active' : '';?>" data-filter data-type="maybe">
									<a href="javascript:void(0);" class="o-tabs__link">
										<?php echo JText::_('APP_EVENT_GUESTS_FILTER_MAYBE');?>
										<div class="o-tabs__bubble" data-counter><?php echo $counters['maybe'];?></div>
									</a>
								</li>
								<?php } ?>

								<?php if ($event->getParams()->get('allownotgoingguest', true)) { ?>
								<li class="o-tabs__item has-notice <?php echo $active == 'notgoing' ? ' active' : '';?>" data-filter data-type="notgoing">
									<a href="javascript:void(0);" class="o-tabs__link">
										<?php echo JText::_('APP_EVENT_GUESTS_FILTER_NOTGOING');?>
										<div class="o-tabs__bubble" data-counter><?php echo $counters['notgoing'];?></div>
									</a>
								</li>
								<?php } ?>

								<li class="o-tabs__item has-notice <?php echo $active == 'admin' ? ' active' : '';?>" data-filter data-type="admin">
									<a href="javascript:void(0);" class="o-tabs__link">
										<?php echo JText::_('APP_EVENT_GUESTS_FILTER_ADMINS');?>
										<div class="o-tabs__bubble" data-counter><?php echo $counters['admins'];?></div>
									</a>
								</li>

								<?php if ($event->isClosed() && ($event->isAdmin() || $event->isOwner())) { ?>
								<li class="o-tabs__item <?php echo $active == 'pending' ? ' active' : '';?> <?php echo $counters['pending'] ? 'has-notice' : '';?>" data-filter data-type="pending">
									<a href="javascript:void(0);" class="o-tabs__link">
										<?php echo JText::_('APP_EVENT_GUESTS_FILTER_PENDING');?>
										<div class="o-tabs__bubble" data-counter><?php echo $counters['pending'];?></div>
									</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="fbstfrnds_bd">
					<div class="o-input-group">
						<input type="text" class="o-form-control" data-search-input placeholder="<?php echo JText::_('APP_EVENT_GUESTS_SEARCH_GUESTS'); ?>" />
					</div>
					<div class="t-lg-mt--xl" data-wrapper>
						<?php echo $this->html('html.loading'); ?>

						<div data-result class="fbstfrnds_bd1">
							<?php echo $this->includeTemplate('apps/event/guests/events/list'); ?>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
