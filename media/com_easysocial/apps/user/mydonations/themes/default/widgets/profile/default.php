<?php
/**
* @version		1.0
* @package		DJ Classifieds
* @subpackage	DJ Classifieds and EasySocial integration
* @copyright	Copyright (C) 2010 DJ-Extensions.com LTD, All rights reserved.
* @license		http://www.gnu.org/licenses GNU/GPL
* @autor url    http://design-joomla.eu
* @autor email  contact@design-joomla.eu
* @Developer    Lukasz Ciastek - lukasz.ciastek@design-joomla.eu
* 
* 
* DJ Classifieds is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DJ Classifieds is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DJ Classifieds. If not, see <http://www.gnu.org/licenses/>.
* 
*/

defined( '_JEXEC' ) or die( 'Unauthorized Access' );
if(!defined("DS")){define('DS',DIRECTORY_SEPARATOR);}
require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_djclassifieds'.DS.'lib'.DS.'djseo.php');
require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_djclassifieds'.DS.'lib'.DS.'djtheme.php');

$cols = $params->get( 'widget-columns-limit',1);	
?>
<div class="es-widget adverts">
	<div class="es-widget-head">
		<div class="pull-left widget-title">
			<?php echo JText::_( 'APP_DJCFITEMS_WIDGET_TITLE' ); ?>
		</div>
	</div>
	<div class="es-widget-body">
		<?php if( $items ){ ?>
			<div class="djcf_items items-cols<?php echo $cols; ?>" >
				<?php foreach( $items as $item ){ ?>
					<div class="item">
						<?php if($params->get( 'widget-display-image') || $params->get( 'widget-display-name')  ){ ?>
							<div class="title">
							<?php if(count($item->images) && $params->get( 'widget-display-image') ){																						
									echo '<a class="title_img" href="'.JRoute::_(DJClassifiedsSEO::getItemRoute($item->id.':'.$item->alias,$item->cat_id.':'.$item->c_alias)).'">';
									echo '<img style="margin-right:3px;" src="'.JURI::base().$item->images[0]->thumb_s.'" alt="'.str_ireplace('"', "'", $item->name).'" title="'.str_ireplace('"', "'", $item->images[0]->caption).'" />';
									echo '</a>';
								} ?>
								<?php if($params->get( 'widget-display-name')  ){ 																											
									echo '<a class="title" href="'.JRoute::_(DJClassifiedsSEO::getItemRoute($item->id.':'.$item->alias,$item->cat_id.':'.$item->c_alias)).'">'.$item->name.'</a>';
								} ?>	
							</div>				
						<?php } ?>		
						<div class="date_cat">
							<?php if($params->get( 'widget-display-pdate')  ){ ?>								
								<span class="date">							
									<?php if($cfpar->get('date_format_type_modules',0)){
										echo DJClassifiedsTheme::dateFormatFromTo(strtotime($item->date_start));	
									}else{
										echo date($cfpar->get('date_format','Y-m-d H:i:s'),strtotime($item->date_start));	
									}?>
								</span>
							<?php } ?>											
							<?php if($params->get( 'widget-display-pdate')  ){ ?>					
								<span class="category">																			
									<?php echo '<a class="title_cat" href="'.JRoute::_(DJClassifiedsSEO::getCategoryRoute($item->cat_id.':'.$item->c_alias)).'">'.$item->c_name.'</a>'; ?>												
								</span>
							<?php } ?>											
							<?php if($params->get( 'widget-display-region')  ){ ?>																														
								<span class="region">
									<?php echo $item->r_name; ?>
								</span>
							<?php } ?>											
													
							<?php if($item->price && $params->get( 'widget-display-price')){ ?>
								<span class="price">
									<?php echo DJClassifiedsTheme::priceFormat($item->price,$item->currency); ?>
								</span>
							<?php } ?>
						</div>
						<?php if($params->get( 'widget-display-desc')  ){
								$desc_c = $params->get('widget-desc-chars-limit');
								if($desc_c!=0 && $item->intro_desc!='' && strlen($item->intro_desc)>$desc_c){
									$item_desc = mb_substr($item->intro_desc, 0, $desc_c,'utf-8').' ...';
								}else{
									$item_desc = $item->intro_desc;
								}
							 ?>																												
							<div class="desc">																	
								<?php echo $item_desc; ?>		
							</div>
						<?php } ?>							
					</div>							
				<?php } ?>
				<div style="clear:both"></div>
			</div>
		<?php } else { ?>
			<p><?php echo JText::_('APP_DJCFITEMS_WIDGET_NO_ADVERTS_FOUND'); ?></p>
		<?php } ?>
	</div>	
</div>