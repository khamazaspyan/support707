<?php
/**
 * @package		EasySocial
 * @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * EasySocial is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
$jgiveFrontendHelper = new jgiveFrontendHelper;
$user = JFactory::getUser();
$input = JFactory::getApplication()->input;

$session       = JFactory::getSession();
$this->session = $session;

$this->params          = $params = JComponentHelper::getParams('com_jgive');
$show_selected_fields_on_donate = $this->params->get('show_selected_fields_on_donation');
$donationfield = array();
$show_field = 0;
$donation_anonym = 0;

if ($show_selected_fields_on_donate)
{
    $donationfield = $this->params->get('donationfield');

    if (isset($donationfield))
    {
        if (in_array("donation_anonym", $donationfield))
        {
            $donation_anonym = 1;
        }
    }
}
else
{
    $show_field = 1;
}
// Fetched data


$lang = JFactory::getLanguage();
$extension = 'com_jgive';
$base_dir = JPATH_SITE;
$language_tag = 'en-GB';
$reload = true;
$lang->load($extension, $base_dir, $language_tag, $reload);
$db = JFactory::getDBO();
$cdata_array = $db->setQuery("SELECT * FROM #__jg_campaigns WHERE id='".$campaign_id."'")->loadObject();
$cdata['campaign'] = $cdata_array;
$cdata['images'] = array();
$cdata['givebacks'] = array();


$dispatcher = JDispatcher::getInstance();
JPluginHelper::importPlugin('payment');

$params = JComponentHelper::getParams('com_jgive');

if (!is_array($params->get('gateways')))
{
    $gateway_param[] = $params->get('gateways');
}
else
{
    $gateway_param = $params->get('gateways');
}

if (!empty($gateway_param))
{
    $gateways = $dispatcher->trigger('onTP_GetInfo', array($gateway_param));
}

$this->gateways = $gateways;




jimport('joomla.filesystem.file');

jimport('joomla.filesystem.folder');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.modal', 'a.modal');
$input=JFactory::getApplication()->input;


$campaign_title=$this->cdata['campaign']->title;
$pagetitle=JText::sprintf('COM_JGIVE_CHECKOUT_TITLE',$campaign_title);
$document=JFactory::getDocument();
$document->addScript(JUri::root(true).'/media/com_jgive/javascript/jgive.js');
$document->addStyleSheet(JUri::root(true).'/media/com_jgive/css/jgive_bs3.css');
$document->addStyleSheet(JUri::root(true).'/media/techjoomla_strapper/bs3/css/bootstrap.css');
$document->addScript(JUri::root(true).'/media/com_jgive/javascript/fields_validation.js');
$document->addScript(JUri::root(true).'/media/com_jgive/javascript/donations.js');

$document->setTitle($pagetitle);
$baseurl=JRoute::_ (JUri::root().'index.php');

$params=JComponentHelper::getParams('com_jgive');

$user = JFactory::getuser();
$es_user = ES::user($user->id);
$isguest=$user->id;

// Get the data to idetify which field to show on donation view
$show_selected_fields_on_donate=$params->get('show_selected_fields_on_donation');
$donationfield=array();

$show_field=0;
$first_name=1;
$last_name=1;
$email=1;
$address=1;
$address2=1;
$hide_country=1;
$state=1;
$city=1;
$zip=1;
$phone_no=1;
$donation_type=0;
$donation_anonym=0;

if ($show_selected_fields_on_donate)
{
    $donationfield=$params->get('donationfield');

    if(isset($donationfield))
        foreach($donationfield as $tmp)
        {
            switch($tmp)
            {
                case 'first_name':
                    $first_name=1;
                    break;

                case 'last_name':
                    $last_name=1;
                    break;

                case 'email':
                    $email=1;
                    break;

                case 'address':
                    $address=1;
                    break;

                case 'address2':
                    $address2=1;
                    break;

                case 'country':
                    $hide_country=1;
                    break;

                case 'state':
                    $state=1;
                    break;

                case 'city':
                    $city=1;
                    break;

                case 'zip':
                    $zip=1;
                    break;


                case 'donation_type':
                    $donation_type=1;
                    break;


            }
        }
}
else
{
    $show_field=1;
}

if($user->id)
{
    $registr_field_display="display:none";
}
else
{
    $registr_field_display="display:display";
}

if(empty($isguest))
{
    $isguest=1;
}
else
{
    $isguest=0;
}
$this->donations = $donations;
$result=$this->donations;
?>

<?php if($pageId == $my->id): ?>

<div class="es-container" data-mydonations>
    <div class="es-content support_a_family_page" style="margin-bottom: 20px;">

        <div id="jgive_my_donations" style="background: #fff;">
            <div class="col-lg-12 col-md-12 col-sm-12  col-xs-12">

                <form action="" name="adminForm" id="adminForm" class="form-validate" method="post">
                    <!-- show pagination limit box and filters -->


                    <!-- show message if no items found -->
                    <?php if (!count($this->donations))
                    { ?>
                        <div class="alert alert-warning"><?php echo JText::_('COM_JGIVE_NO_DATA_FOUND');?></div>
                        <?php
                    }
                    ?>

                    <?php if (count($this->donations))
                    { ?>
                        <div class="no-more-tables">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center com_jgive_td_center com_jgive_width1">
                                        <?php echo JText::_('COM_JGIVE_NO'); ?>
                                    </th>
                                    <th>
                                        <?php echo JText::_( 'COM_JGIVE_DONATION_ID'); ?>
                                    </th>
                                    <th class="nowrap center com_jgive_td_center com_jgive_width10">
                                        <?php echo JText::_( 'COM_JGIVE_DONATION_STATUS'); ?>
                                    </th>
                                    <th class="nowrap center com_jgive_td_center com_jgive_width10">
                                        <?php echo JText::_( 'COM_JGIVE_DONATION_DATE'); ?>
                                    </th>
                                    <th class="nowrap com_jgive_align_right com_jgive_width10">
                                        <?php echo JText::_( 'COM_JGIVE_AMOUNT'); ?>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $id=1;
                                foreach($result as $donations)
                                {
                                    ?>
                                    <tr>
                                        <td class="center com_jgive_td_center com_jgive_width1" data-title="<?php echo JText::_("COM_JGIVE_NO"); ?>">
                                            <?php echo $id++;?>
                                        </td>
                                        <td class="small" data-title="<?php echo JText::_("COM_JGIVE_DONATION_ID"); ?>">
                                            <?php

                                            if(!$donations->order_id)
                                            {
                                                $donations->order_id=$donations->id;
                                            }
                                            ?>
                                            <a target="_blank" href="<?php echo JRoute::_('index.php?option=com_jgive&view=donations&layout=details&donationid='.$donations->id.'&Itemid='.$Itemid); ?>">
                                                <?php echo $donations->order_id; ?>
                                            </a>
                                        </td>

                                        <td class="nowrap small center com_jgive_td_center com_jgive_width10" data-title="<?php echo JText::_("COM_JGIVE_DONATION_STATUS"); ?>">
                                            <?php
                                            $whichever = '';

                                            switch($donations->status)
                                            {
                                                case 'C' :
                                                    $whichever =  JText::_('COM_JGIVE_CONFIRMED');
                                                    $class="success";
                                                    break;
                                                case 'RF' :
                                                    $whichever = JText::_('COM_JGIVE_REFUND') ;
                                                    $class="error";
                                                    break;
                                                case 'E' :
                                                    $whichever = JText::_('COM_JGIVE_CANCELED') ;
                                                    $class="error";
                                                    break;
                                                case 'P' :
                                                    $whichever = JText::_('COM_JGIVE_PENDING') ;
                                                    $class="warning";
                                                    break;

                                                case 'D' :
                                                    $whichever = JText::_('COM_JGIVE_DENIED') ;
                                                    $class="error";
                                                    break;
                                            }
                                            ?>
                                            <span class="small not-es-badge badge-<?php echo $class;?>">
										<?php
                                        echo $whichever;
                                        ?>
									</span>
                                            <?php
                                            if($donations->status == 'P' || $donations->status == 'E')
                                            {?>
                                                <a target="_blank" class="btn btn-primary btn-xs mrg-lft-bt" href="<?php echo JRoute::_('index.php?option=com_jgive&view=donations&layout=details&donationid='.$donations->id.'&Itemid='.$Itemid);?>">
                                                    <small>	<?php echo JText::_('COM_JGIVE_RETRY_DONATION');?></small>
                                                </a>
                                                <?php
                                            }?>
                                        </td>
                                        <td class="nowrap small center com_jgive_td_center com_jgive_width10" data-title="<?php echo JText::_("COM_JGIVE_DONATION_DATE"); ?>">
                                            <?php echo JHtml::_('date', $donations->cdate, JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));?>
                                        </td>
                                        <td class="nowrap small com_jgive_align_right com_jgive_width10" data-title="<?php echo JText::_("COM_JGIVE_AMOUNT"); ?>">
                                            <?php
                                            $jgiveFrontendHelper=new jgiveFrontendHelper();
                                            $diplay_amount_with_format=$jgiveFrontendHelper->getFormattedPrice($donations->amount);
                                            echo $diplay_amount_with_format;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                } // End for.
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<span class="pull-right">
							<strong><?php echo JText::_( 'COM_JGIVE_TOTAL_DONATION');?></strong>
                            <?php
                            $totalpaid=0;

                            if(!empty($result))
                            {
                                foreach($result as $data)
                                {
                                    $totalpaid=$totalpaid+$data->amount;
                                }
                            }

                            $jgiveFrontendHelper=new jgiveFrontendHelper();
                            $diplay_amount_with_format=$jgiveFrontendHelper->getFormattedPrice($totalpaid);
                            echo $diplay_amount_with_format;
                            ?>
							</span>

                        </div>

                        <hr class="hr hr-condensed"/>
                        <?php
                    }
                    ?>

                    <?php $class_pagination='pagination';?>

                    <div class="<?php echo $class_pagination; ?> pull-right">
                        <p class="<?php //echo $this->pagination->getPagesCounter() ? 'counter pull-right' : ''; ?>">
                            <?php //echo $this->pagination->getPagesCounter(); ?>
                        </p>
                        <?php //echo $this->pagination->getPagesLinks(); ?>
                    </div>

                    <input type="hidden" name="option" value="com_jgive" />
                    <input type="hidden" id='hidid' name="id" value="" />
                    <input type="hidden" id='hidstat' name="status" value="" />
                    <input type="hidden" name="task" id="task" value="" />
                    <input type="hidden" name="view" value="donations" />
                    <!--<input type="hidden" name="controller" value="donations" />-->
                    <input type="hidden" name="boxchecked" value="0" />
                    <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
                    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />

                </form>
            </div>
        </div>

    </div>

</div>

<?php endif; ?>