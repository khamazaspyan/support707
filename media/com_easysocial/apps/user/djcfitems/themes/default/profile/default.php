<?php

/**

* @version		1.0

* @package		DJ Classifieds

* @subpackage	DJ Classifieds and EasySocial integration

* @copyright	Copyright (C) 2010 DJ-Extensions.com LTD, All rights reserved.

* @license		http://www.gnu.org/licenses GNU/GPL

* @autor url    http://design-joomla.eu

* @autor email  contact@design-joomla.eu

* @Developer    Lukasz Ciastek - lukasz.ciastek@design-joomla.eu

* 

* 

* DJ Classifieds is free software: you can redistribute it and/or modify

* it under the terms of the GNU General Public License as published by

* the Free Software Foundation, either version 3 of the License, or

* (at your option) any later version.

*

* DJ Classifieds is distributed in the hope that it will be useful,

* but WITHOUT ANY WARRANTY; without even the implied warranty of

* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

* GNU General Public License for more details.

*

* You should have received a copy of the GNU General Public License

* along with DJ Classifieds. If not, see <http://www.gnu.org/licenses/>.

* 

*/



defined( '_JEXEC' ) or die( 'Unauthorized Access' );

$cols = $params->get( 'view-columns-limit',1);

$uid_slug = $user->id.':'.DJClassifiedsSEO::getAliasName($user->name);	
$me = ES::user();
//echo '<pre>';print_r($user);die();

?>
<div id="dj-classifieds">

    <?php if($items): ?>
<div class="dj-items">
    <div class="dj-items-table-smart">


        <div class="dj-items-rows">
            <?php foreach($items as $item): ?>

                <div class="item_row item_row0"><div class="item_row_in">


                        <div class="item_outer">
                            <div class="item_outer_in">
                                <div class="item_img_box">
                                    <div class="item_img_box_in">
                                        <a href="/index.php/free-items?id=<?php echo $item->id; ?>">
                                            <img src="<?php echo $item->images[0]->thumb_m; ?>" alt="<?php echo $item->name;?>">			</a>									</a>
                                    </div>
                                </div>
                                <div class="item_content">
                                    <div class="item_content_in">
                                        <div class="item_title">
                                            <h3>
                                                <a class="title Tips1" href="<?php echo $current_url.'?id='.$item->id;?>" title="<?php echo $item->name;?>" ><?php echo $item->name;?></a>
                                            </h3>
                                        </div>

                                        <div class="item_cat_region_outer">
                                            <div class="item_date">
                                                <i class="fa fa-clock-o "></i>
                                                <span><?php echo JHTML::_('date', $item->date_start , JText::_('DATE_FORMAT_LC3')); ?></span>
                                            </div>
                                            <div class="item_category">
                                                <a href="/index.php/free-items?cat=<?php echo $item->c_alias;?>"><i style="padding-right: 4px;" class="fa fa-list-ul"></i><?php echo $item->c_name;?></a>														</div>
                                            <?php if($item->address): ?>
                                                <div class="item_region">
                                                    <a class="" target="_blank" href="https://www.google.com/maps/?q=<?php echo $item->latitude;?>,<?php echo $item->longitude;?>"><i style="padding-right: 6px;" class="fa fa-location-arrow"></i><?php echo $item->address; ?></a>
                                                </div>
                                            <?php endif; ?>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
    <?php endif; ?>
</div>



