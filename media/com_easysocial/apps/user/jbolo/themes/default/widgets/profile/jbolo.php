<?php
/**
 * @package		JBolo
 * @version		$versionID$
 * @author		TechJoomla
 * @author mail	extensions@techjoomla.com
 * @website		http://techjoomla.com
 * @copyright	Copyright © 2009-2013 TechJoomla. All rights reserved.
 * @license		GNU General Public License version 2, or later
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

$my=JFactory::getUser();

if ($my->id==$user->id) // Not on own profile
{
  return;
}

//Not to show user status if one of both, blocked one to one chat
if($isBlocked)
{
	return;
}
?>
<div class="es-widget">
	<div class="es-widget-head">
		<div class="pull-left widget-title">
			<?php 
			  echo JText::_( 'APP_JBOLO_ONLIE_TITLE' ); 
			?>
		</div>
	</div>
	<div class="es-widget-body">

	<?php
	
	$db=JFactory::getDBO();
	global $return;
	$accepted=0;
	$show_to_friends=0;//show public
	$profile_show_friends;

	if ($profile_show_friends)
	{
		$show_to_friends=1;
	}
	else if($app_fonly)
	{
		$show_to_friends=1;
	}
	else if($fonly)
	{
		$show_to_friends=1;
	}

	if($show_to_friends)// If jbolo config is set to friends only chat
	{
		// Connection request sent  by logged in user
		$query="SELECT sf.state
		FROM #__social_friends AS sf
		WHERE sf.state=1
		AND sf.actor_id=".$my->id."
		AND sf.target_id=".$user->id;
		$db->setQuery($query);
		$acceptedLIU=$db->loadResult();

		// Connection request accepted by other user
		$query="SELECT sf.state
		FROM #__social_friends AS sf
		WHERE sf.state=1
		AND sf.target_id=".$my->id."
		AND sf.actor_id=".$user->id;
		$db->setQuery($query);
		$acceptedOU=$db->loadResult();

		if($acceptedLIU OR $acceptedOU){
			$accepted=1;
		}
	}
	else // Jbolo config is set to chat with everyone
	{
		$accepted=1;
	}

	if ($accepted) // If logged in and if conncection
	{
		$my=JFactory::getUser();

		if ($my->id!=$user->id) // Not on own profile
		{

			if ($chatusertitle)
			{
				$chattitle='username';
			}else
			{
				$chattitle='name';
			}

			$db->setQuery("SELECT userid FROM #__session WHERE userid = $user->id");

			if($db->loadResult())
			{ ?>
				<table width='100%'>
					<tr>
						<td class='titleCell'>
							<label for='Chat'><?php echo JText::_('APP_JBOLO_CHAT'); ?></label>
						</td>
						<td class='fieldCell'>
							<a style='text-decoration:none;' href='javascript:void(0)' onclick="javascript:chatFromAnywhere(<?php echo$my->id; ?>,<?php echo $user->id; ?>)">
								<div class='statusicon_1'></div>
									<span> <?php 
										echo sprintf(JText::_('APP_JBOLO_ONLINE_MSG'),$user->username); ?>
									</span>
							</a>
						</td>
					</tr>
				</table>
			<?php
			}
			else
			{ ?>
				<table width=100% >
					<tr>
						<td class='titleCell'>
							<label for='Chat'><?php echo JText::_('APP_JBOLO_CHAT');?></label>
						</td>
						<td class='fieldCell'>
							<div class='statusicon_4'></div>
								<span>
									<?php echo sprintf(JText::_('APP_JBOLO_OFFLINE_MSG'),$user->username); ?>
								</span>
						</td>
					</tr>
				</table>
			<?php
			}
		}
	}
	else if ($user->id!=$my->id) // If logged in and if NO conncection and IF not on own profile
	{	?>
		<table width=100% >
			<tr>
				<td class='titleCell'>
					<label for='Chat'><?php echo JText::_('APP_JBOLO_CHAT'); ?></label>
				</td>
				<td class='fieldCell'><?php echo JText::_('APP_JBOLO_FRND_MSG'); ?></td>
			</tr>
		</table> 
		<?php
	}
	?>

	</div>
</div>
