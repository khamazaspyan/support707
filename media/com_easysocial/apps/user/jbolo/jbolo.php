<?php
/**
 * @package		JBolo
 * @version		$versionID$
 * @author		TechJoomla
 * @author mail	extensions@techjoomla.com
 * @website		http://techjoomla.com
 * @copyright	Copyright © 2009-2013 TechJoomla. All rights reserved.
 * @license		GNU General Public License version 2, or later
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

if(!defined('DS'))
{
	define('DS',DIRECTORY_SEPARATOR);
}

Foundry::import( 'admin:/includes/apps/apps' );

class SocialUserAppJBolo extends SocialAppItem
{
	/**
	 * Class constructor.
	 *
	 * @since	1.0
	 * @access	public
	 */
	public function __construct( $options = array() )
	{
		// Load language file for plugin
		$lang = JFactory::getLanguage();
		$lang->load('plg_app_user_jbolo', JPATH_ADMINISTRATOR);

		parent::__construct( $options );
	}
}
