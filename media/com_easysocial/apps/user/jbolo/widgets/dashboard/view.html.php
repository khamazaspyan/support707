<?php
/**
 * @package		JBolo
 * @version		$versionID$
 * @author		TechJoomla
 * @author mail	extensions@techjoomla.com
 * @website		http://techjoomla.com
 * @copyright	Copyright © 2009-2013 TechJoomla. All rights reserved.
 * @license		GNU General Public License version 2, or later
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

/**
 *
 *
 * @since	3.0.0
 * @access	public
 */
class jboloWidgetsDashboard extends SocialAppsWidgets
{
	/**
	 * Display chat status on the side bar
	 *
	 * @since	1.0
	 * @access	public
	 * @param	string
	 * @return
	 */
	public function sidebarBottom()
	{

	}
}
