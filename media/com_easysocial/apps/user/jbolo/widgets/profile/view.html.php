<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  jbolo
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

/**
 * Widgets class for JBolo Easysocial group
 *
 * @since  3.0.2
 *
 * @access	public
 */
class JboloWidgetsProfile extends SocialAppsWidgets
{
	/**
	 * Display user chat option abover header
	 *
	 * @param   string  $user  User object
	 *
	 * @since	3.0.2
	 *
	 * @return  null
	 */
	public function aboveHeader($user)
	{
	}

	/**
	 * Display user chat option on the side bar
	 *
	 * @param   string  $user  User object
	 *
	 * @since	3.0.2
	 *
	 * @return  html
	 */
	public function sidebarBottom($user)
	{
		// Get the user params
		$params = $this->getUserParams($user->id);
		echo $this->_jboloHTML($user, $params);
	}

	/**
	 * Display user chat option on the side bar
	 *
	 * @param   array  $user    User object
	 * @param   array  $params  User params
	 *
	 * @since	3.0.2
	 *
	 * @return  html
	 */
	private function _jboloHTML($user, $params)
	{
		// Return if Jbolo component not found or is disabled
		if (!JFile::exists(JPATH_ROOT . '/components/com_jbolo/jbolo.php') || !JComponentHelper::isEnabled('com_jbolo', true))
		{
			return false;
		}

		// Check if JBolo module is enabled
		jimport('joomla.application.module.helper');
		$module = JModuleHelper::getModule('jbolo');

		// If the module is disabled, return
		if (!$module)
		{
			return false;
		}

		// Get jbolo system plugin
		$plugin = JPluginHelper::getPlugin('system', 'jbolo');

		// If the plugin is disabled, return
		if (!$plugin)
		{
			return false;
		}

		jimport('joomla.filesystem.file');

		if ( JFile::exists(JPATH_SITE . '/components/com_jbolo/jbolo.php'))
		{
			$appParams 	= $this->app->getParams();

			// Get profile id
			$user = Foundry::user($user->id);

			$theme = Foundry::themes();
			$theme->set('user', $user);

			$com_params = JComponentHelper::getParams('com_jbolo');
			$fonly = $com_params->get('fonly');
			$theme->set('fonly', $fonly);

			$chatusertitle = $com_params->get('chatusertitle');
			$theme->set('chatusertitle', $chatusertitle);

			$app_fonly = $appParams->get('fonly');
			$theme->set('app_fonly', $app_fonly);

			$profile_show_friends = $params->get('profile_show_friends');
			$theme->set('profile_show_friends', $profile_show_friends);

			// Load nodes helper file
			$nodesHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/nodes.php';

			if (!class_exists('nodesHelper'))
			{
				JLoader::register('nodesHelper', $nodesHelperPath);
				JLoader::load('nodesHelper');
			}

			$nodesHelper = new nodesHelper;

			$my = JFactory::getUser();

			$isBlocked = $nodesHelper->isBlockedBy($user->id, $my->id);

			if (!$isBlocked)
			{
				$isBlocked = $nodesHelper->isBlockedBy($my->id, $user->id);
			}

			$theme->set('isBlocked', $isBlocked);

			return $theme->output('themes:/apps/user/jbolo/widgets/profile/jbolo');
		}
	}
}
