<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  jbolo
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

/**
 * JBolo controller groups class for EasySocial.
 *
 * @since  1.0
 *
 */
class JBoloControllerGroups extends SocialAppsController
{
}
