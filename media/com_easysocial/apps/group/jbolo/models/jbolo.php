<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

Foundry::import('admin:/includes/model');

/**
 * JBolo Model class for application for EasySocial.
 *
 * @since  3.2.5
 *
 */
class JBoloModel extends EasySocialModel
{
}
