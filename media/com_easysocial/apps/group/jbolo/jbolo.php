<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  jbolo
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

FD::import('admin:/includes/apps/apps');

/**
 * JBolo application for EasySocial.
 *
 * @since  3.2
 *
 */
class SocialGroupAppJBolo extends SocialAppItem
{
	/**
	 * Fixed legacy issues where the app is displayed on apps list of a group.
	 *
	 * @param   string  $view  View
	 * @param   string  $id    App ID
	 * @param   string  $type  App type
	 *
	 * @since  1.2
	 *
	 * @return boolean
	 */
	public function appListing($view , $id, $type)
	{
		return false;
	}
}
