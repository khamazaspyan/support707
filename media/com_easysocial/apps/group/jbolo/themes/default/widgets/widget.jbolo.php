<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  jbolo
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/css/jbolo.css');
?>
<script>
var groupid=<?php echo $groupId; ?>;

function setGroupChat(flag, reload)
{
	/*
	* Set heartbeat time to jb_minChatHeartbeat second, and poll,
	* as we want it to poll quick after adding new user
	*
	*/
	jb_chatHeartbeatTime = jb_minChatHeartbeat;

	poll_msg();

	techjoomla.jQuery.ajax({
		url: site_link + "index.php?option=com_jbolo&action=setGroupChat",
		cache: false,
		async: true,
		type: 'POST',
		dataType: "json",
		data: {
			gid: groupid,
			onOff: flag,
			client: "<?php echo $group_integration; ?>"
		},
		beforeSend: function (){
			techjoomla.jQuery('#mod_jbolo_group .btn').prop('disabled', true);
		},
		success: function (data) {
			var jboloError = 0;

			if (data.validate.error)
			{
				jboloError = data.validate.error;
			}

			if (jboloError === 1)
			{
				alert(data.validate.error_msg);
				techjoomla.jQuery('#mod_jbolo_group .btn').prop('disabled', false);
				return 0;
			}
			else
			{
				nid = data.nodeinfo.nid;
				rec_name = data.nodeinfo.wt;
				var n = rec_name + "__" + nid;
				open_gc(n);

				if (reload === 1)
				{
					window.location.reload();
				}
				else
				{
					techjoomla.jQuery('#mod_jbolo_group .btn').prop('disabled', false);
				}
			}
		}
	});
}

function joinSocialGroupChat(groupid)
{
	/*
	* Set heartbeat time to jb_minChatHeartbeat second, and poll,
	* as we want it to poll quick after adding new user
	*
	*/
	jb_chatHeartbeatTime = jb_minChatHeartbeat;

	poll_msg();

	techjoomla.jQuery.ajax({
		url: site_link + "index.php?option=com_jbolo&action=joinSocialGroupChat",
		cache: false,
		async: true,
		type: 'POST',
		dataType: "json",
		data: {
			gid: groupid,
			client: "<?php echo $group_integration; ?>"
		},
		success: function (data) {
			var jboloError = 0;

			if (data.validate.error)
			{
				jboloError = data.validate.error;
			}

			if (jboloError === 1)
			{
				alert(data.validate.error_msg);
				return 0;
			}
			nid = data.nodeinfo.nid;
			rec_name = data.nodeinfo.wt;
			var n = rec_name + "__" + nid;
			open_gc(n);
		}
	});
}
</script>

<?php
if ($groupOwner != JFactory::getUser()->id)
{
	if (!isset($groupChatDetails->status) || !$groupChatDetails->status)
	{
		return false;
	}
}
?>
<div class="es-widget">
	<div class="es-widget-head">
		<div class="pull-left widget-title"><?php echo JText::_('APP_GROUP_JBOLO_WIDGET_TITLE'); ?></div>
		<div class="clearfix">&nbsp;</div>
	</div>
	<div class="es-widget-body">
		<?php /*if($groupOwner == JFactory::getUser()->id):*/ ?>
			<?php
			$enabled = $disabled = '';

			if (isset($groupChatDetails->status))
			{
				if ($groupChatDetails->status)
				{
					$enabled = 'btn btn-success active';
				}
				else
				{
					$disabled = 'btn btn-danger disabled';
				}
			}
			?>

			<div class="">
				<div class="clearfix">&nbsp;</div>
				<fieldset id="social_groupchat" class="">
					<?php if ($groupOwner == JFactory::getUser()->id): ?>
						<?php if (!isset($groupChatDetails->status) || !$groupChatDetails->status): ?>
							<p class="">
								<?php echo JText::_('APP_GROUP_JBOLO_ENABLE_GROUP_CHAT'); ?>
							</p>
							<button type="button" class="btn btn-success jbolo-btn-wrapper" id="enable_groupchat" name="enable_groupchat" onclick="setGroupChat(1, 1);"
								title="<?php echo JText::_('APP_GROUP_JBOLO_BTN_ENABLE_DESC'); ?>">
								<i class="ies-checkmark"></i> <?php echo JText::_('APP_GROUP_JBOLO_BTN_ENABLE'); ?>
							</button>
						<?php else: ?>
							<button type="button" class="btn btn-primary jbolo-btn-wrapper" id="sync_groupchat" name="sync_groupchat" onclick="setGroupChat(1, 0);"
								title="<?php echo JText::_('APP_GROUP_JBOLO_BTN_SYNC_DESC'); ?>">
								<i class="ies-wand"></i> <?php echo JText::_('APP_GROUP_JBOLO_BTN_SYNC'); ?>
							</button>
						<?php endif; ?>

						<?php if (isset($groupChatDetails->status) && $groupChatDetails->status): ?>
							<button type="button" class="btn btn-danger jbolo-btn-wrapper" id="disable_groupchat" name="disable_groupchat" onclick="setGroupChat(0, 1);"
								title="<?php echo JText::_('APP_GROUP_JBOLO_BTN_DISABLE_DESC'); ?>">
								<i class="ies-cancel"></i> <?php echo JText::_('APP_GROUP_JBOLO_BTN_DISABLE'); ?>
							</button>
						<?php endif; ?>
					<?php endif; ?>

					<?php if (isset($groupChatDetails->status) && $groupChatDetails->status): ?>
						<button type="button" class="btn btn-success jbolo-btn-wrapper" id="join_groupchat" name="join_groupchat"
							onclick="joinSocialGroupChat(<?php echo $groupId; ?>);"
							title="<?php echo JText::_('APP_GROUP_JBOLO_BTN_JOIN_GROUP_CHAT_DESC'); ?>">
								<i class="ies-comments"></i> <?php echo JText::_('APP_GROUP_JBOLO_BTN_JOIN_GROUP_CHAT'); ?>
						</button>
					<?php endif; ?>
				</fieldset>
			</div>
		<?php /*endif;*/ ?>
	</div>
</div>
