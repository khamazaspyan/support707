<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  jbolo
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

/**
 * Widgets class for JBolo Easysocial group
 *
 * @since  3.2.5
 */
class JBoloWidgetsGroups extends SocialAppsWidgets
{
	/**
	 * Display user photos on the side bar
	 *
	 * @param   object  $group  Group object
	 *
	 * @since  1.3
	 *
	 * @return  html
	 */
	public function afterCategory($group)
	{
		$app       = $this->getApp();
		$permalink = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias(), 'appId' => $app->getAlias()));

		$theme     = FD::themes();
		$theme->set('permalink', $permalink);
		$theme->set('group', $group);

		echo $theme->output('themes:/apps/group/jbolo/widgets/header');
	}

	/**
	 * Renders the sidebar widget for group members
	 *
	 * @param   integer  $groupId  Social group id
	 *
	 * @since  1.3
	 *
	 * @return  html
	 */
	public function sidebarBottom($groupId)
	{
		// Return if Jbolo component not found or is disabled
		if (!JFile::exists(JPATH_ROOT . '/components/com_jbolo/jbolo.php') || !JComponentHelper::isEnabled('com_jbolo', true))
		{
			return false;
		}

		// Check if JBolo module is enabled
		jimport('joomla.application.module.helper');
		$module = JModuleHelper::getModule('jbolo');

		// If the module is disabled, return
		if (!$module)
		{
			return false;
		}

		// Get jbolo system plugin
		$plugin = JPluginHelper::getPlugin('system', 'jbolo');

		// If the plugin is disabled, return
		if (!$plugin)
		{
			return false;
		}

		$theme = FD::themes();

		$params = $this->app->getParams();
		$limit = (int) $params->get('limit', 10);

		// Load up the group
		$group = FD::group($groupId);

		$options = array('state' => SOCIAL_STATE_PUBLISHED, 'limit' => $limit);

		// Include tpdgroups helper
		require_once JPATH_SITE . '/components/com_jbolo/helpers/tpdgroups.php';

		// Get group owner id
		$groupOwner = JBoloTpdGroupsHelper::getTpdGroupOwnerId($groupId, $client = 'com_easysocial.group');

		// Get group chat details
		$groupChatDetails = JBoloTpdGroupsHelper::getTpdGroupChatDetails($groupId, $client = 'com_easysocial.group');

		$link = FRoute::groups(array('id' => $group->getAlias(),'appId' => $this->app->getAlias(),'layout' => 'item'));

		$theme->set('group_integration', 'com_easysocial.group');
		$theme->set('groupId', $groupId);
		$theme->set('groupOwner', $groupOwner);
		$theme->set('groupChatDetails', $groupChatDetails);

		echo $theme->output('themes:/apps/group/jbolo/widgets/widget.jbolo');
	}
}
