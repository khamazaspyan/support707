<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

jimport('joomla.form.formfield');

class JFormFieldCronjoburl extends JFormField
{
	var $type = 'Cronjoburl';

	public function getInput()
	{
		$params = JComponentHelper::getParams('com_jbolo');
		$this->purge_key = $params->get('purge_key');

		$cronjoburl = JRoute::_(JUri::root() . 'index.php?option=com_jbolo&action=purgeChats&purge_key=' . $this->purge_key);
		$return = '<div class="alert alert-info" style="margin-left:8%;">' . $cronjoburl . '</div>';

		return $return;
	}
}
