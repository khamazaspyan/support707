/*
 * @version    SVN:<SVN_ID>
 * @package    SocialAds
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved
 * @license    GNU General Public License version 2, or later
 */

function validateForm()
{
}

function validateAmount(fieldId, fieldMsg)
{
	switch(fieldId)
	{
		case 'max_donors':
		case 'minimum_amount':
			if( (techjoomla.jQuery('#'+fieldId).val() !=='') && (! parseInt(techjoomla.jQuery('#'+fieldId).val(),10) > 1 ) )
			{
				alert(fieldMsg);
				techjoomla.jQuery('#'+fieldId).val('');
				techjoomla.jQuery('#'+fieldId).focus();
				return false;
			}

			if( (techjoomla.jQuery('#'+fieldId).val() !=='') && (techjoomla.jQuery('#'+fieldId).val() < 0) )
			{
				alert(fieldMsg);
				techjoomla.jQuery('#'+fieldId).val('');
				techjoomla.jQuery('#'+fieldId).focus();
				return false;
			}
		break;

		case 'recurring_count':

			if( (techjoomla.jQuery('#'+fieldId).val() !=='') && (! parseInt(techjoomla.jQuery('#'+fieldId).val(),10) > 2 ) )
			{
				alert(fieldMsg);
				techjoomla.jQuery('#'+fieldId).val('');
				techjoomla.jQuery('#'+fieldId).focus();
				return false;
			}

			if( (techjoomla.jQuery('#'+fieldId).val() !=='') && (techjoomla.jQuery('#'+fieldId).val() < 2) )
			{
				alert(fieldMsg);
				techjoomla.jQuery('#'+fieldId).val('');
				techjoomla.jQuery('#'+fieldId).focus();
				return false;
			}
		break;

		default:

			// To fix separated amount issue
			// issue was : suppose user entered amount 10,200, Now in db amount column type is float so the amount 10.20 is only get stored

			var replace = ',';

			if(decimal_separator == '.')
			{
				replace = ',';
			}
			else
			{
				replace = '.';
			}

			var ans = techjoomla.jQuery('#'+fieldId).val().replace(replace, '');
			techjoomla.jQuery('#'+fieldId).val(ans);

			if( (techjoomla.jQuery('#'+fieldId).val() !=='') && (! parseInt(techjoomla.jQuery('#'+fieldId).val(),10) > 0 ) )
			{
				alert(fieldMsg);
				techjoomla.jQuery('#'+fieldId).val('');
				techjoomla.jQuery('#'+fieldId).focus();
				return false;
			}

			if( (techjoomla.jQuery('#'+fieldId).val() !=='') && (techjoomla.jQuery('#'+fieldId).val() < 0) )
			{
				alert(fieldMsg);
				techjoomla.jQuery('#'+fieldId).val('');
				techjoomla.jQuery('#'+fieldId).focus();
				return false;
			}
	}
}
