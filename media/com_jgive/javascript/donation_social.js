function selectstatusorder(appid,ele)
{
    var selInd=ele.selectedIndex;
    var status =ele.options[selInd].value;

    document.getElementById('hidid').value = appid;
    document.getElementById('hidstat').value = status;
    submitbutton('donations.save');

    return;
}

/**
 * Disply selected giveback amount in donation amount field
 *
 * @params   float  amount to fill in donation amount field
 *
 * @since   1.7
 */
function populateSelected_GivebackAmount(giveback_minamount, ref)
{
    /** Update amount in donation amount field */
    if (giveback_minamount)
    {
        /** Remove old active giveback style **/
        techjoomla.jQuery("#jgive_givebacks ul .jgive_active").removeClass("jgive_active");

        /** Add new active giveback style **/
        techjoomla.jQuery(ref).closest( "li" ).addClass( "jgive_active");

        /** Populate giveback amount in donation amount field **/
        var shtml = '<input type="number" class="jgive-amount-field" id="donation_amount" name="donation_amount" title="" value="'+giveback_minamount+'" class="required" min="'+giveback_minamount+'"/>';

        techjoomla.jQuery("#donation_amount_area").html(shtml);
    }
    else
    {
        /** Default populate 0 amount in donation amount field **/
        var shtml = '<input type="number" class="jgive-amount-field" id="donation_amount" name="donation_amount" title="" value="0" class="required" min="0"/>';

        techjoomla.jQuery("#donation_amount_area").html(shtml);
    }
}

/**
 * Send request to place order
 *
 * @params void
 *
 * @return   boolean/HTML  false if failed validation or string html if order placed
 *
 * @since 1.7
 **/
function placeQuickPayment()
{
    var form_values = techjoomla.jQuery('#payment_quick').serialize();
    var order_id=techjoomla.jQuery('#order_id').val();

    var requiredFieldValueMiss = 0;

    techjoomla.jQuery("#jgive_form_field .required").each(function() {
        if(!techjoomla.jQuery(this).val())
        {
            techjoomla.jQuery(this).focus();
            alert(Joomla.JText._("COM_JGIVE_FILL_MANDATORY_FIELDS_DATA"));
            requiredFieldValueMiss = 1;
            return false;
        }
    });

    if (requiredFieldValueMiss === 1)
    {
        return false;
    }

    // Email validation
    if (!validateEmail(document.getElementById("paypal_email").value))
    {
        techjoomla.jQuery('#paypal_email').focus();
        return false;
    }

    /** Validate donation amount */
    if (!donationAmountValidation())
    {
        return false;
    }

    /** Send request to place order **/
    techjoomla.jQuery.ajax({
        url:jgive_baseurl+"index.php?option=com_jgive&task=donations.placeOrderSocial&tmpl=component&payView=social",
        type:"POST",
        dataType:"json",
        async:true,
        data:form_values,
        beforeSend:function(){
            requestProcessingImg();

            /** Blur editable fields */
            techjoomla.jQuery("#jgive_form_field").addClass("jgive-blur-fields");
            techjoomla.jQuery("#jgive_givebacks").addClass("jgive-blur-fields");

            /** JGive disabled fields */
            techjoomla.jQuery("#jgive_disabled_fields input").each(function(){
                techjoomla.jQuery(this).attr("readonly","readonly");
                techjoomla.jQuery("#payment_quick input:radio").attr('disabled',true);
            });

            techjoomla.jQuery( "input:radio[name=givebacks]").attr("readonly","readonly");

            /** Show edit button */
            techjoomla.jQuery("#jgive_edit_button").removeClass("jgive_edit_button");
        },
        complete: function(data){
            hiderequestProcessingImg();
        },
        success:function(data){
            hiderequestProcessingImg();

            if(data['success'] == 1)
            {
                techjoomla.jQuery('#jgive_continue_btn').hide();
                techjoomla.jQuery('#payment_tab_table_html').html(data['payhtml']);
                techjoomla.jQuery('#payment_tab_table_html').html(data['gatewayhtml']);
                techjoomla.jQuery('#payment_tab_table_html').show();

               // techjoomla.jQuery('html,body').animate({scrollTop: techjoomla.jQuery("#payment_tab_table_html").offset().top},'slow');
            }
            else
            {
                alert(Joomla.JText._("COM_JGIVE_ORDER_PLACING_ERROR"));
                techjoomla.jQuery('#jgive_continue_btn').show();
            }
        },
        error:function(){
            alert(Joomla.JText._("COM_JGIVE_ORDER_PLACING_ERROR"));
            techjoomla.jQuery('#jgive_continue_btn').show();
            hiderequestProcessingImg();
        },
    });

}

/**
 * Show request processing image
 *
 * @params void
 *
 * @return   html  adding request processing image to jgive_content div
 *
 * @since  1.7
 */
function requestProcessingImg()
{
    var width = techjoomla.jQuery("#jgive_content").width();
    var height = techjoomla.jQuery("#jgive_content").height();

    techjoomla.jQuery('<div id="jgive_processing"></div>')
        .css("background", "rgba(255, 255, 255, .8) url('"+jgive_baseurl+"media/com_jgive/images/facebook.gif') 50% 15% no-repeat")
        .css("top", techjoomla.jQuery('#jgive_content').position().top - techjoomla.jQuery("#jgive_content").scrollTop())
        .css("left", techjoomla.jQuery('#jgive_content').position().left - techjoomla.jQuery("#jgive_content").scrollLeft())
        .css("width", "100%")
        .css("height", "100%")
        .css("position", "fixed")
        .css("z-index", "1000")
        .css("opacity", "0.80")
        .css("-ms-filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity = 80)")
        .css("filter", "alpha(opacity = 80)")
        .appendTo('#jgive_content');
}

/**
 * Remove request processing image
 *
 * @params   void
 *
 * @return  void
 *
 * @since  1.7
 */
function hiderequestProcessingImg()
{
    techjoomla.jQuery('#campaign_status').remove();
    techjoomla.jQuery('#jgive_processing').remove();
    techjoomla.jQuery('#check_pay_status').remove();
}

/**
 * Donation amount validation
 *
 * @params   void
 *
 * @return   boolean  result in true/false with alert message if needed
 *
 * @since  1.7
 */
function donationAmountValidation()
{
    var donation_amount = document.getElementById('donation_amount').value;
    donation_amount = parseFloat(donation_amount);

    if(donation_amount)
    {
        if(!send_payments_to_owner)
        {
            if(commission_fee)
            {
                total_commission_amount = ((donation_amount*commission_fee)/100)+fixed_commissionfee;
            }
            else
            {
                total_commission_amount = fixed_commissionfee;
            }
        }
        else
        {
            total_commission_amount = 0;
        }

        if(total_commission_amount < minimum_amount)
        {
            total_commission_amount = minimum_amount;
        }

        if(total_commission_amount > donation_amount)
        {
            alert(Joomla.JText._("COM_JGIVE_MINIMUM_DONATION_AMOUNT")+ minimum_amount);
            return false;
        }

        var response = validateGiveBackAmount(donation_amount);

        if(!response)
        {
            return false;
        }
    }
    else
    {
        alert(Joomla.JText._("COM_JGIVE_MINIMUM_DONATION_AMOUNT")+ minimum_amount);
        return false;
    }
    return true;
}

/**
 * Validate if selected giveback matching entered donation amount
 *
 * @params   integer  amount to donate
 *
 * @return   boolean  result in true/false with alert message if needed
 *
 * @since  1.7
 */
function validateGiveBackAmount(donation_amount)
{
    var givebackId = techjoomla.jQuery( "input:radio[name=givebacks]:checked" ).val();

    if(givebackId !=0 )
    {
        for(index = 0; index < givebackDetails.length; index++)
        {
            if(givebackDetails[index]['id'] == givebackId)
            {
                var givebackAmount = givebackDetails[index]['amount'];

                if(donation_amount >= givebackAmount)
                {
                    return true;
                }
                else
                {
                    alert(Joomla.JText._("COM_JGIVE_AMOUNT_SHOULD_BE")+givebackAmount);
                    return false;
                }
            }
        }
    }
    return true;
}

/**
 * Email validation
 *
 * @param  string  mail
 *
 * @return  boolean
 */
function validateEmail(mail)
{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
    {
        return (true)
    }

    alert(Joomla.JText._("COM_JGIVE_EMAIL_VALIDATION"));

    return (false)
}

/**
 * Unlock jgive donations fields to edit placed order
 *
 * @params void
 *
 * @return  void
 *
 * @since  1.7
 */
function editDetails()
{
    /** Hide payment gateways html */
    techjoomla.jQuery('#payment_tab_table_html').html('');
    techjoomla.jQuery('#payment_tab_table_html').hide();

    /** JGive disabled fields */
    techjoomla.jQuery("#jgive_disabled_fields input").each(function(){
        techjoomla.jQuery(this).removeAttr("readonly");
        techjoomla.jQuery("#payment_quick input:radio").attr('disabled',false);
    });

    techjoomla.jQuery( "input:radio[name=givebacks]").removeAttr("readonly");

    /** Remove blur editable fields */
    techjoomla.jQuery("#jgive_form_field").removeClass("jgive-blur-fields");
    techjoomla.jQuery("#jgive_givebacks").removeClass("jgive-blur-fields");

    /** Hide edit details button */
    techjoomla.jQuery("#jgive_edit_button").addClass("jgive_edit_button");

    /** Show continue btn */
    techjoomla.jQuery('#jgive_continue_btn').show();

}

/**
 * Validate allow donations to exceed goal amount setting
 *
 * @param  INT  donation_amount
 * @param  INT  goal_amount
 * @param  INT  allow_exceed_set
 *
 * @return  boolean flag
 *
 * @since  1.7
 */
function donationsToExceedGoalAmount(donation_amount, goal_amount, allow_exceed_set)
{

}

function validateCheckoutFields()
{
// Donation Form Fields Value

    var paypal_email = techjoomla.jQuery('#paypal_email').val();
    var emailres = validateEmailFields(paypal_email);

    if(emailres == false)
    {
        alert(Joomla.JText._("COM_JGIVE_EMAIL_VALIDATION"));
        techjoomla.jQuery('#paypal_email').focus();
        return false;
    }

    return true;
}


