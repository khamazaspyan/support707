/**
 * Validate Donation Form Name Fields
 *
 * @param   String   textval
 *
 * @return  boolean
 *
 * @since  1.8
 */
/*function validateDonationFormNameFields(textval)
{
	// Regular Expression for Alphabet validation
	var name = /^[a-zA-Z\s]+$/;
	if(!(textval.match(name)))
	{
		return false;
	}
}*/

/**
 * Validate Donation Form email  Fields
 *
 * @param   String   mailid
 *
 * @return  boolean
 *
 * @since  1.8
 */
function validateEmailFields(mailid)
{
	// Regular Expression for EmailId validation
	var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

	if(!(mailid.match(email)))
	{
		return false;
	}
}
/**
 * Validate Donation Form Zip Fields
 *
 * @param   Integer  zip
 *
 * @return  boolean
 *
 * @since  1.8
 */
/*function validateDonationFormZipFields(zip)
{
	// Regular Expression for Checking Nummeric Value
	var numbers = /^[0-9]+$/;
	if(!(zip.match(numbers)))
	{
		return false;
	}
}*/
/**
 * Validate Donation Form Phone Fields
 *
 * @param   Integer  phone
 *
 * @return  boolean
 *
 * @since  1.8
 */
/*function validateDonationFormPhoneFields(phone)
{
	var numbers = /^[0-9]+$/;

	// Not allowed any character
	if(!(phone.match(numbers)))
	{
		alert("Enter valid Number");
		return false;
	}
}*/
