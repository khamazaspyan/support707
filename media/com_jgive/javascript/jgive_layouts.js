/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

if(typeof(techjoomla) == 'undefined')
{
	var techjoomla = {};
}

if(typeof techjoomla.jQuery == "undefined")
{
	techjoomla.jQuery = jQuery;
}

/**
 * Front end JavaScript
 */
var jgive = {

	searchFilter: function (){
		var searchInputfield = document.getElementById("SearchFilterInputBox");

		if (searchInputfield.style.display == "inline-flex")
		{
			searchInputfield.style.display = "none";
		}
		else
		{
			searchInputfield.style.display = "inline-flex";
		}
	},

	campaign: {
				searchCampDonors: function (){
					var searchInputfield = document.getElementById("SearchDonorsinputbox");

					if (searchInputfield.style.display == "inline")
					{
						searchInputfield.style.display = "none";
					}
					else
					{
						searchInputfield.style.display = "inline";
					}
				},

				searchDonor: function (){
					var input, filter, table, tr, td, i;
					input = document.getElementById("donorInput");
					filter = input.value.toUpperCase();
					table = document.getElementById("singlecampaignDonor");
					tr = table.getElementsByTagName("tr");

					for (i = 0; i < tr.length; i++)
					{
						td = tr[i].getElementsByTagName("td")[0];

						if (td)
						{
							if (td.innerHTML.toUpperCase().indexOf(filter) > -1)
							{
								tr[i].style.display = "";
							}
							else
							{
								tr[i].style.display = "none";
							}
						}
					}
				},

				viewMoreDonorProPic: function(){

					var cid = document.getElementById('camp_id').value;

					if(gbl_jgive_pro_pic == 0)
					{
						gbl_jgive_pro_pic = document.getElementById('donors_pro_pic_index').value;
					}

					techjoomla.jQuery.ajax({
						url:jgive_baseurl+'index.php?option=com_jgive&task=campaign.viewMoreDonorProPic&tmpl=component',
						type:'POST',
						dataType:'json',
						data:
						{
							cid:cid,
							jgive_index:gbl_jgive_pro_pic
						},
						success:function(data)
						{
							gbl_jgive_pro_pic = data['jgive_index'];
							techjoomla.jQuery("#jgive_donors_pic ").append(data['records']);

							if(!data['records'] || orders_count <= gbl_jgive_pro_pic)
							{
								techjoomla.jQuery("#btn_showMorePic").hide();
							}
						},
						error:function(data)
						{
							console.log('error');
						}
					});
				},

				featuredCamp: function(featuredData, cid){

					if (featuredData == 1)
					{
						var featureval = 0;
					}
					else
					{
						var featureval = 1;
					}

					techjoomla.jQuery.ajax({
						url:'?option=com_jgive&task=campaign.changeCampFeatureStatus&tmpl=component',
						type:'POST',
						dataType:'json',
						data:
						{
							cid:cid,
							featured:featureval
						},
						success:function(data)
						{
							if (data == true)
							{
								window.location.reload();
							}
						},
						error:function(data)
						{
							console.log('error');
						}
					});
				},

				onChangefun: function(){

					jQuery("#gallary_filter").change(function()
					{
						var filterVal = document.getElementById("gallary_filter").value;

						if(filterVal=="1")
						{
							jQuery("#videos").show();
							jQuery("#images").hide();
							jQuery(".videosText").text(Joomla.JText._('COM_JGIVE_GALLERY_VIDEO_TEXT'));
							jQuery(".imagesText").hide();
						}
						else if(filterVal=="2")
						{
							jQuery("#videos").hide();
							jQuery("#images").show();
							jQuery(".videosText").text(Joomla.JText._('COM_JGIVE_GALLERY_IMAGE_TEXT'));
							jQuery(".imagesText").hide();
						}
						else if(filterVal=="0")
						{
							jQuery("#videos").show();
							jQuery("#images").show();
							jQuery(".videosText").text(Joomla.JText._('COM_JGIVE_GALLERY_VIDEO_TEXT'));
							jQuery(".imagesText").show();
						}
					}).change();
				},

				campaignImgPopup: function(){
					techjoomla.jQuery('.popup-gallery').magnificPopup({
						delegate: 'a',
						type: 'image',
						tLoading: 'Loading image #%curr%...',
						mainClass: 'mfp-img-mobile',
						gallery: {
							enabled: true,
							navigateByImgClick: true,
							preload: [0,1]
						},
						image: {
							tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
							titleSrc: function(item) {
								return item.el.attr('title') + '<small></small>';
							}
						}
					});
				},

				onChangeGetDonationData: function(){

					techjoomla.jQuery("#campaigns_graph_period").change(function()
					{
						var graph_filter_val = document.getElementById("campaigns_graph_period").value;
						var cid = document.getElementById('camp_id').value;

						var ajaxcall = techjoomla.jQuery.ajax({
							type:'GET',
							url:'?option=com_jgive&view=campaigns&task=campaign.getCampaignGraphData&cid='+cid+'&filtervalue='+graph_filter_val,
							dataType:'json',
							sucess:function(data)
							{
							},
							error:function(data)
							{
								console.log('error');
							}
						});

						ajaxcall.done(function (data) {

							var graphid = document.getElementById("mycampaign_graph").getContext('2d');
							var total_donationchart = new Chart(graphid, {
							  type: 'line',
							  data: {
								labels: data.donationDate,
								datasets: [{
								  label: data.totalDonation ? data.totalDonation : 0,
								  data: data.donationAmount ? data.donationAmount : 0,
								  backgroundColor: "rgba(203, 235, 230, 0.5)",
								  borderColor:"rgba(55, 179, 142, 1)",
								  lineTension:'0',
								  borderWidth:'2',
								  pointRadius:1,
								  pointBackgroundColor: "rgba(55, 179, 142, 1)",
								  pointBorderColor: "rgba(55, 179, 142, 1)",
								  pointHoverBackgroundColor: "rgba(55, 179, 142, 1)",
								  pointHoverBorderColor: "rgba(55, 179, 142, 1)"
								},{
								  label: data.avgDonation ? data.avgDonation : 0,
								  data: data.donationAvg ? data.donationAvg : 0,
								  backgroundColor: "rgba(216, 225, 180, 1)",
								  borderColor:"rgba(251, 214, 20, 0.90)",
								  lineTension:'0',
								  borderWidth:'2',
								  pointRadius:1,
								  pointBackgroundColor: "rgba(251, 214, 20, 0.90)",
								  pointBorderColor: "rgba(251, 214, 20, 0.90)",
								  pointHoverBackgroundColor: "rgba(251, 214, 20, 0.90)",
								  pointHoverBorderColor: "rgba(251, 214, 20, 0.90)"
								}]
							  }
							});
						});
					}).change();
				},

				loadActivity: function(){
					techjoomla.jQuery(window).load(function() {

						if (techjoomla.jQuery('#tj-activitystream .feed-item-cover').length == '0')
						{
							techjoomla.jQuery('.todays-activity .feed-item').css('border-left', '0px');
						}

						techjoomla.jQuery('#postactivity').attr('disabled',true);
						techjoomla.jQuery('#activity-post-text').on('input', function(){

								if (techjoomla.jQuery('#activity-post-text').val() == '')
								{
									techjoomla.jQuery('#postactivity').attr('disabled',true);
								}
								else
								{
									techjoomla.jQuery('#postactivity').attr('disabled',false);
								}

							var textMax = techjoomla.jQuery('#activity-post-text').attr('maxlength');
							var textLength = techjoomla.jQuery('#activity-post-text').val().length;
							var text_remaining = textMax - textLength;

							techjoomla.jQuery('#activity-post-text-length').html(text_remaining + ' ' + Joomla.JText._('COM_JGIVE_POST_TEXT_ACTIVITY_REMAINING_TEXT_LIMIT'));
						});
					});
				}
			},

	dashboard: {

				onChangeGetDashboardDonationData: function(){
					techjoomla.jQuery("#dashboardcampaignsOption").change(function()
					{
						var graph_filter_val = document.getElementById("dashboardcampaignsOption").value;
						var userId = document.getElementById('user_id').value;

						var ajaxcall = techjoomla.jQuery.ajax({
							type:'GET',
							url:'?option=com_jgive&view=dashboard&task=dashboard.getDashboardGraphData&userId='+userId+'&filtervalue='+graph_filter_val,
							dataType:'json',
							sucess:function(data)
							{
							},
							error:function(data)
							{
								console.log('error');
							}
						});

						ajaxcall.done(function (data) {

							var graphid = document.getElementById("dashboardCampaign_graph").getContext('2d');
							var total_donationchart = new Chart(graphid, {
							  type: 'line',
							  data: {
								labels: data.donationDate,
								datasets: [{
								  label: data.totalDonation ? data.totalDonation : 0,
								  data: data.donationAmount ? data.donationAmount : 0,
								  backgroundColor: "rgba(203, 235, 230, 0.5)",
								  borderColor:"rgba(55, 179, 142, 1)",
								  lineTension:'0',
								  borderWidth:'2',
								  pointRadius:1,
								  pointBackgroundColor: "rgba(55, 179, 142, 1)",
								  pointBorderColor: "rgba(55, 179, 142, 1)",
								  pointHoverBackgroundColor: "rgba(55, 179, 142, 1)",
								  pointHoverBorderColor: "rgba(55, 179, 142, 1)"
								},{
								  label: data.avgDonation ? data.avgDonation : 0,
								  data: data.donationAvg ? data.donationAvg : 0,
								  backgroundColor: "rgba(216, 225, 180, 1)",
								  borderColor:"rgba(251, 214, 20, 0.90)",
								  lineTension:'0',
								  borderWidth:'2',
								  pointRadius:1,
								  pointBackgroundColor: "rgba(251, 214, 20, 0.90)",
								  pointBorderColor: "rgba(251, 214, 20, 0.90)",
								  pointHoverBackgroundColor: "rgba(251, 214, 20, 0.90)",
								  pointHoverBorderColor: "rgba(251, 214, 20, 0.90)"
								}]
							  }
							});
						});
					}).change();
				},
				dashboardFilterList: function(){
					var dashboardFilterlayout = document.getElementById("dashboardFilterOptions");
					var dashboardFilterOptions = document.getElementById("dashboardFilterList");

					if (dashboardFilterlayout.style.display == "block")
					{
						dashboardFilterlayout.style.display = "none";
					}
					else
					{
						dashboardFilterlayout.style.display = "block";
					}
				},
				clearFilter: function(){
					window.location = location.pathname;
				},
			},

	campaigns: {

				filterCamp: function (){
					var filterlayout = document.getElementById("displayFilterText");
					var filterbutton = document.getElementById("displayFilter");
					if (filterlayout.style.display == "block")
					{
						filterlayout.style.display = "none";
					}
					else
					{
						filterlayout.style.display = "block";
					}
				}
			},

	donations: {
		cancelRetryDonation: function (){
			var redirectUrl = jgive_baseurl + "index.php?option=com_jgive&view=donations&layout=my";

			window.location = redirectUrl;
		}
	}
}

/**
 * Backend end JavaScript
 */
var jgiveAdmin = {
}
