/**
 * Set/marked default video
 */
function setDefault(ele, videoid, camp_id, type, container)
{
	if (videoid !=undefined)
	{
		techjoomla.jQuery.ajax(
		{
			url:root_path+'index.php?option=com_jgive&task=campaign.setDefaultVideo',
			type:'POST',
			dataType:'json',
			data:{
				videoid:videoid,
				camp_id:camp_id,
				type:type,
			},
			success:function(data)
			{
				alert(Joomla.JText._("COM_JGIVE_VIDEO_SET_DEFAULT"));
			},
			error: function (err) {
				console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
			}
		});
	}

	/** In default marked array first reset to all to 0 */
	techjoomla.jQuery("input[name='default_marked[]']").each(function() {
		techjoomla.jQuery(this).val(0);
	});

	/** Set clicked radio value to 1 */
	techjoomla.jQuery(ele).parent().find("input[name='default_marked[]']").val(1);

	/** Set all videos/url to non-default */
	techjoomla.jQuery("label[for*='radio']").each(function() {
		techjoomla.jQuery(this).html('<i class="icon-star-empty"></i>');
	});

	/** Set clicked video/img to default */
	var defult_img ="<img src='"+root_path+"media/com_jgive/images/featured.png' width='13' height='13' border='0' >";
	techjoomla.jQuery(ele).parent().find("label[for="+ele.id+"]").html(defult_img);
}

/**
 * Check if default video is set, of not give alert
 */
function checkDefaultVideoIsSet()
{
	var default_available = 0;
	techjoomla.jQuery("input[name='default_marked[]']").each(function() {
		if(techjoomla.jQuery(this).val() == 1)
		{
			default_available = 1;
		}
	});

	/** If no default video found */
	if(default_available == 0)
	{
		alert(Joomla.JText._("COM_JGIVE_PLEASE_SELECT_DEFAULT_VIDEO"));
		techjoomla.jQuery("#video_on_details_page1").prop("checked", true)
	}
}

/** Function to delete video from media table */
function deleteVideo(type ,videoid, cloneId, admin = '0')
{
	if (confirm(Joomla.JText._("COM_JGIVE_DELETE_VIDEO_CONFIRM_MSG")))
	{
		var url = root_path+'index.php?option=com_jgive&task=campaign.deleteVideo&videoid='+videoid+'&type='+type;

		if (admin == 1)
		{
			url = root_path+'administrator/index.php?option=com_jgive&task=campaign.deleteVideo&videoid='+videoid+'&type='+type;
		}

		techjoomla.jQuery.ajax(
		{
			url:url,
			type:'GET',
			dataType:'json',

			success:function(data)
			{
				removeClone(cloneId);
				alert(Joomla.JText._("COM_JGIVE_VIDEO_DELETED"));
			},
			error:function(){

			}
		});

	}
}

function validateCreateCampFields()
{
	var paypal_email = techjoomla.jQuery('#paypal_email').val();
	var emailres = validateEmailFields(paypal_email);

	if(emailres == false)
	{
		alert(Joomla.JText._("COM_JGIVE_EMAIL_VALIDATION"));
		techjoomla.jQuery('#paypal_email').focus();
		return false;
	}

	return true;
}


/**
Function to active step
@params button pressed (pre, next)
*/
function stepsWizard(value)
{
	var requiredFieldValueMiss = 0 ;

	/** Form Field Validation is not on previous click */
	if(value != 'previous' )
	{
		var activeAreaid = techjoomla.jQuery("#contentArea>li.active").attr('id');

		techjoomla.jQuery("#"+activeAreaid+" .required").each(function() {
			if(!techjoomla.jQuery(this).val() && techjoomla.jQuery(this).attr('name'))
			{
				techjoomla.jQuery(this).focus();
				requiredFieldValueMiss = 1;
				return false;
			}
		});
	}

	if (requiredFieldValueMiss === 1)
	{
		/** Show error message **/
		techjoomla.jQuery('#jgive_validation_message').show();
		return false;
	}
	else
	{
		var tabToActivate;
		var tabContentAreaToActivate;

		techjoomla.jQuery('#jgive_validation_message').hide();
		techjoomla.jQuery('#submit-btn').hide();
		techjoomla.jQuery('#cancel-btn').hide();
		techjoomla.jQuery('#resent-btn').hide();
		techjoomla.jQuery('#previous-btn').show();

		switch(value)
		{
			case 'next':
				tabToActivate = techjoomla.jQuery("#tabContainer>ul>li.active").next();
				tabContentAreaToActivate = techjoomla.jQuery("#contentArea>li.active").next();
			break;

			case 'previous':
				tabToActivate = techjoomla.jQuery("#tabContainer>ul>li.active").prev();
				tabContentAreaToActivate = techjoomla.jQuery("#contentArea>li.active").prev();
			break;
		}

		techjoomla.jQuery("#tabContainer>ul>li.active>button").removeClass("btn-primary")
		techjoomla.jQuery("#tabContainer>ul>li.active").removeClass("active");

		techjoomla.jQuery(tabToActivate).addClass('active');
		techjoomla.jQuery("#tabContainer>ul>li.active>button").addClass("btn-primary");

		techjoomla.jQuery("#contentArea>li.active").removeClass("active");
		techjoomla.jQuery(tabContentAreaToActivate).addClass('active');

		techjoomla.jQuery('html,body').animate({scrollTop: techjoomla.jQuery("#tabContainer").offset().top},'slow');

		/** Check last li to show save, cancel & reset buttons */

		/** For first tab */
		var firstliId = techjoomla.jQuery(techjoomla.jQuery("#contentArea>li").first()).attr('id');
		var activeliId = techjoomla.jQuery(techjoomla.jQuery("#contentArea>li.active")).attr('id');

		/** Current active tab is first tab **/
		if(firstliId === activeliId)
		{
			/** Hide Next & last Button */
			techjoomla.jQuery('#previous-btn').hide();
		}

		/** For Last Tab */
		var LastliId = techjoomla.jQuery(techjoomla.jQuery("#contentArea>li").last()).attr('id');


		/** Current active tab is last tab **/
		if(LastliId === activeliId)
		{
			/** Hide Next & last Button */
			techjoomla.jQuery('#next-btn').hide();
			techjoomla.jQuery('#last-btn').hide();

			/** Show following buttons */
			techjoomla.jQuery('#submit-btn').show();
			techjoomla.jQuery('#cancel-btn').show();
			techjoomla.jQuery('#resent-btn').show();
		}
		else
		{
			/** Show following buttons */
			techjoomla.jQuery('#next-btn').show();
		}
	}

}
