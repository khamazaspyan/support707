<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.formvalidation');
$document = JFactory::getDocument();

// Load techjoomla bootstrapper
jimport('joomla.html.parameter.element');
jimport('joomla.form.formfield');
jimport('joomla.html.html.access');
jimport('joomla.utilities.xmlelement');

require_once JPATH_SITE . '/libraries/joomla/form/fields/textarea.php';

/**
 * Custom Mapping field for component params.
 *
 * @package  JGive
 *
 * @since    2.2
 */
class JFormFieldjomsocialfieldmapping extends JFormFieldTextarea
{
	protected $type = 'jomsocialfieldmapping';

	/**
	 * Method to get the field input markup.
	 *
	 * @return string  The field input markup.
	 *
	 * @since 1.6
	 */
	public function getInput()
	{
		return $textarea = $this->fetchElement($this->name, $this->value, $this->element, $this->options['control']);
	}

	protected $name = 'jomsocial_fieldmap';

	/**
	 * Function fetchElement
	 *
	 * @param   string  $name          name of field
	 * @param   string  $value         value of field
	 * @param   string  &$node         node of field
	 * @param   string  $control_name  control_name of field
	 *
	 * @return  HTML
	 *
	 * @since  1.0.0
	 */
	public function fetchElement($name, $value, &$node, $control_name)
	{
		$rows = $node->attributes()->rows;
		$cols = $node->attributes()->cols;
		$class = ( $node->attributes('class') ? 'class="' . $node->attributes('class') . '"' : 'class="text_area"' );

		// To render field which already saved in db
		$fieldvalue = trim($this->renderedfield());

		// For first time installation check value or textarea is empty
		if (($fieldvalue == ''))
		{
			$fieldvalue  = 'first_name=name' . "\n";
			$fieldvalue .= 'address= FIELD_ADDRESS ' . "\n";
			$fieldvalue .= 'city= FIELD_CITY ' . "\n";
			$fieldvalue .= 'phone= FIELD_LANDPHONE ' . "\n";
			$fieldvalue .= 'website_address= FIELD_WEBSITE ' . "\n";
			$fieldvalue .= 'paypal_email=email' . "\n";
		}

		$fieldavi = 'first_name=name' . "\n";
		$fieldavi .= 'last_name=' . "\n";
		$fieldavi .= 'address= FIELD_ADDRESS ' . "\n";
		$fieldavi .= 'address2=' . "\n";
		$fieldavi .= 'city= FIELD_CITY ' . "\n";
		$fieldavi .= 'zip=' . "\n";
		$fieldavi .= 'phone= FIELD_LANDPHONE ' . "\n";
		$fieldavi .= 'website_address= FIELD_WEBSITE ' . "\n";
		$fieldavi .= 'paypal_email=email' . "\n";

		$html = '<textarea name="' . $control_name . $name . '" cols="' . $cols . '" rows="' .
		$rows . '" ' . $class . ' id="' . $control_name . $name . '" >' . $fieldvalue . '</textarea>';

		return $html .= '&nbsp;&nbsp<textarea cols="' . $cols . '" rows="' . $rows . '" ' . $class . ' disabled="disabled" >' . $fieldavi . '</textarea>';
	}

	/**
	 * Method to renderedfield
	 *
	 * @return array
	 *
	 * @since 1.6
	 */
	public function renderedfield()
	{
		$params = JComponentHelper::getParams('com_jgive');
		$mapping = trim($params->get('jomsocial_fieldmap'));
		$field_explode = explode('\n', $mapping);
		$fieldvalue = '';

		if (isset($mapping))
		{
			foreach ($field_explode as $field)
			{
				$fieldvalue .= $field . "\n";
			}
		}

		return $fieldvalue;
	}
}
