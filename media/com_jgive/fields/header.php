<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('JPATH_BASE') or die();
jimport('joomla.html.parameter.element');
jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Custom Header field for component params.
 *
 * @package  JGive
 *
 * @since    2.2
 */
class JFormFieldHeader extends JFormField
{
	protected $type = 'Header';

	/**
	 * Method to get the field input markup.
	 *
	 * @return string  The field input markup.
	 *
	 * @since 1.6
	 */
	public function getInput()
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_admin.css');
		$return = '<div class="jbolo_div_outer">
			<div class="jbolo_div_inner">
				' . JText::_($this->value) . '
			</div>
		</div>';

		return $return;
	}
}
