<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
// No direct access.
defined('_JEXEC') or die('Restricted access');

jimport('joomla.html.pane');
jimport('joomla.application.component.helper');
jimport('joomla.filesystem.folder');
jimport('joomla.form.formfield');

/**
 * Class for custom gateway element
 *
 * @since  1.0.0
 */
class JFormFieldIntegrations extends JFormField
{
	/**
	 * Function to genarate html of custom element
	 *
	 * @return  HTML
	 *
	 * @since  2.0
	 */
	public function getInput()
	{
		return $this->fetchElement($this->name, $this->value, $this->element, $this->options['control']);
	}

	/**
	 * Function to fetch a tooltip
	 *
	 * @param   string  $name          name of field
	 * @param   string  $value         value of field
	 * @param   string  &$node         node of field
	 * @param   string  $control_name  control_name of field
	 *
	 * @return  HTML
	 *
	 * @since  2.0
	 */
	public function fetchElement($name, $value, &$node, $control_name)
	{
		$cbfolder = JPATH_SITE . '/components/com_comprofiler';
		$jomsocialfolder = JPATH_SITE . '/components/com_community';
		$jwfolder = JPATH_SITE . '/components/com_awdwall';
		$esfolder = JPATH_SITE . '/components/com_easysocial';
		$epfolder = JPATH_SITE . '/components/com_jsn';

		$jsString =	"
		<script>
			function checkIfExtInstalled(selectBoxName, extention)
			{
				var flag = 0;
				if (extention == 'cb')
				{";

					if (!JFolder::exists($cbfolder))
					{
						$jsString .= " flag = 1";
					}

					$jsString .= "
				}
				else if (extention == 'jomsocial')
				{
					";

						if (!JFolder::exists($jomsocialfolder))
						{
							$jsString .= " flag = 1";
						}

					$jsString .= "
				}
				else if (extention == 'jomwall')
				{
					";

						if (!JFolder::exists($jwfolder))
						{
							$jsString .= " flag = 1";
						}

					$jsString .= "
				}
				else if (extention == 'easySocial')
				{
					";

						if (!JFolder::exists($esfolder))
						{
							$jsString .= " flag = 1";
						}

					$jsString .= "
				}
				else if (extention == 'easyprofile')
				{
					";

						if (!JFolder::exists($epfolder))
						{
							$jsString .= " flag = 1";
						}

					$jsString .= "
				}


				if (flag == 1)
				{
					var extentionName = jQuery('#jformintegration').val();
					alert('Selected component is not installed');
					jQuery('#jformintegration').val('joomla');
					jQuery('select').trigger('liszt:updated');
				}
			}
		</script>";

		echo   $jsString;

		$options[] = JHTML::_('select.option', 'joomla', JText::_('COM_JGIVE_INTERATION_JOOMLA'));
		$options[] = JHTML::_('select.option', 'cb', JText::_('COM_JGIVE_INTERATION_CB'));
		$options[] = JHTML::_('select.option', 'jomsocial', JText::_('COM_JGIVE_INTERATION_JOMSOCIAL'));
		$options[] = JHTML::_('select.option', 'jomwall', JText::_('COM_JGIVE_INTERATION_JOMWALL'));
		$options[] = JHTML::_('select.option', 'easySocial', JText::_('COM_JGIVE_EASYSOCIAL'));
		$options[] = JHTML::_('select.option', 'easyprofile', JText::_('COM_JGIVE_EASYPROFILE'));

		$fieldName = $name;

		return JHtml::_('select.genericlist',
											$options, $fieldName,
						'class="inputbox btn-group" onchange="checkIfExtInstalled(this.name, this.value)" ',
						'value', 'text', $value, $control_name . $name
						);
	}
}
