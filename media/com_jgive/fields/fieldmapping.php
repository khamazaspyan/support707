<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.formvalidation');
$document = JFactory::getDocument();

// Load techjoomla bootstrapper

jimport('joomla.html.parameter.element');
jimport('joomla.form.formfield');
jimport('joomla.html.html.access');
jimport('joomla.utilities.xmlelement');

require_once JPATH_SITE . '/libraries/joomla/form/fields/textarea.php';

/**
 * Custom Field Mapping field for component params.
 *
 * @package  JGive
 *
 * @since    2.2
 */
class JFormFieldfieldmapping extends JFormFieldTextarea
{
	protected $type = 'fieldmapping';

	protected $name = 'joomla_mapping';

	/**
	 * Method to get the field input markup.
	 *
	 * @return string  The field input markup.
	 *
	 * @since 1.6
	 */
	public function getInput()
	{
		return $textarea = $this->fetchElement($this->name, $this->value, $this->element, $this->options['control']);
	}

	/**
	 * Function fetchElement
	 *
	 * @param   string  $name          name of field
	 * @param   string  $value         value of field
	 * @param   string  &$node         node of field
	 * @param   string  $control_name  control_name of field
	 *
	 * @return  HTML
	 *
	 * @since  1.0.0
	 */
	public function fetchElement($name, $value, &$node, $control_name)
	{
		$rows = $node->attributes()->rows;
		$cols = $node->attributes()->cols;
		$class = ($node->attributes('class') ? 'class="' . $node->attributes('class') . '"' : 'class="text_area"');

		// To render field which already saved in db
		$fieldvalue = trim($this->renderedfield());

		// For first time installation check value or textarea is empty
		if (($fieldvalue == ''))
		{
			$fieldvalue  = 'first_name=name,*' . "\n";
			$fieldvalue .= 'address=address1' . "\n";
			$fieldvalue .= 'address2=address2' . "\n";
			$fieldvalue .= 'city=city' . "\n";
			$fieldvalue .= 'zip=postal_code' . "\n";
			$fieldvalue .= 'phone=phone' . "\n";
			$fieldvalue .= 'website_address=website' . "\n";
			$fieldvalue .= 'paypal_email=email,*' . "\n";
		}

		$fieldavi  = 'first_name=name,*' . "\n";
		$fieldavi .= 'address=address1' . "\n";
		$fieldavi .= 'address2=address2' . "\n";
		$fieldavi .= 'city=city' . "\n";
		$fieldavi .= 'zip=postal_code' . "\n";
		$fieldavi .= 'phone=phone' . "\n";
		$fieldavi .= 'website_address=website' . "\n";
		$fieldavi .= 'paypal_email=email,*' . "\n";

		$html = '<textarea name="' . $control_name . $name . '" cols="' . $cols . '" rows="' . $rows . '" ' .
		$class . ' id="' . $control_name . $name . '" >' . $fieldvalue . '</textarea>';

		return $html .= '&nbsp;&nbsp<textarea  cols="' . $cols . '" rows="' . $rows . '" ' . $class . ' disabled="disabled" >' . $fieldavi . '</textarea>';
	}

	/**
	 * Method to renderedfield
	 *
	 * @return array
	 *
	 * @since 1.6
	 */
	public function renderedfield()
	{
		$params = JComponentHelper::getParams('com_jgive');
		$mapping = trim($params->get('fieldmap'));
		$field_explode = explode('\n', $mapping);
		$fieldvalue = '';

		// Check value exist in array
		if (isset($mapping))
		{
			foreach ($field_explode as $field)
			{
				$fieldvalue .= $field . "\n";
			}
		}

		return $fieldvalue;
	}
}
