<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.view');

/**
 * View class for JBolo control panel.
 *
 * @package  JBolo
 *
 * @since    3.0
 */
class JboloViewcp extends JViewLegacy
{
	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		// Get download id
		$params = JComponentHelper::getParams('com_jbolo');
		$this->downloadid = $params->get('downloadid');

		// Get model
		$model = $this->getModel();

		// Use model functions
		$this->nodeTypesArray      = $model->getNodeTypesArray();
		$this->messageTypesArray   = $model->getMessageTypesArray();
		$this->messagesPerDayArray = $model->getMessagesPerDayArray();

		// Get installed version from xml file
		$xml     = JFactory::getXML(JPATH_COMPONENT . '/jbolo.xml');
		$version = (string) $xml->version;
		$this->version = $version;

		// Refresh update site
		$model->refreshUpdateSite();

		// Get new version
		$this->latestVersion = $model->getLatestVersion();

		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jbolo'))
		{
			$app->enqueueMessage(JText::_('COM_JBOLO_AUTH_ERROR'), 'error');

			return false;
		}

		// Set toolbar
		$this->addToolbar();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		if (JVERSION >= '3.0')
		{
			JToolBarHelper::title(JText::_('COM_JBOLO_MENU_JBOLO') . ' - ' . JText::_('COM_JBOLO_MENU_CP'), 'comments-2');
		}
		else
		{
			JToolBarHelper::title(JText::_('COM_JBOLO_MENU_JBOLO') . ' - ' . JText::_('COM_JBOLO_MENU_CP'), 'jbolo.png');
		}

		$user = JFactory::getUser();

		if (JFactory::getUser($user->id)->authorise('core.admin', 'com_jbolo'))
		{
			JToolBarHelper::preferences('com_jbolo', 550, 875);
		}
	}
}
