<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die('Restricted access');

// Load style sheet
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/css/jbolo_backend.css');
$document->addStyleSheet(JUri::root(true).'/media/com_jbolo/vendors/morris/morris.css');

// Load CSS & JS resources
if (JVERSION > '3.0')
{
	// Load jQuery.
	JHtml::_('jquery.framework');

	$params         = JComponentHelper::getParams('com_jbolo');
	$load_bootstrap = $params->get('force_load_bootstrap');

	if ($load_bootstrap)
	{
		// Load bootstrap CSS and JS.
		JHtml::_('bootstrap.loadcss');
		JHtml::_('bootstrap.framework');
	}
}
else
{
	// Load TJ jQuery
	$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/akeebajq.js');

	// Load bootstrap JS.
	$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/bootstrap.min.js');
}

// Load chart js files
$document->addScript(JUri::root(true) . '/media/com_jbolo/vendors/morris/morris.min.js');
$document->addScript(JUri::root(true) . '/media/com_jbolo/vendors/morris/raphael-min.js');
?>

<div class="<?php echo JBOLO_WRAPPER_CLASS;?>" id="jbolo-cp">
	<div class="row-fluid">
		<div class="span8">
			<div class="row-fluid">
				<div class="span6">
					<div class="well">
						<?php
						// Draw chart
						if($this->nodeTypesArray['one2oneChatsCount'] || $this->nodeTypesArray['groupChatsCount'])
						{
							?>
							<div id="chatTypeDivisonChart" class="jbolo-donut-chart"></div>
							<div class="center"><h3><?php echo JText::_("COM_JBOLO_NODES_DIVISION");?></h3></div>

							<script>
								techjoomla.jQuery(document).ready(function()
								{
									Morris.Donut({
										element: 'chatTypeDivisonChart',
										data: [
											{
												label: "<?php echo JText::_("COM_JBOLO_1TO1_NODES");?>",
												value: <?php echo $this->nodeTypesArray['one2oneChatsCount'];?>
											},
											{
												label: "<?php echo JText::_("COM_JBOLO_GROUPCHAT_NODES");?>",
												value: <?php echo $this->nodeTypesArray['groupChatsCount'];?>
											},
										],
										colors: ["#78CD51", "#428BCA"],
										resize: true
									});
								});
							</script>
							<?php
						}
						else
						{
							echo '<div><strong>'.JText::_('COM_JBOLO_NODES_DIVISION').'</strong></div>';
							echo '<div class="alert alert-warning">'.JText::_('COM_JBOLO_NO_DATA_FOUND').'</div>';
						}
						?>
					</div>
				</div>

				<div class="span6">
					<div class="well">
						<?php
						// Draw chart
						if($this->messageTypesArray['txtMsgs'] || $this->messageTypesArray['fileMsgs'])
						{
							?>
							<div id="msgTypeDivisonChart" class="jbolo-donut-chart"></div>
							<div class="center">
								<h3><?php echo JText::_("COM_JBOLO_MSGS_DIVISION");?></h3>
							</div>

							<script>
								techjoomla.jQuery(document).ready(function()
								{
									Morris.Donut({
										element: 'msgTypeDivisonChart',
										data: [
											{
												label: "<?php echo JText::_("COM_JBOLO_TXT_MSGS");?>",
												value: <?php echo $this->messageTypesArray['txtMsgs'];?>
											},
											{
												label: "<?php echo JText::_("COM_JBOLO_FILE_MSGS");?>",
												value: <?php echo $this->messageTypesArray['fileMsgs'];?>
											},
										],
										colors: ["#428BCA", "#78CD51"],
										resize: true
									});
								});
							</script>
						<?php
						}
						else
						{
							echo '<div><strong>'.JText::_('COM_JBOLO_MSGS_DIVISION').'</strong></div>';
							echo '<div class="alert alert-warning">'.JText::_('COM_JBOLO_NO_DATA_FOUND').'</div>';
						}
						?>
					</div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12" style="width:99%">
					<div class="well">
						<?php
						// Draw chart
						if($this->messagesPerDayArray)
						{
							?>
							<div id="msgsPerDayChart" class="jbolo-line-chart"></div>
							<div class="center"><h3><?php echo JText::_("COM_JBOLO_CHAT_MSGS_EXCHANGED");?></h3></div>

							<script type="text/javascript">
								techjoomla.jQuery(document).ready(function()
								{
									Morris.Area({
										element: 'msgsPerDayChart',
										data: [
											<?php
											foreach ($this->messagesPerDayArray as $mpd)
											{
												echo "{date:'" . $mpd->date . "', msgCount: " . $mpd->count . "},";
											}
											?>
										],
										xkey: 'date',
										ykeys: ['msgCount'],
										smooth: false,
										lineColors: ["#428BCA"],
										labels: ['<?php echo JText::_("COM_JBOLO_CHAT_MSGS_EXCHANGED");?>'],
										resize: true
									});
								});

							</script>
							<?php
						}
						else
						{
							echo '<div><strong>'.JText::_('COM_JBOLO_CHAT_MSGS_EXCHANGED').'</strong></div>';
							echo '<div class="alert alert-warning">'.JText::_('COM_JBOLO_NO_DATA_FOUND').'</div>';
						}
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="span4">
			<?php
			$versionHTML = '<span class="label label-info">' .
								JText::_('COM_JBOLO_HAVE_INSTALLED_VER') . ': ' . $this->version .
							'</span>';

			if ($this->latestVersion)
			{
				if ($this->latestVersion->version > $this->version)
				{
					$versionHTML = '<div class="alert alert-error">' .
										'<i class="icon-puzzle install"></i>' .
										JText::_('COM_JBOLO_HAVE_INSTALLED_VER') . ': ' . $this->version .
										'<br/>' .
										'<i class="icon icon-info"></i>' .
										JText::_("COM_JBOLO_NEW_VER_AVAIL") . ': ' .
										'<span class="jbolo_latest_version_number">' .
											$this->latestVersion->version .
										'</span>
										<br/>' .
										'<i class="icon icon-warning"></i>' .
										'<span class="small">' .
											JText::_("COM_JBOLO_LIVE_UPDATE_BACKUP_WARNING") . '
										</span>' . '
									</div>

									<div>
										<a href="index.php?option=com_installer&view=update" class="jbolo-btn-wrapper btn btn-small btn-primary">' .
											JText::sprintf('COM_JBOLO_LIVE_UPDATE_TEXT', $this->latestVersion->version) . '
										</a>
										<a href="' . $this->latestVersion->infourl . '/?utm_source=clientinstallation&utm_medium=dashboard&utm_term=jbolo&utm_content=updatedetailslink&utm_campaign=jbolo_ci' . '" target="_blank" class="jbolo-btn-wrapper btn btn-small btn-info">' .
											JText::_('COM_JBOLO_LIVE_UPDATE_KNOW_MORE') . '
										</a>
									</div>';
				}
			}
			?>

			<div class="row-fluid">
				<?php if (!$this->downloadid): ?>
					<div class="">
						<div class="clearfix pull-right">
							<div class="alert alert-warning">
								<?php echo JText::sprintf('COM_JBOLO_LIVE_UPDATE_DOWNLOAD_ID_MSG', '<a href="https://techjoomla.com/about-tj/faqs/#how-to-get-your-download-id" target="_blank">' . JText::_('COM_JBOLO_LIVE_UPDATE_DOWNLOAD_ID_MSG2') . '</a>'); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<div class="">
					<div class="clearfix pull-right">
						<?php echo $versionHTML; ?>
					</div>
				</div>
			</div>

			<div class="clearfix">&nbsp;</div>

			<div class="well well-small">
				<div class="module-title nav-header">
					<?php
					if(JVERSION >= '3.0')
						echo '<i class="icon-comments-2"></i>';
					else
						echo '<i class="icon-comment"></i>';
					?> <strong><?php echo JText::_('COM_JBOLO'); ?></strong>
				</div>
				<hr class="hr-condensed"/>

				<div class="row-fluid">
					<div class="span12 alert alert-success"><?php echo JText::_('COM_JBOLO_INTRO'); ?></div>
				</div>

				<div class="row-fluid">
					<div class="span12">
						<ul class="nav nav-tabs" id="myTab">
							<li class="active">
								<a data-toggle="tab" href="#links"><?php echo JText::_('COM_JBOLO_TABS_LINKS'); ?></a>
							</li>
							<li>
								<a data-toggle="tab" href="#news"><?php echo JText::_('COM_JBOLO_TABS_NEWS'); ?></a>
							</li>
							<li>
								<a data-toggle="tab" href="#about"><?php echo JText::_('COM_JBOLO_TABS_ABOUT'); ?></a>
							</li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane active" id="links">
								<div class="row-fluid">
									<div class="span12">
										<p class="pull-right"><span class="label label-info"><?php echo JText::_('COM_JBOLO_LINKS'); ?></span></p>
									</div>
								</div>

								<div class="row-striped">
									<div class="row-fluid">
										<div class="span12">
											<a href="https://techjoomla.com/table/extension-documentation/documentation-for-jbolo/?utm_source=clientinstallation&utm_medium=dashboard&utm_term=jbolo&utm_content=textlink&utm_campaign=jbolo_ci" target="_blank"><i class="icon-file"></i> <?php echo JText::_('COM_JBOLO_DOCS');?></a>
										</div>
									</div>
									<div class="row-fluid">
										<div class="span12">
											<a href="https://techjoomla.com/documentation-for-jbolo/jbolo-faqs.html/?utm_source=clientinstallation&utm_medium=dashboard&utm_term=jbolo&utm_content=textlink&utm_campaign=jbolo_ci" target="_blank">
												<?php
												if(JVERSION >= '3.0')
													echo '<i class="icon-help"></i>';
												else
													echo '<i class="icon-question-sign"></i>';
												?>
												<?php echo JText::_('COM_JBOLO_FAQS');?>
											</a>
										</div>
									</div>
									<div class="row-fluid">
										<div class="span12">
											<a href="http://feeds.feedburner.com/techjoomla/blogfeed" target="_blank">
												<?php
												if(JVERSION >= '3.0')
													echo '<i class="icon-feed"></i>';
												else
													echo '<i class="icon-bell"></i>';
												?> <?php echo JText::_('COM_JBOLO_RSS');?></a>
										</div>
									</div>
									<div class="row-fluid">
										<div class="span12">
											<a href="https://techjoomla.com/support-tickets/?utm_source=clientinstallation&utm_medium=dashboard&utm_term=jbolo&utm_content=textlink&utm_campaign=jbolo_ci" target="_blank">
												<?php
												if(JVERSION >= '3.0')
													echo '<i class="icon-support"></i>';
												else
													echo '<i class="icon-user"></i>';
												?> <?php echo JText::_('COM_JBOLO_TECHJOOMLA_SUPPORT_CENTER'); ?></a>
										</div>
									</div>
									<div class="row-fluid">
										<div class="span12">
											<a href="http://extensions.joomla.org/extensions/extension/communication/instant-messaging/jbolo" target="_blank">
												<?php
												if(JVERSION >= '3.0')
													echo '<i class="icon-quote"></i>';
												else
													echo '<i class="icon-bullhorn"></i>';
												?> <?php echo JText::_('COM_JBOLO_LEAVE_JED_FEEDBACK'); ?></a>
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="news"></div>

							<div class="tab-pane" id="about">
								<div class="row-fluid">
									<div class="span12">
										<p class="pull-right">
											<span class="label label-info"><?php echo JText::_('COM_JBOLO_STAY_TUNNED'); ?></span>
										</p>
									</div>
								</div>

								<div class="row-striped">
									<div class="row-fluid">
										<div class="span3"><?php echo JText::_('COM_JBOLO_FACEBOOK'); ?></div>
										<div class="span9">
											<!-- facebook button code -->
											<div id="fb-root"></div>
											<script>(function(d, s, id) {
											  var js, fjs = d.getElementsByTagName(s)[0];
											  if (d.getElementById(id)) return;
											  js = d.createElement(s); js.id = id;
											  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
											  fjs.parentNode.insertBefore(js, fjs);
											}(document, 'script', 'facebook-jssdk'));</script>
											<div class="fb-like" data-href="https://www.facebook.com/techjoomla" data-send="true" data-layout="button_count" data-width="250" data-show-faces="false" data-font="verdana"></div>
										</div>
									</div>

									<div class="row-fluid">
										<div class="span3"><?php echo JText::_('COM_JBOLO_TWITTER'); ?></div>
										<div class="span9">
											<!-- twitter button code -->
											<a href="https://twitter.com/techjoomla" class="twitter-follow-button" data-show-count="false">Follow @techjoomla</a>
											<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
										</div>
									</div>

									<div class="row-fluid">
										<div class="span3"><?php echo JText::_('COM_JBOLO_GPLUS'); ?></div>
										<div class="span9">
											<!-- Place this tag where you want the +1 button to render. -->
											<div class="g-plusone" data-annotation="inline" data-width="200" data-href="https://plus.google.com/+techjoomla"></div>
											<!-- Place this tag after the last +1 button tag. -->
											<script type="text/javascript">
											(function() {
											var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
											po.src = 'https://apis.google.com/js/plusone.js';
											var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
											})();
											</script>
										</div>
									</div>
								</div>

								<br/>
								<div class="row-fluid">
									<div class="span12 center">
										<?php
										$logo_path='<img src="'.JUri::root(true) . '/media/com_jbolo/img/techjoomla.png" alt="Techjoomla" class="jbolo_vertical_align_top"/>';
										?>
										<a href='https://techjoomla.com/?utm_source=clientinstallation&utm_medium=dashboard&utm_term=jbolo&utm_content=logolink&utm_campaign=jbolo_ci' target='_blank' alt="Techjoomla">
											<?php echo $logo_path;?>
										</a>
										<p><?php echo JText::_('COM_JBOLO_COPYRIGHT'); ?></p>
									</div>
								</div>

								<div class="row-fluid">
									<div class="span12">
										<hr/>
										<ul>
											<li>JBolo is released under Licence <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html" title="GNU General Public License, version 2" target="_blank">GNU General Public License, version 2</a></li>
											<li>Emoji set designed and offered free by <a href="http://emojione.com" target="_blank">Emoji One</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!--END span4 -->
	</div>
	<!--END outermost row-fluid -->
</div>

<script>
techjoomla.jQuery(document).ready(function(){
	techjoomla.jQuery.ajax({
		beforeSend: function()
		{
			techjoomla.jQuery('#news').html('<div class="center"><img src="<?php echo JUri::root(true);?>/media/com_jbolo/img/ajax.gif"/></div>');
		},
		type: 'GET',
		url: 'index.php?option=com_jbolo&task=getNewsFeeds',
		async: true,
		dataType: 'json',
		success: function(data)
		{
			if (!data){
				techjoomla.jQuery('#news').html('<?php echo JText::_('COM_JBOLO_ERROR_LOADING_FEEDS'); ?>');
			}
			else{
				var newsFeedsHtml = '';

				var weekDays   = [
					"<?php echo JText::_('SUN'); ?>",
					"<?php echo JText::_('MON'); ?>",
					"<?php echo JText::_('TUE'); ?>",
					"<?php echo JText::_('WED'); ?>",
					"<?php echo JText::_('THU'); ?>",
					"<?php echo JText::_('FRI'); ?>",
					"<?php echo JText::_('SAT'); ?>"
				];

				var monthNames = [
					"<?php echo JText::_('JANUARY_SHORT'); ?>",
					"<?php echo JText::_('FEBRUARY_SHORT'); ?>",
					"<?php echo JText::_('MARCH_SHORT'); ?>",
					"<?php echo JText::_('APRIL_SHORT'); ?>",
					"<?php echo JText::_('MAY_SHORT'); ?>",
					"<?php echo JText::_('JUNE_SHORT'); ?>",
					"<?php echo JText::_('JULY_SHORT'); ?>",
					"<?php echo JText::_('AUGUST_SHORT'); ?>",
					"<?php echo JText::_('SEPTEMBER_SHORT'); ?>",
					"<?php echo JText::_('OCTOBER_SHORT'); ?>",
					"<?php echo JText::_('NOVEMBER_SHORT'); ?>",
					"<?php echo JText::_('DECEMBER_SHORT'); ?>"
				];


				newsFeedsHtml += '<div class="row-fluid">';
					newsFeedsHtml += '<div class="span12 jbolo-news-items">';

						techjoomla.jQuery.each(data, function (i, feeds){
							feeds.link = feeds.link + '?utm_source=clientinstallation&utm_medium=dashboard&utm_term=jbolo&utm_content=newslink&utm_campaign=jbolo_ci';

							newsFeedsHtml += '<div class="row-fluid jbolo-news-item">';
								newsFeedsHtml += '<div class="span4">';
									newsFeedsHtml += '<time class="icon" datetime="' + feeds.date + '">';
										var d = new Date(feeds.date);
										/*Get date, month and day*/
										newsFeedsHtml += '<em>' + weekDays[d.getDay()] + '</em>';
										newsFeedsHtml += '<strong>' + monthNames[d.getMonth()] + '</strong>';
										newsFeedsHtml += '<span>' + d.getDate() + '</span>';
									newsFeedsHtml += '</time>';
								newsFeedsHtml += '</div>';

								newsFeedsHtml += '<div class="span7">';
									newsFeedsHtml += '<div>';
										newsFeedsHtml += '<a target="_blank" href="' + feeds.link + '">';
											newsFeedsHtml += feeds.title;
										newsFeedsHtml += '</a>';
									newsFeedsHtml += '</div>';

									newsFeedsHtml += '<div>';
										newsFeedsHtml += '<br/><p class="jbolo-text-justify">';
											newsFeedsHtml += feeds.text;
										newsFeedsHtml += '</p>';
									newsFeedsHtml += '</div>';
								newsFeedsHtml += '</div>';
							newsFeedsHtml += '</div>';
						});

					newsFeedsHtml += '</div>';
				newsFeedsHtml += '</div>';

				techjoomla.jQuery('#news').html(newsFeedsHtml);
			}
		}
	});
});
</script>
