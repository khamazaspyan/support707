<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.model');

/**
 * Model class for JBolo control panel.
 *
 * @package  JBolo
 *
 * @since    3.0
 */
class JboloModelCp extends JModelLegacy
{
	/**
	 * Constructor
	 *
	 * @since  3.0
	 */
	public function __construct()
	{
		// Get download id
		$params           = JComponentHelper::getParams('com_jbolo');
		$this->downloadid = $params->get('downloadid');

		// Setup vars
		$this->updateStreamName = 'JBolo';
		$this->updateStreamType = 'extension';
		$this->updateStreamUrl  = "https://techjoomla.com/updates/stream/jbolo?dummy=extension.xml";
		$this->extensionElement = 'com_jbolo';
		$this->extensionType    = 'component';

		// Call the parents constructor
		parent::__construct();
	}

	/**
	 * Returns array of nodetypes and count of no. of nodes of each type
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getNodeTypesArray()
	{
		$db    = JFactory::getDbo();
		$query = "SELECT n.type, count(n.node_id) AS count
		 FROM #__jbolo_nodes AS n
		 GROUP BY n.type";
		$db->setQuery($query);
		$nodes = $db->loadObjectList();
		$count = count($nodes);

		// Set default counts
		$nodes['one2oneChatsCount'] = 0;
		$nodes['groupChatsCount']   = 0;

		if ($nodes)
		{
			for ($i = 0; $i < $count; $i++)
			{
				if ($nodes[$i]->type == 1)
				{
					$nodes['one2oneChatsCount'] = $nodes[$i]->count;
				}

				if ($nodes[$i]->type == 2)
				{
					$nodes['groupChatsCount'] = $nodes[$i]->count;
				}
			}
		}

		return $nodes;
	}

	/**
	 * Returns array of messages types and count of no. of messages of each type
	 *
	 * @return  array
	 *
	 * @since   3.0
	 */
	public function getMessageTypesArray()
	{
		$db = JFactory::getDbo();
		$query = "SELECT cm.msg_type AS type, count(cm.msg_id) AS count
		 FROM #__jbolo_chat_msgs AS cm
		 GROUP BY cm.msg_type";
		$db->setQuery($query);
		$msgTypes = $db->loadObjectList();
		$count = count($msgTypes);

		// Set default counts
		$msgTypes['txtMsgs']  = 0;
		$msgTypes['fileMsgs'] = 0;

		if ($msgTypes)
		{
			for ($i = 0; $i < $count; $i++)
			{
				// Text messages
				if ($msgTypes[$i]->type == 'txt')
				{
					$msgTypes['txtMsgs'] = $msgTypes[$i]->count;
				}

				// File transfer messages
				if ($msgTypes[$i]->type == 'file')
				{
					$msgTypes['fileMsgs'] = $msgTypes[$i]->count;
				}
			}
		}

		return $msgTypes;
	}

	/**
	 * Returns array of number of mesages exchanged per day for 7 applicable days
	 *
	 * @return  array
	 *
	 * @since   3.0
	 */
	public function getMessagesPerDayArray()
	{
		$db        = JFactory::getDbo();

		// PHP date format Y-m-d to match sql date format is 2013-05-15
		$date_today = date('Y-m-d');

		// Set dates for past 6 days in an array
		$msgsPerDay = array();

		for ($i = 6, $k = 0; $i > 0; $i--, $k++)
		{
			$msgsPerDay[$k]       = new stdClass;
			$msgsPerDay[$k]->date = date('Y-m-d', strtotime(date('Y-m-d') . ' - ' . $i . ' days'));
		}

		// Get today's date
		$msgsPerDay[$k] = new stdClass;
		$msgsPerDay[$k]->date = date('Y-m-d');

		// Find number of messages per day
		for ($i = 6; $i >= 0; $i--)
		{
			// Date format here is 2013-05-15
			$query = "SELECT count(cm.msg_id) AS count
			 FROM #__jbolo_chat_msgs AS cm
			 WHERE date(cm.time)='" . $msgsPerDay[$i]->date . "'";
			$db->setQuery($query);
			$count = $db->loadResult();

			if ($count)
			{
				$msgsPerDay[$i]->count = $count;
			}
			else
			{
				$msgsPerDay[$i]->count = 0;
			}
		}

		return $msgsPerDay;
	}

	/**
	 * Get extension id for tis extension
	 *
	 * @return  string
	 *
	 * @since   3.2.5
	 */
	public function getExtensionId()
	{
		$db = $this->getDbo();

		// Get current extension ID
		$query = $db->getQuery(true)
			->select($db->qn('extension_id'))
			->from($db->qn('#__extensions'))
			->where($db->qn('type') . ' = ' . $db->q($this->extensionType))
			->where($db->qn('element') . ' = ' . $db->q($this->extensionElement));
		$db->setQuery($query);

		$extension_id = $db->loadResult();

		if (empty($extension_id))
		{
			return 0;
		}
		else
		{
			return $extension_id;
		}
	}

	/**
	 * Refreshes the Joomla! update sites for this extension as needed
	 *
	 * @return  void
	 */
	public function refreshUpdateSite()
	{
		// Extra query for Joomla 3.0 onwards
		$extra_query = null;

		if (preg_match('/^([0-9]{1,}:)?[0-9a-f]{32}$/i', $this->downloadid))
		{
			$extra_query = 'dlid=' . $this->downloadid;
		}

		// Setup update site array for storing in database
		$update_site = array(
			'name' => $this->updateStreamName,
			'type' => $this->updateStreamType,
			'location' => $this->updateStreamUrl,
			'enabled'  => 1,
			'last_check_timestamp' => 0,
			'extra_query'          => $extra_query
		);

		// For joomla versions < 3.0
		if (version_compare(JVERSION, '3.0.0', 'lt'))
		{
			unset($update_site['extra_query']);
		}

		$db = $this->getDbo();

		// Get current extension ID
		$extension_id = $this->getExtensionId();

		if (!$extension_id)
		{
			return;
		}

		// Get the update sites for current extension
		$query = $db->getQuery(true)
			->select($db->qn('update_site_id'))
			->from($db->qn('#__update_sites_extensions'))
			->where($db->qn('extension_id') . ' = ' . $db->q($extension_id));
		$db->setQuery($query);

		$updateSiteIDs = $db->loadColumn(0);

		if (!count($updateSiteIDs))
		{
			// No update sites defined. Create a new one.
			$newSite = (object) $update_site;
			$db->insertObject('#__update_sites', $newSite);

			$id = $db->insertid();

			$updateSiteExtension = (object) array(
				'update_site_id' => $id,
				'extension_id'   => $extension_id,
			);

			$db->insertObject('#__update_sites_extensions', $updateSiteExtension);
		}
		else
		{
			// Loop through all update sites
			foreach ($updateSiteIDs as $id)
			{
				$query = $db->getQuery(true)
					->select('*')
					->from($db->qn('#__update_sites'))
					->where($db->qn('update_site_id') . ' = ' . $db->q($id));
				$db->setQuery($query);
				$aSite = $db->loadObject();

				// Does the name and location match?
				if (($aSite->name == $update_site['name']) && ($aSite->location == $update_site['location']))
				{
					// Do we have the extra_query property (J 3.2+) and does it match?
					if (property_exists($aSite, 'extra_query'))
					{
						if ($aSite->extra_query == $update_site['extra_query'])
						{
							continue;
						}
					}
					else
					{
						// Joomla! 3.1 or earlier. Updates may or may not work.
						continue;
					}
				}

				$update_site['update_site_id'] = $id;
				$newSite = (object) $update_site;
				$db->updateObject('#__update_sites', $newSite, 'update_site_id', true);
			}
		}
	}

	/**
	 * Get latest version fetched by joomla updater
	 *
	 * @return  string
	 *
	 * @since   3.2.5
	 */
	public function getLatestVersion()
	{
		// Get current extension ID
		$extension_id = $this->getExtensionId();

		if (!$extension_id)
		{
			return 0;
		}

		$db = $this->getDbo();

		// Get current extension ID
		$query = $db->getQuery(true)
			->select($db->qn(array('version', 'infourl')))
			->from($db->qn('#__updates'))
			->where($db->qn('extension_id') . ' = ' . $db->q($extension_id));
		$db->setQuery($query);

		$latestVersion = $db->loadObject();

		if (empty($latestVersion))
		{
			return 0;
		}
		else
		{
			return $latestVersion;
		}
	}

	/**
	 * Get News Feed
	 *
	 * @return  array
	 */
	public function getNewsFeeds()
	{
		$rssUrl  = 'https://techjoomla.com/categories/jbolo.html?format=feed&type=rss';

		if (JVERSION > '3.0')
		{
			// Get RSS parsed object
			try
			{
				$rssDoc   = new JFeedFactory;
				$feeds = $rssDoc->getFeed($rssUrl);
			}
			catch (InvalidArgumentException $e)
			{
				return false;
			}
			catch (RunTimeException $e)
			{
				return false;
			}
			catch (LogicException $e)
			{
				return false;
			}

			if (empty($feeds))
			{
				return false;
			}

			if ($feeds)
			{
				$parsedFeeds = array();

				for ($i = 0; $i < 4; $i++)
				{
					if (!$feeds->offsetExists($i))
					{
						break;
					}

					$parsedFeeds[$i] = new stdClass;
					$parsedFeeds[$i]->title = $feeds[$i]->title;

					$parsedFeeds[$i]->link = (! empty($feeds[$i]->uri) || !is_null($feeds[$i]->uri)) ? $feeds[$i]->uri : $feeds[$i]->guid;
					$parsedFeeds[$i]->link = htmlspecialchars($parsedFeeds[$i]->link);

					$parsedFeeds[$i]->text = !empty($feeds[$i]->content) ||  !is_null($feeds[$i]->content) ? $feeds[$i]->content : $feeds[$i]->description;
					$parsedFeeds[$i]->text = JFilterOutput::stripImages($parsedFeeds[$i]->text);
					$parsedFeeds[$i]->text = strip_tags($parsedFeeds[$i]->text);
					$parsedFeeds[$i]->text = JHtml::_('string.truncate', $parsedFeeds[$i]->text, 125);

					$date = $feeds[$i]->updatedDate;
					$parsedFeeds[$i]->date = $date->format('d-M-Y');
				}

				return $parsedFeeds;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$rssDoc = @JFactory::getFeedParser($rssUrl);

			$feed = new stdclass;

			if ($rssDoc != false)
			{
				// Items
				$items = @$rssDoc->get_items();

				// Feed elements
				$feed->items = array_slice($items, 0, 4);

				foreach ($feed->items as $item)
				{
					$data = new stdClass;

					$data->title = @$item->get_title();
					$data->link  = @$item->get_link();
					$data->date  = @strtolower($item->get_date('d-M-Y'));
					$data->text = @JFilterOutput::stripImages($item->get_description());
					$data->text = strip_tags($data->text);
					$data->text = JHtml::_('string.truncate', $data->text, 125);

					$parsedFeeds[] = $data;
				}
			}
			else
			{
				$parsedFeeds = false;
			}

			return $parsedFeeds;
		}
	}
}
