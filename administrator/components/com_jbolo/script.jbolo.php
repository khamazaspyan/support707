<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2010-2011 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

defined('_JEXEC') or die();

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

class com_jboloInstallerScript
{
	/** @var array The list of extra modules and plugins to install */
	private $installation_queue = array(
		// modules => { (folder) => { (module) => { (position), (published) } }* }*
		'modules' => array(
			'admin' => array(
			),
			'site' => array(
				'mod_jbolo'       => array('', 1),
				'mod_jbolotheme'  => 0,
				'mod_jbolo_group' => array('js_groups_side_top', 0)
			)
		),

		// plugins => { (folder) => { (element) => (published) }* }*
		'plugins'=>array(
			'community'=>array(
				'plg_js_jbolo_online'=>1
			),
			'jbolo'=>array(
				'plg_jbolo_textprocessing'=>1
			),
			'system'=>array(
				'jbolo'=>1,
				'tjassetsloader'=>1
			),
			'user'=>array(
				'plg_user_jbolo_user'=>1
			)
		)
	);

	/** @var array The list of extra modules and plugins to uninstall */
	private $uninstall_queue = array(
		// modules => { (folder) => { (module) => { (position), (published) } }* }*
		'modules' => array(
			'admin' => array(
			),
			'site' => array(
				'mod_jbolo'       => 1,
				'mod_jbolotheme'  => 1,
				'mod_jbolo_group' => 1
			)
		),
		// plugins => { (folder) => { (element) => (published) }* }*
		'plugins'=>array(
			'community'=>array(
				'plg_js_jbolo_online'=>1
			),
			'jbolo'=>array(
				'plg_jbolo_textprocessing'=>1
			),
			'system'=>array(
				'jbolo'=>1
			),
			'user'=>array(
				'plg_user_jbolo_user'=>1
			)
		)
	);

	/** @var array The list of obsolete extra modules and plugins to uninstall when upgrading the component */
	private $obsolete_extensions_uninstallation_que = array(
		// modules => { (folder) => { (module) }* }*
		'modules' => array(
			'admin' => array(
			),
			'site' => array(
			)
		),
		// plugins => { (folder) => { (element) }* }*
		'plugins' => array(
			'system' => array(
				'plg_sys_jbolo_api'
			)
		)
	);

	/** @var array Obsolete files and folders to remove*/
	private $removeFilesAndFolders = array(
		'files'	=> array(
			/*since v3.1*/
			'components/com_jbolo/jbolo/assets/css/smoothness/jquery-ui-1.9.2.custom.min.css',
			'components/com_jbolo/jbolo/assets/css/bootstrap-ie6.min.css',
			'components/com_jbolo/jbolo/assets/css/bootstrap-image-gallery.min.css',
			'components/com_jbolo/jbolo/assets/js/ajaxupload-min.js',
			'components/com_jbolo/jbolo/assets/js/ajaxupload.js',
			'components/com_jbolo/jbolo/assets/js/bootstrap-image-gallery.min.js',
			'components/com_jbolo/jbolo/assets/js/bootstrap.min.js',
			'components/com_jbolo/jbolo/assets/js/html5.js',
			'components/com_jbolo/jbolo/assets/js/jquery-ui-1.9.2.custom.min.js',
			'components/com_jbolo/jbolo/assets/js/jquery.fileupload-fp.js',
			'components/com_jbolo/jbolo/assets/js/json_parse.js',
			/*since v3.2*/
			'components/com_jbolo/css/jboloapi.css',
			'components/com_jbolo/css/pure-css-speech-bubbles.css'
		),
		'folders' => array(
			/*since v3.2.5*/
			'administrator/components/com_jbolo/css',
			'administrator/components/com_jbolo/elements',
			'administrator/components/com_jbolo/images',
			'components/com_jbolo/css',
			'components/com_jbolo/jbolo'
		)
	);

	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	public function preflight($type, $parent)
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		// Only allow to install on Joomla! 2.5.0 or later
		//return version_compare(JVERSION, '2.5.0', 'ge');
	}

	/**
	 * Runs after install, update or discover_update
	 * @param string $type install, update or discover_update
	 * @param JInstaller $parent
	 */
	public function postflight( $type, $parent )
	{
		// Install subextensions
		$status = $this->_installSubextensions($parent);

		// Uninstall obsolete subextensions
		$uninstall_status = $this->_uninstallObsoleteSubextensions($parent);

		// Remove obsolete files and folders
		$removeFilesAndFolders = $this->removeFilesAndFolders;
		$this->_removeObsoleteFilesAndFolders($removeFilesAndFolders);

		// Install Techjoomla Straper
		$straperStatus = $this->_installStraper($parent);

		// Show the post-installation page
		$this->_renderPostInstallation($status, $straperStatus, $parent);

		//permission fix on after component install
		$this->permissionFix();
	}

	/**
	 * Removes obsolete files and folders
	 *
	 * @param array $removeFilesAndFolders
	 */
	private function _removeObsoleteFilesAndFolders($removeFilesAndFolders)
	{
		// Remove files

		jimport('joomla.filesystem.file');

		if (!empty($removeFilesAndFolders['files']))
		{
			foreach ($removeFilesAndFolders['files'] as $file)
			{
				$f = JPATH_ROOT . '/' . $file;

				if (!JFile::exists($f))
				{
					continue;
				}
				else
				{
					JFile::delete($f);
				}
			}
		}

		// Remove folders
		jimport('joomla.filesystem.file');

		if (!empty($removeFilesAndFolders['folders']))
		{
			foreach ($removeFilesAndFolders['folders'] as $folder)
			{
				$f = JPATH_ROOT . '/' . $folder;

				if (!JFolder::exists($f))
				{
					continue;
				}
				else
				{
					JFolder::delete($f);
				}
			}
		}
	}

	/**
	 * Renders the post-installation message
	 */
	private function _renderPostInstallation($status, $straperStatus, $parent)
	{
		if (JVERSION < '3.0')
		{
			$document = JFactory::getDocument();
			$document->addStyleSheet(JUri::root() . '/media/techjoomla_strapper/css/bootstrap.min.css');
		}

		$rows = 1;
		?>

		<h4><?php echo JText::_('JBolo Installation Status'); ?></h4>
		<table class="table table-striped table-condensed" style="font-weight:normal !important;">
			<thead>
				<tr>
					<th colspan="2">Extension</th>
					<th width="30%">Status</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
			<tbody>
				<tr>
					<td colspan="2">JBolo component</td>
					<td><strong style="color: green">Installed</strong></td>
				</tr>
				<tr>
					<td colspan="2">
						<strong>TechJoomla Strapper <?php echo $straperStatus['version']?></strong> [<?php echo $straperStatus['date'] ?>]
					</td>
					<td><strong>
						<span style="color: <?php echo $straperStatus['required'] ? ($straperStatus['installed']?'green':'red') : '#660' ?>; font-weight: bold;">
							<?php echo $straperStatus['required'] ? ($straperStatus['installed'] ?'Installed':'Not Installed') : 'Already up-to-date'; ?>
						</span>
					</strong></td>
				</tr>
				<?php if (count($status->modules)) : ?>
				<tr>
					<th>Module</th>
					<th>Client</th>
					<th></th>
				</tr>
				<?php foreach ($status->modules as $module) : ?>
				<tr class="row<?php echo ($rows++ % 2); ?>">
					<td><?php echo $module['name']; ?></td>
					<td><?php echo $module['client']; ?></td>
					<td><strong style="color: <?php echo ($module['result'])? "green" : "red"?>"><?php echo ($module['result'])?'Installed':'Not installed'; ?></strong></td>
				</tr>
				<?php endforeach;?>
				<?php endif;?>
				<?php if (count($status->plugins)) : ?>
				<tr>
					<th>Plugin</th>
					<th>Group</th>
					<th></th>
				</tr>
				<?php foreach ($status->plugins as $plugin) : ?>
				<tr class="row<?php echo ($rows++ % 2); ?>">
					<td><?php echo $plugin['name']; ?></td>
					<td><?php echo $plugin['group']; ?></td>
					<td><strong style="color: <?php echo ($plugin['result'])? "green" : "red"?>"><?php echo ($plugin['result'])?'Installed':'Not installed'; ?></strong></td>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>

				<?php
				if (isset($status->app_install)) :
					if (count($status->app_install)) : ?>
					<tr class="row1">
						<th>EasySocial App: JBolo</th>
						<th></th>
						<th></th>
						</tr>
					<?php

						foreach ($status->app_install as $app_install) : ?>
							<tr class="row2">
								<td><?php echo $app_install['name']; ?></td>
								<td></td>
								<td><strong style="color: <?php echo ($app_install['result'])? "green" : "red"?>"><?php echo ($app_install['result'])?'Installed':'Not installed'; ?></strong>
								<?php

									if(!empty($app_install['result'])) // if installed then only show msg
									{
										echo $mstat=($app_install['status']? "<span class=\"label label-success\">Enabled</span>" : "<span class=\"label label-important\">Disabled</span>");
									}

								?>
								</td>
							</tr>
						<?php endforeach;?>
					<?php endif;
				endif;?>
			</tbody>
		</table>

		<?php
	}

	private function _renderPostUninstallation($status, $parent)
	{
		if (JVERSION < '3.0')
		{
			$document = JFactory::getDocument();
			$document->addStyleSheet(JUri::root() . '/media/techjoomla_strapper/css/bootstrap.min.css');
		}

		$rows = 1;
		?>

		<h4><?php echo JText::_('JBolo Uninstallation Status'); ?></h4>
		<table class="adminlist table table-striped table-condensed" style="font-weight:normal !important;">
			<thead>
				<tr>
					<th colspan="2"><?php echo JText::_('Extension'); ?></th>
					<th width="30%"><?php echo JText::_('Status'); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
			<tbody>
				<tr>
					<td colspan="2"><?php echo 'JBolo '.JText::_('Component'); ?></td>
					<td><strong style="color: green"><?php echo JText::_('Removed'); ?></strong></td>
				</tr>
				<?php if (count($status->modules)) : ?>
				<tr>
					<th><?php echo JText::_('Module'); ?></th>
					<th><?php echo JText::_('Client'); ?></th>
					<th></th>
				</tr>
				<?php foreach ($status->modules as $module) : ?>
				<tr class="row<?php echo (++ $rows % 2); ?>">
					<td><?php echo $module['name']; ?></td>
					<td><?php echo $module['client']; ?></td>
					<td><strong style="color: <?php echo ($module['result'])? "green" : "red"?>"><?php echo ($module['result'])?JText::_('Removed'):JText::_('Not removed'); ?></strong></td>
				</tr>
				<?php endforeach;?>
				<?php endif;?>
				<?php if (count($status->plugins)) : ?>
				<tr>
					<th><?php echo JText::_('Plugin'); ?></th>
					<th><?php echo JText::_('Group'); ?></th>
					<th></th>
				</tr>
				<?php foreach ($status->plugins as $plugin) : ?>
				<tr class="row<?php echo (++ $rows % 2); ?>">
					<td><?php echo $plugin['name']; ?></td>
					<td><?php echo $plugin['group']; ?></td>
					<td><strong style="color: <?php echo ($plugin['result'])? "green" : "red"?>"><?php echo ($plugin['result'])?JText::_('Removed'):JText::_('Not removed'); ?></strong></td>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
		<?php
	}

	/**
	 * Installs subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param JInstaller $parent
	 * @return JObject The subextension installation status
	 */
	private function _installSubextensions($parent)
	{
		$src = $parent->getParent()->getPath('source');

		$db = JFactory::getDbo();

		$status = new JObject();
		$status->modules = array();
		$status->plugins = array();

		// Modules installation
		if(count($this->installation_queue['modules'])) {
			foreach($this->installation_queue['modules'] as $folder => $modules) {
				if(count($modules)) foreach($modules as $module => $modulePreferences) {
					// Install the module
					if(empty($folder)) $folder = 'site';
					$path = "$src/modules/$folder/$module";
					if(!is_dir($path)) {
						$path = "$src/modules/$folder/mod_$module";
					}
					if(!is_dir($path)) {
						$path = "$src/modules/$module";
					}
					if(!is_dir($path)) {
						$path = "$src/modules/mod_$module";
					}
					if(!is_dir($path)) continue;
					// Was the module already installed?
					$sql = $db->getQuery(true)
						->select('COUNT(*)')
						->from('#__modules')
						->where($db->qn('module').' = '.$db->q($module));
					$db->setQuery($sql);
					$count = $db->loadResult();
					$installer = new JInstaller;
					$result = $installer->install($path);
					$status->modules[] = array(
						'name'=>$module,
						'client'=>$folder,
						'result'=>$result
					);
					// Modify where it's published and its published state
					if(!$count) {
						// A. Position and state
						list($modulePosition, $modulePublished) = $modulePreferences;
						if($modulePosition == 'cpanel') {
							$modulePosition = 'icon';
						}
						$sql = $db->getQuery(true)
							->update($db->qn('#__modules'))
							->set($db->qn('position').' = '.$db->q($modulePosition))
							->where($db->qn('module').' = '.$db->q($module));
						if($modulePublished) {
							$sql->set($db->qn('published').' = '.$db->q('1'));
						}
						$db->setQuery($sql);
						$db->execute();

						// B. Change the ordering of back-end modules to 1 + max ordering
						if($folder == 'admin') {
							$query = $db->getQuery(true);
							$query->select('MAX('.$db->qn('ordering').')')
								->from($db->qn('#__modules'))
								->where($db->qn('position').'='.$db->q($modulePosition));
							$db->setQuery($query);
							$position = $db->loadResult();
							$position++;

							$query = $db->getQuery(true);
							$query->update($db->qn('#__modules'))
								->set($db->qn('ordering').' = '.$db->q($position))
								->where($db->qn('module').' = '.$db->q($module));
							$db->setQuery($query);
							$db->execute();
						}

						// C. Link to all pages
						$query = $db->getQuery(true);
						$query->select('id')->from($db->qn('#__modules'))
							->where($db->qn('module').' = '.$db->q($module));
						$db->setQuery($query);
						$moduleid = $db->loadResult();

						$query = $db->getQuery(true);
						$query->select('*')->from($db->qn('#__modules_menu'))
							->where($db->qn('moduleid').' = '.$db->q($moduleid));
						$db->setQuery($query);
						$assignments = $db->loadObjectList();
						$isAssigned = !empty($assignments);
						if(!$isAssigned) {
							$o = (object)array(
								'moduleid'	=> $moduleid,
								'menuid'	=> 0
							);
							$db->insertObject('#__modules_menu', $o);
						}
					}
				}
			}
		}

		// Plugins installation
		if(count($this->installation_queue['plugins'])) {
			foreach($this->installation_queue['plugins'] as $folder => $plugins) {
				if(count($plugins)) foreach($plugins as $plugin => $published) {
					$path = "$src/plugins/$folder/$plugin";
					if(!is_dir($path)) {
						$path = "$src/plugins/$folder/plg_$plugin";
					}
					if(!is_dir($path)) {
						$path = "$src/plugins/$plugin";
					}
					if(!is_dir($path)) {
						$path = "$src/plugins/plg_$plugin";
					}
					if(!is_dir($path)) continue;

					// Was the plugin already installed?
					$query = $db->getQuery(true)
						->select('COUNT(*)')
						->from($db->qn('#__extensions'))
						->where($db->qn('element').' = '.$db->q($plugin))
						->where($db->qn('folder').' = '.$db->q($folder));
					$db->setQuery($query);
					$count = $db->loadResult();

					$installer = new JInstaller;
					$result = $installer->install($path);

					$status->plugins[] = array('name'=>$plugin,'group'=>$folder, 'result'=>$result);

					if($published && !$count) {
						$query = $db->getQuery(true)
							->update($db->qn('#__extensions'))
							->set($db->qn('enabled').' = '.$db->q('1'))
							->where($db->qn('element').' = '.$db->q($plugin))
							->where($db->qn('folder').' = '.$db->q($folder));
						$db->setQuery($query);
						$db->execute();
					}
				}
			}
		}

		// Install easysocial plugins
		if (file_exists(JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/foundry.php'))
		{
			require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/foundry.php';
			$installer = Foundry::get( 'Installer' );

			// The $path here refers to your application path
			$installer->load($src . "/plugins/easysocial/jbolo");
			$plg_install           = $installer->install();
			$status->app_install[] = array('name' => 'jbolo', 'group' => 'user', 'result' => $plg_install, 'status' => '1');

			// Second app
			$installer->load($src . "/plugins/easysocial/jbolo_group");
			$plg_install           = $installer->install();
			$status->app_install[] = array('name' => 'jbolo_group', 'group' => 'groups', 'result' => $plg_install, 'status' => '1');
		}

		return $status;
	}

	/**
	 * Uninstalls subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param JInstaller $parent
	 * @return JObject The subextension uninstallation status
	 */
	private function _uninstallSubextensions($parent)
	{
		jimport('joomla.installer.installer');

		$db =  JFactory::getDBO();

		$status = new JObject();
		$status->modules = array();
		$status->plugins = array();

		$src = $parent->getParent()->getPath('source');

		// Modules uninstallation
		if(count($this->uninstall_queue['modules'])) {
			foreach($this->uninstall_queue['modules'] as $folder => $modules) {
				if(count($modules)) foreach($modules as $module => $modulePreferences) {
					// Find the module ID
					$sql = $db->getQuery(true)
						->select($db->qn('extension_id'))
						->from($db->qn('#__extensions'))
						->where($db->qn('element').' = '.$db->q($module))
						->where($db->qn('type').' = '.$db->q('module'));
					$db->setQuery($sql);
					$id = $db->loadResult();
					// Uninstall the module
					if($id) {
						$installer = new JInstaller;
						$result = $installer->uninstall('module',$id,1);
						$status->modules[] = array(
							'name'=>$module,
							'client'=>$folder,
							'result'=>$result
						);
					}
				}
			}
		}

		// Plugins uninstallation
		if(count($this->uninstall_queue['plugins'])) {
			foreach($this->uninstall_queue['plugins'] as $folder => $plugins) {
				if(count($plugins)) foreach($plugins as $plugin => $published) {
					$sql = $db->getQuery(true)
						->select($db->qn('extension_id'))
						->from($db->qn('#__extensions'))
						->where($db->qn('type').' = '.$db->q('plugin'))
						->where($db->qn('element').' = '.$db->q($plugin))
						->where($db->qn('folder').' = '.$db->q($folder));
					$db->setQuery($sql);

					$id = $db->loadResult();
					if($id)
					{
						$installer = new JInstaller;
						$result = $installer->uninstall('plugin',$id);
						$status->plugins[] = array(
							'name'=>$plugin,
							'group'=>$folder,
							'result'=>$result
						);
					}
				}
			}
		}

		return $status;
	}

	private function _installStraper($parent)
	{
		$src = $parent->getParent()->getPath('source');

		// Install strapper
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.date');

		$source = $src . '/tj_strapper';
		$target = JPATH_ROOT . '/media/techjoomla_strapper';

		$haveToInstallStraper = false;

		if (!JFolder::exists($target))
		{
			$haveToInstallStraper = true;
		}
		else
		{
			$straperVersion = array();

			if (JFile::exists($target . '/version.txt'))
			{
				$rawData = file_get_contents($target . '/version.txt');
				$info    = explode("\n", $rawData);
				$straperVersion['installed'] = array(
					'version' => trim($info[0]),
					'date'    => new JDate(trim($info[1]))
				);
			}
			else
			{
				$straperVersion['installed'] = array(
					'version' => '0.0',
					'date'    => new JDate('2011-01-01')
				);
			}

			$rawData = file_get_contents($source . '/version.txt');
			$info    = explode("\n", $rawData);
			$straperVersion['package'] = array(
				'version' => trim($info[0]),
				'date'    => new JDate(trim($info[1]))
			);

			$haveToInstallStraper = $straperVersion['package']['date']->toUNIX() > $straperVersion['installed']['date']->toUNIX();
		}

		$installedStraper = false;

		if ($haveToInstallStraper)
		{
			$versionSource = 'package';
			$installer     = new JInstaller;
			$installedStraper = $installer->install($source);
		}
		else
		{
			$versionSource = 'installed';
		}

		if (!isset($straperVersion))
		{
			$straperVersion = array();

			if (JFile::exists($target . '/version.txt'))
			{
				$rawData = file_get_contents($target . '/version.txt');
				$info    = explode("\n", $rawData);
				$straperVersion['installed'] = array(
					'version' => trim($info[0]),
					'date'    => new JDate(trim($info[1]))
				);
			}
			else
			{
				$straperVersion['installed'] = array(
					'version' => '0.0',
					'date'    => new JDate('2011-01-01')
				);
			}

			$rawData = file_get_contents($source . '/version.txt');
			$info    = explode("\n", $rawData);
			$straperVersion['package'] = array(
				'version' => trim($info[0]),
				'date'    => new JDate(trim($info[1]))
			);

			$versionSource = 'installed';
		}

		if (!($straperVersion[$versionSource]['date'] instanceof JDate))
		{
			$straperVersion[$versionSource]['date'] = new JDate;
		}

		return array(
			'required'	=> $haveToInstallStraper,
			'installed'	=> $installedStraper,
			'version'	=> $straperVersion[$versionSource]['version'],
			'date'		=> $straperVersion[$versionSource]['date']->format('Y-m-d'),
		);
	}

	/**
	 * Method to install the component
	 *
	 * @param   JInstaller  $parent  Class calling this method
	 *
	 * @return  void
	 */
	public function install($parent)
	{
	}

	/**
	 * Method to uninstall the component
	 *
	 * @param   JInstaller  $parent  Class calling this method
	 *
	 * @return  void
	 */
	public function uninstall($parent)
	{
		// Uninstall subextensions
		$status = $this->_uninstallSubextensions($parent);

		// Show the post-uninstallation page
		$this->_renderPostUninstallation($status, $parent);
	}

	/**
	 * Method to update the component
	 *
	 * @param   JInstaller  $parent  Class calling this method
	 *
	 * @return  void
	 */
	public function update($parent)
	{
		// Since version 3.0.2
		$this->fix_db_on_update();

		// Create core tables
		$this->runSQL($parent, 'install.sql');
	}

	/**
	 * Execute sql files
	 *
	 * @param   JInstaller  $parent   Class calling this method
	 * @param   string      $sqlfile  Sql file name
	 *
	 * @return  boolean
	 */
	private function runSQL($parent, $sqlfile)
	{
		$db = JFactory::getDBO();

		// Obviously you may have to change the path and name if your installation SQL file ;)
		if (method_exists($parent, 'extension_root'))
		{
			$sqlfile = $parent->getPath('extension_root') . '/admin/sqlfiles/' . $sqlfile;
		}
		else
		{
			$sqlfile = $parent->getParent()->getPath('extension_root') . '/sqlfiles/' . $sqlfile;
		}

		// Don't modify below this line
		$buffer = file_get_contents($sqlfile);

		if ($buffer !== false)
		{
			jimport('joomla.installer.helper');
			$queries = JInstallerHelper::splitSql($buffer);

			if (count($queries) != 0)
			{
				foreach ($queries as $query)
				{
					$query = trim($query);

					if ($query != '' && $query{0} != '#')
					{
						$db->setQuery($query);

						if (!$db->execute())
						{
							JError::raiseWarning(1, JText::sprintf('JLIB_INSTALLER_ERROR_SQL_ERROR', $db->stderr(true)));

							return false;
						}
					}
				}
			}
		}
	}

	/**
	 * Update database structure to current Jbolo version
	 *
	 * @return  void
	 */
	private function fix_db_on_update()
	{
		$db = JFactory::getDBO();

		// JBolo users table changes start

		// Since version 3.0.2
		// Check if column - state exists
		$query = "SHOW COLUMNS FROM `#__jbolo_users` WHERE `Field` = 'state' AND `Type` = 'TINYINT(1)'";
		$db->setQuery($query);
		$check = $db->loadResult();

		if (!$check)
		{
			$query = "ALTER TABLE `#__jbolo_users`
			 ADD `state` TINYINT(1) NOT NULL DEFAULT '1' AFTER `status_msg`";
			$db->setQuery($query);

			if (!$db->execute())
			{
				JError::raiseError(500, $db->stderr());
			}
		}

		// Since version 3.2.5
		// Check if column - id exists
		$query = "SHOW COLUMNS FROM `#__jbolo_users` WHERE `Field` = 'id' AND `Type` = 'INT(11)'";
		$db->setQuery($query);
		$check = $db->loadResult();

		if (!$check)
		{
			$query = "ALTER TABLE `#__jbolo_users` ADD `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST";
			$db->setQuery($query);

			if (!$db->execute())
			{
				JError::raiseError(500, $db->stderr());
			}
		}

		// Since version 3.2.5
		// Check if column - is_mobile exists
		$query = "SHOW COLUMNS FROM `#__jbolo_users` WHERE `Field` = 'is_mobile' AND `Type` = 'TINYINT(1)'";
		$db->setQuery($query);
		$check = $db->loadResult();

		if (!$check)
		{
			$query = "ALTER TABLE `#__jbolo_users`
			 ADD `is_mobile` TINYINT(1) NOT NULL DEFAULT '0' COMMENT " . $db->quote("0-PC, 1-mobile, 2-tablet") . " AFTER `state`";
			$db->setQuery($query);

			if (!$db->execute())
			{
				JError::raiseError(500, $db->stderr());
			}
		}

		// Since version 3.2.5
		// Check if column - last_activity exists
		$query = "SHOW COLUMNS FROM `#__jbolo_users` WHERE `Field` = 'last_activity' AND `Type` = 'TIMESTAMP'";
		$db->setQuery($query);
		$check = $db->loadResult();

		if (!$check)
		{
			$query = "ALTER TABLE `#__jbolo_users`
			 ADD `last_activity` TIMESTAMP NULL DEFAULT NULL AFTER `is_mobile`";
			$db->setQuery($query);

			if (!$db->execute())
			{
				JError::raiseError(500, $db->stderr());
			}
		}

		// JBolo users table changes end

		// JBolo nodes table changes start

		// Since version 3.2
		// Check if column - type exists
		$query = "SHOW COLUMNS FROM `#__jbolo_nodes` WHERE `Field` = 'group_id' AND `Type` = 'INT(11)'";
		$db->setQuery($query);
		$check = $db->loadResult();

		if (!$check)
		{
			$query = "ALTER TABLE `#__jbolo_nodes`
			 ADD `group_id` INT(11) NOT NULL DEFAULT '0' COMMENT " . $db->quote("Client id") . " AFTER `time`";

			$db->setQuery($query);

			if (!$db->execute())
			{
				JError::raiseError(500, $db->stderr());
			}
		}

		// Since version 3.2
		// Check if column - type exists
		$query = "SHOW COLUMNS FROM `#__jbolo_nodes` WHERE `Field` = 'client' AND `Type` = 'VARCHAR(255)'";
		$db->setQuery($query);
		$check = $db->loadResult();

		if (!$check)
		{
			$query = "ALTER TABLE `#__jbolo_nodes`
			 ADD `client` VARCHAR(255) DEFAULT NULL COMMENT " . $db->quote("Client ID for e.g. com_community.groups") . " AFTER `group_id`";
			$db->setQuery($query);

			if (!$db->execute())
			{
				JError::raiseError(500, $db->stderr());
			}
		}

		// Since version 3.2
		// Check if column - type exists
		$query = "SHOW COLUMNS FROM `#__jbolo_nodes` WHERE `Field` = 'status' AND `Type` = 'TINYINT(1)'";
		$db->setQuery($query);
		$check = $db->loadResult();

		if (!$check)
		{
			$query = "ALTER TABLE `#__jbolo_nodes`
			 ADD `status` TINYINT(1) NOT NULL DEFAULT '1' COMMENT " . $db->quote("Node status 0 - inactive, 1 - active") . " AFTER `client`";
			$db->setQuery($query);

			if (!$db->execute())
			{
				JError::raiseError(500, $db->stderr());
			}
		}

		// JBolo nodes table changes end
	}

	/**
	 * Adds default permission entores in Joomla ACL for JBoolo
	 *
	 * @return  void
	 */
	private function permissionFix()
	{
		$db    = JFactory::getDbo();
		$query = "SELECT id, rules
		 FROM `#__assets`
		 WHERE `name`='com_jbolo'";
		$db->setQuery($query);
		$result = $db->loadObject();

		if (strlen(trim($result->rules)) <= 3)
		{
			$obj        = new Stdclass;
			$obj->id    = $result->id;
			$obj->rules = '{"core.admin":[],"core.manage":[],"core.chat":{"6":1,"2":1},"core.group_chat":{"6":1,"2":1},"core.add_member_in_group_chat":{"6":1,"2":1},"core.send_file":{"6":1,"2":1},"core.view_history":{"6":1,"2":1},"core.live_support":{"6":1,"2":0,"3":0,"4":0,"5":0}}';

			if (!$db->updateObject('#__assets', $obj, 'id'))
			{
				$app = JFactory::getApplication();
				$app->enqueueMessage($db->stderr(), 'error');
			}
		}
	}

	/**
	 * Uninstalls obsolete subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param   JInstaller  $parent  Install source
	 *
	 * @return  JObject     The subextension uninstallation status
	 */
	private function _uninstallObsoleteSubextensions($parent)
	{
		JLoader::import('joomla.installer.installer');

		$db = JFactory::getDbo();

		$status          = new JObject;
		$status->modules = array();
		$status->plugins = array();

		$src = $parent->getParent()->getPath('source');

		// Modules uninstallation
		if (count($this->obsolete_extensions_uninstallation_que['modules']))
		{
			foreach ($this->obsolete_extensions_uninstallation_que['modules'] as $folder => $modules)
			{
				if (count($modules))
				{
					foreach ($modules as $module)
					{
						// Find the module ID
						$sql = $db->getQuery(true)
							->select($db->qn('extension_id'))
							->from($db->qn('#__extensions'))
							->where($db->qn('element') . ' = ' . $db->q($module))
							->where($db->qn('type') . ' = ' . $db->q('module'));
						$db->setQuery($sql);
						$id = $db->loadResult();

						// Uninstall the module
						if ($id)
						{
							$installer = new JInstaller;
							$result    = $installer->uninstall('module', $id, 1);
							$status->modules[] = array(
								'name'   => $module,
								'client' => $folder,
								'result' => $result
							);
						}
					}
				}
			}
		}

		// Plugins uninstallation
		if (count($this->obsolete_extensions_uninstallation_que['plugins']))
		{
			foreach ($this->obsolete_extensions_uninstallation_que['plugins'] as $folder => $plugins)
			{
				if (count($plugins))
				{
					foreach ($plugins as $plugin)
					{
						$sql = $db->getQuery(true)
							->select($db->qn('extension_id'))
							->from($db->qn('#__extensions'))
							->where($db->qn('type') . ' = ' . $db->q('plugin'))
							->where($db->qn('element') . ' = ' . $db->q($plugin))
							->where($db->qn('folder') . ' = ' . $db->q($folder));
						$db->setQuery($sql);

						$id = $db->loadResult();

						if ($id)
						{
							$installer = new JInstaller;
							$result    = $installer->uninstall('plugin', $id, 1);
							$status->plugins[] = array(
								'name'   => 'plg_' . $plugin,
								'group'  => $folder,
								'result' => $result
							);
						}
					}
				}
			}
		}

		return $status;
	}
}
