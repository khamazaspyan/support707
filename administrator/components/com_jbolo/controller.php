<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2012-2013 Techjoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 * @website    http://techjoomla.com
 */

// No direct access.
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Component Controller
 *
 * @since  1.5
 */
class JboloController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   array    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController		This object to support chaining.
	 *
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$mainframe = JFactory::getApplication();
		$input     = JFactory::getApplication()->input;

		$vName          = $input->get('view', 'cp');
		$controllerName = $input->get('controller', 'cp');
		$cp             = '';

		switch ($vName)
		{
			default:
			case 'cp':
				$cp      = true;
				$vName   = 'cp';
				$vLayout = $input->get('layout', 'default');
				$mName   = 'cp';
			break;
		}

		$document = JFactory::getDocument();
		$vType    = $document->getType();

		// Get / create the view
		$view = $this->getView($vName, $vType);

		// Get / create the model
		if ($model = $this->getModel($mName))
		{
			// Push the model into the view (as default)
			$view->setModel($model, true);
		}

		// Set the layout
		$view->setLayout($vLayout);

		// Display the view
		$view->display();
	}

	/**
	 * Get News Feeds
	 *
	 * @return  json
	 */
	public function getNewsFeeds()
	{
		$model = $this->getModel('cp');
		$feeds = $model->getNewsFeeds();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($feeds);
		jexit();
	}
}
