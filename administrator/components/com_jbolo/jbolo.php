<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2012-2013 Techjoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 * @website    http://techjoomla.com
 */

// No direct access.
defined('_JEXEC') or die('Restricted access');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_jbolo'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Define directory separator.
if (! defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

// Load assets
jimport('joomla.filesystem.file');
$tjStrapperPath = JPATH_ROOT . '/media/techjoomla_strapper/tjstrapper.php';

if (JFile::exists($tjStrapperPath))
{
	require_once $tjStrapperPath;
	TjStrapper::loadTjAssets('com_jbolo');
}

if (JVERSION < '3.0')
{
	// Define wrapper class
	define('JBOLO_WRAPPER_CLASS', "jbolo-wrapper techjoomla-bootstrap");
}
else
{
	// Define wrapper class
	define('JBOLO_WRAPPER_CLASS', "jbolo-wrapper");
}

// Require the base controller.
require_once JPATH_COMPONENT . DS . 'controller.php';

// Require specific controller if requested.
$input = JFactory::getApplication()->input;

if ($controller = $input->get('controller', '', 'STRING'))
{
	$path = JPATH_COMPONENT . DS . 'controllers' . DS . $controller . '.php';

	if (file_exists($path))
	{
		require_once $path;
	}
	else
	{
		$controller = '';
	}
}

// Create the controller.
$classname = 'JboloController' . ucfirst($controller);
$controller = new $classname;

// Perform the Request task.
$controller->execute($input->get('task', '', 'STRING'));

// Redirect if set by the controller.
$controller->redirect();
