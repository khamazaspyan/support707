<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
// No direct access
defined("_JEXEC") or die();

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Donors records.
 *
 * @since  1.8.9
 */
class JGiveModelDonors extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.7
	 * @see     JController
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'user_id', 'a.user_id',
				'campaign_id', 'a.campaign_id',
				'email', 'a.email',
				'first_name', 'a.first_name',
				'last_name', 'a.last_name',
				'address', 'a.address',
				'address2', 'a.address2',
				'city', 'a.city',
				'state', 'a.state',
				'country', 'a.country',
				'zip', 'a.zip',
				'phone', 'a.phone',
				'giveback_id', 'g.id',
				'donation_amount', 'o.amount',
				'cdate', 'o.cdate',
				'campaigns_title', 'c.campaigns_title',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Set ordering.
		$orderCol = $app->getUserStateFromRequest($this->context . '.filter_order', 'filter_order');

		if (!in_array($orderCol, $this->filter_fields))
		{
			$orderCol = 'a.id';
		}

		$this->setState('list.ordering', $orderCol);

		// Set ordering direction.
		$listOrder = $app->getUserStateFromRequest($this->context . 'filter_order_Dir', 'filter_order_Dir');

		if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', '')))
		{
			$listOrder = 'ASC';
		}

		// Load the filter search
		$search = $app->getUserStateFromRequest($this->context . 'filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		// Load the filter campaign
		$campaign_id = $app->getUserStateFromRequest($this->context . 'filter.campaign_id', 'filter_campaign_id', '', 'string');
		$this->setState('filter.campaign_id', $campaign_id);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jgive');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.8.9
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select($this->getState('list.select', 'DISTINCT a.*'));
		$query->from('`#__jg_donors` AS a');

		// Join over the foreign key 'campaign_id'
		$query->select('c.title AS campaigns_title');
		$query->select('g.id AS gid');
		$query->select('g.description AS gdesc');
		$query->select('o.amount AS donation_amount');
		$query->select('o.cdate AS cdate');
		$query->select('cou.country');
		$query->select('reg.region AS state');
		$query->join('LEFT', '#__jg_campaigns AS c ON c.id = a.campaign_id');
		$query->join('LEFT', '#__jg_donations AS d ON d.donor_id = a.id');
		$query->join('LEFT', '#__jg_campaigns_givebacks AS g ON g.id = d.giveback_id');
		$query->join('LEFT', '#__jg_orders AS o ON o.donor_id = a.id');
		$query->join('LEFT', '#__tj_country AS cou ON cou.id = a.country');
		$query->join('LEFT', '#__tj_region AS reg ON reg.id = a.state');
		$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				// Search record
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('(a.first_name LIKE ' . $search . ' OR a.last_name LIKE' . $search . ' OR  a.email LIKE ' . $search . ')');
			}
		}

		// Filtering campaign_id
		$filter_campaign_id = $this->state->get("filter.campaign_id");

		if ($filter_campaign_id)
		{
			// Code for filter record
			$query->where("a.campaign_id = '" . $db->escape($filter_campaign_id) . "'");
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
		$db = JFactory::getDbo();

		foreach ($items as $item)
		{
			if (isset($item->city) && $item->city != '')
			{
				if (is_object($item->city))
				{
					$item->city = JArrayHelper::fromObject($item->city);
				}

				$values = (is_array($item->city)) ? $item->city : explode(',', $item->city);
				$textValue = array();

				foreach ($values as $value)
				{
					// Fetch city by id
					$query = $db->getQuery(true);
					$query->select($db->quoteName('city'))
							->from('`#__tj_city`')
							->where($db->quoteName('id') . ' = ' . $db->quote($db->escape($value)));

					$db->setQuery($query);
					$results = $db->loadObject();

					if ($results)
					{
						$textValue[] = $results->city;
					}
				}

				$item->city = !empty($textValue) ? implode(', ', $textValue) : $item->city;
			}
		}

		return $items;
	}
}
