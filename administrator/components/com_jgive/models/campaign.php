<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modeladmin');
jimport('techjoomla.common');

// Load videos class
require_once JPATH_SITE . '/components/com_jgive/helpers/video/videos.php';

// Load TJ-Fields trait
require_once JPATH_SITE . "/components/com_tjfields/filterFields.php";

/**
 * Methods supporting a campaign records.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.7
 */
class JgiveModelCampaign extends JModelAdmin
{
	use TjfieldsFilterField;

	protected $params;

	/**
	 * Class constructor.
	 *
	 * @since   1.6
	 */
	public function __construct()
	{
		$this->params = JComponentHelper::getParams('com_jgive');
		$this->techjoomlacommon = new TechjoomlaCommon;

		parent::__construct();
	}

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.7
	 */
	protected $text_prefix = 'COM_JGIVE';

	/**
	 * Returns a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable    A database object
	 */
	public function getTable($type = 'Campaign', $prefix = 'JgiveTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	$data  The data for the form.
	 *
	 * @since	1.7
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_jgive.edit.campaign.data', array());

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed  $item  Object on success, false on failure.
	 *
	 * @since	1.7
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed

			// Needed for loading states
			require_once JPATH_SITE . "/components/com_jgive/helper.php";
			$jgiveFrontendHelper = new jgiveFrontendHelper;

			require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
			$campaignHelper = new campaignHelper;

			// Get campaign images
			$item->images = $campaignHelper->getCampaignImages($item->id);

			// Get campaign videos
			$item->video = $this->_getCampaignMedia($item->id);

			// Get campaign givebacks
			$item->givebacks = $campaignHelper->getCampaignGivebacks($item->id);

			// Get Campaign Updates
			$item->updates = $campaignHelper->getCampaignUpdates($item->id);

			$item->days_limit = $campaignHelper->getDateDiffInDays($item->start_date, $item->end_date);
		}

		return $item;
	}

	/**
	 * Get Thumbnail video
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  Video Thumbnail
	 */
	public function _getCampaignMedia($cid)
	{
		// Create a new query object.
		try
		{
			$query = $this->_db->getQuery(true);

			$query->select(array('id','filename','path','type','`default`','content_id','thumb_filename','thumb_path'));
			$query->select(array('url'));

			$query->from('#__jg_campaigns_media');
			$query->where('content_id  = ' . $cid);
			$query->order('type ASC');

			$this->_db->setQuery($query);
			$media = $this->_db->loadObjectList();

			return $media;
		}
		catch (Exception $e)
		{
			$app->enqueueMessage($e->getMessage(), 'error');
			$this->setError($e->getMessage());

			throw new Exception($e->getMessage());
		}
	}

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable  $table  A JTable object.
	 *
	 * @return  void
	 *
	 * @since   1.7
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__jg_campaigns');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

	/**
	 * Method to toggle the featured setting of products.
	 *
	 * @param   array    $pks    The ids of the items to toggle.
	 * @param   integer  $value  The value to toggle to.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.7
	 */
	public function featured($pks, $value = 0)
	{
		$table = $this->getTable();

		// Featured/UnFeatured campaigns
		if (!$table->featured($pks, $value))
		{
			$this->setError($table->getError());

			return false;
		}

		return true;
	}

	/**
	 * Method to saves a new campaign details
	 *
	 * @param   Object  $extra_jform_data  Extra fields data
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.7
	 */
	public function saveNewCampaign($extra_jform_data)
	{
		$is_error_flag_set = 0;

		require_once JPATH_SITE . "/components/com_jgive/helpers/media.php";
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$session     = JFactory::getSession();
		$promoter_id = $post->get('promoter_id');
		$user        = JFactory::getUser($promoter_id);
		$userid      = $user->id;

		if (!$userid)
		{
			$userid = 0;

			return false;
		}

		// Save campaign details
		// Prepare object for insert
		$obj          = $this->createCampaignObject($post, $userid, null);

		// @TODO use lang constant for date
		$obj->created = date(JText::_('Y-m-d H:i:s'));

		// Insert object
		if (!$this->_db->insertObject('#__jg_campaigns', $obj, 'id'))
		{
			echo $this->_db->stderr();

			return false;
		}

		// Get last insert id
		$camp_id = $this->_db->insertid();
		$session->set('camapign_id', $camp_id);

		// Save giveback details
		$params               = JComponentHelper::getParams('com_jgive');
		$creatorfield         = array();
		$show_selected_fields = $params->get('show_selected_fields');
		$show_field           = 0;
		$give_back_cnf        = 0;

		if ($show_selected_fields)
		{
			$creatorfield = $params->get('creatorfield');

			if (isset($creatorfield))
			{
				if (in_array('give_back', $creatorfield))
				{
					$give_back_cnf = 1;
				}
			}
		}
		else
		{
			$show_field = 1;
		}

		// Save give back detail only when show give back is yes (When editing campaign)
		if ($show_field == 1 OR $give_back_cnf == 0)
		{
			$givebackSaveSuccess = $this->saveGivebacks($post, $camp_id);

			if (!$givebackSaveSuccess)
			{
				return false;
			}
		}

		// Save Campaign Updates here
		$updatesSaveSucess = $this->saveUpdates($post, $camp_id);

		if (!$updatesSaveSucess)
		{
			return false;
		}

		// Upload campaign image
		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;
		$jgivemediaHelper = new jgivemediaHelper;

		$img_dimensions   = array();
		$img_dimensions[] = 'small';
		$img_dimensions[] = 'medium';
		$img_dimensions[] = 'large';
		$image_path       = array();

		// @params filed_name,image dimensions,resize=0 or not_resize=1(upload original)
		$img_org_name = $jgivemediaHelper->imageupload('camp_image', $img_dimensions, 0);

		if (empty($img_org_name))
		{
			if ($img_org_name)
			{
				$uploadSuccess = $campaignHelper->uploadImage($camp_id, 'camp_image', 0, '');

				if (!$uploadSuccess)
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			$db               = JFactory::getDBO();
			$obj              = new stdClass;
			$obj->id          = '';
			$obj->campaign_id = $camp_id;

			$image_upload_path_for_db = 'images/jGive';
			$image_upload_path_for_db .= DS . $img_org_name;

			$obj->path          = $image_upload_path_for_db;
			$obj->gallery       = 0;
			$obj->order         = '';

			if (!$db->insertObject('#__jg_campaigns_images', $obj, 'id'))
			{
				echo $db->stderr();

				return false;
			}

			$uploadSuccess = $db->insertid();
		}

		// Save video_url & other details
		if ($uploadSuccess)
		{
			$obj                 = new stdClass;
			$obj->id             = $uploadSuccess;
			$obj->video_provider = $post->get('video_provider', '', 'STRING');
			$obj->video_url      = $video_url;
			$obj->video_img      = $post->get('video_img', '', 'INT');

			if (!$this->_db->updateObject('#__jg_campaigns_images', $obj, 'id'))
			{
				echo $db->stderr();

				return false;
			}
		}

		// @Amol Add new images in gallery
		$file_field  = 'jgive_img_gallery';
		$file_errors = $_FILES[$file_field]['error'];

		foreach ($file_errors as $key => $file_error)
		{
			// If file field is not empty
			if (!$file_error == 4)
			{
				$uploadSuccess = $campaignHelper->uploadImage($camp_id, 'jgive_img_gallery', 0, $key);
			}

			// If file upload_max_filesize exceed
			if ($file_error == 1)
			{
				$app     = JFactory::getApplication();
				$size    = ini_get("upload_max_filesize");
				$message = JText::sprintf('COM_JGIVE_FILE_SIZE_EXCEEDS', $size);

				$app->enqueueMessage($message, 'error');
				$is_error_flag_set = 1;
			}
			elseif($uploadSuccess === false)
			{
				$is_error_flag_set = 1;
			}
		}

		// Call function to save videos
		$video_error_flag_set = JgiveVideos::videoGallery($post, $camp_id);

		// Send campaigns created email to creator & site admin
		$params = JComponentHelper::getParams('com_jgive');

		if ($params->get('admin_approval'))
		{
			// Email to site admin
			$campaignHelper->sendCmap_create_mail($post, $camp_id);

			// Email to campaign creator
			$campaignHelper->sendCmapCreateMailToPromoter($post, $camp_id);
		}

		// Trigger plugins
		// OnAfterJGiveCampaignSave
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');

		// Call the plugin and get the result
		$result = $dispatcher->trigger('OnAfterJGiveCampaignSave', array($camp_id,$post));

		// Trigger after save campaign
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('content');
		$dispatcher->trigger('onAfterCampaignSave', array($post));

		// The error flag is set
		if ($is_error_flag_set)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * Lets you edit campaign
	 *
	 * @param   Object  $extra_jform_data  Extra fields data
	 *
	 * @return  void
	 */
	public function edit($extra_jform_data)
	{
		$is_error_flag_set = 0;
		$oldDetails      = '';
		$newDetails      = '';

		require_once JPATH_SITE . "/components/com_jgive/helpers/media.php";
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$cid         = $post->get('cid');
		$promoter_id = $post->get('promoter_id');
		$user        = JFactory::getUser($promoter_id);
		$userid      = $user->id;

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;

		$oldDetails      = $this->getItem($cid);

		if (!$userid)
		{
			$userid = 0;

			return false;
		}

		$give_back_ids         = $post->get('ids', '', 'Array');
		$givebackDeleteSuccess = $this->deleteGivebacks($give_back_ids, $cid);

		$updatesids = $post->get('updatesids', '', 'Array');
		$updatesDeleteSuccess = $this->deleteUpdates($updatesids, $cid);

		// Save campaign details
		$obj           = $this->createCampaignObject($post, $userid, $cid);
		$obj->modified = date(JText::_('Y-m-d H:i:s'));

		if (!$this->_db->updateObject('#__jg_campaigns', $obj, 'id'))
		{
			echo $this->_db->stderr();

			return false;
		}

		// Save giveback details

		// For edit , delete all existing givebacks for this campaign and re-add new
		$params               = JComponentHelper::getParams('com_jgive');
		$creatorfield         = array();
		$show_selected_fields = $params->get('show_selected_fields');
		$show_field           = 0;
		$give_back_cnf        = 0;

		if ($show_selected_fields)
		{
			$creatorfield = $params->get('creatorfield');

			if (isset($creatorfield))
			{
				if (in_array('give_back', $creatorfield))
				{
					$give_back_cnf = 1;
				}
			}
		}
		else
		{
			$show_field = 1;
		}

		// Save giveback details
		$params               = JComponentHelper::getParams('com_jgive');
		$creatorfield         = array();
		$show_selected_fields = $params->get('show_selected_fields');
		$show_field           = 0;
		$give_back_cnf        = 0;

		if ($show_selected_fields)
		{
			$creatorfield = $params->get('creatorfield');

			if (isset($creatorfield))
			{
				if (in_array('give_back', $creatorfield))
				{
					$give_back_cnf = 1;
				}
			}
		}
		else
		{
			$show_field = 1;
		}

		// Save give back detail only when show give back is yes (When editing campaign)
		if ($show_field == 1 OR $give_back_cnf == 0)
		{
			$givebackSaveSuccess = $this->saveGivebacks($post, $cid);

			if (!$givebackSaveSuccess)
			{
				return false;
			}
		}

		// Save Campaign Updates here
		$updatesSaveSucess = $this->saveUpdates($post, $cid);

		if (!$updatesSaveSucess)
		{
			return false;
		}

		// Upload image
		// For edit , delete all existing images for this campaign and re-add new
		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;

		// Delete only images so set isvideo to it is not a video
		$isvideo = 0;

		// Delete only gallery image
		$gallery = array(1);

		// @params  campaign Id, existing image ids (not to delete), delete images only, delete gallery images only
		$existing_imgs_ids = $post->get('existing_imgs_ids', '', 'Array');
		$campaignHelper->deleteGalleryRecords($cid, $existing_imgs_ids, $isvideo, $gallery);

		// Delete video records
		$isvideo = 1;

		// Delete default & gallery videos
		$gallery = array(0,1);

		// @params  campaign Id, existing image ids (not to delete), delete images only, delete videos
		$campaignHelper->deleteGalleryRecords($cid, $existing_imgs_ids, $isvideo, $gallery);

		// Function added to delete videos - Added by Nidhi
		$video_file_name = $post->get('video_files_name', '', 'Array');
		$video_ids = $post->get('video_ids', '', 'Array');
		$this->_deleteGalleryVideos($cid, $video_ids);

		$file_field = 'camp_image';
		$file_error = $_FILES[$file_field]['error'];

		// Get video embed url
		$video_url      = $post->get('video_url', '', 'STRING');
		$video_provider = $post->get('video_provider', '', 'STRING');

		if (!empty($video_url))
		{
			$jgivemediaHelper = new jgivemediaHelper;
			$result           = $jgivemediaHelper->geturl($video_provider, $video_url);

			if ($result)
			{
				$video_url = $result;
			}
		}

		$jgivemediaHelper = new jgivemediaHelper;
		$img_dimensions   = array();
		$img_dimensions[] = 'small';
		$img_dimensions[] = 'medium';
		$img_dimensions[] = 'large';
		$image_path       = array();

		// If file field is not empty
		if (!$file_error == 4)
		{
			$main_img_id  = $post->get('main_img_id');

			// @params filed_name,image dimensions,resize=0 or not_resize=1(upload original)
			$img_org_name = $jgivemediaHelper->imageupload('camp_image', $img_dimensions, 0);

			// Before deleting image get image path to delete image from folder
			$campaignHelper->deleteCampaignMainImage($cid);

			// Handling error Not Valid Extension

			if (empty($img_org_name))
			{
				if ($img_org_name)
				{
					$uploadSuccess = $campaignHelper->uploadImage($cid, 'camp_image', $main_img_id, 0);
				}
				else
				{
					return false;
				}
			}
			else
			{
				$db = JFactory::getDBO();

				$obj              = new stdClass;
				$obj->id          = $main_img_id;
				$obj->campaign_id = $cid;

				$image_upload_path_for_db = 'images/jGive';
				$image_upload_path_for_db .= DS . $img_org_name;

				$obj->path          = $image_upload_path_for_db;
				$obj->gallery       = 0;
				$obj->order         = '';

				if ($main_img_id)
				{
					if (!$db->updateObject('#__jg_campaigns_images', $obj, 'id'))
					{
						echo $db->stderr();

						return false;
					}
				}
				else
				{
					if (!$db->insertObject('#__jg_campaigns_images', $obj, 'id'))
					{
						echo $db->stderr();

						return false;
					}

					$main_img_id = $db->insertid();
				}
			}
		}

		// @Amol Add new images in gallery
		$file_field  = 'jgive_img_gallery';
		$file_errors = $_FILES[$file_field]['error'];

		foreach ($file_errors as $key => $file_error)
		{
			// If file field is not empty
			if (!$file_error == 4)
			{
				$uploadSuccess = $campaignHelper->uploadImage($cid, 'jgive_img_gallery', 0, $key);
			}

			// File size exceed upload_max_filesize
			if ($file_error == 1)
			{
				$app     = JFactory::getApplication();
				$size    = ini_get("upload_max_filesize");
				$message = JText::sprintf('COM_JGIVE_FILE_SIZE_EXCEEDS', $size);

				$app->enqueueMessage($message, 'error');
				$is_error_flag_set = 1;
			}
			elseif($uploadSuccess === false)
			{
				$is_error_flag_set = 1;
			}
		}

		// Video Gallery
		$video_error_flag_set = JgiveVideos::videoGallery($post, $cid);

		// Save extra fields information
		$data = array();
		$data['content_id'] = $cid;
		$data['client'] = 'com_jgive.campaign';
		$data['fieldsvalue'] = $extra_jform_data;

		$this->saveExtraFields($data);
		$newDetails      = $this->getItem($cid);

		/** Delete
		 Trigger plugins
		  OnAfterJGiveCampaignSave */
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');
		$result = $dispatcher->trigger('OnAfterJGiveCampaignEdit', array($cid,$post,$newDetails, $oldDetails));

		// Trigger after edit campaign
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('content');
		$dispatcher->trigger('onAfterCampaignEdit', array($post));

		// END plugin triggers

		// The error flag is set
		if ($is_error_flag_set)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * Used when creating/editing a campaign
	 *
	 * @param   Object  $post    Form data
	 * @param   INT     $userid  User ID
	 * @param   INT     $cid     Campaign ID
	 *
	 * @return  void
	 */
	public function createCampaignObject($post, $userid, $cid)
	{
		$params = JComponentHelper::getParams('com_jgive');

		$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $helperPath);
			JLoader::load('campaignHelper');
		}

		$campaignHelper = new campaignHelper;

		$obj              = new stdClass;
		$obj->id          = $cid;
		$obj->creator_id  = $userid;

		// @TODO use in next versions
		$obj->category_id = '';
		$obj->title       = $post->get('title', '', 'STRING');
		$alias            = $post->get('alias', '', 'RAW');

		if (empty($alias))
		{
			$alias = $obj->title;
		}

		$alias      = $campaignHelper->formatttedTitle($alias, $obj->id);
		$obj->alias = $campaignHelper->formatttedVanityURL($alias, $alias, $obj->id);

		if ($post->get('type'))
		{
			$obj->type = $post->get('type', '', 'STRING');
		}
		else
		{
			// Get the campaign set by admin to be created & save in db
			$camp_type = $params->get('camp_type');

			if (is_array($camp_type))
			{
				if (isset($camp_type[0]))
				{
					$obj->type = $camp_type[0];
				}
			}
			elseif (!empty($camp_type))
			{
				$obj->type = $camp_type;
			}
			else
			{
				$obj->type = 'donation';
			}
		}

		$obj->category_id  = $post->get('campaign_category', '', 'INT');

		// Org_ind_type since version 1.5.1
		$obj->org_ind_type = $post->get('org_ind_type', '', 'STRING');

		// Getting Meta Data
		$obj->meta_data = $post->get('meta_data', '', 'STRING');
		$obj->meta_desc = $post->get('meta_desc', '', 'STRING');

		if ($post->get('max_donors'))
		{
			$obj->max_donors = (int) $post->get('max_donors', '', 'INT');
		}

		if ($post->get('minimum_amount'))
		{
			$obj->minimum_amount = (int) $post->get('minimum_amount', '', 'FLOAT');
		}

		$obj->short_description = $post->get('short_desc', '', 'STRING');
		$obj->long_description = JRequest::getVar('long_desc', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$obj->goal_amount  = $post->get('goal_amount', '', 'FLOAT');
		$obj->paypal_email = $post->get('paypal_email', '', 'STRING');
		$obj->first_name = $post->get('first_name', '', 'STRING');
		$obj->last_name  = $post->get('last_name', '', 'STRING');

		if ($post->get('address'))
		{
			$obj->address = $post->get('address', '', 'STRING');
		}

		if ($post->get('address2'))
		{
			$obj->address2 = $post->get('address2', '', 'STRING');
		}

		$jgiveFrontendHelper = new jgiveFrontendHelper;

		// For city since version 1.6
		$other_city_check = $post->get('other_city_check', '', 'STRING');

		if (!empty($other_city_check))
		{
			$obj->city       = $post->get('other_city', '', 'STRING');
			$obj->other_city = 1;
		}
		elseif (($post->get('city')) && $post->get('city') != '')
		{
			$obj->city       = $post->get('city');
			$obj->other_city = 0;
		}

		$obj->country = $post->get('country');
		$obj->state = $post->get('state');

		if ($post->get('zip'))
		{
			$obj->zip = $post->get('zip', '', 'STRING');
		}

		if ($post->get('phone'))
		{
			$obj->phone = $post->get('phone', '', 'STRING');
		}

		if ($post->get('group_name'))
		{
			$obj->group_name = $post->get('group_name', '', 'STRING');
		}

		if ($post->get('website_address'))
		{
			$obj->website_address = $post->get('website_address', '', 'STRING');
		}

		// Get formatted start time & end time for campaign.
		$formattedTime = $campaignHelper->getFormattedTime($post);

		// Append formatted start time & end time for event to startdate & enddate.
		$obj->start_date = $post->get('start_date', '', 'STRING') . " " . $formattedTime['campaign_start_time'];
		$obj->end_date   = $post->get('end_date', '', 'STRING') . " " . $formattedTime['campaign_end_time'];

		$campaign_period_in_days = $params->get('campaign_period_in_days');

		if (empty($campaign_period_in_days) || $campaign_period_in_days == 0)
		{
			$obj->end_date   = $post->get('end_date', '', 'STRING') . " " . $formattedTime['campaign_end_time'];
		}
		else
		{
			$days_limit    = $post->get('days_limit', '', 'STRING');
			$obj->end_date = date('Y-m-d H:i:s', strtotime($obj->start_date) + (24 * 3600 * $days_limit));
		}

		$obj->start_date = $this->techjoomlacommon->getDateInUtc($obj->start_date);
		$obj->end_date = $this->techjoomlacommon->getDateInUtc($obj->end_date);

		$obj->published             = $post->get('publish', '', 'INT');
		$obj->allow_exceed          = $post->get('allow_exceed', '', 'INT');
		$obj->allow_view_donations  = $post->get('show_public', '', 'INT');
		$obj->internal_use          = $post->get('internal_use', '', 'STRING');
		$obj->video_on_details_page = $post->get('video_on_details_page', '0', 'INT');

		// Jomsocial groupid
		$js_group = $post->get('js_group', '', 'INT');

		if ($js_group)
		{
			$obj->js_groupid = $post->get('js_group', '', 'INT');
		}

		return $obj;
	}

	/**
	 * Save campaign givebacks
	 *
	 * @param   Object  $post  Form data
	 * @param   INT     $cid   Campaign ID
	 *
	 * @return  void
	 */
	public function saveGivebacks($post, $cid)
	{
		$give_back_value    = $post->get('give_back_value', '', 'Array');
		$give_back_details  = $post->get('give_back_details', '', 'Array');
		$giveback_count     = count($post->get('give_back_value', '', 'Array'));
		$give_back_ids      = $post->get('ids', '', 'Array');
		$give_back_order    = $post->get('give_back_order', '', 'Array');
		$give_back_quantity = $post->get('give_back_quantity', '', 'Array');

		foreach ($give_back_ids as $key => $value)
		{
			$db     = JFactory::getDBO();
			$coupon = new stdClass;

			if ($value)
			{
				$coupon->id = $value;
			}
			else
			{
				$coupon->id = '';
			}

			$coupon->campaign_id = $cid;

			if (!empty($coupon->campaign_id))
			{
				$coupon->amount         = $give_back_value[$key];
				$coupon->description    = $give_back_details[$key];
				$coupon->order          = $give_back_order[$key];
				$coupon->total_quantity = $give_back_quantity[$key];

				if ($value)
				{
					if (!$db->updateObject('#__jg_campaigns_givebacks', $coupon, 'id'))
					{
						return false;
					}
				}
				elseif (!empty($coupon->amount))
				{
					$coupon->quantity = 0;

					if (!$db->insertObject('#__jg_campaigns_givebacks', $coupon, 'id'))
					{
						echo $db->stderr();

						return false;
					}
				}

				if ($value)
				{
					$giveback_id = $value;
				}
				else
				{
					$giveback_id = $db->insertid();
				}

				require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";

				$campaignHelper   = new campaignHelper;
				$couponimgSuccess = $campaignHelper->uploadImageForGiveback($key, $giveback_id, $cid);
			}
		}

		return true;
	}

	/**
	 * Function Save Campaign Updates Details
	 *
	 * @param   Array  $post  Form data
	 * @param   INT    $cid   Campaign ID
	 *
	 * @return  Object
	 */
	public function saveUpdates($post, $cid)
	{
		$updates_title = $post->get('updates_title', '', 'Array');
		$updates_desc  = $post->get('updates_desc', '', 'Array');
		$updates_cdate  = date('Y-m-d H:i:s');
		$updates_mdate  = date('Y-m-d H:i:s');
		$campaign_updates_count = count($post->get('updates_title', '', 'Array'));
		$updatesids = $post->get('updatesids', '', 'Array');

		$i = 0;

		foreach ($updatesids as $key => $value)
		{
			$updated_news = new stdClass;

			if ($value)
			{
				$updated_news->id = $value;
				$updated_news->mdate = $updates_mdate;
			}
			else
			{
				$updated_news->id = '';
				$updated_news->cdate = $updates_cdate;
			}

			$updated_news->campaign_id = $cid;

			if (!empty($updated_news->campaign_id))
			{
				$updated_news->title = $updates_title[$key];
				$updated_news->description = $updates_desc[$key];

				if ($value)
				{
					if (!$this->_db->updateObject('#__jg_updates', $updated_news, 'id'))
					{
						return false;
					}
				}
				elseif (!empty($updated_news->title))
				{
					if (!$this->_db->insertObject('#__jg_updates', $updated_news, 'id'))
					{
						echo $this->_db->stderr();

						return false;
					}
				}
			}
		}

		return true;
	}

	/**
	 * Delete entries from giveback table
	 *
	 * @param   INT  $give_back_ids  Giveback ID
	 * @param   INT  $cid            Campaign ID
	 *
	 * @return  void
	 */
	public function deleteGivebacks($give_back_ids, $cid)
	{
		$db               = JFactory::getDBO();
		$give_back_idsarr = (array) $give_back_ids;
		$query            = "SELECT id FROM #__jg_campaigns_givebacks WHERE campaign_id=" . $cid;
		$db->setQuery($query);
		$type_ids = $db->loadColumn();
		$diff     = array_diff($type_ids, $give_back_idsarr);
		$diffids  = implode("','", $diff);
		$query    = "DELETE FROM #__jg_campaigns_givebacks WHERE id IN ('" . $diffids . "') AND campaign_id=" . $cid;
		$db->setQuery($query);
		$db->execute();
	}

	/**
	 * Delete entries from Updates table
	 *
	 * @param   INT  $updatesids  Updates ID
	 * @param   INT  $cid         Campaign ID
	 *
	 * @return  void
	 */
	public function deleteUpdates($updatesids, $cid)
	{
		// Camapign Updates ids Array
		$updates_idarr = (array) $updatesids;
		$query            = "SELECT id FROM #__jg_updates WHERE campaign_id=" . $cid;
		$this->_db->setQuery($query);

		// Fetch Array of existed Campaign Updates
		$type_ids = $this->_db->loadColumn();

		// Finding difference between Existed Array of updates and edited data records
		$diff     = array_diff($type_ids, $updates_idarr);

		$diffids  = implode("','", $diff);

		$query    = "DELETE FROM #__jg_updates WHERE id IN ('" . $diffids . "') AND campaign_id=" . $cid;
		$this->_db->setQuery($query);
		$this->_db->execute();
	}

	/**
	 * Method to change campaign success state
	 *
	 * @param   INT  $cid            Campaign Id
	 * @param   INT  $successStatus  Campaign selected state
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.7
	 */
	public function changeSuccessState($cid, $successStatus)
	{
		$campaignHelper = new campaignHelper;
		$result = $campaignHelper->updateCampaignSuccessStatus($cid, $successStatus, $orderId = 0);

		if ($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Method to get a Campaign Category.
	 *
	 * @return  mixed  $item  Object on success, false on failure.
	 *
	 * @since	1.7
	 */
	public function getCampaignsCats()
	{
		$lang = JFactory::getLanguage();
		$tag  = $lang->gettag();

		if (JFactory::getApplication()->input->get('id'))
		{
			$details  = $this->getCampaignDetails(JFactory::getApplication()->input->get('id'));
			$camp_cat = $details->category_id;

			$config = array('filter.published' => array(1),'filter.language' => array('*',$tag));

			$categoryoptions = JHtml::_('category.options', 'com_jgive', $config);
			$dropdown        = JHtml::_(
			'select.genericlist', $categoryoptions, 'campaign_category',
			'size="1" class="required"', 'value', 'text', intval($camp_cat)
			);
		}
		else
		{
			$categories = JHtml::_('category.options', 'com_jgive', $config = array('filter.published' => array(1),'filter.language' => array('*', $tag)));

			if (!empty($categories))
			{
				$dropdown = JHtml::_('select.genericlist', $categories, 'campaign_category', 'size="1" class="required"');
			}
			else
			{
				$msg = '<p class="text-warning">' . JText::_('COM_JGIVE_NO_CATEGORY') . '</p>';

				$drop = JHtml::_('select.genericlist', $categories, 'campaign_category', 'size="1" class="required"');
				$dropdown = $drop . $msg;
			}
		}

		return $dropdown;
	}

	/**
	 * Get Campaign details
	 *
	 * @param   integer  $c_id  Campaign id
	 *
	 * @return   INT  Js group
	 *
	 * since 1.7
	 */
	public function getCampaignDetails($c_id)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT * FROM #__jg_campaigns WHERE id='" . $c_id . "'";
		$db->setQuery($query);

		return ($db->loadObject());
	}

	/**
	 * Get Video Ids
	 *
	 * @param   integer  $cid        Campaign id
	 * @param   integer  $video_ids  video id
	 *
	 * @return   INT  Js group
	 *
	 * since 1.7
	 */
	public function _deleteGalleryVideos($cid,$video_ids)
	{
		$new_array = array();

		if (array_filter($video_ids) > 0)
		{
			foreach ($video_ids as $i => $v)
			{
				if ($v != null)
				{
					$new_array[] = $v;
				}
			}
		}

		$videoid = implode(',', $new_array);

		$db = JFactory::getDBO();

		if (!empty($videoid))
		{
			$query = "SELECT path,thumb_path FROM #__jg_campaigns_media WHERE id not in (" . $videoid . ") AND content_id=" . $cid;
			$db->setQuery($query);
			$galleryVideoToDelPath = $db->loadObjectlist();

			// Delete image physically
			if (!empty($galleryVideoToDelPath))
			{
				$this->deleteFile($galleryVideoToDelPath);

				// Delete image from db
				$query = "DELETE FROM #__jg_campaigns_media WHERE id not in (" . $videoid . ") AND content_id=" . $cid;
				$db->setQuery($query);

				if (!$db->execute())
				{
					echo $db->stderr();

					return false;
				}
			}
		}
		else
		{
			$db = JFactory::getDBO();
			$query = "SELECT path,thumb_path FROM #__jg_campaigns_media WHERE content_id=" . $cid;
			$db->setQuery($query);
			$galleryVideoToDelPath = $db->loadObject();

			// Delete image physically
			if (isset($galleryVideoToDelPath))
			{
				$this->deleteFile($galleryVideoToDelPath);

				// Delete image from db
				$query = "DELETE FROM  #__jg_campaigns_media WHERE  content_id=" . $cid;
				$db->setQuery($query);

				if (!$db->execute())
				{
					echo $db->stderr();

					return false;
				}
			}
		}
	}

	/**
	 * Get Video Ids
	 *
	 * @param   array  $galleryVideoToDelPath  Path to thumbnail file
	 *
	 * @return   void
	 *
	 * since 1.7
	 */
	public function deleteFile($galleryVideoToDelPath)
	{
		// If to delete video file then we will need to specify video file location
		$thumbpath = JPATH_SITE . $galleryVideoToDelPath->thumb_path;
		$videopath = JPATH_SITE . $galleryVideoToDelPath->path;

		if (JFile::exists($thumbpath) && JFile::exists($videopath))
		{
			JFile::delete($thumbpath);
			JFile::delete($videopath);
		}
	}

	/**
	 * Get JS usergroup
	 *
	 * @return   INT  Js group
	 *
	 * since 1.7
	 */
	public function getJS_usergroup()
	{
		require_once JPATH_SITE . "/components/com_jgive/helpers/integrations.php";
		$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;

		return $result = $JgiveIntegrationsHelper->getJS_usergroup();
	}

	/**
	 * Method to get the profile form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since  1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_jgive.campaign', 'campaignform', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to Get Campaign Video Id
	 *
	 * @param   String  $path  Videopath
	 * @param   String  $type  type
	 *
	 * @return boolean
	 *
	 * @since  2.0
	 */
	public function getCampaignVideoId($path, $type)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select('id');
		$query->select('type');
		$query->from($db->qn('#__jg_campaigns_media', 'm'));

		if ($type == "path")
		{
			$query->where($db->qn('m.path') . ' = ' . $db->quote($path));
		}
		elseif ($type == "url")
		{
			$query->where($db->qn('m.url') . ' = ' . $db->quote($path));
		}

		$db->setQuery($query);

		$results = $db->loadAssoc();

		return $results;
	}

	/**
	 * Delete video
	 *
	 * @param   integer  $videoid  Video id
	 * @param   string   $type     Type of video
	 *
	 * @return   boolean
	 *
	 * since 1.7
	 */
	public function deleteGalleryVideos($videoid, $type)
	{
		$app = JFactory::getApplication();

		try
		{
			if ($videoid)
			{
				if ($type == "video")
				{
					$query = $this->_db->getQuery(true);

					$query->select(array('path','thumb_path'));
					$query->from('#__jg_campaigns_media');
					$query->where('id  = ' . $videoid);

					$this->_db->setQuery($query);
					$galleryVideoToDelPath = $this->_db->loadObject();

					if ($galleryVideoToDelPath)
					{
						$this->deleteFile($galleryVideoToDelPath);
					}
				}

				// Jgive plugin onCampaignVideoDelete
				JPluginHelper::importPlugin('system');
				$dispatcher = JDispatcher::getInstance();
				$dispatcher->trigger('onAfterCampaignVideoDelete', array($videoid, $type));

				// Create a new query object.
				$query = $this->_db->getQuery(true);

				$query->delete($this->_db->quoteName('#__jg_campaigns_media'));
				$query->where($this->_db->quoteName('id') . '=' . $this->_db->quote($videoid));

				$this->_db->setQuery($query);
				$result = $this->_db->execute();
			}

			return true;
		}
		catch (Exception $e)
		{
			$app->enqueueMessage($e->getMessage(), 'error');
			$this->setError($e->getMessage());

			return false;
		}
	}
}
