<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');
jimport('techjoomla.tjmail.mail');

/**
 * Class for JGive Email Template Model
 *
 * @package  JGive
 *
 * @since    1.8.5
 */
class JgiveModelEmail_Template extends JModelLegacy
{
	/**
	 * Method to store email template
	 *
	 * @return  void
	 *
	 * @since   1.8.5
	 */
	public function store()
	{
		$app      = JFactory::getApplication();
		$post  = JFactory::getApplication()->input->post;

		// Get Email Subject here
		$email_subject      = $post->get('jgive_subject', '', 'STRING');

		// Get Donation Receipt content
		$message_body   = JRequest::getVar('data', '', 'post', 'array', JREQUEST_ALLOWHTML);

		$file     = JPATH_ADMINISTRATOR . "/components/com_jgive/template/email_template.php";
		$msg      = '';
		$msg_type = '';

		if ($message_body)
		{
			$template_css = $message_body['template_css'];
			unset($message_body['template_css']);

			$file_contents = "<?php \n\n";

			// Add email_subject value into array
			$file_contents .= "\$emails_config=array('email_subject' => '" . $email_subject . "', \n" . $this->row2text($message_body) . "\n);\n";
			$file_contents .= "\n?>";

			if (JFile::write($file, $file_contents))
			{
				$msg = JText::_('COM_JGIVE_CONFIG_SAVED');
			}
			else
			{
				$msg      = JText::_('COM_JGIVE_CONFIG_SAVE_PROBLEM');
				$msg_type = 'error';
			}

			$cssfile = JPATH_SITE . "/media/com_jgive/css/email_template.css";
			JFile::write($cssfile, $template_css);
		}

		$app->redirect('index.php?option=com_jgive&view=email_template&layout=email_template', $msg, $msg_type);
	}

	/**
	 * Method to get data
	 *
	 * @param   array  $row    row for template
	 * @param   array  $dvars  dvars
	 *
	 * @return  void
	 *
	 * @since   1.8.5
	 */
	public function row2text($row, $dvars = array())
	{
		reset($dvars);

		while (list($idx, $var) = each($dvars))
		{
			unset($row[$var]);
		}

		$text = '';
		reset($row);
		$flag = 0;
		$i    = 0;

		while (list($var, $val) = each($row))
		{
			if ($flag == 1)
			{
				$text .= ",\n";
			}
			elseif ($flag == 2)
			{
				$text .= ",\n";
			}

			$flag = 1;

			if (is_numeric($var))
			{
				if ($var{0} == '0')
				{
					$text .= "'$var'=>";
				}
				else
				{
					if ($var !== $i)
					{
						$text .= "$var=>";
					}

					$i = $var;
				}
			}
			else
			{
				$text .= "'$var'=>";
			}

			$i++;

			if (is_array($val))
			{
				$text .= "array(" . $this->row2text($val, $dvars) . ")";
				$flag = 2;
			}
			else
			{
				$text .= "\"" . addslashes($val) . "\"";
			}
		}

		return $text;
	}

	/**
	 * Method For Making Donation Receipt after donation done successfully
	 *
	 * @param   INT  $donation_id  Array of data that contain donation related information
	 *
	 * @return boolean
	 */
	public function generateDonationReceipt($donation_id)
	{
		// Get site name
		$config = JFactory::getConfig();
		$site_name = $config->get('sitename');

		// File which contain receipt html
		require_once JPATH_ADMINISTRATOR . '/components/com_jgive/template/email_template.php';

		// Fetching Receipt Html here
		$htmldata = $emails_config['message_body'];
		$html_subject_data = $emails_config['email_subject'];

		if (!class_exists('JgiveFrontendHelper'))
		{
			JLoader::register('JgiveFrontendHelper', JPATH_SITE . '/components/com_jgive/helper.php');
			JLoader::load('JgiveFrontendHelper');
		}

		$jgivefrontendhelper = new JgiveFrontendHelper;

		// Query for fetching order id from orders table using Donation id
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from($db->qn('#__jg_orders'));
		$query->where($db->qn('#__jg_orders.donation_id') . ' = ' . $db->quote($donation_id));
		$db->setQuery($query);
		$order_id = $db->loadResult();

		if (!class_exists('DonationsHelper'))
		{
			JLoader::register('DonationsHelper', JPATH_SITE . '/components/com_jgive/helpers/donations.php');
			JLoader::load('DonationsHelper');
		}

		$donationhepler = new DonationsHelper;

		// Pass order_id to getSingleDonationInfo for getting all requird data for donation receipt
		$donor_data = $donationhepler->getSingleDonationInfo($order_id);

		// Getting email_id for sentmail()
		$email = $donor_data['donor']->email;

		// Added currency to amount
		$donor_data['campaign']->donation_amount = $jgivefrontendhelper->getFormattedPrice($donor_data['campaign']->donation_amount);

		// Convert donation date into Y-m-d format
		$donor_data['payment']->cdate = JFactory::getDate($donor_data['payment']->cdate)->Format(
		JText::_('COM_JGIVE_EMAIL_TEMPLATE_DATE_FORMAT_SHOW_SHORT')
		);

		$donor_data['site']->site_name = $site_name;

		// Pass html & data for replacing tag
		$body = TjMail::TagReplace($htmldata, $donor_data);

		// Send Donation Receipt data to sendmail function
		$result = $donationhepler->sendmail($email, $html_subject_data, $body);

		if ($result == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
