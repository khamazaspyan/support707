<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die;

require_once JPATH_ADMINISTRATOR . '/components/com_installer/models/database.php';

/**
 * Jlike Manage Model
 *
 * @since  1.6
 */
class JgiveModelMigration extends InstallerModelDatabase
{
	/**
	 * Gets the changeset object.
	 *
	 * @return  JSchemaChangeset
	 */
	public function getItems()
	{
		$folder = JPATH_ADMINISTRATOR . '/components/com_tjlms/sql/updates/';

		try
		{
			$changeSet = JSchemaChangeset::getInstance($this->getDbo(), $folder);
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage(), 'warning');

			return false;
		}

		return $changeSet;
	}

	/**
	 * Method to add avtivity for old campaigns prior to v 2.0
	 *
	 * @return  boolean
	 *
	 * @since   1.0
	 */
	public function createActivity()
	{
		require_once JPATH_SITE . '/plugins/system/jgiveactivities/helper.php';
		JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_jgive/models');
		JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_activitystream/models');
		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');

		$plgSystemJgiveActivitiesHelper = new PlgSystemJgiveActivitiesHelper;

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query->select('id');
		$query->from($db->quoteName('#__jg_campaigns'));
		$db->setQuery($query);
		$campaignIds = $db->loadColumn();

		$query = $db->getQuery(true);
		$query->select('DISTINCT target_id');
		$query->from($db->quoteName('#__tj_activities'));
		$db->setQuery($query);
		$targetIds = $db->loadColumn();

		foreach ($campaignIds as $campaignId)
		{
			// Add campaign create activity
			if (!in_array($campaignId, $targetIds))
			{
				$jgiveModelCampaign = JModelLegacy::getInstance('Campaign', 'JgiveModel');

				$campaignDetail = $jgiveModelCampaign->getItem($campaignId);

				$user = JFactory::getUser($campaignDetail->creator_id);

				$activityData = array();
				$activityData['id'] = '';
				$activityData['created_date'] = $campaignDetail->created;
				$actorData = $this->getActorData($user);
				$activityData['actor'] = json_encode($actorData);
				$activityData['actor_id'] = $user->get('id');

				$objectData = array();
				$objectData['type'] = 'campaign';
				$objectData['name'] = $campaignDetail->get('title', '', 'STRING');

				$cid = $campaignDetail->id;

				$objectData['id'] = $cid;
				$objectData['url'] = JRoute::_(JUri::root() . '/index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cid);
				$activityData['object'] = json_encode($objectData);
				$activityData['object_id'] = $cid;

				$targetData = array();
				$targetData['type'] = 'campaign';
				$targetData['name'] = $campaignDetail->get('title', '', 'STRING');
				$targetData['id'] = $cid;
				$targetData['url'] = JRoute::_(JUri::root() . '/index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cid);
				$activityData['target'] = json_encode($targetData);
				$activityData['target_id'] = $cid;

				$activityData['type'] = 'jgive.addcampaign';
				$activityData['template'] = 'addcampaign.mustache';

				$result = $activityStreamModelActivity->save($activityData);
			}
		}

		// Push donation activity only is there are no activities
		if (empty($targetIds))
		{
			$this->pushDonationActivity();
		}

		$campaignUpdateData = $this->migrateUpdatesToActivity();

		return true;
	}

	/**
	 * Method to get actor data
	 *
	 * @param   OBJECT  $user  user object
	 *
	 * @return  null
	 *
	 * @since   1.0
	 */
	public function getActorData($user)
	{
		$userData = array();
		$userData['type'] = 'person';
		$userData['id'] = $user->get('id');
		$userData['name'] = $user->get('name');
		$params = JComponentHelper::getParams('com_jgive');
		$integration = $params->get('integration');

		switch (strtolower($integration))
		{
			case 'jomsocial':
				jimport('techjoomla.jsocial.jomsocial');
				$sociallibraryclass = new JSocialJomsocial;
				break;
			case 'easysocial':
				jimport('techjoomla.jsocial.easysocial');
				$sociallibraryclass = new JSocialEasysocial;
				break;
			case 'joomla':
				jimport('techjoomla.jsocial.joomla');
				$sociallibraryclass = new JSocialJoomla;
				break;
			case 'cb':
				jimport('techjoomla.jsocial.cb');
				$sociallibraryclass = new JSocialCB;
				break;
			case 'jomwall':
				jimport('techjoomla.jsocial.jomwall');
				$sociallibraryclass = new JSocialJomwall;
				break;
			default:
				jimport('techjoomla.jsocial.joomla');
				$sociallibraryclass = new JSocialJoomla;
				break;
		}

		/*Added By Deepa*/
		$helperPath = JPATH_SITE . '/components/com_jgive/helpers/integrations.php';

		if (!class_exists('JgiveIntegrationsHelper'))
		{
			JLoader::register('JgiveIntegrationsHelper', $helperPath);
			JLoader::load('JgiveIntegrationsHelper');
		}

		$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
		$user->avatar      = $JgiveIntegrationsHelper->getUserAvatar($user->id);
		/*End here*/

		$userData['url'] = $sociallibraryclass->getProfileUrl($user);
		$imageData = array();
		$imageData['type'] = "link";

		/* Commented by Deepa*/
		if (!$user->avatar)
		{
			// If no avatar, use default avatar
			$user->avatar = JUri::root(true) . '/media/com_jgive/images/default_avatar.png';
		}

		$imageData['avatar'] = $user->avatar;
		$userData['image'] = json_encode($imageData);

		return $userData;
	}

	/**
	 * Method to add avtivity for campaign donation
	 *
	 * @return  boolean
	 *
	 * @since   2.0
	 */
	public function pushDonationActivity()
	{
		JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_activitystream/models');
		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');

		// Actitivty for donations - start
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from($db->quoteName('#__jg_orders'));
		$query->where($db->quoteName('status') . "=" . "'C'");
		$query->order('order_id ASC');
		$db->setQuery($query);

		$completedDonations = $db->loadColumn();

		$path = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$donationsHelper  = new donationsHelper;

		$path = JPATH_SITE . '/components/com_jgive/helper.php';

		if (!class_exists('JgiveFrontendHelper'))
		{
			JLoader::register('JgiveFrontendHelper', $path);
			JLoader::load('JgiveFrontendHelper');
		}

		$jGiveFrontendHelper = new JgiveFrontendHelper;

		$completedCampaign = array();

		foreach ($completedDonations as $completedDonation)
		{
			$donationDetails = $donationsHelper->getSingleDonationInfo($completedDonation);

			$user = JFactory::getUser($donationDetails['donor']->user_id);
			$activityData = array();
			$activityData['id'] = '';

			$activityData['created_date'] = $donationDetails['payment']->cdate;

			if ($donationDetails['payment']->annonymous_donation == 1)
			{
				// For annonymous donation
				$imageData = array();
				$imageData['type'] = "link";
				$imageData['avatar'] = JUri::root(true) . '/media/com_jgive/images/default_avatar.png';
				$activityData['actor'] = json_encode(array('image' => json_encode($imageData)));
				$activityData['actor_id'] = $user->get('id');
			}
			else
			{
				$actorData = $this->getActorData($user);
				$activityData['actor'] = json_encode($actorData);
				$activityData['actor_id'] = $user->get('id');
			}

			$objectData = array();
			$objectData['type'] = 'donation';
			$objectData['amount'] = str_replace("&nbsp;", "", strip_tags($jGiveFrontendHelper->getFormattedPrice($donationDetails['payment']->amount)));
			$activityData['object'] = json_encode($objectData);
			$activityData['object_id'] = 'donation';

			// Get campaign-target data
			$targetData = array();
			$targetData['id'] = $donationDetails['campaign']->id;
			$targetData['type'] = 'campaign';
			$targetData['url'] = JUri::root() . '/index.php?option=com_jgive&view=campaign&layout=single&cid=' . $donationDetails['campaign']->id;
			$targetData['name'] = $donationDetails['campaign']->title;
			$activityData['target'] = json_encode($targetData);
			$activityData['target_id'] = $donationDetails['campaign']->id;

			$activityData['type'] = 'jgive.donation';

			// For annonymous donation
			if ($donationDetails['payment']->annonymous_donation == 1)
			{
				$activityData['template'] = 'anonymousdonation.mustache';
			}
			else
			{
				$activityData['template'] = 'donation.mustache';
			}

			// For campaign complete activity
			if (!in_array($donationDetails['campaign']->id, $completedCampaign))
			{
				if ($donationDetails['campaign']->remaining_amount <= 0)
				{
					$completedCampaign[] = $donationDetails['campaign']->id;
					$this->pushCampaignCompletedActivity($donationDetails);
				}
			}

			$activityStreamModelActivity->save($activityData);
		}

		// Actitivty for donations - end
	}

	/**
	 * Method to add avtivity for campaign complition
	 *
	 * @param   ARRAY  $donationDetails  donation details
	 *
	 * @return  boolean
	 *
	 * @since   2.0
	 */
	public function pushCampaignCompletedActivity($donationDetails)
	{
		JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_activitystream/models');
		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');
		$user = JFactory::getUser($donationDetails['donor']->user_id);
		$data = array();

		$data['id'] = '';
		$actorData = $this->getActorData($user);
		$data['actor'] = json_encode($actorData);
		$data['actor_id'] = $user->get('id');

		$data['object_id'] = $donationDetails['campaign']->id;
		$objectData = array();
		$objectData['id'] = $donationDetails['campaign']->id;
		$objectData['name'] = $donationDetails['campaign']->title;
		$objectData['url'] = JUri::root() . '/index.php?option=com_jgive&view=campaign&layout=single&cid=' . $donationDetails['campaign']->id;
		$objectData['type'] = 'campaign';
		$data['object'] = json_encode($objectData);
		$data['type'] = 'campaign.completed';
		$data['template'] = 'completed.mustache';

		$targetData = array();
		$targetData['type'] = 'campaign';
		$targetData['name'] = $donationDetails['campaign']->title;
		$targetData['id'] = $donationDetails['campaign']->id;
		$targetData['url'] = JRoute::_(JUri::root() . '/index.php?option=com_jgive&view=campaign&layout=single&cid=' . $donationDetails['campaign']->id);
		$data['target'] = json_encode($targetData);
		$data['target_id'] = $donationDetails['campaign']->id;

		$activityStreamModelActivity = JModelLegacy::getInstance('Activity', 'ActivityStreamModel');

		$result = $activityStreamModelActivity->save($data);
	}

	/**
	 * Method to migrate Campaigns Updates into Activity
	 *
	 * @return  array
	 *
	 * @since   2.0
	 */
	public function migrateUpdatesToActivity()
	{
		// Get table prefix
		$config   = JFactory::getConfig();
		$dbprefix = $config->get('dbprefix');

		// Checking table is exist or not
		$query = $this->_db->getQuery(true);
		$query = "SHOW TABLES LIKE '" . $dbprefix . "jg_updates'";
		$this->_db->setQuery($query);
		$fields = $this->_db->loadResult();

		// Checking if table is exist then record is exist or not
		if ($fields)
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from($db->qn('#__jg_updates'));
			$db->setQuery($query);
			$campaignUpdatesData = $db->loadAssocList();

			if (!empty($campaignUpdatesData))
			{
				foreach ($campaignUpdatesData as $campUpdatedata)
				{
					$updateActivityData['postData'] = $campUpdatedata['title'] . ' ' . $campUpdatedata['description'];
					$updateActivityData['cid'] = $campUpdatedata['campaign_id'];
					$updateActivityData['cdate'] = $campUpdatedata['cdate'];
					$updateActivityData['mdate'] = $campUpdatedata['mdate'];

					// Trigger jgiveactivity plugin to add test activity
					$dispatcher = JDispatcher::getInstance();
					JPluginHelper::importPlugin('system');

					$result = $dispatcher->trigger('postActivity', array($updateActivityData));
				}
			}

			$query = $db->getQuery(true);
			$query->delete($db->qn('#__jg_updates'));
			$db->setQuery($query);
			$db->execute();

			/* This is required added by Deepa
			$query = $db->getQuery(true);
			$query = "DROP TABLE " . $dbprefix . "jg_updates";
			$db->setQuery($query);*/
		}
	}
}
