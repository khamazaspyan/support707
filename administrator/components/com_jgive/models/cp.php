<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('FFF');
jimport('joomla.application.component.model');
jimport('joomla.filesystem.folder');

/**
 * Cp form model class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveModelCp extends JModelLegacy
{
	/**
	 * Constructor.
	 *
	 * @see     JController
	 * @since   1.8
	 */
	public function __construct()
	{
		// Get download id
		$params           = JComponentHelper::getParams('com_jgive');
		$this->downloadid = $params->get('downloadid');

		// Setup vars
		$this->updateStreamName = 'JGive';
		$this->updateStreamType = 'collection';
		$this->updateStreamUrl  = "https://techjoomla.com/updates/packages/all?dummy=jgive.xml";
		$this->extensionElement = 'pkg_jgive';
		$this->extensionType    = 'package';

		parent::__construct();
	}

	/**
	 * Method for getExtensionId
	 *
	 * @return  record data
	 *
	 * @since   1.8
	 */
	public function getExtensionId()
	{
		$db = $this->getDbo();

		// Get current extension ID
		$query = $db->getQuery(true)
			->select($db->qn('extension_id'))
			->from($db->qn('#__extensions'))
			->where($db->qn('type') . ' = ' . $db->q($this->extensionType))
			->where($db->qn('element') . ' = ' . $db->q($this->extensionElement));
		$db->setQuery($query);

		$extension_id = $db->loadResult();

		if (empty($extension_id))
		{
			return 0;
		}
		else
		{
			return $extension_id;
		}
	}

	/**
	 * Method for refreshUpdateSite
	 *
	 * @return  updated version of jgive
	 *
	 * @since   1.8
	 */
	public function refreshUpdateSite()
	{
		// Extra query for Joomla 3.0 onwards
		$extra_query = null;

		if (preg_match('/^([0-9]{1,}:)?[0-9a-f]{32}$/i', $this->downloadid))
		{
			$extra_query = 'dlid=' . $this->downloadid;
		}

		// Setup update site array for storing in database
		$update_site = array(
			'name' => $this->updateStreamName,
			'type' => $this->updateStreamType,
			'location' => $this->updateStreamUrl,
			'enabled'  => 1,
			'last_check_timestamp' => 0,
			'extra_query'          => $extra_query
		);

		// For joomla versions < 3.0
		if (version_compare(JVERSION, '3.1', 'lt'))
		{
			unset($update_site['extra_query']);
		}

		$db = $this->getDbo();

		// Get current extension ID
		$extension_id = $this->getExtensionId();

		if (!$extension_id)
		{
			return;
		}

		// Get the update sites for current extension
		$query = $db->getQuery(true)
			->select($db->qn('update_site_id'))
			->from($db->qn('#__update_sites_extensions'))
			->where($db->qn('extension_id') . ' = ' . $db->q($extension_id));
		$db->setQuery($query);

		$updateSiteIDs = $db->loadColumn(0);

		if (!count($updateSiteIDs))
		{
			// No update sites defined. Create a new one.
			$newSite = (object) $update_site;
			$db->insertObject('#__update_sites', $newSite);

			$id = $db->insertid();

			$updateSiteExtension = (object) array(
				'update_site_id' => $id,
				'extension_id'   => $extension_id,
			);

			$db->insertObject('#__update_sites_extensions', $updateSiteExtension);
		}
		else
		{
			// Loop through all update sites
			foreach ($updateSiteIDs as $id)
			{
				$query = $db->getQuery(true)
					->select('*')
					->from($db->qn('#__update_sites'))
					->where($db->qn('update_site_id') . ' = ' . $db->q($id));
				$db->setQuery($query);
				$aSite = $db->loadObject();

				// Does the name and location match?
				if (($aSite->name == $update_site['name']) && ($aSite->location == $update_site['location']))
				{
					// Do we have the extra_query property (J 3.2+) and does it match?
					if (property_exists($aSite, 'extra_query'))
					{
						if ($aSite->extra_query == $update_site['extra_query'])
						{
							continue;
						}
					}
					else
					{
						// Joomla! 3.1 or earlier. Updates may or may not work.
						continue;
					}
				}

				$update_site['update_site_id'] = $id;
				$newSite = (object) $update_site;
				$db->updateObject('#__update_sites', $newSite, 'update_site_id', true);
			}
		}
	}

	/**
	 * Method for get Latest version
	 *
	 * @return  updated version of jgive
	 *
	 * @since   1.8
	 */
	public function getLatestVersion()
	{
		// Get current extension ID
		$extension_id = $this->getExtensionId();

		if (!$extension_id)
		{
			return 0;
		}

		$db = $this->getDbo();

		// Get current extension ID
		$query = $db->getQuery(true)
			->select($db->qn(array('version', 'infourl')))
			->from($db->qn('#__updates'))
			->where($db->qn('extension_id') . ' = ' . $db->q($extension_id));

		$db->setQuery($query);

		$latestVersion = $db->loadObject();

		if (empty($latestVersion))
		{
			return 0;
		}
		else
		{
			return $latestVersion;
		}
	}

	/**
	 * Method to get a array record.
	 *
	 * @return  array  $result.
	 *
	 * @since	1.8
	 */
	public function getDashboardData()
	{
		$db = JFactory::getDbo();

		// Create a new query object.
		$query = $db->getQuery(true);
		$query->select('COUNT(*) as total_campaigns');
		$query->select('SUM(goal_amount) as total_goal_amount');
		$query->from($db->qn('#__jg_campaigns'));

		$db->setQuery($query);
		$campaignInfo = $db->loadAssoc();

		$query = $db->getQuery(true);
		$query->select('SUM(amount) as total_funded_amount');
		$query->select('SUM(fee) as commision_amount');
		$query->from($db->qn('#__jg_orders'));
		$query->where($db->qn('#__jg_orders.status') . ' = ' . $db->quote('C'));

		$db->setQuery($query);
		$orderInfo = $db->loadAssoc();

		$result = array();
		$result['campaignInfo'] = $campaignInfo;
		$result['orderInfo'] = $orderInfo;

		return $result;
	}

	/**
	 * Function to get a array All months.
	 *
	 * @return  array  $months.
	 *
	 * @since	1.8
	 */
	public function getAllMonths()
	{
		$date2      = date('Y-m-d');

		// Get one year back date
		$date1 = date('Y-m-d', strtotime(date("Y-m-d", time()) . " - 365 day"));

		// Convert dates to UNIX timestamp
		$time1 = strtotime($date1);
		$time2 = strtotime($date2);
		$tmp = date('mY', $time2);
		$year = date('Y', $time1);

		while ($time1 < $time2)
		{
			$month31 = array(1,3,5,7,8,10,12);
			$month30 = array(4,6,9,11);

			$month = date('m', $time1);

			if (array_search($month, $month31))
			{
				$time1 = strtotime(date('Y-m-d', $time1) . ' +31 days');
			}
			elseif (array_search($month, $month30))
			{
				$time1 = strtotime(date('Y-m-d', $time1) . ' +30 days');
			}
			else
			{
				if (((0 == $year % 4) && (0 != $year % 100)) || (0 == $year % 400))
				{
					$time1 = strtotime(date('Y-m-d', $time1) . ' +29 days');
				}
				else
				{
					$time1 = strtotime(date('Y-m-d', $time1) . ' +28 days');
				}
			}

			if (date('mY', $time1) != $tmp && ($time1 < $time2))
			{
				$months[] = array(
					"month" => date('F', $time1),
					"year" => date('Y', $time1)
				);
			}
		}

		$months[] = array("month" => date('F', $time2),"year" => date('Y', $time2));

		return $months;
	}

	/**
	 * Function for to  get a All months donation.
	 *
	 * @return  Object List $donation.
	 *
	 * @since	1.8
	 */
	public function getMonthDonation()
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$curdate    = date('Y-m-d');
		$back_year  = date('Y') - 1;
		$back_month = date('m') + 1;
		$backdate   = $back_year . '-' . $back_month . '-' . '01';

		$query->select('FORMAT( SUM(amount) , 2) AS amount');
		$query->select('MONTH(cdate) AS MONTHSNAME');
		$query->select('YEAR(cdate) AS YEARNAME');
		$query->from($db->qn('#__jg_orders', 'o'));

		$query->where(
						'DATE(' . $db->qn('o.cdate') . ')' . ' Between ' . $db->quote($backdate) . ' AND ' .
						$db->quote($curdate) . ' AND ' . $db->qn('o.status') . ' = ' . $db->quote('C')
						);

		$query->group($db->quote('YEARNAME'));
		$query->group('MONTHSNAME');

		$query->order($db->quote('YEAR( o.cdate )') . 'ASC');
		$query->order($db->quote('MONTH( o.cdate )') . 'ASC');

		$db->setQuery($query);
		$donation = $db->loadObjectList();

		return $donation;
	}

	/**
	 * Function for Recent periodic donation
	 *
	 * @return  Object List of Recent periodic donation
	 *
	 * @since   1.8
	 */
	public function getRecentDonationDetails()
	{
		$db = $this->getDbo();

		$query = $db->getQuery(true)
			->select($db->quoteName(array('a.id', 'a.order_id', 'a.amount', 'b.title')))
			->from($db->quoteName('#__jg_orders', 'a'))
			->join('INNER', $db->quoteName('#__jg_campaigns', 'b') . ' ON (' . $db->quoteName('a.campaign_id') . ' = ' . $db->   quoteName('b.id') . ')')
			->where($db->quoteName('a.status') . ' = ' . "'C'")
			->order($db->quoteName('a.mdate') . ' DESC');

		$query->setLimit(2);

		$db->setQuery($query);

		$recentDonations = $db->loadObjectList();

		return $recentDonations;
	}

	/**
	 * Function for Periodic donation count
	 *
	 * @return  record of periodic donation count
	 *
	 * @since   1.8
	 */
	public function getPeriodicDonationsCount()
	{
		$db      = JFactory::getDBO();
		$session = JFactory::getSession();

		$query = $db->getQuery(true);

		$jgive_graph_from_date = $session->get('jgive_graph_from_date');
		$jgive_socialads_end_date  = $session->get('jgive_socialads_end_date');
		$where               = '';
		$groupby             = '';

		if ($jgive_graph_from_date)
		{
			$where = ' AND DATE(' . $db->qn('#__jg_orders.mdate') . ') BETWEEN DATE(' .
			$db->quote($jgive_graph_from_date) . ' ) AND DATE (' . $db->quote($jgive_socialads_end_date) . ')';
		}
		else
		{
			$jgive_graph_from_date = date('Y-m-d');
			$backdate            = date('Y-m-d', strtotime(date('Y-m-d') . ' - 30 days'));

			$where = ' AND DATE(' . $db->qn('#__jg_orders.mdate') . ') BETWEEN DATE(' .
			$db->quote($backdate) . ' ) AND DATE (' . $db->quote($jgive_graph_from_date) . ')';

			$groupby             = "";
		}

		$query->select('FORMAT(SUM(amount),2)');
		$query->from('#__jg_orders');
		$query->where($db->qn('#__jg_orders.status') . ' = ' . $db->quote('C') . $where);

		$this->_db->setQuery($query);
		$result = $this->_db->loadResult();

		return $result;
	}

	/**
	 * Function for pie chart
	 *
	 * @return  array  Get data for pie chart
	 *
	 * @since   1.8
	 */
	public function statsforpie()
	{
		$db                  = JFactory::getDBO();
		$session             = JFactory::getSession();

		$query = $db->getQuery(true);

		$jgive_graph_from_date = $session->get('jgive_graph_from_date');
		$jgive_socialads_end_date  = $session->get('jgive_socialads_end_date');
		$where               = '';

		if ($jgive_graph_from_date)
		{
			// For graph
			$where .= " AND DATE(mdate) BETWEEN DATE('" . $jgive_graph_from_date . "') AND DATE('" . $jgive_socialads_end_date . "')";
		}
		else
		{
			$day         = date('d');
			$month       = date('m');
			$year        = date('Y');
			$statsforpie = array();

			$backdate = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' - 30 days'));
			$groupby  = "";
		}

		// Pending donations
		$query = " SELECT COUNT(id) AS donations FROM #__jg_orders WHERE status= 'P'" . $where;
		$db->setQuery($query);
		$statsforpie[] = $db->loadObjectList();

		// Confirmed donations
		$query = " SELECT COUNT(id) AS donations FROM #__jg_orders WHERE status= 'C'" . $where;
		$db->setQuery($query);
		$statsforpie[] = $db->loadObjectList();

		// Canceled donations
		$query = " SELECT COUNT(id) AS donations FROM #__jg_orders WHERE status= 'E'" . $where;
		$db->setQuery($query);
		$statsforpie[] = $db->loadObjectList();

		// Denied donations
		$query = " SELECT COUNT(id) AS donations FROM #__jg_orders WHERE status= 'D'" . $where;
		$db->setQuery($query);
		$statsforpie[] = $db->loadObjectList();

		// Refunded donations
		$query = " SELECT COUNT(id) AS donations FROM #__jg_orders WHERE status= 'RF' " . $where;
		$db->setQuery($query);
		$statsforpie[] = $db->loadObjectList();

		return $statsforpie;
	}

	/**
	 * Returns periodic income based on session data
	 *
	 * @return  INT  periodic income based on session data
	 *
	 * @since   1.8
	 */
	public function getPendingPayouts()
	{
		if (!class_exists('jgiveModelReports'))
		{
			JLoader::register('jgiveModelReports', JPATH_ADMINISTRATOR . '/components/com_jgive/models/reports.php');
			JLoader::load('jgiveModelReports');
		}

		$jgiveModelReports = new jgiveModelReports;

		return $jgiveModelReports->getPayoutData();
	}
}
