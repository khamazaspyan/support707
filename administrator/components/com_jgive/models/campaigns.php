<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined("_JEXEC") or die();
jimport("joomla.application.components.modellist");

/**
 * Methods supporting a list of cities records.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.7
 */
class JgiveModelCampaigns extends JModelList
{
	protected $campaignHelper;

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.7
	 * @see     JController
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'published', 'a.published',
				'title', 'a.title',
				'start_date', 'a.start_date',
				'end_date', 'a.end_date',
				'goal_amount', 'a.goal_amount',
				'featured', 'a.featured',
				'success_status', 'a.success_status',
				'first_name', 'a.first_name',
				'last_name', 'a.last_name'
			);
		}

		$this->campaignHelper = new campaignHelper;

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.7
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Set ordering.
		$orderCol = $app->getUserStateFromRequest($this->context . '.filter_order', 'filter_order');

		if (!in_array($orderCol, $this->filter_fields))
		{
			$orderCol = 'a.id';
		}

		$this->setState('list.ordering', $orderCol);

		// Load the filter search
		$search = $app->getUserStateFromRequest($this->context . 'filter.filter_search', 'filter_search');
		$this->setState('filter.filter_search', $search);

		// Filter category.
		$category = $app->getUserStateFromRequest($this->context . '.filter.campaign_category', 'campaign_category', '', 'string');
		$this->setState('filter.campaign_category', $category);

		// Load the filter state
		$published = $app->getUserStateFromRequest($this->context . 'filter.publish_states', 'publish_states', '', 'string');
		$this->setState('filter.publish_states', $published);

		// Load the filter campaign type
		$campaign_type = $app->getUserStateFromRequest($this->context . 'filter.campaign_type', 'campaign_type', '', 'string');
		$this->setState('filter.campaign_type', $campaign_type);

		// Load the filter org type
		$campaign_type = $app->getUserStateFromRequest($this->context . 'filter.org_ind_type', 'org_ind_type', '', 'string');
		$this->setState('filter.org_ind_type', $campaign_type);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_jgive');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.title', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.7
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.filter_search');
		$id .= ':' . $this->getState('filter.published');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 *
	 * @since   1.7
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		// Select the required fields from the table.
		$query->select($this->getState('list.select', 'a.*'));
		$query->from('`#__jg_campaigns` AS a');

		// Join over the categories.
		$query->select('c.title AS category_title')
			->join('LEFT', '#__categories AS c ON c.id = a.category_id');

		// Join over the users
		$query->select('u.name AS campaign_creator')
			->join('LEFT', '#__users AS u ON a.creator_id = u.id');

		// Filter by published state.
		$published = $this->getState('filter.publish_states');

		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published IN (0, 1))');
		}

		// Filter by published state.
		$published = $this->getState('filter.publish_states');

		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published IN (0, 1))');
		}
		// Filter by search in title
		$search = $this->getState('filter.filter_search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.title LIKE ' . $search .
						' OR a.first_name LIKE ' . $search .
						' OR a.last_name LIKE ' . $search . ' )');
			}
		}

		// Filter by search in title
		$search = $this->getState('filter.filter_search');

		// Filter by category
		$campaign_category = $this->getState('filter.campaign_category');

		if (!empty($campaign_category))
		{
			if (is_numeric($campaign_category))
			{
				$cat_tbl = JTable::getInstance('Category', 'JTable');
				$cat_tbl->load($campaign_category);
				$rgt = $cat_tbl->rgt;
				$lft = $cat_tbl->lft;
				$baselevel = (int) $cat_tbl->level;
				$query->where('c.lft >= ' . (int) $lft);
				$query->where('c.rgt <= ' . (int) $rgt);
			}
		}

		// Filter campaign type
		$campaign_type = $this->getState('filter.campaign_type');

		if (!empty($campaign_type))
		{
			$query->where('a.type = ' . $db->quote($db->escape($campaign_type)));
		}

		// Filter org ind type
		$org_ind_type = $this->getState('filter.org_ind_type');

		if (!empty($org_ind_type))
		{
			$query->where('a.org_ind_type = ' . $db->quote($db->escape($org_ind_type)));
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Method to get a list of campaigns.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.7
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $item)
		{
			// Get campaign amounts
			$amounts = $this->campaignHelper->getCampaignAmounts($item->id);
			$item->amount_received = $amounts['amount_received'];
			$item->remaining_amount = $amounts['remaining_amount'];
			$item->donor_count = $this->campaignHelper->getCampaignDonorsCount($item->id);
		}

		return $items;
	}
}
