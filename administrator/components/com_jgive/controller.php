<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;

/**
 * JgiveControllerCampaign form controller class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JGiveController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   array    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return   JController  This object to support chaining.
	 *
	 * @since  1.7
	 */
	public function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT . '/helpers/jgive.php';

		$view = JFactory::getApplication()->input->getCmd('view', 'cp');

		JFactory::getApplication()->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}

	/**
	 * Method For getting jgive version
	 *
	 * @return   void
	 *
	 * @since  1.7
	 */
	public function getVersion()
	{
		echo $recdata = @file_get_contents('https://techjoomla.com/vc/index.php?key=abcd1234&product=jgive');
		jexit();
	}

	/**
	 * Method for creating previous created campaign
	 *
	 * @return boolean
	 *
	 * @since   2.0
	 */
	public function createActivity()
	{
		require_once JPATH_ADMINISTRATOR . '/components/com_jgive/models/migration.php';
		$model = $this->getModel('migration');

		$result = $model->createActivity();

		if ($result)
		{
			echo "<br/>" . JText::_('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_DONE');
		}
		else
		{
			echo "<br/>" . JText::_('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_ERROR');
		}
	}
}
