<?php 

$emails_config=array(
'message_body'=>"<table width=\"100%\">
<tbody>
<tr>
<td align=\"center\">
<h2><strong>Donation Receipt</strong></h2>
</td>
</tr>
<tr>
<td>
<p style=\"text-align: right;\"><strong>Receipt No</strong>. : {payment.order_id}<br /><strong>Date</strong>: {payment.cdate}</p>
</td>
</tr>
<tr>
<td><hr />
<p>A donation in the amount of : <strong> {campaign.donation_amount}</strong> has been made to <strong>{campaign.title}</strong></p>
</td>
</tr>
<tr>
<td style=\"text-align: left;\" colspan=\"2\">
<p><span style=\"font-family: verdana;\"><span style=\"font-size: 16px;\"><strong>Donor</strong>: {donor.first_name} {donor.last_name}</span></span>   </p>
</td>
</tr>
<tr>
<td colspan=\"2\" align=\"center\">
<p><strong><span style=\"font-size: 18px; font-family: verdana;\">Thank you for your generous support!</span></strong></p>
</td>
</tr>
</tbody>
</table>",
'email_subject'=>"Donation Receipt"
);

?>