<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

/**
 * JTable class for Campaign.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.7
 */
class JgiveTablecampaign extends JTable
{
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$_db  Database connector object
	 *
	 * @since 1.7
	 */
	public function __construct (&$_db)
	{
		parent::__construct('#__jg_campaigns', 'id', $_db);
	}

	/**
	 * Overloaded bind function
	 *
	 * @param   array  $array   Named array to bind
	 * @param   mixed  $ignore  An optional array or space separated list of properties to ignore while binding.
	 *
	 * @return  mixed  Null if operation was satisfactory, otherwise returns an error
	 *
	 * @since   1.7
	 */
	public function bind ($array, $ignore = '')
	{
		if (isset($array['params']) && is_array($array['params']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['params']);
			$array['params'] = (string) $registry;
		}

		if (isset($array['metadata']) && is_array($array['metadata']))
		{
			$registry = new JRegistry;
			$registry->loadArray($array['metadata']);
			$array['metadata'] = (string) $registry;
		}

		if (! JFactory::getUser()->authorise('core.admin', 'com_tjfields.country.' . $array['id']))
		{
			$actions = JFactory::getACL()->getActions('com_tjfields', 'country');
			$default_actions = JFactory::getACL()->getAssetRules('com_tjfields.country.' . $array['id'])->getData();
			$array_jaccess = array();

			foreach ($actions as $action)
			{
				$array_jaccess[$action->name] = $default_actions[$action->name];
			}

			$array['rules'] = $this->JAccessRulestoArray($array_jaccess);
		}

		// Bind the rules for ACL where supported.
		if (isset($array['rules']) && is_array($array['rules']))
		{
			$this->setRules($array['rules']);
		}

		return parent::bind($array, $ignore);
	}

	/**
	 * This function convert an array of JAccessRule objects into an rules array.
	 *
	 * @param   type  $jaccessrules  an array of JAccessRule objects.
	 *
	 * @return  mixed  $rules  Set of rules
	 *
	 * @since   1.7
	 */
	private function JAccessRulestoArray ($jaccessrules)
	{
		$rules = array();

		foreach ($jaccessrules as $action => $jaccess)
		{
			$actions = array();

			foreach ($jaccess->getData() as $group => $allow)
			{
				$actions[$group] = ((bool) $allow);
			}

			$rules[$action] = $actions;
		}

		return $rules;
	}

	/**
	 * Overloaded check function
	 *
	 * @return  boolean
	 *
	 * @see     JTable::check
	 *
	 * @since   1.7
	 */
	public function check ()
	{
		// If there is an ordering column and this is a new row then get the
		// next ordering value
		if (property_exists($this, 'ordering') && $this->id == 0)
		{
			$this->ordering = self::getNextOrder();
		}

		// Start code validations
		$db = JFactory::getDbo();

		// Check for 3 digit code
		$query = "SELECT id, country_3_code
		 FROM #__tj_country
		 WHERE country_3_code = " . $db->quote($this->country_3_code) . "
		 AND id != " . (int) $this->id;
		$db->setQuery($query);
		$result = intval($db->loadResult());

		if ($result)
		{
			$this->setError(JText::_('COM_TJFIELDS_COUNTRY_CODE_3_EXISTS'));

			return false;
		}
		else
		{
			// Check for 2 digit code
			$query = "SELECT id, country_code
			 FROM #__tj_country
			 WHERE country_code = " . $db->quote($this->country_code) . "
			 AND id != " . (int) $this->id;

			$db->setQuery($query);
			$result = intval($db->loadResult());

			if ($result)
			{
				$this->setError(JText::_('COM_TJFIELDS_COUNTRY_CODE_EXISTS'));

				return false;
			}
		}

		return parent::check();
	}

	/**
	 * Method to set the publishing state for a row or list of rows in the database
	 * table.  The method respects checked out rows by other users and will attempt
	 * to checkin rows that it can after adjustments are made.
	 *
	 * @param   mixed    $pks     An optional array of primary key values to update.  If not set the instance property value is used.
	 * @param   integer  $state   The publishing state. eg. [0 = unpublished, 1 = published, 2=archived, -2=trashed]
	 * @param   integer  $userId  The user id of the user performing the operation.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.6
	 */
	public function publish ($pks = null, $state = 1, $userId = 0)
	{
		// Initialise variables.
		$k = $this->_tbl_key;

		// Sanitize input.
		JArrayHelper::toInteger($pks);
		$userId = (int) $userId;
		$state = (int) $state;

		// If there are no primary keys set check to see if the instance key is
		if (empty($pks))
		{
			if ($this->$k)
			{
				$pks = array(
						$this->$k
				);
			}
			// Nothing to set publishing state on, return false.
			else
			{
				$this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));

				return false;
			}
		}

		// Build the WHERE clause for the primary keys.
		$where = $k . '=' . implode(' OR ' . $k . '=', $pks);

		// Determine if there is checkin support for the table.
		if (! property_exists($this, 'checked_out') || ! property_exists($this, 'checked_out_time'))
		{
			$checkin = '';
		}
		else
		{
			$checkin = ' AND (checked_out = 0 OR checked_out = ' . (int) $userId . ')';
		}

		if (is_array($pks))
		{
			$params = JComponentHelper::getParams('com_jgive');
			$admin_approval = (int) $params->get('admin_approval');

			foreach ($pks as $pk)
			{
				$query = $this->_db->getQuery(true);

				// Update the state flag
				$query->update($this->_db->quoteName('#__jg_campaigns'))
					->set($this->_db->quoteName('published') . ' = ' . $state)
					->where($this->_db->quoteName('id') . ' = ' . $pk);

				$this->_db->setQuery($query);
				$this->_db->execute();

				// Check for a database error.
				if ($this->_db->getErrorNum())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}

				if ($state == 1)
				{
					// If admin approval is on for products
					if ($admin_approval === 1)
					{
						$query = $this->_db->getQuery(true);

						$query->select('u.email, u.name, u.username');
						$query->from('#__users As u');

						$query->select(' c.title, c.id');
						$query->from('#__jg_campaigns As c');

						$query->where("u.id = c.creator_id AND c.id = " . $pk);

						$this->_db->setQuery($query);
						$camp_info = $this->_db->loadObject();

						$this->SendMailToOwnerAfterApproval($camp_info, $state);
					}
				}
			}
		}

		// If checkin is supported and all rows were adjusted, check them in.
		if ($checkin && (count($pks) == $this->_db->getAffectedRows()))
		{
			// Checkin each row.
			foreach ($pks as $pk)
			{
				$this->checkin($pk);
			}
		}

		// If the JTable instance value is in the list of primary keys that were. Set, set the instance.
		if (in_array($this->$k, $pks))
		{
			$this->state = $state;
		}

		$this->setError('');

		return true;
	}

	/**
	 * Method to toggle the featured setting of products.
	 *
	 * @param   array    $pks    The ids of the items to toggle.
	 * @param   integer  $value  The value to toggle to.
	 *
	 * @return  boolean  True on success.
	 */
	public function featured($pks, $value = 0)
	{
		// Sanitize the ids.
		$pks = (array) $pks;
		JArrayHelper::toInteger($pks);

		try
		{
			$db = $this->getDbo();
			$query = $db->getQuery(true)
						->update($db->quoteName('#__jg_campaigns'))
						->set('featured = ' . (int) $value)
						->where('id IN (' . implode(',', $pks) . ')');
			$query;
			$db->setQuery($query);
			$db->execute();
		}
		catch (RuntimeException $e)
		{
			throw new Exception($e->getMessage());
		}

		return true;
	}

	/**
	 * Define a namespaced asset name for inclusion in the #__assets table
	 *
	 * @return string The asset name
	 *
	 * @see JTable::_getAssetName
	 */
	protected function _getAssetName ()
	{
		$k = $this->_tbl_key;

		return 'com_jgive.campaign.' . (int) $this->$k;
	}

	/**
	 * Method to get the parent asset under which to register this one.
	 * By default, all assets are registered to the ROOT node with ID,
	 * which will default to 1 if none exists.
	 * The extended class can define a table and id to lookup.  If the
	 * asset does not exist it will be created.
	 *
	 * @param   JTable   $table  A JTable object for the asset parent.
	 * @param   integer  $id     Id to look up
	 *
	 * @return  integer
	 *
	 * @since   11.1
	 */
	protected function _getAssetParentId (JTable $table = null, $id = null)
	{
		// We will retrieve the parent-asset from the Asset-table
		$assetParent = JTable::getInstance('Asset');

		// Default: if no asset-parent can be found we take the global asset
		$assetParentId = $assetParent->getRootId();

		// The item has the component as asset-parent
		$assetParent->loadByName('com_jgive');

		// Return the found asset-parent-id
		if ($assetParent->id)
		{
			$assetParentId = $assetParent->id;
		}

		return $assetParentId;
	}

	/**
	 * Method to send email to owner after approving campaign by site admin
	 *
	 * @param   mixed  $owner  data required for email
	 *
	 * @return  boolean
	 *
	 * @since   1.7
	 **/
	public function SendMailToOwnerAfterApproval($owner)
	{
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');

		$app = JFactory::getApplication();
		$fromname = $app->getCfg('fromname');
		$sitename = $app->getCfg('sitename');

		$subject = JText::_('COM_JGIVE_CAMPAIGN_AAPROVED_SUBJECT');
		$subject = str_replace('{sellername}', $owner->name, $subject);

		$body = JText::_('COM_JGIVE_CAMPAIGN_AAPROVED_BODY');
		$body = str_replace('{owner_name}', $owner->name, $body);
		$body = str_replace('{name}', $owner->title, $body);
		$body = str_replace('{admin}', $fromname, $body);
		$body = str_replace('{link}', JUri::root() . 'index.php?option=com_jgive&view=campaigns&layout=my&Itemid=' . $itemid, $body);
		$body = str_replace('{sitelink}', JUri::root(), $body);
		$body = str_replace('{sitename}', $sitename, $body);

		$donationsHelper = new donationsHelper;
		$res = $donationsHelper->sendmail($owner->email, $subject, $body, $owner->email, 'campaign.statuschange');

		return $res;
	}

	/**
	 * Method to Delete the campaign
	 *
	 * @param   array  $pk  Primary Key
	 *
	 * @return  boolean
	 *
	 * @since   1.7
	 **/
	public function delete($pk = null)
	{
		if (is_array($pk))
		{
			foreach ($pk as $pkid)
			{
				$this->deleteCampData($pkid);
			}
		}
		else
		{
			$this->deleteCampData($pk);
		}

		return true;
	}

	/**
	 * Method to Delete Campaign Data
	 *
	 * @param   array  $pk  Primary Key
	 *
	 * @return  boolean
	 *
	 * @since   1.9.5
	 **/
	public function deleteCampData($pk = null)
	{
		// Delete Campaign data
		$query = $this->_db->getQuery(true);
		$query->delete($this->_db->qn('#__jg_campaigns'));
		$query->where('id=' . $pk);
		$this->_db->setQuery($query);
		$this->_db->execute();

		if (!$this->_db->execute()->stderr)
		{
			// Delete Campaign Giveback data
			$query = $this->_db->getQuery(true);
			$query->delete($this->_db->qn('#__jg_campaigns_givebacks'));
			$query->where('campaign_id=' . $pk);
			$this->_db->setQuery($query);
			$this->_db->execute();

			// Delete Campaign Image data
			$query = $this->_db->getQuery(true);
			$query->delete($this->_db->qn('#__jg_campaigns_images'));
			$query->where('campaign_id=' . $pk);
			$this->_db->setQuery($query);
			$this->_db->execute();

			// Delete Campaign Donation table data
			$query = $this->_db->getQuery(true);
			$query->delete($this->_db->qn('#__jg_donations'));
			$query->where('campaign_id=' . $pk);
			$this->_db->setQuery($query);
			$this->_db->execute();

			// Delete Campaign Donors data
			$query = $this->_db->getQuery(true);
			$query->delete($this->_db->qn('#__jg_donors'));
			$query->where('campaign_id=' . $pk);
			$this->_db->setQuery($query);
			$this->_db->execute();

			// Delete Campaign Orders data
			$query = $this->_db->getQuery(true);
			$query->delete($this->_db->qn('#__jg_orders'));
			$query->where('campaign_id=' . $pk);
			$this->_db->setQuery($query);
			$this->_db->execute();

			// Delete Campaign Updates data
			$query = $this->_db->getQuery(true);
			$query->delete($this->_db->qn('#__jg_updates'));
			$query->where('campaign_id=' . $pk);
			$this->_db->setQuery($query);
			$this->_db->execute();
		}
	}
}
