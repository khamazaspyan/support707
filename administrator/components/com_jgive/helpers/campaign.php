<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

// Component Helper
jimport('joomla.application.component.helper');

/**
 * CampaignHelper
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class CampaignHelper
{
	/**
	 * Get campaign Promoter Paypal Id
	 *
	 * @param   INT  $orderid  Order Id
	 *
	 * @return  promoter paypal id
	 */
	public function getCampaignPromoterPaypalId($orderid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT c.paypal_email
			FROM #__jg_campaigns AS c
			LEFT JOIN #__jg_orders AS o ON o.campaign_id=c.id
			WHERE o.id=" . $orderid;
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get Campaign Title
	 *
	 * @param   INT  $orderid  Order Id
	 *
	 * @return  campaign title
	 */
	public function getCampaignTitle($orderid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT c.title
			FROM #__jg_campaigns AS c
			LEFT JOIN #__jg_orders AS o ON o.campaign_id=c.id
			WHERE o.id=" . $orderid;
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get Campaign Title From Cid
	 *
	 * @param   INT  $cid  Campaign ID
	 *
	 * @return  campaign title
	 */
	public function getCampaignTitleFromCid($cid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT c.title
			FROM #__jg_campaigns AS c
			WHERE c.id=" . $cid;
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Used in backend - reports view
	 *
	 * @return  campaign data
	 */
	public function getAllCampaignOptions()
	{
		$db    = JFactory::getDBO();
		$query = "SELECT c.id, c.title
		FROM `#__jg_campaigns` AS c
		ORDER BY c.title";

		$db->setQuery($query);
		$campaigns = $db->loadObjectList();

		return $campaigns;
	}

	/**
	 * Get Campaign amounts
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  amounts
	 */
	public function getCampaignAmounts($cid)
	{
		$db = JFactory::getDBO();

		$query = "SELECT c.goal_amount
		FROM `#__jg_campaigns` AS c
		WHERE c.id=" . $cid;
		$db->setQuery($query);
		$goal_amount = $db->loadResult();

		$query = "SELECT SUM(o.amount) AS amount_received
		FROM `#__jg_orders` AS o
		WHERE o.campaign_id=" . $cid . "
		AND o.status='C'";
		$db->setQuery($query);
		$amounts                    = array();
		$amounts['amount_received'] = $db->loadResult();

		// If no donations, set receved amount as zero
		if ($amounts['amount_received'] == '')
		{
			$amounts['amount_received'] = 0;
		}

		// Calculate remaining amount
		$amounts['remaining_amount'] = ($goal_amount) - ($amounts['amount_received']);

		return $amounts;
	}

	/**
	 * Get Campaign Images
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  images
	 */
	public function getCampaignImages($cid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT *
		FROM `#__jg_campaigns_images`
		WHERE `campaign_id`=" . $cid;
		$db->setQuery($query);
		$images = $db->loadObjectList();

		return $images;
	}

	/**
	 * Get Campaign Givebacks
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  givebacks
	 */
	public function getCampaignGivebacks($cid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT *
		FROM `#__jg_campaigns_givebacks`
		WHERE `campaign_id`=" . $cid . "
		ORDER BY amount ";
		$db->setQuery($query);
		$givebacks = $db->loadObjectList();

		return $givebacks;
	}

	/**
	 * Get Campaign Updates
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  Updates
	 */
	/*public function getCampaignUpdates($cid)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select('*');
		$query->from($db->qn('#__jg_updates', 'ud'));
		$query->where($db->qn('ud.campaign_id') . ' = ' . $db->quote($cid));

		$db->setQuery($query);
		$updates = $db->loadObjectList();

		return $updates;
	}*/

	/**
	 * Get Campaign Donors Count
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  donors
	 */
	public function getCampaignDonors($cid)
	{
		$db    = JFactory::getDBO();

		// Get campaign donors
		$query = "SELECT du.user_id, du.first_name, du.last_name, du.address, du.address2, du.city, du.state, du.country, du.zip,
		ds.annonymous_donation, o.amount, o.cdate
		FROM `#__jg_donors` AS du
		LEFT JOIN `#__jg_donations` AS ds on ds.donor_id=du.id
		LEFT JOIN `#__jg_orders` AS o on o.donation_id=ds.id
		WHERE ds.campaign_id=" . $cid . "
		AND o.status='C'";
		$db->setQuery($query);
		$donors = $db->loadObjectList();

		if (isset($donors))
		{
			foreach ($donors as $donor)
			{
				$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
				$donor->avatar      = $JgiveIntegrationsHelper->getUserAvatar($donor->user_id);
				$donor->profile_url = $JgiveIntegrationsHelper->getUserProfileUrl($donor->user_id);
			}
		}

		return $donors;
	}

	/**
	 * Get Campaign Donors Count
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  void
	 */
	public function getCampaignDonorsCount($cid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT COUNT(o.amount) AS donors_count
			FROM `#__jg_orders` AS o
			WHERE o.campaign_id=" . $cid . "
			AND o.status='C'";
		$db->setQuery($query);
		$donors_count = $db->loadResult();

		if ($donors_count == '')
		{
			$donors_count = 0;
		}

		return $donors_count;
	}

	/**
	 * Upload Image
	 *
	 * @param   INT  $camp_id  Campaign Id
	 *
	 * @return  void
	 */
	public function uploadImage($camp_id)
	{
		$db = JFactory::getDBO();

		/* save uploaded image
		check the file extension is ok */
		$file_field            = 'camp_image';
		$file_name             = $_FILES[$file_field]['name'];
		$media_info            = pathinfo($file_name);

		$uploadedFileName      = $media_info['filename'];
		$uploadedFileExtension = $media_info['extension'];
		$validFileExts         = explode(',', 'jpeg,jpg,png,gif');

		// Assume the extension is false until we know its ok
		$extOk                 = false;

		/* go through every ok extension, if the ok extension matches the file extension (case insensitive)
		then the file extension is ok */
		foreach ($validFileExts as $key => $value)
		{
			if (preg_match("/$value/i", $uploadedFileExtension))
			{
				$extOk = true;
			}
		}

		if ($extOk == false)
		{
			echo JText::_('COM_JGIVE_INVALID_IMAGE_EXTENSION');

			return;
		}

		// The name of the file in PHP's temp directory that we are going to move to our folder
		$file_temp      = $_FILES[$file_field]['tmp_name'];

		// For security purposes, we will also do a getimagesize on the temp file (before we have moved
		// it to the folder) to check the MIME type of the file, and whether it has a width and height */
		$image_info     = getimagesize($file_temp);

		/** We are going to define what file extensions/MIMEs are ok, and only let these ones in (whitelisting), rather than try to scan for bad
		Types, where we might miss one (whitelisting is always better than blacklisting) */
		$okMIMETypes    = 'image/jpeg,image/pjpeg,image/png,image/x-png,image/gif';
		$validFileTypes = explode(",", $okMIMETypes);

		// If the temp file does not have a width or a height, or it has a non ok MIME, return
		if (!is_int($image_info[0]) || !is_int($image_info[1]) || !in_array($image_info['mime'], $validFileTypes))
		{
			echo JText::_('COM_JGIVE_INVALID_IMAGE_EXTENSION');

			return;
		}

		// Clean up filename to get rid of strange characters like spaces etc
		$file_name = JFile::makeSafe($uploadedFileName);

		// Lose any special characters in the filename
		$file_name = preg_replace("/[^A-Za-z0-9]/i", "-", $file_name);

		// Use lowercase
		$file_name = strtolower($file_name);

		// Add timestamp to file name
		$timestamp = time();
		$file_name = $file_name . '_' . $timestamp . '.' . $uploadedFileExtension;

		// Always use constants when making file paths, to avoid the possibilty of remote file inclusion
		$upload_path_folder       = JPATH_SITE . '/images/jGive';
		$image_upload_path_for_db = 'images/jGive';

		// If folder is not present create it
		if (!file_exists($upload_path_folder))
		{
			@mkdir($upload_path_folder);
		}

		$upload_path = $upload_path_folder . '/' . $file_name;
		$image_upload_path_for_db .= '/' . $file_name;

		if (!JFile::upload($file_temp, $upload_path))
		{
			echo JText::_('COM_JGIVE_ERROR_MOVING_FILE');

			return false;
		}
		else
		{
			$obj              = new stdClass;
			$obj->id          = '';
			$obj->campaign_id = $camp_id;
			$obj->path        = $image_upload_path_for_db;
			$obj->order       = '';

			if (!$db->insertObject('#__jg_campaigns_images', $obj, 'id'))
			{
				echo $db->stderr();

				return false;
			}
			else
			{
				return $db->insertid();
			}
		}

		return true;
	}

	/**
	 * Get All Category Options
	 *
	 * @return  void
	 */
	public function getAllCategoryOptions()
	{
		$db    = JFactory::getDBO();
		$query = "SELECT c.category_id,cat.title
		FROM `#__jg_campaigns` AS c
		INNER JOIN #__categories as cat ON cat.id=c.category_id
		ORDER BY cat.title";

		$db->setQuery($query);
		$campaigns = $db->loadObjectList();

		return $campaigns;
	}

	/**
	 * Get campaign category
	 *
	 * @return  void
	 */
	public function getCampaignsCats()
	{
		$mainframe = JFactory::getApplication();
		$db        = JFactory::getDBO();
		$query     = "SELECT id,title FROM #__categories WHERE extension='com_jgive' && parent_id=1";
		$db->setQuery($query);
		$categories = $db->loadObjectList();
		$default    = '';
		$options[]  = JHtml::_('select.option', '0', '-Select Category-');

		foreach ($categories as $cat_obj)
		{
			$options[] = JHtml::_('select.option', $cat_obj->id, $cat_obj->title);
		}

		if (JFactory::getApplication()->input->get('cid'))
		{
			$details  = $this->getCampaignDetails(JFactory::getApplication()->input->get('cid'));
			$camp_cat = $details->category_id;

			$cid   = JFactory::getApplication()->input->get('cid');
			$db    = JFactory::getDBO();
			$query = "SELECT * FROM #__jg_campaigns WHERE id='" . $cid . "'";
			$db->setQuery($query);

			$options  = array_merge($options, $db->loadObjectlist());
			$dropdown = JHtml::_('list.category', 'campaign_category', "com_jgive", intval($camp_cat));
		}
		else
		{
			$dropdown = JHtml::_('list.category', 'campaign_category', "com_jgive", intval(1));
		}

		return $dropdown;
	}

	/**
	 * Get campaign category filter
	 *
	 * @param   INT  $c_id  Camp id
	 *
	 * @return  void
	 */
	public function getCampaignDetails($c_id)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT * FROM #__jg_campaigns WHERE id='" . $c_id . "'";
		$db->setQuery($query);

		return ($db->loadObject());
	}

	/**
	 * Get campaign category filter
	 *
	 * @param   INT  $firstOption  First Option
	 *
	 * @return  void
	 */
	public function getCampaignsCategories($firstOption = '')
	{
		$app         = JFactory::getApplication();
		$categories  = JHtml::_('category.options', 'com_jgive');
		$cat_options = array();

		if ($app->isSite() OR JVERSION < 3.0)
		{
			$cat_options[] = JHtml::_('select.option', '0', JText::_('COM_JGIVE_CAMPAIGN_CATEGORIES'));
		}

		if (!empty($categories))
		{
			foreach ($categories as $category)
			{
				if (!empty($category))
				{
					$cat_options[] = JHtml::_('select.option', $category->value, $category->text);
				}
			}
		}

		return $cat_options;
	}

	/**
	 * Get Campaign Type FilterOptions
	 *
	 * @return  void
	 */
	public function getCampaignTypeFilterOptions()
	{
		$mainframe            = JFactory::getApplication();
		$filter_campaign_type = $mainframe->getUserStateFromRequest('com_jgive.filter_campaign_type', 'filter_campaign_type');

		$options = array();
		$options[] = JHtml::_('select.option', 'donation', JText::_('COM_JGIVE_CAMPAIGN_TYPE_DONATION'));
		$options[] = JHtml::_('select.option', 'investment', JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT'));

		return $options;
	}

	/**
	 * Check the campaign marks as featured
	 *
	 * @param   INT  $contentId  Contend ID
	 *
	 * @return  void
	 */
	public function isFeatured($contentId)
	{
		$db    = JFactory::getDBO();
		$query = 'SELECT featured FROM `#__jg_campaigns` WHERE `id` = ' . $contentId;
		$db->setQuery($query);
		$result = $db->loadResult();

		return $result = (empty($result)) ? 0 : $result;
	}

	/**
	 * Get Campaign type Donate/Invest
	 *
	 * @param   INT  $campid  Camp ID
	 *
	 * @return  void
	 */
	public function getCampaignType($campid)
	{
		$db    = JFactory::getDBO();
		$query = 'SELECT id,creator_id,type FROM `#__jg_campaigns` WHERE `id`= ' . $campid;
		$db->setQuery($query);
		$result = $db->LoadObject();

		return $result = (empty($result)) ? '' : $result;
	}

	/**
	 * Send email to site admin when campaigns is created
	 *
	 * @param   Object  $camp_details  Camp details
	 * @param   INT     $camp_id       Camp ID
	 *
	 * @return  void
	 */
	public function SendCmap_Create_mail($camp_details, $camp_id)
	{
		$params    = JComponentHelper::getParams('com_jgive');
		$billemail = $params->get('email');

		if (!empty($billemail))
		{
			$userid          = JFactory::getUser()->id;
			$body            = JText::_('COM_JGIVE_CAMP_AAPROVAL_BODY');
			$body            = str_replace('{title}', $camp_details->get('title', '', 'STRING'), $body);
			$body            = str_replace('{campid}', ':' . $camp_id, $body);
			$body            = str_replace('{username}', $camp_details->get('first_name', '', 'STRING'), $body);
			$body            = str_replace('{userid}', $userid, $body);
			$camp_link       = 'administrator/index.php?option=com_jgive&view=campaigns&layout=default&approve=1';
			$body            = str_replace('{link}', JUri::base() . $camp_link, $body);
			$subject         = JText::sprintf('COM_JGIVE_CAMP_CREATED_EMAIL_SUBJECT', $camp_details->get('title', '', 'STRING'));
			$donationsHelper = new donationsHelper;
			$donationsHelper->sendmail($billemail, $subject, $body, $params->get('email'), 'campaign.create');
		}
	}

	/**
	 * Send email to promoter when campaigns is created
	 *
	 * @param   Object  $camp_details  Camp details
	 * @param   INT     $camp_id       Camp ID
	 *
	 * @return  void
	 */
	public function sendCmapCreateMailToPromoter($camp_details, $camp_id)
	{
		$user                = JFactory::getUser();
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$donationsHelper     = new donationsHelper;
		$body                = JText::_('COM_JGIVE_CAMP_AAPROVAL_BODY_PROMOTER');
		$body                = str_replace('{title}', $camp_details->get('title', '', 'STRING'), $body);
		$body                = str_replace('{campid}', ':' . $camp_id, $body);
		$itemid              = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');
		$body                = str_replace('{link}', JUri::base() . 'index.php?option=com_jgive&view=campaigns&layout=my&itemid=' . $itemid, $body);
		$billemail           = $user->email;
		$subject             = JText::sprintf('COM_JGIVE_CAMP_CREATED_EMAIL_SUBJECT_PROMOTER', $camp_details->get('title', '', 'STRING'));
		$donationsHelper->sendmail($billemail, $subject, $body, $user->email, 'campaign.create');
	}

	/**
	 * Send email to promoter after approved campaign
	 *
	 * @param   Object  $camp_info  Camp Id
	 * @param   INT     $state      Camp state
	 *
	 * @return  void
	 */
	public function sendemailCampaignApprovedReject($camp_info, $state)
	{
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$donationsHelper     = new donationsHelper;
		$objectcount         = count($camp_info);

		foreach ($camp_info as $each_creator)
		{
			$html = JText::_('COM_JGIVE_CAMPAIGN_DETAILS_MAIL');
			$html .= '<table cellspacing="15">
					<th>' . JText::sprintf('COM_JGIVE_CAMPAIGN_ID') . '</th>
					<th>' . JText::sprintf('COM_JGIVE_CAMPAIGN_NAME') . '</th> ';

			foreach ($each_creator as $row)
			{
				$html .= '<tr>
							<td>' . $row->id . '</td>
							<td>' . $row->title . '</td>
						</tr>';
				$billemail = $row->email;
			}

			$html .= '</table>';

			$subject = JText::_('COM_JGIVE_CAMP_CREATED_EMAIL_SUBJECT_PROMOTER_APPROVED');

			// According to state get approved / rejected /delete language constant
			// Rejected
			if ($state == 0)
			{
				$campaign_status = JText::sprintf('COM_JGIVE_CAMAPIGN_REJECTED');
			}
			elseif ($state == 1) // Approved
			{
				$campaign_status = JText::sprintf('COM_JGIVE_CAMAPIGN_APPROVED');
			}
			else // Deleted
			{
				$campaign_status = JText::sprintf('COM_JGIVE_CAMAPIGN_DELETED');
			}

			$subject = str_replace('{status}', $campaign_status, $subject);

			// My campaign link for user only when campaign is approved
			if ($state == 1 or $state == 0)
			{
				$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');
				$html .= JText::_('COM_JGIVE_CLICK_HERE');
				$html .= JUri::root() . 'index.php?option=com_jgive&view=campaigns&layout=my&itemid=' . $itemid;
			}

			$donationsHelper->sendmail($billemail, $subject, $html, $billemail, 'campaign.statuschange');
		}
	}

	/**
	 * Function to get main category name
	 *
	 * @param   INT  $catid  Camp Id
	 *
	 * @return  Cat name
	 */
	public function getCatname($catid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT title FROM #__categories as cat
				WHERE cat.id=" . $catid . " AND `extension`='com_jgive'";
		$db->setquery($query);

		return $result = $db->loadResult();
	}

	/**
	 * Function to add the organization/individual type field Individuals
	 *
	 * @return  Options
	 */
	public function Organization_Individual_type()
	{
		$options = array();
		$app     = JFactory::getApplication();

		if ($app->isSite() OR JVERSION < 3.0)
		{
			$options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_SELECT_TYPE_ORG_INDIVIDUALS'));
		}

		$options[] = JHtml::_('select.option', 'non_profit', JText::_('COM_JGIVE_ORG_NON_PROFIT'));
		$options[] = JHtml::_('select.option', 'self_help', JText::_('COM_JGIVE_SELF_HELP'));
		$options[] = JHtml::_('select.option', 'individuals', JText::_('COM_JGIVE_SELF_INDIVIDUALS'));

		return $options;
	}

	/**
	 * Campaigns To Show Options
	 *
	 * @return  Options
	 */
	public function campaignsToShowOptions()
	{
		$options = array();
		$app     = JFactory::getApplication();

		if ($app->isSite() OR JVERSION < 3.0)
		{
			$options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_CAM_TO_SHOW'));
		}

		$options[] = JHtml::_('select.option', 'featured', JText::_('COM_JGIVE_FEATURED_CAMP'));
		$options[] = JHtml::_('select.option', 'successful', JText::_('COM_JGIVE_SUCCESSFUL_CAMP'));
		$options[] = JHtml::_('select.option', 'other', JText::_('COM_JGIVE_OTHTER_CAMP'));

		return $options;
	}
}
