<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();
jimport('joomla.application.component.model');
jimport('techjoomla.common');

/**
 * Donations_Backend Helper
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class Donations_BackendHelper
{
	/**
	 * Function used in donation details view
	 *
	 * @return  $donation_details
	 */
	public function getSingleDonationInfo()
	{
		$order_id_key = JFactory::getApplication()->input->get('donationid');

		$path         = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$donationsHelper  = new donationsHelper;
		$donation_details = $donationsHelper->getSingleDonationInfo($order_id_key);

		return $donation_details;
	}

	/**
	 * Function for sets campaign id in session
	 *
	 * @param   Array  $post  POST
	 *
	 * @return  boolean
	 */
	public function setSessionCampaignId($post)
	{
		$session = JFactory::getSession();
		$this->clearSessionCampaignId();
		$session->set('JGIVE_cid', $post['cid']);

		return true;
	}

	/**
	 * Function for clears campaign id from session
	 *
	 * @return  boolean
	 */
	public function clearSessionCampaignId()
	{
		$session = JFactory::getSession();
		$session->set('JGIVE_cid', '');

		return true;
	}

	/**
	 * Function for get campaign id from session
	 *
	 * @return  campaign id
	 */
	public function getCampaignId()
	{
		$session = JFactory::getSession();
		$post    = JRequest::get('post');

		if (empty($post['cid']))
		{
			$cid = $session->get('JGIVE_cid');
		}
		else
		{
			$cid = $post['cid'];
		}

		return $cid;
	}

	/**
	 * Function for adding  order in order table
	 *
	 * @param   Array  $post  Post
	 *
	 * @return  boolean
	 */
	public function addOrder($post)
	{
		$this->techjoomlacommon = new TechjoomlaCommon;

		$path = JPATH_ADMINISTRATOR . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('donations_backendHelper'))
		{
			JLoader::register('donations_backendHelper', $path);
			JLoader::load('donations_backendHelper');
		}

		$path = JPATH_ROOT . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $path);
			JLoader::load('campaignHelper');
		}

		$path = JPATH_ROOT . '/components/com_jgive/helper.php';

		if (!class_exists('jgiveFrontendHelper'))
		{
			JLoader::register('jgiveFrontendHelper', $path);
			JLoader::load('jgiveFrontendHelper');
		}

		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$campaignHelper      = new campaignHelper;

		// Get params
		$session              = JFactory::getSession();
		$campaignHelper       = new campaignHelper;
		$jgiveFrontendHelper  = new jgiveFrontendHelper;
		$params               = JComponentHelper::getParams('com_jgive');
		$commission_fee       = $params->get('commission_fee');
		$fixed_commission_fee = $params->get('fixed_commissionfee');

		if (empty($fixed_commission_fee))
		{
			$fixed_commission_fee = 0;
		}

		$send_payments_to_owner = $params->get('send_payments_to_owner');
		$db                     = JFactory::getDBO();
		$donorid       = $post->get('donor_id', '', 'STRING');

		// Get Checkout Type Registerd/Guest 0=Registerd 1=Guest
		$checkout_type = $post->get('checkout_type', '', 'STRING');
		$user          = JFactory::getUser($donorid);

		// Get the user groupwise commision form params
		$params_usergroup = $params->get('usergroup');

		// Get campaign type donation/Investment & its creator
		$camp_details            = $campaignHelper->getCampaignType($post->get('cid', '', 'INT'));

		// Get Campaign creator groups ids
		$campaign_creator        = JFactory::getUser($camp_details->creator_id);
		$camp_creator_groups_ids = $campaign_creator->groups;

		// If checkout type=Registered
		if ($checkout_type == 0)
		{
			$userid = $user->id;
		}
		else
		{
			$userid = 0;
		}

		$this->guest_donation = $params->get('guest_donation');

		if ($this->guest_donation)
		{
			if (!$userid)
			{
				$session->set('quick_reg_no_login', '1');
			}
		}
		elseif (!$userid)
		{
			$userid = 0;

			return false;
		}

		// Save donor details
		$obj = new stdClass;

		$obj->id = '';

		$obj->user_id     = $userid;
		$obj->campaign_id = $post->get('cid', '', 'INT');

		$obj->email      = $post->get('paypal_email', '', 'STRING');
		$obj->first_name = $post->get('first_name', '', 'STRING');
		$obj->last_name  = $post->get('last_name', '', 'STRING');
		$obj->address    = $post->get('address', '', '', 'STRING');
		$obj->address2   = $post->get('address2', '', 'STRING');

		$other_city_check = $post->get('other_city_check', '', 'STRING');

		if (!empty($other_city_check))
		{
			$obj->city = $post->get('other_city', '', 'STRING');
		}
		elseif (($post->get('city', '', 'STRING')) && ($post->get('city', '', 'STRING') != ''))
		{
			$obj->city = $post->get('city');
		}

		$obj->country = $post->get('country');
		$obj->state   = $post->get('state');

		$obj->zip   = $post->get('zip', '', 'STRING');
		$obj->phone = $post->get('phone', '', 'STRING');

		if ($obj->id)
		{
			if (!$db->updateObject('#__jg_donors', $obj, 'id'))
			{
				echo $db->stderr();

				return false;
			}
		}
		elseif (!$db->insertObject('#__jg_donors', $obj, 'id'))
		{
			echo $db->stderr();

			return false;
		}

		// Get last insert id
		if ($obj->id)
		{
			$donors_key = $obj->id;
		}
		else
		{
			$donors_key = $db->insertid();
		}

		$obj              = new stdClass;
		$obj->id          = '';
		$obj->campaign_id = $post->get('cid');
		$obj->donor_id    = $donors_key;

		if ($post->get('donation_type', '', 'INT'))
		{
			$obj->is_recurring        = 1;
			$obj->recurring_frequency = $post->get('recurring_freq', '', 'STRING');
			$obj->recurring_count     = $post->get('recurring_count', '', 'INT');
		}
		else
		{
			$obj->is_recurring        = 0;
			$obj->recurring_frequency = '';
			$obj->recurring_count     = '';
		}

		$obj->annonymous_donation = $post->get('annonymousDonation', '', 'INT');

		$no_giveback = $post->get('no_giveback', '', 'INT');

		// Check donor not checked no giveback option
		if (!$no_giveback)
		{
			$obj->giveback_id = $post->get('givebacks', '', 'INT');
		}
		else
		{
			$obj->giveback_id = 0;
		}

		if (!$db->insertObject('#__jg_donations', $obj, 'id'))
		{
			echo $db->stderr();

			return false;
		}

		$donation_id = $db->insertid();

		// Save order details
		$obj     = new stdClass;
		$obj->id = '';

		/*Lets make a random char for this order
		take order prefix set by admin*/
		$order_prefix       = (string) $params->get('order_prefix');

		// String length should not be more than 5
		$order_prefix       = substr($order_prefix, 0, 5);

		// Take separator set by admin
		$separator          = (string) $params->get('separator');
		$obj->order_id      = $order_prefix . $separator;

		// Check if we have to add random number to order id
		$use_random_orderid = (int) $params->get('random_orderid');

		if ($use_random_orderid)
		{
			$random_numer = $this->_random(5);
			$obj->order_id .= $random_numer . $separator;
			/* This length shud be such that it matches the column lenth of primary key
			it is used to add pading order_id_column_field_length - prefix_length - no_of_underscores - length_of_random number */
			$len = (23 - 5 - 2 - 5);
		}
		else
		{
			/*this length shud be such that it matches the column lenth of primary key
			it is used to add pading
			order_id_column_field_length - prefix_length - no_of_underscores*/
			$len = (23 - 5 - 2);
		}

		$obj->campaign_id     = $post->get('cid', '', 'INT');
		$obj->donor_id        = $donors_key;
		$obj->donation_id     = $donation_id;

		$obj->cdate = $this->techjoomlacommon->getDateInUtc(date("Y-m-d H:i:s"));
		$obj->mdate = $this->techjoomlacommon->getDateInUtc(date("Y-m-d H:i:s"));

		$obj->transaction_id  = '';
		$obj->original_amount = $post->get('donation_amount', '', 'FLOAT');
		$obj->amount          = $post->get('donation_amount', '', 'FLOAT');
		$obj->fee             = 0;

		if (!$send_payments_to_owner)
		{
			if (!empty($params_usergroup))
			{
				$count = count($params_usergroup);

				for ($l = 0; $l < $count; $l = $l + 3)
				{
					if (in_array($params_usergroup[$l], $camp_creator_groups_ids))
					{
						if ($camp_details->type == 'donation')
						{
							$commission_fee = (int) ($params_usergroup[$l + 1]);
						}
						elseif ($camp_details->type == 'investment')
						{
							$commission_fee = (int) ($params_usergroup[$l + 2]);
						}

						break;
					}
				}
			}

			if ($commission_fee > 0)
			{
				$obj->fee = (($obj->amount * $commission_fee) / 100) + $fixed_commission_fee;
			}
		}

		$obj->fund_holder = 0;
		$obj->vat_number  = $post->get('vat_number', '', 'STRING');

		if ($send_payments_to_owner)
		{
			// Money for this order will goto campaign promoters account
			$obj->fund_holder = 1;
		}

		// By default pending status
		$obj->status    = 'C';
		$obj->processor = $post->get('gateways', '', 'STRING');

		// Get the IP Address
		if (!empty($_SERVER['REMOTE_ADDR']))
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		else
		{
			$ip = 'unknown';
		}

		$obj->ip_address = $ip;

		if (!$db->insertObject('#__jg_orders', $obj, 'id'))
		{
			echo $db->stderr();
		}

		// Get last insert id
		$orders_key = $db->insertid();

		// Set order id in session
		$session = JFactory::getSession();
		$session->set('JGIVE_order_id', $orders_key);

		$db->setQuery('SELECT order_id FROM #__jg_orders WHERE id=' . $orders_key);
		$order_id      = (string) $db->loadResult();
		$maxlen        = 23 - strlen($order_id) - strlen($orders_key);
		$padding_count = (int) $params->get('padding_count');

		// Use padding length set by admin only if it is les than allowed(calculate) length
		if ($padding_count > $maxlen)
		{
			$padding_count = $maxlen;
		}

		if (strlen((string) $orders_key) <= $len)
		{
			$append = '';

			for ($z = 0; $z < $padding_count; $z++)
			{
				$append .= '0';
			}

			$append = $append . $orders_key;
		}

		$res      = new stdClass;
		$res->id  = $orders_key;
		$order_id = $res->order_id = $order_id . $append;

		if (!$db->updateObject('#__jg_orders', $res, 'id'))
		{
			// They return false;
		}

		// Trigger for campaign donation status
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');
		$result = $dispatcher->trigger('OnAfterJGivePaymentStatusChange', array($orders_key, $obj->status , '', 0));

		require_once JPATH_SITE . "/components/com_jgive/helpers/donations.php";
		$donationsHelper = new donationsHelper;
		$donationsHelper->sendOrderEmail($orders_key, $post->get('cid', '', 'INT'));

		return true;
	}

	/**
	 * Function getPaymentVars
	 *
	 * @param   String  $pg_plugin  PG Plugin
	 * @param   String  $tid        Order id
	 *
	 * @return  vars
	 */
	public function getPaymentVars($pg_plugin, $tid)
	{
		$campaignHelper         = new campaignHelper;
		$vars                   = new stdclass;
		$params                 = JComponentHelper::getParams('com_jgive');
		$currency_code          = $params->get('currency');
		$send_payments_to_owner = $params->get('send_payments_to_owner');

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";

		$pass_data                = $this->getdetails($tid);
		$vars->order_id           = $pass_data->order_id;

		// Get the campaign promoter paypal email id
		$CampaignPromoterPaypalId = $campaignHelper->getCampaignPromoterPaypalId($tid);

		$session = JFactory::getSession();
		$session->set('order_id', $tid);

		$vars->client = 'com_jgive';

		$vars->user_firstname = $pass_data->first_name;
		$vars->user_id        = JFactory::getUser()->id;
		$vars->user_email     = $pass_data->email;

		$this->guest_donation = $params->get('guest_donation');
		$guest_email          = '';

		if ($this->guest_donation)
		{
			if (!$vars->user_id)
			{
				$vars->user_id = 0;
				$session       = JFactory::getSession();
				$session->set('quick_reg_no_login', '1');
				$guest_email = '';
				$guest_email = md5($vars->user_email);
				$session     = JFactory::getSession();
				$session->set('guest_email', $guest_email);
			}
		}

		return $vars;
	}

	/**
	 * Function For loads payment plugin gateway html
	 *
	 * @param   String  $pg_plugin  PG Plugin
	 * @param   String  $tid        Order id
	 *
	 * @return  html
	 */
	public function getHTML($pg_plugin, $tid)
	{
		$vars       = $this->getPaymentVars($pg_plugin, $tid);
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('payment', $pg_plugin);
		$html = $dispatcher->trigger('onTP_GetHTML', array($vars));

		return $html;
	}

	/**
	 * Function For get details of order and donor
	 *
	 * @param   String  $tid  Order id
	 *
	 * @return  Array
	 */
	public function getdetails($tid)
	{
		$user_id = JFactory::getUser()->id;
		$query   = "SELECT o.id, o.order_id, d.first_name, d.email, d.phone,ds.is_recurring,ds.recurring_frequency,ds.recurring_count
		FROM #__jg_orders AS o
		LEFT JOIN #__jg_donors AS d ON d.id=o.donor_id
		LEFT JOIN #__jg_donations as ds ON ds.id=o.donation_id
		WHERE o.id=" . $tid . " AND d.user_id=" . $user_id;

		$this->_db->setQuery($query);
		$orderdetails = $this->_db->loadObjectlist();

		return $orderdetails['0'];
	}

	/**
	 * Function For load campaign data for campaign id stored in session
	 *
	 * @return  Array
	 */
	public function getCampaign()
	{
		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;
		$cid            = $this->getCampaignId();

		$query = "SELECT c.*
		FROM `#__jg_campaigns` AS c
		WHERE c.id=" . $cid;
		$this->_db->setQuery($query);
		$campaign          = $this->_db->loadObject();
		$cdata['campaign'] = $campaign;

		// Get campaign amounts
		$amounts                             = $campaignHelper->getCampaignAmounts($cid);
		$cdata['campaign']->amount_received  = $amounts['amount_received'];
		$cdata['campaign']->remaining_amount = $amounts['remaining_amount'];

		// Get campaign images
		$cdata['images']    = $campaignHelper->getCampaignImages($cid);

		// Get campaign givebacks
		$cdata['givebacks'] = $campaignHelper->getCampaignGivebacks($cid);

		return $cdata;
	}

	/**
	 * Function Payment Process
	 *
	 * @param   String  $post       POST
	 * @param   Array   $pg_plugin  PG Plugin
	 * @param   INT     $order_id   Order id
	 *
	 * @return  Array
	 */
	public function processPayment($post, $pg_plugin, $order_id)
	{
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$session             = JFactory::getSession();
		$guest_email         = '';
		$guest_email         = $session->get('guest_email');
		$session->clear('guest_email');
		$session->set('order_link_guestemail', $guest_email);

		// Load donations helper
		require_once JPATH_SITE . '/components/com_jgive/helpers/donations.php';
		$donationsHelper = new donationsHelper;

		JRequest::setVar('remote', 1);
		$donationItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations');

		$return_resp = array();

		// Authorise Post Data
		if ($post['plugin_payment_method'] == 'onsite')
		{
			$plugin_payment_method = $post['plugin_payment_method'];
		}

		// Trigger payment plugins- onTP_Processpayment
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('payment', $pg_plugin);
		$data = $dispatcher->trigger('onTP_Processpayment', array($post));
		$data = $data[0];

		// Store log
		$res = @$this->storelog($pg_plugin, $data);

		// Get order id
		if (empty($order_id))
		{
			$order_id = $data['order_id'];
		}

		// Get id for orders table using order_id
		$order_id_key      = $donationsHelper->getOrderIdKeyFromOrderId($order_id);

		// Gateway used
		$data['processor'] = $pg_plugin;

		// Payment status
		$data['status']    = trim($data['status']);

		// Get order amount
		$query = "SELECT o.original_amount
				FROM #__jg_orders as o
				where o.id=" . $order_id_key;
		$this->_db->setQuery($query);
		$order_amount          = $this->_db->loadResult();

		// Return url
		$return_resp['return'] = $data['return'];

		// If payment status is confirmed
		if ($data['status'] == 'C' && $order_amount == $data['total_paid_amt'])
		{
			// Update order details
			$this->updateOrder($data);

			// Update order status, send email,
			$donationsHelper->updatestatus($order_id_key, $data['status'], $comment = '', $notify_chk = 1);

			// Trigger plugins
			// OnAfterJGivePaymentSuccess
			$dispatcher = JDispatcher::getInstance();
			JPluginHelper::importPlugin('system');
			$result = $dispatcher->trigger('OnAfterJGivePaymentSuccess', array($order_id_key));

			if ($result === false)
			{
				return false;
			}

			// Added guest email in url for payment processs on site
			$return_resp['return'] = JUri::root() . substr(
			JRoute::_("index.php?option=com_jgive&view=donations&layout=details&donationid=" .
			$order_id_key . "&processor={$pg_plugin}&email=" . $guest_email . "&Itemid=" . $donationItemid, false
			), strlen(JUri::base(true)) + 1
			);

			$return_resp['status'] = '1';
		}
		elseif ($order_amount != $data['total_paid_amt'])
		{
			$data['status']        = 'E';
			$return_resp['status'] = '0';
		}
		elseif (!empty($data['status']))
		{
			// Added guest email in url for payment processs on site
			if ($plugin_payment_method && $data['status'] == 'P')
			{
				$return_resp['return'] = JUri::root() . substr(
				JRoute::_("index.php?option=com_jgive&view=donations&layout=details&donationid=" .
				$order_id_key . "&processor={$pg_plugin}&email=" . $guest_email . "&Itemid=" . $donationItemid, false
				), strlen(JUri::base(true)) + 1
				);
			}

			if ($data['status'] != 'C')
			{
				$this->updateOrder($data);
			}
			elseif ($data['status'] == 'C')
			{
				// Added guest email in url for payment processs on site
				$return_resp['return'] = JUri::root() . substr(
				JRoute::_(
				"index.php?option=com_jgive&view=donations&layout=cancel&processor={$pg_plugin}&email=" .
				$guest_email . "&Itemid=" . $chkoutItemid
				), strlen(JUri::base(true)) + 1
				);
			}

			$return_resp['status'] = '0';

			// TO DO where is this  used ???
			$res->processor        = $data['processor'];
			$return_resp['msg']    = $data['error']['code'] . " " . $data['error']['desc'];
		}

		return $return_resp;
	}

	/**
	 * Function For store log
	 *
	 * @param   String  $name  Name
	 * @param   Array   $data  Data
	 *
	 * @return  void
	 */
	public function storelog($name, $data)
	{
		$data1              = array();
		$data1['raw_data']  = $data['raw_data'];
		$data1['JT_CLIENT'] = "com_jgive";
		$dispatcher         = JDispatcher::getInstance();
		JPluginHelper::importPlugin('payment', $name);
		$data = $dispatcher->trigger('onTP_Storelog', array($data1));
	}

	/**
	 * Function For Update order
	 *
	 * @param   Array  $data  Data
	 *
	 * @return  void
	 */
	public function updateOrder($data)
	{
		// Load donations helper
		require_once JPATH_SITE . '/components/com_jgive/helpers/donations.php';
		$donationsHelper = new donationsHelper;

		// Get id for orders table using order_id

		$order_id_key = $donationsHelper->getOrderIdKeyFromOrderId($data['order_id']);

		// Get donation id
		$db    = JFactory::getDBO();
		$query = "SELECT donation_id
		FROM #__jg_orders where id=" . $order_id_key;
		$db->setQuery($query);
		$donation_id = $db->loadResult();

		$db    = JFactory::getDBO();
		$query = "SELECT subscr_id,is_recurring
		FROM #__jg_donations where id=" . $donation_id;
		$db->setQuery($query);
		$donation_details = $db->loadObject();

		/* If subscriber id is not exist then it is first response from paypal
		hence update donation table insert subsriber id & also update transaction id*/
		if ($donation_details->is_recurring)
		{
			if ($data['txn_type'] == 'subscr_payment') // For recurring payment
			{
				// Insert subscriber id in donation table if not exits (Recurring donations)
				if (empty($donation_details->subscr_id))
				{
					$db             = JFactory::getDBO();
					$res            = new stdClass;
					$res->id        = $donation_id;
					$res->subscr_id = $data['subscriber_id'];

					if (!$db->updateObject('#__jg_donations', $res, 'id'))
					{
						// They return false;
					}

					// Update First order status & transaction id

					$db                  = JFactory::getDBO();
					$res                 = new stdClass;
					$res->id             = $order_id_key;
					$res->mdate          = date("Y-m-d H:i:s");
					$res->transaction_id = $data['transaction_id'];
					$res->status         = $data['status'];
					$res->processor      = $data['processor'];
					$res->extra          = json_encode($data['raw_data']);

					if (!$db->updateObject('#__jg_orders', $res, 'id'))
					{
						// They return false;
					}
				}
				// For recurring payment for more responses other than first
				else
				{
					/*check the transaction id is present in the order table
					--------- get transaction id*/

					$db    = JFactory::getDBO();
					$query = "SELECT transaction_id
					FROM #__jg_orders where donation_id=" . $donation_id;
					$db->setQuery($query);
					$transaction_ids = $db->loadColumn();

					// Check is transaction id exist in array
					if ($transaction_ids[0])
					{
						$flag = 0;

						for ($i = 0; $i < count($transaction_ids); $i++)
						{
							// If same transaction id then update the order
							if ($transaction_ids[$i] == $data['transaction_id'])
							{
								// Transaction id already in table
								$flag = 1;
								break;
							}
						}

						// New transaction
						if ($flag == 0)
						{
							/*order_id campaign_id  donor_id donation_id fund_holder cdate mdate
							transaction_id transaction_id original_amount amount  fee  status processor ip_address extra*/
							$this->_addNewRecurringOrder($data, $order_id_key, $donation_id);
						}
						// Update existing transaction
						else
						{
							$db                  = JFactory::getDBO();
							$res                 = new stdClass;
							$res->mdate          = date("Y-m-d H:i:s");
							$res->transaction_id = $data['transaction_id'];
							$res->status         = $data['status'];
							$res->processor      = $data['processor'];
							$res->extra          = json_encode($data['raw_data']);

							if (!$db->updateObject('#__jg_orders', $res, 'transaction_id'))
							{
								// They return false;
							}
						}
					}
				}
			}
		}
		else
		{
			$db                  = JFactory::getDBO();
			$res                 = new stdClass;
			$res->id             = $order_id_key;
			$res->mdate          = date("Y-m-d H:i:s");
			$res->transaction_id = $data['transaction_id'];
			$res->status         = $data['status'];
			$res->processor      = $data['processor'];
			$res->extra          = json_encode($data['raw_data']);

			if (!$db->updateObject('#__jg_orders', $res, 'id'))
			{
				// The return false;
			}

			// For adaptive payment add entry in payout report
			if ($data['txn_type'] == 'Adaptive Payment PAY')
			{
				$this->addPayout($data['raw_data']['paymentInfoList']['paymentInfo'][1]);
			}
		}
	}

	/**
	 * Function For add payout entry after adaptive payment from paypal
	 *
	 * @param   Array  $data  Data
	 *
	 * @return  boolean
	 */
	public function addPayout($data)
	{
		/* Amol DO no delete it sample response form paypal (second receiver (campaign promoter))
		$data=array(
		'transactionId' => '111J75396ML51041',
		'transactionStatus' => 'COMPLETED',
		'receiver' => Array
		(
		'amount' => '9.78',
		'email' => 'ahghatol@gmail.com',
		'primary' => 'false',
		'paymentType' => 'SERVICE',
		'accountId' => 'K48UJGXSU3S48'
		),
		'refundedAmount' => '0.00',
		'pendingRefund' => 'false',
		'senderTransactionId' => '648606593G164884A',
		'senderTransactionStatus' => 'COMPLETED'
		);*/
		$camp_promoter_email = $data['receiver']['email'];

		if (!$camp_promoter_email)
		{
			return;
		}

		$db    = JFactory::getDBO();
		$query = "SELECT creator_id FROM #__jg_campaigns WHERE paypal_email='" . $camp_promoter_email . "'";
		$db->setQuery($query);
		$camp_promoter_id = $db->loadResult();

		$db    = JFactory::getDBO();
		$query = "SELECT id FROM #__jg_payouts WHERE transaction_id='" . $data['transactionId'] . "'";
		$db->setQuery($query);
		$payout_id = $db->loadResult();

		$res = new stdClass;

		$db = JFactory::getDBO();

		if ($payout_id)
		{
			$res->id = $payout_id;
		}
		else
		{
			$res->id = '';
		}

		$res->user_id        = $camp_promoter_id;
		$res->payee_name     = JFactory::getUser($camp_promoter_id)->name;
		$res->date           = date("Y-m-d H:i:s");
		$res->transaction_id = $data['transactionId'];
		$res->email_id       = $camp_promoter_email;
		$res->amount         = $data['receiver']['amount'];

		if ($data['transactionStatus'] == 'COMPLETED')
		{
			$res->status = 1;
		}
		else
		{
			$res->status = 0;
		}

		$res->ip_address = '';
		$res->type       = 'adaptive_paypal';

		if ($res->id)
		{
			if (!$db->updateObject('#__jg_payouts', $res, 'id'))
			{
			}
		}
		else
		{
			if (!$db->insertObject('#__jg_payouts', $res, 'id'))
			{
			}
		}

		return true;
	}

	/**
	 * Function For _addNewRecurringOrder
	 *
	 * @param   Array  $data          Data
	 * @param   INT    $order_id_key  Order_id_key
	 *
	 * @return  void
	 */
	public function _addNewRecurringOrder($data, $order_id_key)
	{
		$donationsHelper = new donationsHelper;
		$donationInfo    = $donationsHelper->getSingleDonationInfo($order_id_key);

		/*order_id campaign_id  donor_id donation_id fund_holder cdate mdate
		transaction_id transaction_id original_amount amount  fee  status processor ip_address extra
		save order details*/
		$db              = JFactory::getDBO();
		$obj             = new stdClass;
		$obj->id         = '';

		// Lets make a random char for this order take order prefix set by admin
		$obj->id            = '';
		$params             = JComponentHelper::getParams('com_jgive');
		$order_prefix       = (string) $params->get('order_prefix');

		// String length should not be more than 5
		$order_prefix       = substr($order_prefix, 0, 5);

		// Take separator set by admin
		$separator          = (string) $params->get('separator');
		$obj->order_id      = $order_prefix . $separator;

		// Check if we have to add random number to order id
		$use_random_orderid = (int) $params->get('random_orderid');

		if ($use_random_orderid)
		{
			$random_numer = $this->_random(5);
			$obj->order_id .= $random_numer . $separator;
			/* This length shud be such that it matches the column lenth of primary key
			it is used to add pading order_id_column_field_length - prefix_length - no_of_underscores - length_of_random number*/
			$len = (23 - 5 - 2 - 5);
		}
		else
		{
			/*This length shud be such that it matches the column lenth of primary key
			 * it is used to add pading order_id_column_field_length - prefix_length - no_of_underscores*/
			$len = (23 - 5 - 2);
		}

		$obj->campaign_id     = $donationInfo['campaign']->id;
		$obj->donor_id        = $donationInfo['donor']->id;
		$obj->donation_id     = $donationInfo['payment']->donation_id;
		$obj->cdate           = date("Y-m-d H:i:s");
		$obj->mdate           = date("Y-m-d H:i:s");
		$obj->original_amount = $donationInfo['payment']->original_amount;
		$obj->amount          = $donationInfo['payment']->amount;

		// Need To Modify
		$obj->fee = $donationInfo['payment']->fee;

		$obj->fund_holder = $donationInfo['payment']->fund_holder;

		$obj->processor = $donationInfo['payment']->processor;

		// Get the IP Address
		$obj->ip_address = $donationInfo['payment']->ip_address;

		$obj->transaction_id = $data['transaction_id'];
		$obj->status == $data['status'];
		$obj->extra = json_encode($data['raw_data']);

		if (!$db->insertObject('#__jg_orders', $obj, 'id'))
		{
			echo $db->stderr();
		}

		$orders_key = $db->insertid();

		if (!$orders_key)
		{
			return 'Error in saving order details';
		}

		$db->setQuery('SELECT order_id FROM #__jg_orders WHERE id=' . $orders_key);
		$order_id      = (string) $db->loadResult();
		$maxlen        = 23 - strlen($order_id) - strlen($orders_key);
		$padding_count = (int) $params->get('padding_count');

		// Use padding length set by admin only if it is les than allowed(calculate) length
		if ($padding_count > $maxlen)
		{
			$padding_count = $maxlen;
		}

		if (strlen((string) $orders_key) <= $len)
		{
			$append = '';

			for ($z = 0; $z < $padding_count; $z++)
			{
				$append .= '0';
			}

			$append = $append . $orders_key;
		}

		$res           = new stdClass;
		$res->id       = $orders_key;
		$res->order_id = $order_id . $append;

		if (!$db->updateObject('#__jg_orders', $res, 'id'))
		{
		}
	}

	/**
	 * Function For changing Order status
	 *
	 * @return  if 1 = success 2= error 3 = refund order
	 */
	public function changeOrderStatus()
	{
		$returnvaule = 1;
		$data        = JRequest::get('post');

		if (isset($data['notify_chk']))
		{
			$notify_chk = 1;
		}
		else
		{
			$notify_chk = 0;
		}

		if (isset($data['comment']) && $data['comment'])
		{
			$comment = $data['comment'];
		}
		else
		{
			$comment = '';
		}

		$path = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$donationsHelper = new donationsHelper;
		$donationsHelper->updatestatus($data['id'], $data['status'], $comment, $notify_chk);

		if ($status == 'RF')
		{
			$returnvaule = 3;
		}

		return $returnvaule;
	}

	/**
	 * Function deleteDonations For Delete donation record
	 *
	 * @param   INT  $odid  Order id
	 *
	 * @return  boolean
	 */
	public function deleteDonations($odid)
	{
		// Check which orders is recurring
		for ($i = 0; $i < (count($odid)); $i++)
		{
			$q = "SELECT donation_id FROM  #__jg_orders WHERE id=" . $odid[$i];
			$this->_db->setQuery($q);
			$donation_id = $this->_db->loadResult();

			if ($donation_id)
			{
				$q = "SELECT is_recurring FROM  #__jg_donations WHERE id=" . $donation_id;
				$this->_db->setQuery($q);
				$is_recurring = $this->_db->loadResult();

				// For recurring donations delete only order entry
				if ($is_recurring)
				{
					// Delete from  orders
					$query = "DELETE FROM #__jg_orders where id =" . $odid[$i];
					$this->_db->setQuery($query);

					if (!$this->_db->execute())
					{
						$this->setError($this->_db->getErrorMsg());

						return false;
					}
				}

				// Delete donor, Order, donations
				else
				{
					// Delete from donors
					$q = "SELECT donor_id FROM  #__jg_donations WHERE id=" . $donation_id;
					$this->_db->setQuery($q);
					$donor_id = $this->_db->loadResult();

					$query = "DELETE FROM #__jg_donors where id =" . $donor_id;
					$this->_db->setQuery($query);

					if (!$this->_db->execute())
					{
						$this->setError($this->_db->getErrorMsg());

						return false;
					}

					// Delete from  orders
					$query = "DELETE FROM #__jg_orders where id =" . $odid[$i];
					$this->_db->setQuery($query);

					if (!$this->_db->execute())
					{
						$this->setError($this->_db->getErrorMsg());

						return false;
					}

					// Delete from donations
					$query = "DELETE FROM #__jg_donations where id=" . $donation_id;
					$this->_db->setQuery($query);

					if (!$this->_db->execute())
					{
						$this->setError($this->_db->getErrorMsg());

						return false;
					}
				}
			}
			// Support the version of jGive before 1.6 (Recurring payment & One page checkout) release
			else
			{
				// Delete from donors
				$q = "SELECT donor_id FROM  #__jg_orders WHERE id=" . $odid[$i];
				$this->_db->setQuery($q);
				$donor_id = $this->_db->loadResult();

				$query = "DELETE FROM #__jg_donors where id =" . $donor_id;
				$this->_db->setQuery($query);

				if (!$this->_db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}

				// Delete from  orders
				$query = "DELETE FROM #__jg_orders where id =" . $odid[$i];
				$this->_db->setQuery($query);

				if (!$this->_db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}

				// Delete from donations
				$query = "DELETE FROM #__jg_donations where order_id=" . $odid[$i];
				$this->_db->setQuery($query);

				if (!$this->_db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Function _random
	 *
	 * @param   INT  $length  Length
	 *
	 * @return  INT
	 */
	public function _random($length = 17)
	{
		$salt   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$len    = strlen($salt);
		$random = '';

		$stat = @stat(__FILE__);

		if (empty($stat) || !is_array($stat))
		{
			$stat = array(php_uname());
		}

		mt_srand(crc32(microtime() . implode('|', $stat)));

		for ($i = 0; $i < $length; $i++)
		{
			$random .= $salt[mt_rand(0, $len - 1)];
		}

		return $random;
	}

	/**
	 * Function Set minimum amount
	 *
	 * @param   String  $cid  Campaign ID
	 *
	 * @return  row
	 */
	public function getMinimumAmount($cid)
	{
		$query = "SELECT c.minimum_amount
			FROM #__jg_campaigns AS c
			WHERE c.id=" . $cid;
		$this->_db->setQuery($query);

		return $this->_db->loadResult();
	}

	/**
	 * Function For check mail is existing or not
	 *
	 * @param   String  $mail  Mail
	 *
	 * @return  INT
	 */
	public function checkMailExists($mail)
	{
		$mailexist = 0;
		$query     = "SELECT id FROM #__users where email  LIKE '" . $mail . "'";
		$this->_db->setQuery($query);
		$result = $this->_db->loadResult();

		if ($result)
		{
			$mailexist = 1;
		}
		else
		{
			$mailexist = 0;
		}

		return $mailexist;
	}

	/**
	 * Get All Campaigns
	 *
	 * @return  Object list
	 */
	public function getAllCampaigns()
	{
		$query = "SELECT * FROM
			 #__jg_campaigns WHERE published=1";
		$this->_db->setQuery($query);

		return $this->_db->loadObjectlist();
	}

	/**
	 * Get All Users
	 *
	 * @return  Object list
	 */
	public function getAllusers()
	{
		$query = "SELECT * FROM #__users";
		$this->_db->setQuery($query);

		return $this->_db->loadObjectlist();
	}
}
