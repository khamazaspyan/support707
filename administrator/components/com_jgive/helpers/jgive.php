<?php
/**
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2011-2015 Techjoomla. All rights reserved.
 * @license    GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

// No direct access to this file
defined('_JEXEC') or die;

/**
 * Jgive helper class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveHelper
{
	/**
	 * Function addSubmenu.
	 *
	 * @param   string  $vName  The
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public static function addSubmenu($vName = '')
	{
		$cp           = '';
		$campaigns    = '';
		$donations    = '';
		$reports      = '';
		$payoutlayout = '';
		$categories   = '';
		$ending_camp  = '';
		$email_template  = '';
		$donors = '';
		$queue        = JFactory::getApplication()->input->get('layout');

		switch ($vName)
		{
			case 'cp':
				$cp = true;
				break;

			case 'campaigns':
				$campaigns = true;
				break;

			case 'donations':
				$donations = true;
				break;

			case 'ending_camp':
				$ending_camp = true;
				break;

			case 'reports':
				if ($queue == 'payouts' || $queue == 'edit_payout')
				{
					$payoutlayout = true;
				}
				else
				{
					$reports = true;
				}
				break;

			case 'categories':
				$categories = true;
				break;

			case 'email_template':
				$email_template = true;
				break;

			case 'donors':
				$donors = true;
				break;
		}

		$jgive_field_group_view = 'index.php?option=com_tjfields&view=groups&client=com_jgive.campaign';

			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_CP'), 'index.php?option=com_jgive&view=cp&layout=dashboard', $cp);

			// Added categories menu
			JHtmlSidebar::addEntry(
									JText::_('COM_JGIVE_SUBMENU_CATEGORIES'), 'index.php?option=com_categories&view=categories&extension=com_jgive', $categories
			);
			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_CAMPAIGNS'), 'index.php?option=com_jgive&view=campaigns&layout=default', $campaigns);

			$document = JFactory::getDocument();
			$document->addStyleDeclaration('.icon-48-helloworld ' . '{background-image: url(../media/com_helloworld/images/tux-48x48.png);}');

			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_REPORTS'), 'index.php?option=com_jgive&view=reports&layout=default', $reports);
			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_PAYOUT_REPORTS'), 'index.php?option=com_jgive&view=reports&layout=payouts', $payoutlayout);
			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_DONATIONS'), 'index.php?option=com_jgive&view=donations&layout=all', $donations);

			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_CAMPAIGNS_DONORS'), 'index.php?option=com_jgive&view=donors&layout=default', $donors);

			JHtmlSidebar::addEntry(
									JText::_('COM_JGIVE_TITLE_EMAIL_TEMPLATE'), 'index.php?option=com_jgive&view=email_template&layout=email_template',
									$email_template
			);

			// Added by Sneha, new menu for ending campaigns report
			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_END_CAMPAIGNS'), 'index.php?option=com_jgive&view=ending_camp', $ending_camp);

			$field_link = "index.php?option=com_tjfields&view=fields&client=com_jgive.campaign";
			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_CAMPAIGN_FIELD'), $field_link, $vName == 'fields');

			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_JGIVE_CAMPAIGN_FIELDS_GROUP'), $jgive_field_group_view, $vName == 'groups');

			JHtmlSidebar::addEntry(
			JText::_('COM_JGIVE_TITLE_COUNTRIES'), 'index.php?option=com_tjfields&view=countries&client=com_jgive', $vName == 'countries');
			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_TITLE_REGIONS'), 'index.php?option=com_tjfields&view=regions&client=com_jgive', $vName == 'regions');
			JHtmlSidebar::addEntry(JText::_('COM_JGIVE_TITLE_CITIES'), 'index.php?option=com_tjfields&view=cities&client=com_jgive', $vName == 'cities');

		// Load bootsraped filter

		JHtml::_('bootstrap.tooltip');

		if ($vName != 'donations' and $queue != 'paymentform')
		{
			JHtml::_('behavior.multiselect');
			JHtml::_('formbehavior.chosen', 'select');
		}
	}
}
