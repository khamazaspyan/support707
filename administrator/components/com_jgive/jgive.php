<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

// Include dependancies
jimport('joomla.application.component.controller');

if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

jimport('joomla.filesystem.file');
$tjStrapperPath = JPATH_SITE . '/media/techjoomla_strapper/tjstrapper.php';

if (JFile::exists($tjStrapperPath))
{
	require_once $tjStrapperPath;
	TjStrapper::loadTjAssets('com_jgive');
}

$document = JFactory::getDocument();

// Frontend css
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive.css');

// Backend css
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_admin.css');

// Load jgive helper
$jgivehelperPath = JPATH_ADMINISTRATOR . '/components/com_jgive/helpers/jgive.php';

if (!class_exists('JgiveHelper'))
{
	// Require_once $path;
	JLoader::register('JgiveHelper', $jgivehelperPath);
	JLoader::load('JgiveHelper');
}

// Load jgiveFontendHelper
$helperPath = JPATH_SITE . '/components/com_jgive/helper.php';

if (!class_exists('jgiveFrontendHelper'))
{
	// Require_once $path;
	JLoader::register('jgiveFrontendHelper', $helperPath);
	JLoader::load('jgiveFrontendHelper');
}

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

if (!class_exists('campaignHelper'))
{
	JLoader::register('campaignHelper', $helperPath);
	JLoader::load('campaignHelper');
}

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

if (!class_exists('donationsHelper'))
{
	JLoader::register('donationsHelper', $helperPath);
	JLoader::load('donationsHelper');
}

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/reports.php';

if (!class_exists('reportsHelper'))
{
	JLoader::register('reportsHelper', $helperPath);
	JLoader::load('reportsHelper');
}

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/media.php';

if (!class_exists('jgivemediaHelper'))
{
	JLoader::register('jgivemediaHelper', $helperPath);
	JLoader::load('jgivemediaHelper');
}

// Load Global language constants to in .js file
jgiveFrontendHelper::getLanguageConstant();

// Require the base controller
require_once JPATH_COMPONENT . '/controller.php';

// Execute the task.
$controller = JControllerLegacy::getInstance('jgive');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
