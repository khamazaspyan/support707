<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * JgiveControllerCampaign form controller class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JgiveControllerCampaign extends jgiveController
{
	/**
	 * Redirect to create new campaign view
	 *
	 * @return  void
	 */
	public function addNew()
	{
		$input    = JFactory::getApplication()->input;
		$redirect = JRoute::_('index.php?option=com_jgive&view=campaign&layout=create', false);
		$this->setRedirect($redirect, '');
	}

	/**
	 * Saves new campaign
	 *
	 * @return  void
	 */
	public function save()
	{
		// Check token
		JSession::checkToken() or jexit('Invalid Token');
		$session             = JFactory::getSession();
		$app = JFactory::getApplication();

		// Get model
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$model               = $this->getModel('campaign');

		$extra_jform_data = JFactory::getApplication()->input->get('jform', array(), 'array');

		$ccat = JFactory::getApplication()->input->get('campaign_category', '', 'INT');

		// Get model
		$model  = $this->getModel('campaign');

		if (!empty($ccat))
		{
			// Validate the posted data.
			$formExtra = $model->getFormExtra(
			array("category" => $ccat,
					"clientComponent" => 'com_jgive',
					"client" => 'com_jgive.campaign',
					"view" => 'campaign',
					"layout" => 'create')
					);

			$formExtra = array_filter($formExtra);

			// Validate the posted extra data.
			if (!empty($formExtra[0]))
			{
				$extra_jform_data = $model->validateExtra($formExtra[0], $extra_jform_data);
			}

			if (!empty($formExtra[1]))
			{
				$extra_jform_data = $model->validateExtra($formExtra[1], $extra_jform_data);
			}

			// Check for errors.
			if ($extra_jform_data === false)
			{
				// Get the validation messages.
				$errors	= $model->getErrors();

				// Push up to three validation messages out to the user.
				for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
				{
					if ($errors[$i] instanceof Exception)
					{
						$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
					}
					else
					{
						$app->enqueueMessage($errors[$i], 'warning');
					}
				}

				// Save the data in the session.
				// Tweak.
				$app->setUserState('com_jgive.edit.campaign.data', $extra_jform_data);

				// Tweak *important
				$app->setUserState('com_jgive.edit.campaign.id', $extra_jform_data['id']);

				// Redirect back to the edit screen.
				$id = (int) $app->getUserState('com_jgive.edit.campaign.id');

				$this->setRedirect(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $id, false));
			}
		}

		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$campaignhelperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $helperPath);
			JLoader::load('campaignHelper');
		}

		$campaignHelper = new campaignHelper;

		$formattedTime = $campaignHelper->getFormattedTime($post);

		$data = array();

		// Append formatted start time & end time for event to startdate & enddate.
		$data['startdate']  = $post->get('start_date', '', 'STRING') . " " . $formattedTime['campaign_start_time'];
		$data['enddate']    = $post->get('end_date', '', 'STRING') . " " . $formattedTime['campaign_end_time'];
		$start_dt_timestamp = strtotime($data['startdate']);
		$end_dt_timestamp   = strtotime($data['enddate']);

		// Validate if start-date-time <= end-date-time.
		if ($start_dt_timestamp > $end_dt_timestamp)
		{
			// Save the data in the session.
			// Tweak.
			$app->setUserState('com_jgive.edit.campaign.data', $post);

			// Tweak *important
			$app->setUserState('com_jgive.edit.campaign.id', $post->get('cid'));

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_jgive.edit.campaign.id');
			$this->setMessage(JText::_('COM_JGIVE_DATE_ERROR'), 'warning');

			$this->setRedirect(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $id, false));

			return false;
		}

		$result              = $model->saveNewCampaign($extra_jform_data);
		$campaignid          = $session->get('camapign_id');
		$session->set('camapign_id', '');

		if ($result)
		{
			$redirect = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=default', false);
			$msg      = JText::_('COM_JGIVE_CAMPAIGN_SAVED');
		}
		else
		{
			$redirect = JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&id=' . $campaignid, false);
			$msg      = JText::_('COM_JGIVE_CAMPAIGN_ERROR_SAVING');
		}

		$this->setRedirect($redirect, $msg);
	}

	/**
	 * Redirect to campaign edit view
	 *
	 * @return  void
	 */
	public function edit()
	{
		$input = JFactory::getApplication()->input;
		$cid   = $input->get('cid', '', 'INT');

		if ($cid)
		{
			$redirect = JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&id=' . $cid, false);
			$this->setRedirect($redirect, '');
		}
	}

	/**
	 * Saves new campaign
	 *
	 * @return  void
	 */
	public function SaveEditedCampaign()
	{
		$app = JFactory::getApplication();
		JSession::checkToken() or jexit('Invalid Token');

		// Total length of post back data in bytes.
		$contentLength = (int) $_SERVER['CONTENT_LENGTH'];

		// Maximum allowed size of post back data in MB.
		$postMaxSize = (int) ini_get('post_max_size');

		// Maximum allowed size of script execution in MB.
		$memoryLimit = (int) ini_get('memory_limit');

		// Check for the total size of post back data.
		if (($postMaxSize > 0 && $contentLength > $postMaxSize * 1024 * 1024) || ($memoryLimit != -1 && $contentLength > $memoryLimit * 1024 * 1024))
		{
			JError::raiseWarning(100, JText::_('COM_JGIVE_ERROR_WARNUPLOADTOOLARGE'));
		}

		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$campaignhelperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $helperPath);
			JLoader::load('campaignHelper');
		}

		$campaignHelper = new campaignHelper;

		$formattedTime = $campaignHelper->getFormattedTime($post);

		$data = array();

		// Append formatted start time & end time for event to startdate & enddate.
		$data['startdate']  = $post->get('start_date', '', 'STRING') . " " . $formattedTime['campaign_start_time'];
		$data['enddate']    = $post->get('end_date', '', 'STRING') . " " . $formattedTime['campaign_end_time'];
		$start_dt_timestamp = strtotime($data['startdate']);
		$end_dt_timestamp   = strtotime($data['enddate']);

		// Validate if start-date-time <= end-date-time.
		if ($start_dt_timestamp > $end_dt_timestamp)
		{
			// Save the data in the session.
			// Tweak.
			$app->setUserState('com_jgive.edit.campaign.data', $post);

			// Tweak *important
			$app->setUserState('com_jgive.edit.campaign.id', $post->get('cid'));

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_jgive.edit.campaign.id');
			$this->setMessage(JText::_('COM_JGIVE_DATE_ERROR'), 'warning');

			$this->setRedirect(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $id, false));

			return false;
		}

		/*$post                = JRequest::get('post');*/
		$campaignid          = $post->get('cid');
		$jgiveFrontendHelper = new jgiveFrontendHelper;

		$extra_jform_data = JFactory::getApplication()->input->get('jform', array(), 'array');

		// Get model
		$model = $this->getModel('campaign');

		$result = $model->edit($extra_jform_data);

		if ($result)
		{
			$redirect = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=default', false);
			$msg      = JText::_('COM_JGIVE_CAMPAIGN_SAVED');
		}
		else
		{
			$redirect = JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&id=' . $campaignid, false);
			$msg      = JText::_('COM_JGIVE_CAMPAIGN_ERROR_SAVING');
		}

		$this->setRedirect($redirect, $msg);
	}

	/**
	 * Set Success State
	 *
	 * @return  void
	 */
	public function changeSuccessState()
	{
		$post  = JFactory::getApplication()->input->post;
		$model = $this->getModel('campaign');
		$model->setState('request', $post);

		$cid = $post->get('hiddenCid');
		$successStatus = $post->get('hiddenSuccessStatus');

		$result = $model->changeSuccessState($cid, $successStatus);

		if ($result == true)
		{
			$msg = JText::_('COM_JGIVE_MSG_SUCCESS_STATUS_CHANGED_SUCCESS');
		}
		else
		{
			$msg = JText::_('COM_JGIVE_MSG_SUCCESS_STATUS_CHANGED_ERROR');
		}

		$link = 'index.php?option=com_jgive&view=campaigns&layout=default';
		$this->setRedirect($link, $msg);
	}

	/**
	 * Delete video
	 *
	 * @return   boolean
	 *
	 * since 1.7
	 */
	public function deleteVideo()
	{
		$app = JFactory::getApplication();

		// Fetch value from ajax call
		$videoid = $app->input->get('videoid');
		$type    = $app->input->get('type');

		$model = $this->getModel('campaign');
		echo $deleteGalleryVideos = $model->deleteGalleryVideos($videoid, $type);

		jexit();
	}
}
