<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * JgiveControllerReports controller class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveControllerReports extends jgiveController
{
	/**
	 * Method to For redirecting Add payout
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function add()
	{
		$redirect = JRoute::_('index.php?option=com_jgive&view=reports&layout=edit_payout', false);
		$this->setRedirect($redirect);
	}

	/**
	 * Method For save new payout
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function SaveNewPayout()
	{
		$model   = $this->getModel("reports");
		$CSVData = $model->savePayout();

		$redirect = JRoute::_('index.php?option=com_jgive&view=reports&layout=payouts', false);
		$this->setRedirect($redirect);
	}

	/**
	 * Method For edit payout
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function editPayout()
	{
		$model = $this->getModel("reports");
		$model->editPayout();

		$redirect = JRoute::_('index.php?option=com_jgive&view=reports&layout=payouts', false);
		$this->setRedirect($redirect);
	}

	/**
	 * Method For delete payout
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function deletePayouts()
	{
		$input = JFactory::getApplication()->input;
		$view  = $input->get('view', 'reports');

		// Get some variables from the request
		$cid   = $input->get('cid', '', 'array');

		JArrayHelper::toInteger($cid);

		if ($view == "reports")
		{
			$model = $this->getModel('reports');

			if ($model->deletePayouts($cid))
			{
				$msg = JText::sprintf('Payout(s) deleted ', count($cid));
			}
			else
			{
				$msg = $model->getError();
			}

			$this->setRedirect('index.php?option=com_jgive&view=reports&layout=payouts', $msg);
		}
	}

	/**
	 * Method CSVEXPORT
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function csvexport()
	{
		$model         = $this->getModel("reports");
		$CSVData       = $model->getCsvexportData();
		$filename      = JText::_('COM_JGIVE_CAMPAIGNS_WISE_REPORTS') . date("Y-m-d");
		$params        = JComponentHelper::getParams('com_jgive');
		$currency      = $params->get('currency_symbol');
		$csvData       = null;
		$headColumn    = array();
		$headColumn[0] = JText::_('COM_JGIVE_NAME');
		$headColumn[1] = JText::_('COM_JGIVE_CAMPAIGN_USER');
		$headColumn[2] = JText::_('COM_JGIVE_NOF_DONATIONS');
		$headColumn[3] = JText::_('COM_JGIVE_TOTAL_AMOUNT_DONATION');
		$csvData .= implode(";", $headColumn);
		$csvData .= "\n";
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: csv" . date("Y-m-d") . ".csv");
		header("Content-disposition: filename=" . $filename . ".csv");

		if (!empty($CSVData))
		{
			foreach ($CSVData as $data)
			{
				$csvrow    = array();
				$csvrow[0] = '"' . $data['title'] . '"';
				$csvrow[1] = '"' . $data['first_name'] . ' ' . $data['last_name'] . ' ' . $data['paypal_email'] . ' ' . $data['username'] . '"';
				$csvrow[2] = '"' . $data['donations_count'] . '"';
				$csvrow[3] = '"' . $currency . ' ' . $data['total_amount'] . '"';

				$csvData .= implode(";", $csvrow);
				$csvData .= "\n";
			}
		}

		ob_clean();
		echo $csvData . "\n";
		jexit();
		$link = JUri::base() . substr(JRoute::_('index.php??option=com_jgive&view=reports&layout=default', false), strlen(JUri::base(true)) + 1);
		$this->setRedirect($link);
	}

	/**
	 * Method CSVEXPORT PAYOUT
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function csvexportpayouts()
	{
		$model    = $this->getModel("reports");
		$CSVData  = $model->getCsvexportData();
		$filename = JText::_('COM_JGIVE_PAYOUT_REPORTS') . date("Y-m-d");
		$params   = JComponentHelper::getParams('com_jgive');
		$currency = $params->get('currency_symbol');
		$csvData  = null;

		$headColumn    = array();
		$headColumn[0] = JText::_('COM_JGIVE_NUMBER');
		$headColumn[1] = JText::_('COM_JGIVE_PAYOUT_ID');
		$headColumn[2] = JText::_('COM_JGIVE_PAYEE_NAME');
		$headColumn[3] = JText::_('COM_JGIVE_TRANSACTION_ID');
		$headColumn[4] = JText::_('COM_JGIVE_PAYOUT_DATE');
		$headColumn[5] = JText::_('COM_JGIVE_PAYMENT_STATUS');
		$headColumn[6] = JText::_('COM_JGIVE_PAYOUT_AMOUNT');

		$csvData .= implode(";", $headColumn);
		$csvData .= "\n";
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: csv" . date("Y-m-d") . ".csv");
		header("Content-disposition: filename=" . $filename . ".csv");

		if (!empty($CSVData))
		{
			foreach ($CSVData as $data)
			{
				$csvrow    = array();
				$csvrow[0] = '"' . $data['title'] . '"';
				$csvrow[1] = '"' . $data['id'] . '"';
				$csvrow[2] = '"' . $data['payee_name'] . '"';
				$csvrow[3] = '"' . $data['transaction_id'] . '"';
				$date = JHtml::_('date', $payout->date, "d-m-Y");

				$csvrow[4] = '"' . $date . '"';

				if ($data['status'])
				{
					$status = JText::_('COM_JGIVE_PAID');
				}
				else
				{
					$status = JText::_('COM_JGIVE_NOT_PAID');
				}

				$csvrow[5] = '"' . $status . '"';
				$csvrow[6] = '"' . $currency . ' ' . $data['amount'] . '"';

				$csvData .= implode(";", $csvrow);
				$csvData .= "\n";
			}
		}

		ob_clean();
		echo $csvData . "\n";
		jexit();
		$link = JUri::base() . substr(JRoute::_('index.php?option=com_jgive&view=reports&layout=default', false), strlen(JUri::base(true)) + 1);
		$this->setRedirect($link);
	}
}
