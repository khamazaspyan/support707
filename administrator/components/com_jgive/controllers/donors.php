<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

use Joomla\Utilities\ArrayHelper;

/**
 * Donors list controller class.
 *
 * @since  1.6
 */
class JGiveControllerDonors extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    Optional. Model name
	 * @param   string  $prefix  Optional. Class prefix
	 * @param   array   $config  Optional. Configuration array for model
	 *
	 * @return  object	The Model
	 *
	 * @since    1.6
	 */
	public function getModel($name = 'donors', $prefix = 'JGiveModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to redirect for Mass mailing
	 *
	 * @return  void
	 *
	 * @since   3.0
	 */
	public function redirectToMassmailing()
	{
		if (!class_exists('JgiveControllerDonations'))
		{
			JLoader::register('JgiveControllerDonations', JPATH_ADMINISTRATOR . '/components/com_jgive/controllers/donations.php');
			JLoader::load('JgiveControllerDonations');
		}

		$jgiveControllerDonations = new JgiveControllerDonations;

		$jgiveControllerDonations->redirectToMassmailing();
	}
}
