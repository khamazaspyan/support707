<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();
/**
 * Donations controller class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveControllerDonations extends jgiveController
{
	/**
	 * Method Save whatever status site admin set
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function save()
	{
		$model = $this->getModel('donations');
		$emailmodel = $this->getModel('email_template');

		$post  = JRequest::get('post');

		$model->setState('request', $post);
		$result = $model->changeOrderStatus();

		// If donation status is C (confirm) then it forward for donation receipt
		if ($post['status'] == 'C')
		{
			$emailmodel->generateDonationReceipt($post['id']);
		}

		if ($result == 1)
		{
			$msg = JText::_('COM_JGIVE_SAVING_MSG');
		}
		elseif ($result == 3)
		{
			$msg = JText::_('COM_JGIVE_REFUND_SAVING_MSG');
		}
		else
		{
			$msg = JText::_('COM_JGIVE_ERROR_SAVING_MSG');
		}

		$link       = 'index.php?option=com_jgive&view=donations&layout=all';

		// Added by sagar for custom project This is trigger  when status changed
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');

		// Call the plugin and get the result
		$result = $dispatcher->trigger('OnAfterJGivePaymentUpdate', array($post['id']));

		// Added by sagar for custom project This is trigger  when status changed
		$this->setRedirect($link, $msg);
	}

	/**
	 * Method Export order payment stats into a csv file
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function payment_csvexport()
	{
		// Load language file for plugin frontend
		$db =& JFactory::getDBO();
		$query = "SELECT i.id, i.first_name, i.last_name, i.email, i.user_id,i.cdate,i.transaction_id,i.processor,i.order_tax,
		i.order_tax_details,i.order_shipping,i.order_shipping_details,
		i.amount,i.status,i.ip_address
		FROM  #__jg_orders AS i
		ORDER BY i.id";

		$db->setQuery($query);
		$results = $db->loadObjectList();

		$csvData = null;
		$csvData .= "Order_Id,Order_Date,User_Name,User_IP,Order_Tax, Order_Tax_details,Order_Shipping,Order_Shipping_details,Order_Amount,
		Order_Status,Payment_Gateway,Cart_Items,billing_email,billing_first_name,
		billing_last_name,billing_phone,billing_address,billing_city,billing_state,
		billing_country_name,billing_postal_code,shipping_email,shipping_first_name,
		shipping_last_name,shipping_phone,shipping_address,shipping_city,shipping_state,
		shipping_country_name,shipping_postal_code";

		$csvData .= "\n";
		$filename = "Donations_" . date("Y-m-d_H-i", time());
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: csv" . date("Y-m") . ".csv");
		header("Content-disposition: filename=" . $filename . ".csv");

		foreach ($results as $result)
		{
			if (($result->id))
			{
				$csvData .= '"' . $result->id . '"' . ',' . '"' .
									$result->cdate . '"' . ',' . '"' .
									JFactory::getUser($result->user_info_id)->username . '"' . ',' . '"' .
									$result->ip_address . '"' . ',' . '"' .
									$result->order_tax . '"' . ',' . '"' .
									str_replace(",", ";", $result->order_tax_details) . '"' . ',' . '"' .
									$result->order_shipping . '"' . ',' . '"' .
									str_replace(",", ";", $result->order_shipping_details) . '"' . ',' . '"' .
									$result->amount . '"' . ',';

				switch ($result->status)
				{
					case 'C':
						$orderstatus = JText::_('COM_JGIVE_CONFIRMED');
						break;
					case 'RF':
						$orderstatus = JText::_('COM_JGIVE_REFUND');
						break;
					case 'P':
						$orderstatus = JText::_('COM_JGIVE_PENDING');
						break;
				}

				$query = "SELECT count(order_item_id) FROM #__jg_order_item WHERE order_id =" . $result->id;
				$db->setQuery($query);
				$cart_items = $db->loadResult();
				$csvData .= '"' . $orderstatus . '"' . ',' . '"' . $result->processor . '"' . ',' . '"' . $cart_items . '"' . ',';

				$query = "SELECT ou.* FROM #__jg_users as ou WHERE ou.address_type='BT' AND ou.user_id =" . $result->user_info_id;
				$db->setQuery($query);
				$billin = $db->loadObject();
				$csvData .= '"' . $result->user_email . '"' . ',' . '"' .
									$result->firstname . '"' . ',' . '"' .
									$result->lastname . '"' . ',' . '"' .
									$result->phone . '"' . ',' . '"' .
									$result->address . '"' . ',' . '"' .
									$result->city . '"' . ',' . '"' .
									$result->state_code . '"' . ',' . '"' .
									$result->country_code . '"' . ',' . '"' .
									$result->zipcode . '"' . ',';

								$csvData .= "\n";
			}
		}

		ob_clean();
		print $csvData;

		exit();
	}

	/**
	 * Method cancel
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function cancel()
	{
		$msg = JText::_('COM_JGIVE_CANCEL_MSG');
		$this->setRedirect('index.php?option=com_jgive', $msg);
	}

	/**
	 * Method loadprofiledata
	 *
	 * @return  json
	 *
	 * @since   1.8.1
	 */
	public function loadprofiledata()
	{
		$compaignuserid = JFactory::getApplication()->input->get('compaignuserid');
		$path           = JPATH_ADMINISTRATOR . '/components/com_jgive/helpers/integrations.php';

		if (!class_exists('JgiveIntegrationsHelper_backend'))
		{
			JLoader::register('JgiveIntegrationsHelper_backend', $path);
			JLoader::load('JgiveIntegrationsHelper_backend');
		}

		$params = JComponentHelper::getParams('com_jgive');

		$profile_import = $params->get('profile_import');

		// If profie import is on the call profile import function
		if ($profile_import)
		{
			$JgiveIntegrationsHelper_backend = new JgiveIntegrationsHelper_backend;
			$profiledata   = $JgiveIntegrationsHelper_backend->profileImport(1, $compaignuserid);

			if (!empty($profiledata))
			{
				unset($profiledata['campaign']);

				if (!empty($profiledata))
				{
				}

				echo json_encode($profiledata);
				jexit();
			}
		}
	}

	/**
	 * Method placeOrder used for submit donation
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function placeOrder()
	{
		$path = JPATH_ADMINISTRATOR . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donations_backendHelper'))
		{
			JLoader::register('donations_backendHelper', $path);
			JLoader::load('donations_backendHelper');
		}

		$redirect_url            = JRoute::_('index.php?option=com_jgive&view=donations');
		$input                   = JFactory::getApplication()->input;
		$post                    = $input->post;
		$donations_backendHelper = new donations_backendHelper;

		$res = $donations_backendHelper->addOrder($post);

		$session = JFactory::getSession();

		if ($session->get('JGIVE_order_id'))
		{
			$payment_plg         = $session->get('payment_plg');
			$itemid              = $input->get('Itemid', 0);
			$orderid             = $session->get('JGIVE_order_id');
			$data['success_msg'] = JText::_('COM_JGIVE_ORDER_CREATED_SUCCESS');
			$data['success']     = 1;
			$data['order_id']    = $orderid;
		}
		else
		{
			$data['success_msg']  = JText::_('COM_JGIVE_ORDER_CREATED_FAILED');
			$data['success']      = 0;
			$data['redirect_uri'] = $redirect_url;
		}

		$link = 'index.php?option=com_jgive&view=donations&layout=all';
		$this->setRedirect($link, $msg);
	}

	/**
	 * Method For Adding new donation Redirect to Donation Form
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function addNewDonation()
	{
		$link = 'index.php?option=com_jgive&view=donations&layout=paymentform';
		$this->setRedirect($link, $msg);
	}

	/**
	 * Method For Cancel Order
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function cancelorder()
	{
		$link = 'index.php?option=com_jgive&view=donations&layout=all';
		$this->setRedirect($link, $msg);
	}

	/**
	 * Method For Getting GiveBack against campaign
	 *
	 * @return  json
	 *
	 * @since   1.8.1
	 */
	public function getGiveBackAgainstCampaign()
	{
		$input = JFactory::getApplication()->input;
		$post  = $input->post;
		$cid   = $post->get('cid', '', 'INT');

		$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $helperPath);
			JLoader::load('campaignHelper');
		}

		$campaignHelper = new campaignHelper;

		$result = $campaignHelper->getCampaignGivebacks($cid);
		echo json_encode($result);
		jexit();
	}

	/**
	 * Method For CSV Report of Donations
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function donationsCsvexport()
	{
		$input  = JFactory::getApplication()->input;
		$post   = $input->post;
		$params = JComponentHelper::getParams('com_jgive');
		$model  = $this->getModel('donations');

		$campaign_cat = $post->get('campaign_cat', '', 'STRING');
		$result       = $model->donationsCsvexport($campaign_cat);

		$filename = JText::_('COM_JGIVE_DONATIONS_CSV_EXPORT_FILE_NAME') . date("Y-m-d");
		$currency = $params->get('currency_symbol');
		$csvData  = null;

		$headColumn     = array();
		$headColumn[0]  = JText::_('COM_JGIVE_NAME');
		$headColumn[1]  = JText::_('COM_JGIVE_HIDE_EMAIL');
		$headColumn[2]  = JText::_('COM_JGIVE_DONATION_AMOUNT');
		$headColumn[3]  = JText::_('COM_JGIVE_COMMISSION_AMOUNT');
		$headColumn[4]  = JText::_('COM_JGIVE_DONATION_DATE');
		$headColumn[5]  = JText::_('COM_JGIVE_BACK_ID');
		$headColumn[6]  = JText::_('COM_JGIVE_ORDER_GIVEBACK_DESC');
		$headColumn[7]  = JText::_('COM_JGIVE_ADDRESS');
		$headColumn[8]  = JText::_('COM_JGIVE_ADDRESS2');
		$headColumn[9]  = JText::_('COM_JGIVE_CITY');
		$headColumn[10] = JText::_('COM_JGIVE_STATE');
		$headColumn[11] = JText::_('COM_JGIVE_COUNTRY');
		$headColumn[12] = JText::_('COM_JGIVE_POSTAL_CODE');

		$csvData .= implode(";", $headColumn);
		$csvData .= "\n";
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: csv" . date("Y-m-d") . ".csv");
		header("Content-disposition: filename=" . $filename . ".csv");

		if (!empty($result))
		{
			foreach ($result as $data)
			{
				$csvrow    = array();
				$csvrow[0] = '"' . $data->first_name . ' ' . $data->last_name . '"';
				$csvrow[1] = '"' . $data->email . '"';
				$csvrow[2] = '"' . $currency . ' ' . $data->amount . '"';
				$csvrow[3] = '"' . $currency . ' ' . $data->fee . '"';
				$cdate     = JFactory::getDate($data->cdate)->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
				$csvrow[4] = '"' . $cdate . '"';

				if ($data->giveback_id != 0)
				{
					$csvrow[5] = '"' . $data->giveback_id . '"';
				}
				else
				{
					$csvrow[5] = '"' . JText::_('COM_JGIVE_NO') . '"';
				}

				$city = $data->city;

				if (!$city)
				{
					$city = $data->othercity;
				}

				$csvrow[6]  = '"' . $data->giveback_desc . '"';
				$csvrow[7]  = '"' . $data->address . '"';
				$csvrow[8]  = '"' . $data->address2 . '"';
				$csvrow[9]  = '"' . $city . '"';
				$csvrow[10] = '"' . $data->state . '"';
				$csvrow[11] = '"' . $data->country . '"';
				$csvrow[12] = '"' . $data->zip . '"';

				$csvData .= implode(";", $csvrow);
				$csvData .= "\n";
			}
		}

		ob_clean();
		echo $csvData . "\n";
		jexit();

		$link = JUri::base() . substr(JRoute::_('index.php?option=com_jgive&view=reports&layout=default', false), strlen(JUri::base(true)) + 1);
		$this->setRedirect($link);
	}

	/**
	 * Method For Delete Donation
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function deleteDonations()
	{
		$model      = $this->getModel('donations');
		$post       = JRequest::get('post');
		$donationid = $post['cid'];

		if ($model->deleteDonations($donationid))
		{
			$msg = JText::_('COM_JGIVE_DONATION_DELETED');
		}
		else
		{
			$msg = JText::_('COM_JGIVE_ERR_DONATION_DELETED');
		}

		$this->setRedirect(JUri::base() . "index.php?option=com_jgive&view=donations", $msg);
	}

	/**
	 * Method For redirecting on mass mailing layout
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function redirectToMassmailing()
	{
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;
		$post      = $input->post;

		// Get Donation ids
		$cids    = $input->get('cid', '', 'POST', 'ARRAY');

		$session = JFactory::getSession();
		$session->set('selected_donations_ids', $cids);
		$contact_ink = JRoute::_(JUri::base() . 'index.php?option=com_jgive&view=donations&layout=mass_mailing');

		// Redirect to Mass mailing layout by passing $contact_link
		$mainframe->redirect($contact_ink);
	}

	/**
	 * Method For sending email to selected email id
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function emailToSelected()
	{
		$mainframe     = JFactory::getApplication();
		$input         = $mainframe->input;
		$selected_emails  = $input->get('selected_emails', '', 'POST', 'STRING');
		$subject       = $input->get('jgive_subject', '', 'POST', 'STRING');
		$body          = JRequest::getVar('jgive_message', '', 'post', 'string', JREQUEST_ALLOWHTML);
		$img_path      = 'img src="' . JUri::root();

		$res           = new stdClass;
		$res->content  = str_replace('img src="' . JUri::root(), 'img src="', $body);
		$res->content  = str_replace('img src="', $img_path, $res->content);
		$res->content  = str_replace("background: url('" . JUri::root(), "background: url('", $res->content);
		$res->content  = str_replace("background: url('", "background: url('" . JUri::root(), $res->content);

		$emails = explode(",", $selected_emails);

		$email_id = array_unique($emails);

		$model = $this->getModel('donations');
		$msg = JText::_('COM_JGIVE_EMAIL_SUCCESSFUL');

		$result = $model->emailtoSelected($email_id, $subject, $body, $attachmentPath = '');

		if ($result == 1)
		{
			$msg = JText::_('COM_JGIVE_EMAIL_SUCCESSFUL');
		}
		else
		{
			$msg = $model->getError();
		}

		$contact_ink = JRoute::_(JUri::base() . 'index.php?option=com_jgive&view=donations&layout=all');
		$mainframe->redirect($contact_ink, $msg);
	}

	/**
	 * Method For Cancel email sending redirect to donation list view all layout
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function cancelEmail()
	{
		$link = 'index.php?option=com_jgive&view=donations&layout=all';
		$this->setRedirect($link);
	}
}
