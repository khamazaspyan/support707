<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

jimport('joomla.application.component.controlleradmin');
jimport('joomla.filesystem.file');

// Load jgive list Controller for list views
require_once __DIR__ . '/jgivelist.php';

require_once JPATH_ADMINISTRATOR . '/components/com_jgive/models/campaign.php';

/**
 * Countries list controller class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.7
 */
class JgiveControllerCampaigns extends JgiveControllerJgivelist
{
	/**
	 * Create New Campaign
	 *
	 * @return void
	 *
	 * @since 1.6
	 */
	public function addNew()
	{
		$redirect = JRoute::_('index.php?option=com_jgive&view=campaign&layout=create', false);
		$this->setRedirect($redirect, $msg);
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The name of the model.
	 * @param   string  $prefix  The prefix for the PHP class name.
	 * @param   array   $config  A named array of configuration variables.
	 *
	 * @return  JModel
	 *
	 * @since   1.7
	 */
	public function getModel($name = 'Campaign', $prefix = 'JgiveModel', $config = array('ignore_request' => true))
	{
		// Added by Nidhi
		$post = JRequest::get('post');

		if ($post['task'] == 'campaigns.delete')
		{
			$this->deleteThumbs($post);
		}

		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Proxy for getModel. - Added by Nidhi
	 *
	 * @param   Array  $postdata  A named array of configuration variables.
	 *
	 * @return  JModel
	 *
	 * @since   1.7
	 */
	public function deleteThumbs($postdata)
	{
		$db    = JFactory::getDBO();
		$input = JFactory::getApplication()->input;
		$pks   = $postdata['cid'][0];

		if ($pks)
		{
			$query = "SELECT thumb_path,path FROM #__jg_campaigns_media WHERE content_id=" . $pks;
			$db->setQuery($query);
			$galleryVideoToDelPath = $db->loadObjectlist();

			// Delete product data from TJ - Fields srart
			if (!class_exists('JgiveModelCampaign'))
			{
				JLoader::register('JgiveModelCampaign', JPATH_SITE . '/components/com_jgive/models/campaign.php');
				JLoader::load('JgiveModelCampaign');
			}

			$JgiveModelCampaign = new JgiveModelCampaign;
			$JgiveModelCampaign->deleteExtraFieldsData($pks, 'com_jgive.campaign');

			// Delete image physically
			if (!empty($galleryVideoToDelPath))
			{
				$this->deleteFile($galleryVideoToDelPath);

				// Delete image from db
				$query = "DELETE FROM  #__jg_campaigns_media WHERE  content_id=" . $pks;
				$db->setQuery($query);

				if (!$db->execute())
				{
					echo $db->stderr();

					return false;
				}
			}
		}
	}

	/**
	 * To delete actual file  - Added by Nidhi
	 *
	 * @param   Array  $galleryVideoToDelPath  A named array of thumbnail and video file path
	 *
	 * @return  JModel
	 *
	 * @since   1.7
	 */
	public function deleteFile($galleryVideoToDelPath)
	{
		// If to delete video file then we will need to specify video file location but in-case of image full location we are getting from db itself
		$thumbpath = JPATH_SITE . $galleryVideoToDelPath->thumb_path;
		$videopath = JPATH_SITE . $galleryVideoToDelPath->path;

		if (JFile::exists($thumbpath) && JFile::exists($videopath))
		{
			JFile::delete($thumbpath);
			JFile::delete($videopath);
		}
	}
}
