<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

/**
 * JgiveControllerEnding_camp controller class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveControllerEnding_Camp extends jgiveController
{
	/**
	 * Method CSVExport
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function csvexport()
	{
		$model    = $this->getModel("ending_camp");
		$CSVData  = $model->getCsvexportData();
		$filename = "EndingCampReport_" . date("Y-m-d");
		$params   = JComponentHelper::getParams('com_jgive');
		$currency = $params->get('currency_symbol');
		$csvData  = null;

		$headColumn    = array();
		$headColumn[0] = JText::_('COM_JGIVE_CAMPAIGN_DETAILS');
		$headColumn[1] = JText::_('COM_JGIVE_START_DATE');
		$headColumn[2] = JText::_('COM_JGIVE_END_DATE');
		$headColumn[3] = JText::_('COM_JGIVE_GOAL_AMOUNT');
		$headColumn[4] = JText::_('COM_JGIVE_AMOUNT_RECEIVED');
		$headColumn[5] = JText::_('COM_JGIVE_DONORS');
		$headColumn[7] = JText::_('COM_JGIVE_ID');

		$csvData .= implode(";", $headColumn);
		$csvData .= "\n";
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: csv" . date("Y-m-d") . ".csv");
		header("Content-disposition: filename=" . $filename . ".csv");

		if (!empty($CSVData))
		{
			foreach ($CSVData as $data)
			{
				$csvrow              = array();
				$csvrow[0]           = '"' . $data['title'] . '"';
				$sdate               = JFactory::getDate($data['start_date'])->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
				$csvrow[1]           = '"' . $sdate . '"';
				$end_date            = JFactory::getDate($data['end_date'])->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
				$csvrow[2]           = '"' . $end_date . '"';
				$jgiveFrontendHelper = new jgiveFrontendHelper;
				$csvrow[3]           = '"' . $currency . ' ' . $data['goal_amount'] . '"';
				$campaignHelper      = new campaignHelper;
				$amounts             = $campaignHelper->getCampaignAmounts($data['id']);
				$csvrow[4]           = '"' . $currency . ' ' . $amounts['amount_received'] . '"';
				$csvrow[5]           = '"' . $data['donor_count'] . '"';
				$csvrow[7]           = '"' . $data['id'] . '"';
				$csvData .= implode(";", $csvrow);
				$csvData .= "\n";
			}
		}

		ob_clean();
		echo $csvData . "\n";
		jexit();
		$link = JUri::base() . substr(JRoute::_('index.php?option=com_jgive&view=ending_camp', false), strlen(JUri::base(true)) + 1);
		$this->setRedirect($link);
	}
}