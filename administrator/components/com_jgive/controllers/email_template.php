<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

/**
 * Class for JGive Email Template Controller
 *
 * @package  Jgive
 *
 * @since    1.8.5
 */
class JgiveControllerEmail_Template extends JControllerLegacy
{
	/**
	 * Method Save a email template structure
	 *
	 * @return void
	 */
	public function save()
	{
		$model	= $this->getModel('email_template');

		if ($model->store())
		{
			$msg = JText::_('COM_JGIVE_MENU_ITEM_SAVED');
		}
		else
		{
			$msg = JText::_('COM_JGIVE_ERROR_SAVING_MENU_ITEM');
		}

		$this->setRedirect('index.php?option=com_jgive&view=email_template&layout=email_template');
	}
}
