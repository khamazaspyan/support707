<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

// Load frontend donations view - layout all
$document = JFactory::getDocument();
JToolBarHelper::DeleteList(JText::_('COM_JGIVE_DELETE_CONFRIM'),'remove','JTOOLBAR_DELETE');

$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$saveOrder = $listOrder == 'a.ordering';

$core_js = JUri::root().'media/system/js/core.js';
$flg=0;
foreach($document->_scripts as $name=>$ar)
{
	if($name == $core_js )
	$flg=1;
}

if ($flg==0)
	echo "<script type='text/javascript' src='".$core_js."'></script>";

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$campaignHelper =new campaignHelper();


$js_joomla16 ="Joomla.submitbutton = function(prm)
{

		window.location = 'index.php?option=com_jgive&view=campaigns&layout=default';

}";
$document->addScriptDeclaration($js_joomla16);
?>

<?php
	if($this->issite)
	{
		?>
		<div class="well" >
			<div class="alert alert-error">
				<span ><?php echo JText::_('COM_JGIVE_NO_ACCESS_MSG'); ?> </span>
			</div>
		</div>
		</div><!-- eoc akeeba-bootstrap -->
		<?php
			return false;
	}
	?>

	<?php
	if($this->issite)
	{
		?>
		<!--page header-->
		<div class="componentheading">
			<?php echo JText::_('COM_JGIVE_ALL_CAMPAIGNS');?>
		</div>
		<hr/>
		<?php
	}
	?>

	<form action="index.php" method="post" name="adminForm" id="adminForm">

		<div class="btn-group pull-right hidden-phone">
			<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
			<?php echo $this->pagination->getLimitBox(); ?>
		</div>

	<?php $tblclass='table table-striped';?>
	<table class="<?php echo $tblclass; ?>" width="100%">
		<thead>
			<tr>
				<th class='left'>
					<?php echo JHtml::_('grid.sort', 'COM_JGIVE_CAMPAIGN_DETAILS', 'a.title', $listDirn, $listOrder); ?>
				</th>

				<th class="center hidden-phone">
					<?php echo JHtml::_('grid.sort', 'COM_JGIVE_START_DATE', 'a.start_date', $listDirn, $listOrder); ?>
				</th>

				<th class="center hidden-phone">
					<?php echo JHtml::_('grid.sort', 'COM_JGIVE_END_DATE', 'a.end_date', $listDirn, $listOrder); ?>
				</th>

				<th class='left hidden-phone'>
					<?php echo JHtml::_('grid.sort', 'COM_JGIVE_GOAL_AMOUNT', 'a.goal_amount', $listDirn, $listOrder); ?>
				</th>

				<th class='left hidden-phone'>
					<?php echo JHtml::_('grid.sort', 'COM_JGIVE_AMOUNT_RECEIVED', 'a.goal_amount', $listDirn, $listOrder); ?>
				</th>

				<th class='left hidden-phone'>
					<?php echo JText::_('COM_JGIVE_DONORS'); ?>
				</th>

				<?php if (isset($this->items[0]->id)): ?>
					<th width="1%" class="nowrap center hidden-phone">
						<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
					</th>
				<?php endif; ?>
			</tr>
	</thead>
			<?php

			if(!empty($this->items))
			{
				$i=1;
				$j=0;
				$k=0;
				foreach($this->items as $camp_data)
				{
					$data=$camp_data;
					$row=$data;
					$published=JHtml::_('jgrid.published',$row->published,$j);
					?>
					<tr class="row<?php echo $j % 2;?>">
						<td>
							<div>
								<a class="pointer"  onclick="window.parent.SelectCompaign(<?php echo $data->id ?>, '<?php echo addslashes($data->title); ?>')" title="<?php echo JText::_('COM_JGIVE_CLICK_TO_VIEW_CAMP_TOOLTIP');?>">
									<?php echo $data->title;?>
								</a>
							</div>
							<div class="com_jgive_clear_both"></div>
						</td>
						<!-- categories -->

						<td><?php echo JFactory::getDate($data->start_date)->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));	?></td>

						<td><?php echo JFactory::getDate($data->end_date)->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));	?></td>

						<td><?php echo $this->jgiveFrontendHelper->getFormattedPrice($data->goal_amount); ?></td>

						<td><?php echo $this->jgiveFrontendHelper->getFormattedPrice($data->amount_received); ?></td>

						<td><?php echo $data->donor_count;?></td>

						<td><?php echo $data->id;?></td>
					</tr>
					<?php
					$i++;
					$j++;
					$k++;
				}

			}
			else{
				?>
				<tr>
					<td colspan="11">
						<?php echo JText::_('COM_JGIVE_NO_DATA');?>
					</td>
				</tr>
				<?php
			}

			if(!$this->issite)
			{
				?>
				<tr>
					<?php $class_pagination='';?>
					<td colspan="11" class="com_jgive_align_center <?php echo $class_pagination; ?> ">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				<?php
			}
			?>


		</table>

		<?php
		if($this->issite)
		{
			?>
			<?php $class_pagination='';?>
			<div class="<?php echo $class_pagination; ?> com_jgive_align_center">
				<?php echo $this->pagination->getListFooter(); ?>
			</div>
			<?php
		}
		?>

		<input type="hidden" name="option" value="com_jgive" />
		<input type="hidden" name="view" value="campaigns" />
		<input type="hidden" name="layout" value="default" />
		<input type="hidden" name="filter_order" value="<?php echo $this->lists['filter_order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['filter_order_Dir']; ?>" />
		<input type="hidden" name="defaltevent" value="<?php echo $this->lists['filter_campaign_cat'];?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="task" value="" />

	</form>
</div>


