<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

jimport('joomla.application.component.view');

/**
 * View class for a list of cities.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.7
 */

class JgiveViewCampaigns extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display ($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		$user      = JFactory::getUser();

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jgive'))
		{
			$mainframe->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		$this->state 		 = $this->get('State');
		$this->items 		 = $this->get('Items');
		$this->pagination 	 = $this->get('Pagination');

		// $this->filterForm 	 = $this->get('FilterForm');

		$this->activeFilters = $this->get('ActiveFilters');
		$this->input 		 = JFactory::getApplication()->input;

		// Declare helpers objects
		$this->jgiveFrontendHelper = new jgiveFrontendHelper;
		$this->campaignHelper = new campaignHelper;

		// Get campaign status
		$this->campaignSuccessStatus = $this->campaignHelper->getCampaignSuccessStatusArray();

		// Get campaign itemid
		$this->singleCampaignItemid = $this->jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		// Add submenu
		JgiveHelper::addSubmenu('campaigns');

		$this->publish_states = array(
			'' => JText::_('JOPTION_SELECT_PUBLISHED'),
			'1'  => JText::_('JPUBLISHED'),
			'0'  => JText::_('JUNPUBLISHED')
		);

		$this->campaign_type = array(
			'' => JText::_('COM_JGIVE_FILTER_SELECT_TYPE'),
			'donation'  => JText::_('COM_JGIVE_CAMPAIGN_TYPE_DONATION'),
			'investment'  => JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT')
		);

		$this->org_ind_type = $this->campaignHelper->organization_individual_type();

		$filter_order_Dir = $mainframe->getUserStateFromRequest('com_jgive.filter_order_Dir', 'filter_order_Dir', 'desc', 'word');
		$lists['filter_order_Dir'] = $filter_order_Dir;

		$this->issite = 0;

		$this->sidebar = JHtmlSidebar::render();
		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   1.7
	 */
	protected function getSortFields ()
	{
		return array(
			'a.ordering' => JText::_('COM_JGIVE_ORDERING'),
			'a.published' => JText::_('COM_JGIVE_PUBLISHED'),
			'a.title' => JText::_('COM_JGIVE_TITLE'),
			'a.start_date' => JText::_('COM_JGIVE_START_DATE'),
			'a.end_date' => JText::_('COM_JGIVE_END_DATE'),
			'a.goal_amount' => JText::_('COM_JGIVE_GOAL_AMOUNT'),
			'a.featured' => JText::_('COM_JGIVE_FEATURED'),
			'a.success_status' => JText::_('COM_JGIVE_SUCCESS_STATUS'),
			'a.id' => JText::_('COM_JGIVE_ID')
		);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT . '/helpers/jgive.php';

		$state      = $this->get('State');
		$user       = JFactory::getUser();
		$canCreate  = $user->authorise('core.create', 'com_jgive');
		$canEdit    = $user->authorise('core.edit', 'com_jgive');
		$canCheckin = $user->authorise('core.manage', 'com_jgive');
		$canChange  = $user->authorise('core.edit.state', 'com_jgive');
		$canDelete  = $user->authorise('core.delete', 'com_jgive');

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_CAMPAIGNS'), 'list');

		if ($canCreate)
		{
			// Add buttons on toolbar
			JToolbarHelper::addNew('campaign.addNew');
		}

		JToolbarHelper::custom('campaigns.featured', 'featured.png', 'featured_f2.png', 'JFEATURED', true);
		JToolBarHelper::custom('campaigns.unfeatured', 'star-empty', '', 'JUNFEATURED');

		if ($canChange)
		{
			JToolbarHelper::publish('campaigns.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('campaigns.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		}

		if ($canDelete)
		{
			JToolbarHelper::deleteList('', 'campaigns.delete', 'COM_JGIVE_TOOLBAR_DELETE');
		}

		JHtml::_('bootstrap.modal', 'collapseModal');
		JToolbarHelper::preferences('com_jgive');
	}
}
