<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

JToolBarHelper::preferences('com_jgive');
$params = JComponentHelper::getParams('com_jgive');

// Load frontend donations view - layout all
$document = JFactory::getDocument();

// Added by Sneha to hide unwanted filters
$menuCssOverrideJs = "jQuery(document).ready(function(){
jQuery('#filter_campaign_type_chzn').hide();
jQuery('#filter_org_ind_type_chzn').hide();
});";

$document->addScriptDeclaration($menuCssOverrideJs);

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$campaignHelper = new campaignHelper;
?>
<script type="text/javascript">
	function jgive_action()
	{
		document.getElementById('task').value=" ";
		Joomla.submitform();
	}
</script>
<?php
	if ($this->issite)
	{
	?>
		<div class="well" >
			<div class="alert alert-error">
				<span ><?php echo JText::_('COM_JGIVE_NO_ACCESS_MSG'); ?> </span>
			</div>
		</div>
</div>
		<?php
		return false;
	}?>

	<?php
	if ($this->issite)
	{
	?>
		<div class="componentheading">
			<?php echo JText::_('COM_JGIVE_ENDING_CAMPAIGNS');?>
		</div>
		<hr/>
	<?php
	}
	?>

	<?php
	// Sice version 3.0 Jhtmlsidebar for menu
		if (!empty( $this->sidebar)) :
		?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php
		else :
		?>
			<div id="j-main-container">
		<?php
		endif;
		?>

	<div class="btn-group pull-right hidden-phone">
		<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
	</div>
	<?php $tblclass = 'table table-striped';?>

	<form action="" method="post" name="adminForm" id="adminForm">
		<div class="row-fluid inline">
				<div class="filter-search btn-group pull-left">
					<input type="text" name="filter_search" id="filter_search"
					placeholder="<?php echo JText::_('COM_JGIVE_ENTER_CAMPAIGN_NAME'); ?>"
					value="<?php echo $this->lists['filter_search']; ?>"
					class="hasTooltip input-medium"
					title="<?php echo JText::_('COM_JGIVE_ENTER_CAMPAIGN_NAME'); ?>" />
				</div>
				<div class="btn-group pull-left">
					<button type="submit" class="btn hasTooltip"
					title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
						<i class="icon-search"></i>
					</button>
					<button type="button" class="btn hasTooltip"
					title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"
					onclick="document.id('filter_search').value='';this.form.submit();">
						<i class="icon-remove"></i>
					</button>
				</div>

				<div  class="pull-right">
					<div  class="pull-left">
					<div  class="pull-left">
					<?php echo JHtml::_(
					'calendar', $this->date['start_date'], 'start_date', 'start_date', JText::_('COM_JGIVE_DATE_FORMAT'),
					array('class' => 'inputbox', 'placeholder' => JText::_('COM_JGIVE_ENDING_CAMPAIGNS_FROM_DATE'))
					);
					?>
					</div>
					<div  class="pull-left">
					<?php echo JHtml::_(
					'calendar', $this->date['end_date'], 'end_date', 'end_date', JText::_('COM_JGIVE_DATE_FORMAT'),
					array('class' => 'inputbox','placeholder' => JText::_('COM_JGIVE_ENDING_CAMPAIGNS_TILL_DATE'))
					);?>
					</div>
					<input id="btnRefresh" class="btn  btn-small btn-primary" type="button" value=">>" style="font-weight: bold;" onclick="jgive_action()"/>
					</div>
				</div>

		</div>
		<?php
		if (empty($this->data))
		{
		?>
			<div class="clearfix">&nbsp;</div>
			<div class="alert alert-no-items">
				<?php echo JText::_('COM_JGIVE_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php
		}
		else
		{
		?>
			<table class="<?php echo $tblclass; ?>" width="100%">
				<thead>
					<tr>
						<th class = "center" width = "15%">
							<?php echo JHtml::_('grid.sort', 'COM_JGIVE_CAMPAIGN_DETAILS', 'title', $this->lists['filter_order_Dir'], $this->lists['filter_order']);?>
						</th>
						<th class = "center" width = "10%">
							<?php echo JHtml::_('grid.sort', 'COM_JGIVE_START_DATE', 'start_date', $this->lists['filter_order_Dir'], $this->lists['filter_order']);?>
						</th>
						<th class="center" width="10%">
							<?php
							$campaign_period_in_days = $params->get('campaign_period_in_days');

							if (empty($campaign_period_in_days) || $campaign_period_in_days == 0 )
							{
								echo JHtml::_('grid.sort', 'COM_JGIVE_END_DATE', 'end_date', $this->lists['filter_order_Dir'], $this->lists['filter_order']);
							}
							else
							{
								echo JText::_('COM_JGIVE_END_DURATION');
							}
							?>
						</th>

						<th class = "center" width = "15%">
							<?php echo JHtml::_('grid.sort', 'COM_JGIVE_GOAL_AMOUNT', 'goal_amount', $this->lists['filter_order_Dir'], $this->lists['filter_order']);?>
						</th>
						<th class = "center" width = "15%">
							<?php echo JHtml::_('grid.sort', 'COM_JGIVE_AMOUNT_RECEIVED', 'amount_received', $this->lists['filter_order_Dir'], $this->lists['filter_order']);?>
						</th>
						<th class = "center" width = "5%">
							<?php echo JHtml::_('grid.sort', 'COM_JGIVE_DONORS', 'donor_count', $this->lists['filter_order_Dir'], $this->lists['filter_order']);?>
						</th>
						<th class = "center" width = "9%">
							<?php echo JHtml::_('grid.sort', 'COM_JGIVE_ID', 'id', $this->lists['filter_order_Dir'], $this->lists['filter_order']);?>
						</th>
					</tr>
				</thead>
			<?php
			if (!empty($this->data))
			{
				$i = 1;
				$j = 0;
				$k = 0;

				foreach ($this->data as $camp_data)
				{
					$data      = $camp_data['campaign'];
					$images    = $camp_data['images'];
					$row       = $data;
					$published = JHtml::_('jgrid.published', $row->published, $j);

					?>
					<tr class="row<?php echo $j % 2;?>">
						<td class="center">
							<div>
								<a target="_blank"
								href="<?php echo JUri::root() . substr(
								JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $data->id),
								strlen(JUri::base(true)) + 1
								);?>"
								title="<?php echo JText::_('COM_JGIVE_CLICK_TO_VIEW_CAMP_TOOLTIP');?>">
								<?php echo $data->title;?>
								</a>
							</div>
							<div class="com_jgive_clear_both"></div>
						</td>

						<td class="center"><?php echo JHtml::_('date', $data->start_date, JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3')); ?></td>

						<td class="center">
							<?php
							if (empty($campaign_period_in_days) || $campaign_period_in_days == 0 )
							{
								echo JHtml::_('date', $data->end_date, JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
							}
							else
							{
								echo $data->days_limit;
							}
							?>
						</td>

						<td class="center"><?php
							$jgiveFrontendHelper = new jgiveFrontendHelper;
							$diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($data->goal_amount);
							echo $diplay_amount_with_format;
							?>
						</td>

						<td class="center"><?php

							$jgiveFrontendHelper = new jgiveFrontendHelper;
							$diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($data->amount_received);
							echo $diplay_amount_with_format;
							?>
						</td>

						<td class="center"><?php echo $data->donor_count;?></td>

						<td class="center"><?php echo $data->id;?></td>
					</tr>
					<?php
					$i++;
					$j++;
					$k++;
				}
			}

			if (!$this->issite)
			{
			?>
				<tr>
					<?php $class_pagination = '';?>
					<td colspan="11" class="com_jgive_align_center <?php echo $class_pagination; ?> ">
						<?php /*echo $this->pagination->getListFooter();*/ ?>
					</td>
				</tr>
			<?php
			}?>
		</table>
		<?php
		}?>

		<?php
		if ($this->issite)
		{
			$class_pagination = '';?>
			<div class="<?php echo $class_pagination; ?> com_jgive_align_center">
				<?php echo $this->pagination->getListFooter(); ?>
			</div>
		<?php
		}
		?>

		<input type="hidden" name="option" value="com_jgive" />
		<input type="hidden" name="view" value="ending_camp" />
		<input type="hidden" name="filter_order" value="<?php echo $this->lists['filter_order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['filter_order_Dir']; ?>" />
		<input type="hidden" name="defaltevent" value="<?php echo $this->lists['filter_campaign_cat'];?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" id="task" name="task" value="" />
		<input type="hidden" id="controller" name="controller" value="ending_camp" />
	</form>
</div>
