<?php
/**
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2011-2015 Techjoomla. All rights reserved.
 * @license    GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * Ending Camp view class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveViewEnding_Camp extends JViewLegacy
{
	/**
	 * Function to display.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function display($tpl = null)
	{
		global $mainframe, $option;
		$user = JFactory::getUser();

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jgive'))
		{
			$mainframe->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		JHtmlBehavior::framework();

		$mainframe = JFactory::getApplication();

		// Get logged in user id
		$user                = JFactory::getUser();
		$this->logged_userid = $user->id;

		$this->issite = 0;

		// Get params
		$params              = JComponentHelper::getParams('com_jgive');
		$this->currency_code = $params->get('currency');

		$lang = JFactory::getLanguage();
		$lang->load('com_jgive', JPATH_SITE);

		// Added By Sneha
		$layout = JFactory::getApplication()->input->get('layout', 'default');
		$this->setLayout($layout);

		// Load submenu
		$JgiveHelper = new JgiveHelper;

		if ($layout == 'default')
		{
			$JgiveHelper->addSubmenu('ending_camp');
		}
		else
		{
			$JgiveHelper->addSubmenu('campaigns');
		}

		jimport('joomla.html.pagination');

		// Get data from the model
		$data       = $this->get('Data');
		$pagination = $this->get('Pagination');

		// Get campaign status
		$campaignHelper = new campaignHelper;
		$data           = $campaignHelper->getCampaignStatus($data);

		// Push data into the template
		$this->data       = $data;
		$this->pagination = $pagination;

		// Ordering
		$filter_order     = $mainframe->getUserStateFromRequest('com_jgive.filter_order', 'filter_order', '', 'string');
		$filter_order_Dir = $mainframe->getUserStateFromRequest('com_jgive.filter_order_Dir', 'filter_order_Dir', '', 'word');

		// Category fillter

		$this->cat_options            = $campaignHelper->getCampaignsCategories();

		// Get filter value and set list
		$filter_campaign_cat          = $mainframe->getUserStateFromRequest('com_jgive.filter_campaign_cat', 'filter_campaign_cat', '', 'INT');
		$lists['filter_campaign_cat'] = $filter_campaign_cat;
		$this->lists                  = $lists;

		// Organization_individual_type filter since version 1.5.1

		// Load all filter values
		$this->user_filter_options             = $this->get('UserFilterOptions');
		$this->campaign_type_filter_options    = $this->get('CampaignTypeFilterOptions');
		$this->campaign_approve_filter_options = $this->get('CampaignApproveFilterOptions');
		$this->ordering_options                = $this->get('OrderingOptions');
		$this->ordering_direction_options      = $this->get('OrderingDirectionOptions');

		// Load current value for filter
		$filter_user          = $mainframe->getUserStateFromRequest('com_jgive' . 'filter_user', 'filter_user');
		$filter_campaign_type = $mainframe->getUserStateFromRequest('com_jgive' . 'filter_campaign_type', 'filter_campaign_type');

		$filter_org_ind_type     = $mainframe->getUserStateFromRequest('com_jgive' . 'filter_org_ind_type', 'filter_org_ind_type');

		// Filter to view unpulish & publish campaign
		$filter_campaign_approve = $mainframe->getUserStateFromRequest('com_jgive' . 'filter_campaign_approve', 'filter_campaign_approve', '', 'INT');

		// Check the email link to apporove if it is then set pending value for filter
		$approve                 = JFactory::getApplication()->input->get('approve', '', 'INT');

		if ($approve)
		{
			$filter_campaign_approve = 0;
		}

		// Set all filters in list
		$lists['filter_order']     = $filter_order;
		$lists['filter_order_Dir'] = $filter_order_Dir;

		$lists['filter_user']             = $filter_user;
		$lists['filter_campaign_type']    = $filter_campaign_type;
		$lists['filter_campaign_approve'] = $filter_campaign_approve;
		$lists['filter_org_ind_type']     = $filter_org_ind_type;
		$this->lists                      = $lists;

		// Added by Sneha
		$filter_state           = $mainframe->getUserStateFromRequest($option . 'filter_search', 'filter_search', '', 'string');
		$lists['filter_search'] = $filter_state;
		$this->assignRef('lists', $lists);

		$start_date         = $mainframe->getUserStateFromRequest($option . 'start_date', 'start_date', '', 'string');
		$date['start_date'] = $start_date;
		$this->assignRef('date', $date);

		$end_date         = $mainframe->getUserStateFromRequest($option . 'end_date', 'end_date', '', 'string');
		$date['end_date'] = $end_date;
		$this->assignRef('date', $date);

		// Set toolbar
		$this->_setToolBar();

		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Function to set tool bar
	 *
	 * @return void
	 *
	 * @since	1.8
	 */
	public function _setToolBar()
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_admin.css');
		$bar = JToolBar::getInstance('toolbar');

		$layout = JFactory::getApplication()->input->get('layout', 'default');
		$this->setLayout($layout);

		if ($layout == 'default')
		{
			JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_END_CAMPAIGNS'), 'list');
			JToolBarHelper::back(JText::_('JGIVE_HOME'), 'index.php?option=com_jgive');

			$button = "<a class='btn btn-small'
			class='button'
			type='submit'
			onclick=\"javascript:document.getElementById('task').value = 'ending_camp.csvexport';
			document.adminForm.submit();
			document.getElementById('task').value = '';\" href='#'>
			<span title='Export' class='icon-download'>
			</span>" . JText::_('CSV_EXPORT') . "</a>";
			$bar->appendButton('Custom', $button);
		}
	}
}
