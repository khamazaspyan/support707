<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * Email Template view for email invite
 *
 * @package     JGive
 * @subpackage  component
 * @since       1.0
 */
class JgiveViewemail_Template extends JViewLegacy
{
	/**
	 * Display view
	 *
	 * @param   STRING  $tpl  template name
	 *
	 * @return  void
	 *
	 * @since   1.8.5
	 */
	public function display($tpl = null)
	{
		// Load submenu
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jgive'))
		{
			$app->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		// Load submenu
		$JgiveHelper = new JgiveHelper;
		$JgiveHelper->addSubmenu('email_template');

		$layout = JFactory::getApplication()->input->get('layout', 'email_template');
		$this->setLayout($layout);
		$this->sidebar = JHtmlSidebar::render();

		// Set toolbar
		$this->_setToolBar($layout);
		parent::display($tpl);
	}

	/**
	 * Function to set tool bar.
	 *
	 * @param   String  $layout  Get Layout
	 *
	 * @return void
	 *
	 * @since	1.8
	 */
	public function _setToolBar($layout)
	{
		JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_EMAIL_TEMPLATE'), 'list');
		JToolBarHelper::preferences('com_jgive');

		$layout = JFactory::getApplication()->input->get('layout', '');

		if ($layout == 'email_template')
		{
			JToolBarHelper::save('email_template.' . $task = 'save', $alt = JText::_('COM_JGIVE_SAVE_EMAIL_TEMPLATE'));
		}
	}
}
