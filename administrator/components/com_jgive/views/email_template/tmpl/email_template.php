<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

 // No direct access
defined('_JEXEC') or die();

// Fetching Email message body
include_once JPATH_ADMINISTRATOR . "/components/com_jgive/template/email_template.php";

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive-tables.css');
$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/bs3/css/bootstrap.min.css');
$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/bs3');
?>
<?php
	if (!empty($this->sidebar))
	{?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php
	}
	else
	{?>
		<div id="j-main-container">
	<?php
	}
?>

<div class="tjBs3">
	<form action="" id="adminForm" name="adminForm" method="post" class="form-validate">
		<div class="row">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<div class="form-horizontal">
					<div class="control-group">
						<label class="control-label">
							<?php echo  JText::_('COM_JGIVE_ENTER_EMAIL_SUBJECT');?>
						</label>
						<div class="controls">
							<input type="text" id="jgive_subject"
							name="jgive_subject"  class="form-control"
							placeholder="<?php echo  JText::_('COM_JGIVE_ENTER_EMAIL_SUBJECT'); ?>"
							value="<?php echo $emails_config['email_subject'];?>">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?php
					$emorgdata=$emails_config['message_body'];
					$editor      =JFactory::getEditor();
					echo $editor->display("data[message_body]",stripslashes($emorgdata),670,600,60,20,false);?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="alert alert-info">
					<?php echo JText::_('COM_JGIVE_EMAIL_TAGS_DESC');?>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{payment.processor}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_PAYMENT_METHOD');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{payment.annonymous_donation}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_ANONYMOUS_DONATION');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{payment.order_id}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_ORDER_ID');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{payment.cdate}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_DONATION_DATE');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.address}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_ADDRESS');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.zip}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_ZIP');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.country_name}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_COUNTRY');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.state_name}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_STATE');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.city_name}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_CITY');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.email}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_EMAIL_ID');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.phone}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_PHONE_NO');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.first_name}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_FIRST_NAME');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{donor.last_name}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_LAST_NAME');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{campaign.donation_amount}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_DONATION_AMOUNT');?>
					</div>
				</div>
				<div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<strong>{campaign.title}</strong>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php echo JText::_('COM_JGIVE_TAGS_CAMPAIGN_TITLE');?>
					</div>
				</div>
			</div>
		</div>

		<input type = "hidden" name = "option"     value = "com_jgive" />
		<input type = "hidden" name = "task"  id="task" value = "" />
		<input type = "hidden" name = "view"  value = "email_template" />
		<input type = "hidden" name = "layout"  value = "email_template" />

			<?php echo JHtml::_('form.token');?>
	</form>
</div>
