<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.user.helper');
jimport('joomla.utilities.arrayhelper');

/**
 * JgiveViewCampaign form view class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JgiveViewCampaign extends JViewLegacy
{
	protected $item;

	protected $form_extra;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		$input                     = $mainframe->input;

		$user      = JFactory::getUser();

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jgive'))
		{
			$mainframe->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		if (!JFactory::getUser($user->id)->authorise('core.create', 'com_jgive'))
		{
			$mainframe->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		$path      = JPATH_ROOT . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$path = JPATH_ROOT . '/components/com_jgive/helpers/integrations.php';

		if (!class_exists('JgiveIntegrationsHelper'))
		{
			JLoader::register('JgiveIntegrationsHelper', $path);
			JLoader::load('JgiveIntegrationsHelper');
		}

		// Get logged in user id
		$user                = JFactory::getUser();
		$this->logged_userid = $user->id;

		// Get params
		$this->params                 = JComponentHelper::getParams('com_jgive');
		$this->currency_code          = $this->params->get('currency');
		$this->commission_fee         = $this->params->get('commission_fee');
		$this->send_payments_to_owner = $this->params->get('send_payments_to_owner');
		$this->default_country        = $this->params->get('default_country');
		$this->admin_approval         = $this->params->get('admin_approval');

		// Create is a default layout
		$layout = JFactory::getApplication()->input->get('layout', 'create');
		$this->setLayout($layout);

		// Create jgive helper object
		$jgiveFrontendHelper = new jgiveFrontendHelper;

		if ($layout == 'create')
		{
			if (!$this->logged_userid)
			{
				$msg = JText::_('COM_JGIVE_LOGIN_MSG');
				$uri = JFactory::getApplication()->input->get('REQUEST_URI', '', 'server', 'string');
				$url = base64_encode($uri);
				$mainframe->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url), $msg);
			}

			// Get country list options
			$countries               = $jgiveFrontendHelper->getCountries();
			$this->countries         = $countries;

			// Default task is createNewCampaign
			$this->task              = 'save';
			$id                     = JRequest::getInt('id', '');

			// If cid is passed task is - edit
			if ($id)
			{
				$input->set("content_id", $id);
				$this->task = 'SaveEditedCampaign';
				$this->item		= $this->get('Item');

				// Get form for extra fields.
				$this->form_extra = array();
				$ModelCampaign = $this->getModel('campaign');

				// Call to extra fields
				$this->form_extra = $ModelCampaign->getFormExtra(
				array("category" => $this->item->category_id,
					"clientComponent" => 'com_jgive',
					"client" => 'com_jgive.campaign',
					"view" => 'campaign',
					"layout" => 'create')
					);

				$this->form_extra = array_filter($this->form_extra);
			}

			// Get js group for loggend in user
			$this->js_groups = $this->get('JS_usergroup');

			// Get the campaigns categeory
			$cats = $this->get('CampaignsCats');

			$this->assignRef('cats', $cats);
		}

		$this->_setToolBar($layout, $this->task);

		$this->createCampaignTime();

		parent::display($tpl);
	}

	/**
	 * Set tool bar
	 *
	 * @param   STRING  $layout  Layout name
	 * @param   STRING  $task    Task name
	 *
	 * @return  void
	 */
	public function _setToolBar($layout, $task)
	{
		// Get the toolbar object instance
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_admin.css');
		$bar    = JToolBar::getInstance('toolbar');
		$layout = JFactory::getApplication()->input->get('layout', '');

		JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_CREATE_NEW_CAMPAIGN'), 'list');
		JToolBarHelper::preferences('com_jgive');

		if ($layout == 'create')
		{
			JToolBarHelper::back('COM_JGIVE_BACK', 'index.php?option=com_jgive&view=campaigns&layout=default');
		}
	}

	/**
	 * Method for setting hr and min
	 *
	 * @return  array
	 */
	public function createCampaignTime()
	{
		for ($i = 1; $i <= 12; $i++)
		{
			$hours[] = JHtml::_('select.option', $i, $i);
		}

		$minutes   = array();
		$minutes[] = JHtml::_('select.option', '00', '00');
		$minutes[] = JHtml::_('select.option', 15, '15');
		$minutes[] = JHtml::_('select.option', 30, '30');
		$minutes[] = JHtml::_('select.option', 45, '45');

		$amPmSelect   = array();
		$amPmSelect[] = JHtml::_('select.option', 'AM', 'am');
		$amPmSelect[] = JHtml::_('select.option', 'PM', 'pm');

		$this->campaignTimeData['hours'] = $hours;
		$this->campaignTimeData['minutes'] = $minutes;
		$this->campaignTimeData['amPmSelect'] = $amPmSelect;

		return $this->campaignTimeData;
	}
}
