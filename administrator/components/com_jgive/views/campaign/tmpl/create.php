<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
define('COM_JGIVE_ICON_MINUS', " icon-minus ");
define('COM_JGIVE_ICON_PLUS', "icon-plus-sign");

$document = JFactory::getDocument();
$document->addScript(JUri::root(true) . '/media/com_jgive/javascript/create_camp.js');
$user = JFactory::getUser();

// Load validation scripts
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.tooltip');
jimport('joomla.html.html.list');
JHtml::_('behavior.modal', 'a.modal');
jimport('joomla.html.html.bootstrap');

if (!empty($this->item))
{
	$this->cdata = $cdata = $this->item;
}

if (!empty($cdata->state))
{
	$state = $cdata->state;
}

if (!empty($cdata->city))
{
	$city = $cdata->city;
}

// Get the data to idetify which field to show
$show_selected_fields = $this->params->get('show_selected_fields');
$max_images_limit = $this->params->get('max_images', 6);
$creatorfield = array();

$show_field = 0;
$max_donation_cnf = 0;
$min_donation_cnf = 0;
$long_desc_cnf = 0;
$allow_exceed_cnf = 0;
$show_public_cnf = 0;
$address_cnf = 0;
$address2_cnf = 0;
$zip_cnf = 0;
$phone_cnf = 0;
$group_name_cnf = 0;
$website_address_cnf = 0;
$internal_use_cnf = 0;
$give_back_cnf = 0;
$js_group = 0;
$campaign_type_cnf = 0;
$goal_amount = 0;

if ($show_selected_fields)
{
	$creatorfield = $this->params->get('creatorfield');

	if (isset($creatorfield))

	foreach ($creatorfield as $tmp)
	{
		switch ($tmp)
		{
			case 'max_donation':
				$max_donation_cnf = 1;
			break;

			case 'min_donation':
				$min_donation_cnf = 1;
			break;

			case 'long_desc':
				$long_desc_cnf = 1;
			break;

			case 'allow_exceed':
				$allow_exceed_cnf = 1;
			break;

			case 'show_public':
				$show_public_cnf = 1;
			break;

			case 'address':
				$address_cnf = 1;
			break;

			case 'address2':
				$address2_cnf = 1;
			break;

			case 'zip':
				$zip_cnf = 1;
			break;

			case 'phone':
				$phone_cnf = 1;
			break;

			case 'group_name':
				$group_name_cnf = 1;
			break;

			case 'website_address':
				$website_address_cnf = 1;
			break;

			case 'internal_use':
				$internal_use_cnf = 1;
			break;

			case 'give_back':
				$give_back_cnf = 1;
			break;

			case 'js_group':
				$js_group = 1;
			break;

			case 'campaign_type':
				$campaign_type_cnf = 1;
			break;

			case 'goal_amount':
				$goal_amount = 1;
			break;
		}
	}
}
else
{
	$show_field=1;
}
// by default selected category at time of edit
$selected_cats=$this->cats;

//for icon
$jgive_icon_plus=COM_JGIVE_ICON_PLUS;
?>

<style>
.invalid{border-color: #E9322D;color:red;}
</style>
<script type="text/javascript">
var _URL = window.URL || window.webkitURL;

var jgiveAllowedMediaSize = '<?php echo $max_images_size = $this->params->get('max_size') * 1024; ?>';
var allowedMediaSizeErrorMessage = "<?php echo JText::_("COM_JGIVE_IMAGE_EXTENSIONS_AND_SIZE") . $this->params->get('max_size') . 'KB';?>";
var allowedImageDimensionErrorMessage = "<?php echo JText::sprintf('COM_JGIVE_IMAGE_SIZE_TIP', $this->params->get('large_width'), $this->params->get('large_height'));?>";
var jgiveAllowedMainImgTypes = ['image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg'];

jQuery(window).load(function(){
	jQuery("#camp_image").change(function(e) {
		var file, img;

		if ((file = this.files[0]))
		{
			img = new Image();
			img.onload = function() {

				if (file.size > jgiveAllowedMediaSize)
				{
					alert(allowedMediaSizeErrorMessage);
					jQuery("#camp_image").val('');
					return false;
				}

				if (this.width < 445 || this.height < 265)
				{
					alert(allowedImageDimensionErrorMessage + Joomla.JText._('COM_JGIVE_CAMPAIGN_MAIN_IMAGE_DIMES_INFO') + this.width + "px X " + this.height + "px");
				}
				
				if (typeof file.type != "undefined")
				{
					if (jQuery.inArray(file.type, jgiveAllowedMainImgTypes) == -1)
					{
						alert(Joomla.JText._('COM_JGIVE_CAMPAIGN_MAIN_IMAGE_TYPE_VALIDATION') + file.type);
						jQuery("#camp_image").val('');
						return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					return false;
				}
			};

			img.onerror = function()
			{
				alert(Joomla.JText._('COM_JGIVE_CAMPAIGN_MAIN_IMAGE_TYPE_VALIDATION') + file.type);
				jQuery("#camp_image").val('');
				return false;
			};

			img.src = _URL.createObjectURL(file);
		}
});
});
Joomla.submitbutton = function(action){
	var form = document.adminForm;
	if(action=='campaign.save' || action=='campaign.edit')
	{
		var goal_amount=techjoomla.jQuery('#goal_amount').val();
		var minimum_amount=techjoomla.jQuery('#minimum_amount').val();
		var daterangefrom=techjoomla.jQuery('#start_date').val();
		var daterangeto=techjoomla.jQuery('#end_date').val();

		if((parseFloat(goal_amount))<(parseFloat(minimum_amount)))
		{
			var msg="<?php echo JText::_('COM_JGIVE_GOAL_LESS_MINIMUM_AMT'); ?>";
			alert(msg);
			return false;
		}

		if(daterangefrom=='' && daterangeto==''){
		}
		var now=new Date();
		var year = now.getFullYear();
		var month = now.getMonth()+1;
		var date = now.getDate();
		if(date >=1 && date <=9)
		{
			var newdate = '0'+date;
		}
		else
		{
			var newdate = date;
		}
		if(month >=1 && month <=9)
		{
			var newmonth = '0'+month;
		}
		else
		{
			var newmonth = month;
		}

		today=year+'-'+newmonth+'-'+newdate;


		if((daterangefrom) > (daterangeto))
		{
			alert('<?php echo JText::_('COM_JGIVE_DATE_ERROR'); ?>');
			return false;
		}

		var validateflag = document.formvalidator.isValid(document.id('adminForm'));

		if(validateflag)
		{
			Joomla.submitform(action );
		}
		else
		{
			alert("<?php echo JText::_('COM_JGIVE_VALIDATATION_ERROR'); ?>");
			return false;
		}
	}
	else
	{
		Joomla.submitform(action );
	}
}

	var tabId=1;
	var lang_const_of="<?php echo JText::_('COM_JGIVE_STEPS');?>";
	techjoomla.jQuery(document).ready(function()
	{
		var allowed_img=<?php echo $max_images_limit;?> ;
		var selected_imgs = techjoomla.jQuery('.img_checkbox:checked').length;
		var remaing_imgs= new Number(allowed_img - selected_imgs);

		if(remaing_imgs <= 0)
		{
			hideShowChooseGalleryImageButton(1);
		}

		// Call function on image checked/ unchecked
		techjoomla.jQuery('.img_checkbox').change(function()
		{
			// get checked image count
			selected_imgs=techjoomla.jQuery('.img_checkbox:checked').length;

			// get choose button count
			var imgChoosebts = techjoomla.jQuery(".filediv").length;

			// find how mutch user can upload now
			remaing_imgs= new Number(allowed_img - selected_imgs);

			// Delete last newly added element if unchecked image checked again by user
			// Do not delete if only one image button exit because it require for cloning
			if(remaing_imgs < imgChoosebts && imgChoosebts>1)
			{
				techjoomla.jQuery(techjoomla.jQuery( ".filediv" ).last()).remove();
			}

			// If limit reached
			if(remaing_imgs <= 0)
			{
				//hide choose button, Image limit reached
				hideShowChooseGalleryImageButton(1);
			}
			else
			{
				//show choose button, Image limit not reached
				hideShowChooseGalleryImageButton(0);
			}

		});

		var state='',city='',category='';
		<?php if(!empty($state)) { ?>
		state="<?php echo $state;?>";
		<?php } ?>

		<?php if(!empty($city)) { ?>
		city="<?php echo $city;?>";

		<?php }
		if(!empty($selected_cats)) {
		} ?>

		generateState('country',state,city);
		// add required calss to category
		techjoomla.jQuery('#campaigncat_id').addClass("required");
		otherCity();

	});

	/*add clone script*/
	function addClone(rId,rClass)
	{
		var num=techjoomla.jQuery('.'+rClass).length;
		var removeButton="<div class='com_jgive_remove_button'>";
		removeButton+="<button class='btn btn-mini' type='button' id='remove"+num+"'";
		removeButton+="onclick=\"removeClone('jgive_container"+num+"','jgive_container');\" title=\"<?php echo JText::_('COM_JGIVE_REMOVE_TOOLTIP');?>\" >";
		removeButton+="<i class=\"icon-minus-sign\"></i></button>";
		removeButton+="</div>";
		var newElem=techjoomla.jQuery('#'+rId).clone().attr('id',rId+num);
		techjoomla.jQuery(newElem).children('.control-group').children('.controls').children('.control-group').children('.controls').children('.input-prepend,.input-append').children().each(function()
		{
			var kid=techjoomla.jQuery(this);
			if(kid.attr('id')!=undefined)
			{
				var idN=kid.attr('id');
				kid.attr('id',idN+num).attr('id',idN+num);
				kid.attr('title',idN+num).attr('title',idN+num);
				kid.attr('value','');
			}
		});
		techjoomla.jQuery(newElem).children('.control-group').children('.controls').children('.control-group').children('.controls').children().each(function()
		{
			var kid=techjoomla.jQuery(this);
			if(kid.attr('id')!=undefined)
			{
				var idN=kid.attr('id');
				kid.attr('id',idN+num).attr('id',idN+num);
				kid.attr('value','');
			}
		});

		techjoomla.jQuery('.'+rClass+':last').after(newElem);
		techjoomla.jQuery('div.'+rClass +' :last').append(removeButton);
	}

	//* show hide choose gallery image button
	function hideShowChooseGalleryImageButton(flag)
	{
		// Hide image choose block
		if(flag == 1)
		{
			techjoomla.jQuery('.addMoreImage').css('display','none');
			techjoomla.jQuery(".gallery_choose_btn").replaceWith('<input  type="file" name="jgive_img_gallery[]"  id="avatar" placeholder="" class="gallery_choose_btn"  accept="image/*">');
		}
		else if(flag == 0)
		{
			techjoomla.jQuery('.addMoreImage').css('display','block');
		}

	}

	function removeClone(rId,rClass)
	{
		var msg = "<?php echo JText::_('COM_JGIVE_CONFIRM_TO_DELETE_GIVEBACK'); ?>";

		if (confirm(msg) == true)
		{
			techjoomla.jQuery('#'+rId).remove();
		}
    }

	/*
	To generate State list according to selected Country
	@param id of select list
	*/
	function generateState(countryId,state,city)
	{
		//alert(countryId);
		generateCity(countryId,city);
		var country=techjoomla.jQuery('#'+countryId).val();
		techjoomla.jQuery.ajax(
		{
			url:'<?php echo JUri::root();?>'+'index.php?option=com_jgive&task=loadState&country='+country+'&tmpl=component&format=raw',
			type:'GET',
			dataType:'json',
			success:function(data)
			{
				if (data === undefined || data == null || data.length <= 0)
				{
					var op='<option value="">'+"<?php echo JText::_('COM_JGIVE_STATE');?>"+'</option>';
					select=techjoomla.jQuery('#state');
					select.find('option').remove().end();
					select.append(op);
				}
				else{
					generateoption(data,countryId,state);
				}
			}
		});
	}
	function generateCity(countryId,city)
	{
		var country=techjoomla.jQuery('#'+countryId).val();
		techjoomla.jQuery.ajax(
		{
			url:'<?php echo JUri::root();?>'+'index.php?option=com_jgive&task=loadCity&country='+country+'&tmpl=component&format=raw',
			type:'GET',
			dataType:'json',
			success:function(data)
			{
				if (data === undefined || data == null || data.length <= 0)
				{
					var op='<option value="">'+"<?php echo JText::_('COM_JGIVE_CITY');?>"+'</option>';
					select=techjoomla.jQuery('#city');
					select.find('option').remove().end();
					select.append(op);
				}
				else{
					generateoptioncity(data,countryId,city);
				}
			}
		});
	}
	/*
	TO generate option
	@param: data=list of state/region in Json format
	countryID=called country select list
	Source ID which generate Option list
	*/
	//State
	function generateoption(data,countryId,state)
	{
		var options, index, select, option;
		if(countryId=='country'){
			select = techjoomla.jQuery('#state');
		}
		select.find('option').remove().end();
		options=data.options;
		for(index = 0; index < data.length; ++index)
		{
			var region=data[index];
			if(state==region['id'])
				var op="<option value="  +region['id']+  " selected='selected'>"  +region['region']+   '</option>'     ;
			else
				var op="<option value="  +region['id']+  ">"  +region['region']+   '</option>'     ;
			if(countryId=='country'){
				techjoomla.jQuery('#state').append(op);
			}
		}
	}
	// City
	function generateoptioncity(data,countryId,citydeafult)
	{
		var options, index, select, option;
		if(countryId=='country'){
			select = techjoomla.jQuery('#city');
		}
		select.find('option').remove().end();
		options=data.options;
		for(index = 0; index < data.length; ++index)
		{
			var city=data[index];
			if(citydeafult==city['id'])
			{
				var op="<option value="  +city['id']+  " selected='selected'>"  +city['city']+   '</option>'     ;
			}
			else
			{
				//alert('no re');
				var op="<option value="  +city['id']+  ">"  +city['city']+   '</option>'     ;
			}
			if(countryId=='country'){
				techjoomla.jQuery('#city').append(op);
			}
		}
	}

	function change_max_donors(el)
	{
		var selectBox = el;
		var selectedValue = selectBox.options[selectBox.selectedIndex].value;
		if(selectedValue=='investment'){
			techjoomla.jQuery('#max_donors').val(99);
		}else{
			techjoomla.jQuery('#max_donors').val(0);
		}
	}

	function validateForm()
	{
		var goal_amount=techjoomla.jQuery('#goal_amount').val();
		var minimum_amount=techjoomla.jQuery('#minimum_amount').val();
		var daterangefrom=techjoomla.jQuery('#start_date').val();
		var daterangeto=techjoomla.jQuery('#end_date').val();

		if((parseFloat(goal_amount))<(parseFloat(minimum_amount)))
		{
			var msg="<?php echo JText::_('COM_JGIVE_GOAL_LESS_MINIMUM_AMT'); ?>";
			alert(msg);
			return false;
		}

		//Minimum giveback amount must be >= campaign minimum donation amount
		response = validateGiveBackAmount();

		if(! response)
		{
			return false;
		}

		if(daterangefrom=='' && daterangeto==''){
		}
		var now=new Date();
		var year = now.getFullYear();
		var month = now.getMonth()+1;
		var date = now.getDate();
		if(date >=1 && date <=9)
		{
			var newdate = '0'+date;
		}
		else
		{
			var newdate = date;
		}
		if(month >=1 && month <=9)
		{
			var newmonth = '0'+month;
		}
		else
		{
			var newmonth = month;
		}

		today=year+'-'+newmonth+'-'+newdate;

		if((daterangefrom) > (daterangeto))
		{
			alert('Start Date should not be greater than End Date');
			return false;
		}
	}

	function validateGiveBackAmount()
	{
		var minimum_amount=parseFloat(techjoomla.jQuery('#minimum_amount').val());
		var flag = 0;

		techjoomla.jQuery(".give_back_value").each(function()
		{
			var givbackamount = parseFloat(techjoomla.jQuery(this).val());

			if(givbackamount>0)
			{
				console.log(givbackamount);
				if(givbackamount < minimum_amount)
				{
					alert("<?php echo JText::_('COM_JGIVE_GIVEBACK_MINIMUM_AMOUNT_GT_CAMP_MIN_AMT'); ?>");
					flag = 1;
					return false;
				}
			}
		});

		if(flag==1)
		{
			return false;
		}

		return true;
	}

	var imageid=0;
	function addmoreImg(rId,rClass)
	{
			var selected_imgs=techjoomla.jQuery('.img_checkbox:checked').length;
			var visible_file=techjoomla.jQuery('.filediv').length;
			var allowed_img=<?php echo $max_images_limit;?> ;
			var remaing_imgs= new Number(allowed_img - selected_imgs - visible_file);

			if(remaing_imgs > 0)
			{
				imageid++;
				//var num=techjoomla.jQuery('.'+rClass).length;
				var num=imageid;
				/*console.log('div total= '+num);*/
				var pre = new Number(num - 1);
				var removeButton="<span class=''>";
				removeButton+="<button class='btn btn-danger btn-mini' type='button' id='remove"+num+"'";
				removeButton+="onclick=\"removeClone('filediv"+num+"','jgive_container');\" title=\"<?php echo JText::_('COM_JGIVE_REMOVE_TOOLTIP');?>\" >";
				removeButton+="<i class=\"<?php echo COM_JGIVE_ICON_MINUS;?> icon-white \"></i></button>";
				removeButton+="</span>";

				// create the new element via clone(), and manipulate it's ID using newNum value
				//if(num==1)
				{
					var newElem = techjoomla.jQuery('#' +rId).clone().attr('id', rId + num);
					var delid=rId;
				}
				//else
				/*{
					var newElem = techjoomla.jQuery('#' +rId+pre).clone().attr('id', rId + num);
					var delid=rId + pre;
				}*/
				newElem.find('.addmore').attr('id','addmoreid'+ num);
				//newElem.find(':file').attr('name','jgive_img_gallery'+ imageid);
				//removeClone('addmoreid'+pre ,'addmoreid'+pre );
				techjoomla.jQuery('.'+rClass+':last').after(newElem);
				techjoomla.jQuery('#'+rId+num).append(removeButton);
			}
			else
			{
				alert("<?php echo JText::sprintf('COM_JGIVE_U_ALLOWD_TO_UPLOAD_IMGES',$max_images_limit)?>");
			}
	}

	function otherCity()
	{
		if(document.adminForm.other_city_check.checked===true)
		{
			jQuery("#other_city").show();
			jQuery("#hide_city").hide();
		}
		else
		{
			jQuery("#hide_city").show();
			jQuery("#other_city").hide();
		}
	}
	function addRemoveRequired()
	{
		video=parseInt(techjoomla.jQuery('input:radio[name="video_img"]:checked').val());
		if(video==1)
		{
			techjoomla.jQuery('#video_url_imput_id').addClass('required');
		}
		else
		{
			techjoomla.jQuery('#video_url_imput_id').removeClass('required');
		}
	}
	function jSelectUser_jform_created_by(id, title)
	{
			var old_id = document.getElementById("promoter_id").value;
			if (old_id != id) {
				document.getElementById("promoter_id").value = id;
				document.getElementById("promoter_name").value = title;

			}
		SqueezeBox.close();
	}

	/**
	Function to active step
	@params button pressed (pre, next)
	*/
	function stepsWizard(value)
	{
		var requiredFieldValueMiss = 0 ;

		/** Form Field Validation is not on previous click */
		if(value != 'previous' )
		{
			var activeAreaid = techjoomla.jQuery("#contentArea>li.active").attr('id');

			techjoomla.jQuery("#"+activeAreaid+" .required").each(function() {
				if(!techjoomla.jQuery(this).val())
				{
					techjoomla.jQuery(this).focus();
					requiredFieldValueMiss = 1;
					return false;
				}
			});
		}

		if (requiredFieldValueMiss === 1)
		{
			/** Show error message **/
			techjoomla.jQuery('#jgive_validation_message').show();
			return false;
		}
		else
		{
			var tabToActivate;
			var tabContentAreaToActivate;

			techjoomla.jQuery('#jgive_validation_message').hide();
			techjoomla.jQuery('#submit-btn').hide();
			techjoomla.jQuery('#cancel-btn').hide();
			techjoomla.jQuery('#resent-btn').hide();
			techjoomla.jQuery('#previous-btn').show();

			switch(value)
			{
				case 'next':
					tabToActivate = techjoomla.jQuery("#tabContainer>ul>li.active").next();
					tabContentAreaToActivate = techjoomla.jQuery("#contentArea>li.active").next();
				break;

				case 'previous':
					tabToActivate = techjoomla.jQuery("#tabContainer>ul>li.active").prev();
					tabContentAreaToActivate = techjoomla.jQuery("#contentArea>li.active").prev();
				break;
			}

			techjoomla.jQuery("#tabContainer>ul>li.active>button").removeClass("btn-primary")
			techjoomla.jQuery("#tabContainer>ul>li.active").removeClass("active");

			techjoomla.jQuery(tabToActivate).addClass('active');
			techjoomla.jQuery("#tabContainer>ul>li.active>button").addClass("btn-primary");

			techjoomla.jQuery("#contentArea>li.active").removeClass("active");
			techjoomla.jQuery(tabContentAreaToActivate).addClass('active');

			techjoomla.jQuery('html,body').animate({scrollTop: techjoomla.jQuery("#tabContainer").offset().top},'slow');

			/** Check last li to show save, cancel & reset buttons */

			/** For first tab */
			var firstliId = techjoomla.jQuery(techjoomla.jQuery("#contentArea>li").first()).attr('id');
			var activeliId = techjoomla.jQuery(techjoomla.jQuery("#contentArea>li.active")).attr('id');

			/** Current active tab is first tab **/
			if(firstliId === activeliId)
			{
				/** Hide Next & last Button */
				techjoomla.jQuery('#previous-btn').hide();
			}
			/** For Last Tab */
			var LastliId = techjoomla.jQuery(techjoomla.jQuery("#contentArea>li").last()).attr('id');
			/** Current active tab is last tab **/
			if(LastliId === activeliId)
			{
				/** Hide Next & last Button */
				techjoomla.jQuery('#next-btn').hide();
				techjoomla.jQuery('#last-btn').hide();
				/** Show following buttons */
				techjoomla.jQuery('#submit-btn').show();
				techjoomla.jQuery('#cancel-btn').show();
				techjoomla.jQuery('#resent-btn').show();
			}
			else
			{
				/** Show following buttons */
				techjoomla.jQuery('#next-btn').show();
			}
		}
	}
	/**
	Function to check file extension
	*/
	function checkFileExtension(element)
	{
		// Refactored by Nilesh
		/*Check for browser support for all File API*/
		if (window.File && window.FileList && window.Blob)
		{
			/*Get file size and file type*/
			var fsize = techjoomla.jQuery(element)[0].files[0].size;
			var ftype = techjoomla.jQuery(element)[0].files[0].type;
			console.log('Selected file mimeType = ' + ftype);
			console.log('Selected file size = ' + fsize);
			/*Allowed MIME types*/
			var jgiveAllowedMimeTypes = ['image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif'];
			console.log(jQuery.inArray(ftype, jgiveAllowedMimeTypes));
			/*Get file size*/
			var jgiveAllowedMediaSize = '<?php echo $max_images_size = $this->params->get('max_size') * 1024; ?>';
			console.log(jgiveAllowedMediaSize);
			/*Prepare error message */
			var errorMessage = "<?php echo JText::_("COM_JGIVE_IMAGE_EXTENSIONS_AND_SIZE") . $this->params->get('max_size') . 'KB';?>";
			/*Check file size*/
			if (fsize > jgiveAllowedMediaSize) {
				alert(errorMessage);
				techjoomla.jQuery(element).val('');
				return false;
			}
			/*Check mime type*/
			if (ftype){
				if (jQuery.inArray(ftype, jgiveAllowedMimeTypes) == -1) {
					alert(errorMessage);
					techjoomla.jQuery(element).val('');
					return false;
				}
			}
			/*If no file type is detected, return true*/
			else{
				return true;
			}
			return true;
		}
		/*If no browser suppoer for File apis, return true*/
		else{
			return true;
		}
	}
</script>
<div class="techjoomla-bootstrap">
	<div class="row-fluid">
		<div id="jgive_validation_message" class="alert alert-error jgive_disply_none">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			 <h4><?php echo JText::_("COM_JGIVE_ERRORS") ?></h4>
			<?php echo JText::_("COM_JGIVE_FILL_MANDATORY_FIELDS_DATA"); ?>
		</div>
		<form action="" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data"
		class="form-horizontal form-validate form-validate" onsubmit="return validateForm();">
			<div id="tabContainer" class="stepwizard">
				<ul class="stepwizard-row">
					<?php $nex_step_id = 1; ?>
					<li class="stepwizard-step active">
						<button type="button" class="btn btn-primary jgive-btn-circle" disabled="disabled"><?php echo $nex_step_id++; ?></button>
						<p><?php echo JText::_('COM_JGIVE_CAMPAIGN_DETAILS');?></p>
					</li>
					<li class="stepwizard-step">
						<button type="button" class="btn btn-default jgive-btn-circle" disabled="disabled"><?php echo $nex_step_id++; ?></button>
						<p><?php echo JText::_('COM_JGIVE_PROMOTER_DETAILS');?></p>
					</li>
					<?php
					if($show_field==1 || $give_back_cnf==0 )
					{
					?>
						<li class="stepwizard-step">
							<button type="button" class="btn btn-default jgive-btn-circle" disabled="disabled"><?php echo $nex_step_id++; ?></button>
							<p><?php echo JText::_('COM_JGIVE_GIVE_BACK_DETAILS');?></p>
						</li>
					<?php
					}
					?>
					<li class="stepwizard-step">
						<button type="button" class="btn btn-default jgive-btn-circle" disabled="disabled"><?php echo  $nex_step_id++; ?></button>
						<p><?php echo JText::_('COM_JGIVE_PHOTOS_GALLERY');?></p>
					</li>
					<?php
					if( $this->params->get('video_gallery'))
					{
					?>
						<li class="stepwizard-step ">
							<button type="button" class="btn btn-default jgive-btn-circle" disabled="disabled"><?php echo $nex_step_id++; ?></button>
							<p><?php echo JText::_('COM_JGIVE_VIDEOS');?></p>
						</li>
					<?php
					}
					?>
					<li class="stepwizard-step ">
						<button type="button" class="btn btn-default jgive-btn-circle" disabled="disabled"><?php echo $nex_step_id++; ?></button>
						<p><?php echo JText::_('COM_JGIVE_EXTA_FIELDS');?></p>
					</li>
				</ul>
			</div>
			<div class="clearfix">&nbsp;</div>
			<ul id="contentArea" class="tab-content section-content item">
				<!--Start tab1 -->
				<li id="com_jgveTab1fb" class="tab-pane active">
					<div class="row-fluid">
						<div class="span12">
							<fieldset>
								<div class="control-group">
									<label class="control-label" for="title">
										<?php echo JHtml::tooltip(
													JText::_('COM_JGIVE_TITLE_TOOLTIP'),
													JText::_('COM_JGIVE_TITLE'),
													'',
													JText::_('COM_JGIVE_TITLE')
													);?>
										<span class="star">&nbsp;*</span>
									</label>
									<div class="controls">
										<input type="text" id="title" name="title" class="required" required="required" maxlength="250" placeholder="<?php echo JText::_('COM_JGIVE_TITLE');?>"
										value="<?php if(isset($cdata->title)) echo $cdata->title;?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="alias" title="<?php echo JText::_('COM_JGIVE_CAMPAIGN_ALIAS_TOOLTIP');?>">
										<?php echo JHtml::tooltip(
												JText::_('COM_JGIVE_CAMPAIGN_ALIAS_TOOLTIP'),
												JText::_('COM_JGIVE_CAMPAIGN_ALIAS'),
												'',
												JText::_('COM_JGIVE_CAMPAIGN_ALIAS'));?>
									</label>
									<div class="controls">
										<input type="text" id="alias" name="alias" class="" maxlength="250" placeholder="<?php echo JText::_('COM_JGIVE_CAMPAIGN_ALIAS_PLACEHOLDER');?>"
										value="<?php if(isset($cdata->alias)) echo $cdata->alias;?>">
									</div>
								</div>
								<?php
									$donation=$investment='';
									if(isset($cdata->type))
									{
										if($cdata->type=='donation')
											$donation='selected';
										else
											$investment='selected';
									}
									else{
										$donation='selected';
									}
								?>
								<div class="control-group">
									<label class="control-label" for="type">
										<?php echo JHtml::tooltip(
												JText::_('COM_JGIVE_TYPE_TOOLTIP'),
												JText::_('COM_JGIVE_TYPE'),
												'',
												JText::_('COM_JGIVE_TYPE'));?>
									</label>
									<div class="controls">
										<?php
										$count=count($this->params->get('camp_type'));
										//check for admin set the allowed campaigns to created are multiple
										if($count>1) { ?>
										<select id="type" name="type" class="required" onchange="change_max_donors(this)">
											<option id="donation" value="donation" <?php echo $donation;?>><?php echo JText::_('COM_JGIVE_CAMPAIGN_TYPE_DONATION');?></option>
											<option id="investment" value="investment" <?php echo $investment;?>><?php echo JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT');?></option>
										</select>
										<?php } else { //if admin set single type of campaigns to created then show this type in text box. ?>
										<input type="text" name="type"  value="<?php $type_array=$this->params->get('camp_type'); echo $type_array[0]=='investment' ? JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT'):JText::_('COM_JGIVE_CAMPAIGN_TYPE_DONATION'); ?>" disabled="disabled">
										<?php } ?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="campaign_category">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_CATEGORY_TOOLTIP'),
										JText::_('COM_JGIVE_CATEGORY'),
										'',
										JText::_('COM_JGIVE_CATEGORY'));?>
									</label>
									<div class="controls">
										<?php
											echo  $this->cats;
										?>
									</div>
								</div>
								<?php
									$non_profit=$self_help=$individuals='';
									if(isset($cdata->org_ind_type))
									{
										if($cdata->org_ind_type=='non_profit')
											$non_profit='selected';
										else if($cdata->org_ind_type=='self_help')
											$self_help='selected';
										else if($cdata->org_ind_type=='individuals')
											$individuals='selected';
									}
								?>
								<div class="control-group">
									<label class="control-label" for="org_ind_type">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_TYPE_ORG_INDIVIDUALS_TOOLTIP'),
										JText::_('COM_JGIVE_TYPE_ORG_INDIVIDUALS'),
										'',
										JText::_('COM_JGIVE_TYPE_ORG_INDIVIDUALS'));?>
									</label>
									<div class="controls">
										<select id="org_ind_type" name="org_ind_type">
											<option id="non_profit" value="non_profit" <?php echo $non_profit;?>><?php echo JText::_('COM_JGIVE_ORG_NON_PROFIT'); ?></option>
											<option id="self_help" value="self_help" <?php echo $self_help;?>><?php echo JText::_('COM_JGIVE_SELF_HELP'); ?></option>
											<option id="individuals" value="individuals" <?php echo $individuals;?>><?php echo JText::_('COM_JGIVE_SELF_INDIVIDUALS'); ?></option>
										</select>
									</div>
								</div>
							<?php if($show_field==1 OR $max_donation_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="max_donors">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_MAX_DONORS_TOOLTIP'),
										JText::_('COM_JGIVE_MAX_DONORS'),
										'',
										JText::_('COM_JGIVE_MAX_DONORS'));?>
									</label>
									<div class="controls">
										<input type="text" id="max_donors" name="max_donors" class="required validate-numeric" maxlength="11" placeholder="<?php echo JText::_('COM_JGIVE_MAX_DONORS');?>"
										value="<?php
										if(isset($cdata->max_donors))
											echo $cdata->max_donors;
										else
											echo 0;
										?>"/>
									</div>
								</div>
							<?php endif;?>
							<!--if added by Sneha-->
							<?php if($show_field==1 OR $goal_amount==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="goal_amount">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_GOAL_AMOUNT_TOOLTIP'),
										JText::_('COM_JGIVE_GOAL_AMOUNT'),
										'',
										JText::_('COM_JGIVE_GOAL_AMOUNT'));?>
										<span class="star">&nbsp;*</span>
									</label>
									<div class="controls">
										<div class="input-append">
											<input type="text" id="goal_amount" name="goal_amount" class="required validate-numeric" maxlength="11" placeholder="<?php echo JText::_('COM_JGIVE_GOAL_AMOUNT');?>"
											value="<?php if(isset($cdata->goal_amount)) echo $cdata->goal_amount;?>">
											<span class="add-on"><?php echo $this->currency_code;?></span>
										</div>
									</div>
								</div>
							<?php endif;?>
							<!--End added by Sneha-->
							<?php if($show_field==1 OR $min_donation_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="minimum_amount">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_MINIMUM_AMOUNT_TOOLTIP'),
										JText::_('COM_JGIVE_MINIMUM_AMOUNT'),
										'',
										JText::_('COM_JGIVE_MINIMUM_AMOUNT'));?>
									</label>
									<div class="controls">
										<div class="input-append">
											<input type="text" id="minimum_amount" name="minimum_amount" class="required validate-numeric" maxlength="11" placeholder="<?php echo JText::_('COM_JGIVE_MINIMUM_AMOUNT');?>"
											value="<?php
											if(isset($cdata->minimum_amount))
												echo $cdata->minimum_amount;
											else
												echo 0;
											?>">
											<span class="add-on"><?php echo $this->currency_code;?></span>
										</div>
									</div>
								</div>
							<?php endif;?>
								<div class="control-group">
									<label class="control-label" for="short_desc">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_SHORT_DESC_TOOLTIP'),
										JText::_('COM_JGIVE_SHORT_DESC'),
										'',
										JText::_('COM_JGIVE_SHORT_DESC'));?>
										<span class="star">&nbsp;*</span>
									</label>
									<div class="controls">
										<textarea rows="3" cols="50" id="short_desc" name="short_desc" maxlength="250" class="required" placeholder="<?php echo JText::_('COM_JGIVE_SHORT_DESC');?>"><?php if(isset($cdata->short_description)) echo $cdata->short_description;?></textarea>
									</div>
								</div>
							<?php if($show_field==1 OR $long_desc_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="long_desc" title="<?php echo JText::_('COM_JGIVE_LONG_DESC_TOOLTIP');?>">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_LONG_DESC_TOOLTIP'),
										JText::_('COM_JGIVE_LONG_DESC'),
										'',
										JText::_('COM_JGIVE_LONG_DESC'));?>
									</label>
									<div class="controls">
										<?php
											$params = array( 'safehtml'=> 'true' ,);
											$editor      =JFactory::getEditor();
											if(!isset($cdata->long_description))
											{
												$cdata=new Stdclass;
												$cdata->long_description='';
											}
											echo $editor->display('long_desc',$cdata->long_description,'95%','100',5,50,false);
										?>
									</div>
								</div>
							<?php endif;?>
							<!--rearange  -->
								<div class="control-group">
									<label class="control-label" for="start_date">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_START_DATE_TOOLTIP'),
										JText::_('COM_JGIVE_START_DATE'),
										'',
										JText::_('COM_JGIVE_START_DATE')
										);?>
									</label>
									<div class="controls">
										<?php
											if(!isset($cdata->start_date))
											{
												$selectedStartHour = JFactory::getDate()->Format('H');
												$selectedmin = JFactory::getDate()->Format('i');
												$startAmPm   = JFactory::getDate()->Format('H') >= 12 ? 'PM' : 'AM';
												$date = JFactory::getDate()->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
											}
											else
											{
												$selectedStartHour = JFactory::getDate($cdata->start_date)->Format('H');
												$selectedStartHour = JHtml::date($cdata->start_date,JText::_('H'), true);
												$selectedmin = JFactory::getDate($cdata->start_date)->Format('i');
												$selectedmin = JHtml::date($cdata->start_date,JText::_('i'), true);
												$startAmPm   = JFactory::getDate($cdata->start_date)->Format('H');
												$startAmPm   = JHtml::date($cdata->start_date,JText::_('H'), true);
												$startAmPm  = $startAmPm >= 12 ? 'PM' : 'AM';

												if($selectedStartHour > 12)
												{
													$selectedStartHour = $selectedStartHour - 12;
												}

												$date = JFactory::getDate($cdata->start_date)->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
												$date = JHtml::date($cdata->start_date,JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'), true);
											}

											if($selectedStartHour=='00' or $selectedStartHour=='0')
											{
												$selectedStartHour = 12;
											}

											echo $calendar=JHtml::_('calendar',$date,'start_date','start_date',JText::_('COM_JGIVE_DATE_FORMAT'), 'class="required" required="required" readonly="true"');
											echo "&nbsp;&nbsp;";

											echo $startHourSelect = JHtml::_('select.genericlist', $this->campaignTimeData['hours'], 'campaign_start_time_hour', array('class'=>'required input input-mini chzn-done changevenue'), 'value', 'text', $selectedStartHour, false );

											echo $startMinSelect = JHtml::_('select.genericlist', $this->campaignTimeData['minutes'], 'campaign_start_time_min', array('class'=>'required input input-mini chzn-done changevenue'), 'value', 'text', $selectedmin, false );

											echo $startAmPmSelect = JHtml::_('select.genericlist', $this->campaignTimeData['amPmSelect'], 'campaign_start_time_ampm', array('class'=>'required input input-mini chzn-done changevenue'), 'value', 'text', $startAmPm, false);

											echo "<br/>";
											echo "<i>".JText::_('COM_JGIVE_DATE_FORMAT_DESC')."</i>";
										?>
									</div>
								</div>

								<?php
								$campaign_period_in_days = $this->params->get('campaign_period_in_days');

								if(empty($campaign_period_in_days) || $campaign_period_in_days == 0 )
								{?>
									<div class="control-group">
										<label class="control-label" for="end_date">
											<?php echo JHtml::tooltip(
											JText::_('COM_JGIVE_END_DATE_TOOLTIP'),
											JText::_('COM_JGIVE_END_DATE'),
											'',
											JText::_('COM_JGIVE_END_DATE'));?>
										</label>
										<div class="controls">
											<?php
												$selectedStartHour = $selectedmin = $startAmPm = $end_date= '';

												if(!isset($cdata->end_date))
												{
													$selectedStartHour = JFactory::getDate()->Format('H');
													$selectedmin = JFactory::getDate()->Format('i');
													$startAmPm   = JFactory::getDate()->Format('H') >= 12 ? 'PM' : 'AM';
													$end_date = JFactory::getDate()->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
												}
												else
												{
													$selectedStartHour = JFactory::getDate($cdata->end_date)->Format('H');
													$selectedStartHour = JHtml::date($cdata->end_date,JText::_('H'), true);
													$selectedmin = JFactory::getDate($cdata->end_date)->Format('i');
													$selectedmin = JHtml::date($cdata->end_date,JText::_('i'), true);
													$startAmPm   = JFactory::getDate($cdata->end_date)->Format('H');
													$startAmPm   = JHtml::date($cdata->end_date,JText::_('H'), true);
													$startAmPm  = $startAmPm >= 12 ? 'PM' : 'AM';


													if($selectedStartHour > 12)
													{
														$selectedStartHour = $selectedStartHour - 12;
													}

													$end_date = JFactory::getDate($cdata->end_date)->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
													$end_date = JHtml::date($cdata->end_date,JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'), true);
												}

												echo $calendar= JHtml::_('calendar',$end_date,'end_date','end_date',JText::_('COM_JGIVE_DATE_FORMAT'), 'class="required" required="required" readonly="true"');

												echo "&nbsp;&nbsp;";

												echo $endHourSelect = JHtml::_('select.genericlist', $this->campaignTimeData['hours'], 'campaign_end_time_hour', array('class'=>'required input input-mini chzn-done changevenue'), 'value', 'text', $selectedStartHour, false );

												echo $endMinSelect = JHtml::_('select.genericlist',  $this->campaignTimeData['minutes'] , 'campaign_end_time_min', array('class'=>'required input input-mini chzn-done changevenue'), 'value', 'text',$selectedmin , false );

												echo $endAmPmSelect = JHtml::_('select.genericlist', $this->campaignTimeData['amPmSelect'], 'campaign_end_time_ampm', array('class'=>'required input input-mini chzn-done changevenue'), 'value', 'text', $startAmPm, false );

												echo "<br/>";
												echo "<i>".JText::_('COM_JGIVE_DATE_FORMAT_DESC')."</i>";
											?>
										</div>
									</div>
									<?php
								}
								else
								{ ?>
									<div class="control-group">
										<label class="control-label" for="days_limit">
											<?php echo JHtml::tooltip(
											JText::_('COM_JGIVE_PERIOD_IN_DAYS'),
											JText::_('COM_JGIVE_PERIOD_IN_DAYS'),
											'',
											JText::_('COM_JGIVE_PERIOD_IN_DAYS'));?>
										</label>
										<div class="controls">
											<input type="text" id="days_limit" name="days_limit" placeholder="<?php echo JText::_('COM_JGIVE_PERIOD_IN_DAYS_PLACEHOLDER');?>"
											value="<?php if(isset($cdata->days_limit)) echo $cdata->days_limit;?>"
											class="required" required="required">
										</div>
								</div>
								 <?php
								}  ?>
								<?php
								$publish1=$publish2='';
								if(isset($cdata->published))
								{
									if($cdata->published)
								$publish1='checked';
									else
											$publish2='checked';
								}else{
									$publish1='checked';
								}
								?>
								<div class="control-group">
									<label class="control-label" for="publish1" title="<?php echo JText::_('COM_JGIVE_PUBLISH_TOOLTIP');?>">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_PUBLISH_TOOLTIP'),
										JText::_('COM_JGIVE_PUBLISH'),
										'',
										JText::_('COM_JGIVE_PUBLISH'));?>
									</label>
									<div class="controls">
										<label class="radio inline">
											<input type="radio" name="publish" id="publish1" value="1" <?php echo $publish1;?> >
												<?php echo JText::_('COM_JGIVE_YES');?>
										</label>
										<label class="radio inline">
											<input type="radio" name="publish" id="publish2" value="0" <?php echo $publish2;?>>
												<?php echo JText::_('COM_JGIVE_NO');?>
										</label>
									</div>
								</div>
							<!--Condition changed by Sneha-->
							<?php if($show_field==1 OR ($allow_exceed_cnf==0 AND $goal_amount==0 )): ?>
								<?php
									$allow_exceed1=$allow_exceed2='';
									if(isset($cdata->allow_exceed))
									{
										if($cdata->allow_exceed)
											$allow_exceed1='checked';
										else
											$allow_exceed2='checked';
									}else{
										$allow_exceed1='checked';
									}
								?>
								<div class="control-group">
									<label class="control-label" for="allow_exceed1">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_ALLOW_DONATIONS_EXCEED_TOOLTIP'),
										JText::_('COM_JGIVE_ALLOW_DONATIONS_EXCEED'),
										'',
										JText::_('COM_JGIVE_ALLOW_DONATIONS_EXCEED'));?>
									</label>
									<div class="controls">
										<label class="radio inline">
											<input type="radio" name="allow_exceed" id="allow_exceed1" value="1" <?php echo $allow_exceed1;?>>
												<?php echo JText::_('COM_JGIVE_YES');?>
										</label>
										<label class="radio inline">
											<input type="radio" name="allow_exceed" id="allow_exceed2" value="0" <?php echo $allow_exceed2;?>>
												<?php echo JText::_('COM_JGIVE_NO');?>
										</label>
									</div>
								</div>
							<?php endif; ?>
							<?php if($show_field==1 OR $show_public_cnf==0 ): ?>
								<?php
									$show_public1=$show_public2='';
									if(isset($cdata->allow_view_donations))
									{
										if($cdata->allow_view_donations)
											$show_public1='checked';
										else
											$show_public2='checked';
									}else{
										$show_public1='checked';
									}
								?>
								<div class="control-group">
									<label class="control-label"  for="show_public1">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_SHOW_DONATIONS_TO_TOOLTIP'),
										JText::_('COM_JGIVE_SHOW_DONATIONS_TO'),
										'',
										JText::_('COM_JGIVE_SHOW_DONATIONS_TO'));?>
									</label>
									<div class="controls">
										<label class="radio inline">
											<input type="radio" name="show_public" id="show_public1" value="1" <?php echo $show_public1;?>>
												<?php echo JText::_('COM_JGIVE_YES');?>
										</label>
										<label class="radio inline">
											<input type="radio" name="show_public" id="show_public2" value="0" <?php echo $show_public2;?>>
												<?php echo JText::_('COM_JGIVE_NO');?>
										</label>
									</div>
								</div>
							<?php endif;?>
							<div class="control-group">
								<label class="control-label" for="meta_data_lbl">
									<?php echo JHtml::tooltip(
									JText::_('COM_JGIVE_META_DATA_TOOLTIP'),
									JText::_('COM_JGIVE_META_DATA'),
									'',
									JText::_('COM_JGIVE_META_DATA')
									);?>
								</label>
								<div class="controls">
									<textarea rows="3" cols="50" id="meta_data" name="meta_data" maxlength="250"  placeholder="<?php echo JText::_('COM_JGIVE_META_DATA');?>"><?php if(isset($cdata->meta_data)) echo $cdata->meta_data;?></textarea>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="meta_desc_lbl">
									<?php echo JHtml::tooltip(
									JText::_('COM_JGIVE_META_DESCRIPTION_TOOLTIP'),
									JText::_('COM_JGIVE_META_DESCRIPTION'),
									'',
									JText::_('COM_JGIVE_META_DESCRIPTION'));?>
								</label>
								<div class="controls">
									<textarea rows="3" cols="50" id="meta_desc" name="meta_desc" maxlength="250"  placeholder="<?php echo JText::_('COM_JGIVE_META_DESCRIPTION');?>"><?php if(isset($cdata->meta_desc)) echo $cdata->meta_desc;?></textarea>
								</div>
							</div>
							<?php
							$integration=$this->params->get('integration');
							if($integration=='jomsocial')
							{
							 if($show_field==1 OR $js_group==0 ):
								$count=count($this->js_groups);
								if($count>=1) { ?>
								<div class="control-group">
									<label class="control-label" for="js_group">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_SELECT_GROUP_TP'),
										JText::_('COM_JGIVE_SELECT_GROUP'),
										'',
										JText::_('COM_JGIVE_SELECT_GROUP'));?>
									</label>
									<div class="controls">
										<select id="js_group" name="js_group" class="">
											<option value="0"><?php echo JText::_('COM_JGIVE_SELECT_JS_GROUP'); ?></option>
											<?php
											foreach($this->js_groups as $grp){
												$selected='';
												if($grp['id']==$cdata->js_groupid)
													$selected='selected="selected"';
												 ?>
												<option value="<?php echo $grp['id']; ?>"<?php echo $selected;?> >
												<?php echo $grp['title'];?></option>
												<?php
											} ?>
										</select>
									</div>
								</div>
							<?php } ?>
							<?php endif;
							}
							?>
							</fieldset>
						</div>
					</div>
				</li>
				<!-- End tab1 -->
				<!--Start tab2 -->
				<li id="com_jgveTab2fb" class="tab-pane">
					<div class="row-fluid">
						<div class="span12">
							<fieldset>
								<legend><i class="fontello-icon-bag"></i> <span> <?php echo JText::_('COM_JGIVE_PROMOTER_DETAILS');?></span></legend>
								<div class="control-group jgive_select_user">
									<label class="control-label" for="promoter_name" title="<?php echo JText::_('COM_JGIVE_CAMPAIGN_PROMOTER_LABEL');?>">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_CAMPAIGN_PROMOTER_LABEL'),
										JText::_('COM_JGIVE_CAMPAIGN_PROMOTER_LABEL_SELECT'),
										'',
										JText::_('COM_JGIVE_CAMPAIGN_PROMOTER_LABEL_SELECT'));?>
									</label>
									<div class="controls">
										<?php
											$userId = isset($cdata->creator_id) ? JFactory::getUser($cdata->creator_id)->id : JFactory::getUser()->id;

											$userFieldData = array ("required"=>1,
												"class" =>"",
												"size" => 0,
												"readonly" => "",
												"onchange" => "",
												"id" => "promoter_id",
												"name" => "promoter_id",
												"value" => $userId,
												"userName" => JFactory::getUser($userId)->name
												);

											$jGiveLayout = new JLayoutFile('joomla.form.field.user');

											echo $jGiveLayout->render($userFieldData);
										?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="first_name">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_FIRST_NAME_TOOLTIP'),
										JText::_('COM_JGIVE_FIRST_NAME'),
										'',
										JText::_('COM_JGIVE_FIRST_NAME'));?>
										<span class="star">&nbsp;*</span>
									</label>
									<div class="controls">
										<input type="text" id="first_name" name="first_name" class="required" placeholder="<?php echo JText::_('COM_JGIVE_FIRST_NAME');?>"
										value="<?php if(isset($cdata->first_name)) echo $cdata->first_name;?>">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="last_name">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_LAST_NAME_TOOLTIP'),
										JText::_('COM_JGIVE_LAST_NAME'),
										'',
										JText::_('COM_JGIVE_LAST_NAME'));?>
										<span class="star">&nbsp;*</span>
									</label>
									<div class="controls">
										<input type="text" id="last_name" name="last_name" class="required" placeholder="<?php echo JText::_('COM_JGIVE_LAST_NAME');?>"
										value="<?php if(isset($cdata->last_name)) echo $cdata->last_name;?>">
									</div>
								</div>
							<?php if($show_field==1 OR $address_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="address">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_ADDRESS_TOOLTIP'),
										JText::_('COM_JGIVE_ADDRESS'),
										'',
										JText::_('COM_JGIVE_ADDRESS'));?>
										<span class="star">&nbsp;*</span>
									</label>
									<div class="controls">
										<input type="text" id="address" name="address" class="required" placeholder="<?php echo JText::_('COM_JGIVE_ADDRESS');?>"
										value="<?php if(isset($cdata->address)) echo $cdata->address;?>">
									</div>
								</div>
							<?php endif;?>
							<?php if($show_field==1 OR $address2_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="address2">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_ADDRESS2_TOOLTIP'),
										JText::_('COM_JGIVE_ADDRESS2'),
										'',
										JText::_('COM_JGIVE_ADDRESS2'));?>
									</label>
									<div class="controls">
										<input type="text" id="address2" name="address2" placeholder="<?php echo JText::_('COM_JGIVE_ADDRESS2');?>"
										value="<?php if(isset($cdata->address2)) echo $cdata->address2;?>">
									</div>
								</div>
							<?php endif;?>
								<div class="control-group">
									<label class="control-label" for="country">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_COUNTRY_TOOLTIP'),
										JText::_('COM_JGIVE_COUNTRY'),
										'',
										JText::_('COM_JGIVE_COUNTRY'));?>
										<span class="star">&nbsp;*</span>
									</label>
									<div class="controls">
										<?php
										$countries=$this->countries;
										$default=NULL;
										if(isset($cdata->country)){
											$default=$cdata->country;
										}else{
											$default=$this->default_country;
										}
										$options=array();
										$options[]=JHtml::_('select.option',"",JText::_('COM_JGIVE_COUNTRY'));
										foreach($countries as $key=>$value)
										{
											$country=$countries[$key];
											$id=$country['id'];
											$value=$country['country'];
											$options[]=JHtml::_('select.option', $id, $value);
										}
										if(empty($cdata->state))
										{	$cdata->state='';
											$cdata->city='';
										}
										echo $this->dropdown=JHtml::_('select.genericlist',$options,'country','class="required" required="required" aria-invalid="false" size="1" onchange="generateState(id,\''.$cdata->state.'\',\''.$cdata->city.'\')"','value','text',$default,'country');
										?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="state">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_STATE_TOOLTIP'),
										JText::_('COM_JGIVE_STATE'),
										'',
										JText::_('COM_JGIVE_STATE'));?>
									</label>
									<div class="controls">
										<select name="state" id="state"></select>
									</div>
								</div>
								<div class="control-group" id="hide_city">
									<label class="control-label" for="city">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_CITY_TOOLTIP'),
										JText::_('COM_JGIVE_CITY'),
										'',
										JText::_('COM_JGIVE_CITY'));?>
									</label>
									<div class="controls">
										<select name="city" id="city"></select>
									</div>
								</div>
								<?php
									$other_city_checked='';
									if(!empty($cdata->other_city))
									{
										$other_city_checked='checked="checked"';
									}
								?>
								<div class="control-group">
									<label class="control-label" for="other_city">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_OTHER_CITY_TOOLTIP'),
										JText::_('COM_JGIVE_OTHER_CITY'),
										'',
										JText::_('COM_JGIVE_OTHER_CITY'));?>
									</label>
									<div class="controls">
										<input type="checkbox" name="other_city_check" id="other_city_check" <?php echo $other_city_checked;?>  onchange="otherCity()"/>
										<?php echo JText::_('COM_JGIVE_CHECK_OTHER_CITY_MSG'); ?> <br/><br/>
										<input type="text" name="other_city" id="other_city" placeholder="<?php echo JText::_('COM_JGIVE_ENTER_OTHER_CITY');?>" value="<?php echo $cdata->city; ?>" >
									</div>
								</div>
							<?php if($show_field==1 OR $zip_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="zip" title="<?php echo JText::_('COM_JGIVE_ZIP_TOOLTIP');?>">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_ZIP_TOOLTIP'),
										JText::_('COM_JGIVE_ZIP'),
										'',
										JText::_('COM_JGIVE_ZIP')
										);?>
										<span class="star">&nbsp;*</span>
									</label>
									<div class="controls">
										<input type="text" id="zip" name="zip" class="required" placeholder="<?php echo JText::_('COM_JGIVE_ZIP');?>"
										value="<?php if(isset($cdata->zip)) echo $cdata->zip;?>">
									</div>
								</div>
							<?php endif;?>
							<?php if($show_field==1 OR $phone_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="phone">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_PHONE_TOOLTIP'),
										JText::_('COM_JGIVE_PHONE'),
										'',
										JText::_('COM_JGIVE_PHONE'));?>
									</label>
									<div class="controls">
										<input type="text" id="phone" name="phone" placeholder="<?php echo JText::_('COM_JGIVE_PHONE');?>"
										value="<?php if(isset($cdata->phone)) echo $cdata->phone;?>">
									</div>
								</div>
							<?php endif;?>
							<!-- group name & website (blk) -->
							<?php if($show_field==1 OR $group_name_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="group_name" title="<?php echo JText::_('COM_JGIVE_GROUP_NAME_TOOLTIP');?>">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_GROUP_NAME_TOOLTIP'),
										JText::_('COM_JGIVE_GROUP_NAME'),
										'',
										JText::_('COM_JGIVE_GROUP_NAME'));?>
									</label>
									<div class="controls">
										<input type="text" id="group_name" name="group_name" class="" placeholder="<?php echo JText::_('COM_JGIVE_GROUP_NAME');?>"
										value="<?php if(isset($cdata->group_name)) echo $cdata->group_name;?>">
									</div>
								</div>
							<?php endif; ?>
							<?php if($show_field==1 OR $website_address_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="website_address">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_WEBSITE_TOOLTIP'),
										JText::_('COM_JGIVE_WEBSITE'),
										'',
										JText::_('COM_JGIVE_WEBSITE'));?>
									</label>
									<div class="controls">
										<input type="text" id="website_address" name="website_address" class="" placeholder="<?php echo JText::_('COM_JGIVE_WEBSITE');?>"
										value="<?php if(isset($cdata->website_address)) echo $cdata->website_address;?>">
									</div>
								</div>
							<?php endif; ?>
								<?php
								if($this->send_payments_to_owner){
									$paypal_required='required';
								}else{
									$paypal_required=' ';
								}
								?>
								<div class="control-group">
									<label class="control-label" for="paypal_email">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_PAYPAL_EMAIL_TOOLTIP'),
										JText::_('COM_JGIVE_PAYPAL_EMAIL'),
										'',
										JText::_('COM_JGIVE_PAYPAL_EMAIL'));?>
									</label>
									<div class="controls">
										<div class="input-prepend">
											<span class="add-on"><i class="icon-envelope"></i></span>
											<input type="text" id="paypal_email" name="paypal_email" class="<?php echo $paypal_required;?> validate-email" placeholder="<?php echo JText::_('COM_JGIVE_PAYPAL_EMAIL');?>"
											value="<?php if(isset($cdata->paypal_email)) echo $cdata->paypal_email;?>">
										</div>
									</div>
								</div>
							<?php if($show_field==1 OR $internal_use_cnf==0 ): ?>
								<div class="control-group">
									<label class="control-label" for="internal_use">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_INTERNAL_USE_TOOLTIP'),
										JText::_('COM_JGIVE_INTERNAL_USE'),
										'',
										JText::_('COM_JGIVE_INTERNAL_USE'));?>
									</label>
									<div class="controls">
										<textarea rows="5" cols="50" id="internal_use" name="internal_use" placeholder="<?php echo JText::_('COM_JGIVE_INTERNAL_USE_PLACEHOLDER');?>"><?php if(isset($cdata->internal_use)) echo $cdata->internal_use;?></textarea>
									</div>
								</div>
							<?php endif;?>
							</fieldset>
						</div>
					</div>
				</li>
				<!-- End tab2 -->
			<?php
			if($show_field==1 OR $give_back_cnf==0 )
			{
			?>
				<!--Start tab3fb-->
				<li id="com_jgveTab3fb" class="tab-pane">
					<div class="row-fluid">
						<div class="span12">
							<fieldset>
								<?php
									if(isset($cdata->givebacks))//for edit - recreate giveback blocks
									{
										$i=1;
										foreach($cdata->givebacks as $giveback)
										{
										?>
											<div id="jgive_container<?php echo $i;?>" class="jgive_container">
												<div class="com_jgive_repeating_block" >
													<div class="control-group">
														<label class="control-label" for="give_back_value">
															<?php echo JHtml::tooltip(
															JText::_('COM_JGIVE_GIVE_BACK_VALUE_TOOLTIP'),
															JText::_('COM_JGIVE_GIVEBACK_VALUE'),
															'',
															JText::_('COM_JGIVE_GIVEBACK_VALUE'));?>
														</label>
														<div class="controls">
															 <div class="input-prepend input-append">
																<input type="hidden"  class="" name="ids[]" value="<?php echo $giveback->id; ?>" >
																<input type="hidden"  class="" name="give_back_order[]" value="<?php echo $giveback->order; ?>" >
																<input type="text" id="give_back_value<?php echo $i;?>" name="give_back_value[]"  placeholder="<?php echo JText::_('COM_JGIVE_GIVEBACK_VALUE');?>" value="<?php echo $giveback->amount;?>" class="give_back_value validate-numeric">
																<span class="add-on"><?php echo $this->currency_code;?></span>
															</div>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">
															<?php echo JHtml::tooltip(
															JText::_('COM_JGIVE_GIVE_BACK_DETAILS_TOOLTIP'),
															JText::_('COM_JGIVE_GIVE_BACK_DETAILS'),
															'',
															JText::_('COM_JGIVE_GIVE_BACK_DETAILS'));?>
														</label>
														<div class="controls">
															<textarea rows="4" cols="50" id="give_back_details<?php echo $i;?>" name="give_back_details[]" placeholder="<?php echo JText::_('COM_JGIVE_GIVE_BACK_DETAILS');?>"><?php echo $giveback->description;?></textarea>
														</div>
													</div>
												<!-- Added by Sneha -->
													<div class="control-group">
														<label class="control-label">
															<?php echo JHtml::tooltip(
															JText::_('COM_JGIVE_GIVE_BACK_QUANTITY_TOOLTIP'),
															JText::_('COM_JGIVE_GIVEBACK_QUANTITY'),
															'',
															JText::_('COM_JGIVE_GIVEBACK_QUANTITY'));?>
														</label>
														<div class="controls">
															<input type="text" id="give_back_quantity<?php echo $i;?>" name="give_back_quantity[]" placeholder="<?php echo JText::_('COM_JGIVE_GIVEBACK_QUANTITY');?>" value="<?php echo $giveback->total_quantity;?>" >
														</div>
													</div>
													<div class="control-group">
														<label class="control-label" >
															<?php echo JHtml::tooltip(
															JText::_('COM_JGIVE_IMAGE_TOOLTIP'),
															JText::_('COM_JGIVE_IMAGE'),
															'',
															JText::_('COM_JGIVE_IMAGE'));?></label>
															<div class="controls">
																<?php
																$giveback_image = 'Giveback Image';
																if(isset($cdata->givebacks) && count($cdata->givebacks))
																{
																	?>
																	<input type="file" id="coupon_image[<?php echo $i;?>]" name="coupon_image[]" placeholder="<?php echo JText::_('COM_JGIVE_IMAGE');?>" accept="image/*">
																	<?php
																		if (!empty($giveback->image_path))
																		{
																		?>
																		<div class="text-warning">
																			<?php echo JText::_('COM_JGIVE_EXISTING_IMAGE_MSG');?>
																		</div>
																		<div class="text-info">
																			<?php echo JText::_('COM_JGIVE_EXISTING_IMAGE');?>
																		</div>
																		<div>
																			<?php
																			echo "<img class='img-rounded com_jgive_img_128_128' src='".JUri::root().$giveback->image_path."' alt='" . $giveback_image . "'/>";
																			?>
																		</div>
																	<?php
																		}
																}
																else//while editing image field is not required
																{
																	?>
																	<input type="file" id="coupon_image" name="coupon_image[]" placeholder="<?php echo JText::_('COM_JGIVE_IMAGE');?>" class="required" accept="image/*">
																	<?php
																}
																?>
															</div>
													</div>
												<!--Added by Sneha ends-->
												</div>
												<div class='com_jgive_remove_button pull-right'>
													<button class='btn btn-mini btn-primary' type='button' id='remove<?php echo $i;?>'
														onclick="removeClone('jgive_container<?php echo $i;?>','jgive_container');" title="<?php echo JText::_('COM_JGIVE_REMOVE_TOOLTIP');?>" >
														<i class="icon-minus-sign icon-white"></i>
													</button>
												</div>
												<div class="clearfix"></div>
												<hr class="hr hr-condensed"/>
											</div>
										<?php
										$i++;
										}
									}
								?>
								<!--This is a repating block of html-->
								<div id="jgive_container" class="jgive_container">
									<hr class="hr hr-condensed"/>
									<div class="com_jgive_repeating_block">
										<div class="control-group">
											<label class="control-label" for="give_back_value">
												<?php echo JHtml::tooltip(
												JText::_('COM_JGIVE_GIVE_BACK_VALUE_TOOLTIP'),
												JText::_('COM_JGIVE_GIVEBACK_VALUE'),
												'',
												JText::_('COM_JGIVE_GIVEBACK_VALUE'));?>
											</label>
											<div class="controls">
												 <div class="input-prepend input-append">
													<input type="hidden"  class="" name="ids[]" value="" >
													<input type="hidden"  class="" name="give_back_order[]" value="" >
													<input type="text" id="give_back_value" name="give_back_value[]"
													placeholder="<?php echo JText::_('COM_JGIVE_GIVEBACK_VALUE');?>" class="validate-numeric">
													<span class="add-on"><?php echo $this->currency_code;?></span>
												</div>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label " for="give_back_details">
												<?php echo JHtml::tooltip(
												JText::_('COM_JGIVE_GIVE_BACK_DETAILS_TOOLTIP'),
												JText::_('COM_JGIVE_GIVE_BACK_DETAILS'),
												'',
												JText::_('COM_JGIVE_GIVE_BACK_DETAILS'));?>
											</label>
											<div class="controls">
												<textarea rows="4" cols="50" id="give_back_details" name="give_back_details[]" placeholder="<?php echo JText::_('COM_JGIVE_GIVE_BACK_DETAILS');?>"></textarea>
											</div>
										</div>
										<!-- Added by Sneha -->
										<div class="control-group">
											<label class="control-label" for="give_back_quantity">
												<?php echo JHtml::tooltip(
												JText::_('COM_JGIVE_GIVE_BACK_QUANTITY_TOOLTIP'),
												JText::_('COM_JGIVE_GIVEBACK_QUANTITY'),
												'',
												JText::_('COM_JGIVE_GIVEBACK_QUANTITY'));?>
											</label>
											<div class="controls">
												<input type="text" id="give_back_quantity" name="give_back_quantity[]" placeholder="<?php echo JText::_('COM_JGIVE_GIVEBACK_QUANTITY');?>" class="validate-numeric">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="coupon_image">
												<?php echo JHtml::tooltip(
												JText::_('COM_JGIVE_IMAGE_TOOLTIP'),
												JText::_('COM_JGIVE_IMAGE'),
												'',
												JText::_('COM_JGIVE_IMAGE'));?>
											</label>
											<div class="controls">
												<input type="file" id="coupon_image" name="coupon_image[]" placeholder="<?php echo JText::_('COM_JGIVE_IMAGE');?>" accept="image/*">
											</div>
										</div>
									</div>
									<div>&nbsp;</div>
								</div>
								<div style="clear:both"></div>
								<hr class="hr hr-condensed"/>
								<div class="com_jgive_add_button">
									<button class="btn btn-mini btn-primary " type="button" id='add'
									onclick="addClone('jgive_container','jgive_container');"
									title='<?php echo JText::_('COM_JGIVE_ADD_MORE_TOOLTIP');?>'>
										<i class="<?php echo $jgive_icon_plus; ?> icon-white"></i>
									</button>
								</div>
							</fieldset>
						</div>
					</div>
				</li>
				<!--End tab3fb-->
			<?php
			}?>
				<!--Start tab4-->
				<li id="com_jgveTab4fb" class="tab-pane">
					<div class="row-fluid">
						<div class="span12">
							<fieldset>
								<!--avatar -->
								<div class="control-group">
									<label class="control-label" for="camp_image">
										<?php echo JHtml::tooltip(
										JText::_('COM_JGIVE_IMAGE_TOOLTIP'),
										JText::_('COM_JGIVE_IMAGE'),
										'',
										JText::_('COM_JGIVE_IMAGE').' * ');?>
									</label>
										<div class="controls">
											<?php
											$camp_main_image = 'Campaign Main Image';
											if(isset($cdata->images) && count($cdata->images))
											{
												?>
												<input type="file" id="camp_image" name="camp_image" placeholder="<?php echo JText::_('COM_JGIVE_IMAGE');?>" accept="image/*">
												<div class="text-warning">
													<?php echo JText::_('COM_JGIVE_EXISTING_IMAGE_MSG');?>
												</div>
												<div class="text-info">
													<?php echo JText::_('COM_JGIVE_EXISTING_IMAGE');?>
												</div>
												<div>
													<?php
													foreach($cdata->images as $img)
													{
														if($img->gallery==0)
														{
															if(JFile::exists(JPATH_SITE.DS.$img->path))
															{
																echo "<input type='hidden' name='main_img_id' value=".$img->id.">";
																echo "<img class='img-rounded com_jgive_img_128_128' src='".JUri::root().$img->path."' alt='". $camp_main_image ."'/>";
																break;//print only 1 image
															}
															else
															{
																$path='images'.DS.'jGive'.DS;
																//get original image name to find it resize images (S,M,L)
																$org_file_after_removing_path=trim(str_replace($path,'S_',$img->path));
																$img_link= JUri::root().$path.$org_file_after_removing_path;
																echo "<input type='hidden' name='main_img_id' value=".$img->id.">";
																echo "<img class='img-rounded com_jgive_img_128_128' src='".$img_link."' alt='". $camp_main_image ."'/>";
																break;
															}
														}
													}
													?>
												</div>
												<?php
											}
											else//while editing image field is not required
											{
												?>
												<input type='hidden' name='main_img_id' value="">
												<input type="file" id="camp_image" name="camp_image" placeholder="<?php echo JText::_('COM_JGIVE_IMAGE');?>" class="required" accept="image/*">
												<?php
											}
											?>
											<!-- File extensions & size -->
											<div class="alert alert-warning">
												<?php echo JText::sprintf('COM_JGIVE_IMAGE_SIZE', $this->params->get('large_width'), $this->params->get('large_height'));?>
												<br/>
												<?php echo JText::_("COM_JGIVE_IMAGE_EXTENSIONS"); ?>
												<br/>
												<?php echo JText::sprintf('COM_JGIVE_MAX_ALLOWED_FILE_SIZE', $this->params->get( 'max_size' )); ?>
											</div>
										</div>
								</div>
								<?php if($this->params->get('img_gallery')){ ?>
								<div class="control-group imagediv" id="imagediv">
									<label class="control-label"><?php echo JHtml::tooltip(JText::_('COM_JGIVE_PROD_IMG_TOOLTIP'), JText::_('COM_JGIVE_PROD_IMG'), '',JText::_('COM_JGIVE_PROD_IMG'));?></label>
									<div class="controls">
										<?php
										if(!empty($cdata->images))
										{ ?>
										<span class="alert alert-warning"><?php echo JText::_('COM_JGIVE_UNCHECK_TO_REMOVE_EXISTING_IMAGE');?></span>
										<div class="clearfix">
										<?php
											foreach($cdata->images as $img)
											{
												if ($img->gallery && $img->isvideo == 0)
												{
													?>
													<div class="pull-left jgive_images">
														<input type='checkbox' class="img_checkbox" name='existing_imgs_ids[]' value='<?php echo $img->id;?>' checked>
														<?php
														echo "<img class='img-rounded com_jgive_img_128_128' src='".JUri::root().$img->path."' />";
														?>
													</div>
													<?php
												}
											}
											?>
										</div>
										<?php
										}
										?>
										<div class="row-fluid">
											<div class="span7 addMoreImage">
												<?php
												/*@TODO JUGAD done for add more images display */
												if(version_compare(JVERSION, '3.0', 'lt')) { ?>
												<span class="filediv" id="filediv" >
													<input  type="file" name="jgive_img_gallery[]"  id="avatar" placeholder="" class=""  accept="image/*" onchange="checkFileExtension(this)">
												</span>
												<?php }
												else{ ?>
													<span class="filediv" id="filediv" >
														<input  type="file" name="jgive_img_gallery[]"  id="avatar" placeholder="" class=""  accept="image/*" onchange="checkFileExtension(this)">
													</span>
												<?php } ?>
											<!-- ADD MORE BTN-->
													<span class="addmore"  id="addmoreid"  id="addmoreid" >
														<button onclick="addmoreImg('filediv','filediv');" type="button" class="btn btn-mini btn-primary" title="<?php echo JText::_('COM_JGIVE_IMAGE_ADD_MORE');?>">
															<i class="<?php echo COM_JGIVE_ICON_PLUS;?> icon-white "></i>
														</button>
													</span>
											</div>
										</div> <!--END OF ROW FLUID -->
										<!-- File extensions & size -->
										<div class="alert alert-warning">
											<?php echo JText::_("COM_JGIVE_GALLERY_IMAGE_EXT"); ?>
											<br/>

											<?php echo JText::sprintf('COM_JGIVE_MAX_ALLOWED_FILE_SIZE', $this->params->get( 'max_size' )); ?>
										</div>
									</div> <!-- END OF CONTROL-->
								</div><!-- END OF control-group -->
								<?php } ?>
								<input type="hidden" name="option" value="com_jgive"/>
								<input type="hidden" name="task" value="campaign.<?php echo $this->task;?>"/>

								<?php
									if (isset($cdata->id))
									{
								?>
										<input type="hidden" name="cid" value="<?php echo $cdata->id;?>"/>
										<input type="hidden" name="img_id" value="<?php echo $cdata->images[0]->id;?>"/>
								<?php
									}
								?>
								<?php
								if (!$this->send_payments_to_owner && $this->commission_fee > 0)
								{
									?>
									<div class="alert alert-info">
										<em><i>
											<?php
												echo JText::sprintf('COM_JGIVE_COMMISSION_FEE_NOTICE', $this->commission_fee . '%');
											?>
										</i></em>
									</div>
									<?php
								}

								if ($this->admin_approval)
								{
								}
								?>
							</fieldset>
						</div>
					</div>
				</li>
				<!--End tab4-->
				<!--Start tab5-->
				<?php
				if ( $this->params->get('video_gallery'))
				{
				?>
				<li id="com_jgveTab5fb" class="tab-pane ">
					<?php
						echo $this->loadTemplate('videos');
					?>
				</li>
				<?php
				}
				?>
				<!--End tab5-->
				<?php if ($this->form_extra): ?>
					<li id="com_jgveTab6fb" class="tab-pane">
						<?php echo $this->loadTemplate('extrafields'); ?>
					</li>
				<?php else: ?>
					<li id="com_jgveTab6fb" class="tab-pane">
						<div class="alert alert-info">
							<?php echo JText::_('COM_JGIVE_CAMPAIGN_OTHER_DETAILS_SAVE_PROD_MSG');?>
						</div>
					</li>
				<?php endif; ?>
			</ul>
			<hr/>
			<div class="section-content footer">
				<ul class="nav nav-pills ">
					<li id="previous-btn" style="display: none;">
						<button class="previous btn " type="button" onclick="stepsWizard('previous')" >
							<i class="icon-arrow-left jgive-icon-font-size"></i><?php echo JText::_('COM_JGIVE_PRE_BTN'); ?>
						</button>
					</li>

					<li id="next-btn" class="pull-right" >
						<button class="next btn btn-primary" type="button" onclick="stepsWizard('next')" >
							<?php echo JText::_('COM_JGIVE_NEXT_BTN'); ?>
							<i class="icon-arrow-right icon-white jgive-icon-font-size"></i>
						</button>
					</li>

					<li id="submit-btn" class="pull-right jgive_disply_none" >
						<button class="btn btn-success validate com_jgive_button"><?php echo JText::_('COM_JGIVE_BUTTON_SAVE_CAMPAIGN'); ?></button>
					</li>
				</ul>
				<!-- // Action -->
			</div>
			<hr/>
			<?php echo JHtml::_('form.token');?>
	</form>
	</div>
</div>
