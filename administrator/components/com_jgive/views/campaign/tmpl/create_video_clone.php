<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
?>
<div class="new-video" name="<?php echo JText::_("COM_JGIVE_ADD_NEW_VIDE"); ?>">
	<div id="jgive_mediaContainer" class="jgive_mediaContainer jgive_container_border jgive_disply_none">

		<span class="pull-right" style="padding-right:10px">
			<input type="hidden" name="default_marked[]"  value="0" />

			<label class="radio inline btn"
				title="<?php echo JText::_("COM_JGIVE_SET_THIS_VIDEO_AS_DEFAULT"); ?>"
				for="radio">
					<i class="icon-star-empty"></i>
			</label>

			<input class="jgive_display_none" id="radio" type="radio" name="marked_as_default"  value="0" onclick="setDefault(this)" />
		</span>

		<div class="control-group">
			<label class="control-label" for="upload_options" title="<?php echo JText::_('COM_JGIVE_UPLOAD_OPTION_TOOLTIP');?>">
				<?php echo JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION');?>
			</label>

			<div class="controls">
				<select id="upload_options" name="upload_options[]" onchange="videoUploadOption(this);" aria-invalid="false">
					<option value="url"><?php echo JText::_("COM_JGIVE_ENTER_VIDEO_URL"); ?></option>
					<option value="video" selected="selected"><?php echo JText::_("COM_JGIVE_ENTER_VIDEO_UPLOAD") ?></option>
				</select>
			</div>

			<div class="controls">
				<div class="clearfix">&nbsp;</div>

				<div id="upload_file" class="fileupload fileupload-new pull-left" data-provides="fileupload">
					<div class="input-append">
						<div class="uneditable-input span4">
							<span class="fileupload-preview"><?php echo str_ireplace("'","",$this->extArrString); ?>
							</span>
						</div>
						<span class="btn btn-file">
							<span class="fileupload-new">
								<?php echo JText::_("COM_JGIVE_FILE_BROWSE"); ?>
							</span>
							<input class="jgiveMediaFileUploadEle " id="jgiveMediaFile" type="file" name="jgiveMediaFile" />
						</span>
					</div>

					<div class="progress progress-primary progress-striped ">
						<div class="bar" id="jgive_progress-bar" style="width:0%"></div>
					</div>
				</div>

				<!-- Enter video URL -->
				<input id="video_url"
					type="text"
					name="video_urls[]"
					class="jgive_disply_none input-xlarge"
					placeholder="<?php echo JText::_('COM_JGIVE_ENTER_VIDEO_URL');?>"
					value=""/>

			</div>
		</div>

		<input type="hidden" name="existing_imgs_ids[]" value="0" />
		<input type="hidden" id="ajax_upload_hidden" name="video_files_name[]" value="" />
		<input type="hidden" name="video_ids[]" value="" />
		<!-- file upload END -->

	</div>

	<?php
		if($this->video_container <= $this->maxVideoFilesLimit)
		{ ?>
		<div id="jgive_mediaContainer0" class="jgive_mediaContainer jgive_container_border">

			<span class="pull-right" style="padding-right:10px">
				<input type="hidden" name="default_marked[]"  value="0" />

				<label class="radio inline btn"
					title="<?php echo JText::_("COM_JGIVE_SET_THIS_VIDEO_AS_DEFAULT"); ?>"
					for="radio0">
						<i class="icon-star-empty"></i>
				</label>
				<input id="radio0" class="jgive_display_none" type="radio" name="marked_as_default"  value="0" onclick="setDefault(this)" />
			</span>

			<div class="control-group">
				<label class="control-label" for="upload_options" title="<?php echo JText::_('COM_JGIVE_UPLOAD_OPTION_TOOLTIP');?>">
					<?php echo JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION');?>
				</label>

				<div class="controls">
					<select id="upload_options0" name="upload_options[]" onchange="videoUploadOption(this);" aria-invalid="false">
						<option value="url"><?php echo JText::_("COM_JGIVE_ENTER_VIDEO_URL"); ?></option>
						<option value="video" selected="selected"><?php echo JText::_("COM_JGIVE_ENTER_VIDEO_UPLOAD") ?></option>
					</select>
				</div>

				<div class="controls">
					<div class="clearfix">&nbsp;</div>

					<div id="upload_file0" class="fileupload fileupload-new pull-left" data-provides="fileupload">
						<div class="input-append">
							<div class="uneditable-input span4">
								<span class="fileupload-preview"><?php echo str_ireplace("'","",$this->extArrString); ?>
								</span>
							</div>
							<span class="btn btn-file">
								<span class="fileupload-new">
									<?php echo JText::_("COM_JGIVE_FILE_BROWSE"); ?>
								</span>
								<input class="jgiveMediaFileUploadEle " id="jgiveMediaFile0" type="file" name="jgiveMediaFile" >
							</span>
						</div>
						<span class="label label-info"><?php echo JText::_(''); ?></span>
						<div class="progress progress-primary progress-striped">
							<div class="bar" id="jgive_progress-bar0" style="width:0%"></div>
						</div>
					</div>

					<!-- Enter video URL -->
					<input id="video_url0"
						type="text"
						name="video_urls[]"
						class="jgive_disply_none input-xlarge"
						placeholder="<?php echo JText::_('COM_JGIVE_ENTER_VIDEO_URL');?>"
						value=""/>

				</div>
			</div>

			<input type="hidden" name="existing_imgs_ids[]" value="0" />
			<input type="hidden" id="ajax_upload_hidden0" name="video_files_name[]" value="" />
			<input type="hidden" name="video_ids[]" value="" />
			<!-- file upload END -->

		</div>
		<?php
		} ?>
	<button class="btn btn-primary pull-right"
		type="button"
		id='add'
		onclick="addCloneMedia('jgive_mediaContainer','jgive_mediaContainer');" >
			<i class="icon-plus-sign icon-white"></i>
			<?php echo JText::_('COM_JGIVE_ADD_MORE'); ?>
	</button>
	<div class="clearfix"></div>
</div>
