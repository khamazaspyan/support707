<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

/**
 * @TO DO, get data into media array instead of images
 * $k = 0;
 */

$count = 0;

if (!empty($this->cdata->video))
{
	$count = count($this->cdata->video);
}


$no_video_found = 0;

// Video thumbnail
if (!empty($this->cdata->id) )
{
?>
	<div class="existing-videos-files-urls" name="<?php echo JText::_("COM_JGIVE_EXISTING_VIDEOS"); ?>" >
		<ul class="unstyled">

			<?php

			if ($count > 0)
			{
				for ($j = 1; $j <= $count; $j++)
				{
					$video = $this->cdata->video[$j - 1];

					switch ($video->type)
					{
						case 'video':
							$no_video_found = 1; ?>

							<li id="videoid<?php echo $this->video_container; ?>" class="jgive_container_border">
								<div class="row-fluid">
									<?php
									// Code to display thumbanil of existing video -Added by Nidhi
									if (($video->id)  && ($video->type))
									{
										if ($video->thumb_path)
										{
											//  Show thumbnail
											$thumbSrc = JUri::root() . $video->thumb_path;
											$fileInfo = new SplFileInfo($video->path);

											if (!file_exists(JPATH_SITE . $video->thumb_path))
											{
												$thumbSrc = JUri::root(true) . '/media/com_jgive/images/no_thumb.png';
											}

											$link = JRoute::_(
											JUri::root() . "index.php?option=com_jgive&view=campaign&layout=single_playvideo&vid=" .
											$video->id . "&type=" . trim($video->type) . "&tmpl=component"
											);?>
											<div class="span10">
												<a rel="{handler: 'iframe', size: {x: 600, y: 600}}" href="<?php echo $link; ?>" class="modal ">
													<img src="<?php echo $thumbSrc; ?>" data-src="holder.js/300x200"  class="img-rounded com_jgive_img_96_96 thumbnail"/>
												</a>
												<?php echo substr(basename($video->path), 0, 15) . '...' . $fileInfo->getExtension(); ?>
											</div>
										<?php
										} ?>

										<div class="span2">

											<label class="radio inline btn"
												title="<?php echo JText::_("COM_JGIVE_SET_THIS_VIDEO_AS_DEFAULT"); ?>"
												for="radio<?php echo $this->video_container; ?>">
												<?php
												if ($video->default == 0)
												{
													?>
													<i class="icon-star-empty"></i>
													<?php
												}
												else
												{
													?>
													<img src="<?php echo JUri::root(true); ?>/media/com_jgive/images/featured.png" width="13" height="13" border="0" >
												<?php
												} ?>
											</label>

											<input class="jgive_display_none"
												type="radio"
												id="radio<?php echo $this->video_container; ?>"
												name="marked_as_default"
												<?php echo $video->default == 1 ? 'checked="checked"': ''; ?>
												onclick="setDefault(this,'<?php echo $video->id; ?>','<?php echo $this->cdata->id; ?>','video')" />

											<button class="btn"
												type="button"
												id="removeClone<?php echo $this->video_container; ?>"
												onclick="deleteVideo('<?php echo $video->type; ?>','<?php echo $video->id; ?>','videoid<?php echo $this->video_container; ?>');"
												title="<?php echo JText::_('COM_JGIVE_DELETE_VIDEO'); ?>">
												<i class="icon icon-white icon-trash"></i>
											</button>

											<input type="hidden" name="existing_imgs_ids[]" value="<?php echo $video->id; ?>" />
											<input type="hidden" name="upload_options[]" value="video" />

											<input type="hidden" name="default_marked[]" value="<?php echo $video->default; ?>" />

											<!-- Enter video URL -->
											<input type="hidden" name="video_urls[]" value="" />
											<input type="hidden" name="video_files_name[]" value="" />
											<input type="hidden" name="video_ids[]" value="<?php echo $video->id; ?>" />

										</div>
										<?php
									} ?>

								</div>
								<hr/>
							</li>
								<?php
							$this->video_container++;
						break;

						case 'youtube' || 'vimeo':
							$no_video_found = 1; ?>

							<li  id="jgive_mediaContainer<?php echo $this->video_container; ?>" class="jgive_mediaContainer jgive_container_border">
								<div class=" row-fluid">
									<div class="span10">
										<div class="control-group">
											<label class="control-label" for="upload_options" title="<?php echo JText::_('COM_JGIVE_UPLOAD_OPTION_TOOLTIP');?>">
												<?php echo JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION');?>
											</label>

												<div class="controls">
													<select id="upload_options<?php echo $this->video_container; ?>"
														name="upload_options[]"
														onchange="videoUploadOption(this, <?php echo $this->video_container; ?>);"
														aria-invalid="false">
															<option value="url" selected="selected" >
																	<?php echo JText::_("COM_JGIVE_ENTER_VIDEO_URL"); ?>
															</option>
															<option value="video" >
																<?php echo JText::_("COM_JGIVE_ENTER_VIDEO_UPLOAD") ?>
															</option>
													</select>
												</div>

												<div class="clearfix"></div>
												<div class="controls">

													<!--Choose video -->
													<div id="upload_file<?php echo $this->video_container; ?>"
														class="fileupload fileupload-new pull-left  jgive_disply_none"
														data-provides="fileupload" >

														<div class="input-append">
															<div class="uneditable-input span4">
																<span class="fileupload-preview"><?php echo str_ireplace("'", "", $this->extArrString); ?>
																</span>
															</div>
															<span class="btn btn-file">
																<span class="fileupload-new">
																	<?php echo JText::_("COM_JGIVE_FILE_BROWSE"); ?>
																</span>
																<input class="jgiveMediaFileUploadEle "
																	id="jgiveMediaFile<?php echo $this->video_container; ?>"
																	type="file"
																	name="jgiveMediaFile" />
															</span>
														</div>

														<div class="progress progress-primary progress-striped">
															<div class="bar"
																id="jgive_progress-bar<?php echo $this->video_container; ?>"
																style="width:0%">
															</div>
														</div>

													</div>

													<!-- Enter video URL -->
													<input id="video_url<?php echo $this->video_container; ?>"
														type="text"
														name="video_urls[]"
														class="input-xlarge <?php echo $video->type === 'video' ? 'jgive_disply_none' : ""; ?>"
														placeholder="<?php echo JText::_('COM_JGIVE_ENTER_VIDEO_URL');?>"
														value="<?php echo $video->url; ?>" />

														<?php
														// Show video preview link
														$prew_link = JRoute::_(
														JUri::root(true) . "/index.php?option=com_jgive&view=campaign&layout=single_playvideo&vid=" . $video->id . "&type= " .
														trim($video->type) . "&tmpl=component"
														);
														?>
												</div>
										</div>
									</div>
									<div class="span2">
										<label class="radio inline btn"
											title="<?php echo JText::_("COM_JGIVE_SET_THIS_VIDEO_AS_DEFAULT"); ?>"
											for="radio<?php echo $this->video_container; ?>">
												<?php
												if ($video->default == 0)
												{
													?>
													<i class="icon-star-empty"></i>
													<?php
												}
												else
												{
													?>
													<img src="<?php echo JUri::root(true); ?>/media/com_jgive/images/featured.png" width="13" height="13" border="0" >
													<?php
												} ?>
										</label>

										<input class="jgive_display_none"
										id="radio<?php echo $this->video_container; ?>"
										type="radio" name="marked_as_default" <?php echo $video->default == 1 ? 'checked="checked"': ''; ?>
										onclick="setDefault(this,'<?php echo $video->id; ?>','<?php echo $this->cdata->id; ?>','url', '<?php echo $this->video_container; ?>')" />

										<button class="btn"
										type="button" id="removeClone<?php echo $this->video_container; ?>"
										onclick="deleteVideo('url',<?php echo $video->id; ?>,
										'jgive_mediaContainer<?php echo $this->video_container; ?>');"
										title="<?php echo JText::_('COM_JGIVE_DELETE_VIDEO'); ?>">
												<i class="icon icon-white icon-trash"></i>
										</button>


									<input type="hidden"
										id="ajax_upload_hidden<?php echo $this->video_container; ?>"
										name="video_files_name[]"
										value="" />

									<input type="hidden"
										name="existing_imgs_ids[]"
										value="<?php echo $video->id; ?>" />

									<input type="hidden"
										name="default_marked[]"
										value="<?php echo $video->default; ?>" />

									<input type="hidden"
										name="video_ids[]"
										value="<?php echo $video->id; ?>" />
								</div>
								</div>
								<hr/>
							</li>
								<?php
							$this->video_container++;
						break;
					}
				}
			}?>
		</ul>
		<?php
		if ($no_video_found == 0)
		{
			echo JText::_("COM_JGIVE_NO_VIDEO_UPLOADED");
		}
		?>
	</div>
<?php
}
