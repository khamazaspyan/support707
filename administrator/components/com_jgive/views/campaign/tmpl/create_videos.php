<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Load resumable js library
$document = JFactory::getDocument();
$document->addScript(JUri::root(true) . '/media/com_jgive/vendors/js/resumable.js');

// Get existing uploaded videos count
$this->mediaCount = !empty ($this->getMediaDetail) ? count($this->getMediaDetail) - 1 : 0;
$mediaDetail = !empty ($this->getMediaDetail) ? $this->getMediaDetail:array();

// Get max videos files allowed to upload limit
$this->maxVideoFilesLimit = $this->params->get('max_videos', 6);

// Get allowed to upload files extensions
$allowedFileExtensions = $this->params->get('allowedFileExtensions', "avi,flv,mkv,mov,mp4,wav,wma,wmv");
$extArr = explode(',', $allowedFileExtensions);
$this->extArrString = "'" . implode("','", $extArr) . "'";

$this->video_container = 1;

?>


<div class="row-fluid">
<!--
	<div id="jgive_validation_message" class="alert alert-success ">
		 <h4><?php echo JText::_("COM_JGIVE_ERRORS") ?></h4>
		<?php echo JText::_("COM_JGIVE_FILL_MANDATORY_FIELDS_DATA"); ?>
	</div>

	<div class="clearfix">&nbsp;</div>
-->

	<?php echo $this->loadTemplate('video_uploaded');
		echo $this->loadTemplate('video_clone');

		$video_on_details_page1 = $video_on_details_page2 = '';

		if (isset($this->cdata->video_on_details_page))
		{
			if ($this->cdata->video_on_details_page == 0)
			{
				$video_on_details_page1 = 'checked';
			}
			else
			{
				$video_on_details_page2 = 'checked';
			}
		}
		else
		{
			$video_on_details_page1 = 'checked';
		}
	?>

	<div class="control-group">
		<label for="video_on_details_page1" class="control-label" name="<?php echo JText::_('COM_JGIVE_VIDEO_IMG_TOOLTIP');?>" >
			<?php echo JText::_('COM_JGIVE_VIDEO_IMG');?>
		</label>
		<div class="controls">
			<label class="radio inline">
				<input type="radio"
					name="video_on_details_page"
					id="video_on_details_page1"
					value="0"
					<?php echo $video_on_details_page1;?>
					/>
						<?php echo JText::_('COM_JGIVE_IMG');?>
			</label>

			<label class="radio inline">
				<input type="radio"
					name="video_on_details_page"
					id="video_on_details_page2"
					value="1"
					<?php echo $video_on_details_page2;?>
					onchange="checkDefaultVideoIsSet()" />
						<?php echo JText::_('COM_JGIVE_VIDEO');?>
			</label>
		</div>
	</div>

</div>

<script type="text/javascript">

	var num = parseInt("<?php echo $this->video_container + 1; ?>");
	/** Globle params */
	var media_current_id = <?php echo $this->mediaCount;?>;

	/** As media start from 0 */
	var maxAllowedMedia = <?php echo $this->maxVideoFilesLimit;?>;

	var r = new Array();

	var fileUpload_maxFileSize=<?php echo $this->params->get('max_video_file_size') * 1024 * 1024;?>;
	var fileUpload_acceptFileTypes = [<?php
			$arr = explode(',', $this->params->get('allowedFileExtensions'));
			echo "'" . implode("', '", $arr) . "'";?>];

	/** Upload ajax target */
	var jgiveUploadTarget = '<?php echo Juri::root();?>index.php?option=com_jgive&task=campaign.videoUpload&tmpl=component' ;

	/** This function is used to add attribute(eg. color) */

	/** Add clone script*/
	function addCloneMedia(rId,rClass)
	{
		/** CHECK FOR MEDIA UPLAD LIMIT */
		var mdiaCount = techjoomla.jQuery(".jgive_container_border").length ;

		/** Check if max allowed to upload videos count reached */
		if (mdiaCount <= (maxAllowedMedia))
		{
			/** CREATE REMOVE BUTTON */
			var removeButton = "<button class='btn pull-right' type='button' id='removeClone"+num+"'";
			removeButton += "onclick=\"removeClone('"+rId+num+"');\" title=\"<?php echo JText::_('COM_JGIVE_DELETE_VIDEO');?>\" >";
			removeButton += "<i class=\"icon icon-white icon-trash\"></i></button>";

			/** Clone video upload div */
			var newElem=techjoomla.jQuery('#'+rId).clone().attr('id',rId+num);

			/** Update cloned object with new ids */
			newElem.find('label[for=\"radio\"]').attr({'for': 'radio'+num});
			newElem.find('input[id=\"radio\"]').attr({'id': 'radio'+num});
			newElem.find('input[id=\"marked_as_default\"]').attr({'id': 'marked_as_default'+num,'value':+num});
			newElem.find('input[id=\"jgiveMediaFile\"]').attr({'id': 'jgiveMediaFile'+num,'value':''});
			newElem.find('input[id=\"ajax_upload_hidden\"]').attr({'id': 'ajax_upload_hidden'+num,'value':''});
			newElem.find('div[id=\"jgive_progress-bar_container\"]').attr({'id': 'jgive_progress-bar_container'+num});
			newElem.find('div[id=\"jgive_progress-bar\"]').attr({'id': 'jgive_progress-bar'+num,'value':'','style':''});
			newElem.find('button[id=\"removeClone\"]').attr({'id': 'removeClone'+num});
			newElem.find('select[id=\"upload_options\"]').attr({'id': 'upload_options'+num});
			newElem.find('input[id=\"video_url\"]').attr({'id': 'video_url'+num, 'value':'','class':'jgive_disply_none input-xlarge'});
			newElem.find('div[id=\"upload_file\"]').attr({'id': 'upload_file'+num, 'style':''});

			newElem.find('button[id=\"removeClone'+num+'\"]').removeClass('jgive_disply_none');
			techjoomla.jQuery('.'+rClass+':last').after(newElem);
			techjoomla.jQuery('#jgive_mediaContainer'+num).removeClass('jgive_disply_none');
			techjoomla.jQuery('div#jgive_mediaContainer'+num).prepend(removeButton);

			/** Create resumable empty object to video upload **/
			var result = jgiveVideoUpload(num);

			/** Execute script **/
			eval(result);
		}
		else
		{
			/** If max allowed to upload videos count reached */
			alert("<?php echo JText::sprintf('COM_JGIVE_REACHED_MAX_VIDEO_LIMIT', $this->maxVideoFilesLimit); ?>");
		}
		num++;
	}

	/**
	* Resumablejs function to upload video in chuck
	*
	* @params  current div index
	*/
	function jgiveVideoUpload(index)
	{
		/**
		A new Resumable object is created with information of what and where to post:
		**/
		var obj = 'r'+index;

		var str= 	"var "+obj+" = new Resumable({target:'"+jgiveUploadTarget+"', fileParameterName:'jgiveMediaFile"+index+"', chunkSize : 2*1024*1024, query:{}, maxFiles:1, fileType:[<?php echo $this->extArrString;?>], maxFileSize: "+fileUpload_maxFileSize+" });";

		/**
		To allow files to be either selected and drag-dropped, you’ll assign drop target and a DOM item to be clicked for browsing:
		*/
		str = str +  obj+".assignBrowse(document.getElementById('jgiveMediaFile"+index+"'));   ";
		str = str +  obj+".assignDrop(document.getElementById('jgiveMediaFile"+index+"'));   ";

		str = str +  obj+".on('fileSuccess', function(file){ " +
			"var len = file.chunks.length ; " +

			"if (jQuery.parseJSON(file.chunks[len-1].xhr.response).validate.error === 1)  "+
			" { "+
				 ' alert("<?php echo JText::_('COM_JGIVE_FILE_UPLOAD_ERROR_MSG'); ?>"); ' +
			"}" +
			"else "+
			"{  "+
			"	jQuery('#ajax_upload_hidden"+index+ "').val( jQuery.parseJSON(file.chunks[len-1].xhr.response).fileUpload.filePath ); "+
				'alert("<?php echo JText::_('COM_JGIVE_FILE_UPLOAD_SUCCESS_MSG'); ?>"); ' +
			"} "+
		"}); " +
 ";  ";

		/**
		* After this, interaction with Resumable.js is done by listening to events:
		*/
		str = str + obj+".on('fileAdded', function(file, event){ " +
			"jQuery('#jgive_progress-bar"+index+"').css('width',0 +'%'); " +
			obj+".upload(); "+
		"});  "+
		obj +".on('fileError', function(file, message){"+
			"alert(message); "+
		"});		";

		str = str + obj+".on('progress', function(){ "+
			"jQuery('#jgive_progress-bar"+index+"').css('width',(100 * "+obj+".progress(1)) + '%');  "+
			"jQuery('#jgive_progress-bar"+index+"').css('width',(100 * "+obj+".progress(1)) + '%');  "+
		"});		" ;

		str = str + obj+".on('error', function(message, file){ " +
			 " alert(message); " +
		" }); ";

		return str;
	}

	/**
	* Remove video upload clone
	*
	* @params Id of element to remove
	*/
	function removeClone(rId)
	{
		techjoomla.jQuery('#'+rId).remove();
	}

	/**
	* Remove video upload clone
	*
	* @params reference of the element
	*/
	function videoUploadOption(ref, index)
	{
		var selected_val = techjoomla.jQuery('#'+ref.id+" option:selected").val()

		var elementsIndex = (ref.id).replace("upload_options", "");
		techjoomla.jQuery('#video_url'+elementsIndex).removeClass('jgive_disply_none')

		if(selected_val ===  "url")
		{
			techjoomla.jQuery('#upload_file'+elementsIndex).hide();
			techjoomla.jQuery('#video_url'+elementsIndex).show();
			//techjoomla.jQuery('#vide_preview'+elementsIndex).show();
		}
		else if(selected_val ===  "video")
		{
			techjoomla.jQuery('#video_url'+elementsIndex).hide();
			techjoomla.jQuery('#upload_file'+elementsIndex).show();
			//techjoomla.jQuery('#vide_preview'+elementsIndex).hide();
			if(index != undefined)
			{
				var estr = jgiveVideoUpload(index);
				/** Execute script **/
				eval(estr);
			}
		}
	}

</script>


<script type="text/javascript">

	// Declare global variables here
	var root_path = "<?php echo JUri::root(); ?>";

	/** Create resumable empty object for first video upload clone **/
	var Mstr = jgiveVideoUpload(0);

	/** Execute script **/
	eval(Mstr);
</script>
