<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('techjoomla.tjtoolbar.button.csvexport');

/**
 * View class for a list of Donors.
 *
 * @since  1.6
 */
class JGiveViewDonors extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		$user      = JFactory::getUser();

		// Declare FrontendHelper object
		$this->jgiveFrontendHelper = new jgiveFrontendHelper;

		// Default layout is default
		$this->input = JFactory::getApplication()->input;
		$layout = $this->input->get('layout', 'default');
		$this->setLayout($layout);

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jgive'))
		{
			$mainframe->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		// Get campaign itemid
		$this->singleCampaignItemid = $this->jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');

		$this->state = $this->get('State');
		$this->items = $this->get('Items');

		$this->pagination = $this->get('Pagination');

		// Get filter form.
		$this->filterForm = $this->get('FilterForm');

		// Get active filters.
		$this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		$JgiveHelper = new JgiveHelper;
		$JgiveHelper->addSubmenu('donors');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT . '/helpers/donors.php';
		$state = $this->get('State');
		$canDo = JGiveHelpersDonors::getActions();
		$bar    = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_TITLE_DONORS'), 'list');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/donor';

		if (count($this->items) > 0)
		{
			JToolBarHelper::custom('donors.redirectToMassmailing', 'mail.png', '', JText::_('COM_JGIVE_EMAIL_TO_DONORS'));

			$message = array();
			$message['success'] = JText::_("COM_JGIVE_EXPORT_FILE_SUCCESS");
			$message['error'] = JText::_("COM_JGIVE_EXPORT_FILE_ERROR");
			$message['inprogress'] = JText::_("COM_JGIVE_EXPORT_FILE_NOTICE");

			$bar->appendButton('CsvExport', $message);
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_jgive');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_jgive&view=donors');

		$this->extra_sidebar = '';
	}

	/**
	 * Method to order fields
	 *
	 * @return void
	 */
	protected function getSortFields()
	{
		return array
		(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`first_name`' => JText::_('COM_JGIVE_DONORS_FIRST_NAME'),
			'a.`last_name`' => JText::_('COM_JGIVE_DONORS_LAST_NAME'),
			'a.`address`' => JText::_('COM_JGIVE_DONORS_ADDRESS'),
			'a.`address2`' => JText::_('COM_JGIVE_DONORS_ADDRESS2'),
			'a.`city`' => JText::_('COM_JGIVE_DONORS_CITY'),
			'a.`country`' => JText::_('COM_JGIVE_DONORS_COUNTRY'),
			'a.`zip`' => JText::_('COM_JGIVE_DONORS_ZIP'),
			'a.`phone`' => JText::_('COM_JGIVE_DONORS_PHONE'),
			'a.`user_id`' => JText::_('COM_JGIVE_DONORS_USER_ID'),
			'a.`campaign_id`' => JText::_('COM_JGIVE_DONORS_CAMPAIGN_ID'),
			'a.`email`' => JText::_('COM_JGIVE_DONORS_EMAIL'),
		);
	}
}
