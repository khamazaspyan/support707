<?php
/**
 * @version    SVN: <svn_id>
 * @package    Tjfields
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.multiselect');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$saveOrder = $listOrder == 'a.ordering';
$user = JFactory::getUser();
$sortFields = $this->getSortFields();
$jgivehelper   = $this->jgiveFrontendHelper;
?>
<form action="<?php echo JRoute::_('index.php?option=com_jgive&view=donors&layout=' . $this->input->get('layout', '', 'STRING')); ?>" method="post" name="adminForm" id="adminForm">
	<?php

	if(!empty($this->sidebar))
	{?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar;?>
		</div>
		<div id="j-main-container" class="span10">
	<?php
	}
	else
	{?>
		<div id="j-main-container">
	<?php
	}


	// Sorting and Searching Options
	echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
	?>
	<div class="btn-group pull-right hidden-phone">
		<label for="limit" class="element-invisible">
			<?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
		</label>
		<?php echo $this->pagination->getLimitBox(); ?>
	</div>


	<div class="clearfix"> </div>

	<?php
	if (empty($this->items))
	{ ?>
		<div class="clearfix">&nbsp;</div>
		<div class="alert alert-no-items">
			<?php echo JText::_('COM_JGIVE_NO_MATCHING_RESULTS'); ?>
		</div>
	<?php
	}
	else
	{ ?>
		<table class="table table-striped" id="all_donorslist">
			<thead>
				<tr>
					<th width="1%" class="center hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
						title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>"
						onclick="Joomla.checkAll(this)" />
					</th>
					<th class="center" width="5%">
						<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DASHBOARD_DONOR_NAME', 'a.first_name', $listDirn, $listOrder); ?>
					</th>
					<th class="center" width="5%">
						<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DASHBOARD_DONOR_EMAIL', 'a.email', $listDirn, $listOrder); ?>
					</th>
					<th class="center" width="5%">
						<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_ADDRESS', 'a.address', $listDirn, $listOrder); ?>
					</th>
					<th class="center" width="5%">
						<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_PHONE', 'a.phone', $listDirn, $listOrder); ?>
					</th>
					<th class="center" width="5%">
						<?php echo JHtml::_('grid.sort',  'COM_JGIVE_CAMPAIGN_DETAILS', 'a.campaign_id', $listDirn, $listOrder); ?>
					</th>
					<th class="center" width="5%">
						<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_DONATION_AMOUNT', 'o.amount', $listDirn, $listOrder); ?>
					</th>
					<th class="center" width="5%">
						<?php echo JText::_('COM_JGIVE_DONORS_GIVEBACK_DESC'); ?>
					</th>
					<th class="center" width="5%">
						<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_CDATE', 'o.cdate', $listDirn, $listOrder); ?>
					</th>
					<th width="1%" class="center nowrap center hidden-phone">
						<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
				<?php
				if (isset($this->items[0]))
				{
					$colspan = count(get_object_vars($this->items[0]));
				}
				else
				{
					$colspan = 10;
				}
				?>
				<tr>
					<td colspan="<?php echo $colspan ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<?php
				$num = 1;
				foreach ($this->items as $i => $item)
				{

					$canEdit = $user->authorise('core.edit', 'com_jgive');
					if (!$canEdit && $user->authorise('core.edit.own', 'com_jgive'))
					{
						$canEdit = JFactory::getUser()->id == $item->created_by;
					}
					?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="center hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<td class="center" data-title="<?php echo JText::_("COM_JGIVE_DASHBOARD_DONOR_NAME"); ?>">
							<?php if (isset($item->checked_out) && $item->checked_out) { ?>
								<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'donors.', $canCheckin); ?>
							<?php }?>
							<?php echo $this->escape($item->first_name) . " " . $item->last_name; ?>
						</td>
						<td class="center wordbreak" data-title="<?php echo JText::_("COM_JGIVE_DASHBOARD_DONOR_EMAIL"); ?>">
							<?php echo $item->email; ?>
						</td>
						<td class="center" data-title="<?php echo JText::_("COM_JGIVE_DONORS_ADDRESS"); ?>">
							<?php if(empty($item->address) && empty($item->city) && empty($item->state))
							{
								echo '-';
							}
							else
							{?>
								<?php echo !empty($item->address) ? $item->address . ',</br>' : ''; ?>
								<?php echo !empty($item->city) ? $item->city . ',</br>' : ''; ?>
								<?php echo !empty($item->state) ? $item->state . ',</br>' : ''; ?>
								<?php echo !empty($item->country) ? $item->country . ',</br>' : ''; ?>
								<?php echo !empty($item->zip) ? $item->zip . ',</br>' : '-'; ?>
								<?php echo !empty($item->address2) ? $item->address2 . ',</br>' : '-'; ?>
							<?php
							}?>
						</td>
						<td class="center" data-title="<?php echo JText::_("COM_JGIVE_DONORS_PHONE"); ?>">
							<?php echo $item->phone ? $item->phone : " - "; ?>
						</td>
						<td class="center" data-title="<?php echo JText::_("COM_JGIVE_CAMPAIGN_DETAILS"); ?>">
							<a href="<?php echo JUri::root().substr(JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $item->campaign_id . '&Itemid='.$this->singleCampaignItemid),strlen(JUri::base(true))+1);?>"
								title="<?php echo JText::_('COM_JGIVE_DASHBOARD_TOOLTIP_VIEW_ORDER_MSG');?>">
							<?php echo $item->campaigns_title;?>
							</a>
						</td>
						<td class="center" data-title="<?php echo JText::_("COM_JGIVE_DONORS_DONATION_AMOUNT"); ?>">
							<?php echo $jgivehelper->getFormattedPrice($item->donation_amount);?>
						</td>
						<td class="center" data-title="<?php echo JText::_("COM_JGIVE_DONORS_GIVEBACK_DESC"); ?>">
							<?php echo $item->gdesc ? $item->gdesc : ' - ';?>
						</td>
						<td class="center hidden-phone">
							<?php echo JHtml::_('date', $item->cdate, JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3')); ?>
						</td>
						<td class="center hidden-phone">
							<?php echo (int) $item->id; ?>
						</td>
					</tr>
				<?php
				}?>
			</tbody>
		</table>
	<?php
	}?>

		<input type="hidden" id="task" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if(task == 'donors.redirectToMassmailing')
		{
			if(document.adminForm.boxchecked.value == 0)
			{
				var msg = "<?php echo JText::_('COM_JGIVE_MSG_FOR_SELECT_USER'); ?>";
				alert(msg);
				return false;
			}
			else
			{
				Joomla.submitform(task);
			}
		}
		else
		{
			Joomla.submitform(task);
		}
	}
</script>
