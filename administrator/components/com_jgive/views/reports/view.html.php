<?php
/**
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2011-2015 Techjoomla. All rights reserved.
 * @license    GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * Report view class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveViewReports extends JViewLegacy
{
	/**
	 * Function to display.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function display($tpl = null)
	{
		global $mainframe, $option;
		$user = JFactory::getUser();

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jgive'))
		{
			$mainframe->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		$mainframe   = JFactory::getApplication();

		// Load submenu
		$JgiveHelper = new JgiveHelper;
		$JgiveHelper->addSubmenu('reports');

		$option = JFactory::getApplication()->input->get('option');

		$campaignHelper      = new campaignHelper;

		// Get params
		$params              = JComponentHelper::getParams('com_jgive');
		$this->currency_code = $params->get('currency');

		// Default layout is default
		$layout = JFactory::getApplication()->input->get('layout', 'default');
		$this->setLayout($layout);

		$filter_order_Dir = $mainframe->getUserStateFromRequest('com_jgive.filter_order_Dir', 'filter_order_Dir', 'desc', 'word');
		$filter_type      = $mainframe->getUserStateFromRequest('com_jgive.filter_order', 'filter_order', 'goal_amount', 'string');

		if ($layout == 'default')
		{
			// Set filter options
			$filter_campaign_options = array();

			// Use helpr function
			$campaign_list = $campaignHelper->getAllCampaignOptions();

			if (!empty($campaign_list))
			{
				foreach ($campaign_list as $key => $campaign)
				{
					$filter_campaign_options[] = JHtml::_('select.option', $campaign->id, $campaign->title);
				}
			}

			$this->filter_campaign_options = $filter_campaign_options;

			// Get Campaigns Type fillter
			$this->campaign_type_filter_options = $campaignHelper->getCampaignTypeFilterOptions();
			$filter_campaign_type               = $mainframe->getUserStateFromRequest($option . 'filter_campaign_type', 'filter_campaign_type', '', 'string');
			$lists['filter_campaign_type']      = $filter_campaign_type;
			$this->lists                        = $lists;

			$filter_campaign          = $mainframe->getUserStateFromRequest($option . 'filter_campaign', 'filter_campaign', '', 'string');
			$lists['filter_campaign'] = $filter_campaign;
			$this->lists              = $lists;

			$cwdonations       = $this->get('CampaignWiseDonations');
			$this->cwdonations = $cwdonations;

			$total       = $this->get('Total');
			$this->total = $total;

			$pagination       = $this->get('Pagination');
			$this->pagination = $pagination;

			// Category fillter
			$this->cat_options            = $campaignHelper->getCampaignsCategories();

			// Get filter value and set list
			$filter_campaign_cat          = $mainframe->getUserStateFromRequest('com_jgive.filter_campaign_cat', 'filter_campaign_cat', '', 'INT');
			$lists['filter_campaign_cat'] = $filter_campaign_cat;
			$this->lists                  = $lists;

			// Organization_individual_type filter since version 1.5.1
			$campaignHelper                   = new campaignHelper;
			$org_ind_type                     = $campaignHelper->organization_individual_type();
			$this->filter_org_ind_type_report = $org_ind_type;

			$filter_org_ind_type_report  = $mainframe->getUserStateFromRequest('com_jgive' . 'filter_org_ind_type_report', 'filter_org_ind_type_report');
			$lists['filter_org_ind_type_report'] = $filter_org_ind_type_report;
		}

		if ($layout == 'payouts')
		{
			$payouts       = $this->get('Payouts');
			$this->payouts = $payouts;

			$total       = $this->get('Total');
			$this->total = $total;

			$pagination       = $this->get('Pagination');
			$this->pagination = $pagination;
		}

		$payout_id = JFactory::getApplication()->input->get('payout_id', '');

		if ($layout == 'edit_payout')
		{
			$getPayoutFormData       = $this->get('PayoutFormData');
			$this->getPayoutFormData = $getPayoutFormData;

			$payee_options   = array();
			$payee_options[] = JHtml::_('select.option', '0', JText::_('COM_JGIVE_SELECT_CAMPAIGN_OWNER'));

			if (!empty($getPayoutFormData))
			{
				foreach ($getPayoutFormData as $payout)
				{
					$payee_options[] = JHtml::_('select.option', $payout->creator_id, $payout->first_name . ' ' . $payout->last_name);
				}
			}

			$this->payee_options = $payee_options;

			$task        = JFactory::getApplication()->input->get('task');
			$this->task  = $task;
			$payout_data = array();

			if (!empty($payout_id))
			{
				$payout_data = $this->get('SinglePayoutData');
			}

			$this->assignRef('payout_data', $payout_data);
		}

		$payee_name = $mainframe->getUserStateFromRequest('com_jgive', 'payee_name', '', 'string');

		$lists['payee_name'] = $payee_name;
		$lists['order_Dir']  = $filter_order_Dir;
		$lists['order']      = $filter_type;

		$this->lists = $lists;

		// Set toolbar
		$this->_setToolBar($layout, $payout_id);

		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Function to set tool bar.
	 *
	 * @param   String  $layout     Layout
	 * @param   INT     $payout_id  Payout Id
	 *
	 * @return void
	 *
	 * @since	1.8
	 */
	public function _setToolBar($layout, $payout_id)
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_admin.css');
		$bar  = JToolBar::getInstance('toolbar');
		$user = JFactory::getUser();

		$canCreate  = $user->authorise('core.create', 'com_jgive');
		$canEdit    = $user->authorise('core.edit', 'com_jgive');
		$canCheckin = $user->authorise('core.manage', 'com_jgive');
		$canChange  = $user->authorise('core.edit.state', 'com_jgive');
		$canDelete  = $user->authorise('core.delete', 'com_jgive');

		$layout = JFactory::getApplication()->input->get('layout');

		if ($layout == 'payouts')
		{
			JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_PAYOUT_REPORTS'), 'list');

			if ($canCreate)
			{
				JToolBarHelper::addNew('reports.add');
			}

			if (count($this->payouts) > 0)
			{
				if ($canDelete)
				{
					JToolBarHelper::DeleteList(JText::_('COM_JGIVE_DELETE_PAYOUT_CONFIRM'), 'reports.deletePayouts', 'JTOOLBAR_DELETE');
				}

				// CSV EXPORT
				$button = "<a class='btn btn-small'
				class='button'
				type='submit'
				onclick=\"javascript:document.getElementById('task').value = 'reports.csvexportpayouts';
				document.adminForm.submit();
				document.getElementById('task').value = '';\" href='#'>
				<span title='Export' class='icon-download'>
				</span>" . JText::_('CSV_EXPORT') . "</a>";
				$bar->appendButton('Custom', $button);
			}
		}
		elseif ($layout == 'edit_payout')
		{
			JToolBarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_EDIT_PAYOUT'), 'list');
			JToolBarHelper::back(JText::_('COM_JGIVE_BACK'), 'javascript:history.back();');

			if ($layout == 'edit_payout' AND (!empty($payout_id)))
			{
				JToolBarHelper::save('reports.edit');
			}
			else
			{
				JToolBarHelper::save('reports.save');
			}
		}
		else
		{
			JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_REPORTS'), 'list');

			$button = "<a class='btn btn-small'
			class='button'
			type='submit'
			onclick=\"javascript:document.getElementById('task').value = 'reports.csvexport';
			document.adminForm.submit();
			document.getElementById('task').value = '';\" href='#'>
			<span title='Export' class='icon-download'>
			</span>" . JText::_('CSV_EXPORT') . "</a>";

			$bar->appendButton('Custom', $button);
		}

		JToolBarHelper::preferences('com_jgive');

		// Filter since joomla3.0
		if ($layout == 'default')
		{
			JHtmlSidebar::setAction('index.php?option=com_jgive&view=reports&layout=default');

			// Type filter
			$campaignHelper = new campaignHelper;
			$campaign_type  = $campaignHelper->filedToShowOrHide('campaign_type');

			if ($campaign_type)
			{
				JHtmlSidebar::addFilter(
				JText::_('COM_JGIVE_FILTER_SELECT_TYPE'), 'filter_campaign_type',
				JHtml::_('select.options', $this->campaign_type_filter_options, 'value', 'text',
				$this->lists['filter_campaign_type'], true
				)
				);
			}

			// Categorty filter
			JHtmlSidebar::addFilter(
			JText::_('COM_JGIVE_CAMPAIGN_CATEGORIES'), 'filter_campaign_cat',
			JHtml::_('select.options', $this->cat_options, 'value', 'text',
			$this->lists['filter_campaign_cat'], true
			)
			);

			// Organization filter
			JHtmlSidebar::addFilter(
			JText::_('COM_JGIVE_SELECT_TYPE_ORG_INDIVIDUALS'), 'filter_org_ind_type_report',
			JHtml::_('select.options', $this->filter_org_ind_type_report, 'value', 'text', $this->lists['filter_org_ind_type_report'], true
			)
			);

			// Campaign Filters
			JHtmlSidebar::addFilter(
			JText::_('COM_JGIVE_SELECT_CAMPAIGN'), 'filter_campaign',
			JHtml::_('select.options', $this->filter_campaign_options, 'value', 'text',
			$this->lists['filter_campaign'], true
			)
			);
		}
	}
}
