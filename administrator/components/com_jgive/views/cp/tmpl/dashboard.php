<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/bs3/css/bootstrap.min.css');
$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/bs3/css/bootstrap.css');
$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/vendors/font-awesome/css/font-awesome.min.css');
$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/vendors/font-awesome/css/font-awesome.css');

$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/vendors/css/morris.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive-dashboard.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/tjdashboard.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/tjdashboard-sb-admin.css');

$document->addScript(JUri::root(true) . '/media/com_jgive/vendors/js/morris.min.js');
$document->addScript(JUri::root(true) . '/media/com_jgive/vendors/js/raphael.min.js');


$session = JFactory::getSession();
$session->set('PeriodicDonationsCount', '');

$i = 0;
$backdate = date('Y-m-d', strtotime(date('Y-m-d') . ' - 30 days'));
$params   = JComponentHelper::getParams('com_jgive');
$curr_sym = $params->get('currency_symbol');

$jgivehelper = new JgiveFrontendHelper;

$dashboard_data = $this->dashBoard;
$AllMonthName = $this->allMonthName;
$MonthDonation = $this->monthDonation;

foreach ($this->allMonthName as $AllMonthName)
{
	$AllMonthName_final[$i] = $AllMonthName['month'];
	$curr_MON = $AllMonthName['month'];
	$month_amt_val[$curr_MON] = 0;
	$i++;
}

$barChartData = 0;

foreach ($this->monthDonation as $MonthDonation)
{
	$month_year = '';
	$month_year = $MonthDonation->YEARNAME;
	$month_name = $MonthDonation->MONTHSNAME;

	$month_int = (int) $month_name;
	$timestamp = mktime(0, 0, 0, $month_int);
	$curr_month = date("F", $timestamp);

	foreach ($this->allMonthName as $AllMonthName)
	{
		if (($curr_month == $AllMonthName['month']) and ($MonthDonation->amount) and ($month_year == $AllMonthName['year']))
		{
			$month_amt_val[$curr_month] = str_replace(",", '', $MonthDonation->amount);
		}

		if ($barChartData == 0)
		{
			if ($MonthDonation->amount)
			{
				$barChartData = 1;
			}
		}
	}
}

$month_amt_str  = implode(",", $month_amt_val);
$month_name_str = implode("','", $AllMonthName_final);
$month_name_str = "'" . $month_name_str . "'";
$month_array_name = array();

$js = "
	function refreshViews()
	{
		var curr_sym = \"" . $curr_sym . "\";
		fromDate = document.getElementById('from').value;
		toDate = document.getElementById('to').value;
		fromDate1 = new Date(fromDate.toString());
		toDate1 = new Date(toDate.toString());
		difference = toDate1 - fromDate1;
		days = Math.round(difference/(1000*60*60*24));

		if (parseInt(days)< 0)
		{
			alert(\"" . JText::_('COM_JGIVE_DATELESS') . "\");

			return;
		}

		/*Set Session Variables*/
		var info = {};
		techjoomla.jQuery.ajax({
			type: 'GET',
			url: 'index.php?option=com_jgive&task=cp.SetsessionForGraph&fromDate='+fromDate+'&toDate='+toDate,
			dataType: 'json',
			async:false,
			success: function(data) {
			}
		});

		/*Get periodic data and redraw chart*/
		techjoomla.jQuery.ajax({
			type: 'GET',
			url: 'index.php?option=com_jgive&task=cp.makechart',
			dataType: 'json',
			success: function(data)
			{
				techjoomla.jQuery('#bar_chart_graph').html('' + data.barchart);
				/*Reset hidden field values*/
				document.getElementById('pending_donations').value=data.pending_donations;
				document.getElementById('confirmed_donations').value=data.confirmed_donations;

				document.getElementById('denied_donations').value=data.denied_donations;
				document.getElementById('refunded_donations').value=data.refunded_donations;

				document.getElementById('canceled_donations').value=data.canceled_donations;
				/*Redraw charts*/
				document.getElementById('periodic_donations').innerHTML = curr_sym + ' ' + data.periodicDonationsCount;
				drawPeriodicDonationsChart();
			}
		});
	}";

$document->addScriptDeclaration($js);

?>
	<form name="adminForm" id="adminForm" class="form-validate" method="post">
		<?php
			if (!empty($this->sidebar))
			{
			?>
				<div id="j-sidebar-container" class="span2">
					<?php echo $this->sidebar;?>
				</div>
				<div id="j-main-container" class="span10">
			<?php
			}
			else
			{
			?>
				<div id="j-main-container">
			<?php
			}
			?>
		<!-- TJ Bootstrap3 -->
		<div class="tjBs3">
			<!-- TJ Dashboard -->
			<div class="tjDB">
				<div id="wrapper">
					<div id="page-wrapper">
						<div class="row">
							<div class="col-lg-12">
<!--
								<div id="activityStatus"></div>
-->
							<?php if (!$this->downloadid)
							{
							?>
								<div class="alert alert-warning">
									<?php echo JText::sprintf(
									'COM_JGIVE_LIVE_UPDATE_DOWNLOAD_ID_MSG', '<a href="https://techjoomla.com/about-tj/faqs/#how-to-get-your-download-id" target="_blank">' .
									JText::_('COM_JGIVE_LIVE_UPDATE_DOWNLOAD_ID_MSG2') . '</a>'
									);
									?>
								</div>
							<?php
}
							?>
								<div>
									<?php
									$versionHTML = '<span style="font-size: 9pt" class="label label-info">' .
											JText::_('COM_JGIVE_HAVE_INSTALLED_VER') . ': ' . $this->version . '</span>';

									if ($this->latestVersion)
									{
										if ($this->latestVersion->version > $this->version)
										{
											$versionHTML = '<div class="alert alert-error">' .
											'<i class="icon-puzzle install"></i>' .
											JText::_('COM_JGIVE_HAVE_INSTALLED_VER') .
											': ' . $this->version . '<br/>' . '<i class="icon icon-info"></i>' .
											JText::_("COM_JGIVE_NEW_VER_AVAIL") . ': ' .
											$this->latestVersion->version . '<br/>' . '<i class="icon icon-warning"></i>' . '<span class="label label-info">' .
											JText::_("COM_JGIVE_LIVE_UPDATE_BACKUP_WARNING") . '</span>' . '</div>

											<div style="text-align:right">
												<a href="index.php?option=com_installer&view=update" class="btn btn-small btn-primary">' .
												JText::sprintf('COM_JGIVE_LIVE_UPDATE_TEXT', $this->latestVersion->version) . '
												</a>
											</div>';
										}
									}?>
									<?php echo $versionHTML; ?>
								</div>
							</div>
						</div>
					<div class="clearfix">&nbsp;</div>
						<!-- Start - stat boxes -->
						<div class="row">
							<!--Start - campaigns -->
							<div class="col-lg-3 col-md-6">
								<div class="panel panel-green">
									<a href="<?php echo JRoute::_('index.php?option=com_jgive&view=campaigns&layout=default',    false)?>">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3 ">
													<i class="fa fa-bullhorn fa-4x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge"><span><?php echo $dashboard_data['campaignInfo']['total_campaigns'];?></span> </div>
													<div><?php echo JText::_('COM_JGIVE_CAMPAIGNS');?></div>
												</div>
											</div>
										</div>
										<div class="panel-footer">
											<span class="pull-left"><?php echo JText::_('COM_JGIVE_VIEW_DETAILS');?></span>
											<span class="pull-right">
												<i class="fa fa-arrow-circle-right"></i>
											</span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
							<!-- /.campaigns -->

							<!--Start - total goal -->
							<div class="col-lg-3 col-md-6">
								<a href="<?php echo JRoute::_('index.php?option=com_jgive&view=reports&layout=default', false)?>">
									<div class="panel panel-primary">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3 ">
													<i class="fa fa-money fa-4x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">
														<span>
															<?php echo $jgivehelper->getFormattedPrice($dashboard_data['campaignInfo']['total_goal_amount']);?>
														</span>
													 </div>
													<div><?php echo JText::_('COM_JGIVE_TOTAL_GOAL_AMOUNT');?></div>
												</div>
											</div>
										</div>
										<div class="panel-footer">
											<span class="pull-left"><?php echo JText::_('COM_JGIVE_VIEW_DETAILS');?></span>
											<span class="pull-right">
												<i class="fa fa-arrow-circle-right"></i>
											</span>
											<div class="clearfix"></div>
										</div>
									</div>
								</a>
							</div>
							<!-- /.total goal -->
							<!--Start - funded amount -->
							<div class="col-lg-3 col-md-6">
								<div class="panel panel-yellow">
									<a href="<?php echo JRoute::_('index.php?option=com_jgive&view=reports&layout=default', false)?>">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3 ">
													<!--<i class="fa fa-money fa-4x"></i>-->
													<i class="fa fa-dollar fa-4x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge"><span><?php echo $jgivehelper->getFormattedPrice($dashboard_data['orderInfo']['total_funded_amount']);?></span> </div>
													<div><?php echo JText::_('COM_JGIVE_FUNDED_AMOUNT');?></div>
												</div>
											</div>
										</div>
										<div class="panel-footer">
											<span class="pull-left"><?php echo JText::_('COM_JGIVE_VIEW_DETAILS');?></span>
											<span class="pull-right">
												<i class="fa fa-arrow-circle-right"></i>
											</span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>

							</div>
							<!-- /.funded amount -->
							<!--Start - commision amount -->
							<div class="col-lg-3 col-md-6">
								<div class="panel panel-red">
									<a href="<?php echo JRoute::_('index.php?option=com_jgive&view=reports&layout=default', false)?>">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-3 ">
													<i class="fa fa-money fa-4x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge"><span><?php echo $jgivehelper->getFormattedPrice($dashboard_data['orderInfo']['commision_amount']);?></span> </div>
													<div><?php echo JTEXT::_('COM_JGIVE_COMMISION_AMOUNT');?></div>
												</div>
											</div>
										</div>
										<div class="panel-footer">
											<span class="pull-left"><?php echo JText::_('COM_JGIVE_VIEW_DETAILS');?></span>
											<span class="pull-right">
												<i class="fa fa-arrow-circle-right"></i>
											</span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
						</div>

						<div class ="row">
							<div class="col-lg-8">
								<!-- Start - Bar Chart for Monthly Donations for past 12 months -->
								<div class="panel panel-default">
									<div class="panel-heading">
										<i class="fa fa-bar-chart-o fa-fw"></i>
										<?php echo JText::_('COM_JGIVE_MONTHLY_DONATIONS');?>
									</div>
									<div class="panel-body">
										<div id="graph-monthly-donations"></div>
										<hr class="hr hr-condensed"/>
										<div class="center">
											<?php echo JText::_('COM_JGIVE_BAR_CHART_HAXIS_TITLE');?>
										</div>
									</div>
								</div>
								<!-- End - Bar Chart for Monthly Income for past 12 months -->

								<div class="row">
									<div class="col-lg-6">
										<div class="panel panel-default">
											<div class="panel-heading">
												<i class="fa fa-pie-chart fa-fw"></i>
												<?php echo JText::_('COM_JGIVE_PERIODIC_DONATION');?>
											</div>
											<div class="panel-body">
												<!-- CALENDER ND REFRESH BTN  -->
												<div class="clearfix row">
													<div class="col-sm-12 col-lg-12 col-md-12">
														<div class="pull-right">
															<div class="form-group">
																<label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label"><?php echo JText::_('COM_JGIVE_FROM_DATE');?></label>
																<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
																	<div class="input-group">
																		<?php echo JHtml::_('calendar',
																			$backdate, 'fromDate', 'from', '%Y-%m-%d',
																			array('class' => 'inputbox input-xs','readonly' => 'true', 'style' => 'min-height:35px!important; max-width:90px!important;'
																				)
																			); ?>
																	</div>
																</div>
															</div>

															<div class="form-group">
																<label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label"><?php echo JText::_('COM_JGIVE_TO_DATE');?></label>
																<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
																	<div class="input-group">
																		<?php echo JHtml::_('calendar',
																			date('Y-m-d'), 'toDate', 'to', '%Y-%m-%d',
																			array('class' => 'inputbox input-xs','readonly' => 'true', 'style' => 'min-height:35px!important; max-width:90px!important;'
																				)
																			); ?>
																	</div>
																</div>
															</div>

															<div class="form-group">
																<div class="pull-right col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<div class="input-group">
																		<input id="btnRefresh"
																		class="pull-right btn btn-micro btn-primary"
																		type="button"
																		value="<?php echo JText::_('COM_JGIVE_GO'); ?>"
																		title="<?php echo JText::_('COM_JGIVE_DONATIONS_GO_TOOLTIP');?>"
																		style="font-weight: bold;" onclick="refreshViews();"/>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--END::CALENDER ND REFRESH BTN  -->

												<div class="clearifx">&nbsp;</div>
												<?php

												if (!$this->tot_periodicDonationsCount)
												{
													$this->tot_periodicDonationsCount = 0;
												}
												?>
												<div class="list-group">
													<span class="list-group-item">
														<i class="fa fa-money fa-fw"></i> <?php echo JText::_('COM_JGIVE_PERIODIC_DONATION_AMOUNT');?>

														<span class="text-muted small">
															<strong id="periodic_donations">
																<?php echo $jgivehelper->getFormattedPrice($this->tot_periodicDonationsCount);
																?>
															</strong>
														</span>

													</span>
												</div>
												<!-- Periodic donations - graph start -->
												<div id="graph-periodic-donations"></div>
												<hr class="hr hr-condensed"/>
												<div class="center">
													<strong class="">
														<?php echo JText::_('COM_JGIVE_PERIODIC_DONATION');?>
													</strong>
												</div>
												<!-- Periodic donations - graph end -->
											</div>
											<!-- /.panel-body -->
										</div>
									</div>
									<!-- /.col-lg-6 -->

									<div class="col-lg-6">
										<!-- Start - recent Donation Details -->
										<div class="panel panel-default">
											<div class="panel-heading">
												<i class="fa fa-list fa-fw"></i>
												<?php echo JText::_('COM_JGIVE_RECENTDONATION_DETAILS');?>
											</div>
											<div class="panel-body">
												<?php if (!empty($this->recentDonationDetails))
												{
												?>
													<table class="table table-striped table-hover" >
														<thead>
															<th><?php echo JText::_('COM_JGIVE_ORDER_ID')?></th>
															<th><?php echo JText::_('COM_JGIVE_CAMPAIGN_TITLE')?></th>
															<th><?php echo JText::_('COM_JGIVE_AMOUNT')?></th>
														</thead>
														<tbody>
														<?php
															foreach ($this->recentDonationDetails as $ord)
															{
														?>
															<tr>
																<td><a href="<?php echo JRoute::_('index.php?option=com_jgive&view=donations&layout=details&donationid=' . $ord->id, false);?>"
																	title="<?php echo JText::_('COM_JGIVE_TOOLTIP_VIEW_ORDER_MSG');?>">
																	<?php echo $ord->order_id;?>
																	</a>
																</td>
																<td><?php echo ucwords($ord->title);?></td>
																<td><?php echo $jgivehelper->getFormattedPrice($ord->amount);?></td>
															</tr>
														<?php
															}?>
														</tbody>
													</table>
													<a title="<?php echo JText::_('COM_JGIVE_DONATIONS_SHOW_ALL');?>"
														class="btn btn-primary btn-small pull-right"
														href="<?php echo JRoute::_('index.php?option=com_jgive&view=donations');?>"
														target="_blank" >
															<?php echo JText::_('COM_JGIVE_DONATIONS_SHOW_ALL'); ?>
													</a>
												<?php
}
												else
												{
													?>
													<div class="">
														<?php echo JText::_("COM_JGIVE_NO_STORE_PREVIOUS_DONATIONS");?>
													</div>
													<?php
												} ?>
											</div>

										</div>
										<!-- End - recent Donation Details -->
												<!-- Start - pending payout -->
												<div class="panel panel-default">
													<div class="panel-heading">
														<i class="fa fa-list fa-fw"></i>
														<?php echo JText::_('COM_JGIVE_PENDING_PAYOUTS');?>
													</div>
											<div class="panel-body">
												<?php
													if (!empty($this->pendingPayouts))
													{
															?>
															<table class="table table-striped table-hover">
																<thead>
																	<th><?php echo JText::_('COM_JGIVE_NAME')?></th>
																	<th><?php echo JText::_('COM_JGIVE_REMAINING_AMOUNT')?></th>
																</thead>
															<tbody>
																	<?php
																	$count = 1;

																	foreach ($this->pendingPayouts as $allpay)
																	{
																	?>
																		<tr>
																			<td><?php echo $allpay->first_name;?></td>
																			<td><?php echo $jgivehelper->getFormattedPrice($allpay->total_pending_amount); ?></td>
																		</tr>
																			<?php
																			if ($count >= 5)
																			{
																				break;
																			}
																	}
																	?>
																</tbody>
															</table>
													<a title="<?php echo JText::_('COM_JGIVE_DONATIONS_SHOW_ALL');?>"
														class="btn btn-primary btn-small pull-right"
														href="<?php echo JRoute::_('index.php?option=com_jgive&view=reports&layout=edit_payout');?>"
														target="_blank" >
															<?php echo JText::_('COM_JGIVE_DONATIONS_SHOW_ALL'); ?>
													</a>
													<?php
													}
													else
													{
													?>
													<div class="">
														<?php echo JText::_("COM_JGIVE_DASHBORD_NO_PENDING_PAYOUTS");?>
													</div>
													<?php
													}
													?>
											</div>
												</div>
												<!-- End - pending payout -->
											<?php
										?>
									</div>
									<!-- /.col-lg-6 -->
								</div>
								<div class="row">
									<div class="col-lg-8" title=" Update all campaign success status.">
											<a class="btn btn-primary"
											href="index.php?option=com_jgive&task=cp.updateAllCampaignsSuccessStatus"
											target="_blank">
												<?php echo JText::_('COM_JGIVE_UPDATE_CAMP_SUCCESS_STATUS_TASK_1'); ?>
											</a>
											 <?php echo JText::_('COM_JGIVE_UPDATE_CAMP_SUCCESS_STATUS_TASK_2'); ?>
									</div>
								</div>
								<!-- /.row -->
							</div>
							<!-- /.col-lg-8 -->

							<div class="col-lg-4">
								<!--INFO,HELP + ETC START -->
								<div class = "panel panel-default">
									<div class = "panel-heading">
										<i class="fa fa-bullhorn"></i>
										<?php echo JText::_('COM_JGIVE'); ?>
									</div>
									<div class="panel-body">
										<div class = "">
											<blockquote class="blockquote-reverse">
												<p><?php echo JText::_('COM_JGIVE_ABOUT1');?></p>
											</blockquote>
										</div>

										<div class="row">
											<div class = "col-lg-12 col-md-12 col-sm-12">
												<p class = "pull-right"><span class="label label-info"><?php echo JText::_('COM_JGIVE_LINKS'); ?></span></p>
											</div>
										</div>

										<div class = "list-group">
											<a href = "https://techjoomla.com/table/extension-documentation/documentation-for-jgive-formerly-jomgive/ "
											class="list-group-item" target = "_blank">
											<i class="fa fa-file fa-fw i-document"></i>
											<?php echo JText::_('COM_JGIVE_DOCS');?>
											</a>

											<a href="https://techjoomla.com/documentation-for-jgive-formerly-jomgive/faqs47.html" class = "list-group-item" target="_blank">
												<i class = "fa fa-question fa-fw i-question"></i> <?php echo JText::_('COM_JGIVE_FAQS');?>
											</a>
											<a href = "https://techjoomla.com/support/support-tickets" class = "list-group-item" target = "_blank">
												<i class = "fa fa-support fa-fw i-support"></i> <?php echo JText::_('COM_JGIVE_TECHJOOMLA_SUPPORT_CENTER');?>
											</a>

											<a href = "http://extensions.joomla.org/extension/jgive" class = "list-group-item" target = "_blank">
												<i class = "fa fa-bullhorn fa-fw i-horn"></i> <?php echo JText::_('COM_JGIVE_LEAVE_JED_FEEDBACK');?>
											</a>
										</div>

										<div class = "row">
											<div class = "col-lg-12 col-md-12 col-sm-12">
												<p class = "pull-right">
													<span class = "label label-info"><?php echo JText::_('COM_JGIVE_STAY_TUNNED'); ?></span>
												</p>
											</div>
										</div>

										<div class = "list-group">
											<div class = "list-group-item">
												<div class = "pull-left">
													<i class = "fa fa-facebook fa-fw i-facebook"></i>
													<?php echo JText::_('COM_JGIVE_FACEBOOK'); ?>
												</div>
												<div class = "pull-right">
													<!-- facebook button code -->
													<div id = "fb-root"></div>
													<script>
														(function(d, s, id)
															{
																var js, fjs = d.getElementsByTagName(s)[0];

																if (d.getElementById(id)) return;
																js = d.createElement(s); js.id = id;
																js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
																fjs.parentNode.insertBefore(js, fjs);
															}
															(document, 'script', 'facebook-jssdk')
														);
														</script>
													<div class = "fb-like"
														data-href = "https://www.facebook.com/techjoomla"
														data-send = "true" data-layout = "button_count"
														data-width = "250" data-show-faces = "false"
														data-font = "verdana">
													</div>
												</div>
												<div class = "clearfix">&nbsp;</div>
											</div>

											<div class="list-group-item">
												<div class="pull-left">
													<i class="fa fa-twitter fa-fw i-twitter"></i>
													<?php echo JText::_('COM_JGIVE_TWITTER'); ?>
												</div>
												<div class = "pull-right">
													<!-- twitter button code -->
													<a href = "https://twitter.com/techjoomla" class = "twitter-follow-button" data-show-count = "false">Follow @techjoomla</a>
													<script>
														!function(d,s,id)
														{
															var js,fjs = d.getElementsByTagName(s)[0];
															if(!d.getElementById(id))
															{
																js = d.createElement(s);
																js.id = id;
																js.src = "//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);
															}
														}
														(document,"script","twitter-wjs");
													</script>
												</div>
												<div class = "clearfix">&nbsp;</div>
											</div>

											<div class = "list-group-item">
												<div class = "pull-left">
													<i class = "fa fa-google fa-fw i-google"></i>
													<?php echo JText::_('COM_JGIVE_GPLUS'); ?>
												</div>
												<div class = "pull-right">
													<!-- Place this tag where you want the +1 button to render. -->
													<div class = "g-plusone" data-annotation = "inline" data-width = "120" data-href = "https://plus.google.com/102908017252609853905"></div>
													<!-- Place this tag after the last +1 button tag. -->
													<script type = "text/javascript">
													(function() {
													var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
													po.src = 'https://apis.google.com/js/plusone.js';
													var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
													})();
													</script>
												</div>
												<div class = "clearfix">&nbsp;</div>
											</div>
										</div>

										<div class = "row">
											<div class = "col-lg-12 col-md-12 col-sm-12 center">
												<?php
												$logo = '<img src = "' . JUri::root(true) . '/media/com_jgive/images/techjoomla.png" alt = "TechJoomla" class = ""/>';?>
												<span class = "center thumbnail">
													<a href = 'http://techjoomla.com/' target = '_blank'>
														<?php echo $logo;?>
													</a>
												</span>
												<p><?php echo JText::_('COM_JGIVE_COPYRIGHT'); ?></p>
											</div>
										</div>
									<!-- /.panel-body -->
								</div>
								<!-- /.panel -->
							</div>
							<!-- /.col-lg-4 -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.page-wrapper -->
				</div>
				<!-- /.wrapper -->
			</div>
			<!-- /.tjDB -->
		</div>
		<!-- /.tjBs3 -->

		<?php
		// Get data for periodic orders chart
		$statsforpie = $this->statsForPie;
		$currentmonth = '';

		$pending_donations = $confirmed_donations = $denied_donations = 0;
		$refunded_donations = $canceled_donations = 0;

		if (empty($statsforpie[0][0]) && empty($statsforpie[1][0]) && empty($statsforpie[2][0]) && empty($statsforpie[3][0]) && empty($statsforpie[4][0]))
		{
			$barchart = JText::_('COM_JGIVE_NO_STATS');
			$emptylinechart = 1;
		}
		else
		{
			if (!empty($statsforpie[0]))
			{
				$pending_donations = $statsforpie[0][0]->donations;
			}

			if (!empty($statsforpie[1]))
			{
				$confirmed_donations = $statsforpie[1][0]->donations;
			}

			if (!empty($statsforpie[2]))
			{
				$canceled_donations = $statsforpie[2][0]->donations;
			}

			if (!empty($statsforpie[3]))
			{
				$denied_donations = $statsforpie[3][0]->donations;
			}

			if (!empty($statsforpie[4]))
			{
				$refunded_donations = $statsforpie[4][0]->donations;
			}
		}

		$emptypiechart = 0;

		if (!$pending_donations and !$confirmed_donations and !$denied_donations and !$refunded_donations and !$canceled_donations)
		{
			$emptypiechart = 1;
		}
		?>
		<input type = "hidden" name="pending_donations" id = "pending_donations"
		value = "<?php echo !empty($pending_donations) ? $pending_donations : '0'; ?>">

		<input type = "hidden" name = "confirmed_donations" id = "confirmed_donations"
		value = "<?php echo !empty($confirmed_donations) ? $confirmed_donations : '0';  ?>">

		<input type = "hidden" name = "denied_donations" id = "denied_donations"
		value = "<?php echo !empty($denied_donations) ? $denied_donations : '0'; ?>">

		<input type = "hidden" name = "refunded_donations" id = "refunded_donations"
		value = "<?php echo !empty($refunded_donations) ? $refunded_donations: '0';  ?>">

		<input type = "hidden" name = "canceled_donations" id = "canceled_donations"
		value = "<?php echo !empty($canceled_donations) ? $canceled_donations : '0'; ?>">
</form>
<!--</div>-->

<script type = 'text/javascript'>

/*Joomla.submitbutton = function(task)
{
	if (task == 'createactivities')
	{
		createactivities();
	}
}

function createactivities()
{
	techjoomla.jQuery('#activityStatus').addClass('alert alert-info');
	techjoomla.jQuery('#activityStatus').html(Joomla.JText._('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_PROCESSING'));

	techjoomla.jQuery.ajax({
			url: 'index.php?option=com_jgive&task=migration.createActivity',
			type: 'POST',
			dataType:'json',
			success: function(response)
			{
				if (response == 1)
				{
					techjoomla.jQuery('#activityStatus').addClass('alert alert-success');
					techjoomla.jQuery('#activityStatus').removeClass('alert-info');
					techjoomla.jQuery('#activityStatus').html(Joomla.JText._('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_DONE'));
				}
				else
				{
					techjoomla.jQuery('#activityStatus').removeClass('alert-info alert-success');
					techjoomla.jQuery('#activityStatus').addClass('alert-danger');
					techjoomla.jQuery('#activityStatus').html(Joomla.JText._('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_ERROR'));
				}
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				techjoomla.jQuery('#activityStatus').removeClass('alert-info alert-success');
				techjoomla.jQuery('#activityStatus').addClass('alert-danger');
				techjoomla.jQuery('#activityStatus').html(jqXHR.responseText);
			}
	});
}*/

techjoomla.jQuery(document).ready(function()
{
document.getElementById("pending_donations").value = <?php echo !empty($pending_donations) ? $pending_donations : '0'; ?>;
document.getElementById("confirmed_donations").value = <?php echo !empty($confirmed_donations) ? $confirmed_donations : '0'; ?>;
document.getElementById("denied_donations").value = <?php  echo !empty($denied_donations) ? $denied_donations : '0'; ?>;
document.getElementById("refunded_donations").value = <?php echo !empty($refunded_donations) ? $refunded_donations : '0'; ?>;
document.getElementById("canceled_donations").value = <?php  echo !empty($canceled_donations) ?$canceled_donations : '0'; ?>;

drawPeriodicDonationsChart();
});
<?php
if ($barChartData)
{
?>
		drawBarChart();
		function drawBarChart()
		{
			Morris.Bar({
				element: 'graph-monthly-donations',
				data:
					<?php
						$dataArray = "[";

						for ($i = 0; $i < count($AllMonthName_final); $i++)
						{
							$dataArray .= "{period : '" . $AllMonthName_final[$i] . "', donationTotal : " . $month_amt_val[$AllMonthName_final[$i]] . "},";
						}

						$dataArray .= "]";
					echo $dataArray;

					?>,
				xkey: 'period',
				ykeys: ['donationTotal'],
				labels: ['<?php echo JText::_('COM_JGIVE_BAR_CHART_VAXIS_TITLE');?>'],
				barColors: ['#428bca'],
				barRatio: 0.4,
				xLabelAngle: 35,
				hideHover: 'auto',
				resize:true
			});
		}
	<?php
}
else
{
?>
	techjoomla.jQuery('#graph-monthly-donations').html("<div class = 'center'><?php echo JText::_("COM_JGIVE_NO_STORE_PREVIOUS_ORDERS");?></div>");
<?php
}
?>
	function drawPeriodicDonationsChart()
	{
		techjoomla.jQuery('#graph-periodic-donations').html('');

		var pending_donations = document.getElementById('pending_donations').value;
		var confirmed_donations = document.getElementById('confirmed_donations').value;
		var denied_donations = document.getElementById('denied_donations').value;
		var refunded_donations = document.getElementById('refunded_donations').value;
		var canceled_donations = document.getElementById('canceled_donations').value;

		if (pending_donations > 0 || confirmed_donations > 0 || denied_donations > 0 || refunded_donations > 0 || canceled_donations > 0)
		{
			Morris.Donut({
					element: 'graph-periodic-donations',
					data: [
								{
									label: "<?php echo JText::_("COM_JGIVE_PENDING_DONATIONS");?>",
									value: pending_donations
								},
								{
									label: "<?php echo JText::_("COM_JGIVE_CONFIRMED_DONATIONS");?>",
									value: confirmed_donations
								},
								{
									label: "<?php echo JText::_("COM_JGIVE_DENIED_DONATIONS");?>",
									value: denied_donations
								},
								{
									label: "<?php echo JText::_("COM_JGIVE_REFUNDED_DONATIONS");?>",
									value: refunded_donations
								},
								{
									label: "<?php echo JText::_("COM_JGIVE_CANCELED_DONATIONS");?>",
									value: canceled_donations
								}],
					colors: ["#f0ad4e", "#5cb85c", "#428bca", "#d9534f", "#8A2BE2"],
					resize: true
				});
		}
		else
		{
			techjoomla.jQuery('#graph-periodic-donations').html("<div class = 'center'><?php echo JText::_("COM_JGIVE_NO_STORE_PREVIOUS_ORDERS");?></div>");
		}
	}
</script>
