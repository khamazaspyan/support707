<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * Dashboard form controller class.
 *
 * @package  JGive
 * @since    1.8
 */
class JGiveViewCp extends JViewLegacy
{
	/**
	 * Function to display.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function display($tpl = null)
	{
		// Load submenu
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jgive'))
		{
			$app->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		// Get download id
		$params = JComponentHelper::getParams('com_jgive');
		$this->downloadid = $params->get('downloadid');

		// Get installed version from xml file
		$xml     = JFactory::getXML(JPATH_COMPONENT . '/jgive.xml');
		$version = (string) $xml->version;
		$this->version = $version;

		$model = $this->getModel();
		$model->refreshUpdateSite();

		// Get dashboard data
		$this->dashBoard = $this->get('DashboardData');
		$this->allMonthName = $this->get('AllMonths');
		$this->monthDonation = $this->get('MonthDonation');
		$this->recentDonationDetails = $this->get('RecentDonationDetails');
		$this->tot_periodicDonationsCount = $this->get('PeriodicDonationsCount');
		$this->statsForPie = $model->statsforpie();

		$this->pendingPayouts = $this->get('PendingPayouts');

		// Get new version
		$this->latestVersion = $this->get('LatestVersion');

		$JgiveHelper = new JgiveHelper;
		$JgiveHelper->addSubmenu('cp');

		$this->_setToolBar();

		$this->sidebar = JHtmlSidebar::render();

		if (!JFactory::getApplication()->input->get('layout'))
		{
			$this->setLayout('dashboard');
		}

		parent::display($tpl);
	}

	/**
	 * Function to set tool bar.
	 *
	 * @return void
	 *
	 * @since	1.8
	 */
	public function _setToolBar()
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_admin.css');
		$bar = JToolBar::getInstance('toolbar');
		JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_CP'), 'list');
		JToolBarHelper::preferences('com_jgive');
	}
}
