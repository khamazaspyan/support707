<?php
/**
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (C) 2011-2015 Techjoomla. All rights reserved.
 * @license    GNU/GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.view');
JHtml::_('behavior.modal', 'a.modal');
jimport('joomla.html.html.bootstrap');

/**
 * Donations view class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveViewDonations extends JViewLegacy
{
	/**
	 * Function to display.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function display($tpl = null)
	{
		global $mainframe, $option;
		$mainframe = JFactory::getApplication();
		$user      = JFactory::getUser();

		if (!JFactory::getUser($user->id)->authorise('core.manage', 'com_jgive'))
		{
			$mainframe->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

			return false;
		}

		// Load submenu
		$JgiveHelper = new JgiveHelper;
		$JgiveHelper->addSubmenu('donations');

		$option              = JFactory::getApplication()->input->get('option');
		$campaignHelper      = new campaignHelper;

		// Get params
		$params              = JComponentHelper::getParams('com_jgive');
		$this->currency_code = $params->get('currency');

		$this->retryPayment = new StdClass;
		$this->retryPayment->status = '';
		$this->retryPayment->msg = '';

		// Get logged in user id
		$user                = JFactory::getUser();
		$this->logged_userid = $user->id;

		// Default layout is default
		$layout = JFactory::getApplication()->input->get('layout', 'all');
		$this->setLayout($layout);

		// Set common view variables
		// Use frontend helper
		$frontpath = JPATH_SITE . '/components/com_jgive/helper.php';

		if (!class_exists('jgiveFrontendHelper'))
		{
			JLoader::register('jgiveFrontendHelper', $frontpath);
			JLoader::load('jgiveFrontendHelper');
		}

		$path = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$donationsHelper = new donationsHelper;
		$this->pstatus   = $donationsHelper->getPStatusArray();

		// Used on donattions view for payment status filter
		$this->sstatus   = $donationsHelper->getSStatusArray();

		$this->donations_site = 0;

		if ($layout == 'details')
		{
			// Load language file for component backend
			$lang = JFactory::getLanguage();
			$lang->load('com_jgive', JPATH_SITE);

			$donation_details = $this->get('SingleDonationInfo');
			$this->assignRef('donation_details', $donation_details);
		}

		if ($layout == 'all')
		{
			$donations       = $this->get('Donations');
			$this->donations = $donations;

			// Get ordering filter
			$filter_order_Dir                   = $mainframe->getUserStateFromRequest('com_jgive.filter_order_Dir', 'filter_order_Dir', 'desc', 'word');
			$filter_type                        = $mainframe->getUserStateFromRequest('com_jgive.filter_order', 'filter_order', 'id', 'string');

			// Campaigns Type Fillter
			$this->campaign_type_filter_options = $campaignHelper->getCampaignTypeFilterOptions();

			// Get filter value and set list
			$filter_campaign_type               = $mainframe->getUserStateFromRequest('com_jgive.filter_campaign_type', 'filter_campaign_type', '', 'string');
			$lists['filter_campaign_type']      = $filter_campaign_type;

			// Campaign list filter
			$filter_campaign_options            = array();

			if (JVERSION < 3.0)
			{
				$filter_campaign_options[] = JHtml::_('select.option', '0', JText::_('COM_JGIVE_SELECT_CAMPAIGN'));
			}

			$campaign_list = $campaignHelper->getAllCampaignOptions();

			if (!empty($campaign_list))
			{
				foreach ($campaign_list as $key => $campaign)
				{
					$filter_campaign_options[] = JHtml::_('select.option', $campaign->id, $campaign->title);
				}
			}

			$this->filter_campaign_options = $filter_campaign_options;

			// Get filter value and set list
			$filter_campaign               = $mainframe->getUserStateFromRequest($option . 'filter_campaign', 'filter_campaign', '', 'string');

			// Category fillter
			$this->cat_options = $campaignHelper->getCampaignsCategories();

			// Get filter value and set list
			$campaign_cat      = $mainframe->getUserStateFromRequest('com_jgive.campaign_cat', 'campaign_cat', '', 'INT');

			$cid = JFactory::getApplication()->input->get('cid', 0);

			if ($cid != 0)
			{
				// This is used when redirected from other view to this view
				// So change filter value to be set as passed in url
				$filter_campaign = $cid;
			}

			// Payment status filter
			$payment_status = $mainframe->getUserStateFromRequest('com_jgive' . 'payment_status', 'payment_status', '', 'string');

			if ($payment_status == null)
			{
				$payment_status = '-1';
			}

			// Set filters
			$lists['campaign_cat']    = $campaign_cat;
			$lists['filter_campaign'] = $filter_campaign;
			$lists['payment_status']  = $payment_status;
			$lists['order_Dir']       = $filter_order_Dir;
			$lists['order']           = $filter_type;
			$this->lists              = $lists;

			$total       = $this->get('Total');
			$this->total = $total;

			$pagination       = $this->get('Pagination');
			$this->pagination = $pagination;
		}

		if ($layout == 'paymentform')
		{
			$path = JPATH_ROOT . '/components/com_jgive/helpers/donations.php';

			if (!class_exists('donationsHelper'))
			{
				JLoader::register('donationsHelper', $path);
				JLoader::load('donationsHelper');
			}

			$path = JPATH_ROOT . '/components/com_jgive/helpers/integrations.php';

			if (!class_exists('JgiveIntegrationsHelper'))
			{
				JLoader::register('JgiveIntegrationsHelper', $path);
				JLoader::load('JgiveIntegrationsHelper');
			}

			$jgiveFrontendHelper = new jgiveFrontendHelper;
			$donationsHelper     = new donationsHelper;
			$this->pstatus       = $donationsHelper->getPStatusArray();

			// Used on donations view for payment status filter
			$this->sstatus       = $donationsHelper->getSStatusArray();

			// Get country list options
			// Use helper function
			$countries       = $jgiveFrontendHelper->getCountries();
			$this->countries = $countries;

			// Get campaign id
			$cid       = $this->get('CampaignId');
			$this->cid = $cid;
			$cdata     = $this->get('AllCampaigns');
			$cusers    = $this->get('Allusers');
			$this->assignRef('cdata', $cdata);
			$this->assignRef('cusers', $cusers);

			$params  = JComponentHelper::getparams('com_jgive');

			// Joomla profile import
			$session = JFactory::getSession();
			$nofirst = '';
			$nofirst = $session->get('No_first_donation');

			// If no user data set in session then only import the user profile or only first time after login
			if (empty($nofirst))
			{
				$profile_import = $params->get('profile_import');

				// If profie import is on the call profile import function
				if ($profile_import)
				{
					$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
				}
			}

			$this->guest_donation = $params->get('guest_donation');
			$session              = JFactory::getSession();
			$this->session        = $session;

			// Geteways
			$dispatcher           = JDispatcher::getInstance();
			JPluginHelper::importPlugin('payment');

			$params         = JComponentHelper::getParams('com_jgive');
			$this->gateways = $params->get('gateways');

			$gateways = array();

			if (!empty($this->gateways))
			{
				$gateways = $dispatcher->trigger('onTP_GetInfo', array((array) $this->gateways));
			}

			$this->assignRef('gateways', $gateways);
		}

		if ($layout == 'mass_mailing')
		{
			$session                 = JFactory::getSession();
			$selected_donations_ids = $session->get('selected_donations_ids');
			$this->selected_emails   = $this->getModel()->getDonorsEmailByDonationId($selected_donations_ids);
		}
		// Set toolbar
		$this->_setToolBar($layout);

		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Function to set tool bar.
	 *
	 * @param   String  $layout  Get Layout
	 *
	 * @return void
	 *
	 * @since	1.8
	 */
	public function _setToolBar($layout)
	{
		$user       = JFactory::getUser();
		$canCreate  = $user->authorise('core.create', 'com_jgive');
		$canEdit    = $user->authorise('core.edit', 'com_jgive');
		$canCheckin = $user->authorise('core.manage', 'com_jgive');
		$canChange  = $user->authorise('core.edit.state', 'com_jgive');
		$canDelete  = $user->authorise('core.delete', 'com_jgive');

		// Get the toolbar object instance
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_admin.css');
		$bar    = JToolBar::getInstance('toolbar');
		$layout = JFactory::getApplication()->input->get('layout', '');

		if ($layout == 'paymentform')
		{
			JToolBarHelper::save('donations.' . $task = 'placeOrder', $alt = JText::_('COM_JGIVE_CONTINUE_CONFIRM_FREE'));
			JToolBarHelper::cancel('donations.' . $task = 'cancelorder', $alt = JText::_('COM_JGIVE_CANCEL'));
		}

		if ($layout == 'mass_mailing')
		{
			JToolBarHelper::save('donations.' . $task = 'emailToSelected', $alt = JText::_('COM_JGIVE_SEND_EMAIL'));
			JToolBarHelper::cancel('donations.' . $task = 'cancelEmail', $alt = JText::_('COM_JGIVE_CANCEL_EMAIL'));
		}

		if ($layout == 'all' or $layout == '')
		{
			if ($canCreate)
			{
				JToolbarHelper::addNew('donations.' . $task = 'addNewDonation');
			}

			if (count($this->donations) > 0)
			{
				JToolBarHelper::custom('donations.redirectToMassmailing', 'mail.png', '', JText::_('COM_JGIVE_EMAIL_TO_DONORS'));

				$button = "<a class='btn btn-small'
				class='button'
				type='submit'
				onclick=\"javascript:document.getElementById('task').value = 'donations.donationsCsvexport';
				document.adminForm.submit();
				document.getElementById('task').value = '';\" href='#'>
				<span title='Export' class='icon-download'>
				</span>" . JText::_('CSV_EXPORT') . "</a>";
				$bar->appendButton('Custom', $button);

				if ($canDelete)
				{
					JToolBarHelper::deleteList('', 'donations.deleteDonations');
				}
			}
		}

		if ($layout == 'details')
		{
			JToolBarHelper::back('COM_JGIVE_BACK', 'index.php?option=com_jgive&view=donations');
		}

		JToolbarHelper::title(JText::_("COM_JGIVE") . ": " . JText::_('COM_JGIVE_DONATIONS'), 'list');
		JToolBarHelper::preferences('com_jgive');

		if ($layout == 'all')
		{
			$campaignHelper = new campaignHelper;
			$campaign_type  = $campaignHelper->filedToShowOrHide('campaign_type');

			if ($campaign_type)
			{
				JHtmlSidebar::addFilter(
										JText::_('COM_JGIVE_FILTER_SELECT_TYPE'), 'filter_campaign_type',
										JHtml::_('select.options', $this->campaign_type_filter_options, 'value', 'text', $this->lists['filter_campaign_type'], true
												)
										);
			}
			// Campaign Filter
			JHtmlSidebar::addFilter(
									JText::_('COM_JGIVE_SELECT_CAMPAIGN'), 'filter_campaign',
									JHtml::_('select.options', $this->filter_campaign_options, 'value', 'text', $this->lists['filter_campaign'], true
											)
									);

			// Campaign category filter
			JHtmlSidebar::addFilter(
									JText::_('COM_JGIVE_CAMPAIGN_CATEGORIES'), 'campaign_cat',
									JHtml::_('select.options', $this->cat_options, 'value', 'text',
									$this->lists['campaign_cat'], true
											)
									);

			// Donation Status Filter
			JHtmlSidebar::addFilter(
									JText::_('COM_JGIVE_APPROVAL_STATUS'), 'payment_status',
									JHtml::_('select.options', $this->sstatus, 'value', 'text',
									$this->lists['payment_status'], true
											)
									);
		}
	}
}
