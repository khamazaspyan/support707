<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
// No direct access
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidator');

	// Sice version 3.0 Jhtmlsidebar for menu
		if (!empty($this->sidebar))
		{?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php
		}
		else
		{?>
			<div id="j-main-container">
		<?php
		}
		?>
<div class="techjoomla-bootstrap" id="jgive-massmailing">
	<div class="row-fluid">
		<form action="" id="adminForm" name="adminForm" method="post" class="form-validate">
			<div class="form-horizontal">
				<div class="control-group">
					<label class="control-label">
						<?php echo  JText::_('COM_JGIVE_ENTER_EMAIL_ID') . ' * ' ?>
					</label>
					<div class="controls">
						<textarea class="form-control" id="selected_emails" name="selected_emails" readonly="true" ><?php echo implode(",", $this->selected_emails);?></textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">
						<?php echo  JText::_('COM_JGIVE_ENTER_EMAIL_SUBJECT') ?>
					</label>
					<div class="controls">
						<input type="text" id="jgive_subject"
						name="jgive_subject"  class="form-control"
						placeholder="<?php echo  JText::_('COM_JGIVE_ENTER_EMAIL_SUBJECT') ?>">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">
						<?php echo JText::_('COM_JGIVE_ENTER_EMAIL_BODY') ?>
					</label>
					<div class="controls">
						<?php
						$editor      = JFactory::getEditor();
						echo $editor->display("jgive_message", "", 670, 600, 60, 20, false);
						?>
					</div>
				</div>
			</div>
				<input type="hidden" name="option" value="com_jgive" />
				<input type="hidden" name="task" id="task" value="" />
				<input type="hidden" name="view" value="donations" />
				<input type="hidden" name="layout" value="mass_mailing" />
		</form>
	</div>
</div>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if(task == 'donations.emailToSelected')
		{
			// Check if Email Subject is empty
			if(document.getElementById("jgive_subject").value == 0 || document.getElementById("jgive_message").value == 0)
			{
				// If user want to send mail without subject or text
				var msg = "<?php echo JText::_('COM_JGIVE_CONFIRM_MSG_FOR_SEND_MAIL_WITHOUT_SUB_AND_TEXT'); ?>";

				alert(msg);
				return false;
			}
			else
			{
				Joomla.submitform(task);
			}
		}
		else
		{
			Joomla.submitform(task);
		}
	}
</script>


