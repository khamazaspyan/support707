<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     sh404SEF
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     4.7.0.3065
 * @date        2015-10-26
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_JEXEC'))
	die('Direct Access to this location is not allowed.');

/**
 * Stores 404 infos into database
 */
class Sh404sefModelNotfoundstore
{

	private static $_instance = null;

	/**
	 * Singleton method
	 *
	 * @param string $extension
	 *            extension name, with com_ - ie com_content
	 * @return object instance of Sh404sefModelCategories
	 */
	public static function getInstance()
	{
		if (is_null(self::$_instance))
		{
			self::$_instance = new Sh404sefModelNotfoundstore();
		}

		return self::$_instance;
	}

	public function store($reqPath, $config)
	{
		// normalize
		$reqPath = JString::ltrim($reqPath, '/');

		// optionnally log the 404 details
		if ($config->shLog404Errors && !empty($reqPath))
		{
			try
			{
				$record = ShlDbHelper::selectAssoc('#__sh404sef_urls', '*', 'oldurl = ? and newurl = ? and dateadd <> ?', array($reqPath, '', ''));

				$updatedRecord = empty($record) ? array(
					'id' => 0,
					'cpt' => 0,
					'rank' => 0,
					'oldurl' => $reqPath,
					'newurl' => '',
					'option' => '',
					'dateadd' => ShlSystem_Date::getUTCNow('Y-m-d'),
					'referrer_type' => Sh404sefHelperUrl::IS_UNKNOWN, // allow displaying a warning in 404 list, w/o having to do a join
				) : $record;

				// update counter
				$updatedRecord['cpt'] += 1;

				// find if internal request
				$referrer = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
				$isInternal = !empty($referrer) && JUri::isInternal($referrer);
				$isInternal ? Sh404sefHelperUrl::IS_INTERNAL : Sh404sefHelperUrl::IS_EXTERNAL;

				// get referrer and find if this is a local URL (but don't override a previous INTERNAL FLAG on that URL)
				if (empty($updatedRecord['referrer_type'])
					|| (!empty($updatedRecord['referrer_type']) && $updatedRecord['referrer_type'] != Sh404sefHelperUrl::IS_INTERNAL)
				)
				{
					$updatedRecord['referrer_type'] = $isInternal;
				}

				// write back the record, with updated counter
				if (empty($record))
				{
					ShlDbHelper::insert('#__sh404sef_urls', $updatedRecord);
				}
				else
				{
					ShlDbHelper::update('#__sh404sef_urls', $updatedRecord, array('id' => $updatedRecord['id']));
				}

				// record a detailed log of the 404, if set to
				if ($config->log404sHits)
				{
					$recorder = Sh404sefModelReqrecorder::getInstance(Sh404sefModelReqrecorder::REQUEST_404);
					$recorder->record($reqPath, '', $isInternal, $referrer);
				}
			}
			catch (Exception $e)
			{
				ShlSystem_Log::error('sh404sef', '%s::%d: %s', __METHOD__, __LINE__, ' Database error: ' . $e->getMessage());
				return false;
			}
		}

		return true;
	}
}
