<?php
/**
 * Shlib - programming library
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier 2015
 * @package      shlib
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      0.3.0.448
 * @date         2015-10-26
 */

// Security check to ensure this file is being included by a parent file.
defined('_JEXEC') or die;

/**
 * Manages html helpers
 *
 */
class ShlHtml_Manager
{
	static $assetsType = '.min';

	static $_manager = null;

	public static function getInstance()
	{
		if (is_null(self::$_manager))
		{
			self::$_manager = new ShlHtml_Manager;
		}

		return self::$_manager;
	}

	public function addAssets($document, $options = array())
	{
		$root = empty($options['root']) ? JURI::root(true) : $options['root'];
		$theme = empty($options['theme']) ? 'default' : $options['theme'];
		$document->addStyleSheet($root . '/media/plg_shlib/dist/css/theme.' . $theme . ShlHtml_Manager::$assetsType . '.css');
		return $this;
	}

	public function addSpinnerAssets($document, $options = array())
	{
		$root = empty($options['root']) ? JURI::root(true) : $options['root'];
		$document->addStyleSheet($root . '/media/plg_shlib/dist/css/spinner' . ShlHtml_Manager::$assetsType . '.css');
		$document->addScript($root . '/media/plg_shlib/dist/js/spinner' . ShlHtml_Manager::$assetsType . '.js');
		return $this;
	}
}
