<?php
/**
 * Shlib - programming library
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier 2015
 * @package     shlib
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     0.3.0.448
 * @date				2015-10-26
 */

// Security check to ensure this file is being included by a parent file.
defined('_JEXEC') or die;

/**
 * Base class to group our exceptions
 *
 * @author shumisha
 *
 */
class ShlException extends Exception {

}
