<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     sh404SEF
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     4.7.0.3065
 * @date		2015-10-26
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');

?>

<div id="sh-progress-analyticsprogress"></div>

<div id="analyticscontent_headers"></div>

<table style="width:100%;" class="qcontrol" id="sh404sef-analytics-wrapper">

    <tr>
      <td  width="100%" colspan="3" style="text-align: center;vertical-align: top;">
        <div id="analyticscontent_visits"></div>
      </td>
    </tr>

</table>
