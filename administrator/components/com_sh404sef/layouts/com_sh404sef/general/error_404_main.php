<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier - Weeblr llc - 2015
 * @package      sh404SEF
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      4.7.0.3065
 * @date        2015-10-26
 *
 */

/**
 * $displayData['text'] string the main content of the page
 * $displayData['language_tag'] string the full language page is identified by Joomla
 */

defined('_JEXEC') or die;

echo $displayData['text'];
