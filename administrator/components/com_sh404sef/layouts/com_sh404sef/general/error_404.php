<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author      Yannick Gaultier
 * @copyright   (c) Yannick Gaultier - Weeblr llc - 2015
 * @package     sh404SEF
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     4.7.0.3065
 * @date        2015-10-26
 */


defined('_JEXEC') or die;

// placeholder for B/C
include 'error_404_redirect_msg.php';
