<?php 

/**
 * @package         Engage Box
 * @version         3.3.3 Pro
 * 
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2016 Tassos Marinos All Rights Reserved
 * @license         GNU GPLv3 <http://www.gnu.org/licenses/gpl.html> or later
*/

defined('_JEXEC') or die('Restricted access');

class eBoxLog 
{
	/**
	 *  List of valid event IDs
	 *
     *  1 = Impressions
     *  2 = Closes
     *  3 = Engage
	 */
	private $events = array(1);

    /**
     *  Logs table
     *
     *  @var  string
     */
    private $table = "#__rstbox_logs";
	
    /**
     *  Logs box events to the database
     *
     *  @param   integer  $boxid    The box id
     *  @param   integer  $eventid  Event id
     *
     *  @return  bool     Returns a boolean indicating if the event logged successfully
     */
    function track($boxid, $eventid = 1)
    {
    	// Making sure we have a valid Boxid and Eventid
        if (!$boxid || !$eventid || !in_array($eventid, $this->events))
        {
            return;
        }

        // Return if session is not active
        if (!JFactory::getSession()->isActive())
        {           
        	return;
        }

        // Get visitor's token id
        if (!$visitorID = EBHelper::getVisitorID())
        {
        	return;
        }

        // Everything seems OK. Let's save data to db.
        $data = new stdClass();

        $data->sessionid = JFactory::getSession()->getId();
        $data->user      = JFactory::getUser()->id;
        $data->visitorid = $visitorID;
        $data->box       = $boxid;
        $data->event     = $eventid;
        $data->date      = JFactory::getDate()->toSql();
         
        // Insert the object into the user profile table.
        try
        {
            return JFactory::getDbo()->insertObject($this->table, $data);
        } 
        catch (Exception $e)
        {
            
        }
    }

    /**
     *  Removes old rows from the logs table
     *  Runs every 12 hours with a self-check
     *
     *  @return void
     */
    function clean()
    {
        include_once(JPATH_PLUGINS . '/system/nrframework/helpers/cache.php');

        $hash = MD5("eboxclean");
        $dontRun = NRCache::read($hash, true);

        if ($dontRun)
        {
            return;
        }

        $params = JComponentHelper::getParams('com_rstbox');
        $days = $params->get("statsdays", 180);

        // Removes rows older than $days days
        $db = JFactory::getDbo();
         
        $query = $db->getQuery(true);
        $query
            ->delete($db->quoteName($this->table))
            ->where($db->quoteName('date') . ' < DATE_SUB(NOW(), INTERVAL '.$days.' DAY)');
         
        $db->setQuery($query);
        $db->execute();

        // Write to cache file
        NRCache::write($hash, 1, 720);

        return true;
    }
}

?>