<?php
/**
 * @package		EasySocial
 * @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * EasySocial is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Unauthorized Access');

FD::import('admin:/tables/clustercategory');

class SocialTableFastfoundingCategory extends SocialTableClusterCategory
{

    /**
     * Retrieve the permalink of the category filter page
     *
     * @since   2.1
     * @access  public
     */
    public function getFilterPermalink($xhtml = true)
    {
        $url = ESR::events(array('categoryid' => $this->getAlias()), $xhtml);
        return $url;
    }


    /**
     * Preprocess before calling parent::store();
     *
     * @since   1.3
     * @access  public
     */
    public function store($updateNulls = false)
    {
        $this->type = SOCIAL_TYPE_FASTFOUNDING;

        return parent::store($updateNulls);
    }

    /**
     * Returns the total number of events in this category.
     *
     * @since  1.3
     * @access public
     */
    public function getTotalEvents($options = array())
    {
        static $total = array();

        $defaultOptions = array(
            'state' => SOCIAL_STATE_PUBLISHED,
            'type' => array(SOCIAL_EVENT_TYPE_PUBLIC, SOCIAL_EVENT_TYPE_PRIVATE),
            'category' =>  $this->id
        );

        if ($this->container) {
            // Get all child ids from this category
            $model = ES::model('ClusterCategory');
            $childs = $model->getChildCategories($this->id, array(), SOCIAL_TYPE_FASTFOUNDING, array('state' => SOCIAL_STATE_PUBLISHED));

            $childIds = array();

            foreach ($childs as $child) {
                $childIds[] = $child->id;
            }

            if (!empty($childIds)) {
                $defaultOptions['category'] = $childIds;
            }
        }

        if (!isset($options['type'])) {
            $user = ES::user();
            $options['type'] = $user->isSiteAdmin() ? 'all' : 'user';
        }

        // If this is from page/group event listing
        // We need to get a correct count
        if (isset($options['cluster']) && $options['cluster']) {
            $cluster = $options['cluster'];

            if ($cluster->getType() == SOCIAL_TYPE_PAGE) {
                $options['page_id'] = $cluster->id;
            }
            if ($cluster->getType() == SOCIAL_TYPE_GROUP) {
                $options['group_id'] = $cluster->id;
            }
        }

        $options = array_merge($defaultOptions, $options);

        ksort($options);

        $key = serialize($options);

        if (!isset($total[$this->id][$key])) {
            $total[$this->id][$key] = FD::model('events')->getTotalEvents($options);
        }

        return $total[$this->id][$key];
    }

    /**
     * Determines if the user has the points to create page
     *
     * @since   2.0.15
     * @access  public
     */
    public function hasPointsToCreate($userId = null)
    {
        $user = ES::user($userId);

        if ($user->isSiteAdmin()) {
            return true;
        }

        $valid = true;

        $access = ES::access($this->id, SOCIAL_TYPE_CLUSTERS);

        if ($access->get('userpoints.limit', 0)) {

            $points = $user->getPoints();

            if ($points < $access->get('userpoints.threshold')) {
                $valid = false;
            }
        }

        return $valid;
    }
}
