EasySocial.ready(function($) {

	$('[data-audio-uploads]').on('change', function() {
		var checkbox = $(this);
		var checked = checkbox.is(':checked');

		$('[data-encoder-option]').toggleClass('t-hidden', !checked);

		var echecked = $('[data-enable-encoder]').is(':checked');
		var showOptions = (echecked && checked);
		
		$('[data-audio-encoding]').toggleClass('t-hidden', !showOptions);
	});

	$('[data-enable-encoder]').on('change', function() {
		var checkbox = $(this);
		var checked = checkbox.is(':checked');

		$('[data-audio-encoding]').toggleClass('t-hidden', !checked);
	});
});