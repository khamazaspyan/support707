<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

require_once(__DIR__ . '/abstract.php');

class SocialCrawlerTwitch extends SocialCrawlerAbstract
{
	public function process(&$result)
	{
		// Check if the url should be processed here.
		if (stristr($this->url, 'twitch.tv') === false) {
			return;
		}

		// Generate the oembed url
		$oembed = $this->crawl($this->url);

		// Try to get the duration from the contents
		$oembed->duration = $oembed->video_length;
		$oembed->thumbnail = $oembed->thumbnail_url;

		$result->oembed = $oembed;

		// Do not use the opengraph data if there is an oembed data
		if ($result->oembed) {
			unset($result->opengraph);
		}
	}

	public function crawl($url)
	{
		// Need to ensure the URL is always twitch.tv
		$url = str_ireplace('go.twitch.tv', 'twitch.tv', $url);
		$endpoint = 'https://api.twitch.tv/v5/oembed?url=' . urlencode($url);

		$connector = ES::connector();
		$connector->addUrl($endpoint);
		$connector->connect();

		$contents = $connector->getResult($endpoint);

		$oembed = json_decode($contents);

		return $oembed;
	}
}
