<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

class ThemesHelperCover extends ThemesHelperAbstract
{
	/**
	 * Determines if the current active item is an app type
	 *
	 * @since	2.1.0
	 * @access	private
	 */
	private function isAppActive($currentActive)
	{
		$isAppActive = stristr($currentActive, 'apps.') !== false;

		return $isAppActive;
	}

	/**
	 * Renders the heading for an event
	 *
	 * @since	2.1.0
	 * @access	public
	 */
	public function event(SocialEvent $event, $active = 'timeline')
	{
		static $items = array();

		if (isset($items[$event->id])) {
			return $items[$event->id];
		}

		$totalPendingGuests = 0;

		if ($event->isAdmin()) {
			$totalPendingGuests = $event->getTotalPendingGuests();
		}

		$cover = $event->getCoverData();


		$model = ES::model('Apps');
		$apps = $model->getEventApps($event->id);

		$discussions = null;
		$news = null;
		$events = null;

		// These are the known apps that would be rendered below the cover
		$knownApps = array('discussions', 'news', 'events');

		// We need to exclude certain apps since they are already rendered under the apps dropdown
		if (!$this->isMobile()) {
			$exclusion = array('followers');

			$exclusion = array_merge($exclusion, $knownApps);
			$tmp = array();

			foreach ($apps as $app) {
				if (!in_array($app->element, $exclusion)) {
					$tmp[] = $app;
				}

				if (in_array($app->element, $knownApps)) {
					${$app->element} = $app;
				}
			}

			$apps = $tmp;
		}

		$returnUrl = base64_encode(JRequest::getUri());

		// Get the timeline link
		$defaultDisplay = $this->config->get('events.item.display', 'timeline');
		$timelinePermalink = $event->getPermalink();
		$aboutPermalink = ESR::events(array('id' => $event->getAlias(), 'page' => 'info', 'layout' => 'item'));

		if ($defaultDisplay == 'info') {
			$aboutPermalink = $timelinePermalink;
			$timelinePermalink = ESR::events(array('id' => $event->getAlias(), 'page' => 'timeline', 'layout' => 'item'));
		}

		$showMore = false;

		if ($event->allowPhotos() || $event->allowVideos() || $event->allowAudios() || $apps) {
			$showMore = true;
		}

		$isAppActive = $this->isAppActive($active);

		// Since some of the links are hidden on the apps, we need to check if apps should be active
		if (!$isAppActive) {
			if ($active == 'news' || $active == 'discussions') {
				$isAppActive = true;
			}
		}

		$theme = ES::themes();
		$theme->set('isAppActive', $isAppActive);
		$theme->set('timelinePermalink', $timelinePermalink);
		$theme->set('aboutPermalink', $aboutPermalink);
		$theme->set('totalPendingGuests', $totalPendingGuests);
		$theme->set('active', $active);
		$theme->set('event', $event);
		$theme->set('cover', $cover);
		$theme->set('apps', $apps);
		$theme->set('showMore', $showMore);
		$theme->set('returnUrl', $returnUrl);


		foreach ($knownApps as $knownApp) {
			$theme->set($knownApp, ${$knownApp});
		}
		
		$items[$event->id] = $theme->output('site/helpers/cover/event');

		return $items[$event->id];
	}

	/**
	 * Renders the heading for a page
	 *
	 * @since	2.1.0
	 * @access	public
	 */
	public function page(SocialPage $page, $active = 'timeline')
	{
		static $items = array();

		if (isset($items[$page->id])) {
			return $items[$page->id];
		}

		$pendingFollowers = 0;

		if ($page->isAdmin()) {
			$pendingFollowers = $page->getTotalPendingFollowers();
		}

		$cover = $page->getCoverData();

		$model = ES::model('Apps');
		$apps = $model->getPageApps($page->id);

		$discussions = null;
		$news = null;
		$events = null;

		// These are the known apps that would be rendered below the cover
		$knownApps = array('discussions', 'news', 'events');

		// We need to exclude certain apps since they are already rendered under the apps dropdown
		if (!$this->isMobile()) {
			$exclusion = array('followers');

			$exclusion = array_merge($exclusion, $knownApps);
			$tmp = array();

			foreach ($apps as $app) {
				if (!in_array($app->element, $exclusion)) {
					$tmp[] = $app;
				}

				if (in_array($app->element, $knownApps)) {
					${$app->element} = $app;
				}
			}

			$apps = $tmp;
		}

		// Get the timeline link
		$defaultDisplay = $this->config->get('pages.item.display', 'timeline');
		$timelinePermalink = $page->getPermalink();
		$aboutPermalink = ESR::pages(array('id' => $page->getAlias(), 'type' => 'info', 'layout' => 'item'));

		if ($defaultDisplay == 'info') {
			$aboutPermalink = $timelinePermalink;
			$timelinePermalink = ESR::pages(array('id' => $page->getAlias(), 'type' => 'timeline', 'layout' => 'item'));
		}

		$showMore = false;

		if ($page->allowPhotos() || $page->allowVideos() || $page->allowAudios() || $page->canViewEvent() || $apps) {
			$showMore = true;
		}

		$isAppActive = $this->isAppActive($active);

		if (!$isAppActive) {
			if ($active == 'news' || $active == 'discussions') {
				$isAppActive = true;
			}
		}

		$theme = ES::themes();
		$theme->set('isAppActive', $isAppActive);
		$theme->set('timelinePermalink', $timelinePermalink);
		$theme->set('aboutPermalink', $aboutPermalink);
		$theme->set('pendingFollowers', $pendingFollowers);
		$theme->set('active', $active);
		$theme->set('page', $page);
		$theme->set('cover', $cover);
		$theme->set('apps', $apps);
		$theme->set('showMore', $showMore);
		
		foreach ($knownApps as $knownApp) {
			$theme->set($knownApp, ${$knownApp});
		}

		$items[$page->id] = $theme->output('site/helpers/cover/page');

		return $items[$page->id];
	}

	/**
	 * Renders the heading for a group
	 *
	 * @since	2.1.0
	 * @access	public
	 */
	public function group(SocialGroup $group, $active = 'timeline')
	{
		static $items = array();

		if (isset($items[$group->id])) {
			return $items[$group->id];
		}

		$pendingMembers = 0;

		if ($group->isAdmin()) {
			$pendingMembers = $group->getTotalPendingMembers();
		}

		$cover = $group->getCoverData();

		$model = ES::model('Apps');
		$apps = $model->getGroupApps($group->id);

		$discussions = null;
		$news = null;
		$events = null;
		
		// These are the known apps that would be rendered below the cover
		$knownApps = array('discussions', 'news', 'events');

		// We need to exclude certain apps since they are already rendered under the apps dropdown
		if (!$this->isMobile()) {
			$exclusion = array('members');

			$exclusion = array_merge($exclusion, $knownApps);
			$tmp = array();

			foreach ($apps as $app) {
				if (!in_array($app->element, $exclusion)) {
					$tmp[] = $app;
				}

				if (in_array($app->element, $knownApps)) {
					${$app->element} = $app;
				}
			}

			$apps = $tmp;
		}

		// Get the timeline link
		$defaultDisplay = $this->config->get('groups.item.display', 'timeline');
		$timelinePermalink = $group->getPermalink();
		$aboutPermalink = ESR::groups(array('id' => $group->getAlias(), 'type' => 'info', 'layout' => 'item'));

		if ($defaultDisplay == 'info') {
			$aboutPermalink = $timelinePermalink;
			$timelinePermalink = ESR::groups(array('id' => $group->getAlias(), 'type' => 'timeline', 'layout' => 'item'));
		}

		$showMore = false;

		if ($group->allowPhotos() || $group->allowVideos() || $group->allowAudios() || $group->canViewEvent() || $apps) {
			$showMore = true;
		}

		$isAppActive = $this->isAppActive($active);

		if (!$isAppActive) {
			if ($active == 'news' || $active == 'discussions') {
				$isAppActive = true;
			}
		}

		$theme = ES::themes();
		$theme->set('isAppActive', $isAppActive);
		$theme->set('timelinePermalink', $timelinePermalink);
		$theme->set('aboutPermalink', $aboutPermalink);
		$theme->set('pendingMembers', $pendingMembers);
		$theme->set('active', $active);
		$theme->set('group', $group);
		$theme->set('cover', $cover);
		$theme->set('apps', $apps);
		$theme->set('showMore', $showMore);

		foreach ($knownApps as $knownApp) {
			$theme->set($knownApp, ${$knownApp});
		}

		$items[$group->id] = $theme->output('site/helpers/cover/group');

		return $items[$group->id];
	}

	/**
	 * Renders the heading for a user
	 *
	 * @since	2.1.0
	 * @access	public
	 */
	public function user(SocialUser $user, $active = 'timeline')
	{
		static $items = array();

		if (isset($items[$user->id])) {
			return $items[$user->id];
		}

		// Get user's cover object
		$cover = $user->getCoverData();

		// If we're setting a cover
		$coverId = $this->input->get('cover_id', 0, 'int');

		// Load cover photo
		if ($coverId) {
			$coverTable = ES::table('Photo');
			$coverTable->load($coverId);

			// If the cover photo belongs to the user
			if ($coverTable->isMine()) {
				$newCover = $coverTable;
			}
		}

		// Determines if the avatar should be visible
		$showAvatar = $user->getAvatarPhoto() && $this->my->getPrivacy()->validate('photos.view', $user->getAvatarPhoto()->id, SOCIAL_TYPE_PHOTO, $user->id);

		// Get lists of badges of the user.
		$badges = $user->getBadges();

		// Determine if user has pending friends
		$pendingFriends = 0;

		if ($user->id == $this->my->id) {
			$model = ES::model('Friends');
			$pendingFriends = $model->getTotalPendingFriends($user->id);
		}

		// Get the timeline link
		$defaultDisplay = $this->config->get('users.profile.display', 'timeline');
		$timelinePermalink = $user->getPermalink();
		$aboutPermalink = ESR::profile(array('id' => $user->getAlias(), 'layout' => 'about'));

		if ($defaultDisplay == 'about') {
			$aboutPermalink = $timelinePermalink;
			$timelinePermalink = ESR::profile(array('id' => $user->getAlias(), 'layout' => 'timeline'));
		}

		// Retrieve list of apps for this user
		$profile = $user->getProfile();

		$model = ES::model('Apps');
		$apps = $model->getUserApps($user->id, true, array('includeDefault' => true));

		$showMore = false;

		if (($user->canCreateAlbums() || $user->canCreateVideos() || $user->canCreateAudios() || $this->config->get('followers.enabled') || $this->config->get('groups.enabled') || $this->config->get('events.enabled') || $this->config->get('pages.enabled') || ($apps && $this->config->get('users.layout.sidebarapps')))) {
			$showMore = true;
		}

		$isAppActive = $this->isAppActive($active);

		// Since some of the links are hidden on the apps, we need to check if apps should be active
		if (!$isAppActive) {
			if ($active == 'groups' || $active == 'followers' || $active == 'pages' || $active == 'events') {
				$isAppActive = true;
			}
		}

		$theme = ES::themes();
		$theme->set('isAppActive', $isAppActive);
		$theme->set('timelinePermalink', $timelinePermalink);
		$theme->set('aboutPermalink', $aboutPermalink);
		$theme->set('defaultDisplay', $defaultDisplay);
		$theme->set('pendingFriends', $pendingFriends);
		$theme->set('active', $active);
		$theme->set('apps', $apps);
		$theme->set('user', $user);
		$theme->set('cover', $cover);
		$theme->set('showAvatar', $showAvatar);
		$theme->set('showMore', $showMore);
		$theme->set('badges', $badges);

		$items[$user->id] = $theme->output('site/helpers/cover/user');

		return $items[$user->id];
	}
}
