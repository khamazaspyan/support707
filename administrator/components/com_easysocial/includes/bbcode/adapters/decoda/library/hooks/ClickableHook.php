<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

/**
 * ClickableHook
 *
 * Converts URLs and emails (not wrapped in tags) into clickable links.
 *
 * @author      Miles Johnson - http://milesj.me
 * @copyright   Copyright 2006-2011, Miles Johnson, Inc.
 * @license     http://opensource.org/licenses/mit-license.php - Licensed under The MIT License
 * @link        http://milesj.me/code/php/decoda
 */

class ClickableHook extends DecodaHook {

	/**
	 * Matches a link or an email, and converts it to an anchor tag.
	 *
	 * @access public
	 * @param string $content
	 * @return string
	 */
	public function afterParse($content) {

		if ($this->getParser()->getFilter('Url')) {

            // $chars		= preg_quote('-_=+|\;:&?/[]%,.!@#$*(){}"\'', '/');
            // $protocols	= array('http', 'https', 'ftp', 'irc', 'file', 'telnet');
            // $pattern	= implode('', array(
						      //           '(' . implode('|', $protocols) . ')s?:\/\/', // protocol
						      //           '([\w\.\+]+:[\w\.\+]+@)?', // login
						      //           '([\w\.]{5,255}+)', // domain, tld
						      //           '(:[0-9]{0,6}+)?', // port
						      //           '([a-z0-9' . $chars . ']+)?', // query
						      //           '(#[a-z0-9' . $chars . ']+)?' // fragment
						      //       )
            // 			);

            // $content	= preg_replace_callback('/("|\'|>|<br>|<br\/>)?(' . $pattern . ')/is', array($this, '_urlCallback'), $content);
			$protocol	= '(http|ftp|irc|file|telnet)s?:\/?\/?';
			$login		= '([-a-zA-Z0-9\.\+]+:[-a-zA-Z0-9\.\+]+@)?';
			$domain		= '([-a-zA-Z0-9\.]{5,255}+)';
			$port		= '(:[0-9]{0,6}+)?';
			$query		= '([a-zA-Z0-9' . preg_quote('-_=;:&?/[].,', '/') . '\(\)]+)?';
			$pattern 	= '/(^|\n|\s)' . $protocol . $login . $domain . $query . '/is';

			// $pattern = '/(^|\s)((https|ftp|http|irc|file|telnet):\/\/)?(([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.([a-z]{2,7}))|(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))(:[0-9]{1,5})?(\/.*)?/i';

			$extendedlatinPattern = "\\x{0c0}-\\x{0ff}\\x{100}-\\x{1ff}\\x{180}-\\x{27f}";

			$pattern = '/<img[^>]*>(*SKIP)(*FAIL)|((?<!href=")((http|https):\/{2})+(([0-9a-z_-]+\.)+(aero|asia|berlin|biz|cat|com|coop|edu|gov|guru|club|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mn|mn|mo|mp|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|nom|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ra|rs|ru|ro|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|arpa|guru|live|today|bio|xxx)(:[0-9]+)?((\/([~'.$extendedlatinPattern.'0-9a-zA-Z\#\!\=\+\%@\(\)\:\;\,\.\/_-]+))?(\?[' . $extendedlatinPattern . '0-9a-zA-Z\+\%@\/&\[\];=_-]+)?)?))\b/iu';
			$content = preg_replace_callback($pattern, array($this, '_urlCallback'), $content);

		}

		// Based on schema: http://en.wikipedia.org/wiki/Email_address
		if ($this->getParser()->getFilter('Email')) {
			$content = preg_replace_callback(EmailFilter::EMAIL_PATTERN, array($this, '_emailCallback'), $content);
		}

		return $content;
	}

	/**
	 * Callback for email processing.
	 *
	 * @access protected
	 * @param array $matches
	 * @return string
	 */
	protected function _emailCallback($matches) {
		return $this->getParser()->getFilter('Email')->parse(array(
			'tag' => 'email',
			'attributes' => array()
		), trim($matches[0]));
	}

	/**
	 * Callback for URL processing.
	 *
	 * @access protected
	 * @param array $matches
	 * @return string
	 */
	protected function _urlCallback($matches) {
		return $this->getParser()->getFilter('Url')->parse(array(
			'tag' => 'url',
			'attributes' => array('target' => '_blank', 'rel' => 'nofollow')
		), trim($matches[0]));
	}

}
