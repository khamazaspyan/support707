<?php die("Access Denied"); ?>#x#s:1436:"<?xml version="1.0" encoding="utf-8"?>
<extensions>
	<extension>
		<name>Advanced Module Manager</name>
		<id>advancedmodulemanager</id>
		<alias>advancedmodulemanager</alias>
		<extname>advancedmodules</extname>
		<version>7.2.2</version>
		<has_pro>1</has_pro>
		<pro>0</pro>
		<downloadurl>http://download.regularlabs.com/?ext=advancedmodulemanager</downloadurl>
		<downloadurl_pro></downloadurl_pro>
		<changelog><![CDATA[
<div style="font-family:monospace;font-size:1.2em;">10-Oct-2017 : v7.2.2<br /> ^ Improves loading speed of module edit page by loading select lists through ajax<br /> ^ Updates translations: bg-BG, da-DK, nb-NO<br /> # Fixes issue with multi-select fields not showing correctly<br /> # Fixes issue with some multi-select fields not storing the selection<br /> # [PRO] Fixes issue with IP range conditions not working<br /> # [PRO] Fixes issue with some locations in Mexico City not matching region 'Distrito Federal'<br /><br />05-Sep-2017 : v7.2.1<br /> # Fixes issue with menu assignments not working when using aliases<br /> # Fixes issue with menu assignments not working when using aliases<br /> # Fixes issue with some assignments return false positives<br /><br />04-Sep-2017 : v7.2.0<br /> + Adds ability to collect advanced parameters without using cache (usage for other extensions)</div><div style="text-align: right;"><i>...click for more...</i></div>
		]]></changelog>
	</extension>
</extensions>";