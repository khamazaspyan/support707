<?php
/**
 *  @package AdminTools
 *  @copyright Copyright (c)2012-2013 Nicholas K. Dionysopoulos
 *  @license GNU General Public License version 3, or later
 *  @version $Id$
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

defined('_JEXEC') or die();

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');


class pkg_jgiveInstallerScript
{
	/** @var array The list of extra modules and plugins to install */
	private $installation_queue = array(
	// modules => { (folder) => { (module) => { (position), (published) } }* }*
		'modules' => array('admin' => array(), 'site' => array('mod_jgive_campaigns' => 0, 'mod_jgive_campaigns_pin' => 0, 'mod_jgive_donations' => 0)),
	// plugins => { (folder) => { (element) => (published) }* }*
		'plugins' => array('system' => array('jgive_api' => 1, 'jgiveactivities' => 1, 'jma_campaign_jgive' => 0, 'jgive_invitex_email' => 0, 'tjassetsloader' => 1), 'community' => array('jgive' => 0), 'payment' => array('2checkout' => 0, 'alphauserpoints' => 0, 'authorizenet' => 1, 'bycheck' => 1, 'byorder' => 1, 'ccavenue' => 0, 'jomsocialpoints' => 0, 'linkpoint' => 1, 'paypal' => 1, 'paypalpro' => 0, 'payu' => 1, 'amazon' => 0, 'ogone' => 0, 'paypal_adaptive_payment' => 0), 'tjvideo' => array('jwplayer' => 1, 'vimeo' => 1)), 'libraries' => array('activity' => 1, 'techjoomla' => 1));

	/** @var array The list of extra modules and plugins to install */
	private $uninstall_queue = array(
	// modules => { (folder) => { (module) => { (position), (published) } }* }*
		'modules' => array('admin' => array(), 'site' => array('mod_jgive_campaigns' => 0, 'mod_jgive_campaigns_pin' => 0, 'mod_jgive_donations' => 0)),
	// plugins => { (folder) => { (element) => (published) }* }*
		'plugins' => array('system' => array('jgive_api' => 1, 'jgiveactivities' => 1, 'jma_campaign_jgive' => 0, 'jgive_invitex_email' => 0), 'community' => array('jgive' => 0), 'payment' => array(), 'tjvideo' => array()));

	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent)
	{
		if (!defined('DS'))
		{
			define('DS', DIRECTORY_SEPARATOR);
		}

		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		// Only allow to install on Joomla! 2.5.0 or later
		//return version_compare(JVERSION, '2.5.0', 'ge');
	}

	/**
	 * Runs after install, update or discover_update
	 * @param string $type install, update or discover_update
	 * @param JInstaller $parent
	 */
	function postflight($type, $parent)
	{
		// Install subextensions
		$status = $this->_installSubextensions($parent);

		// Install FOF
		$fofStatus = $this->_installFOF($parent);

		// Install Techjoomla Straper
		$straperStatus = $this->_installStraper($parent);

		// Show the post-installation page
		$this->_renderPostInstallation($status, $fofStatus, $straperStatus, $parent);
	}

	/**
	 * Renders the post-installation message
	 */
	private function _renderPostInstallation($status, $fofStatus, $straperStatus, $parent)
	{
		// Show link for payment plugins.
		$rows = 1; ?>

		<table class="adminlist">
			<thead>
				<tr>
					<th class="title" colspan="2">Extension</th>
					<th width="30%">Status</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
			<tbody>
				<tr class="row0">
					<td class="key" colspan="2"><h4>JGive component</h4></td>
					<td><strong style="color: green">Installed</strong></td>
				</tr>
				<tr class="row0">
					<td class="key" colspan="2"><h4>TjFields component</h4></td>
					<td><strong style="color: green">Installed</strong></td>
				</tr>
				<tr class="row0">
					<td class="key" colspan="2"><h4>TjActivityStream component</h4></td>
					<td><strong style="color: green">Installed</strong></td>
				</tr>
				<tr class="row1">
					<td class="key" colspan="2">
						<strong>Framework on Framework (FOF)
						<?php echo $fofStatus['version'];?></strong> [<?php echo $fofStatus['date'];?>]
					</td>
					<td><strong>
						<span style="color: <?php echo $fofStatus['required'] ? ($fofStatus['installed'] ? 'green' : 'red') : '#660';?>; font-weight: bold;">
							<?php echo $fofStatus['required'] ? ($fofStatus['installed'] ? 'Installed' : 'Not Installed') : 'Already up-to-date'; ?>
						</span>
					</strong></td>
				</tr>
				<tr class="row0">
					<td class="key" colspan="2">
						<strong>TechJoomla Strapper <?php echo $straperStatus['version'];?></strong> [<?php echo $straperStatus['date']; ?>]
					</td>
					<td><strong>
						<span style="color: <?php echo $straperStatus['required'] ? ($straperStatus['installed'] ? 'green' : 'red') : '#660'; ?>; font-weight: bold;">
							<?php echo $straperStatus['required'] ? ($straperStatus['installed'] ? 'Installed' : 'Not Installed') : 'Already up-to-date'; ?>
						</span>
					</strong></td>
				</tr>
				<?php if (count($status->modules)):?>
				<tr>
					<th>Module</th>
					<th>Client</th>
					<th></th>
				</tr>
				<?php foreach ($status->modules as $module): ?>
				<tr class="row<?php
				echo ($rows++ % 2); ?>">
					<td class="key"><?php
				echo $module['name']; ?></td>
					<td class="key"><?php
				echo ucfirst($module['client']); ?></td>
					<td><strong style="color: <?php
				echo ($module['result']) ? "green" : "red";?>"><?php
				echo ($module['result']) ? 'Installed' : 'Not installed';?></strong></td>
				</tr>
				<?php
			endforeach;?>
				<?php endif;?>
				<?php
		if (count($status->plugins)): ?>
				<tr>
					<th>Plugin</th>
					<th>Group</th>
					<th></th>
				</tr>
				<?php
			foreach ($status->plugins as $plugin):?>
				<tr class="row<?php
				echo ($rows++ % 2);?>">
					<td class="key"><?php
				echo ucfirst($plugin['name']);?></td>
					<td class="key"><?php
				echo ucfirst($plugin['group']);?></td>
					<td><strong style="color: <?php
				echo ($plugin['result']) ? "green" : "red";?>"><?php
				echo ($plugin['result']) ? 'Installed' : 'Not installed';?></strong></td>
				</tr>
				<?php
			endforeach;?>
				<?php
		endif;?>
				<?php
		if (count($status->libraries)):?>
				<tr class="row1">
					<th>Library</th>
					<th></th>
					<th></th>
					</tr>
				<?php
			foreach ($status->libraries as $libraries):?>
				<tr class="row2 <?php //echo ($rows++ % 2);?>">
					<td class="key"><?php
				echo ucfirst($libraries['name']);?></td>
					<td class="key"></td>
					<td><strong style="color: <?php
				echo ($libraries['result']) ? "green" : "red";?>"><?php
				echo ($libraries['result']) ? 'Installed' : 'Not installed';?></strong>
					<?php
				if (!empty($libraries['result'])) // if installed then only show msg
				{
					echo $mstat = ($libraries['status'] ? "<span class=\"label label-success\">Enabled</span>" : "<span class=\"label label-important\">Disabled</span>");

				}?>

					</td>
				</tr>
				<?php
			endforeach;?>
				<?php
		endif;?>


			<?php
		if (!empty($status->app_install))
		{
			if (count($status->app_install)):?>
				<tr class="row1">
					<th>EasySocial App</th>
					<th></th>
					<th></th>
					</tr>
				<?php
				foreach ($status->app_install as $app_install):?>
				<tr class="row2">
					<td class="key"><?php
					echo ucfirst($app_install['name']);?></td>
					<td class="key"></td>
					<td><strong style="color: <?php
					echo ($app_install['result']) ? "green" : "red";?>"><?php
					echo ($app_install['result']) ? 'Installed' : 'Not installed';?></strong>
					<?php
					if (!empty($app_install['result'])) // if installed then only show msg
					{
						echo $mstat = ($app_install['status'] ? "<span class=\"label label-success\">Enabled</span>" : "<span class=\"label label-important\">Disabled</span>");

					}?>

					</td>
				</tr>
				<?php
				endforeach;?>
				<?php
			endif;
		}?>

			</tbody>
		</table>

		<br/>
		<div class="row-fluid">
			<div class="span12">
				<div class="alert alert-info">
					<a class="btn"
					href="index.php?option=com_jgive&task=cp.updateAllCampaignsSuccessStatus"
					target="_blank">
						Click here
					</a>
					 to update 'Campaign Success Status' for all campaigns.
				</div>
			</div>
		</div>
		<br/>
		<div class="row-fluid">
			<div class="span12">
				<div class="alert alert-info">
					<a class="btn"
					href="index.php?option=com_jgive&task=createActivity"
					target="_blank">
						<i class="icon-refresh"></i>
						Create Activities
					</a>
					to create the activities for your already existed campaigns.
				</div>
			</div>
		</div>
		<br/>

		<?php
	}

	/**
	 * Installs subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param JInstaller $parent
	 * @return JObject The subextension installation status
	 */
	private function _installSubextensions($parent)
	{
		$src = $parent->getParent()->getPath('source');

		$db = JFactory::getDbo();

		$status          = new JObject();
		$status->modules = array();
		$status->plugins = array();

		// Modules installation
		if (count($this->installation_queue['modules']))
		{
			foreach ($this->installation_queue['modules'] as $folder => $modules)
			{
				if (count($modules))
					foreach ($modules as $module => $modulePreferences)
					{
						// Install the module
						if (empty($folder))
							$folder = 'site';
						$path = "$src/modules/$folder/$module";
						if (!is_dir($path))
						{
							$path = "$src/modules/$folder/mod_$module";
						}
						if (!is_dir($path))
						{
							$path = "$src/modules/$module";
						}
						if (!is_dir($path))
						{
							$path = "$src/modules/mod_$module";
						}
						if (!is_dir($path))
							continue;
						// Was the module already installed?
						$sql = $db->getQuery(true)->select('COUNT(*)')->from('#__modules')->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
						$db->setQuery($sql);
						$count             = $db->loadResult();
						$installer         = new JInstaller;
						$result            = $installer->install($path);
						$status->modules[] = array(
							'name' => 'mod_' . $module,
							'client' => $folder,
							'result' => $result
						);
						// Modify where it's published and its published state
						if (!$count)
						{
							// A. Position and state
							list($modulePosition, $modulePublished) = $modulePreferences;
							if ($modulePosition == 'cpanel')
							{
								$modulePosition = 'icon';
							}
							$sql = $db->getQuery(true)->update($db->qn('#__modules'))->set($db->qn('position') . ' = ' . $db->q($modulePosition))->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
							if ($modulePublished)
							{
								$sql->set($db->qn('published') . ' = ' . $db->q('1'));
							}
							$db->setQuery($sql);
							$db->execute();

							// B. Change the ordering of back-end modules to 1 + max ordering
							if ($folder == 'admin')
							{
								$query = $db->getQuery(true);
								$query->select('MAX(' . $db->qn('ordering') . ')')->from($db->qn('#__modules'))->where($db->qn('position') . '=' . $db->q($modulePosition));
								$db->setQuery($query);
								$position = $db->loadResult();
								$position++;

								$query = $db->getQuery(true);
								$query->update($db->qn('#__modules'))->set($db->qn('ordering') . ' = ' . $db->q($position))->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
								$db->setQuery($query);
								$db->execute();
							}

							// C. Link to all pages
							$query = $db->getQuery(true);
							$query->select('id')->from($db->qn('#__modules'))->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
							$db->setQuery($query);
							$moduleid = $db->loadResult();

							$query = $db->getQuery(true);
							$query->select('*')->from($db->qn('#__modules_menu'))->where($db->qn('moduleid') . ' = ' . $db->q($moduleid));
							$db->setQuery($query);
							$assignments = $db->loadObjectList();
							$isAssigned  = !empty($assignments);
							if (!$isAssigned)
							{
								$o = (object) array(
									'moduleid' => $moduleid,
									'menuid' => 0
								);
								$db->insertObject('#__modules_menu', $o);
							}
						}
					}
			}
		}

		// Plugins installation
		if (count($this->installation_queue['plugins']))
		{
			foreach ($this->installation_queue['plugins'] as $folder => $plugins)
			{
				if (count($plugins))
					foreach ($plugins as $plugin => $published)
					{
						$path = "$src/plugins/$folder/$plugin";
						if (!is_dir($path))
						{
							$path = "$src/plugins/$folder/plg_$plugin";
						}
						if (!is_dir($path))
						{
							$path = "$src/plugins/$plugin";
						}
						if (!is_dir($path))
						{
							$path = "$src/plugins/plg_$plugin";
						}
						if (!is_dir($path))
							continue;

						// Was the plugin already installed?
						$query = $db->getQuery(true)->select('COUNT(*)')->from($db->qn('#__extensions'))->where($db->qn('element') . ' = ' . $db->q($plugin))->where($db->qn('folder') . ' = ' . $db->q($folder));
						$db->setQuery($query);
						$count = $db->loadResult();

						$installer = new JInstaller;
						$result    = $installer->install($path);

						$status->plugins[] = array(
							'name' => 'plg_' . $plugin,
							'group' => $folder,
							'result' => $result
						);

						if ($published && !$count)
						{
							$query = $db->getQuery(true)->update($db->qn('#__extensions'))->set($db->qn('enabled') . ' = ' . $db->q('1'))->where($db->qn('element') . ' = ' . $db->q($plugin))->where($db->qn('folder') . ' = ' . $db->q($folder));
							$db->setQuery($query);
							$db->execute();
						}
					}
			}
		}
		if (count($this->installation_queue['libraries']))
		{
			foreach ($this->installation_queue['libraries'] as $folder => $status1)
			{

				$path = "$src/libraries/$folder";

				$query = $db->getQuery(true)->select('COUNT(*)')->from($db->qn('#__extensions'))->where('( ' . ($db->qn('name') . ' = ' . $db->q($folder)) . ' OR ' . ($db->qn('element') . ' = ' . $db->q($folder)) . ' )')->where($db->qn('folder') . ' = ' . $db->q($folder));
				$db->setQuery($query);
				$count = $db->loadResult();

				$installer = new JInstaller;
				$result    = $installer->install($path);

				$status->libraries[] = array(
					'name' => $folder,
					'group' => $folder,
					'result' => $result,
					'status' => $status1
				);

				if ($published && !$count)
				{
					$query = $db->getQuery(true)->update($db->qn('#__extensions'))->set($db->qn('enabled') . ' = ' . $db->q('1'))->where('( ' . ($db->qn('name') . ' = ' . $db->q($folder)) . ' OR ' . ($db->qn('element') . ' = ' . $db->q($folder)) . ' )')->where($db->qn('folder') . ' = ' . $db->q($folder));
					$db->setQuery($query);
					$db->execute();
				}
			}
		}
		//install easysocial plugin
		if (file_exists(JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/foundry.php'))
		{
			require_once(JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/foundry.php');
			$installer = Foundry::get('Installer');
			// The $path here refers to your application path
			$installer->load($src . "/plugins/easysocial_camp_plg");
			$plg_install           = $installer->install();
			$status->app_install[] = array(
				'name' => 'easysocial_camp_plg',
				'group' => 'easysocial_camp_plg',
				'result' => $plg_install,
				'status' => '1'
			);
		}

		return $status;
	}


	private function _installFOF($parent)
	{
		$src = $parent->getParent()->getPath('source');

		// Install the FOF framework
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.date');

		/*$source = $src.'/fof';*/
		//changed by manoj
		$source = $src . '/tj_lib_fof';

		if (!defined('JPATH_LIBRARIES'))
		{
			$target = JPATH_ROOT . '/libraries/fof';
		}
		else
		{
			$target = JPATH_LIBRARIES . '/fof';
		}
		$haveToInstallFOF = false;
		if (!file_exists($target))
		{
			$haveToInstallFOF = true;
		}
		else
		{
			$fofVersion = array();
			if (JFile::exists($target . '/version.txt'))
			{
				$rawData                 = file_get_contents($target . '/version.txt');
				$info                    = explode("\n", $rawData);
				$fofVersion['installed'] = array(
					'version' => trim($info[0]),
					'date' => new JDate(trim($info[1]))
				);
			}
			else
			{
				$fofVersion['installed'] = array(
					'version' => '0.0',
					'date' => new JDate('2011-01-01')
				);
			}
			$rawData               = file_get_contents($source . '/version.txt');
			$info                  = explode("\n", $rawData);
			$fofVersion['package'] = array(
				'version' => trim($info[0]),
				'date' => new JDate(trim($info[1]))
			);

			$haveToInstallFOF = $fofVersion['package']['date']->toUNIX() > $fofVersion['installed']['date']->toUNIX();
		}

		$installedFOF = false;
		if ($haveToInstallFOF)
		{
			$versionSource = 'package';
			$installer     = new JInstaller;
			$installedFOF  = $installer->install($source);
		}
		else
		{
			$versionSource = 'installed';
		}

		if (!isset($fofVersion))
		{
			$fofVersion = array();
			if (JFile::exists($target . '/version.txt'))
			{
				$rawData                 = file_get_contents($target . '/version.txt');
				$info                    = explode("\n", $rawData);
				$fofVersion['installed'] = array(
					'version' => trim($info[0]),
					'date' => new JDate(trim($info[1]))
				);
			}
			else
			{
				$fofVersion['installed'] = array(
					'version' => '0.0',
					'date' => new JDate('2011-01-01')
				);
			}
			$rawData               = file_get_contents($source . '/version.txt');
			$info                  = explode("\n", $rawData);
			$fofVersion['package'] = array(
				'version' => trim($info[0]),
				'date' => new JDate(trim($info[1]))
			);
			$versionSource         = 'installed';
		}

		if (!($fofVersion[$versionSource]['date'] instanceof JDate))
		{
			$fofVersion[$versionSource]['date'] = new JDate();
		}

		return array(
			'required' => $haveToInstallFOF,
			'installed' => $installedFOF,
			'version' => $fofVersion[$versionSource]['version'],
			'date' => $fofVersion[$versionSource]['date']->format('Y-m-d')
		);
	}

	private function _installStraper($parent)
	{
		$src = $parent->getParent()->getPath('source');

		// Install the FOF framework
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.date');
		$source = $src . '/tj_strapper';
		$target = JPATH_ROOT . '/media/techjoomla_strapper';

		$haveToInstallStraper = false;
		if (!file_exists($target))
		{
			$haveToInstallStraper = true;
		}
		else
		{
			$straperVersion = array();
			if (JFile::exists($target . '/version.txt'))
			{
				$rawData                     = file_get_contents($target . '/version.txt');
				$info                        = explode("\n", $rawData);
				$straperVersion['installed'] = array(
					'version' => trim($info[0]),
					'date' => new JDate(trim($info[1]))
				);
			}
			else
			{
				$straperVersion['installed'] = array(
					'version' => '0.0',
					'date' => new JDate('2011-01-01')
				);
			}
			$rawData                   = file_get_contents($source . '/version.txt');
			$info                      = explode("\n", $rawData);
			$straperVersion['package'] = array(
				'version' => trim($info[0]),
				'date' => new JDate(trim($info[1]))
			);

			$haveToInstallStraper = $straperVersion['package']['date']->toUNIX() > $straperVersion['installed']['date']->toUNIX();
		}

		$installedStraper = false;
		if ($haveToInstallStraper)
		{
			$versionSource    = 'package';
			$installer        = new JInstaller;
			$installedStraper = $installer->install($source);
		}
		else
		{
			$versionSource = 'installed';
		}

		if (!isset($straperVersion))
		{
			$straperVersion = array();
			if (JFile::exists($target . '/version.txt'))
			{
				$rawData                     = file_get_contents($target . '/version.txt');
				$info                        = explode("\n", $rawData);
				$straperVersion['installed'] = array(
					'version' => trim($info[0]),
					'date' => new JDate(trim($info[1]))
				);
			}
			else
			{
				$straperVersion['installed'] = array(
					'version' => '0.0',
					'date' => new JDate('2011-01-01')
				);
			}
			$rawData                   = file_get_contents($source . '/version.txt');
			$info                      = explode("\n", $rawData);
			$straperVersion['package'] = array(
				'version' => trim($info[0]),
				'date' => new JDate(trim($info[1]))
			);
			$versionSource             = 'installed';
		}

		if (!($straperVersion[$versionSource]['date'] instanceof JDate))
		{
			$straperVersion[$versionSource]['date'] = new JDate();
		}

		return array(
			'required' => $haveToInstallStraper,
			'installed' => $installedStraper,
			'version' => $straperVersion[$versionSource]['version'],
			'date' => $straperVersion[$versionSource]['date']->format('Y-m-d')
		);
	}

	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function install($parent)
	{
		// $parent is the class calling this method
	}

	/**
	 * method to uninstall the component
	 *
	 * @return void
	 */
	function uninstall($parent)
	{
		// Uninstall subextensions
		$status = $this->_uninstallSubextensions($parent);

		// $parent is the class calling this method
	}

	/**
	 * Uninstalls subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param JInstaller $parent
	 * @return JObject The subextension uninstallation status
	 */
	private function _uninstallSubextensions($parent)
	{
		jimport('joomla.installer.installer');

		$db = JFactory::getDBO();

		$status          = new JObject();
		$status->modules = array();
		$status->plugins = array();

		$src = $parent->getParent()->getPath('source');

		// Modules uninstallation
		if (count($this->uninstall_queue['modules']))
		{
			foreach ($this->uninstall_queue['modules'] as $folder => $modules)
			{
				if (count($modules))
					foreach ($modules as $module => $modulePreferences)
					{
						// Find the module ID
						$sql = $db->getQuery(true)->select($db->qn('extension_id'))->from($db->qn('#__extensions'))->where($db->qn('element') . ' = ' . $db->q($module))->where($db->qn('type') . ' = ' . $db->q('module'));
						$db->setQuery($sql);
						$id = $db->loadResult();
						// Uninstall the module
						if ($id)
						{
							$installer         = new JInstaller;
							$result            = $installer->uninstall('module', $id, 1);
							$status->modules[] = array(
								'name' => 'mod_' . $module,
								'client' => $folder,
								'result' => $result
							);
						}
					}
			}
		}

		// Plugins uninstallation
		if (count($this->uninstall_queue['plugins']))
		{
			foreach ($this->uninstall_queue['plugins'] as $folder => $plugins)
			{
				if (count($plugins))
					foreach ($plugins as $plugin => $published)
					{
						$sql = $db->getQuery(true)->select($db->qn('extension_id'))->from($db->qn('#__extensions'))->where($db->qn('type') . ' = ' . $db->q('plugin'))->where($db->qn('element') . ' = ' . $db->q($plugin))->where($db->qn('folder') . ' = ' . $db->q($folder));
						$db->setQuery($sql);

						$id = $db->loadResult();
						if ($id)
						{
							$installer         = new JInstaller;
							$result            = $installer->uninstall('plugin', $id);
							$status->plugins[] = array(
								'name' => 'plg_' . $plugin,
								'group' => $folder,
								'result' => $result
							);
						}
					}
			}
		}

		return $status;
	}

	/**
	 * Method to update the component
	 *
	 * @param   String  $parent  String
	 *
	 * @return void
	 */
	public function update($parent)
	{
		$db     = JFactory::getDBO();
		$config = JFactory::getConfig();

		if (JVERSION >= 3.0)
		{
			$configdb = $config->get('db');
		}
		else
		{
			$configdb = $config->getValue('config.db');
		}

		// Get dbprefix
		if (JVERSION >= 3.0)
		{
			$dbprefix = $config->get('dbprefix');
		}
		else
		{
			$dbprefix = $config->getValue('config.dbprefix');
		}
	}
}
