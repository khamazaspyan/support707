<?php
error_reporting(E_ALL ^ E_WARNING);
define('_JEXEC', 1);
define('JPATH_BASE', dirname(dirname(__FILE__)));
define('DS', DIRECTORY_SEPARATOR);
require_once ( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );
require_once ( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );

require_once ( JPATH_BASE . DS . 'administrator' . DS . 'components' . DS . 'com_easysocial' . DS . 'includes' . DS . 'easysocial.php');

$mainframe = & JFactory::getApplication('site');
$mainframe->initialise();
$db = JFactory::getDbo();


$params = '{"photo":{"albums":true},"videos":true,"audios":true,"news":true,"feeds":true,"events":true,"files":true,"polls":true,"discussions":true}';
$time = date("Y-m-d,h:m:s");
$oneYearOn = date("Y-m-d,h:m:s",strtotime(date("Y-m-d,h:m:s", mktime()) . " + 365 day"));


function rcopy($src, $dest){

    // If source is not a directory stop processing
    if(!is_dir($src)) return false;

    // If the destination directory does not exist create it
    if(!is_dir($dest)) {
        if(!mkdir($dest)) {
            // If the destination directory could not be created stop processing
            return false;
        }
    }

    // Open the source directory to read in files
    $i = new DirectoryIterator($src);
    foreach($i as $f) {
        if($f->isFile()) {
            copy($f->getRealPath(), "$dest/" . $f->getFilename());
        } else if(!$f->isDot() && $f->isDir()) {
            rcopy($f->getRealPath(), "$dest/$f");
        }
    }
}

function createPermalink($title)
{
    $result = strtolower($title);
    // strip all non word chars
    $result = preg_replace('/\W/', ' ', $result);
    // replace all white space sections with a dash
    $result = preg_replace('/\ +/', '-', $result);
    // trim dashes
    $result = preg_replace('/\-$/', '', $result);
    $result = preg_replace('/^\-/', '', $result);

    return $result;
}

$query = "SELECT p.user_id FROM #__social_profiles_maps as p
          INNER JOIN #__social_users as u ON p.user_id = u.user_id
          WHERE u.state=1 AND p.profile_id=4";

$users_list = $db->setQuery($query)->loadObjectList();

$users = array();
foreach($users_list as $user){
    $check_page_query = "SELECT id FROM #__social_auto_page WHERE user_id={$user->user_id} AND done=1";
    $check_page = $db->setQuery($check_page_query)->loadResult();
    if(!$check_page){
        array_push($users,$user);
    }
}

foreach($users as $user){
    $temp_user = ES::user($user->user_id);
    $user->avatar = $db->setQuery("SELECT * FROM #__social_avatars WHERE uid={$user->user_id} AND type='user'")->loadObject();
    $user->avatar_photo = $db->setQuery("SELECT a.* FROM #__social_photos as a WHERE a.id = {$user->avatar->photo_id}")->loadObject();
    $user->avatar_photo_meta = $db->setQuery("SELECT a.* FROM #__social_photos_meta as a WHERE a.photo_id = {$user->avatar->photo_id}")->loadObjectList();

    $user->cover = $db->setQuery("SELECT * FROM #__social_covers WHERE uid = {$user->user_id} AND type='user'")->loadObject();
    $user->cover_photo = $db->setQuery("SELECT a.* FROM #__social_photos as a WHERE a.id = {$user->cover->photo_id}")->loadObject();
    $user->cover_photo_meta = $db->setQuery("SELECT a.* FROM #__social_photos_meta as a WHERE a.photo_id = {$user->cover->photo_id}")->loadObjectList();
    $user->page_id = 17;
    $fields_query = "SELECT d.field_id,d.raw,n.title FROM #__social_fields_data as d
                     INNER JOIN #__social_fields as n ON d.field_id = n.id
                     WHERE d.uid = {$user->user_id}";
    $fields = $db->setQuery($fields_query)->loadObjectList();
    foreach($fields as $field){
        if($field->field_id == 294){
            if($field->raw == 'Yes'){
                $user->page = 1;
                $user->fields = $fields;
            }
            else{
                $user->page = 0;
                $update_auto_page_query = "INSERT INTO #__social_auto_page (user_id,page_id,done) VALUES ({$user->user_id},0,1)";
                $db->setQuery($update_auto_page_query)->query();
            }
        }

        if($field->field_id == 196){
            if($field->raw == 'Yes'){
                $user->page_id = 18;
            }
        }
        if($field->field_id == 299){
            $user->page_title = addslashes($field->raw);
        }


        if($field->field_id == 389){
            $user->paypal_email = $field->raw;
        }
        if($field->field_id == 254){
            $user->photo = $field->raw;
        }
        if($field->field_id == 195){
            $user->story = addslashes($field->raw);
        }
    }
}
$root = $_SERVER['DOCUMENT_ROOT'];


foreach($users as $user){

    if($user->page == 1):

    $insert_page_query = "INSERT INTO `#__social_clusters`
(`category_id`, `cluster_type`, `creator_type`, `creator_uid`, `title`, `description`, `alias`, `state`, `featured`, `created`, `params`, `hits`, `type`, `notification`, `key`, `parent_id`) VALUES 
('".$user->page_id."','page','user','".$user->user_id."','".$user->page_title."','','".createPermalink($user->page_title)."','3','0','".$time."','".$params."','0','1','1','','0')";

    $db->setQuery($insert_page_query)->query();
    $insertedPageId = $db->insertid();

    $insert_avatar_q = "INSERT INTO `#__social_avatars`(`uid`, `type`, `avatar_id`, `photo_id`, `small`, `medium`, `square`, `large`, `modified`, `storage`) 
VALUES ('".$insertedPageId."','page','0','".$user->avatar->photo_id."','".$user->avatar->small."','".$user->avatar->medium."','".$user->avatar->square."','".$user->avatar->large."','".$user->avatar->modified."','".$user->avatar->storage."')";

    $db->setQuery($insert_avatar_q)->query();

        rcopy($root . "/media/com_easysocial/avatars/users/".$user->user_id."/".$user->small, $root . "/media/com_easysocial/avatars/page/".$insertedPageId."/".$user->small);
        rcopy($root . "/media/com_easysocial/avatars/users/".$user->user_id."/".$user->medium, $root . "/media/com_easysocial/avatars/page/".$insertedPageId."/".$user->medium);
        rcopy($root . "/media/com_easysocial/avatars/users/".$user->user_id."/".$user->square, $root . "/media/com_easysocial/avatars/page/".$insertedPageId."/".$user->square);
        rcopy($root . "/media/com_easysocial/avatars/users/".$user->user_id."/".$user->large, $root . "/media/com_easysocial/avatars/page/".$insertedPageId."/".$user->large);

    $insert_cover_q = "INSERT INTO `#__social_covers`(`uid`, `type`, `photo_id`, `cover_id`, `x`, `y`, `modified`) 
VALUES ('".$insertedPageId."','page','".$user->cover->photo_id."','0','".$user->cover->x."','".$user->cover->y."','".$user->cover->modified."')";

    $db->setQuery($insert_cover_q)->query();



    $insert_page_node_query = "INSERT INTO `#__social_clusters_nodes`
(`cluster_id`, `uid`, `type`, `created`, `state`, `owner`, `admin`, `invited_by`, `reminder_sent`) VALUES 
('".$insertedPageId."','".$user->user_id."','user','".$time."','1','1','1','0','0')";
    $db->setQuery($insert_page_node_query)->query();


        if($user->paypal_email){
            $insert_jgive_quert ="INSERT INTO `#__jg_campaigns`
(`category_id`, `org_ind_type`, `creator_id`, `title`, `created`, `modified`, `type`, `max_donors`, `minimum_amount`, `short_description`, `long_description`, `goal_amount`,`start_date`, `end_date`, `allow_exceed`, `allow_view_donations`, `published`, `internal_use`, `featured`, `js_groupid`, `success_status`, `processed_flag`, `video_on_details_page`, `meta_data`, `meta_desc`,`paypal_email`) VALUES 

('11','individuals','".$user->user_id."','Support to ".$user->page_title."','".$time."','0000-00-00 00:00:00','donation','0','0','".$user->story."','','0','".$time."','".$oneYearOn."','1','1','1','','0','0','0','','0','','','".$user->paypal_email."')";
        $db->setQuery($insert_jgive_quert)->query();
            $insertedCampaignId = $db->insertid();
            $insert_support_family_query = "INSERT INTO #__social_support_family(`page_id`, `campaign_id`) VALUES ('".$insertedPageId."','".$insertedCampaignId."')";
            $db->setQuery($insert_support_family_query)->query();
        }

    $insert_story_query = "INSERT INTO `#__social_stream`
(`actor_id`, `alias`, `actor_type`, `post_as`, `created`, `modified`, `edited`, `title`, `content`, `context_type`, `verb`, `stream_type`, `sitewide`, `target_id`, `location_id`, `mood_id`, `with`, `ispublic`, `cluster_id`, `cluster_type`, `cluster_access`, `params`, `state`, `privacy_id`, `access`, `custom_access`, `field_access`, `last_action`, `last_userid`, `last_action_date`, `sticky_id`, `anywhere_id`, `datafix`) VALUES 
('".$user->user_id."','','user','page','".$time."','".$time."','0000-00-00 00:00:00','','".$user->story."','story','create','','0','0','0','0','','0','".$insertedPageId."','page','1','','1','41','0','','0','','0','0000-00-00 00:00:00','0','','1')";
    $db->setQuery($insert_story_query)->query();

    $pageUserFields = $db->setQuery("SELECT p_field_id,u_field_id FROM #__social_page_user_fields")->loadObjectList();
    foreach($pageUserFields as $pageUserField){
        $page_field_data = $db->setQuery("SELECT data,raw FROM #__social_fields_data WHERE field_id='".$pageUserField->u_field_id."' AND uid='".$user->user_id."' AND type='user'")->loadObject();
        $pageUserField->raw = $page_field_data->raw;
        $pageUserField->data = $page_field_data->data;
    }

    foreach($pageUserFields as $pageUserField){
        $insert_field_data_query = "INSERT INTO `#__social_fields_data`
(`field_id`, `uid`, `type`, `datakey`, `data`, `params`, `raw`) VALUES 
('".$pageUserField->p_field_id."','".$insertedPageId."','page','','".addslashes($pageUserField->data)."','','".addslashes($pageUserField->raw)."')";
        $db->setQuery($insert_field_data_query)->query();
    }

    $complate_user_page_query = "INSERT INTO `mkpo4_social_auto_page`
(`user_id`, `page_id`, `done`) VALUES 
('".$user->user_id."','".$insertedPageId."','1')";
    $db->setQuery($complate_user_page_query)->query();

    endif;
}



