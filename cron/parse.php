<?php
error_reporting(E_ALL ^ E_WARNING);
define('_JEXEC', 1);
define('JPATH_BASE', dirname(dirname(__FILE__)));
define('DS', DIRECTORY_SEPARATOR);
require_once ( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );
require_once ( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );

require_once ( JPATH_BASE . DS . 'administrator' . DS . 'components' . DS . 'com_easysocial' . DS . 'includes' . DS . 'easysocial.php');

$mainframe = & JFactory::getApplication('site');
$mainframe->initialise();
$db = JFactory::getDbo();


$time = date("Y-m-d,h:m:s");


function createPermalink($title)
{
    $result = strtolower($title);
    // strip all non word chars
    $result = preg_replace('/\W/', ' ', $result);
    // replace all white space sections with a dash
    $result = preg_replace('/\ +/', '-', $result);
    // trim dashes
    $result = preg_replace('/\-$/', '', $result);
    $result = preg_replace('/^\-/', '', $result);

    return $result;
}

$submissions = $db->setQuery("SELECT a.* FROM #__rsform_submissions as a
                              WHERE a.FormId = '4'")->loadObjectList();
foreach ($submissions as $submission){
    $submission->form = $db->setQuery("SELECT * FROM #__rsform_submission_values WHERE SubmissionId = {$submission->SubmissionId}")->loadObjectList();
}
echo "<pre>";
print_r($submissions);
echo "<pre>";