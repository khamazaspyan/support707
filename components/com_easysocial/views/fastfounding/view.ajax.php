<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');



class JgiveFrontendHelper
{
    /**
     * Constructor
     *
     * @since   1.0
     */
    public function __construct()
    {
        $TjGeoHelper = JPATH_ROOT . '/components/com_tjfields/helpers/geo.php';

        if (!class_exists('TjGeoHelper'))
        {
            JLoader::register('TjGeoHelper', $TjGeoHelper);
            JLoader::load('TjGeoHelper');
        }

        $this->TjGeoHelper = new TjGeoHelper;
    }

    /**
     * Methods to get Menu item id.
     *
     * @param   String  $link          Menu link
     * @param   Int     $skipIfNoMenu  Flag
     *
     * @return  Int  menuid
     *
     * @since       1.7
     */
    public function getItemId($link, $skipIfNoMenu = 0)
    {
        $itemid    = 0;
        $mainframe = JFactory::getApplication();

        if ($mainframe->issite())
        {
            $JSite = new JSite;
            $menu  = $JSite->getMenu();
            $items = $menu->getItems('link', $link);

            if (isset($items[0]))
            {
                $itemid = $items[0]->id;
            }
        }

        if (!$itemid)
        {
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $query = "SELECT id FROM #__menu
			WHERE link LIKE '%" . $link . "%'
			AND published =1
			LIMIT 1";

            $db->setQuery($query);
            $itemid = $db->loadResult();
        }

        if (!$itemid)
        {
            if ($skipIfNoMenu)
            {
                $itemid = 0;
            }
            else
            {
                $itemid = JRequest::getInt('Itemid', 0);
            }
        }

        return $itemid;
    }

    /**
     * Methods to get countries
     *
     * @return  countries
     *
     * @since       1.7
     */
    public function getCountries()
    {
        $rows = $this->TjGeoHelper->getCountryList('com_jgive');

        return $rows;
    }

    /**
     * Loads states for given country
     *
     * @param   INT  $country_id  Country Id
     *
     * @return  countries
     *
     * @since       1.7
     */
    public function getState($country_id)
    {
        if (!$country_id)
        {
            return;
        }

        $rows = $this->TjGeoHelper->getRegionList($country_id, 'com_jgive');

        return $rows;
    }

    /**
     * Loads cities for given cities
     *
     * @param   INT  $country_id  Country Id
     *
     * @return  countries
     *
     * @since       1.7
     */
    public function getCity($country_id)
    {
        if (!$country_id)
        {
            return;
        }

        $rows = $this->TjGeoHelper->getCityList($country_id, 'com_jgive');

        return $rows;
    }

    /**
     * Loads country name from country id, used when saving campaign
     *
     * @param   INT  $country_id  Country Id
     *
     * @return  countries
     *
     * @since       1.7
     */
    public function getCountryNameFromId($country_id)
    {
        if (!$country_id)
        {
            return;
        }

        $db    = JFactory::getDBO();
        $query = $db->getQuery(true)
            ->select($db->qn('country'))
            ->from($db->qn('#__tj_country'))
            ->where($db->qn('id') . ' = ' . $db->quote($country_id) . ' AND ' . $db->qn('com_jgive') . ' = ' . '1');

        $db->setQuery($query);

        return $db->loadResult();
    }

    /**
     * Loads country id from country name, used when showing campaign details
     *
     * @param   INT  $country_name  Country name
     *
     * @return  countries
     *
     * @since       1.7
     */
    public function getCountryIdFromName($country_name)
    {
        if (!$country_name)
        {
            return;
        }

        $db = JFactory::getDBO();
        $query = $db->getQuery(true)
            ->select($db->qn('id'))
            ->from($db->qn('#__tj_country'))
            ->where($db->qn('country') . ' = ' . $db->quote($country_name));

        $db->setQuery($query);

        return $db->loadResult();
    }

    /**
     * Loads region name from region id, country id, used when saving campaign
     *
     * @param   INT  $region_id   Region Id
     * @param   INT  $country_id  Country Id
     *
     * @return  countries
     *
     * @since       1.7
     */
    public function getRegionNameFromId($region_id, $country_id)
    {
        if (!$region_id)
        {
            return;
        }

        if (!$country_id)
        {
            return;
        }

        $db    = JFactory::getDBO();
        $query = $db->getQuery(true)
            ->select($db->qn('r.region'))
            ->from($db->qn('#__tj_region', 'r'))
            ->join('LEFT', $db->qn('#__tj_country', 'c') . ' ON (' . $db->qn('r.country_id') . ' = ' . $db->qn('c.id') . ')')
            ->where($db->qn('c.id') . ' = ' . $db->quote($country_id) . ' AND ' . $db->qn('r.id') . ' = ' . $db->quote($region_id));

        $db->setQuery($query);

        return $db->loadResult();
    }

    /**
     * Loads city name from city id, country id, used when saving campaign
     *
     * @param   INT  $city_id     City Id
     * @param   INT  $country_id  Country Id
     *
     * @return  countries
     *
     * @since       1.7
     */
    public function getCityNameFromId($city_id, $country_id)
    {
        if (!$city_id)
        {
            return;
        }

        if (!$country_id)
        {
            return;
        }

        $db    = JFactory::getDBO();

        $query = $db->getQuery(true)
            ->select($db->qn('c.city'))
            ->from($db->qn('#__tj_city', 'c'))
            ->join('LEFT', $db->qn('#__tj_country', 'con') . 'ON (' . $db->qn('c.country_id') . ' = ' . $db->qn('con.id') . ')')
            ->where($db->qn('con.id') . ' = ' . $db->quote($country_id) . ' AND' . $db->qn('c.id') . ' = ' . $db->quote($city_id));

        $db->setQuery($query);

        return $db->loadResult();
    }

    /**
     * To sort the column which are not in table
     *
     * @param   array   $array   Array to sort
     * @param   string  $column  Column to sory by
     * @param   string  $order   Asc or desc
     *
     * @return  array  Sorted array
     *
     * @since       1.7
     */
    public function multi_d_sort($array, $column, $order)
    {
        if (isset($array) && count($array))
        {
            foreach ($array as $key => $row)
            {
                $orderby[$key] = $row->$column;
            }

            if ($order == 'asc')
            {
                array_multisort($orderby, SORT_ASC, $array);
            }
            else
            {
                array_multisort($orderby, SORT_DESC, $array);
            }
        }

        return $array;
    }

    /**
     * To sort the column which are not in table
     *
     * @param   string  $ad_url             Campaign link
     * @param   int     $id                 Campaign id
     * @param   string  $title              Campaign title
     * @param   int     $show_comments      Flag to show or not to show commets
     * @param   int     $show_like_buttons  Flag to show or not to show like buttons
     *
     * @return  array  Sorted array
     *
     * @since       1.7
     */
    public function DisplayjlikeButton($ad_url, $id, $title, $show_comments, $show_like_buttons)
    {
        $jlikeparams               = array();
        $jlikeparams['url']        = $ad_url;
        $jlikeparams['campaignid'] = $id;
        $jlikeparams['title']      = $title;
        $dispatcher                = JDispatcher::getInstance();
        JPluginHelper::importPlugin('content', 'jlike_jgive');

        $grt_response = $dispatcher->trigger('onBeforeDisplaylike', array('com_jgive.compaign', $jlikeparams, $show_comments, $show_like_buttons));

        if (!empty($grt_response['0']))
        {
            return $grt_response['0'];
        }
        else
        {
            return '';
        }
    }

    /**
     * Push to activity stream
     *
     * @param   array  $contentdata  Data required for stream
     *
     * @return  array  Sorted array
     *
     * @since       1.7
     */
    public function pushtoactivitystream($contentdata)
    {
        $a_id           = $contentdata['user_id'];
        $i_opt = $contentdata['integration_option'];
        $a_acc         = 0;
        $a_des    = $contentdata['act_description'];
        $a_type           = '';
        $act_subtype        = '';
        $act_link           = '';
        $act_title          = '';
        $act_access         = 0;

        $activityintegrationstream = new activityintegrationstream;

        $result                    = $activityintegrationstream->pushActivity($a_id, $a_type, $act_subtype, $a_des, $act_link, $act_title, $a_acc, $i_opt);

        if (!$result)
        {
            return false;
        }

        return true;
    }

    /**
     * Push to activity stream
     *
     * @param   float   $price  Amount
     * @param   string  $curr   Currency
     *
     * @return formatted price-currency string
     *
     * @since       1.7
     */
    public function getFormattedPrice($price, $curr = null)
    {
        $price                      = @number_format($price, 2, '.', ',');

        $curr_sym                   = $this->getCurrencySymbol();
        $params                     = JComponentHelper::getParams('com_jgive');
        $currency                   = $params->get('currency');
        $currency_display_format    = $params->get('currency_display_format');
        $currency_display_formatstr = '';
        $currency_display_formatstr = str_replace('{AMOUNT}', "&nbsp;" . $price, $currency_display_format);
        $currency_display_formatstr = str_replace('{CURRENCY_SYMBOL}', "&nbsp;" . $curr_sym, $currency_display_formatstr);
        $currency_display_formatstr = str_replace('{CURRENCY}', "&nbsp;" . $currency, $currency_display_formatstr);
        $html                       = '';
        $html                       = "<span>" . $currency_display_formatstr . " </span>";

        return $html;
    }

    /**
     * Get currency symbol
     *
     * @param   string  $currency  Currency
     *
     * @return  currency symbol
     *
     * @since       1.7
     */
    public function getCurrencySymbol($currency = '')
    {
        $params   = JComponentHelper::getParams('com_jgive');
        $curr_sym = $params->get('currency_symbol');

        if (empty($curr_sym))
        {
            $curr_sym = $params->get('currency');
        }

        return $curr_sym;
    }

    /**
     * Get jomsocial toobar html
     *
     * @return  Js Toolbar
     *
     * @since       1.7
     */
    public function jomsocailToolbarHtml()
    {
        $params = JComponentHelper::getParams('com_jgive');
        $html   = '';

        if (($params->get('integration') == 'jomsocial') && $params->get('jomsocial_toolbar'))
        {
            // Added for JS toolbar inclusion.
            if (JFolder::exists(JPATH_SITE . '/components/com_community'))
            {
                require_once JPATH_ROOT . '/components/com_community/libraries/toolbar.php';
                $toolbar = CFactory::getToolbar();
                $tool    = CToolbarLibrary::getInstance();

                $html .= '<div id="community-wrap">';
                $html .= $tool->getHTML();
                $html .= '</div>';
            }
        }

        return $html;
    }

    /** Checks for view override
     *
     * @param   String  $viewname       Name of view
     * @param   String  $layout         Layout name eg order
     * @param   String  $searchTmpPath  It may be admin or site. it is side(admin/site) where to search override view
     * @param   String  $useViewpath    It may be admin or site. it is side(admin/site) which VIEW shuld be use IF OVERRIDE IS NOT FOUND
     *
     * @return Return path
     *
     * @since  1.7
     */
    public function getViewpath($viewname, $layout = "", $searchTmpPath = 'SITE', $useViewpath = 'SITE')
    {
        $searchTmpPath = ($searchTmpPath == 'SITE') ? JPATH_SITE : JPATH_ADMINISTRATOR;
        $useViewpath   = ($useViewpath == 'SITE') ? JPATH_SITE : JPATH_ADMINISTRATOR;
        $app           = JFactory::getApplication();

        if (!empty($layout))
        {
            $layoutname = $layout . '.php';
        }
        else
        {
            $layoutname = "default.php";
        }

        $override = $searchTmpPath . '/templates/' . $app->getTemplate() . '/html/com_jgive/' . $viewname . '/' . $layoutname;

        if (JFile::exists($override))
        {
            return $view = $override;
        }
        else
        {
            return $view = $useViewpath . '/components/com_jgive/views/' . $viewname . '/tmpl/' . $layoutname;
        }
    }

    /**
     * Declare language constants to use in .js file
     *
     * @params  void
     *
     * @return  void
     *
     * @since   1.7
     */
    public static function getLanguageConstant()
    {
        JText::script('COM_JGIVE_MINIMUM_DONATION_AMOUNT');
        JText::script('COM_JGIVE_ENTER_DONATION_AMOUNT');
        JText::script('COM_JGIVE_AMOUNT_SHOULD_BE');
        JText::script('COM_JGIVE_FILL_MANDATORY_FIELDS_DATA');
        JText::script('COM_JGIVE_ORDER_PLACING_ERROR');
        JText::script('COM_JGIVE_PLEASE_SELECT_DEFAULT_VIDEO');
        JText::script('COM_JGIVE_DELETE_VIDEO_CONFIRM_MSG');
        JText::script('COM_JGIVE_EMAIL_VALIDATION');
        JText::script('COM_JGIVE_VIDEO_DELETED');
        JText::script('COM_JGIVE_VIDEO_SET_DEFAULT');
        JText::script('COM_JGIVE_POST_TEXT_ACTIVITY_REMAINING_TEXT_LIMIT');

        JText::script('COM_JGIVE_FIRST_NAME_VALIDATION');
        JText::script('COM_JGIVE_LAST_NAME_VALIDATION');
        JText::script('COM_JGIVE_EMAIL_VALIDATION');
        JText::script('COM_JGIVE_ZIP_VALIDATION');
        JText::script('COM_JGIVE_PHONE_NUMBER_VALIDATION');
        JText::script('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_PROCESSING');
        JText::script('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_DONE');
        JText::script('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_ERROR');
        JText::script('COM_JGIVE_GALLERY_IMAGE_TEXT');
        JText::script('COM_JGIVE_GALLERY_VIDEO_TEXT');
        JText::script('COM_JGIVE_CAMPAIGN_MAIN_IMAGE_TYPE_VALIDATION');
        JText::script('COM_JGIVE_CAMPAIGN_MAIN_IMAGE_DIMES_INFO');
    }

    /**
     * Pass js file which are needed to load on selected view.
     *
     * @param   array  &$jsFilesArray                  Js file's array.
     * @param   array  &$firstThingsScriptDeclaration  Javascript to be declared first.
     *
     * @return  filled up file array
     */
    public function getJGiveJsFiles(&$jsFilesArray, &$firstThingsScriptDeclaration)
    {
        $input  = JFactory::getApplication()->input;
        $option = $input->get('option', '');
        $view   = $input->get('view', '');
        $layout = $input->get('layout', '');
        $params = JComponentHelper::getParams('com_jgive');

        if ($option === 'com_jgive')
        {
            // For frontend
            if (JFactory::getApplication()->isSite())
            {
                switch ($layout)
                {
                    case 'all':
                        $jsFilesArray[] = 'media/com_jgive/vendors/js/masonry.pkgd.min.js';
                        break;

                    case 'single':
                        $jsFilesArray[] = 'media/com_jgive/vendors/js/jquery.magnific-popup.min.js';
                        break;
                }
            }
        }

        return $jsFilesArray;
    }

    /**
     * getLineChartFormattedData
     *
     * @param   ARRAY  $data  data
     *
     * @return  Chart array
     */
    public function getLineChartFormattedData($data)
    {
        $app        = JFactory::getApplication();
        $backdate   = $app->getUserStateFromRequest('from', 'from', '', 'string');
        $todate     = $app->getUserStateFromRequest('to', 'to', '', 'string');
        $backdate   = !empty($backdate) ? $backdate : (date('Y-m-d', strtotime(date('Y-m-d') . ' - 30 days')));
        $todate     = !empty($todate) ? $todate : date('Y-m-d');

        $donationData = "[";
        $ordersData = "[";
        $firstdate  = $backdate;

        // Will be
        $keydate    = "";

        foreach ($data as $key => $donation)
        {
            $keydate = date('Y-m-d', strtotime($key));

            if ($firstdate < $keydate)
            {
                while ($firstdate < $keydate)
                {
                    $donationData .= " { period:'" . $firstdate . "', amount:0 },
					";
                    $ordersData .= " { period:'" . $firstdate . "', orders:0 },
					";
                    $firstdate = $this->add_date($firstdate, 1);
                }
            }

            $donationData .= " { period:'" . $donation->cdate . "', amount:" . $donation->donation_amount . "},
		";
            $ordersData .= " { period:'" . $donation->cdate . "', orders:" . $donation->orders_count . "},
		";
            $firstdate = $keydate;
        }

        // Vm: remaing date to last date
        while ($keydate < $todate)
        {
            $keydate = $this->add_date($keydate, 1);
            $donationData .= " { period:'" . $keydate . "', amount:0 },
		";
            $ordersData .= " { period:'" . $keydate . "', orders:0 },
		";
        }

        $donationData .= '
		]';
        $ordersData .= '
		]';
        $returnArray    = array();
        $returnArray[0] = $donationData;
        $returnArray[1] = $ordersData;

        return $returnArray;
    }

    /**
     * add_date
     *
     * @param   ARRAY  $givendate  givendate
     * @param   INT    $day        day
     * @param   INT    $mth        month
     * @param   INT    $yr         year
     *
     * @return html
     */
    public function add_date($givendate, $day = 0, $mth = 0, $yr = 0)
    {
        $cd      = strtotime($givendate);

        $newdate = date('Y-m-d H:i:s',
            mktime(
                date('H', $cd),
                date('i', $cd), date('s', $cd), date('m', $cd) + $mth, date('d', $cd) + $day, date('Y', $cd) + $yr
            )
        );

        // Convert to y-m-d format
        $newdate = date('Y-m-d H:i:s', strtotime($newdate));

        return $newdate;
    }

    /**
     * Get sites/administrator default template
     *
     * @param   mixed  $client  0 for site and 1 for admin template
     *
     * @return  json
     *
     * @since   1.5
     */
    public static function getSiteDefaultTemplate($client = 0)
    {
        try
        {
            $db    = JFactory::getDBO();

            // Get current status for Unset previous template from being default
            // For front end => client_id=0
            $query = $db->getQuery(true)
                ->select('template')
                ->from($db->quoteName('#__template_styles'))
                ->where('client_id=' . $client)
                ->where('home=1');
            $db->setQuery($query);

            return $db->loadResult();
        }
        catch (Exception $e)
        {
            return '';
        }
    }
}

class EasySocialViewFastfounding extends EasySocialSiteView
{
	/**
	 * Post processing after filtering events
	 *
	 * @since   2.0
	 * @access  public
	 */

    public function getCampaignGivebacks($cid)
    {
        $db    = JFactory::getDBO();

        $query = $db->getQuery(true);

        $query->select('*');
        $query->from($db->qn('#__jg_campaigns_givebacks', 'cg'));
        $query->where($db->qn('cg.campaign_id') . ' = ' . $db->quote($cid));

        $db->setQuery($query);
        $givebacks = $db->loadObjectList();

        $giveback_tooltip = JText::_('COM_JGIVE_BY_GIVEBACK');

        foreach ($givebacks as $giveback)
        {
            $sold_giveback = 0;
            $query = $db->getQuery(true);

            $query->select('COUNT(d.giveback_id) as sold_giveback');
            $query->from($db->qn('#__jg_donations', 'd'));

            $query->join('LEFT', $db->qn('#__jg_campaigns', 'c') . 'ON (' . $db->qn('c.id') . ' = ' . $db->qn('d.campaign_id') . ')');
            $query->join('LEFT', $db->qn('#__jg_orders', 'o') . 'ON (' . $db->qn('o.donation_id') . ' = ' . $db->qn('d.id') . ')');
            $query->where($db->qn('d.campaign_id') . ' = ' . $db->quote($giveback->campaign_id));
            $query->where($db->qn('d.giveback_id') . ' = ' . $db->quote($giveback->id));
            $query->where($db->qn('o.status') . ' = ' . $db->quote('C'));

            $db->setQuery($query);

            $giveback->sold_giveback = $sold_giveback = $db->loadResult();

            $give_back_flag = 0;

            // Sold out flag @TODO correct this flag instead of sold it should be sold_out
            $giveback->sold = 0;

            if ($sold_giveback == $giveback->total_quantity || $sold_giveback > $giveback->total_quantity)
            {
                // Set sold out flag
                $giveback->sold = 1;
            }
        }

        return $givebacks;
    }

	public function filter($filter, $events, $pagination, $activeCategory, $featuredEvents, $browseView, $activeUserId)
	{
		// Default properties
		$showDateNavigation = false;
		$showPastFilter = true;
		$showSorting = true;
		$showDistanceSorting = false;
		$showDistance = false;
		$distance = 10;
		$includePast = $this->input->get('includePast', 0, 'int');
		$ordering = $this->input->get('ordering', 'start', 'word');
		$delayed = false;
		$user = ES::user($activeUserId);

		$title = 'COM_EASYSOCIAL_PAGE_TITLE_EVENTS';

		// Set the route options so that filter can add extra parameters
		$routeOptions = array('option' => SOCIAL_COMPONENT_NAME, 'view' => 'events');

		if ($filter != 'category') {
			$routeOptions['filter'] = $filter;
		}

		// We want to set a different title for non "all" or "category" filter
		if ($filter != 'all' && $filter != 'category' && $filter != 'date') {
			$title = 'COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_' . strtoupper($filter);
		}

		if ($filter == 'week1') {
			$title = 'COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_UPCOMING_1WEEK';
		}

		if ($filter == 'week2') {
			$title = 'COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_UPCOMING_2WEEK';
		}

		// Date navigation
		$activeDateFilter = '';
		$activeDate = false;
		$navigation = new stdClass();

		// Filtering by date
		if ($filter == 'today' || $filter == 'tomorrow' || $filter == 'month' || $filter == 'year' || $filter == 'date') {

			$showSorting = false;
			$showPastFilter = false;
			$showDateNavigation = true;

			// If the filter is made from sidebar, we need to build the correct date string
			$dateString = '';

			if ($filter == 'today') {
				$dateString = ES::date()->format('Y-m-d');
			}

			if ($filter == 'tomorrow') {
				$dateString = ES::date('+1 day')->format('Y-m-d');
			}

			if ($filter == 'month') {
				$dateString = ES::date()->format('Y-m');
			}

			if ($filter == 'year') {
				$dateString = ES::date()->format('Y');
			}

			// Default to today
			$activeDateFilter = 'today';

			if (!$dateString) {
				$dateString = $this->input->get('date', '', 'string');
			}

			// The only way to determine if the user is filtering by today, tomorrow, month or year is to break up the "-"
			$parts = explode('-', $dateString);
			$totalParts = count($parts);

			// Try to see if it is tomorrow.
			if ($totalParts == 3) {
				$activeDate = ES::date($dateString, false);

				$tomorrow = ES::date('+1 day')->format('Y-m-d');
				$today = ES::date()->format('Y-m-d');

				if ($today == $dateString) {
					$activeDateFilter = 'today';
					$title = 'COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_TODAY';
				} else if ($tomorrow == $dateString) {
					$activeDateFilter = 'tomorrow';
					$title = 'COM_EASYSOCIAL_PAGE_TITLE_EVENTS_FILTER_TOMORROW';
				} else {
					$activeDateFilter = 'normal';
					$title = $activeDate->format(JText::_('COM_EASYSOCIAL_DATE_DMY'));
				}

				$previous = ES::date($dateString, false)->modify('-1 day');
				$next = ES::date($dateString, false)->modify('+1 day');

				// Set the navigation dates
				$navigation->previous = $previous->format('Y-m-d');
				$navigation->next = $next->format('Y-m-d');
			}

			if ($totalParts == 2) {
				$activeDate = ES::date($dateString . '-01', false);
				$activeDateFilter = 'month';

				// due to the timezone issue, for safety purposely, we will use the mid date of the month to get the next / previous months. #5553
				$previous = ES::date($dateString .'-15')->modify('-1 month');
				$next = ES::date($dateString .'-15')->modify('+1 month');

				// Set the navigation dates
				$navigation->previous = $previous->format('Y-m');
				$navigation->next = $next->format('Y-m');
			}

			if ($totalParts == 1) {
				$activeDate = ES::date($dateString . '-01-01', false);
				$activeDateFilter = 'year';

				$previous = ES::date($dateString . '-01-01')->modify('-1 year');
				$next = ES::date($dateString . '-01-01')->modify('+1 year');

				// Set the navigation dates
				$navigation->previous = $previous->format('Y');
				$navigation->next = $next->format('Y');
			}
		}

		// Get the active category alias
		if ($activeCategory && $activeCategory->id) {
			$routeOptions['categoryid'] = $activeCategory->getAlias();
		}

		// Determines if the sorting should be visible
		$disallowedSorting = array('date', 'today', 'tomorrow', 'month', 'year');

		if (in_array($filter, $disallowedSorting)) {
			$showSorting = false;
		}

		// Determines if the past filter should be visible
		$disallowedPastFilters = array('today', 'tomorrow', 'month', 'year', 'past', 'ongoing', 'upcoming', 'week1', 'week2');

		if (in_array($filter, $disallowedPastFilters)) {
			$showPastFilter = false;
		}

		// Filter by near by events
		if ($filter === 'nearby') {
			$showSorting = false;
			$showDistance = true;
			$showDistanceSorting = true;

			$distance = $this->input->get('distance', 10, 'string');

			if (!empty($distance) && $distance != 10) {
				$routeOptions['distance'] = $distance;
			}

			$title = JText::sprintf('COM_EASYSOCIAL_EVENTS_IN_DISTANCE_RADIUS', $distance, $this->config->get('general.location.proximity.unit'));
		}

		$sortingUrls = array();

		// We use start as key because order is always start by default, and it is the page default link
		$sortingUrls['start'] = array('nopast' => ESR::events($routeOptions, false));

		if (!$delayed) {

			// Only need to create the "order by created" link.
			if ($showSorting) {
				$sortingUrls['created'] = array('nopast' => ESR::events(array_merge($routeOptions, array('ordering' => 'created')), false));
			}

			// Only need to create the "order by distance" link.
			if ($showDistanceSorting) {
				$sortingUrls['distance'] = array('nopast' => ESR::events(array_merge($routeOptions, array('ordering' => 'distance')), false));
			}

			// If past filter is displayed on the page, then we need to generate the past links counter part
			if ($showPastFilter) {
				$sortingUrls['start']['past'] = ESR::events(array_merge($routeOptions, array('includePast' => 1)), false);

				// Only need to create the "order by created" link.
				if ($showSorting) {
					$sortingUrls['created']['past'] = ESR::events(array_merge($routeOptions, array('ordering' => 'created', 'includePast' => 1)), false);
				}

				// Only need to create the "order by distance" link.
				if ($showDistanceSorting) {
					$sortingUrls['distance']['past'] = ESR::events(array_merge($routeOptions, array('ordering' => 'distance', 'includePast' => 1)), false);
				}
			}
		}

		// For nearby filter, we want to get the "distance"
		$distanceUrlWithPast = '';
		$distanceUrlWithoutPast = '';

		if ($filter == 'nearby') {
			$distanceUrlWithoutPast = $sortingUrls['distance']['nopast'];
			$distanceUrlWithPast = $sortingUrls['distance']['past'];
		}

		$emptyText = 'COM_EASYSOCIAL_EVENTS_EMPTY_' . strtoupper($filter);

		// If this is viewing profile's event, we display a different empty text
		if (!$browseView) {
			$emptyText = 'COM_ES_EVENTS_EMPTY_' . strtoupper($filter);

			if (!$user->isViewer()) {
				$emptyText = 'COM_ES_EVENTS_USER_EMPTY_' . strtoupper($filter);
			}
		}

		$theme = ES::themes();
		$theme->set('showDistance', $showDistance);
		$theme->set('showDistanceSorting', $showDistanceSorting);
		$theme->set('showPastFilter', $showPastFilter);
		$theme->set('showDateNavigation', $showDateNavigation);
		$theme->set('showSorting', $showSorting);
		$theme->set('delayed', $delayed);
		$theme->set('includePast', $includePast);
		$theme->set('sortingUrls', $sortingUrls);
		$theme->set('ordering', $ordering);
		$theme->set('routeOptions', $routeOptions);
		$theme->set('browseView', $browseView);
		
		// Since ajax requests to filter only occurds when sidebar is enabled, we should enable by default
		$theme->set('showSidebar', true);

		// Date navigation
		$theme->set('activeDateFilter', $activeDateFilter);
		$theme->set('activeDate', $activeDate);
		$theme->set('navigation', $navigation);

		// Distance options
		$theme->set('distance', $distance);
		$theme->set('distanceUnit', $this->config->get('general.location.proximity.unit'));

		// Content attributes
		$theme->set('title', $title);

		$theme->set('filter', $filter);
		$theme->set('featuredEvents', $featuredEvents);
		$theme->set('events', $events);
		$theme->set('pagination', $pagination);
		$theme->set('activeCategory', $activeCategory);
		$theme->set('emptyText', $emptyText);

		$namespace = 'wrapper';

		$sort = $this->input->get('sort', false, 'bool');

		if ($sort) {
			$namespace = 'items';
		}

		$output = $theme->output('site/events/default/' . $namespace);

		return $this->ajax->resolve($output, $distanceUrlWithPast, $distanceUrlWithoutPast);
	}

	/**
	 * Displays confirmation to remove a user from an event
	 *
	 * @since   2.0
	 * @access  public
	 */

    public function form()
    {
        ES::requireLogin();

        $element = $this->input->get('element', '', 'cmd');
        $title = $this->input->get('title', '', '');
        $amount_raised = $this->input->get('amount_raised', '', '');
        $amount_goal = $this->input->get('amount_goal', '', '');
        $precent = $this->input->get('precent', '', '');
        $donors = $this->input->get('donors', '', '');
        $days = $this->input->get('days', '', '');
        $group = $this->input->get('group', '', 'cmd');
        $uid = $this->input->get('id', 0, 'int');
        $clusterId = $this->input->get('clusterId', 0, 'int');
        $clusterType = $this->input->get('clusterType', '', 'word');


        $showPostAsButton = false;

        $this->display($uid);

        $repost = ES::repost($uid, $element, $group);

        if ($clusterId && $clusterType) {
            $repost->setCluster($clusterId, $clusterType);
            $cluster = $repost->getCluster();

            // Only Page admin can repost as Page
            if ($cluster->getType() == SOCIAL_TYPE_PAGE && $cluster->isAdmin()) {
                $showPostAsButton = true;
            }
        }

        $theme = ES::themes();

        // Check if the current user already shared this item or not. if yes, display a message and abort the sharing process.
        if ($repost->isShared($this->my->id)) {
            $html = $theme->output('site/fastfounding/dialogs/message');
            return $this->ajax->resolve($html);
        }

        $campaignGivebacks = $this->getCampaignGivebacks($uid);

        $r_page = ES::page($clusterId);
        $page_avatar = JUri::base() . 'media/com_easysocial/avatars/page/' . $r_page->id .'/'. $r_page->avatars['medium'];
        $page_link = JUri::base() . 'index.php/community/pages/' . $r_page->id . '-' . $r_page->alias;

        // Get dialog
        $preview = $repost->preview();
        $theme->set('preview', $preview);
        $theme->set('title', $title);
        $theme->set('amount_raised', $amount_raised);
        $theme->set('amount_goal', $amount_goal);
        $theme->set('campaignGivebacks', $campaignGivebacks);
        $theme->set('precent', $precent);
        $theme->set('donors', $donors);
        $theme->set('days', $days);
        $theme->set('campaign_id', $uid);
        $theme->set('page_id', $clusterId);
        $theme->set('page_link', $page_link);
        $theme->set('pageTitle',  $r_page->title);
        $theme->set('page_avatar',  $page_avatar);
        $theme->set('cluster',  $r_page);
        $theme->set('showPostAsButton', $showPostAsButton);
        $html = $theme->output('site/fastfounding/dialogs/form');

        return $this->ajax->resolve($html);
    }

    public function display($cid,$tpl = null)
    {
        global $mainframe, $option;
        $mainframe  = JFactory::getApplication();
        $option     = JFactory::getApplication()->input->get('option');
        $dispatcher = JDispatcher::getInstance();

        $session       = JFactory::getSession();
        $this->session = $session;

        // Get component params
        $this->params          = $params = JComponentHelper::getParams('com_jgive');
        $this->currency_code   = $params->get('currency');
        $this->default_country = $params->get('default_country');

        // Imp This is frontend
        $this->donations_site = 1;
        $this->retryPayment_show = 0;

        $this->retryPayment = new StdClass;
        $this->retryPayment->status = '';
        $this->retryPayment->msg = '';

        // Default layout is payment
        $this->layout = $layout = JFactory::getApplication()->input->get('layout', 'payment');
        $this->setLayout($layout);

        // Get logged in user id
        $user                = JFactory::getUser();
        $this->logged_userid = $user->id;

        $donationid = JFactory::getApplication()->input->get('donationid');

        $input      = JFactory::getApplication()->input;
        $guestemail = $input->get('email');
        $this->cid = $cid;
        $jgiveFrontendHelper        = new jgiveFrontendHelper;
        $this->jomsocailToolbarHtml = $jgiveFrontendHelper->jomsocailToolbarHtml();

        if (!(($layout == 'details') AND $donationid AND $guestemail))
        {
            $session->clear('JGIVE_order_id');

            if (!$this->logged_userid)
            {
                $this->guest_donation = $params->get('guest_donation');

                if ($this->guest_donation)
                {
                    $msg         = JText::_('COM_JGIVE_LOGIN_MSG_SILENT');
                    $uri         = JFactory::getApplication()->input->get('REQUEST_URI', '', 'server', 'string');
                    $url         = base64_encode($uri);
                    $guest_login = $session->get('quick_reg_no_login');
                    $session->clear('quick_reg_no_login');
                }
                else
                {
                    $itemid = $input->get('Itemid');
                    $msg = JText::_('COM_JGIVE_LOGIN_MSG');
                    $uri    = 'index.php?option=com_jgive&view=donations&layout=payment&cid=' . $cid . '&Itemid=' . $itemid;
                    $url    = base64_encode($uri);
                    $mainframe->redirect(JRoute::_('index.php?option=com_users&return=' . $url, false), $msg);
                }
            }
        }

        $path = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

        if (!class_exists('donationsHelper'))
        {
            JLoader::register('donationsHelper', $path);
            JLoader::load('donationsHelper');
        }

        $donationsHelper = new donationsHelper;
        $this->pstatus   = $donationsHelper->getPStatusArray();

        // Used on donations view for payment status filter
        $this->sstatus   = $donationsHelper->getSStatusArray();

        if ($layout == 'payment')
        {
            // Get country list options
            // Use helper function
            $countries       = $jgiveFrontendHelper->getCountries();
            $this->countries = $countries;

            // Get campaign id
            $cid             = $this->get('CampaignId');
            $this->cid       = $cid;

            $cdata = $this->get('Campaign');
            $this->assignRef('cdata', $cdata);

            // Joomla profile import
            $nofirst = '';

            // If no user data set in session then only import the user profile or only first time after login
            $nofirst = $session->get('No_first_donation');
/*
            if (empty($nofirst))
            {
                if ($this->logged_userid)
                {
                    // If recurring donor then get donor Infomation
                    $profiledata = $this->get('RecurringDonorInfo');

                    if (!$profiledata)
                    {
                        $profile_import = $params->get('profile_import');

                        // If profie import is on the call profile import function
                        if ($profile_import)
                        {
                            $JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
                            $profiledata             = $JgiveIntegrationsHelper->profileImport(1);
                        }
                    }

                    $this->_setDonorData($profiledata);
                }
            }
*/
            JPluginHelper::importPlugin('payment');

            $this->gateways = $params->get('gateways');

            $gateways = array();

            if (!empty($this->gateways))
            {
                $gateways = $dispatcher->trigger('onTP_GetInfo', array((array) $this->gateways));
            }

            $this->assignRef('gateways', $gateways);

            // Recurring payment gateway
            $this->recurringGateways = array();

            foreach ($gateways as $gateway)
            {
                // Add recurring supported payment gateways html
                if ($gateway->id == "paypal" || $gateway->id == "stripe")
                {
                    $this->recurringGateways[] = $gateway;
                }
            }

            // Get campaign givebacks
            $this->giveback_id = $session->get('JGIVE_giveback_id');
            $JGIVE_cid         = $session->get('JGIVE_cid');

            $campaignHelper          = new campaignHelper;
            $this->campaignGivebacks = $campaignHelper->getCampaignGivebacks($JGIVE_cid);
        }

        if ($layout == 'confirm')
        {
            // @TODO save all posted data somewhere
            $session = JFactory::getSession();
            $this->assignRef('session', $session);
            $cid = $this->get('CampaignId');
            $this->assignRef('cid', $cid);
            $cdata = $this->get('Campaign');
            $this->assignRef('cdata', $cdata);

            JPluginHelper::importPlugin('payment');

            $this->gateways = $params->get('gateways');

            $gateways = array();

            if (!empty($this->gateways))
            {
                $gateways = $dispatcher->trigger('onTP_GetInfo', array((array) $this->gateways));
            }

            $this->assignRef('gateways', $gateways);
        }

        if ($layout == 'details')
        {
            $donation_id      = JFactory::getApplication()->input->get('donationid');
            $donation_details = $this->get('SingleDonationInfo');

            $this->retryPayment = $this->get('AllowedRetryPayment');

            // TO DO check the email id against orderid except below
            $this->logged_userid;
            $donation_details['donor']->user_id;
            $this->guest_donation = $params->get('guest_donation');

            if ($this->guest_donation)
            {
                if (!$this->logged_userid)
                {
                    $input       = JFactory::getApplication()->input;
                    $guest_email = $input->get('email');
                    $donar_email = md5($donation_details['donor']->email);

                    if ($guest_email != $donar_email)
                    {
                        $itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
                        $link   = JRoute::_('index.php?option=com_jgive&view=donations&layout=my&Itemid=' . $itemid, false);
                        $msg    = JText::_('COM_JGIVE_NO_ACCESS_MSG');
                        $mainframe->enqueueMessage($msg, 'notice');
                        $mainframe->redirect($link, '');
                    }
                }
                elseif ($this->logged_userid != $donation_details['donor']->user_id)
                {
                    $itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
                    $link   = JRoute::_('index.php?option=com_jgive&view=donations&layout=my&Itemid=' . $itemid, false);
                    $msg    = JText::_('COM_JGIVE_NO_ACCESS_MSG');
                    $mainframe->enqueueMessage($msg, 'notice');
                    $mainframe->redirect($link, '');
                }
            }
            elseif ($this->logged_userid != $donation_details['donor']->user_id)
            {
                $itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
                $link   = JRoute::_('index.php?option=com_jgive&view=donations&layout=my&Itemid=' . $itemid, false);
                $msg    = JText::_('COM_JGIVE_NO_ACCESS_MSG');
                $mainframe->enqueueMessage($msg, 'notice');
                $mainframe->redirect($link, '');
            }

            // PAYMENT
            $dispatcher = JDispatcher::getInstance();
            JPluginHelper::importPlugin('payment');

            $params = JComponentHelper::getParams('com_jgive');

            if (!is_array($params->get('gateways')))
            {
                $gateway_param[] = $params->get('gateways');
            }
            else
            {
                $gateway_param = $params->get('gateways');
            }

            if (!empty($gateway_param))
            {
                $gateways = $dispatcher->trigger('onTP_GetInfo', array($gateway_param));
            }

            $this->gateways = $gateways;

            // If recurring is 1 pass only paypal
            $recure_gateway[0]['name'] = 'paypal';
            $recure_gateway[0]['id'] = 'paypal';

            $this->recure_gateway = $recure_gateway;

            $this->assignRef('donation_details', $donation_details);
        }

        if ($layout == 'my' || $layout == 'all')
        {
            if (!$this->logged_userid AND $layout == 'my')
            {
                $msg = JText::_('COM_JGIVE_LOGIN_MSG');
                $uri = $_SERVER["REQUEST_URI"];
                $url = base64_encode($uri);
                $mainframe->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url, false), $msg);
            }

            $donations = $this->get('Donations');
            $this->donations = $donations;

            // Get ordering filter
            $filter_order_Dir = $mainframe->getUserStateFromRequest('com_jgive.filter_order_Dir', 'filter_order_Dir', 'desc', 'word');
            $filter_type      = $mainframe->getUserStateFromRequest('com_jgive.filter_order', 'filter_order', 'id', 'string');

            // Payment status filter
            $payment_status = $mainframe->getUserStateFromRequest('com_jgive' . 'payment_status', 'payment_status', '', 'string');

            if ($payment_status == null)
            {
                $payment_status = '-1';
            }

            // Set filters
            $lists['payment_status'] = $payment_status;
            $lists['order_Dir']      = $filter_order_Dir;
            $lists['order']          = $filter_type;
            $this->lists             = $lists;

            $total       = $this->get('Total');
            $this->total = $total;

            $pagination       = $this->get('Pagination');
            $this->pagination = $pagination;

            $itemid       = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
            $this->Itemid = $itemid;
        }

        parent::display($tpl);
    }

    public function _setDonorData($profiledata)
    {
        if (!empty($profiledata))
        {
            $session = JFactory::getSession();

            foreach ($profiledata as $field => $value)
            {
                if (!empty($value))
                {
                    $session->set('JGIVE_' . $field, $value);
                }
            }

            if (!empty($profiledata['first_name']) && !empty($profiledata['last_name']))
            {
                $session->set('JGIVE_user_first_last_name', $profiledata['first_name'] . ' ' . $profiledata['last_name']);
            }

            $session->set('No_first_donation', 1);
        }
    }

	public function confirmRemoveGuest()
	{
		// Only logged in users are allowed here.
		ES::requireLogin();

		// Get the guest object.
		$id = $this->input->get('id', 0, 'int');
		$guest = ES::table('EventGuest');
		$guest->load($id);

		$event = ES::event($guest->cluster_id);

		if (!$event->canRemoveGuest($guest->uid, $this->my->id)) {
			return $this->exception();
		}

		$user = ES::user($guest->uid);

		$theme = ES::themes();
		$theme->set('guest', $guest);
		$theme->set('user', $user);
		$contents = $theme->output('site/events/dialogs/guest.remove');

		return $this->ajax->resolve($contents);
	}

	/**
	 * Displays the confirmation to approve user application
	 *
	 * @since   2.0
	 * @access  public
	 * @param   string
	 * @return
	 */
	public function confirmApproveGuest()
	{
		// Only logged in users are allowed here.
		ES::requireLogin();

		// Load the event
		$id = $this->input->get('id', 0, 'int');
		$event = ES::event($id);

		// Get the user id
		$userId = $this->input->get('userId', 0, 'int');
		$guest = ES::table('EventGuest');
		$guest->load($userId);

		$return = $this->input->get('return', '', 'default');

		$theme = ES::themes();
		$theme->set('return', $return);
		$theme->set('event', $event);
		$theme->set('guest', $guest);

		$contents = $theme->output('site/events/dialogs/approve');

		return $this->ajax->resolve($contents);
	}

	/**
	 * Displays the confirmation to reject user application
	 *
	 * @since   2.0
	 * @access  public
	 * @param   string
	 * @return
	 */
	public function confirmRejectGuest()
	{
		// Only logged in users are allowed here.
		ES::requireLogin();

		// Load the event
		$id = $this->input->get('id', 0, 'int');
		$event = ES::event($id);

		// Get the user id
		$userId = $this->input->get('userId', 0, 'int');
		$guest = ES::table('EventGuest');
		$guest->load($userId);

		$return = $this->input->get('return', '', 'default');

		$theme = ES::themes();
		$theme->set('return', $return);
		$theme->set('event', $event);
		$theme->set('guest', $guest);

		$contents = $theme->output('site/events/dialogs/reject');

		return $this->ajax->resolve($contents);
	}

	public function removeGuest()
	{
		//Remove Event guest user.
		return $this->ajax->resolve();
	}

	public function rsvpFailed($errorMessage)
	{
		$theme = ES::themes();
		$theme->set('errorMessage', $errorMessage);
		$contents = $theme->output('site/events/dialogs/rsvp.failed');

		return $this->ajax->reject($contents);
	}

	/**
	 * Post processing after a user rsvp to an event
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function rsvp(SocialEvent $event, SocialTableEventGuest $guest)
	{
		$button = ES::themes()->html('event.action', $event);

		return $this->ajax->resolve($button, $guest->getCurrentStateTitle());
	}

	public function guestResponse($state = null)
	{
		return $this->ajax->resolve($state);
	}

	public function getFilter($event = null, $filter = null)
	{
		$theme = ES::themes();

		$theme->set('controller', 'events');
		$theme->set('filter', $filter);
		$theme->set('uid', $event->id);

		$contents = $theme->output('site/stream/form.edit');

		return $this->ajax->resolve($contents);
	}

	/**
	 * Post processing after getting app contents
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function getAppContents(SocialEvent $event, $app)
	{
		// Load the library.
		$lib = ES::getInstance('Apps');
		$contents = $lib->renderView(SOCIAL_APPS_VIEW_TYPE_EMBED, 'events', $app, array('eventId' => $event->id));

		// Return the contents
		return $this->ajax->resolve($contents);
	}

	public function initInfo($steps = null)
	{
		return $this->ajax->resolve($steps);
	}

	/**
	 * Retrieves the event info
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function getInfo(SocialEvent $event, $fields)
	{
		// Go through each of the steps and only pick the active one
		$contents = '';

		$theme = ES::themes();
		$theme->set('fields', $fields);

		$contents = $theme->output('site/events/item/about');

		return $this->ajax->resolve($contents);
	}

	/**
	 * Retrieves stream contents
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function getStream(SocialEvent $event, SocialStream $stream, $streamFilter)
	{
		 // RSS
		if ($this->config->get('stream.rss.enabled')) {
			$this->addRss(ESR::events(array('id' => $event->getAlias(), 'layout' => 'item'), false));
		}

		$theme = ES::themes();
		$theme->set('rssLink', $this->rssLink);
		$theme->set('stream', $stream);
		$theme->set('event', $event);
		$theme->set('streamFilter', $streamFilter);

		$contents = $theme->output('site/events/item/feeds');

		return $this->ajax->resolve($contents);
	}

	/**
	 * Responsible to show the invite friends dialog.
	 *
	 * @since  1.3
	 * @access public
	 */
	public function invite()
	{
		ES::requireLogin();

		// Get the event
		$id = $this->input->get('id', '0', 'int');
		$event = ES::event($id);

		if (!$event || !$event->id) {
			return $this->view->exception('COM_EASYSOCIAL_EVENTS_INVALID_EVENT_ID');
		}

		if (!$event->isPublished()) {
			return $this->view->exception('COM_EASYSOCIAL_EVENTS_EVENT_UNAVAILABLE');
		}

		if (!$event->canViewItem()) {
			return $this->view->exception(JText::_('COM_EASYSOCIAL_EVENTS_NO_ACCESS_TO_EVENT'));
		}

		$model = ES::model('Events');
		$friends = $model->getFriendsInEvent($event->id, array('userId' => $this->my->id, 'published' => true, 'invited' => true));

		$exclusion = array();

		foreach ($friends as $friend) {
			$exclusion[] = $friend->id;
		}

		$returnUrl = $this->input->get('return', '', 'default');

		$theme = FD::themes();
		$theme->set('exclusion', $exclusion);
		$theme->set('event', $event);
		$theme->set('returnUrl', $returnUrl);
		$contents = $theme->output('site/events/dialogs/invite');

		return $this->ajax->resolve($contents);
	}

	/**
	 * Callback after inviting friends.
	 *
	 * @since  1.3
	 * @access public
	 */
	public function inviteFriends()
	{
		return $this->ajax->resolve(JText::_('COM_EASYSOCIAL_EVENTS_SUCCESSFULLY_INVITED_FRIENDS'));
	}

	/**
	 * Displays confirmation to feature events
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function confirmUnfeature()
	{
		ES::requireLogin();

		// Get the event
		$id = $this->input->getInt('id', 0);
		$event = ES::event($id);

		if (!$event || !$event->id) {
			return $this->exception('COM_EASYSOCIAL_EVENTS_INVALID_EVENT_ID');
		}

		// Ensure that the user can really unpublish the event
		if (!$event->canFeature($this->my->id)) {
			return $this->exception('COM_EASYSOCIAL_EVENTS_NO_ACCESS_TO_EVENT');
		}

		$returnUrl = $this->input->get('return', '', 'default');

		$theme = ES::themes();
		$theme->set('event', $event);
		$theme->set('returnUrl', $returnUrl);

		$output = $theme->output('site/events/dialogs/unfeature');

		return $this->ajax->resolve($output);
	}

	/**
	 * Displays confirmation to feature events
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function confirmFeature()
	{
		ES::requireLogin();

		// Get the event
		$id = $this->input->getInt('id', 0);
		$event = ES::event($id);

		if (!$event || !$event->id) {
			return $this->exception('COM_EASYSOCIAL_EVENTS_INVALID_EVENT_ID');
		}

		// Ensure that the user can really unpublish the event
		if (!$event->canFeature($this->my->id)) {
			return $this->exception('COM_EASYSOCIAL_EVENTS_NO_ACCESS_TO_EVENT');
		}

		$returnUrl = $this->input->get('return', '', 'default');

		$theme = ES::themes();
		$theme->set('event', $event);
		$theme->set('returnUrl', $returnUrl);

		$output = $theme->output('site/events/dialogs/feature');

		return $this->ajax->resolve($output);
	}

	/**
	 * Displays confirmation to unpublish an event
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function confirmUnpublish()
	{
		ES::requireLogin();

		// Get the event
		$id = $this->input->getInt('id', 0);
		$event = ES::event($id);

		if (!$event || !$event->id) {
			return $this->exception('COM_EASYSOCIAL_EVENTS_INVALID_EVENT_ID');
		}

		// Ensure that the user can really unpublish the event
		if (!$event->canUnpublish($this->my->id)) {
			return $this->exception('COM_EASYSOCIAL_EVENTS_NO_ACCESS_TO_EVENT');
		}

		$theme = ES::themes();
		$theme->set('event', $event);
		$output = $theme->output('site/events/dialogs/unpublish');

		return $this->ajax->resolve($output);
	}

	/**
	 * Displays the delete event dialog
	 *
	 * @since   2.0
	 * @access  public
	 */
	public function confirmDelete()
	{
		ES::requireLogin();

		$id = $this->input->getInt('id', 0);
		$event = ES::event($id);

		if (!$event || !$event->id) {
			return $this->exception('COM_EASYSOCIAL_EVENTS_INVALID_EVENT_ID');
		}

		// Ensure that the user can really unpublish the event
		if (!$event->canDelete($this->my->id)) {
			return $this->exception('COM_EASYSOCIAL_EVENTS_NO_ACCESS_TO_EVENT');
		}

		$theme = FD::themes();
		$theme->set('event', $event);

		$namespace = 'site/events/dialogs/delete';

		// Recurring support
		if ($event->isRecurringEvent() || $event->hasRecurringEvents()) {
			$namespace = 'site/events/dialogs/delete.recurring';
		}

		$contents = $theme->output($namespace);

		return $this->ajax->resolve($contents);
	}

	public function deleteFilter($eventId)
	{
		ES::requireLogin();
		$this->info->set($this->getMessage());

		$event = ES::event($eventId);
		$url = $event->getPermalink(false, false, 'item');

		return $this->ajax->redirect($url);
	}

	public function update($event)
	{
		if (empty($event)) {
			return $this->ajax->reject($this->getMessage());
		}

		return $this->ajax->resolve();
	}

	public function edit($errors)
	{
		if (!empty($errors)) {
			return $this->ajax->reject($this->getMessage(), $errors);
		}

		return $this->ajax->resolve();
	}

	public function createRecurring()
	{
		return $this->ajax->resolve();
	}

	public function deleteRecurringDialog()
	{
		FD::requireLogin();

		// Might be calling this from backend
		FD::language()->loadSite();

		$id = $this->input->getInt('id', 0);

		$event = FD::event($id);

		if (empty($event) || empty($event->id)) {
			return $this->ajax->reject(JText::_('COM_EASYSOCIAL_EVENTS_INVALID_EVENT_ID'));
		}

		$guest = $event->getGuest($this->my->id);

		if (!$guest->isOwner() && !$this->my->isSiteAdmin()) {
			return $this->ajax->reject(JText::_('COM_EASYSOCIAL_EVENTS_NO_ACCESS_TO_EVENT'));
		}

		$theme = FD::themes();
		$theme->set('event', $event);
		$contents = $theme->output('site/events/dialogs/recurringevent.delete');

		return $this->ajax->resolve($contents);
	}

	public function deleteRecurring()
	{
		return $this->ajax->resolve();
	}

	/**
	 * Renders the calendar via ajax
	 *
	 * @since   1.3
	 * @access  public
	 */
	public function renderCalendar()
	{
		$unix = $this->input->getString('date', ES::date()->toUnix());

		$day = date('d', $unix);
		$month = date('m', $unix);
		$year = date('Y', $unix);

		// Create a calendar object
		$calendar = new stdClass();

		$calendar->year = $year;
		$calendar->month = $month;

		// Configurable start of week
		$startOfWeek = $this->config->get('events.startofweek');

		// Here we generate the first day of the month
		$calendar->first_day = mktime(0, 0, 0, $month, 1, $year);

		// This gets us the month name
		$calendar->title = date('F', $calendar->first_day);

		// Sets the calendar header
		$date = ES::date($unix, false);
		$calendar->header = $date->format(JText::_('COM_EASYSOCIAL_DATE_MY'));

		// Here we find out what day of the week the first day of the month falls on
		$calendar->day_of_week = date('D', $calendar->first_day) ;

		// Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero
		$dayOfWeek = 0;

		switch ($calendar->day_of_week) {
			case "Sun":
				$dayOfWeek = 0;
				break;
			case "Mon":
				$dayOfWeek = 1;
				break;
			case "Tue":
				$dayOfWeek = 2;
				break;
			case "Wed":
				$dayOfWeek = 3;
				break;
			case "Thu":
				$dayOfWeek = 4;
				break;
			case "Fri":
				$dayOfWeek = 5;
				break;
			case "Sat":
				$dayOfWeek = 6;
				break;
		}

		// Day of week is dependent on the start of the week
		if ($dayOfWeek < $startOfWeek) {
			$calendar->blank = 7 - $startOfWeek + $dayOfWeek;
		} else {
			$calendar->blank = $dayOfWeek - $startOfWeek;
		}

		// Due to timezone issue, we will use the mid date of the month to get the next / previous months. #300
		$midMonth = ES::date($date->format('Y-m') . '-15');

		// Previous month
		$calendar->previous = strtotime('-1 month', $midMonth->toUnix());

		// Next month
		$calendar->next = strtotime('+1 month', $midMonth->toUnix());

		// Determine how many days are there in the current month
		$calendar->days_in_month = date('t', $calendar->first_day);

		// Create a date range to retrieve all the events
		$start = $year . '-' . $month . '-' . '01 00:00:00';
		$end = $year . '-' . $month . '-' . $calendar->days_in_month . ' 23:59:59';

		$events = ES::model('Events')->getEvents(array(
			'state' => SOCIAL_STATE_PUBLISHED,
			'type' => $this->my->isSiteAdmin() ? 'all' : 'user',
			'ordering' => 'start',
			'start-after' => $start,
			'start-before' => $end
		));

		// This array groups the events by days
		$days = array();

		foreach($events as $event) {

			// Get the number of days this event being held
			$numberOfDays = $event->getNumberOfDays();
			$dayStart = $event->getEventStart()->format('j', true);

			if ($numberOfDays > 1) {
				for ($i=0; $i < $numberOfDays; $i++) { 
					$days[$dayStart + $i][] = $event;
				}
			} else {
				$days[$dayStart][] = $event;
			}
		}

		$lastMonth = $month - 1;
		$start = $year . '-' . $lastMonth . '-' . '01 00:00:00';

		// We check if there are still ongoing events from last month
		$lastMonthEvents = ES::model('Events')->getEvents(array(
			'state' => SOCIAL_STATE_PUBLISHED,
			'type' => $this->my->isSiteAdmin() ? 'all' : 'user',
			'ordering' => 'start',
			'start-after' => $start,
			'start-before' => $end
		));

		foreach($lastMonthEvents as $event) {

			// if the end date is not this month, skip
			if ($event->getEventEnd()->format('m', true) != $month) {
				continue;
			} 

			// if the start date is this month, skip
			if ($event->getEventStart()->format('m', true) == $month) {
				continue;
			} 

			$numberOfDays = $event->getEventEnd()->format('j');
			$dayStart = 1;

			if ($numberOfDays > 1) {
				for ($i=0; $i < $numberOfDays; $i++) { 
					$days[$dayStart + $i][] = $event;
				}
			} else {
				$days[$dayStart][] = $event;
			}
		}

		// Compute the start of week
		$weekdays = $this->getWeekdays();

		$theme = ES::themes();
		$theme->set('weekdays', $weekdays);
		$theme->set('calendar', $calendar);
		$theme->set('days', $days);
		$theme->set('events', $events);

		$today = ES::date()->format('Y-m-d', true);
		$tomorrow = ES::date()->modify('+1 day')->format('Y-m-d', true);

		$theme->set('today', $today);
		$theme->set('tomorrow', $tomorrow);

		$output = $theme->output('site/events/default/calendar');

		return $this->ajax->resolve($output);
	}

	public function getWeekdays()
	{
		$weekdays = array(JText::_('SUN'), JText::_('MON'), JText::_('TUE'), JText::_('WED'), JText::_('THU'), JText::_('FRI'), JText::_('SAT'));

		// Configurable option
		$startOfWeek = $this->config->get('events.startofweek');

		if ($startOfWeek > 0) {
			$spliced = array_splice($weekdays, $startOfWeek);
			$weekdays = array_merge($spliced, $weekdays);
		}

		return $weekdays;
	}

	/**
	 * Confirmation to remove an avatar
	 *
	 * @since   2.0
	 * @access  public
	 * @param   string
	 * @return
	 */
	public function confirmRemoveAvatar()
	{
		// Only registered users can do this
		ES::requireLogin();

		// Get the page id from request
		$id = $this->input->get('id', 0, 'int');

		$theme = ES::themes();
		$theme->set('clusterType', 'events');
		$theme->set('id', $id);
		$contents = $theme->output('site/clusters/dialogs/remove.avatar');

		return $this->ajax->resolve($contents);
	}

	/**
	 * Display confirmation to promote guest
	 *
	 * @since   2.0
	 * @access  public
	 * @param   string
	 * @return
	 */
	public function confirmPromoteGuest()
	{
		// Only logged in users are allowed here.
		ES::requireLogin();

		// Get the guest object.
		$uid = $this->input->get('uid', 0, 'int');
		$guest = ES::table('EventGuest');
		$guest->load($uid);

		// Get the user object.
		$user = ES::user($guest->uid);

		// Get the current user
		$my = ES::user();

		// Get the event
		$id = $this->input->get('id', 0, 'int');
		$event = ES::event($id);

		$return = $this->input->get('return', '', 'default');

		// Get the current user as a guest object in the same event
		$myGuest = ES::table('EventGuest');
		$myGuest->load(array('uid' => $my->id, 'type' => SOCIAL_TYPE_USER, 'cluster_id' => $guest->cluster_id));

		$theme = ES::themes();

		if ($my->isSiteAdmin() || $myGuest->isAdmin()) {
			$theme->set('user', $user);
			$theme->set('uid', $uid);
			$theme->set('event', $event);
			$theme->set('return', $return);

			$contents = $theme->output('site/events/dialogs/promote');

			return $this->ajax->resolve($contents);
		}

		return $this->ajax->resolve($theme->output('site/events/dialogs/error'));
	}

	/**
	 * Display confirmation to demote guest from admin role
	 *
	 * @since   2.0
	 * @access  public
	 * @param   string
	 * @return
	 */
	public function confirmDemoteGuest()
	{
		ES::requireLogin();

		// Get the guest object.
		$uid = $this->input->get('uid', 0, 'int');
		$guest = ES::table('EventGuest');
		$guest->load($uid);

		// Get the user object.
		$user = ES::user($guest->uid);

		// Get the current user
		$my = ES::user();

		// Get the event
		$eventId = $this->input->get('id', 0, 'int');
		$event = ES::event($eventId);

		// Get the current user as a guest object in the same event
		$myGuest = ES::table('EventGuest');
		$myGuest->load(array('uid' => $my->id, 'type' => SOCIAL_TYPE_USER, 'cluster_id' => $guest->cluster_id));

		$theme = ES::themes();

		if ($my->isSiteAdmin() || $myGuest->isAdmin()) {
			$theme->set('user', $user);
			$theme->set('uid', $uid);
			$theme->set('event', $event);

			$contents = $theme->output('site/events/dialogs/demote');

			return $this->ajax->resolve($contents);
		}

		return $this->ajax->resolve($theme->output('site/events/dialogs/error'));
	}

	/**
	 * Allows caller to take a picture
	 *
	 * @since   2.0
	 * @access  public
	 * @param   string
	 * @return
	 */
	public function saveCamPicture()
	{
		// Ensure that the user is a valid user
		ES::requireLogin();

		$image = JRequest::getVar('image', '', 'default');
		$image = imagecreatefrompng($image);

		ob_start();
		imagepng($image, null, 9);
		$contents = ob_get_contents();
		ob_end_clean();

		// Store this in a temporary location
		$file = md5(FD::date()->toSql()) . '.png';
		$tmp = JPATH_ROOT . '/tmp/' . $file;
		$uri = JURI::root() . 'tmp/' . $file;

		JFile::write($tmp, $contents);

		$result = new stdClass();
		$result->file = $file;
		$result->url = $uri;

		return $this->ajax->resolve($result);
	}

	/**
	 * Allows caller to take a picture
	 *
	 * @since   2.0
	 * @access  public
	 * @param   string
	 * @return
	 */
	public function takePicture()
	{
		// Ensure that the user is logged in
		ES::requireLogin();

		$theme = ES::themes();

		$uid = $this->input->get('uid', 0, 'int');

		$event = ES::event($uid);

		$theme->set('uid', $event->id);

		$output = $theme->output('site/avatar/dialogs/capture.picture');

		return $this->ajax->resolve($output);
	}

	/**
	 * Output for getting subcategories
	 *
	 * @since   2.1
	 * @access  public
	 */
	public function getSubcategories($subcategories, $groupId, $pageId, $backId)
	{
		$theme = ES::themes();
		$theme->set('backId', $backId);
		$html = '';

		$categoryRouteBaseOptions = array('controller' => 'events' , 'task' => 'selectCategory');

		if ($groupId) {
			$categoryRouteBaseOptions['group_id'] = $groupId;
		}

		if ($pageId) {
			$categoryRouteBaseOptions['page_id'] = $pageId;
		}

		foreach ($subcategories as $category) {
			$table = ES::table('ClusterCategory');
			$table->load($category->id);

			$theme->set('category', $table);
			$theme->set('categoryRouteBaseOptions', $categoryRouteBaseOptions);
			$html .= $theme->output('site/events/create/category.item');
		}
		
		return $this->ajax->resolve($html);
	}
}
