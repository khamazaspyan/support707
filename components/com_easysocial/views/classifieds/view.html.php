<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
ini_set('display_errors', 1);
error_reporting(1);
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

class EasySocialViewClassifieds extends EasySocialSiteView
{
	public function __construct($config = array())
	{
		parent::__construct($config);
		$this->_addPath('template', JPATH_COMPONENT.  '/themes/default/views/items');
		$par = JComponentHelper::getParams( 'com_djclassifieds' );
		$theme = $par->get('theme','default');
		if ($theme && $theme != 'default') {
			$this->_addPath('template', JPATH_COMPONENT.  '/themes/'.$theme.'/views/items');
		}
	}
	/**
	 * Responsible to output the dashboard layout for the current logged in user.
	 *
	 * @since	2.0
	 * @access	public
	 */
	public function display($tpl = null)
	{


		JHTML::_( 'behavior.modal' );		
		$document 	=  JFactory::getDocument();
		$par 	  	= JComponentHelper::getParams( 'com_djclassifieds' );
		$app	  	= JFactory::getApplication();		
		$user 	  	= JFactory::getUser();

		require JPATH_SITE . '/components/com_djclassifieds/models/items.php';
		$model = JModelLegacy::getInstance('items', 'DjclassifiedsModel');
		require JPATH_SITE . '/components/com_djclassifieds/models/item.php';
		$model2 = JModelLegacy::getInstance('item', 'DjclassifiedsModel');
		
      
		//$model 	  	= $this->getModel();
		$dispatcher	= JDispatcher::getInstance();
		$config  	= JFactory::getConfig();
		
		$cat_id	  = JRequest::getVar('cid', 0, '', 'int');
		$uid	  = JRequest::getVar('uid', 0, '', 'int');
		$se		  = JRequest::getVar('se', 0, '', 'int');
		$reset	  = JRequest::getVar('reset', 0, '', 'int');		
		$layout   = JRequest::getVar('layout','');		
		$type     = JRequest::getVar('type','');
		$order    = JRequest::getCmd('order', $par->get('items_ordering','date_e'));
		$ord_t    = JRequest::getCmd('ord_t', $par->get('items_ordering_dir','desc'));
		$theme 	  = $par->get('theme','default');

		if($par->get('404_cat_redirect','0')==1){
			if($app->input->get('cid','')=='' && $app->input->get('rid','')=='' && $app->input->get('se','')=='' && $app->input->get('uid','')=='' && $app->input->get('type','')==''){
				throw new Exception(JText::_('COM_DJCLASSIFIEDS_CATEGORY_NOT_AVAILABLE'), 404);
			}			
		}
		
		if($layout=='favourites'){
			if($user->id=='0'){
				$uri = JFactory::getURI();
				$login_url = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode($uri),false);
				$app->redirect($login_url,JText::_('COM_DJCLASSIFIEDS_PLEASE_LOGIN'));	
			}			
			JRequest::setVar('fav','1');						
		}

		if($reset){
			$items= $model->resetSearchFilters();	
		}
		
		$catlist = ''; 
		if($cat_id>0){
			$cats= DJClassifiedsCategory::getSubCatIemsCount($cat_id,1,$par->get('subcats_ordering', 'ord'),$par->get('subcats_hide_empty', 0));
			$catlist= $cat_id;			
			foreach($cats as $c){
				$catlist .= ','. $c->id;
			}
					
		}
		else
		{
		$cats= DJClassifiedsCategory::getCatAllItemsCount(1,$par->get('subcats_ordering', 'ord'),$par->get('subcats_hide_empty', 0));
		}
		
		$subcats = '';
		$cat_images='';
		foreach($cats as $c){
			if($c->parent_id==$cat_id){
				$subcats .= $c->id.',';	
			}
		}

		if($subcats){

			$subcats = substr($subcats, 0, -1);

			$cat_images = $model->getCatImages($subcats); 
		}
		
		
		$items= $model->getItems($catlist);

		$item= $model2->getItem();
		$countitems = $model->getCountItems($catlist);



		$this->set('cats',$cats);
		$this->set('items',$items);
		$this->set('item',$item);
		

		return parent::display('site/classifieds/default/default');
	}

	/**
	 * Retrieves events that should appear on the sidebar
	 *
	 * @since	2.0
	 * @access	public
	 */
	private function getEvents()
	{
		// Retrieve participated events
		$model = ES::model('Events');
		$options = array('guestuid' => $this->my->id, 'ongoing' => true, 'upcoming' => true, 'ordering' => 'start');

		$options['limit'] = $this->config->get('users.dashboard.eventslimit');

		// Only show published event
		$options['state'] = SOCIAL_CLUSTER_PUBLISHED;

		$events = $model->getEvents($options);

		return $events;
	}

	/**
	 * Retrieves pages that should be displayed on the sidebar
	 *
	 * @since	2.0
	 * @access	public
	 */
	private function getPages()
	{
		$model = ES::model('Pages');
		$limit = $this->config->get('users.dashboard.pageslimit');
		$pages = $model->getUserPages($this->my->id, 0, $limit);

		return $pages;
	}

	/**
	 * Retrieves groups that should be displayed on the sidebar
	 *
	 * @since	2.0
	 * @access	public
	 */
	private function getGroups()
	{
		$model = ES::model('Groups');
		$limit = $this->config->get('users.dashboard.groupslimit');
		$groups = $model->getUserGroups($this->my->id, 0, $limit);

		return $groups;
	}

	/**
	 * Displays the guest view for the dashboard
	 *
	 * @since	2.0
	 * @access	public
	 */
	public function guests()
	{
		// Add the rss links
		if ($this->config->get('stream.rss.enabled')) {
			$this->addRss(ESR::dashboard(array(), false));
		}

		// Default stream filter
		$filter = 'everyone';

		// Determine if the current request is for "tags"
		$hashtag = $this->input->get('tag', '', 'default');

		if (!empty($hashtag)) {
			$filter = 'hashtag';
		}

		// Get the layout to use.
		$stream = ES::stream();
		$stream->getPublicStream($this->config->get('stream.pagination.pagelimit', 10), 0, $hashtag);

		// Get default return url
		$return = ESR::getMenuLink($this->config->get('general.site.login'));
		$return = ES::getCallback($return);

		// If return value is empty, always redirect back to the dashboard
		if (!$return) {
			$return = ESR::dashboard(array(), false);
		}

		// In guests view, there shouldn't be an app id
		$appId = $this->input->get('appId', '', 'default');

		if ($appId) {
			return JError::raiseError(404, JText::_('COM_EASYSOCIAL_PAGE_IS_NOT_AVAILABLE'));
		}

		// Ensure that the return url is always encoded correctly.
		$return = base64_encode($return);

		// check if there are any modules assigned in dashboard sidebar position or not.
		$hasSidebarModules = $this->hasSideBarModules();

		$this->set('rssLink', $this->rssLink);
		$this->set('filter', $filter);
		$this->set('hashtag', $hashtag);
		$this->set('stream', $stream);
		$this->set('return', $return);
		$this->set('hasSidebarModules', $hasSidebarModules);

		echo parent::display('site/dashboard/guests/default');
	}

	/**
	 * private method to check if there is any modules to display to guest or not.
	 *
	 * @since	2.0
	 * @access	public
	 */
	private function hasSideBarModules()
	{
		$sidebarPositions = array('es-dashboard-sidebar-top', 'es-dashboard-sidebar-before-newsfeeds', 'es-dashboard-sidebar-after-newsfeeds', 'es-dashboard-sidebar-bottom');

		foreach ($sidebarPositions as $position) {
			$modules = JModuleHelper::getModules($position);

			$checkedModules = array();
			// check for mod_easysocial_profile_statistic modules. if exits, remove this module as this module is not meant for guest.
			if ($modules) {
				foreach ($modules as $module) {
					if ($module->module != 'mod_easysocial_profile_statistic') {
						$checkedModules[] = $module;
					}
				}
			}

			if ($checkedModules) {
				return true;
			}
		}

		return false;
	}
}
