<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

ES::import('site:/views/views');

class EasySocialViewLogin extends EasySocialSiteView
{
	/**
	 * Determines if the view should be visible on lockdown mode
	 *
	 * @since	2.0
	 * @access	public
	 */
	public function isLockDown()
	{
		return false;
	}

	/**
	 * Responsible to display the generic login form via ajax
	 *
	 * @since	1.0
	 * @access	public
	 */
	public function form($tpl = null)
	{
		// If user is already logged in, they should not see this page.
		if (!$this->my->guest) {
			$this->setMessage('COM_EASYSOCIAL_LOGIN_ALREADY_LOGGED_IN', SOCIAL_MSG_ERROR);
			return $this->ajax->reject($this->getMessage());
		}

		// Get any callback urls.
		$return = ES::getCallback();

		// If return value is empty, always redirect back to the dashboard
		if (!$return) {
			$return	= ESR::dashboard(array(), false);
		}

		// Determine if there's a login redirection
		$loginMenu = $this->config->get('general.site.login');

		if ($loginMenu != 'null') {
			$return = ESR::getMenuLink($loginMenu);
		}

		$return = base64_encode($return);

		$this->set('return', $return);
		$contents = parent::display('site/login/dialogs/login');

		return $this->ajax->resolve($contents);
	}
}
