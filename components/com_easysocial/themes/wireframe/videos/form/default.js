
EasySocial.require()
.script('site/videos/form')
.done(function($) {

	$('[data-videos-form]').implement(EasySocial.Controller.Videos.Form, {
		"type": "<?php echo $type; ?>",
		"uploadingText": "<?php echo JText::_('COM_ES_UPLOADING');?>",
		<?php if ($userTagItemList) { ?>
		"tagsExclusion": <?php echo FD::json()->encode($userTagItemList); ?>
		<?php } ?>
	});
	
});
