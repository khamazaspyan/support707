<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2016 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="es-social-signon">
	<div class="es-social-signon__hd">
		<h1><?php echo JText::_('COM_EASYSOCIAL_PROFILE_REMIND_PASSWORD_ENTER_VERIFICATION');?></h1>
		<p><?php echo JText::_('COM_EASYSOCIAL_PROFILE_REMIND_PASSWORD_ENTER_VERIFICATION_DESC');?></p>
	</div>

	<form class="es-social-signon__form" action="<?php echo JRoute::_('index.php');?>" method="post">
		<div class="es-social-signon__form-inner">
			
			<div class="o-form-group t-text--center">
				<label for="es-username"><?php echo JText::_($this->config->get('registrations.emailasusername') ? 'COM_EASYSOCIAL_PROFILE_REMIND_PASSWORD_EMAIL' : 'COM_EASYSOCIAL_PROFILE_REMIND_PASSWORD_EMAIL_OR_USERNAME'); ?></label>
				<input type="text" placeholder="<?php echo JText::_($this->config->get('registrations.emailasusername') ? 'COM_EASYSOCIAL_LOGIN_EMAIL_PLACEHOLDER' : 'COM_EASYSOCIAL_LOGIN_USERNAME_PLACEHOLDER', true);?>" 
					name="es-username" id="es-username" class="o-form-control" />
			</div>

			<div class="o-form-group t-text--center">
				<label for="es-code"><?php echo JText::_('COM_EASYSOCIAL_PROFILE_REMIND_PASSWORD_VERIFICATION'); ?></label>
				<input type="text" placeholder="<?php echo JText::_('COM_EASYSOCIAL_PROFILE_REMIND_PASSWORD_VERIFICATION_PLACEHOLDER', true);?>" name="es-code" id="es-code" class="o-form-control" />
			</div>

			<div>
				<button class="btn btn-es-primary btn-block"><?php echo JText::_('COM_EASYSOCIAL_RESET_PASSWORD_BUTTON');?></button>
			</div>
		</div>
		<input type="hidden" name="option" value="com_easysocial" />
		<input type="hidden" name="controller" value="account" />
		<input type="hidden" name="task" value="confirmResetPassword" />
		<?php echo $this->html('form.token'); ?>
	</form>
</div>
