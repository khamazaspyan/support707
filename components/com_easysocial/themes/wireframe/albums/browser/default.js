EasySocial.require()
.script("site/albums/browser")
.done(function($){

	$("[data-album-browser=<?php echo $uuid; ?>]")
		.addController("EasySocial.Controller.Albums.Browser", {
			"uid": "<?php echo $lib->uid;?>",
			"type": "<?php echo $lib->type; ?>"
		});
});
