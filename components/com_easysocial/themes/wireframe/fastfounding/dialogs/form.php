<?php
/**
 * @package		EasySocial
 * @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * EasySocial is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
$jgiveFrontendHelper = new jgiveFrontendHelper;
$user = JFactory::getUser();
$es_user = ES::user($user->id);
$input = JFactory::getApplication()->input;
$db = JFactory::getDbo();

$userGroupIds = $db->setQuery("SELECT id FROM #__social_clusters WHERE creator_uid='".$user->id."' AND cluster_type='page' AND state='1'")->loadObjectList();
if(!empty($userGroupIds)){
    foreach($userGroupIds as $user_page){
        $temp_page = ES::page($user_page->id);
        $user_page->name = $temp_page->title;
        $user_page->image = $temp_page->getAvatar();
    }
}
$session       = JFactory::getSession();
$this->session = $session;
$user = JFactory::getUser();
$this->params          = $params = JComponentHelper::getParams('com_jgive');
$show_selected_fields_on_donate = $this->params->get('show_selected_fields_on_donation');
$donationfield = array();
$show_field = 0;
$donation_anonym = 0;

if ($show_selected_fields_on_donate)
{
    $donationfield = $this->params->get('donationfield');

    if (isset($donationfield))
    {
        if (in_array("donation_anonym", $donationfield))
        {
            $donation_anonym = 1;
        }
    }
}
else
{
    $show_field = 1;
}
// Fetched data


$lang = JFactory::getLanguage();
$extension = 'com_jgive';
$base_dir = JPATH_SITE;
$language_tag = 'en-GB';
$reload = true;
$lang->load($extension, $base_dir, $language_tag, $reload);
$db = JFactory::getDBO();
$cdata_array = $db->setQuery("SELECT * FROM #__jg_campaigns WHERE id='".$campaign_id."'")->loadObject();
$cdata['campaign'] = $cdata_array;
$cdata['images'] = array();
$cdata['givebacks'] = array();


$dispatcher = JDispatcher::getInstance();
JPluginHelper::importPlugin('payment');

$params = JComponentHelper::getParams('com_jgive');

if (!is_array($params->get('gateways')))
{
    $gateway_param[] = $params->get('gateways');
}
else
{
    $gateway_param = $params->get('gateways');
}

if (!empty($gateway_param))
{
    $gateways = $dispatcher->trigger('onTP_GetInfo', array($gateway_param));
}

$this->gateways = $gateways;

?>
<dialog>
    <width>500</width>
    <height>230</height>
    <selectors type="json">
        {
        "{sendButton}": "[data-send-button]",
        "{cancelButton}": "[data-cancel-button]",
        "{repostContent}": "[data-repost-form-content]",
        "{textbox}": "[data-repost-textbox]",
        "{header}": "[data-repost-header]",
        "{form}": "[data-repost-form]",
        "{closeButton}": ".es-dialog-close-button"

        }
    </selectors>
    <bindings type="javascript">
        {
        "{repostContent} focus": function() {
        this.validateContent();
        },

        "validateContent" : function() {

        var content = $('[data-repost-form-content]').val();

        if (content == '<?php echo JText::_('COM_EASYSOCIAL_REPOST_FORM_DIALOG_MSG'); ?>') {
        $('[data-repost-form-content]').val('');
        }
        }
        }
    </bindings>
    <title>

       <?php echo $clusterId->name; ?><?php echo ($cdata['campaign']->type === 'donation') ? JText::_("COM_JGIVE_DONATE_TO"): JText::_("COM_JGIVE_INVEST_TO"); ?> <?php echo $cdata['campaign']->title; ?></title>
    <content>

        <div class="es-repost-form" data-repost-wrapper>
            <div class="tjBs3">
                <div id="jgive_content" style="    padding: 0 40px;" class="container-fluid">

                    <form action="" method="post" id="payment_quick" name="payment_quick" class="form-validate form-vertical" enctype="multipart/form-data">


                        <!-- Edit amount & other details fields -->
                        <div id="jgive_form_field">
                            <div class="row" >
                                <div id="jgive_disabled_fields" >
                                    <aside class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div style="display: table;
    width: 100%;
    height: auto;
        padding: 0 0 0 30px;">
                                            <div class="page_info" style="    float: left;
    width: 50%;">
                                                <a href="<?php echo $page_link; ?>">
                                                    <img src="<?php echo $page_avatar; ?>" />
                                                    <span><?php echo $pageTitle; ?></span>
                                                </a>
                                            </div>
                                            <div style="width: 50%;float: left;    padding-top: 7px;" class="amount_info">
                                                <div class="raised" style="font-size: 14px;">Raised: <span style="color: #5fc6c2;font-weight: bold;font-size: 16px;">$<?php echo $amount_raised; ?></span></div>
                                                <div class="goal" style="font-size: 14px;">Goal: <span style="color: #2b2b2b;font-weight: bold;font-size: 16px;">$<?php echo $amount_goal; ?></span></div>
                                            </div>
                                            </div>
                                            <div class="clr"></div>
                                            <div style="display: table;
    width: 100%;
    margin-top: 14px;
    padding: 0 60px;">
                                            <label class="control-label" style="float: left;width: 40%;text-align: left;position: relative;top: 12px;">
                                                <strong><?php echo JHtml::tooltip(
                                                        JText::_("COM_JGIVE_ENTER_AMOUNT_TP"),
                                                        JText::_("COM_JGIVE_ENTER_AMOUNT"),
                                                        '',
                                                        JText::_("COM_JGIVE_ENTER_AMOUNT")
                                                    );?></strong><span class="input-group-addon" id="basic-addon1">
											<?php
                                            $currency = $this->params->get('currency_symbol');

                                            // If currency symbol is not available then use currency code
                                            if (empty($currency))
                                            {
                                                $currency = $this->currency_code;
                                            }

                                            echo $currency;
                                            ?>
										</span>
                                            </label>
                                            <div class="controls" style="float: left;width: 50%">
                                                <div class="input-group">

                                                    <?php
                                                    // Minimum donation amount
                                                    $predefineAmount = !empty( $cdata['campaign']->minimum_amount) ? $cdata['campaign']->minimum_amount : 0;

                                                    // Get selected giveback amount
                                                    if (!empty($this->giveback_id))
                                                    {
                                                        foreach ($campaignGivebacks as $giveback)
                                                        {
                                                            if ($this->giveback_id == $giveback->id)
                                                            {
                                                                $predefineAmount = $giveback->amount;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <input type="number"
                                                           class="jgive-amount-field"
                                                           id="donation_amount"
                                                           name="donation_amount"
                                                           title="<?php echo JText::_("COM_JGIVE_ENTER_AMOUNT_TP");?>"
                                                           min="<?php echo !empty($this->cdata['campaign']->minimum_amount) ? $this->cdata['campaign']->minimum_amount : 0; ?>"
                                                           placeholder="<?php echo $predefineAmount; ?>"
                                                           class="required"
                                                           required />
                                                </div>
                                            </div>
                                            </div>
                                        </div>
<div id="paypal_fees" style="     width: 26%;   float: right;"><div class="fees" style="display: none;    color: #5fc6c2;">PayPal fees <span style="color: #000;font-weight: bold;"></span></div></div>
                                    </aside>

                                    <aside class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="display: none;">
                                        <div class="form-group">
                                            <div class="controls">
                                                <input type="hidden"
                                                       id="user_first_last_name"
                                                       name="user_first_last_name"
                                                       title="<?php echo JText::_("COM_JGIVE_FIRST_LAST_NAME");?>"
                                                       placeholder="<?php echo JText::_("COM_JGIVE_FIRST_LAST_NAME");?>"
                                                       value="<?php echo $user->name; ?>"
                                                       class="required"
                                                       required />
                                            </div>
                                            &nbsp;
                                            <div class="controls">
                                                <input type="hidden"
                                                       id="paypal_email"
                                                       name="paypal_email"
                                                       title="<?php echo JText::_("COM_JGIVE_EMAIL");?>"
                                                       placeholder="<?php echo JText::_("COM_JGIVE_EMAIL");?>"
                                                       value="<?php echo $user->email; ?>"
                                                       class="required"
                                                       required />
                                            </div>
                                        </div>
                                    </aside>
                                    <?php
                                    if (count($campaignGivebacks))
                                    {
                                        ?>
                                        <div id="jgive_givebacks" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <h4><?php echo JText::_("COM_JGIVE_SELECT_YOUR_GIVEBACK");?></h4>

                                            <div class="radio">
                                                <label>
                                                    <input alt="0.00"
                                                           type="radio"
                                                           name="givebacks"
                                                           value="0"
                                                           title="<?php echo JText::_("COM_JGIVE_NO_GIVEBACK_TITLE"); ?>"
                                                           onclick="populateSelected_GivebackAmount(<?php echo $cdata['campaign']->minimum_amount; ?>, this)" />
                                                    <?php echo JText::_("COM_JGIVE_THANKS_MSG"); ?>
                                                </label>
                                            </div>
                                            <?php
                                            foreach ($campaignGivebacks as $giveback)
                                            {
                                                $sold_givebacks = $giveback->sold_giveback;

                                                if ($sold_givebacks < $giveback->total_quantity)
                                                {
                                                    $checked = "";
                                                    $class = "";

                                                    // Highlight selected giveback
                                                    if ($this->giveback_id == $giveback->id)
                                                    {
                                                        $checked = ' checked="checked" ';
                                                        $class = " jgive_active ";
                                                    } ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input alt="<?php echo $giveback->amount; ?>"
                                                                   type="radio"
                                                                   name="givebacks"
                                                                   value="<?php echo $giveback->id; ?>"
                                                                <?php echo $checked; ?>
                                                                   onclick="populateSelected_GivebackAmount(<?php echo $giveback->amount; ?>, this)"
                                                            />
                                                            <strong> <?php echo $jgiveFrontendHelper->getFormattedPrice($giveback->amount); ?> + </strong> &nbsp;

                                                            <?php echo !empty($giveback->description) ? $giveback->description : JText::_("COM_JGIVE_NO_DESC_AVAILABLE"); ?>
                                                        </label>
                                                    </div>
                                                    <?php
                                                }?>

                                                <?php
                                            } ?>
                                        </div>

                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <footer id="jgive_footer">
                            <div>
                                <div style="width: 50%;float: left;    margin-bottom: 0;" class="form-group">
                                    <?php
                                    if ($cdata['campaign']->type == "donation")
                                    {
                                        if ($show_field == 1 || $donation_anonym == 0 )
                                        {
                                            ?>
                                            <label class="control-label">
                                                <strong>
                                                    <?php echo JHtml::tooltip(
                                                        JText::_("COM_JGIVE_DONATE_ANONYMOUSLY_TOOLTIP"),
                                                        JText::_("COM_JGIVE_DONATE_ANONYMOUSLY"), '',
                                                        JText::_("COM_JGIVE_DONATE_ANONYMOUSLY")
                                                    );?>
                                                </strong>
                                            </label>
                                            <div class="controls radio">
                                                <label style="width: 50%;float: left;" class="radio inline">
                                                    <input type="radio" name="annonymousDonation" id="annonymousDonation1" value="1" >
                                                    <?php echo JText::_('COM_JGIVE_YES');?>
                                                </label>
                                                <label style="width: 50%;float: left;" class="radio inline">
                                                    <input type="radio" name="annonymousDonation" id="annonymousDonation2" value="0" checked>
                                                    <?php echo JText::_('COM_JGIVE_NO');?>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <script>
                                    jQuery('.es-story-post-as .dropdown-menu li').click(function(){
                                        var page_image = jQuery(this).attr('data-image');
                                        var name = jQuery(this).attr('data-name');
                                        jQuery('#post_as-image').attr('src',page_image);
                                        jQuery('#user_first_last_name').val(name);
                                        jQuery(this).addClass('is-active');
                                        jQuery(this).siblings('li').removeClass('is-active');
                                    });
                                </script>
                                <?php if(!empty($userGroupIds)): ?>
                                <div style="    width: 50%;
    float: left;"><strong style="    float: left;
    padding-right: 15px;">
                                        <?php echo JHtml::tooltip('Donate as','Donate as','', 'Donate as');?>
                                    </strong>
                                    <div class="es-story-post-as" style="    padding: 0;float: left;">
                                        <div class="dropdown_">
                                            <button style="padding: 5px 10px!important;" type="button" class="btn btn-es-default-o btn-sm dropdown-toggle_ dropup" data-bs-toggle="dropdown" data-postas-toggle="">
                                                <div class="o-avatar o-avatar--sm" data-postas-avatar="">

                                                    <img id="post_as-image" src="<?php echo $es_user->getAvatar(); ?>" alt="<?php echo $user->name; ?>" width="24" height="24">

                                                </div>
                                                <i class="i-chevron i-chevron--down" data-postas-icon=""></i>
                                            </button>

                                            <ul class="dropdown-menu dropdown-menu-right dropdown-menu--post-as" data-postas-menu="">
                                                <li data-name="<?php echo $user->name; ?>" data-image="<?php echo $es_user->getAvatar(); ?>" data-item="" data-postas-page="" data-value="user" class="is-active">
                                                    <a href="javascript:void(0);">
					<span data-postas-avatar="">

	<img src="<?php echo $es_user->getAvatar(); ?>" alt="<?php echo $user->name; ?>" width="24" height="24">

					</span>
                                                        <?php echo $user->name; ?>				</a>
                                                </li>

                                                <?php foreach($userGroupIds as $user_page): ?>
                                                <li data-name="<?php echo $user_page->name; ?>" data-postas-user="" data-image="<?php echo $user_page->image; ?>" data-value="page" class="">
                                                    <a href="javascript:void(0);">
					<span data-postas-avatar="">

	<img src="<?php echo $user_page->image; ?>" alt="<?php echo $user_page->name; ?>" width="24" height="24">

					</span>
                                                        <?php echo $user_page->name; ?>				</a>
                                                </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                        <input type="hidden" name="postas" value="page" data-postas-hidden="">
                                    </div>

                                <?php endif; ?>
                            </div>
                            <div id="check_pay_status" style="width: 100%;float: left;" class="form-group">
                             

                                    <?php
                                    $select = array();
                                    $gateways = array_merge($select, $this->gateways);
                                    $gateways = array_filter($gateways);

                                    // If only one geteway then keep it as selected
                                    if (count($gateways) >= 1)
                                    {
                                        // Id and value is same
                                        $default = $gateways[0]->id;
                                    }

                                    if (empty($this->gateways))
                                    {
                                        echo JText::_('COM_JGIVE_NO_PAYMENT_GATEWAY');
                                    }
                                    else
                                    {

                                            ?>
                                            <label style="    display: none;" class="radio-inline">
                                                <input type="hidden" name="gateways"
                                                       id="qtc_<?php echo $this->gateways[0]->id; ?>"
                                                       value="<?php echo $this->gateways[0]->id; ?>" onclick="placeQuickPayment()">

                                            </label>
                                            <div class="donate_now_popup_box" style="width: 100%;text-align: center;">
                                                <input class="buton_paypal" type="button" onclick="placeQuickPayment()"  src="https://www.paypal.com/en_US/i/btn/x-click-but02.gif" border="0" value="Donate Now!" alt="Make payments with PayPal - it's fast, free and secure!">
                                            </div>
                                         <?php

                                    }
                                    ?>

                            </div>
                            <div class="clearfix"></div>

                        </footer>

                        <input type="hidden" name="option" value="com_jgive" />
                        <input type="hidden" name="view" value="donations" />
                        <!--<input type="hidden" name="controller" value="donations" />-->
                        <input type="hidden" name="task" value="donations.placeOrder" />

                        <input type="hidden" name="cid" value="<?php echo $cdata['campaign']->id;?>" />
                        <input type="hidden" name="Itemid" value="<?php echo $input->get('Itemid', '', 'INT');?>"/>
                        <input type="hidden" name="userid" id="userid" value="<?php echo !empty($user->id) ? $user->id : 0; ?>">
                        <input type="hidden" name="order_id" id="order_id" value="0" />
                        <input type="hidden" name="account" value="guest" />
                    </form>
                    <div id="payment_tab_table_html"></div>
                </div>
            </div>

            <!--- Javascript global declaration here -->
            <script>
                jQuery("#donation_amount").keyup(function(){
                    var val = this.value;
                    if (val.length > 0) {
                        var fees = (val*2.9)/100 + 0.3;
                        var fee = fees.toFixed(2);
                        jQuery('#paypal_fees .fees').show();
                        jQuery('#paypal_fees .fees span').html("$" + fee);
                    }
                });


                var jgive_baseurl = "<?php echo JUri::root(); ?>";

                var minimum_amount = "<?php echo $cdata['campaign']->minimum_amount;?>";
                minimum_amount = parseInt(minimum_amount);

                var send_payments_to_owner = "<?php echo $this->params->get('send_payments_to_owner');?>";
                var commission_fee = "<?php echo $this->params->get('commission_fee');?>";
                var fixed_commissionfee = "<?php echo $this->params->get('fixed_commissionfee');?>";

                var givebackDetails = <?php echo !empty($this->campaignGivebacks) ? json_encode($this->campaignGivebacks, 1) : 0;?>;
            </script>

        </div>
    </content>
</dialog>
