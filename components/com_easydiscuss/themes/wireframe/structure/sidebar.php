<script>
    jQuery(document).ready(function($) {
        if(jQuery('.es-right-sidebar .sidebar_widget').height() <= jQuery(window).height()){
            jQuery('.es-right-sidebar .es-right-sidebar-fixed').addClass('position', 'fixed');
        }else{
            jQuery(window).scroll(function() {
                var d = jQuery(this).scrollTop() + jQuery(window).height();
                var e = jQuery(".es-right-sidebar .sidebar_widget").height() + jQuery(".es-right-sidebar").offset().top;
                if(d >= e){
                    if(jQuery(".es-right-sidebar .es-right-sidebar-fixed").hasClass('fixed_pos') == false){
                        jQuery('.es-right-sidebar .es-right-sidebar-fixed').addClass('fixed_pos');
                    }
                    //jQuery('.es-right-sidebar .sidebar_widget').css('margin-top', (d-e)+'px');
                }else if(jQuery('.es-right-sidebar .es-right-sidebar-fixed').hasClass('fixed_pos')){
                    jQuery('.es-right-sidebar .es-right-sidebar-fixed').removeClass('fixed_pos');
                }
            });
        }
    });
</script>
<style>
    .es-right-sidebar .es-right-sidebar-fixed.fixed_pos{position: fixed;bottom: 0%;width:278.88px; }
</style>

<div class="es-sidebar" data-sidebar="">
    <div class="es-sidebar_fixed">
        <?php
        $user = ES::user();
        ?>
        <?php if ($user->id) { ?>


            <div class="es-left-sidebar-user-avatar">


                <a href="" class="o-avatar o-avatar--sm" data-user-id="<?php echo $user->id; ?>">
                    <img src="<?php echo $user->getAvatar(); ?>" alt="<?php echo $user->name; ?>" width="24" height="24">
                </a>

                <a class="user-link" href=""> <span><?php echo $user->name; ?></span></a>

            </div>
        <?php } ?>




        <div class="es-side-widget" data-type="feeds">
            <div class="es-side-widget__hd">
                <div class="es-side-widget__title">
                    Resources
                </div>
            </div>
            <?php if($user->id): ?>
            <div class="es-modules-wrap es-modules-es_right_sidebar">


                    <div class="ed-mod-ask">

                        <div class="ed-mod-ask__content" style="padding: 0px 15px 14px 0;">
                            <a class="btn btn-success btn-sm btn-block" href="/discussions/ask?category=0">
                                <span>Ask Question</span>

                            </a>
                            <a class="btn btn-success btn-sm btn-block" href="/discussions?view=resourcepost&amp;category=0">
                                <span>Post to resources</span>
                            </a>
                        </div>
                    </div>

                </div>
            <?php endif; ?>
            <div class="es-side-widget__bd">
                <?php
                $categoryModel = ED::model('Categories');
                $model = ED::model('category');
                $categories = $categoryModel->getCategoryTree();

                ?>

                <ul class="o-tabs o-tabs--stacked feed-items" data-dashboard-feeds="">
                    <?php foreach($categories as $category):
                        if($category->parent_id != 1){
                        continue;
                        }
                        ?>
                        <li class="o-tabs__item">
                            <a href="<?php echo $category->getPermalink(); ?>" class="o-tabs__link">
                                <?php echo $category->title; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>


                </ul>
            </div>
        </div>
        <div class="es-side-widget" data-type="feeds">
            <div class="es-side-widget__hd">
                <div class="es-side-widget__title">
                    Explore
                </div>
            </div>
            <div class="es-side-widget__bd">
                <ul class="o-tabs o-tabs--stacked feed-items">

                    <li class="o-tabs__item  ">
                        <a href="/resource" class="o-tabs__link o-tabs__link2">
                            <i class="es-explore__icons1 es-explore__resources"></i>
                            Resources
                        </a>
                    </li>


                    <li class="o-tabs__item  ">
                        <a href="/free-items" class="o-tabs__link o-tabs__link2">
                            <i class="es-explore__icons1 es-explore__free_items"></i>
                            Free items
                        </a>
                    </li>


                    <li class="o-tabs__item  ">
                        <a href="/community/pages/categories/16-support-profiles/all/latest" class="o-tabs__link o-tabs__link2">
                            <i class="es-explore__icons1 es-explore__support_profiles"></i>
                            Support Profiles
                        </a>
                    </li>




                    <li class="o-tabs__item ">
                        <a href="/community/pages/categories/6-local-business-or-place/all/latest" class="o-tabs__link">
                            <i class="es-explore__icons es-explore__pages"></i>
                            Businesses								</a>
                    </li>

                    <li class="o-tabs__item ">
                        <a href="/community/groups" class="o-tabs__link">
                            <i class="es-explore__icons es-explore__groups"></i>

                            Groups
                        </a>
                    </li>



                    <li class="o-tabs__item ">
                        <a href="/community/friends" class="o-tabs__link">
                            <i class="es-explore__icons es-explore__friends"></i>

                            My Friends
                        </a>
                    </li>







                    <li class="o-tabs__item ">
                        <a href="/community/users" class="o-tabs__link">
                            <i class="es-explore__icons es-explore__people"></i>

                            People
                        </a>
                    </li>


                </ul>

            </div>

        </div>






        <div class="widgets-wrapper">
        </div>

    </div>
</div>


