<?php
/**
* @package      EasyDiscuss
* @copyright    Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyDiscuss is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<!--h2 class="ed-page-titlecat"><?php //echo JText::_('COM_EASYDISCUSS_CATEGORIES'); ?></h2-->
<?php
            $app = JFactory::getApplication(); // Access the Application Object
            $menu = $app->getMenu(); // Load the JMenuSite Object
            $active = $menu->getActive(); // Load the Active Menu Item as an stdClass Object
            $menuurl = $active->alias;
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__discuss_category');
			//$query->where('id="3"');
			$db->setQuery((string)$query);
			$res = $db->loadObjectList();
			
			//$ask='/index.php/'.$menuurl.'/ask';
			
			
			?>
			<?php
			
if ($menuurl=='q-a'){
			?>
			<script>
			jQuery( document ).ready(function() {
				jQuery('.serch1').append(jQuery('.ed-navbar__search').html());
             });
          	</script>

			<div class="serch1"></div>

			

<div class="discuss-timeline">
	<ul class="unstyled discuss-list-grid clearfix">
	<?php
	foreach($res as $res1){
					
			$title = $res1->title;
			$alias = $res1->alias;
			$description = $res1->description;
			?>
				<li class="parent dropdown">
			<div class="media">
				<div class="media-object pull-left">
					<div class="discuss-avatar avatar-medium">
						<img alt="Technical Topic" src="https://dwbi.org/components/com_easydiscuss/themes/simplistic/images/default_category.png">
					</div>
				</div>

				<div class="media-body">
					<div class="pull-left">
						<h3>
							<a href="<?php echo $menuurl.'/'.$alias;?>"><?php echo $title;?></a>
						</h3>
<?php echo $description;?>						
													

													
																		</div>
				</div>
			</div>
		</li>
	<?php } ?>
			</ul>
	</div>
<?php 
			
		}
			
			
?>
<?php
			
if ($menuurl!='q-a'){
			?>
<div class="ed-categories>
	<div class="ed-list">
	<?php if ($categories) { ?>
		<?php foreach ($categories as $category) { ?>
			<div class="ed-cat-panel">
				<div class="ed-cat-panel__hd">
					<?php echo $this->output('site/categories/default.item', array('category' => $category)); ?>

					<?php if ($category->totalSubcategories && $this->config->get('layout_show_all_subcategories')) { ?>
					<a data-ed-toggle="collapse" href="#collapse-cat-<?php echo $category->id; ?>" class="ed-cat-panel__toggle">
						<i class="fa fa-chevron-down"></i>
					</a>
					<?php } ?>
				</div>

				<?php if ($category->totalSubcategories && $this->config->get('layout_show_all_subcategories')) { ?>
					<div class="ed-cat-panel__bd collapse in" id="collapse-cat-<?php echo $category->id; ?>">
						<div class="ed-tree">
							<?php echo $category->printTrees($category->childs, $category->id); ?>
						</div>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
	<?php } ?>
	</div>
</div>
<?php } ?>
<style>
.discuss-timeline li.parent.dropdown {float: left;}
.discuss-timeline ul{margin:0px 0px 0px; padding:0; width:100%;}
.discuss-timeline ul li{border:solid 1px #ccc; width:32.5%; padding:5px; margin:0 1% 0 0; min-height:250px;}
.discuss-timeline ul li:last-child{margin-right:0;}
.question_rht{float:right;}
.discuss-timeline{float:left; width:100%; margin-top:10px;}
.serch1 {float: right;margin-bottom: 10px;margin-right: 15px;width: 83%;}
#ed .serch1 form{padding:0;}
#ed .serch1 input {
    padding: 8px 15px;
}
.discuss-timeline ul { margin: 0 !important;padding: 0 !important;width: 100%;}
.serch1 input[type="text"] {background: #d5d8d8 !important}
.serch1 .ed-navbar__search-input {color: #000!important;}
.ed-page-titlecat{font-size: 24px;line-height: 19.2px;margin: 5px 0;word-wrap: break-word;}
a.question_rht{
    color: #fff;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
    background-color: #3498db;
    background-image: -moz-linear-gradient(top,#3498db,#3498db);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#3498db),to(#3498db));
    background-image: -webkit-linear-gradient(top,#3498db,#3498db);
    background-image: -o-linear-gradient(top,#3498db,#3498db);
    background-image: linear-gradient(to bottom,#3498db,#3498db);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff3498db', endColorstr='#ff3498db', GradientType=0);
    border-color: #3498db #3498db #1d6fa5;
    border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
    filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);padding:6px 30px;
}
}
</style>