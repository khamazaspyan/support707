<?php
/**
* @package		EasyDiscuss
* @copyright	Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyDiscuss is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>

            <script>
			jQuery( document ).ready(function() {
				jQuery('.serch1').append(jQuery('.ed-navbar__search').html());
             });
          	</script>

			<div class="serch1"></div>

			
			<?php
			 $app = JFactory::getApplication(); // Access the Application Object
            $menu = $app->getMenu(); // Load the JMenuSite Object
            $active = $menu->getActive(); // Load the Active Menu Item as an stdClass Object
            $menuurl = $active->alias;
			
			
			?>
			<?php
			
if ($menuurl=='q-a'){
			?>
			
			<?php echo JString::strtoupper($activeCategory->getTitle());?>
			<?php if ($activeCategory->getParams()->get('show_description') && !$this->config->get('layout_category_description_hidden')) { ?>
				<div class="ed-forums-cat-header__desp">
					<?php echo $activeCategory->description;?>
				</div>
			<?php } ?>

    <div class="ed-filter-bar t-lg-mt--lg t-lg-mb--md">

	<!-- Filter tabs -->
	<ul class="o-tabs o-tabs--ed pull-left">
		<li class="o-tabs__item <?php echo !$activeFilter || $activeFilter == 'allposts' || $activeFilter == 'all' ? ' active' : '';?>"
			data-filter-tab
			data-filter-type="allposts"
			data-filter-catid="<?php echo $menuCatId; ?>"
		>
			<a class="o-tabs__link allPostsFilter" data-filter-anchor href="<?php echo EDR::_('view=index');?>">
				<?php echo JText::_('COM_EASYDISCUSS_FILTER_ALL_POSTS'); ?>
			</a>
		</li>

		<?php if($this->config->get('main_qna') && $this->config->get('layout_enablefilter_unresolved')) { ?>
		<li class="o-tabs__item <?php echo $activeFilter == 'unresolved' ? ' active' : '';?>"
			data-filter-tab
			data-filter-type="unresolved"
			data-filter-catid="<?php echo $menuCatId; ?>"
		>
			<a class="o-tabs__link unResolvedFilter" data-filter-anchor href="<?php echo EDR::_('view=index&filter=unresolved');?>">
				<?php echo JText::_('COM_EASYDISCUSS_FILTER_UNRESOLVED');?>
			</a>
		</li>
		<?php } ?>

		<?php if($this->config->get('main_qna') && $this->config->get('layout_enablefilter_resolved')) { ?>
		<li class="o-tabs__item <?php echo $activeFilter == 'resolved' ? ' active' : '';?>"
			data-filter-tab
			data-filter-type="resolved"
			data-filter-catid="<?php echo $menuCatId; ?>"
		>
			<a class="o-tabs__link resolvedFilter" data-filter-anchor href="<?php echo EDR::_('view=index&filter=resolved');?>">
				<?php echo JText::_('COM_EASYDISCUSS_FILTER_RESOLVED');?>
			</a>
		</li>
		<?php } ?>

		<?php if($this->config->get('layout_enablefilter_unanswered')){ ?>
		<li class="o-tabs__item <?php echo $activeFilter == 'unanswered' ? ' active' : '';?>"
			data-filter-tab
			data-filter-type="unanswered"
			data-filter-catid="<?php echo $menuCatId; ?>"
		>
			<a class="o-tabs__link unAnsweredFilter" data-filter-anchor href="<?php echo EDR::_('view=index&filter=unanswered');?>">
				<?php echo JText::_('COM_EASYDISCUSS_FILTER_UNANSWERED'); ?>
			</a>
		</li>
		<?php } ?>

		<?php if ($this->config->get('layout_enablefilter_unread') && ED::isLoggedIn()) { ?>
		<li class="o-tabs__item <?php echo $activeFilter == 'unread' ? ' active' : '';?>"
			data-filter-tab
			data-filter-type="unread"
			data-filter-catid="<?php echo $menuCatId; ?>"
		>
			<a class="o-tabs__link unreadFilter" data-filter-anchor href="<?php echo EDR::_('view=index&filter=unread');?>">
				<?php echo JText::_('COM_EASYDISCUSS_FILTER_UNREAD');?>
			</a>
		</li>
		<?php } ?>

	</ul>

	<!-- Sort tabs -->
	<div class="ed-filter-bar__sort-action pull-right">
		<select data-index-sort-filter>
		  <option value="latest" <?php echo $activeSort == 'latest' || $activeSort == '' ? ' selected="true"' : '';?> data-sort-tab data-sort-type="latest"><?php echo JText::_('COM_EASYDISCUSS_SORT_LATEST');?></option>
		  <?php if ($activeFilter != 'unread') { ?>
		  	<option value="popular" <?php echo $activeSort == 'popular' ? ' selected="true"' : '';?> data-sort-tab data-sort-type="popular"><?php echo JText::_('COM_EASYDISCUSS_SORT_POPULAR');?></option>
		  	<option value="title" <?php echo $activeSort == 'title' ? ' selected="true"' : '';?> data-sort-tab data-sort-type="title"><?php echo JText::_('COM_EASYDISCUSS_SORT_TITLE');?></option>
		  <?php } ?>
		</select>
	</div>

</div>

<?php } ?>	
<?php
//if ($menuurl='q-a'){
	?>
<div class="ed-forums-cat-header t-lg-mb--lg">
	<div class="o-flag">
		<?php if ($this->config->get('layout_category_show_avatar', true)) { ?>
		<div class="o-flag__image o-flag--top">
			<a href="<?php echo $activeCategory->getPermalink(); ?>" class="o-avatar o-avatar--md">
				<img src="<?php echo $activeCategory->getAvatar();?>" width="48" alt="<?php echo $this->html('string.escape', $activeCategory->getTitle());?>" />
			</a>
		</div>
		<?php } ?>
		<div class="o-flag__body">
			<a href="<?php echo $activeCategory->getPermalink(); ?>" class="ed-forums-cat-header__title">

				<?php echo JString::strtoupper($activeCategory->getTitle());?>
			</a>
			<?php if ($activeCategory->getParams()->get('show_description') && !$this->config->get('layout_category_description_hidden')) { ?>
				<div class="ed-forums-cat-header__desp">
					<?php echo $activeCategory->description;?>
				</div>
			<?php } ?>
			<div class="o-grid">
				<div class="o-grid__cell">
					<?php if (isset($childs) && $childs) { ?>
					<div class="ed-forums-cat-header__sub-cat">
						<div class="ed-forums-cat-header__sub-cat-title pull-left">
							<?php echo JText::_('COM_EASYDISCUSS_SUB_CATEGORY'); ?>    
						</div>
						<ol class="g-list-inline g-list-inline--dashed">
						<?php foreach($childs as $cItem) { ?>
						  <li><a href="<?php echo $cItem->getPermalink() ?>"><?php echo $cItem->getTitle(); ?></a></li>
						<?php } ?>
						</ol>
					</div>
					<?php } ?>

					<ol class="g-list-inline g-list-inline--delimited ed-forums-cat-header__breadcrumb">
						<li>
							<a href="<?php echo EDR::getForumsRoute(); ?>">
								<?php echo JText::_('COM_EASYDISCUSS_TOOLBAR_FORUMS'); ?>
							</a>
						</li>

						<?php foreach ($breadcrumbs as $breadcrumb) { ?>
						<li data-breadcrumb="▸">
							<?php if ($breadcrumb->id == $activeCategory->id && !$listing) { ?>
								<?php echo $breadcrumb->title;?>
							<?php } else { ?>
								<a href="<?php echo $breadcrumb->link; ?>"><?php echo $breadcrumb->title; ?></a>
							<?php } ?>
						</li>
						<?php } ?>

						<?php if ($listing) { ?>
							<li data-breadcrumb="▸">
								<?php echo JText::_('COM_EASYDISCUSS_FORUMS_BREADCRUMB_LAYOUT'); ?>
							</li>
						<?php } ?>
					</ol>
				</div>
                <?php
                    $url =  EDR::_('view=ask&category=' . $activeCategory->id);
                    $text = "Ask a question";
                    if($activeCategory->id == 1 || $activeCategory->parent_id == 1){
                        $url = EDR::_('view=resourcepost&category=' . $activeCategory->id);
                        $text =  JText::_('COM_EASYDISCUSS_NEW_POST');
                    }

                ?>
				<div class="o-grid__cell o-grid__cell--auto-size o-grid__cell--bottom t-lg-pl--lg t-xs-pl--no">
					<?php if ($activeCategory->canPost()) { ?>
					<a class="btn btn-primary ed-forums-cat-header__btn t-lg-pull-right t-xs-mt--lg" href="<?php echo $url;?>">
						<i class="fa fa-pencil t-lg-mr--sm"></i> <?php echo $text;?>
					</a>
					<?php } ?>        
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php if (!$activeCategory->container && ($this->config->get('main_rss') || $this->config->get('main_ed_categorysubscription'))) { ?>
<div class="ed-subscribe t-lg-mb--lg">
	<?php if ($this->config->get('main_rss')) { ?>
	<a href="<?php echo $activeCategory->getRSSPermalink();?>" class="t-lg-mr--md" target="_blank">
		<i class="fa fa-rss-square ed-subscribe__icon t-lg-mr--sm"></i>&nbsp;<?php echo JText::_('COM_EASYDISCUSS_SUBSCRIBE_VIA_RSS'); ?>
	</a>
	<?php } ?>

	<?php if($this->config->get('main_ed_categorysubscription')) { ?>
	<?php echo ED::subscription()->html($this->my->id, $activeCategory->id, 'category'); ?>
	<?php } ?>
</div>
<?php //} ?>
<?php } ?>
<style>
.discuss-timeline li.parent.dropdown {float: left;}
.discuss-timeline ul{margin:0px 0px 0px; padding:0; width:100%;}
.discuss-timeline ul li{border:solid 1px #ccc; width:32.5%; padding:5px; margin:0 1% 0 0; min-height:250px;}
.discuss-timeline ul li:last-child{margin-right:0;}
.question_rht{float:right;}
.discuss-timeline{float:left; width:100%; margin-top:10px;}
.serch1 {float: right;margin-bottom: 10px;margin-right: 15px;width: 83%;}
#ed .serch1 form{padding:0;}
#ed .serch1 input {
    padding: 8px 15px;
}
.discuss-timeline ul { margin: 0 !important;padding: 0 !important;width: 100%;}
.serch1 input[type="text"] {background: #d5d8d8 !important}
.serch1 .ed-navbar__search-input {color: #000!important;}
.ed-page-titlecat{font-size: 24px;line-height: 19.2px;margin: 5px 0;word-wrap: break-word;}
a.question_rht{
    color: #fff;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
    background-color: #3498db;
    background-image: -moz-linear-gradient(top,#3498db,#3498db);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#3498db),to(#3498db));
    background-image: -webkit-linear-gradient(top,#3498db,#3498db);
    background-image: -o-linear-gradient(top,#3498db,#3498db);
    background-image: linear-gradient(to bottom,#3498db,#3498db);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff3498db', endColorstr='#ff3498db', GradientType=0);
    border-color: #3498db #3498db #1d6fa5;
    border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
    filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);padding:6px 30px;
}
}
</style>