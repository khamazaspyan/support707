<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2017 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined("_JEXEC") or die("Unauthorized Access");
?>
<div class="eb-sharethis mt-20">
	<span class="st_sharethis_large st-custom-button" data-network="sharethis" data-url="<?php echo $post->getPermalink(false, true);?>" data-title="<?php echo $post->title;?>">ShareThis</span>
	<span class="st_facebook_large st-custom-button" data-network="facebook" data-url="<?php echo $post->getPermalink(false, true);?>" data-title="<?php echo $post->title;?>">Facebook</span>
	<span class="st_twitter_large st-custom-button" data-network="twitter" data-url="<?php echo $post->getPermalink(false, true);?>" data-title="<?php echo $post->title;?>">Tweet</span>
	<span class="st_linkedin_large st-custom-button" data-network="linkedin" data-url="<?php echo $post->getPermalink(false, true);?>" data-title="<?php echo $post->title;?>">LinkedIn</span>
	<span class="st_pinterest_large st-custom-button" data-network="pinterest" data-url="<?php echo $post->getPermalink(false, true);?>" data-title="<?php echo $post->title;?>">Pinterest</span>
	<span class="st_email_large st-custom-button" data-network="email" data-url="<?php echo $post->getPermalink(false, true);?>" data-title="<?php echo $post->title;?>">Email</span>
</div>
