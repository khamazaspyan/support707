<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.model');

/**
 * Class for JBolo send file model
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloModelSendFile extends JModelLegacy
{
	/**
	 * Uploads a file to the configured path
	 * Use upload helper
	 *
	 * @return  void
	 *
	 * @since  3.0
	 */
	public function uploadFile()
	{
		// Load uploadHelper file
		$uploadHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/upload.php';

		if (!class_exists('uploadHelper'))
		{
			JLoader::register('uploadHelper', $uploadHelperPath);
			JLoader::load('uploadHelper');
		}

		// Doing this echoes json response after file is uploded
		$upload_handler = new uploadHelper;
	}

	/**
	 * Downloads the file requested by user
	 *
	 * @return  void
	 *
	 * @since  3.0
	 */
	public function downloadFile()
	{
		/**
		 * Allow direct file download (hotlinking)?
		 * Empty - allow hotlinking
		 * If set to nonempty value (Example: example.com) will only allow downloads when referrer contains this text
		 **/
		define('ALLOWED_REFERRER', '');

		// Download folder, i.e. folder where you keep all files for download.
		// MUST end with slash (i.e. "/" )
		define('BASE_DIR', JPATH_SITE . '/components/com_jbolo/uploads/');

		// Log downloads?  true/false
		define('LOG_DOWNLOADS', true);

		// Log file name
		define('LOG_FILE', JPATH_SITE . '/media/com_jbolo/downloads_log.php');

		/**
		 * Allowed extensions list in format 'extension' => 'mime type'
		 * If myme type is set to empty string then script will try to detect mime type
		 * itself, which would only work if you have Mimetype or Fileinfo extensions
		 * installed on server.
		 **/
		$allowed_ext = array(
			// Archives
			'zip'  => 'application/zip',

			// Documents
			'txt'  => 'text/plain',
			'pdf'  => 'application/pdf',
			'doc'  => 'application/msword',
			'xls'  => 'application/vnd.ms-excel',
			'ppt'  => 'application/vnd.ms-powerpoint',
			'docm' => 'application/vnd.ms-word.document.macroEnabled.12',
			'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'dotm' => 'application/vnd.ms-word.template.macroEnabled.12',
			'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
			'ppsm' => 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
			'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
			'pptm' => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
			'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
			'xlsm' => 'application/vnd.ms-excel.sheet.macroEnabled.12',
			'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'xps'  => 'application/vnd.ms-xpsdocument',

			// Executables
			/*'exe' => 'application/octet-stream',*/

			// Images
			'bmp'  => 'image/x-ms-bmp',
			'bmp'  => 'image/x-bmp',
			'gif'  => 'image/gif',
			'png'  => 'image/png',
			'jpe'  => 'image/jpeg',
			'jpe'  => 'image/pjpeg',
			'jpeg' => 'image/jpeg',
			'jpeg' => 'image/pjpeg',
			'jpg'  => 'image/jpeg',
			'jpg'  => 'image/pjpeg',

			// Audio
			'mp3'  => 'audio/mpeg',
			'wav'  => 'audio/x-wav',

			// Video
			'mpeg' => 'video/mpeg',
			'mpg'  => 'video/mpeg',
			'mpe'  => 'video/mpeg',
			'mov'  => 'video/quicktime',
			'avi'  => 'video/x-msvideo',
		);

		// DO NOT CHANGE BELOW
		// If hotlinking not allowed then make hackers think there are some server problems
		if (ALLOWED_REFERRER !== '')
		{
			if ((!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']), strtoupper(ALLOWED_REFERRER)) === false))
			{
				die("Internal server error. Please contact system administrator.");
			}
		}

		// Make sure program execution doesn't time out
		// Set maximum script execution time in seconds (0 means no limit)
		set_time_limit(0);

		if (!isset($_GET['f']) || empty($_GET['f']))
		{
			die("Please specify file name for download.");
		}

		// Get real file name.
		// Remove any path info to avoid hacking by adding relative path, etc.
		$fname = basename($_GET['f']);

		// Get full file path (including subfolders)
		$file_path = '';
		$this->findFile(BASE_DIR, $fname, $file_path);

		if (!is_file($file_path))
		{
			die("File does not exist. Make sure you specified correct file name.");
		}

		// File size in bytes
		$fsize = filesize($file_path);

		// File extension
		$fext = strtolower(substr(strrchr($fname, "."), 1));

		// Check if allowed extension
		if (!array_key_exists($fext, $allowed_ext))
		{
			die("Not allowed file type.");
		}

		// Get mime type
		if ($allowed_ext[$fext] == '')
		{
			$mtype = '';

			// Mime type is not set, get from server settings
			if (function_exists('mime_content_type'))
			{
				$mtype = mime_content_type($file_path);
			}
			elseif (function_exists('finfo_file'))
			{
				// Return mime type
				$finfo = finfo_open(FILEINFO_MIME);
				$mtype = finfo_file($finfo, $file_path);
				finfo_close($finfo);
			}

			if ($mtype == '')
			{
				$mtype = "application/force-download";
			}
		}
		else
		{
			// Get mime type defined by admin
			$mtype = $allowed_ext[$fext];
		}

		// Browser will try to save file with this filename, regardless original filename.
		// You can override it if needed.
		if (!isset($_GET['fc']) || empty($_GET['fc']))
		{
			$asfname = $fname;
		}
		else
		{
			// Remove some bad chars
			$asfname = str_replace(array('"', "'", '\\', '/'), '', $_GET['fc']);

			if ($asfname === '')
			{
				$asfname = 'NoName';
			}
		}

		ob_end_clean();
		header("Cache-Control: public, must-revalidate");
		header('Cache-Control: pre-check=0, post-check=0, max-age=0');

		// Problems with MS IE
		/*header("Pragma: no-cache");*/
		header("Expires: 0");
		header("Content-Description: File Transfer");
		/*header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");*/
		header("Content-Type: " . $mtype);
		header("Content-Length: " . (string) $fsize);
		header('Content-Disposition: attachment; filename="' . $asfname . '"');
		@readfile($file_path);

		// Log downloads
		if (!LOG_DOWNLOADS)
		{
			die();
		}

		$filename = 'downloads_log.php';
		$path     = JPATH_SITE . '/media/com_jbolo';
		$options  = "{DATE}\t{USER}\t{IP}\t{FILE}";
		jimport('joomla.error.log');
		JLog::addLogger(array('text_file' => $filename, 'text_entry_format' => $options, 'text_file_path' => $path), JLog::INFO, 'com_jbolo');
		$logEntry                    = new JLogEntry('File downloaded', JLog::INFO, 'com_jbolo');
		$logEntry->user              = JFactory::getUser()->id;
		$logEntry->downloadTimestamp = date("m.d.Y g:ia");
		$logEntry->ip                = $_SERVER['REMOTE_ADDR'];
		$logEntry->file              = $fname;
		JLog::add($logEntry);

		exit;
	}

	/**
	 * Check if the file exists, Check in subfolders too
	 *
	 * @param   string  $dirname     Directory name
	 * @param   string  $fname       File name
	 * @param   string  &$file_path  File path
	 *
	 * @return  void
	 */
	public function findFile($dirname, $fname, &$file_path)
	{
		$dir = opendir($dirname);

		while ($file = readdir($dir))
		{
			if (empty($file_path) && $file != '.' && $file != '..')
			{
				if (is_dir($dirname . '/' . $file))
				{
					// Recursion
					$this->findFile($dirname . '/' . $file, $fname, $file_path);
				}
				else
				{
					if (file_exists($dirname . '/' . $fname))
					{
						$file_path = $dirname . '/' . $fname;

						return;
					}
				}
			}
		}
	}
}
