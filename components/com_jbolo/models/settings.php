<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.model');

/**
 * Class for JBolo settings model
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloModelSettings extends JModelLegacy
{
	/**
	 * Saves a chat settings for a user.
	 *
	 * @return  boolean
	 *
	 * @since  3.0
	 */
	public function save()
	{
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		// Save user preferences
		$db  = JFactory::getDBO();
		$obj = new stdClass;
		$obj->user_id = JFactory::getUser()->id;

		if ($post->get('opt_out_of_chat') == 1)
		{
			$obj->state = -1;
		}
		elseif ($post->get('opt_out_of_group_chat') == 1)
		{
			$obj->state = 0;
		}
		else
		{
			$obj->state = 1;
		}

		if (!$db->updateObject('#__jbolo_users', $obj, 'user_id'))
		{
			echo $db->stderr();

			return false;
		}

		return true;
	}

	/**
	 * Get chat setttings.
	 *
	 * @return  object  Chat settings
	 *
	 * @since  3.0
	 */
	public function getSettings()
	{
		$user = JFactory::getUser();
		$db   = JFactory::getDBO();

		$query = "SELECT state
			FROM `#__jbolo_users`
			WHERE user_id=" . $user->id;

		$db->setQuery($query);

		return $db->loadObject();
	}

	/**
	 * Get info of all blocked users.
	 *
	 * @return  object  Details of blocked users
	 *
	 * @since  3.0
	 */
	public function getBlockedUsers()
	{
		$user = JFactory::getUser();

		$jbolousersHelper = new jbolousersHelper;
		$result           = $jbolousersHelper->getBlockedUser($user->id);

		$userInfoObjectList = array();

		// Get each user's info
		foreach ($result as $row)
		{
			$userInfoObj           = new stdclass;
			$userInfoObj->id       = $row->id;
			$userInfoObj->userid   = $row->blocked_user_id;
			$userInfoObj->name     = JFactory::getUser($row->blocked_user_id)->name;
			$userInfoObj->username = JFactory::getUser($row->blocked_user_id)->username;
			$userInfoObjectList[]  = $userInfoObj;
		}

		return $userInfoObjectList;
	}

	/**
	 * Get info of all blocked users.
	 *
	 * @return  object  Details of blocked users
	 *
	 * @since  3.0
	 */
	public function unblockUser()
	{
		$unblock_user_response['validate'] = new stdclass;

		$user = JFactory::getUser();
		$db   = JFactory::getDBO();

		// Get node id
		$input = JFactory::getApplication()->input;
		$rowid = $input->post->get('rowid', '', 'INT');

		$query = "DELETE FROM `#__jbolo_privacy`
			WHERE id=" . $rowid;

		$db->setQuery($query);

		if (!$db->execute())
		{
			$unblock_user_response['validate']->error     = 1;
			$unblock_user_response['validate']->error_msg = $db->stderr();

			return $unblock_user_response;
		}

		$unblock_user_response['validate']->error   = 0;
		$unblock_user_response['validate']->success = $db->stderr();

		return $unblock_user_response;
	}
}
