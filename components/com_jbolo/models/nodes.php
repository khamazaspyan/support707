<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.model');

// For JHtml::date
jimport('joomla.html.html');

/**
 * Class for JBolo nodes model
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloModelNodes extends JModelLegacy
{
	/**
	 * This function =>
	 * - adds an entry for a user in jbolo users table if it's not there
	 * - returns array of info about logged in user & other logged in users
	 * - it also returns info about nodes as it is from session
	 * - echoes json reponse
	 *
	 * @return  json  An array of nodes info, messsages, particpiants & loggedin user info
	 *
	 * @since  3.0
	 */
	public function startChatSession()
	{
		// Validate user
		$response = $this->validateUserLogin();

		$params = JComponentHelper::getParams('com_jbolo');
		$db     = JFactory::getDbo();
		$user   = JFactory::getUser();
		$uid    = $user->id;

		// Log start chat session

		/*if ($uid ==63)
		{
			$logf = JPATH_SITE . '/components/com_jbolo/' . $uid . '.php';
			$f    = @fopen($logf, 'a+');

			if ($f)
			{
				$log = "";

				if (isset($_SESSION['jbolo']))
				{
					$log .= "\n" . '////////////////////////////' . "\n";
					$log .= "\n" . 'StartChatSession START session o/p' . "\n";
					$log .= print_r($_SESSION['jbolo'], true);
				}

				@fwrite($f, $log);
				@fclose($f);
			}
		}*/

		// Use  nodes helper
		$nodesHelper = new nodesHelper;

		$query = "SELECT id, user_id
		FROM #__jbolo_users
		WHERE user_id=" . $uid;
		$db->setQuery($query);
		$jboloUser = $db->loadObject();

		$myobj = new stdclass;
		$date = JFactory::getDate();
		$myobj->last_activity = $date->toSql();

		$jboloFrontendHelper = new JboloFrontendHelper;

		// Get user device - pc, mobile or tablet 0,1 or 2
		$myobj->is_mobile    = $jboloFrontendHelper->isMobile();

		// If there is no entry for logged in user in jbolo users table, add new entry
		// This is needed for chat status and status message
		if ($jboloUser == null)
		{
			$myobj->user_id     = $uid;
			$myobj->chat_status = 1;
			$myobj->status_msg  = JText::_('COM_JBOLO_DEFAULT_STATUS_MSG');
			$db->insertObject('#__jbolo_users', $myobj);
		}
		else
		{
			$myobj->id     = $jboloUser->id;
			$db->updateObject('#__jbolo_users', $myobj, 'id');
		}

		// Get logged in user information
		// Use  users helper
		$jbolousersHelper = new jbolousersHelper;
		$u_data           = $jbolousersHelper->getLoggedinUserInfo($uid);

		$response['userlist']['me'] = $u_data;

		/*if(isset($_SESSION['jbolo']))
		{
			$log .= print_r($_SESSION['jbolo']['nodes'], true);
		}*/

		// Generate online user list
		// Use  users helper
		$data = $jbolousersHelper->getOnlineUsersInfo($uid);

		// Set userlist in response
		$response['userlist']['users'] = $data;

		// Check if nodes array is present in session
		if (isset($_SESSION['jbolo']['nodes']))
		{
			// Lets fix message ordering by ordering them in ascending order by mid
			$_SESSION['jbolo']['nodes'] = $nodesHelper->sortMessages($_SESSION['jbolo']['nodes'], 'mid', 'asc');

			// Remove duplicates
			$_SESSION['jbolo']['nodes'] = $nodesHelper->removeDuplicateMessages($_SESSION['jbolo']['nodes']);

			// Lets fix group chat window titles with current looged in users names
			$_SESSION['jbolo']['nodes'] = $nodesHelper->updateWindowTitles($_SESSION['jbolo']['nodes'], $uid);

			// Lets fix group chat window status
			$_SESSION['jbolo']['nodes'] = $nodesHelper->updateWindowStatus($_SESSION['jbolo']['nodes'], $uid);
			$response['nodes']          = $_SESSION['jbolo']['nodes'];

			// Use  nodes helper
			$nodeStatusArray  = $nodesHelper->getNodeStatusArray($_SESSION['jbolo']['nodes'], $uid);
			$response['nsts'] = $nodeStatusArray;

			// Lets fix chat participant list
			$_SESSION['jbolo']['nodes'] = $nodesHelper->updateNodeParticipants($_SESSION['jbolo']['nodes'], $uid);
			$response['nodes']          = $_SESSION['jbolo']['nodes'];
		}
		else
		{
			// If nodes array not found, initialize it
			$response['nodes'] = array();
			$response['nsts']  = array();
		}

		// For showing activity stream
		// Get settings
		$show_activity = $params->get('show_activity');

		if ($show_activity)
		{
			$template = $params->get('template');

			// Get current template from cookie if available
			if (isset($_COOKIE["jboloTheme"]))
			{
				$template = $_COOKIE["jboloTheme"];
			}

			// Load activity sream only for FB template
			if ($template == 'facebook')
			{
				// Get activity stream
				$integrationsHelper = new integrationsHelper;
				$ashtml             = $integrationsHelper->getActivityStreamHTML();

				if ($ashtml == '')
				{
					$response['ashtml'] = '<strong>' . JText::_('COM_JBOLO_ACTIVITY_INCORRECT_CONFIGURATION') . '</strong>';
				}
				else
				{
					$response['ashtml'] = $ashtml;
				}
			}
		}

		// Log start chat session

		/*if ($uid == 63)
		{
			$logf = JPATH_SITE . '/components/com_jbolo/' . $uid . '.php';
			$f    = @fopen($logf, 'a+');

			if ($f)
			{
				$log = "";

				if (isset($_SESSION['jbolo']))
				{
					$log .= "\n" . 'StartChatSession END session o/p' . "\n";
					$log .= print_r($_SESSION['jbolo'], true);
				}

				@fwrite($f, $log);
				@fclose($f);
			}
		}*/

		return $response;
	}

	/**
	 * Validates User Login
	 *
	 * @return  object
	 */
	public function validateUserLogin()
	{
		$user                 = JFactory::getUser();
		$uid                  = $user->id;
		$response['validate'] = new stdclass;

		// Use r logged out
		if (!$uid)
		{
			$response['validate']->error     = 1;
			$response['validate']->error_msg = JText::_('COM_JBOLO_UNAUTHORZIED_REQUEST');

			// Output json response
			header('Content-type: application/json');
			echo json_encode($response);
			jexit();
		}

		return $response;
	}

	/**
	 * Validates if a user is an active member of a node or not.
	 *
	 * @param   int  $uid  User id
	 * @param   int  $nid  Node id
	 *
	 * @return  json
	 *
	 * @since  3.0
	 */
	public function validateNodeParticipant($uid, $nid)
	{
		$response['validate'] = new stdclass;

		// Use  nodes helper
		$nodesHelper       = new nodesHelper;
		$isNodeParticipant = $nodesHelper->isNodeParticipant($uid, $nid);

		// Active participant
		if ($isNodeParticipant == 1)
		{
			return $response;
		}
		// Inactive participant (who left chat)
		elseif ($isNodeParticipant == 2)
		{
			$response['validate']->error     = 1;
			$response['validate']->error_msg = JText::_('COM_JBOLO_INACTIVE_MEMBER_MSG');
		}
		// 0 - not a valid group chat participant
		elseif (!$isNodeParticipant)
		{
			$response['validate']->error     = 1;
			$response['validate']->error_msg = JText::_('COM_JBOLO_NON_MEMBER_MSG');
		}

		json_encode($response);

		// Output json response
		header('Content-type: application/json');
		echo json_encode($response);
		jexit();
	}

	/**
	 * This function ->
	 * -- pushes a chat mesages sent by user to a node(to server/database actually)
	 * -- it also updates chat xref table so that all participants can get this message when polling is done
	 * -- it also updates session by adding new message data into session
	 * -- and finally echoes json reponse
	 * -- It takes these as POST inputs
	 * -- uid:776 / nid:38 / msg:hiiiiii / ts:1355822592 / type:1
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function pushChatToNode()
	{
		// Validate user
		$pushChat_response = $this->validateUserLogin();

		$db   = JFactory::getDbo();
		$user = JFactory::getUser();
		$uid  = $user->id;

		// Get inputs from posted data
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		// Get nid and msg
		$nid = $input->post->get('nid', '', 'INT');

		if (get_magic_quotes_gpc())
		{
			$msg = stripslashes($input->post->get('msg', '', 'STRING'));
		}
		else
		{
			$msg = $input->post->get('msg', '', 'STRING');
		}

		// Validate if nid is not specified
		if (!$nid)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_INVALID_NODE_ID');

			return $pushChat_response;
		}

		// Validate if message is not blank
		if ($msg === null)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_EMPTY_MSG');

			return $pushChat_response;
		}

		$nodesHelper = new nodesHelper;

		// Validate if this user is participant of this node
		$isNodeParticipant = $nodesHelper->isNodeParticipant($uid, $nid);

		// Error handling for inactive user
		if ($isNodeParticipant == 2)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_INACTIVE_MEMBER_MSG');

			return $pushChat_response;
		}

		// Error handling for not member/unauthorized access to this group chat
		if (!$isNodeParticipant)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_NON_MEMBER_MSG');

			return $pushChat_response;
		}

		// Check if curent node is active or not.
		$isActiveGroupNode = $nodesHelper->isActiveGroupNode($nid);

		// If this is not an active chat node show an error
		if (!$isActiveGroupNode)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_NODE_NOT_ACTIVE_MSG');

			return $pushChat_response;
		}

		// Get participants for this node
		// Status = 1 indicates that user is still part of node
		$query = "SELECT user_id
		FROM #__jbolo_node_users
		WHERE node_id = " . $nid . "
		AND user_id <> " . $uid . "
		AND status = 1";
		$db->setQuery($query);
		$participants = $db->loadColumn();

		// Get Node type
		$ctyp = $nodesHelper->getNodeType($nid);

		// One to one chat
		if ($ctyp == 1)
		{
			// Check is participants blocked logged in user
			$isBlocked = $nodesHelper->isBlockedBy($participants[0], $uid);

			if ($isBlocked)
			{
				$pushChat_response['validate']->error     = 1;
				$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_PATICIPANTS_BLOCKED_YOU');

				return $pushChat_response;
			}
		}
		else
		{
			// Don't allow sending message if there are no active members in this group chat
			if (!count($participants))
			{
				$pushChat_response['validate']->error     = 1;
				$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_NO_ACTIVE_MEMBERS_IN_GROUP_MSG');

				return $pushChat_response;
			}

			// Check if curent node is social group chat node.
			$socialGroupId = $nodesHelper->isSocialGroupChatNode($nid);

			// If no existing node found, create new node.
			if ($socialGroupId)
			{
				// Get social group chat node 'client'
				$client = $nodesHelper->getSocialGroupClient($nid);

				$integrationsHelper = new integrationsHelper;

				// Check if user is part of social group
				$isSocialGroupUser = $integrationsHelper->isSocialGroupUser($socialGroupId, $uid, $client);

				// Check if user is admin of social group
				$isSocialGroupAdmin = $integrationsHelper->isSocialGroupAdmin($socialGroupId, $uid, $client);

				// If no social group user
				if (!$isSocialGroupUser && !$isSocialGroupAdmin)
				{
					$pushChat_response['validate']->error     = 1;
					$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_NON_GROUP_MEMBER_MSG');

					return $pushChat_response;
				}
			}
		}

		/**
		 * @TODO decide what's better? - store first and then process message
		 * OR - process first and then display
		 **/

		// Trigger plugins to process message text
		// Backup original message, needed for social sync
		$originalMsg = $msg;
		$dispatcher  = JDispatcher::getInstance();
		JPluginHelper::importPlugin('jbolo', 'plg_jbolo_textprocessing');

		// Process urls
		$processedText = $dispatcher->trigger('processUrls', array($msg));
		$msg           = $processedText[0];

		/*// Process smilies
		$processedText = $dispatcher->trigger('processSmilies', array($msg));
		$msg           = $processedText[0];*/

		// Process bad words
		$processedText = $dispatcher->trigger('processBadWords', array($msg));
		$msg           = $processedText[0];

		// Add msg to database
		$myobj             = new stdclass;
		$myobj->from       = $uid;
		$myobj->to_node_id = $nid;
		$myobj->msg        = $msg;
		$myobj->msg_type   = 'txt';

		// //NOTE - date format date("Y-m-d H:i:s");
		// $myobj->time  = date("Y-m-d H:i:s");
		$date = JFactory::getDate();
		$myobj->time = $date->toSql();

		// @TODO need to chk if this is really used or is it for future proofing
		$myobj->sent       = 1;
		$db->insertObject('#__jbolo_chat_msgs', $myobj);

		// Get last insert id
		$new_mid = $db->insertid();

		// Update msg xref table
		if ($new_mid)
		{
			$count = count($participants);

			// Add entry for all users against this msg
			for ($i = 0; $i < $count; $i++)
			{
				$xrefMsgObj             = new stdclass;
				$xrefMsgObj->msg_id     = $new_mid;
				$xrefMsgObj->node_id    = $nid;
				$xrefMsgObj->to_user_id = $participants[$i];
				$xrefMsgObj->delivered  = 0;
				$xrefMsgObj->read       = 0;
				$db->insertObject('#__jbolo_chat_msgs_xref', $xrefMsgObj);
			}
		}

		// Prepare json response
		$query = "SELECT chm.to_node_id AS nid, chm.from AS uid, chm.msg_id AS mid, chm.sent, chm.msg
	 	FROM #__jbolo_chat_msgs AS chm
	 	WHERE chm.msg_id=" . $new_mid;
		$db->setQuery($query);
		$node_d                                 = $db->loadObject();

		// $pushChat_response['pushChat_response'] = $node_d;

		// Add this msg to session
		$query = "SELECT m.msg_id AS mid, m.from AS fid, m.msg, m.time AS ts
		FROM #__jbolo_chat_msgs AS m
		LEFT JOIN #__jbolo_chat_msgs_xref AS mx ON mx.msg_id=m.msg_id
		WHERE m.msg_id=" . $new_mid . " AND m.sent=1";
		$db->setQuery($query);
		$msg_dt     = $db->loadObject();

		// $msg_dt->ts = JFactory::getDate($msg_dt->ts)->Format(JText::_('COM_JBOLO_SENT_AT_FORMAT'));
		$msg_dt->ts = JHtml::date($msg_dt->ts, JText::_('COM_JBOLO_SENT_AT_FORMAT'), true);

		// Assign above date format for json reponse as well
		$node_d->ts = $msg_dt->ts;
		$pushChat_response['pushChat_response'] = $node_d;

		// Update session by adding this msg against corresponding node
		// If jbolo nodes array is set
		if (isset($_SESSION['jbolo']['nodes']))
		{
			// Count nodes in session
			$nodecount = count($_SESSION['jbolo']['nodes']);

			// Loop through all nodes
			for ($k = 0; $k < $nodecount; $k++)
			{
				// If k'th node is set
				if (isset($_SESSION['jbolo']['nodes'][$k]))
				{
					// If nodeinfo is set
					if (isset($_SESSION['jbolo']['nodes'][$k]['nodeinfo']))
					{
						// If the required node is found in session
						if ($_SESSION['jbolo']['nodes'][$k]['nodeinfo']->nid == $nid)
						{
							// Initialize mesasge count for node found to 0
							$mcnt = 0;

							// Check if the node found has messages stored in session
							if (isset($_SESSION['jbolo']['nodes'][$k]['messages']))
							{
								// If yes count msgs
								$mcnt = count($_SESSION['jbolo']['nodes'][$k]['messages']);

								// Add new mesage at the end
								$_SESSION['jbolo']['nodes'][$k]['messages'][$mcnt] = $msg_dt;
							}
							else
							{
								// Add new mesage at the start
								$_SESSION['jbolo']['nodes'][$k]['messages'][0] = $msg_dt;
							}
						}
					}
				}
				else
				{
					// If node is not present in session
					// This situation is not expected ideally
				}
			}
		}

		/*
		$logf = JPATH_SITE . '/components/com_jbolo/' . $uid . '.php';
		$f = @fopen($logf, 'a+');

		if ($f)
		{
			$log = "\n" . 'PUSH CHAT' . "\n";

			if (isset($_SESSION['jbolo']))
			{
				$log .= print_r($_SESSION['jbolo'], true);
			}

			$log .= print_r($pushChat_response, true);

			@fclose($f);
		}
		*/

		// Start onJboloAfterSendMessage
		// Trigger plugins to process message text
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('jbolo', 'plg_jbolo_textprocessing');

		// Sync messages
		// Pass orignal unprocessed message for trigger and sync
		$myobj->msg = $originalMsg;

		// Get nodeinfo
		$node = $nodesHelper->getNodeInfo($nid);

		if ($node)
		{
			// Plugin trigger: onJboloAfterSendMessage
			// Pass msg object, node details, participants
			$triggerResultArray = $dispatcher->trigger('onJboloAfterSendMessage', array($msg = $myobj, $node, $participants));
		}

		return $pushChat_response;
	}

	/**
	 * This function =>
	 * - adds an entry for a user in jbolo users table if it's not there
	 * - returns array of info about logged in user & other logged in users
	 * - it also returns info about nodes as it is from session
	 * - echoes json reponse
	 * -- It takes these as POST inputs
	 * -- uid:777 / ts:1355985190
	 * Outputs json array -
	 * -- nodes:
	 * --- 0:
	 * ---- messages:
	 * ------ 0: {mid:11, fid:776, msg:11, ts:2012-12-20 12:48:41}
	 * ------ 1: {mid:12, fid:776, msg:2, ts:2012-12-20 12:48:42}
	 * ------ 2: {mid:13, fid:776, msg:2, ts:2012-12-20 12:48:42}
	 * ---- nodeinfo: {nid:42, uid:777, tid:776}
	 * ---- participants:
	 * ------ 776: {uid:776, uname:admin, name:Super User, sts:1, stsm:Chatting here first time,…}
	 * ------ 777: {uid:777, uname:user1, name:user1, sts:1, stsm:Chatting here first time,…}
	 * ---- userlist:
	 * ------ me: {uid:777, uname:user1, name:user1, sts:1, stsm:Chatting here first time,…}
	 * ------ users:
	 * ------- 0: {uid:776, uname:admin, name:Super User, sts:1, stsm:Chatting here first time,…}
	 *
	 * @return  array
	 *
	 * @since  3.0
	 **/
	public function polling()
	{
		// Validate user
		$polling = $this->validateUserLogin();
		$polling['nodes'] = array();

		$db   = JFactory::getDbo();
		$user = JFactory::getUser();
		$uid  = $user->id;

		// Log polling

		/*if ($uid == 63)
		{
			$logf = JPATH_SITE . '/components/com_jbolo/' . $uid . '.php';
			$f    = @fopen($logf, 'a+');

			if ($f)
			{
				$log = "";

				if (isset($_SESSION['jbolo']))
				{
					$log .= "\n" . 'POLLING START session o/p' . "\n";
					$log .= print_r($_SESSION['jbolo'], true);
				}

				@fwrite($f, $log);
				@fclose($f);
			}
		}*/

		// Get logged in user information
		// Use  users helper
		$jbolousersHelper          = new jbolousersHelper;
		$u_data                    = $jbolousersHelper->getLoggedinUserInfo($uid);
		$polling['userlist']['me'] = $u_data;

		// Generate online user list
		// Use  users helper
		$data                         = $jbolousersHelper->getOnlineUsersInfo($uid);
		$polling['userlist']['users'] = $data;

		// Get nodeinfo for all nodes for logged in user
		// Use  nodes helper
		$nodesHelper = new nodesHelper;
		$nodes       = $nodesHelper->getActiveChatNodes($uid);

		/*
		$nodes:
		Array
		(
			[0] => stdClass Object
			(
				[nid] => 22
				[ctyp] => 2
			)
			[1] => stdClass Object
			(
				[nid] => 23
				[ctyp] => 1
			)
		)
		*/

		// For each node get participants and unread messages for this user
		for ($nc = 0; $nc < count($nodes); $nc++)
		{
			// To get msg data
			$messages = array();

			// Get unread messaged for this node
			$messages        = $this->getUnreadMessages($nodes[$nc]->nid, $uid);
			$unreadMsgsCount = count($messages);

			if ($unreadMsgsCount)
			{
				// Get node participants info
				$participants = $nodesHelper->getNodeParticipants($nodes[$nc]->nid, $uid);

				// Get Title for chat window
				$nodes[$nc]->wt = $nodesHelper->getNodeTitle($nodes[$nc]->nid, $uid, $nodes[$nc]->ctyp);

				// Get chatbox status
				$nodes[$nc]->ns = $nodesHelper->getNodeStatus($nodes[$nc]->nid, $uid, $nodes[$nc]->ctyp);

				// Prepare json output for nodeinfo, Modify nodeinfo
				// Push modified node info into output array
				$polling['nodes'][$nc]['nodeinfo'] = $nodes[$nc];

				// Prepare json output for node participants
				$polling['nodes'][$nc]['participants'] = $participants['participants'];

				// Prepare json output for unread messages
				$polling['nodes'][$nc]['messages'] = $messages;

				// Update chat xref table by setting read to 1 afer pushing a unread msg to session
				// Also set timestamp for each msg as per date format
				for ($k = 0; $k < $unreadMsgsCount; $k++)
				{
					// Set timestamp
					$messages[$k]->ts = JHtml::date($messages[$k]->ts, JText::_('COM_JBOLO_SENT_AT_FORMAT'), true);

					// Update xref entry
					$msg_id = $messages[$k]->mid;
					$db     = JFactory::getDbo();
					$query  = "UPDATE #__jbolo_chat_msgs_xref AS x SET x.read=1
					 WHERE x.read=0
					 AND x.to_user_id=" . $uid . "
					 AND x.msg_id=" . $msg_id;
					$db->setQuery($query);

					if (!$db->execute())
					{
						echo $db->stderr();
					}
				}

				// Add msgs to session for particular node
				// If nodes array is set
				if (isset($_SESSION['jbolo']['nodes']))
				{
					$node_ids  = array();
					$nodecount = count($_SESSION['jbolo']['nodes']);

					// Get all node ids for nodes which are present in session
					for ($d = 0; $d < $nodecount; $d++)
					{
						if (isset($_SESSION['jbolo']['nodes'][$d]))
						{
							$node_ids[$d] = $_SESSION['jbolo']['nodes'][$d]['nodeinfo']->nid;
						}
					}

					// If current node is not in session, add nodeinfo & participants in session
					if (!in_array($nodes[$nc]->nid, $node_ids))
					{
						// If node data not in session, push new nodedata at end of array
						if ($nodecount)
						{
							// Push nodeinfo
							$_SESSION['jbolo']['nodes'][$nodecount]['nodeinfo'] = $nodes[$nc];

							// Push node participants
							$_SESSION['jbolo']['nodes'][$nodecount]['participants'] = $participants['participants'];
						}
						// If no node is present in session - Add a new node in session at 0th index
						else
						{
							// Push nodeinfo
							$_SESSION['jbolo']['nodes'][0]['nodeinfo'] = $nodes[$nc];

							// Push node participants
							$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
						}
					}

					// Loop through all nodes to push new messages in session
					$newNodeCount = count($_SESSION['jbolo']['nodes']);

					for ($k = 0; $k < $newNodeCount; $k++)
					{
						// If current node in consideration is found in session
						if ($_SESSION['jbolo']['nodes'][$k]['nodeinfo']->nid == $nodes[$nc]->nid)
						{
							// This is important
							// Update node participants
							$_SESSION['jbolo']['nodes'][$k]['participants'] = $participants['participants'];

							// Initialize mesasge count for node found to 0
							$oldMsgsCount = 0;

							// Check if the node found has messages stored in session
							if (isset($_SESSION['jbolo']['nodes'][$k]['messages']))
							{
								// If yes count msgs
								$oldMsgsCount = count($_SESSION['jbolo']['nodes'][$k]['messages']);
							}

							for ($m = 0; $m < $unreadMsgsCount; $m++)
							{
								// Add new mesage at the end
								// Changed
								$_SESSION['jbolo']['nodes'][$k]['messages'][$oldMsgsCount] = $messages[$m];

								// Increase message count for messages in session for current node
								$oldMsgsCount++;
							}
						}
					}
				}
				else
				{
					/**
					 * If no node is present in session
					 * Add a new node in session
					 * Push nodeinfo
					 **/
					$_SESSION['jbolo']['nodes'][0]['nodeinfo'] = $nodes[$nc];

					// Push node participants
					$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];

					// Push unread messages
					$_SESSION['jbolo']['nodes'][0]['messages'] = $messages;

					/*$mcnt = 0;

					for ($m = 0; $m < $unreadMsgsCount; $m++)
					{
						$_SESSION['jbolo']['nodes'][0]['messages'][$mcnt] = $messages[$m];
						$mcnt++;
					}*/
				}
			}
		}

		// Check if nodes array is present in session
		if (isset($_SESSION['jbolo']['nodes']))
		{
			// Use  nodes helper
			$nodeStatusArray = $nodesHelper->getNodeStatusArray($_SESSION['jbolo']['nodes'], $uid);
			$polling['nsts'] = $nodeStatusArray;
		}
		else
		{
			$polling['nsts'] = array();
		}

		// Log polling

		/*if ($uid == 63)
		{
			$logf = JPATH_SITE . '/components/com_jbolo/' . $uid . '.php';
			$f    = @fopen($logf, 'a+');

			if ($f)
			{
				$log = "";

				if (isset($_SESSION['jbolo']))
				{
					$log .= "\n" . 'POLLING END session o/p' . "\n";
					$log .= print_r($_SESSION['jbolo'], true);
					$log .= "\n" . '////////////////////////////' . "\n";
				}

				@fwrite($f, $log);
				@fclose($f);
			}
		}*/

		return $polling;
	}

	/**
	 * Get all unread messages for given user against given node
	 *
	 * @param   int  $nid  Node id
	 * @param   int  $uid  User id
	 *
	 * @return  object   List of unread messages
	 */
	public function getUnreadMessages($nid, $uid)
	{
		$db = JFactory::getDbo();

		// Get all unread messages against current node for this user
		$query = "SELECT m.msg_id AS mid,m.from AS fid, m.msg, m.time AS ts
		FROM #__jbolo_chat_msgs AS m
		LEFT JOIN #__jbolo_chat_msgs_xref AS mx ON mx.msg_id=m.msg_id
		WHERE m.to_node_id=" . $nid . "
		AND mx.to_user_id =" . $uid . "
		AND mx.read = 0
		ORDER BY m.msg_id ";
		$db->setQuery($query);
		$messages = $db->loadObjectList();

		return $messages;
	}

	/**
	 * This Clears all chat messages stored in session against given node id
	 * -- It takes these as POST inputs
	 * -- nid:38
	 * -- It outputs
	 * -- 0 indicates - messages not found / not deleted
	 * -- 1 indicates - messages deleted
	 *
	 * @return  int  0 or 1
	 *
	 * @since  3.0
	 */
	public function clearchat()
	{
		// Validate user
		$response = $this->validateUserLogin();

		$input = JFactory::getApplication()->input;
		$post  = $input->post;
		$nid   = $input->post->get('nid', '', 'INT');

		// Validate if nid is not specified
		if (!$nid)
		{
			$response['validate']->error     = 1;
			$response['validate']->error_msg = JText::_('COM_JBOLO_INVALID_NODE_ID');

			return $response;
		}

		$nodecount = count($_SESSION['jbolo']['nodes']);
		$flag      = 0;

		for ($d = 0; $d < $nodecount; $d++)
		{
			// Loop till required node is found
			if (!$flag)
			{
				if (isset($_SESSION['jbolo']['nodes'][$d]))
				{
					// If node found
					if ($_SESSION['jbolo']['nodes'][$d]['nodeinfo']->nid == $nid)
					{
						// Check if messages are present
						if (isset($_SESSION['jbolo']['nodes'][$d]['messages']))
						{
							// Unset messages array from session
							unset($_SESSION['jbolo']['nodes'][$d]['messages']);
						}

						$flag = 1;
					}
				}
			}
			else
			{
				break;
			}
		}

		$response['all_clear'] = $flag;

		return $response;
	}

	/**
	 * This function =>
	 * - is for group chat
	 * - adds a new user to current node for group chat
	 * - echoes json reponse
	 *
	 * -- It takes these as POST inputs
	 * -- when added from 1to1 chat
	 * --	nid:1_1 / pid:778
	 * -- when added from group chat
	 * --	nid:2_2 / pid:779
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function addNodeUser()
	{
		// Validate user
		$ini_node = $this->validateUserLogin();

		// Get iputs from posted data
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		// 10_1 OR 15_2
		$nid   = $input->post->get('nid', '', 'STRING');

		// Get nid
		$pieces = explode("_", $nid);

		// 10 OR 15
		$nid    = $pieces[0];
		$pid    = $input->post->get('pid', '', 'INT');

		// Validate if nid is not specified
		if (!$nid)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_NODE_ID');

			return $ini_node;
		}

		// Validate if pid is not specified
		if (!$pid)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_PARTICIPANT');

			return $ini_node;
		}

		$db   = JFactory::getDbo();
		$user = JFactory::getUser();
		$uid  = $user->id;

		// Check that logged in user has permission to create group chat
		if (!JFactory::getUser($uid)->authorise('core.group_chat', 'com_jbolo'))
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_USER_NOT_PERMITED_TO_START_GROUP_CHAT');

			return $ini_node;
		}

		// Check that logged in user has permission to add member in group chat
		if (!JFactory::getUser($uid)->authorise('core.add_member_in_group_chat', 'com_jbolo'))
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_USER_NOT_PERMITED_ADD_MEMBER_IN_GROUP');

			return $ini_node;
		}

		$params       = JComponentHelper::getParams('com_jbolo');
		$maxChatUsers = $params->get('maxChatUsers');

		// Use  nodes helper
		$nodesHelper = new nodesHelper;

		// Validate max allowed users for group chat
		$activeNodeParticipantsCount = $nodesHelper->getActiveNodeParticipantsCount($nid);

		if ($activeNodeParticipantsCount >= $maxChatUsers)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_GC_MAX_USERS_LIMIT');

			return $ini_node;
		}

		// Validate if this user is participant of this node
		$isNodeParticipant = $nodesHelper->isNodeParticipant($uid, $nid);

		// Error handling for inactive user
		if ($isNodeParticipant == 2)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INACTIVE_MEMBER_MSG');

			return $ini_node;
		}

		// Error handling for not member/unauthorized access to this group chat
		if (!$isNodeParticipant)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_NON_MEMBER_MSG');

			return $ini_node;
		}

		// Get node type
		// Important
		$nodeType = $nodesHelper->getNodeType($nid);

		// If adding a new user to 1to1 chat
		if ($nodeType == 1)
		{
			// Create a new node for this group chat
			$myobj        = new stdclass;
			$myobj->title = null;
			$myobj->type  = 2;
			$myobj->owner = $uid;

			// //NOTE - date format date("Y-m-d H:i:s");
			// $myobj->time  = date("Y-m-d H:i:s");
			$date = JFactory::getDate();
			$myobj->time = $date->toSql();

			$db->insertObject('#__jbolo_nodes', $myobj);
			$db->stderr();
			$new_node_id = $db->insertid();

			// When new node is created
			if ($new_node_id)
			{
				// Get old node users
				$query = "SELECT nu.user_id
				FROM #__jbolo_node_users AS nu
				LEFT JOIN #__jbolo_users as ju ON nu.user_id = ju.user_id
				WHERE nu.node_id=" . $nid . "
				AND ju.state > 0 ";
				$db->setQuery($query);
				$old_node_users          = $db->loadColumn();
				$first_one2one_chat_user = '';

				// Add participants from old node(i.e. current 1to1 node) to newly created group chat node
				for ($i = 0; $i < count($old_node_users); $i++)
				{
					// Check that older means one to one chat user has permission to chat in group chat & if yes then only add this user in group chat
					if (JFactory::getUser($old_node_users[$i])->authorise('core.group_chat', 'com_jbolo'))
					{
						$myobj          = new stdclass;
						$myobj->node_id = $new_node_id;
						$myobj->user_id = $old_node_users[$i];
						$myobj->status  = 1;
						$db->insertObject('#__jbolo_node_users', $myobj);

						if ($uid != $myobj->user_id)
						{
							$first_one2one_chat_user = $myobj->user_id;
						}
					}
				}

				// After adding existing users from 1to1 chat, add new user to new node
				$myobj          = new stdclass;
				$myobj->node_id = $new_node_id;
				$myobj->user_id = $pid;
				$myobj->status  = 1;
				$db->insertObject('#__jbolo_node_users', $myobj);

				// Push welcome messages for actor and others
				$this->pushWelcomeMsgBroadcast('gbc', $new_node_id, $uid, 0, 1);

				// Push invited messages for actor and others
				// Invited first user
				if (!empty($first_one2one_chat_user))
				{
					$this->pushInvitedMsgBroadcast('gbc', $new_node_id, $uid, $first_one2one_chat_user, 0, 1);
				}

				// The added user
				$this->pushInvitedMsgBroadcast('gbc', $new_node_id, $uid, $pid, 0, 1);

				// Push who has joined messages to actor and others
				$this->pushJoinedMsgBroadcast('gbc', $new_node_id, $uid, 0, 1);
			}
		}
		// Called from group chat
		elseif ($nodeType == 2)
		{
			// Check if curent node is social group chat node.
			$socialGroupId = $nodesHelper->isSocialGroupChatNode($nid);

			// If no existing node found, create new node.
			if ($socialGroupId)
			{
				// Get social group chat node 'client'
				$client = $nodesHelper->getSocialGroupClient($nid);

				$integrationsHelper = new integrationsHelper;

				// Check if particpant being added is part of social group
				$isSocialGroupUser = $integrationsHelper->isSocialGroupUser($socialGroupId, $pid, $client);

				// If no social group user
				if (!$isSocialGroupUser)
				{
					$pushChat_response['validate']->error     = 1;
					$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_GC_PARTICPANT_NON_GROUP_MEMBER_MSG');

					return $pushChat_response;
				}
			}

			// Check if user being added is already participant
			$isNodeParticipant = $nodesHelper->isNodeParticipant($pid, $nid);

			// @TODO chk /test
			$new_node_id       = $nid;

			if (!$isNodeParticipant)
			{
				// After adding existing users from 1to1 chat add new user
				$myobj          = new stdclass;
				$myobj->node_id = $nid;
				$myobj->user_id = $pid;
				$myobj->status  = 1;
				$db->insertObject('#__jbolo_node_users', $myobj);

				// Push welcome message only to newly added user
				$particularUID = $pid;
				$this->pushWelcomeMsgBroadcast('gbc', $new_node_id, $uid, $particularUID, 0);

				// Push invited messages for actor and others
				// The added user
				$this->pushInvitedMsgBroadcast('gbc', $new_node_id, $uid, $pid, 0, 1);

				// Push who has joined messages to actor and others
				$this->pushJoinedMsgBroadcast('gbc', $new_node_id, $uid, $particularUID, 0);
			}
			// Re-adding user
			elseif ($isNodeParticipant == 2)
			{
				// Re-add existing user
				$query = "UPDATE #__jbolo_node_users
				SET status=1
				WHERE node_id=" . $nid . "
				AND user_id=" . $pid . "
				AND status=0";
				$db->setQuery($query);
				$db->execute();

				// Use  broadcast helper
				$chatBroadcastHelper = new chatBroadcastHelper;

				// Push welcome message only to newly added user
				$particularUID = $pid;
				$this->pushWelcomeMsgBroadcast('gbc', $new_node_id, $uid, $particularUID, 0);

				// Push invited messages for actor and others
				// The added user
				$this->pushInvitedMsgBroadcast('gbc', $new_node_id, $uid, $pid, 0, 1);

				// Push who has joined messages to actor and others
				$this->pushJoinedMsgBroadcast('gbc', $new_node_id, $uid, $particularUID, 0);
			}
		}

		$query = "SELECT node_id AS nid, type AS ctyp
		FROM #__jbolo_nodes
		WHERE node_id=" . $new_node_id;
		$db->setQuery($query);
		$node_d = $db->loadObject();

		$ini_node['nodeinfo']     = $node_d;
		$ini_node['nodeinfo']->wt = $nodesHelper->getNodeTitle($new_node_id, $pid, $ini_node['nodeinfo']->ctyp);

		// Get chatbox status
		$ini_node['nodeinfo']->ns = $nodesHelper->getNodeStatus($ini_node['nodeinfo']->nid, $pid, $ini_node['nodeinfo']->ctyp);
		$user                     = JFactory::getUser($pid);

		// Add node data to session
		$d = 0;

		// Get node participants info
		$participants = $nodesHelper->getNodeParticipants($new_node_id, $pid);

		// Prepare json output for node participants
		// Check if node array is set
		if (isset($_SESSION['jbolo']['nodes']))
		{
			$node_ids = array();

			for ($d = 0; $d < count($_SESSION['jbolo']['nodes']); $d++)
			{
				$node_info['nodeinfo'] = array();

				if (isset($_SESSION['jbolo']['nodes'][$d]))
				{
					// $node_info['nodeinfo']=$_SESSION['jbolo']['nodes'][$d]['nodeinfo'];
					// Get all node ids set in session
					$node_ids[$d] = $_SESSION['jbolo']['nodes'][$d]['nodeinfo']->nid;
				}
			}

			// If entry for node found in session, update it
			if (in_array($ini_node['nodeinfo']->nid, $node_ids))
			{
				// @TODO blackhole as of 0.2 beta
				// Might be needed when user is added from GC

				// Set session[jbolo][nodes][nid][nodeinfo] = ini_node[nodeinfo]
				// $_SESSION['jbolo']['nodes'][$ini_node['nodeinfo']->nid]['nodeinfo'] = $ini_node['nodeinfo'];

				// Push node participants; Not sure if this needed?
				// $_SESSION['jbolo']['nodes'][$d]['participants'] = $participants['participants'];
			}
			// @TODO else part need to check
			else
			{
				// If node data not session, push new nodedata at end
				{
					$_SESSION['jbolo']['nodes'][$d]['nodeinfo'] = $ini_node['nodeinfo'];

					// Push node participants
					// ??
					$_SESSION['jbolo']['nodes'][$d]['participants'] = $participants['participants'];
				}
			}
		}
		else
		{
			// If nodes array is not set, push new node at start
			{
				// @TODO 0 or $d here?
				$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];

				// ??
				$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
			}
		}

		return $ini_node;
	}

	/**
	 * Pushes welcome to group chat message to new group members.
	 *
	 * @param   string  $msgType        Chat Message type
	 * @param   int     $nid            Node id
	 * @param   int     $uid            User id
	 * @param   int     $particularUID  If msg is to be sent to particular user that - user id
	 * @param   int     $sendToActor    If msg is to be sent to actor himself
	 *
	 * @return  boolean
	 *
	 * @since  3.0
	 */
	public function pushWelcomeMsgBroadcast($msgType, $nid, $uid, $particularUID = 0, $sendToActor = 0)
	{
		// Use  broadcast helper
		$chatBroadcastHelper = new chatBroadcastHelper;
		$msg                 = JText::_('COM_JBOLO_GC_BC_WELCOME_MSG');
		$chatBroadcastHelper->pushChat($msgType, $nid, $msg, $particularUID, $sendToActor);

		return true;
	}

	/**
	 * Pushes invited to group chat message to new group members.
	 *
	 * @param   string  $msgType        Chat Message type
	 * @param   int     $nid            Node id
	 * @param   int     $uid            User id
	 * @param   int     $pid            Particpiant id
	 * @param   int     $particularUID  If msg is to be sent to particular user that - user id
	 * @param   int     $sendToActor    If msg is to be sent to actor himself
	 *
	 * @return  boolean
	 *
	 * @since  3.0
	 */
	public function pushInvitedMsgBroadcast($msgType, $nid, $uid, $pid, $particularUID = 0, $sendToActor = 0)
	{
		// Use broadcast helper
		$chatBroadcastHelper = new chatBroadcastHelper;
		$params              = JComponentHelper::getParams('com_jbolo');

		// Show   username OR name
		if ($params->get('chatusertitle'))
		{
			$msg = JFactory::getUser($uid)->username . ' <i>' . JText::_('COM_JBOLO_GC_INVITED') . ' </i>' . JFactory::getUser($pid)->username;
		}
		else
		{
			$msg = JFactory::getUser($uid)->name . ' <i>' . JText::_('COM_JBOLO_GC_INVITED') . ' </i>' . JFactory::getUser($pid)->name;
		}

		$chatBroadcastHelper->pushChat($msgType, $nid, $msg, $particularUID, $sendToActor);

		return true;
	}

	/**
	 * Pushes New user joined group chat broadcast message to new group members.
	 *
	 * @param   string  $msgType        Chat Message type
	 * @param   int     $nid            Node id
	 * @param   int     $uid            User id
	 * @param   int     $particularUID  If msg is to be sent to particular user that - user id
	 * @param   int     $sendToActor    If msg is to be sent to actor himself
	 *
	 * @return  boolean
	 *
	 * @since  3.0
	 */
	public function pushJoinedMsgBroadcast($msgType, $nid, $uid, $particularUID = 0, $sendToActor = 0)
	{
		// Use  nodes helper
		$nodesHelper  = new nodesHelper;
		$participants = $nodesHelper->getActiveNodeParticipants($nid);

		// Use broadcast helper
		$chatBroadcastHelper = new chatBroadcastHelper;
		$msg                 = "";

		foreach ($participants as $p)
		{
			$msg .= $p->name . ' <i>' . JText::_('COM_JBOLO_GC_JOINED') . "</i><br/>";

			// $particularUID = $pid;
		}

		$chatBroadcastHelper->pushChat($msgType, $nid, $msg, $particularUID, $sendToActor);

		return true;
	}

	/**
	 * This function -
	 * -- changes chat status & chat status message
	 *
	 * -- It takes these as POST inputs
	 * -- chat_sts:2 / stsm:hola chica
	 * -- NOTE - chat stsm is expectd all times but sts is not
	 * --  Output
	 *
	 * - Outputs change_sts_response - @json
	 * -- 0 indicates - error / unauthorized
	 * -- 1 indicates - success
	 *
	 * @return  array
	 *
	 * @since  3.0
	 **/
	public function changeStatus()
	{
		// Validate user
		$response = $this->validateUserLogin();

		$db   = JFactory::getDbo();
		$user = JFactory::getUser();
		$uid  = $user->id;

		// Get inputs from posted data
		$input = JFactory::getApplication()->input;
		$sts   = $input->post->get('sts', '', 'INT');

		// $stsm=$input->post->get('stsm','','STRING');

		if (get_magic_quotes_gpc())
		{
			$stsm = stripslashes($input->post->get('stsm', '', 'STRING'));
		}
		else
		{
			$stsm = $input->post->get('stsm', '', 'STRING');
		}

		// Validate status
		if ($sts >= 5 || $sts < 0)
		{
			$response['validate']->error     = 1;
			$response['validate']->error_msg = JText::_('COM_JBOLO_INVALID_STATUS');

			return $response;
		}

		// Validate status - check if it contains allowed values
		// Strip tags from status message
		$stsm = strip_tags($stsm);

		// Outer double quotes are imp here
		$query = 'UPDATE #__jbolo_users SET status_msg="' . $stsm . '"';

		if ($sts)
		{
			// Update chat sts only if it is there in posted data
			$query .= " , chat_status=" . $sts;
		}

		$query .= " WHERE user_id=" . $uid;
		$db->setQuery($query);

		// Error updating
		if (!$db->execute())
		{
			$response['validate']->error     = 1;
			$response['validate']->error_msg = JText::_('COM_JBOLO_DB_ERROR');
		}
		else
		{
			// Updated successfully
			$change_sts_response['change_sts_response'] = 1;
		}

		return $response;
	}

	/**
	 * This function -
	 * -- lets a user leave a group chat node
	 *
	 * -- It takes these as POST inputs
	 * -- nid:9
	 *
	 * - Outputs change_sts_response - @json
	 * - $ini_node - @json array
	 *
	 * @return  array
	 *
	 * @since  3.0
	 **/
	public function leaveChat()
	{
		// Validate user
		$leaveChat_response = $this->validateUserLogin();

		$db      = JFactory::getDbo();
		$user    = JFactory::getUser();
		$actorid = $user->id;

		// Get node id
		$input = JFactory::getApplication()->input;

		// E.g. 9
		$nid   = $input->post->get('nid', '', 'INT');

		// Validate participant id - check if pid is specified
		if (!$nid)
		{
			$leaveChat_response['validate']->error     = 1;
			$leaveChat_response['validate']->error_msg = JText::_('COM_JBOLO_INVALID_NODE_ID');

			return $leaveChat_response;
		}

		$leaveChat_response = $this->validateNodeParticipant($actorid, $nid);

		// Use  nodes helper
		$nodesHelper = new nodesHelper;

		// Important Coz only group chat can be left
		$nodeType    = $nodesHelper->getNodeType($nid);

		// Called from group chat
		if ($nodeType == 2)
		{
			// Mark user as inactive for this group chat
			$query = "UPDATE #__jbolo_node_users
			SET status=0
			WHERE node_id=" . $nid . "
			AND user_id=" . $actorid;
			$db->setQuery($query);

			if (!$db->execute())
			{
				echo $db->stderr();

				return false;
			}

			// Broadcast msg
			$params = JComponentHelper::getParams('com_jbolo');

			// Show   username OR name
			if ($params->get('chatusertitle'))
			{
				$broadcast_msg = JFactory::getUser($actorid)->username . ' <i>' . JText::_('COM_JBOLO_GC_LEFT_CHAT_MSG') . '</i>';
			}
			else
			{
				$broadcast_msg = JFactory::getUser($actorid)->name . ' <i>' . JText::_('COM_JBOLO_GC_LEFT_CHAT_MSG') . '</i>';
			}

			// Use broadcast helper
			$chatBroadcastHelper = new chatBroadcastHelper;

			// Send to one who left chat
			$chatBroadcastHelper->pushChat('gbc', $nid, $broadcast_msg, $actorid, 0);

			// Send to all
			$chatBroadcastHelper->pushChat('gbc', $nid, $broadcast_msg, 0, 0);

			// Set message to be sent back to ajax request
			$leaveChat_response['lcresponse']->msg = JText::_('COM_JBOLO_YOU') . ' ' . JText::_('COM_JBOLO_GC_LEFT_CHAT_MSG');

			return $leaveChat_response;
		}
	}

	/**
	 * This returns a list of online users based on the search string input.
	 *
	 * -- It takes these as POST inputs
	 * -- filterText:manoj
	 *
	 * - Outputs response - @json
	 * - $response - @json array
	 *
	 * @return  array
	 *
	 * @since  3.0
	 **/
	public function getAutoCompleteUserList()
	{
		// Validate user
		$response = $this->validateUserLogin();

		$db   = JFactory::getDbo();
		$user = JFactory::getUser();
		$uid  = $user->id;

		$input      = JFactory::getApplication()->input;

		// E.g. user
		$filterText = $input->post->get('filterText', '', 'STRING');

		// Validate if filterText is not blank
		if (!$filterText)
		{
			$response['validate']->error     = 1;
			$response['validate']->error_msg = JText::_('COM_JBOLO_EMPTY_SEARCH_STRING');

			return $response;
		}

		// Addslashes, user might enter anything to search
		$filterText = addslashes($filterText);

		// Generate online user list
		// Use  users helper
		$jbolousersHelper              = new jbolousersHelper;
		$data                          = $jbolousersHelper->getAutoCompleteUserList($uid, $filterText);
		$response['userlist']['users'] = $data;

		return $response;
	}

	/**
	 * This function -
	 * -- returns a comma separated list of nodes between for given 2 users
	 *
	 * This accepts these POST INPUT VARIABLES
	 * -- onuser:776
	 * -- offuser:778
	 *
	 * -- This outputs @JSON
	 * {"nodes":"40,41"}
	 * OR
	 * {"error":"No nodes found"}
	 *
	 * @return  string
	 *
	 * @since  3.0
	 */
	public function getUserNodes()
	{
		$input   = JFactory::getApplication()->input;
		$onuser  = $input->post->get('onuser', '', 'INT');
		$offuser = $input->post->get('offuser', '', 'INT');

		$db = JFactory::getDbo();

		// Check if a node already exists for these 2 users
		$query = "SELECT nu.node_id AS nid, GROUP_CONCAT(nu.user_id) AS users
		FROM `#__jbolo_node_users` AS nu
		LEFT JOIN `#__jbolo_nodes` AS nd ON nd.node_id = nu.node_id
		WHERE nu.node_id IN
		(
			SELECT nd.node_id
			FROM `#__jbolo_nodes` AS nd
			WHERE nd.TYPE =1
			AND (nd.owner=" . $onuser . " OR nd.owner=" . $offuser . ")
		)
		GROUP BY nu.node_id";
		$db->setQuery($query);
		$data = $db->loadObjectList();

		// Important
		$node_id_found = null;

		// 776,778
		$users1        = $onuser . "," . $offuser;

		// 778,776
		$users2        = $offuser . "," . $onuser;
		$flag          = 0;
		$count         = count($data);

		for ($i = 0; $i < $count; $i++)
		{
			// If node found for 2 users, append to string
			if ($data[$i]->users == $users1 || $data[$i]->users == $users2)
			{
				if (!$flag)
				{
					$flag = 1;
					$node_id_found .= $data[$i]->nid;
				}
				else
				{
					$node_id_found .= ',' . $data[$i]->nid;
				}
			}
		}

		return $node_id_found;
	}

	/**
	 * This deletes chat messages and files sent during the duration
	 * [today - days set in config] days
	 *
	 * @return  boolean
	 */
	public function purgeChats()
	{
		$params    = JComponentHelper::getParams('com_jbolo');
		$purge     = $params->get('enable_purge');
		$input     = JFactory::getApplication()->input;
		$purge_key = $input->get->get('purge_key', '', 'STRING');

		if ($purge)
		{
			if ($params->get('purge_key') == $purge_key)
			{
				if (!(int) $params->get('purge_days'))
				{
					echo "<div class='alert alert-error'>" .
					JText::_('COM_JBOLO_PURGE_NON_ZERO_DAYS') .
					"</div>";
				}
				else
				{
					$db = JFactory::getDbo();

					// First delete xref table entries
					$query = "DELETE FROM #__jbolo_chat_msgs_xref
					 WHERE msg_id IN (
						 SELECT msg_id
						 FROM #__jbolo_chat_msgs
						 WHERE DATEDIFF('" . date('Y-m-d') . "',time) >=" . $params->get('purge_days') .
					")";
					$db->setQuery($query);

					if (!$db->execute($query))
					{
						echo $db->stderr();

						return false;
					}

					// Then delete main chat entires
					$query = "DELETE FROM #__jbolo_chat_msgs
					 WHERE DATEDIFF('" . date('Y-m-d') . "',time) >=" . $params->get('purge_days');
					$db->setQuery($query);

					if (!$db->execute($query))
					{
						echo $db->stderr();

						return false;
					}

					echo "<div class='alert alert-success'>" .
					JText::_('COM_JBOLO_PURGE_OLD_CHATS') . "<br/>" .
					"</div>";

					// Call purge files function
					$this->purgefiles();
				}
			}
		}

		return true;
	}

	/**
	 * This deletes files older than duration set in the config
	 * Also, echoes html output file deltion success / failure.
	 *
	 * @return  void
	 *
	 * @since  3.0
	 */
	private function purgefiles()
	{
		$params        = JComponentHelper::getParams('com_jbolo');
		$uploaddir     = JPATH_COMPONENT . DS . 'uploads';
		$exp_file_time = 3600 * 24 * $params->get('purge_days');
		$currts        = time();
		echo "<div class='alert alert-success'>" .
		JText::_('COM_JBOLO_PURGE_OLD_FILES');

		/*
		 * JFolder::files($path,$filter,$recurse=false,$full=false,$exclude=array('.svn'),$excludefilter=array('^\..*'))
		 */
		$current_files = JFolder::files($uploaddir, '', 1, true, array('index.html'));

		foreach ($current_files as $file)
		{
			if ($file != ".." && $file != ".")
			{
				$diffts = $currts - filemtime($file);

				if ($diffts > $exp_file_time)
				{
					echo '<br/>' . JText::_('COM_JBOLO_PURGE_DELETING_FILE') . $file;

					if (!JFile::delete($file))
					{
						echo '<br/>' . JText::_('COM_JBOLO_PURGE_ERROR_DELETING_FILE') . '-' . $file;
					}
				}
			}
		}

		echo "</div>";
	}

	/**
	 * This function ->
	 * -- creates a new chat node or opens an existing node for given set of users
	 * -- if a new node is created it adds participants against this node
	 * -- it also updates session by adding new node data into session
	 * -- and finally echoes json reponse
	 *
	 * Needs input from POST
	 * -- pid:778
	 *
	 * Outputs Json array
	 * -- $nodeinfo: {ctyp: "1", nid: "1", wt: "user1"}
	 *
	 * Called from -
	 * - contoller.php
	 *     -- functions => initiateNode()
	 *
	 * Calls -
	 * - models/nodes.php [same file]
	 *     -- functions => validateUser()
	 * - helpers/nodes.php
	 *     -- functions => checkNodeExists() getNodeTitle() getNodeStatus() getNodeParticipants()
	 *
	 * @return array
	 *
	 * @since 3.0
	 **/
	public function initiateNode()
	{
		// Validate user
		$ini_node = $this->validateUserLogin();

		$db   = JFactory::getDbo();
		$user = JFactory::getUser();
		$uid  = $user->id;

		// Get participant from post
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		// Participant id e.g. 778
		$pid   = $input->post->get('pid', '', 'INT');

		// Validate inputs
		// Validate participant id - check if pid is specified
		if (!$pid)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_PARTICIPANT');

			return $ini_node;
		}

		// Check if node exists
		// Use nodesHelper
		$nodesHelper   = new nodesHelper;
		$node_id_found = $nodesHelper->checkNodeExists($uid, $pid);

		// If no existing node found
		if (!$node_id_found)
		{
			// Create new node
			$myobj        = new stdclass;
			$myobj->title = null;
			$myobj->type  = 1;
			$myobj->owner = $uid;

			// //NOTE - date format date("Y-m-d H:i:s");
			// $myobj->time  = date("Y-m-d H:i:s");
			$date = JFactory::getDate();
			$myobj->time = $date->toSql();

			$db->insertObject('#__jbolo_nodes', $myobj);

			// Get last insert id
			$new_node_id = $db->insertid();

			if ($db->insertid())
			{
				// Add participants against newly created node
				for ($i = 0; $i < 2; $i++)
				{
					$myobj          = new stdclass;
					$myobj->node_id = $new_node_id;

					// Add entry for both users one after other
					$myobj->user_id = ($i == 0) ? $uid : $pid;
					$myobj->status  = 1;
					$db->insertObject('#__jbolo_node_users', $myobj);
				}
			}
		}
		else
		{
			// Node already exists
			$new_node_id = $node_id_found;
		}

		// Prepare json response
		$query = "SELECT node_id AS nid, type AS ctyp
		FROM #__jbolo_nodes
		WHERE node_id=" . $new_node_id;
		$db->setQuery($query);
		$node_d = $db->loadObject();

		$ini_node['nodeinfo'] = $node_d;

		// Get chat window title(wt)
		$ini_node['nodeinfo']->wt = $nodesHelper->getNodeTitle($new_node_id, $uid, $ini_node['nodeinfo']->ctyp);

		// Get chatbox status (node status - ns)
		$ini_node['nodeinfo']->ns = $nodesHelper->getNodeStatus($new_node_id, $uid, $ini_node['nodeinfo']->ctyp);

		// Use nodesHelper
		// Get participants list
		$participants = $nodesHelper->getNodeParticipants($new_node_id, $uid);

		// Update this node info in session
		$d = 0;

		// Check if 'nodes' array is set
		if (!isset($_SESSION['jbolo']['nodes']))
		{
			// If nodes array is not set, push new node at the start in nodes array
			$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];
			$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
		}
		else
		{
			// If nodes array is set
			{
				$node_ids  = array();
				$nodecount = count($_SESSION['jbolo']['nodes']);

				for ($d = 0; $d < $nodecount; $d++)
				{
					if (isset($_SESSION['jbolo']['nodes'][$d]))
					{
						// Get all node ids set in session
						$node_ids[$d] = $_SESSION['jbolo']['nodes'][$d]['nodeinfo']->nid;
					}
				}

				// If the current node is not present in session,
				// we add it into session
				if (!in_array($ini_node['nodeinfo']->nid, $node_ids))
				{
					// If nodecount is >0, push new node data at the end of array
					if ($nodecount)
					{
						$_SESSION['jbolo']['nodes'][$nodecount]['nodeinfo']     = $ini_node['nodeinfo'];
						$_SESSION['jbolo']['nodes'][$nodecount]['participants'] = $participants['participants'];
					}
					else
					{
						// If nodecount is 0, push this at start i.e. 0th position in array
						{
							$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];
							$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
						}
					}
				}
			}
		}

		/*
		$logf=JPATH_SITE.'/components/com_jbolo/'.$uid.'.php';
		$f = @fopen($logf, 'a+');
		if ($f) {
		$log="\n".'initiate node'."\n";
		if(isset($_SESSION['jbolo']))$log.= print_r($_SESSION['jbolo'], true);
		$log= print_r($ini_node, true);
		@fputs($f,$log);
		@fclose($f);
		}
		*/

		return $ini_node;
	}

	/**
	 * Blocks a user in 1to1 chat
	 * - This takes these input via post
	 * -- nid: 2
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function blockUser()
	{
		// Validate user
		$block_user_response = $this->validateUserLogin();

		$db      = JFactory::getDbo();
		$user    = JFactory::getUser();
		$actorid = $user->id;

		// Get node id
		$input = JFactory::getApplication()->input;

		// E.g. 9
		$nid   = $input->post->get('nid', '', 'INT');

		// Validate participant id - check if pid is specified
		if (!$nid)
		{
			$block_user_response['validate']->error     = 1;
			$block_user_response['validate']->error_msg = JText::_('COM_JBOLO_INVALID_NODE_ID');

			return $block_user_response;
		}

		$nodesHelper = new nodesHelper;

		// Get Node participants
		$participants = $nodesHelper->getActiveNodeParticipants($nid);

		// Get Participants user id
		$pid = '';

		foreach ($participants as $participant)
		{
			if ($participant->uid != $actorid)
			{
				$pid = $participant->uid;
				break;
			}
		}

		// Add participants entry in privacy table
		if ($pid)
		{
			$obj                     = new stdclass;
			$obj->id                 = '';
			$obj->blocked_by_user_id = $actorid;
			$obj->blocked_user_id    = $pid;
			$obj->blocked_in_node_id = $nid;

			if (!$db->insertObject('#__jbolo_privacy', $obj, 'id'))
			{
				$block_user_response['validate']->error     = 1;
				$block_user_response['validate']->error_msg = $db->stderr();

				return $block_user_response;
			}

			$block_user_response['validate']->error   = 0;
			$block_user_response['validate']->success = JText::_('COM_JBOLO_USER_BLOCKED');

			return $block_user_response;
		}
		else
		{
			$block_user_response['validate']->error     = 1;
			$block_user_response['validate']->error_msg = JText::_('COM_JBOLO_INVALID_NODE_ID');

			return $block_user_response;
		}
	}

	/**
	 * This function =>
	 * - enables or disables a group chat node for social group chat
	 * - only group admin can turn on or off this node
	 * - echoes json reponse
	 *
	 * Inputs needed via POST
	 * -- gid:112 / onOff:1 / client:com_community.group
	 *
	 * Outputs
	 * $ini_node - @json array
	 *
	 * @return  array
	 *
	 * @since  3.2
	 **/
	public function setGroupChat()
	{
		// Validate user.
		$ini_node = $this->validateUserLogin();

		// Get iputs from posted data.
		$input  = JFactory::getApplication()->input;
		$post   = $input->post;

		// 54 or 11
		$gid    = $input->post->get('gid', '', 'INT');

		// 0 or 1
		$onOff  = $input->post->get('onOff', '', 'INT');

		// E.g. com_community.group
		$client = $input->post->get('client', '', 'string');

		// Validate if gid is not specified.
		if (!$gid)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_GROUP_ID');

			return $ini_node;
		}

		// Validate if onOff is not specified, and is corrrect.
		// Acceptable values are 0 or 1 .
		if ($onOff != 0 && $onOff != 1)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_GROUP_OPTION');

			return $ini_node;
		}

		$db     = JFactory::getDbo();
		$user   = JFactory::getUser();
		$uid    = $user->id;
		$params = JComponentHelper::getParams('com_jbolo');

		// Validate if logged in user a group admin.
		$integrationsHelper = new integrationsHelper;
		$isSocialGroupAdmin = $integrationsHelper->isSocialGroupAdmin($gid, $uid, $client);

		if (!$isSocialGroupAdmin)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_NON_GROUP_ADMIN_MSG');

			return $ini_node;
		}

		// Check if node exists for this social-group groupchat.
		// Use  nodesHelper.
		$nodesHelper       = new nodesHelper;
		$socialGroupNodeId = $nodesHelper->checkSocialGroupNodeExists($gid, $client);

		// If no existing node found, create new node.
		$newFlag = 0;

		if (!$socialGroupNodeId)
		{
			// Create a new node for this social-group groupchat.
			// Get group title.
			$socialGroupTitle = $integrationsHelper->getSocialGroupTitle($gid, $client);

			$myobj           = new stdclass;
			$myobj->title    = $socialGroupTitle;
			$myobj->type     = 2;
			$myobj->owner    = $uid;

			// //NOTE - date format date("Y-m-d H:i:s");
			// $myobj->time  = date("Y-m-d H:i:s");
			$date = JFactory::getDate();
			$myobj->time = $date->toSql();

			$myobj->group_id = $gid;
			$myobj->status   = $onOff;
			$myobj->client   = $client;

			$db->insertObject('#__jbolo_nodes', $myobj);
			$db->stderr();
			$socialGroupNodeId = $db->insertid();
			$newFlag           = 1;
		}
		// If node is found, update it.
		else
		{
			// Update node for this social-group groupchat.
			// Get group title.
			$socialGroupTitle = $integrationsHelper->getSocialGroupTitle($gid, $client);

			$myobj          = new stdclass;
			$myobj->node_id = $socialGroupNodeId;
			$myobj->title   = $socialGroupTitle;
			$myobj->status  = $onOff;
			$myobj->client  = $client;
			$db->updateObject('#__jbolo_nodes', $myobj, 'node_id');
			$db->stderr();
		}

		// If chat is to keep active
		if ($onOff == 1)
		{
			// This is like, syncing users between group and groupchat.
			// Get all those users which are social-group members and which are not part of this group chat yet.
			$socialGroupMembers = $integrationsHelper->getSocialGroupMembers($gid, $socialGroupNodeId, $client);

			if ($newFlag)
			{
				// Push welcome to Social group chat broadcast to GroupOwner
				$this->pushSocialGroupWelcomeMsgBroadcast('gbc', $socialGroupNodeId, $socialGroupTitle, $uid);
			}

			// Add above members as participants to newly created group chat node.
			for ($i = 0; $i < count($socialGroupMembers); $i++)
			{
				$myobj          = new stdclass;
				$myobj->node_id = $socialGroupNodeId;
				$myobj->user_id = $socialGroupMembers[$i];
				$myobj->status  = 1;
				$db->insertObject('#__jbolo_node_users', $myobj);

				if ($uid != $socialGroupMembers[$i])
				{
					// Push welcome to Social group chat broadcast to current groupmember being added.
					// $msgType, $nid, $particularUID=0, $socialGroupTitle
					$this->pushSocialGroupWelcomeMsgBroadcast('gbc', $socialGroupNodeId, $socialGroupTitle, $socialGroupMembers[$i]);

					// Push 'welcome' message to current groupmember being added.
					// $msgType, $nid, $particularUID=0, $sendToActor=0, $uid
					$this->pushWelcomeMsgBroadcast('gbc', $socialGroupNodeId, $uid, $socialGroupMembers[$i], 0);

					// Push 'invited' messages for actor and others
					// $msgType, $nid, $particularUID=0, $sendToActor=0, $uid, $pid
					$this->pushInvitedMsgBroadcast('gbc', $socialGroupNodeId, $uid, $socialGroupMembers[$i], 0, 1);
				}

				// Push 'who has joined' messages to actor and others
				// $msgType, $nid, $particularUID=0, $sendToActor=0, $uid
				$this->pushSocialGroupJoinedMsgBroadcast('gbc', $socialGroupNodeId, $socialGroupMembers[$i], 0, 1);
			}
		}
		elseif ($onOff == 0)
		{
			// @TODO
			// Nothing here yet
		}

		$query = "SELECT node_id AS nid, type AS ctyp
		FROM #__jbolo_nodes
		WHERE node_id=" . $socialGroupNodeId;
		$db->setQuery($query);
		$node_d = $db->loadObject();

		$ini_node['nodeinfo']     = $node_d;
		$ini_node['nodeinfo']->wt = $nodesHelper->getNodeTitle($socialGroupNodeId, $uid, $ini_node['nodeinfo']->ctyp);

		// Get chatbox status
		$ini_node['nodeinfo']->ns = $nodesHelper->getNodeStatus($ini_node['nodeinfo']->nid, $uid, $ini_node['nodeinfo']->ctyp);

		// Add node data to session.
		$d = 0;

		// Get node participants info.
		$participants = $nodesHelper->getNodeParticipants($socialGroupNodeId, $uid);

		// Prepare json output for node participants.
		// Check if node array is set.
		if (isset($_SESSION['jbolo']['nodes']))
		{
			$node_ids = array();

			for ($d = 0; $d < count($_SESSION['jbolo']['nodes']); $d++)
			{
				$node_info['nodeinfo'] = array();

				if (isset($_SESSION['jbolo']['nodes'][$d]))
				{
					// Get all node ids set in session
					$node_ids[$d] = $_SESSION['jbolo']['nodes'][$d]['nodeinfo']->nid;
				}
			}

			// If entry for node found in session, update it
			if (in_array($ini_node['nodeinfo']->nid, $node_ids))
			{
				// @TODO blackhole as of 0.2 beta
			}

			// @TODO else part need to check
			else
			{
				// If node data not session, push new nodedata at end
				{
					$_SESSION['jbolo']['nodes'][$d]['nodeinfo'] = $ini_node['nodeinfo'];

					// Push node participants
					// ??
					$_SESSION['jbolo']['nodes'][$d]['participants'] = $participants['participants'];
				}
			}
		}
		else
		{
			// If nodes array is not set, push new node at start
			{
				// @TODO 0 or $d here?
				$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];

				// ??
				$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
			}
		}

		return $ini_node;
	}

	/**
	 * This function =>
	 * - lets social group members jpin the group chat node created for that social group chat
	 * - only group members can join this chat
	 * - echoes json reponse
	 *
	 * Inputs needed via POST
	 * -- gid:112
	 *
	 * Outputs
	 * $ini_node - @json array
	 *
	 * @return  array
	 *
	 * @since  3.2
	 **/
	public function joinSocialGroupChat()
	{
		// Validate user.
		$ini_node = $this->validateUserLogin();

		// Get iputs from posted data.
		$input  = JFactory::getApplication()->input;
		$post   = $input->post;

		// 54 or 11
		$gid    = $input->post->get('gid', '', 'INT');

		// E.g. com_community.group
		$client = $input->post->get('client', '', 'string');

		// Validate if gid is not specified.
		if (!$gid)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_GROUP_ID');

			return $ini_node;
		}

		$db     = JFactory::getDbo();
		$user   = JFactory::getUser();
		$uid    = $user->id;
		$params = JComponentHelper::getParams('com_jbolo');

		// Validate if logged in user a group user.
		$integrationsHelper = new integrationsHelper;
		$isSocialGroupUser  = $integrationsHelper->isSocialGroupUser($gid, $uid, $client);

		// Check if user is part of social group
		$isSocialGroupUser = $integrationsHelper->isSocialGroupUser($gid, $uid, $client);

		// Check if user is admin of social group
		$isSocialGroupAdmin = $integrationsHelper->isSocialGroupAdmin($gid, $uid, $client);

		// If no social group user
		if (!$isSocialGroupUser && !$isSocialGroupAdmin)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_NON_GROUP_MEMBER_MSG');

			return $ini_node;
		}

		// Get node id for this social group.
		// Use  nodesHelper.
		$nodesHelper       = new nodesHelper;
		$socialGroupNodeId = $nodesHelper->checkSocialGroupNodeExists($gid, $client);

		// If no existing node found, create new node.
		if (!$socialGroupNodeId)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_NO_SOCIAL_GROUP_NODE_FOUND_MSG');

			return $ini_node;
		}
		// If node is found, add user to this group.
		else
		{
			// Check if curent node is active or not.
			$isActiveGroupNode = $nodesHelper->isActiveGroupNode($socialGroupNodeId);

			// If no existing node found, create new node.
			if (!$isActiveGroupNode)
			{
				$ini_node['validate']->error     = 1;
				$ini_node['validate']->error_msg = JText::_('COM_JBOLO_NODE_NOT_ACTIVE_MSG');

				return $ini_node;
			}

			// Check if user being added is already participant.
			$isNodeParticipant = $nodesHelper->isNodeParticipant($uid, $socialGroupNodeId);

			// Get group title.
			$socialGroupTitle = $integrationsHelper->getSocialGroupTitle($gid, $client);

			if (!$isNodeParticipant)
			{
				// Add new user to groupchat.
				$myobj          = new stdclass;
				$myobj->node_id = $socialGroupNodeId;
				$myobj->user_id = $uid;
				$myobj->status  = 1;
				$db->insertObject('#__jbolo_node_users', $myobj);

				// Use broadcast helper
				$chatBroadcastHelper = new chatBroadcastHelper;

				// Push welcome to Social group chat broadcast to current groupmember being added.
				// $msgType, $nid, $particularUID=0, $socialGroupTitle
				$this->pushSocialGroupWelcomeMsgBroadcast('gbc', $socialGroupNodeId, $socialGroupTitle, $uid);

				// Push 'welcome' message to current groupmember being added.
				// $msgType, $nid, $particularUID=0, $sendToActor=0, $uid
				$this->pushWelcomeMsgBroadcast('gbc', $socialGroupNodeId, $uid, 0, $uid);

				// Push 'who has joined' messages to actor and others
				// $msgType, $nid, $particularUID=0, $sendToActor=0, $uid
				$this->pushSocialGroupJoinedMsgBroadcast('gbc', $socialGroupNodeId, $uid, 0, 1);
			}
			// Re-adding user
			elseif ($isNodeParticipant == 2)
			{
				// Re-add existing user.
				$query = "UPDATE #__jbolo_node_users
				SET status=1
				WHERE node_id=" . $socialGroupNodeId . "
				AND user_id=" . $uid . "
				AND status=0";
				$db->setQuery($query);
				$db->execute();

				// Use broadcast helper
				$chatBroadcastHelper = new chatBroadcastHelper;

				// Push welcome to Social group chat broadcast to current groupmember being added.
				// $msgType, $nid, $particularUID=0, $socialGroupTitle
				$this->pushSocialGroupWelcomeMsgBroadcast('gbc', $socialGroupNodeId, $socialGroupTitle, $uid);

				// Push 'welcome' message to current groupmember being added.
				// $msgType, $nid, $particularUID=0, $sendToActor=0, $uid
				$this->pushWelcomeMsgBroadcast('gbc', $socialGroupNodeId, $uid, 0, $uid);

				// Push 'who has joined' messages to actor and others
				// $msgType, $nid, $particularUID=0, $sendToActor=0, $uid
				$this->pushSocialGroupJoinedMsgBroadcast('gbc', $socialGroupNodeId, $uid, 0, 1);
			}
		}

		$query = "SELECT node_id AS nid, type AS ctyp
		FROM #__jbolo_nodes
		WHERE node_id=" . $socialGroupNodeId;
		$db->setQuery($query);
		$node_d = $db->loadObject();

		$ini_node['nodeinfo']     = $node_d;
		$ini_node['nodeinfo']->wt = $nodesHelper->getNodeTitle($socialGroupNodeId, $uid, $ini_node['nodeinfo']->ctyp);

		// Get chatbox status
		$ini_node['nodeinfo']->ns = $nodesHelper->getNodeStatus($ini_node['nodeinfo']->nid, $uid, $ini_node['nodeinfo']->ctyp);

		// Add node data to session.
		$d = 0;

		// Get node participants info.
		$participants = $nodesHelper->getNodeParticipants($socialGroupNodeId, $uid);

		// Prepare json output for node participants.
		// Check if node array is set.
		if (isset($_SESSION['jbolo']['nodes']))
		{
			$node_ids = array();

			for ($d = 0; $d < count($_SESSION['jbolo']['nodes']); $d++)
			{
				$node_info['nodeinfo'] = array();

				if (isset($_SESSION['jbolo']['nodes'][$d]))
				{
					// Get all node ids set in session
					$node_ids[$d] = $_SESSION['jbolo']['nodes'][$d]['nodeinfo']->nid;
				}
			}

			// If entry for node found in session, update it
			if (in_array($ini_node['nodeinfo']->nid, $node_ids))
			{
				// @TODO blackhole as of 0.2 beta
			}
			// @TODO else part need to check
			else
			{
				// If node data not session, push new nodedata at end
				{
					$_SESSION['jbolo']['nodes'][$d]['nodeinfo'] = $ini_node['nodeinfo'];

					// Push node participants
					// ??
					$_SESSION['jbolo']['nodes'][$d]['participants'] = $participants['participants'];
				}
			}
		}
		else
		{
			// If nodes array is not set, push new node at start
			{
				// @TODO 0 or $d here?
				$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];

				// ??
				$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
			}
		}

		return $ini_node;
	}

	/**
	 * Pushes Social group welcome message broadcast to new group members.
	 *
	 * @param   string  $msgType           Chat Message type
	 * @param   int     $nid               Node id
	 * @param   int     $socialGroupTitle  Social group title
	 * @param   int     $particularUID     If msg is to be sent to particular user that - user id
	 *
	 * @return  boolean
	 *
	 * @since  3.2
	 */
	public function pushSocialGroupWelcomeMsgBroadcast($msgType, $nid, $socialGroupTitle, $particularUID = 0)
	{
		// Use broadcast helper
		$chatBroadcastHelper = new chatBroadcastHelper;
		$msg                 = JText::sprintf(JText::_('COM_JBOLO_GC_BC_WELCOME_MSG_SOCIAL_GROUP'), $socialGroupTitle);
		$chatBroadcastHelper->pushChat($msgType, $nid, $msg, $particularUID);

		return true;
	}

	/**
	 * Pushes Social group joined message broadcast to group members.
	 *
	 * @param   string  $msgType        Chat Message type
	 * @param   int     $nid            Node id
	 * @param   int     $uid            User id
	 * @param   int     $particularUID  If msg is to be sent to particular user that - user id
	 * @param   int     $sendToActor    If msg is to be sent to actor himself
	 *
	 * @return  boolean
	 *
	 * @since  3.2
	 */
	public function pushSocialGroupJoinedMsgBroadcast($msgType, $nid, $uid, $particularUID = 0, $sendToActor = 0)
	{
		// $db=JFactory::getDbo();
		// Use  nodes helper
		$nodesHelper  = new nodesHelper;
		$participants = $nodesHelper->getActiveNodeParticipants($nid);

		// Use broadcast helper
		$chatBroadcastHelper = new chatBroadcastHelper;
		$msg                 = "";

		$params    = JComponentHelper::getParams('com_jbolo');
		$chattitle = $params->get('chatusertitle');

		// Show username OR name
		if ($params->get('chatusertitle'))
		{
			$chattitle = 'username';
		}
		else
		{
			$chattitle = 'name';
		}

		$msg = JFactory::getUser($uid)->$chattitle . ' <i>' . JText::_('COM_JBOLO_GC_JOINED') . "</i><br/>";

		$chatBroadcastHelper->pushChat($msgType, $nid, $msg, $particularUID, $sendToActor);

		return true;
	}
}
