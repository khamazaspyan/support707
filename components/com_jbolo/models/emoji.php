<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.model');

/**
 * Class for JBolo ticket model
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloModelEmoji extends JModelLegacy
{
	/**
	 * Gets list of emoji groups, group emoji, and emoji shortcodes
	 *
	 * @return  array
	 *
	 * @since  3.2.5
	 */
	public function getEmojis()
	{
		$params      = JComponentHelper::getParams('com_jbolo');
		$emojiGroups = $params->get('emoji_groups', array('people'));

		// Make string to array
		// Needed when config is not saved after fresh installation
		if (!is_array($emojiGroups))
		{
			$temp[]      = $emojiGroups;
			$emojiGroups = $temp;
		}

		require_once JPATH_SITE . '/media/com_jbolo/emojis/tjemojis.php';

		foreach ($emojiGroups as $eg)
		{
			// Derive namespaced emoji group name & emoji group emoji
			$groupName  = 'tjEmojis' . ucfirst($eg);
			$groupEmoji = 'tjEmojis' . ucfirst($eg) . 'Emoji';

			$output['emojiGroups'][]      = $eg;
			$output['emojiGroupsEmoji'][] = $$groupEmoji;
			$output['emojis'][$eg]        = $$groupName;
		}

		return $output;
	}
}
