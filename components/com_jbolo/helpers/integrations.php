<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Component Helper
jimport('joomla.application.component.helper');
jimport('joomla.filesystem.folder');

/**
 * Class for JBolo social integrations helper.
 *
 * @package  JBolo
 * @since    3.0
 */
class IntegrationsHelper
{
	/**
	 * Returns profile page url for given user based on selected integration
	 *
	 * called from -
	 * - helpers/nodes.php
	 *     - functions getNodeParticipants()
	 *
	 * calls -
	 * - helpers/jbolo.php
	 *     -- functions => getItemId()
	 *
	 * @param   integer  $userid  e.g. 777
	 *
	 * @return  string  $uimage
	 *
	 * @since  3.0
	 */
	public function getUserProfileUrl($userid)
	{
		$params             = JComponentHelper::getParams('com_jbolo');
		$integration_option = $params->get('community');
		$link               = '';

		if ($integration_option == 'joomla')
		{
			$link = '';
		}
		elseif ($integration_option == 'cb')
		{
			// Use jboloHelper
			$jboloHelper = new jboloHelper;
			$itemid      = $jboloHelper->getItemId('option=com_comprofiler');
			$link        = JRoute::_('index.php?option=com_comprofiler&task=userprofile&user=' . $userid . '&Itemid=' . $itemid);
			$link        = JUri::root() . substr($link, strlen(JUri::base(true)) + 1);
		}
		elseif ($integration_option == 'jomsocial')
		{
			$jspath = JPATH_ROOT . DS . 'components' . DS . 'com_community';

			if (JFolder::exists($jspath))
			{
				include_once $jspath . DS . 'libraries' . DS . 'core.php';
			}

			$link = JUri::root() . substr(CRoute::_('index.php?option=com_community&view=profile&userid=' . $userid), strlen(JUri::base(true)) + 1);
		}
		elseif ($integration_option == 'jomwall')
		{
			if (!class_exists('AwdwallHelperUser'))
			{
				require_once JPATH_SITE . DS . 'components' . DS . 'com_awdwall' . DS . 'helpers' . DS . 'user.php';
			}

			$awduser = new AwdwallHelperUser;
			$Itemid  = $awduser->getComItemId();
			$link    = JRoute::_('index.php?option=com_awdwall&view=awdwall&layout=mywall&wuid=' . $userid . '&Itemid=' . $Itemid);
		}
		elseif ($integration_option == 'EasySocial')
		{
			$espath = JPATH_ROOT . DS . 'components' . DS . 'com_easysocial';
			$link   = '';

			if (file_exists($espath))
			{
				require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/foundry.php';
				$user = Foundry::user($userid);
				$link = JRoute::_($user->getPermalink());
			}
		}
		elseif ($integration_option == 'easyprofile')
		{
			$eppath = JPATH_SITE . DS . 'components' . DS . 'com_jsn';
			$link   = '';

			if (file_exists($eppath))
			{
				require_once JPATH_SITE . '/components/com_jsn/helpers/helper.php';
				$user = JsnHelper::getUser($userid);
				$link = $user->getLink();
			}
		}

		return $link;
	}

	/**
	 * Returns avatar for given user based on selected integration
	 *
	 * Called from -
	 * - helpers/nodes.php
	 *     - functions getNodeParticipants()
	 *
	 * Calls -
	 * - helpers/integrations.php (this file)
	 *     -- functions => getCBUserAvatar() getJomsocialUserAvatar() getJomwallUserAvatar()
	 *
	 * @param   integer  $userid  e.g. 777
	 *
	 * @return  string  $uimage
	 *
	 * @since  3.0
	 */
	public function getUserAvatar($userid)
	{
		$params             = JComponentHelper::getParams('com_jbolo');
		$integration_option = $params->get('community');
		$gravatar           = $params->get('gravatar');

		$uimage = '';

		if ($gravatar)
		{
			$user     = JFactory::getUser($userid);
			$usermail = $user->get('email');

			// Refer https://en.gravatar.com/site/implement/images/php/
			$hash   = md5(strtolower(trim($usermail)));
			$uimage = 'http://www.gravatar.com/avatar/' . $hash . '?s=32';

			return $uimage;
		}

		if ($integration_option == "joomla")
		{
			$template = $params->get('template');
			$uimage   = JUri::root() . "media/com_jbolo/themes/" . $template . "/images/avatar_default.png";
		}
		elseif ($integration_option == "cb")
		{
			$uimage = $this->getCBUserAvatar($userid);
		}
		elseif ($integration_option == "jomsocial")
		{
			$uimage = $this->getJomsocialUserAvatar($userid);
		}
		elseif ($integration_option == "jomwall")
		{
			$uimage = $this->getJomwallUserAvatar($userid);
		}
		elseif ($integration_option == "EasySocial")
		{
			$uimage = $this->getEasySocialUserAvatar($userid);
		}
		elseif ($integration_option == 'easyprofile')
		{
			$uimage = $this->getEasyProfileUserAvatar($userid);
		}

		return $uimage;
	}

	/**
	 * Returns CB avatar for given user
	 * Called from -
	 * - helpers/integrations.php (this file)
	 *     - functions getUserAvatar()
	 *
	 * @param   integer  $userid  e.g. 777
	 *
	 * @return  string  $uimage
	 *
	 * @since  3.0
	 */
	public function getCBUserAvatar($userid)
	{
		$db = JFactory::getDbo();
		$q  = "SELECT a.id,a.username,a.name, b.avatar, b.avatarapproved
		FROM #__users a, #__comprofiler b
		WHERE a.id=b.user_id AND a.id=" . $userid;
		$db->setQuery($q);
		$user     = $db->loadObject();
		$img_path = JUri::root() . "images/comprofiler";

		if (isset($user->avatar) && isset($user->avatarapproved))
		{
			if (substr_count($user->avatar, "/") == 0)
			{
				$uimage = $img_path . '/tn' . $user->avatar;
			}
			else
			{
				$uimage = $img_path . '/' . $user->avatar;
			}
		}
		elseif (isset($user->avatar))
		{
			// Avatar not approved
			$uimage = JUri::root() . "/components/com_comprofiler/plugin/templates/default/images/avatar/nophoto_n.png";
		}
		else
		{
			// No avatar
			$uimage = JUri::root() . "/components/com_comprofiler/plugin/templates/default/images/avatar/nophoto_n.png";
		}

		return $uimage;
	}

	/**
	 * Returns Jomsocial avatar for given user
	 * Called from -
	 * - helpers/integrations.php (this file)
	 *     - functions getUserAvatar()
	 *
	 * @param   integer  $userid  e.g. 777
	 *
	 * @return  string  $uimage
	 *
	 * @since  3.0
	 */
	public function getJomsocialUserAvatar($userid)
	{
		$mainframe = JFactory::getApplication();
		/*included to get jomsocial avatar*/
		$jspath = JPATH_ROOT . DS . 'components' . DS . 'com_community';

		if (JFolder::exists($jspath))
		{
			include_once $jspath . DS . 'libraries' . DS . 'core.php';
		}

		$user   = CFactory::getUser($userid);
		$uimage = $user->getThumbAvatar();

		if (!$mainframe->isSite())
		{
			$uimage = str_replace('administrator/', '', $uimage);
		}

		return $uimage;
	}

	/**
	 * Returns Jomwall avatar for given user
	 * Called from -
	 * - helpers/integrations.php (this file)
	 *     - functions getUserAvatar()
	 *
	 * @param   integer  $userid  e.g. 777
	 *
	 * @return  string  $uimage
	 *
	 * @since  3.0
	 */
	public function getJomwallUserAvatar($userid)
	{
		if (!class_exists('AwdwallHelperUser'))
		{
			require_once JPATH_SITE . DS . 'components' . DS . 'com_awdwall' . DS . 'helpers' . DS . 'user.php';
		}

		$awduser = new AwdwallHelperUser;
		$uimage  = $awduser->getAvatar($userid);

		return $uimage;
	}

	/**
	 * Returns EasySocial avatar for given user
	 * Called from -
	 * - helpers/integrations.php (this file)
	 *     - functions getUserAvatar()
	 *
	 * @param   integer  $userid  e.g. 777
	 *
	 * @return  string  $uimage
	 *
	 * @since  3.0
	 */
	public function getEasySocialUserAvatar($userid)
	{
		$espath = JPATH_ROOT . DS . 'components' . DS . 'com_easysocial';

		if (file_exists($espath))
		{
			require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/foundry.php';
			$user   = Foundry::user($userid);
			$uimage = $user->getAvatar();

			return $uimage;
		}

		return;
	}

	/**
	 * Returns EasyProfile avatar for given user
	 * Called from -
	 * - helpers/integrations.php (this file)
	 *     - functions getUserAvatar()
	 *
	 * @param   integer  $userid  e.g. 777
	 *
	 * @return  string  $uimage
	 *
	 * @since  3.0
	 */
	public function getEasyProfileUserAvatar($userid)
	{
		$eppath = JPATH_SITE . DS . 'components' . DS . 'com_jsn';

		if (file_exists($eppath))
		{
			require_once JPATH_SITE . '/components/com_jsn/helpers/helper.php';
			$user   = JsnHelper::getUser($userid);
			$uimage = JUri::root() . DS . $user->getValue('avatar_mini');

			return $uimage;
		}

		return;
	}

	/**
	 * Get list of online users for selected integration.
	 *
	 * @param   integer  $uid                Logged in user id
	 * @param   array    $usersNotToInclude  List of users not to be shown in usrlist
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getOnlineUsersList($uid, $usersNotToInclude)
	{
		$params             = JComponentHelper::getParams('com_jbolo');
		$integration_option = $params->get('community');
		$fonly              = $params->get('fonly');
		$chattitle          = $params->get('chatusertitle');
		$hideChatOnMobile   = $params->get('hide_chat');

		// Show username OR name
		if ($params->get('chatusertitle'))
		{
			$chattitle = 'username';
		}
		else
		{
			$chattitle = 'name';
		}

		$online_users = '';

		if ($integration_option == "joomla")
		{
			$online_users = $this->getJoomlaOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile);
		}
		elseif ($integration_option == "cb")
		{
			$online_users = $this->getCBOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile);
		}
		elseif ($integration_option == "jomsocial")
		{
			$online_users = $this->getJomsocialOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile);
		}
		elseif ($integration_option == "jomwall")
		{
			$online_users = $this->getJomwallOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile);
		}
		elseif ($integration_option == "EasySocial")
		{
			$online_users = $this->getEasySocialOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile);
		}
		elseif ($integration_option == "easyprofile")
		{
			$online_users = $this->getEasyProfileOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile);
		}

		return $online_users;
	}

	/**
	 * Get list of online users for Joomla integration.
	 *
	 * @param   integer  $uid                Logged in user id
	 * @param   integer  $fonly              Show only friends or all users?
	 * @param   string   $chattitle          Show name or username?
	 * @param   array    $usersNotToInclude  List of users not to be shown in usrlist
	 * @param   array    $hideChatOnMobile   Hide chat on mobile OR on mobile on tablets both
	 * @param   string   $filterText         Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getJoomlaOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile, $filterText = '')
	{
		// Implode blocked & blocked by users id by comma
		if ($usersNotToInclude)
		{
			$usersNotToInclude = implode(',', $usersNotToInclude);
		}

		if ($usersNotToInclude)
		{
			$usersNotToInclude = " AND u.id NOT IN(" . $usersNotToInclude . ")";
		}
		else
		{
			$usersNotToInclude = " ";
		}

		// Hide on mobile or mobile and tablet both?
		$hideChatOnMobileQuery = '';

		if ($hideChatOnMobile == 1)
		{
			$hideChatOnMobileQuery = " AND ju.is_mobile <> 1";
		}
		elseif ($hideChatOnMobile == 2)
		{
			// 1 is mobile, 2 is tablets, so lets hide on all where > 1
			$hideChatOnMobileQuery = " AND ju.is_mobile < 1";
		}

		$db    = JFactory::getDbo();

		$query = "SELECT DISTINCT u.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
		ju.chat_status AS sts,ju.status_msg AS stsm, ju.state
		FROM #__users AS u, #__session AS s
		LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
		 WHERE u.id IN (s.userid)
		 AND s.client_id = 0
		 AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery;

		if ($filterText)
		{
			$query .= " AND u.username LIKE '%" . $filterText . "%'";
		}

		$query .= " AND u.id <>" . $uid;
		$db->setQuery($query);
		$online_users = $db->loadObjectList();

		$online_users = $this->checkOnlineUserPermission($online_users, $filterText);

		return $online_users;
	}

	/**
	 * Get list of online users for CB integration.
	 *
	 * @param   integer  $uid                Logged in user id
	 * @param   integer  $fonly              Show only friends or all users?
	 * @param   string   $chattitle          Show name or username?
	 * @param   array    $usersNotToInclude  List of users not to be shown in usrlist
	 * @param   array    $hideChatOnMobile   Hide chat on mobile OR on mobile on tablets both
	 * @param   string   $filterText         Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getCBOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile, $filterText = '')
	{
		// Implode blocked & blocked by users id by comma
		if ($usersNotToInclude)
		{
			$usersNotToInclude = implode(',', $usersNotToInclude);
		}

		if ($usersNotToInclude)
		{
			$usersNotToInclude = " AND u.id NOT IN(" . $usersNotToInclude . ")";
		}
		else
		{
			$usersNotToInclude = " ";
		}

		// Hide on mobile or mobile and tablet both?
		$hideChatOnMobileQuery = '';

		if ($hideChatOnMobile == 1)
		{
			$hideChatOnMobileQuery = " AND ju.is_mobile <> 1";
		}
		elseif ($hideChatOnMobile == 2)
		{
			// 1 is mobile, 2 is tablets, so lets hide on all where > 1
			$hideChatOnMobileQuery = " AND ju.is_mobile < 1";
		}

		$db = JFactory::getDbo();

		// Friends  only
		if ($fonly)
		{
			$query = "SELECT DISTINCT a.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
			b.avatar,
			ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			FROM
			(
				SELECT DISTINCT u.id
				FROM #__users u, #__session s, #__comprofiler_members a
				LEFT JOIN #__comprofiler b ON a.memberid = b.user_id
				WHERE a.referenceid=" . $uid . "
				AND u.id = a.memberid
				AND a.memberid IN ( s.userid )
				AND (a.accepted=1)
				AND s.client_id = 0
				AND u.block=0
				ORDER BY u.username
			)
			AS a
			LEFT JOIN #__comprofiler b ON b.user_id = a.id
			LEFT JOIN #__comprofiler_members AS c on c.referenceid=a.id
			LEFT JOIN #__users AS u ON u.id=a.id
			LEFT JOIN #__session AS s ON s.userid=a.id
			LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			WHERE c.memberid=" . $uid . "
			AND c.accepted=1
			AND b.banned=0
			AND s.client_id=0
			AND ju.state <> -1 " . $usersNotToInclude . "
			 AND u.id <> " . $uid;

			if ($filterText)
			{
				$query .= " AND u.username LIKE '%" . $filterText . "%'";
			}

			$query .= " ORDER BY u." . $chattitle;
		}
		// Show all
		else
		{
			$query = "SELECT DISTINCT u.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
			cb.avatar,
			ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			FROM #__users as u
			LEFT JOIN #__session AS s ON s.userid=u.id
			LEFT JOIN #__comprofiler cb ON cb.user_id=u.id
			LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			WHERE u.id IN (s.userid)
			AND u.id <> " . $uid . "
			AND s.client_id=0
			AND ju.state <> -1 " . $usersNotToInclude;

			if ($filterText)
			{
				$query .= " AND u.username LIKE '%" . $filterText . "%'";
			}

			$query .= " ORDER BY u." . $chattitle;
		}

		$db->setQuery($query);
		$online_users = $db->loadObjectList();
		$online_users = $this->checkOnlineUserPermission($online_users, $filterText);

		return $online_users;
	}

	/**
	 * Get list of online users for Jomsocial integration.
	 *
	 * @param   integer  $uid                Logged in user id
	 * @param   integer  $fonly              Show only friends or all users?
	 * @param   string   $chattitle          Show name or username?
	 * @param   array    $usersNotToInclude  List of users not to be shown in usrlist
	 * @param   array    $hideChatOnMobile   Hide chat on mobile OR on mobile on tablets both
	 * @param   string   $filterText         Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getJomsocialOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile, $filterText = '')
	{
		// Implode blocked & blocked by users id by comma
		if ($usersNotToInclude)
		{
			$usersNotToInclude = implode(',', $usersNotToInclude);
		}

		if ($usersNotToInclude)
		{
			$usersNotToInclude = " AND u.id NOT IN(" . $usersNotToInclude . ")";
		}
		else
		{
			$usersNotToInclude = " ";
		}

		// Hide on mobile or mobile and tablet both?
		$hideChatOnMobileQuery = '';

		if ($hideChatOnMobile == 1)
		{
			$hideChatOnMobileQuery = " AND ju.is_mobile <> 1";
		}
		elseif ($hideChatOnMobile == 2)
		{
			// 1 is mobile, 2 is tablets, so lets hide on all where > 1
			$hideChatOnMobileQuery = " AND ju.is_mobile < 1";
		}

		$db = JFactory::getDbo();

		// Friends  only
		if ($fonly)
		{
			$query = "SELECT DISTINCT u.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
			cu.thumb,
			ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			FROM #__users AS u
			LEFT JOIN #__community_users AS cu ON cu.userid=u.id
			LEFT JOIN #__community_connection AS cc ON cc.connect_to=cu.userid
			LEFT JOIN #__session AS s ON s.userid=u.id
			LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			WHERE cc.connect_from=" . $uid . "
			AND cc.status=1
			AND cc.connect_to=u.id
			AND u.id=s.userid
			AND s.client_id=0
			AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery . "
			AND u.id <> " . $uid;

			if ($filterText)
			{
				$query .= " AND ( u.username LIKE '%" . $filterText . "%' OR u.name LIKE '%" . $filterText . "%' )";
			}

			$query .= " ORDER BY u." . $chattitle;
		}
		// Show all
		else
		{
			$query = "SELECT DISTINCT u.id AS uid,  u." . $chattitle . " AS uname, u.name, u.username,
			cu.thumb,
			 ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			 FROM #__users as u
			 LEFT JOIN #__session AS s ON s.userid=u.id
			 LEFT JOIN #__community_users cu ON cu.userid=u.id
			 LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			 WHERE u.id IN (s.userid)
			 AND s.client_id=0
			 AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery;

			if ($filterText)
			{
				$query .= " AND ( u.username LIKE '%" . $filterText . "%' OR u.name LIKE '%" . $filterText . "%' )";
			}

			$query .= " AND u.id <> " . $uid . " ORDER BY u." . $chattitle;
		}

		$db->setQuery($query);
		$online_users = $db->loadObjectList();
		$online_users = $this->checkOnlineUserPermission($online_users, $filterText);

		return $online_users;
	}

	/**
	 * Get list of online users for Easysocial integration.
	 *
	 * @param   integer  $uid                Logged in user id
	 * @param   integer  $fonly              Show only friends or all users?
	 * @param   string   $chattitle          Show name or username?
	 * @param   array    $usersNotToInclude  List of users not to be shown in usrlist
	 * @param   array    $hideChatOnMobile   Hide chat on mobile OR on mobile on tablets both
	 * @param   string   $filterText         Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getEasySocialOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile, $filterText = '')
	{
		// Implode blocked & blocked by users id by comma
		if ($usersNotToInclude)
		{
			$usersNotToInclude = implode(',', $usersNotToInclude);
		}

		if ($usersNotToInclude)
		{
			$usersNotToInclude = " AND u.id NOT IN(" . $usersNotToInclude . ")";
		}
		else
		{
			$usersNotToInclude = " ";
		}

		// Hide on mobile or mobile and tablet both?
		$hideChatOnMobileQuery = '';

		if ($hideChatOnMobile == 1)
		{
			$hideChatOnMobileQuery = " AND ju.is_mobile <> 1";
		}
		elseif ($hideChatOnMobile == 2)
		{
			// 1 is mobile, 2 is tablets, so lets hide on all where > 1
			$hideChatOnMobileQuery = " AND ju.is_mobile < 1";
		}

		$db = JFactory::getDbo();

		// Friends  only
		if ($fonly)
		{
			// Get the friends of user
			/**
			 * easy social
			 * table structure actor_id    target_id
			 *                    896        897
			 *                    899        898
			 *                    900        898
			 *                    900        899
			 */

			// Query_from to get user friends who has friend request sent by login user

			$query_from = "SELECT DISTINCT u.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
			ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			FROM #__users AS u
			LEFT JOIN #__social_users AS su ON su.user_id = u.id
			LEFT JOIN #__social_friends AS sf ON sf.target_id = su.user_id
			LEFT JOIN #__session AS s ON s.userid = u.id
			LEFT JOIN #__jbolo_users AS ju ON ju.user_id = s.userid
			WHERE sf.actor_id = " . $uid . "
			AND sf.state = 1
			AND sf.target_id = u.id
			AND u.id = s.userid
			AND s.client_id = 0
			AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery . "
			 AND u.id <> " . $uid;

			if ($filterText)
			{
				$query_from .= " AND ( u.username LIKE '%" . $filterText . "%' OR u.name LIKE '%" . $filterText . "%' )";
			}

			$query_from .= " ORDER BY u." . $chattitle;

			$db->setQuery($query_from);
			$online_users_from = $db->loadObjectList('uid');

			// $query_targeted=> To get user friends who sent friend request to login user

			$query_targeted = "SELECT DISTINCT u.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
			ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			FROM #__users AS u
			LEFT JOIN #__social_users AS su ON su.user_id=u.id
			LEFT JOIN #__social_friends AS sf ON sf.actor_id=su.user_id
			LEFT JOIN #__session AS s ON s.userid=u.id
			LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			WHERE sf.target_id = " . $uid . "
			AND sf.state = 1
			AND sf.actor_id = u.id
			AND u.id = s.userid
			AND s.client_id = 0
			AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery . "
			 AND u.id <> " . $uid;

			if ($filterText)
			{
				$query_targeted .= " AND ( u.username LIKE '%" . $filterText . "%' OR u.name LIKE '%" . $filterText . "%' )";
			}

			$query_targeted .= " ORDER BY u." . $chattitle;

			$db->setQuery($query_targeted);
			$online_users_targeted = $db->loadObjectList('uid');

			// Merging result to remove repeated users
			$online_users = array_merge($online_users_from, $online_users_targeted);
		}
		// Show all
		else
		{
			$query = "SELECT DISTINCT u.id AS uid,  u." . $chattitle . " AS uname, u.name, u.username,
			ju.chat_status AS sts, ju.status_msg AS stsm,ju.state
			FROM #__users as u
			LEFT JOIN #__session AS s ON s.userid=u.id
			LEFT JOIN #__social_users su ON su.user_id=u.id
			LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			WHERE u.id IN (s.userid)
			AND s.client_id=0
			AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery;

			if ($filterText)
			{
				$query .= " AND ( u.username LIKE '%" . $filterText . "%' OR u.name LIKE '%" . $filterText . "%' )";
			}

			$query .= " AND u.id <> " . $uid
			. " ORDER BY u." . $chattitle;

			$db->setQuery($query);
			$online_users = $db->loadObjectList();
		}

		$online_users = $this->checkOnlineUserPermission($online_users, $filterText);

		return $online_users;
	}

	/**
	 * Get list of online users for Easyprofile integration.
	 *
	 * @param   integer  $uid                Logged in user id
	 * @param   integer  $fonly              Show only friends or all users?
	 * @param   string   $chattitle          Show name or username?
	 * @param   array    $usersNotToInclude  List of users not to be shown in usrlist
	 * @param   array    $hideChatOnMobile   Hide chat on mobile OR on mobile on tablets both
	 * @param   string   $filterText         Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getEasyProfileOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile, $filterText = '')
	{
		// Implode blocked & blocked by users id by comma
		if ($usersNotToInclude)
		{
			$usersNotToInclude = implode(',', $usersNotToInclude);
		}

		if ($usersNotToInclude)
		{
			$usersNotToInclude = " AND u.id NOT IN(" . $usersNotToInclude . ")";
		}
		else
		{
			$usersNotToInclude = " ";
		}

		// Hide on mobile or mobile and tablet both?
		$hideChatOnMobileQuery = '';

		if ($hideChatOnMobile == 1)
		{
			$hideChatOnMobileQuery = " AND ju.is_mobile <> 1";
		}
		elseif ($hideChatOnMobile == 2)
		{
			// 1 is mobile, 2 is tablets, so lets hide on all where > 1
			$hideChatOnMobileQuery = " AND ju.is_mobile < 1";
		}

		$db = JFactory::getDbo();

		global $JSNSOCIAL;

		// Friends only
		if ($fonly && $JSNSOCIAL)
		{
			$query = "SELECT DISTINCT u.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
			cu.avatar,
			 ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			 FROM #__users AS u
			 LEFT JOIN #__jsn_users AS cu ON cu.id=u.id
			 LEFT JOIN #__jsnsocial_friends AS cc ON cc.friend_id=cu.id
			 LEFT JOIN #__session AS s ON s.userid=u.id
			 LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			 WHERE cc.user_id=" . $uid . "
			 AND cc.friend_id=u.id
			 AND u.id=s.userid
			 AND s.client_id=0
			 AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery . "
			 AND u.id <> " . $uid;

			if ($filterText)
			{
				$query .= " AND (u.username LIKE '%" . $filterText . "%' OR u.name LIKE '%" . $filterText . "%')";
			}

			$query .= " ORDER BY u." . $chattitle;
		}

		// Show all
		else
		{
			$query = "SELECT DISTINCT u.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
			cu.avatar,
			 ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			 FROM #__users as u
			 LEFT JOIN #__session AS s ON s.userid=u.id
			 LEFT JOIN #__jsn_users cu ON cu.id=u.id
			 LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			 WHERE u.id IN (s.userid)
			 AND s.client_id=0
			 AND ju.state <> -1" . $usersNotToInclude . $hideChatOnMobileQuery;

			if ($filterText)
			{
				$query .= " AND ( u.username LIKE '%" . $filterText . "%' OR u.name LIKE '%" . $filterText . "%' )";
			}

			$query .= " AND u.id <> " . $uid
			. " ORDER BY u." . $chattitle;
		}

		$db->setQuery($query);
		$online_users = $db->loadObjectList();

		$online_users = $this->checkOnlineUserPermission($online_users, $filterText);

		return $online_users;
	}

	/**
	 * Get list of online users for Jomwall integration.
	 *
	 * @param   integer  $uid                Logged in user id
	 * @param   integer  $fonly              Show only friends or all users?
	 * @param   string   $chattitle          Show name or username?
	 * @param   array    $usersNotToInclude  List of users not to be shown in usrlist
	 * @param   array    $hideChatOnMobile   Hide chat on mobile OR on mobile on tablets both
	 * @param   string   $filterText         Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getJomwallOnlineUsersList($uid, $fonly, $chattitle, $usersNotToInclude, $hideChatOnMobile, $filterText = '')
	{
		// Implode blocked & blocked by users id by comma
		if ($usersNotToInclude)
		{
			$usersNotToInclude = implode(',', $usersNotToInclude);
		}

		if ($usersNotToInclude)
		{
			$usersNotToInclude = " AND u.id NOT IN(" . $usersNotToInclude . ")";
		}
		else
		{
			$usersNotToInclude = " ";
		}

		// Hide on mobile or mobile and tablet both?
		$hideChatOnMobileQuery = '';

		if ($hideChatOnMobile == 1)
		{
			$hideChatOnMobileQuery = " AND ju.is_mobile <> 1";
		}
		elseif ($hideChatOnMobile == 2)
		{
			// 1 is mobile, 2 is tablets, so lets hide on all where > 1
			$hideChatOnMobileQuery = " AND ju.is_mobile < 1";
		}

		$db = JFactory::getDbo();

		if ($fonly) // Friends  only
		{
			$query = "SELECT DISTINCT a.id AS uid, u." . $chattitle . " AS uname, u.name, u.username,
			b.avatar,
			ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			FROM
			(
				SELECT DISTINCT u.id
				FROM #__users u, #__session s, #__awd_connection a
				LEFT JOIN #__awd_wall_users b ON a.connect_to = b.user_id
				WHERE a.connect_from=" . $uid . "
				AND u.id = a.connect_to
				AND a.connect_to IN (s.userid)
				AND a.pending=0
				AND s.client_id=0
				AND u.block=0
				ORDER BY u.username
			)
			AS a
			LEFT JOIN #__awd_wall_users b ON b.user_id = a.id
			LEFT JOIN #__awd_connection AS c on c.connect_from=a.id
			LEFT JOIN #__users AS u ON u.id=a.id
			LEFT JOIN #__session AS s ON s.userid=a.id
			LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			WHERE c.connect_to=" . $uid . "
			AND c.pending=0
			AND s.client_id=0
			AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery . "
			 AND u.id <> " . $uid;

			if ($filterText)
			{
				$query .= " AND u.username LIKE '%" . $filterText . "%'";
			}

			$query .= " ORDER BY u." . $chattitle;
		}
		// Show all
		else
		{
			$query = "SELECT DISTINCT u.id AS uid,  u." . $chattitle . " AS uname, u.name, u.username,
			awu.avatar,
			ju.chat_status AS sts, ju.status_msg AS stsm, ju.state
			FROM #__users AS u
			LEFT JOIN #__session AS s ON s.userid=u.id
			LEFT JOIN #__awd_wall_users AS awu ON awu.user_id=u.id
			LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
			WHERE u.id IN (s.userid)
			AND s.client_id=0
			AND ju.state <> -1 " . $usersNotToInclude . $hideChatOnMobileQuery;

			if ($filterText)
			{
				$query .= " AND u.username LIKE '%" . $filterText . "%'";
			}

			$query .= " AND u.id <> " . $uid . "
			ORDER BY u." . $chattitle;
		}

		$db->setQuery($query);
		$online_users = $db->loadObjectList();
		$online_users = $this->checkOnlineUserPermission($online_users, $filterText);

		return $online_users;
	}

	/**
	 * Get a list of online users for group chat auto completion
	 *
	 * @param   integer  $uid         User id
	 * @param   string   $filterText  Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since   3.0
	 */
	public function getAutoCompleteUserList($uid, $filterText)
	{
		$params             = JComponentHelper::getParams('com_jbolo');
		$integration_option = $params->get('community');
		$fonly              = $params->get('fonly');
		$chattitle          = $params->get('chatusertitle');
		$hideChatOnMobile   = $params->get('hide_chat');

		// Show username OR name
		if ($params->get('chatusertitle'))
		{
			$chattitle = 'username';
		}
		else
		{
			$chattitle = 'name';
		}

		$online_users = '';

		if ($integration_option == "joomla")
		{
			$online_users = $this->getJoomlaOnlineUsersList($uid, $fonly, $chattitle, '', $hideChatOnMobile, $filterText);
		}
		elseif ($integration_option == "cb")
		{
			$online_users = $this->getCBOnlineUsersList($uid, $fonly, $chattitle, '', $hideChatOnMobile, $filterText);
		}
		elseif ($integration_option == "jomsocial")
		{
			$online_users = $this->getJomsocialOnlineUsersList($uid, $fonly, $chattitle, '', $hideChatOnMobile, $filterText);
		}
		elseif ($integration_option == "jomwall")
		{
			$online_users = $this->getJomwallOnlineUsersList($uid, $fonly, $chattitle, '', $hideChatOnMobile, $filterText);
		}
		elseif ($integration_option == "EasySocial")
		{
			$online_users = $this->getEasySocialOnlineUsersList($uid, $fonly, $chattitle, '', $hideChatOnMobile, $filterText);
		}
		elseif ($integration_option == "easyprofile")
		{
			$online_users = $this->getEasyProfileOnlineUsersList($uid, $fonly, $chattitle, '', $hideChatOnMobile, $filterText);
		}

		return $online_users;
	}

	/**
	 * Get Activity Stream Html from modules published at given module position,
	 * depending on the social integration
	 *
	 * @return  string  Module Html
	 *
	 * @since   3.0
	 */
	public function getActivityStreamHTML()
	{
		$params             = JComponentHelper::getParams('com_jbolo');
		$integration_option = $params->get('community');
		$position           = $params->get('activity_module_position');

		$html = '';

		if ($integration_option == "joomla")
		{
			$html = '';
		}
		elseif ($integration_option == "cb")
		{
			$html = $this->getActivityHTMLFromModule($position);
		}
		elseif ($integration_option == "jomsocial")
		{
			$html = $this->getActivityHTMLFromModule($position);
		}
		elseif ($integration_option == "jomwall")
		{
			$html = $this->getActivityHTMLFromModule($position);
		}
		elseif ($integration_option == "EasySocial")
		{
			$html = $this->getActivityHTMLFromModule($position);
		}
		elseif ($integration_option == "easyprofile")
		{
			$html = $this->getActivityHTMLFromModule($position);
		}

		return $html;
	}

	/**
	 * Get Activity Stream Html from modules published at given module position
	 *
	 * @param   string  $position  Module position
	 *
	 * @return  string  Module Html
	 *
	 * @since   3.0
	 */
	public function getActivityHTMLFromModule($position)
	{
		$document = JFactory::getDocument();
		$renderer = $document->loadRenderer('module');
		$modules  = JModuleHelper::getModules($position);
		$params   = array();
		$html     = '';

		foreach ($modules as $module)
		{
			$html .= $renderer->render($module, $params);
		}

		return $html;
	}

	/**
	 * Check that each user has right permission to chat
	 *
	 * @param   array   $online_users  Array of online users
	 * @param   string  $filterText    Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since   3.0
	 */
	public function checkOnlineUserPermission($online_users, $filterText)
	{
		$count                 = count($online_users);
		$online_permited_users = array();
		$k                     = 0;

		for ($i = 0; $i < $count; $i++)
		{
			if ($filterText)
			{
				if ((JFactory::getUser($online_users[$i]->uid)->authorise('core.group_chat', 'com_jbolo')) and $online_users[$i]->state)
				{
					$online_permited_users[$k++] = $online_users[$i];
				}
			}
			elseif (JFactory::getUser($online_users[$i]->uid)->authorise('core.chat', 'com_jbolo'))
			{
				$online_permited_users[$k++] = $online_users[$i];
			}
		}

		// Get user id's who are permited to chat & now check each user own preferences

		return $online_permited_users;
	}

	/**
	 * Checks if a given userid is owner of social group.
	 *
	 * @param   integer  $gid     Social group id
	 * @param   integer  $uid     User id
	 * @param   string   $client  Social group type e.g. com_tjlms.course
	 *
	 * @return  boolean  true or false
	 *
	 * @since   3.1.4
	 */
	public function isSocialGroupAdmin($gid, $uid, $client)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		switch ($client)
		{
			case 'com_community.group':
			$query = "SELECT ownerid
			 FROM #__community_groups
			 WHERE id= " . $gid . "
			 AND ownerid=" . $uid;
			break;

			// Easysocial group
			case 'com_easysocial.group':
				$query->select($db->quoteName('creator_uid'))
				->from($db->quoteName('#__social_clusters'))
				->where($db->quoteName('cluster_type') . ' = ' . $db->quote('group'))
				->where($db->quoteName('id') . ' = ' . (int) $gid)
				->where($db->quoteName('creator_uid') . ' = ' . (int) $uid);
			break;

			case 'com_tjlms.course':
			$query = "SELECT created_by
			 FROM #__tjlms_courses
			 WHERE id = " . $gid . "
			 AND created_by=" . $uid;
			break;
		}

		$db->setQuery($query);
		$ownerId = $db->loadResult();

		if ($ownerId == $uid)
		{
			return true;
		}

		return false;
	}

	/**
	 * Returns title of social group.
	 *
	 * @param   integer  $gid     Social group id
	 * @param   string   $client  Social group type e.g. com_tjlms.course
	 *
	 * @return  string  $title  Group title
	 *
	 * @since   3.1.4
	 */
	public function getSocialGroupTitle($gid, $client)
	{
		// Get the dbo
		$db = JFactory::getDbo();

		// Get query object
		$query = $db->getQuery(true);

		switch ($client)
		{
			case 'com_community.group':
			$query = "SELECT name
			 FROM #__community_groups
			 WHERE id=" . $gid;
			break;

			// Easysocial group
			case 'com_easysocial.group':
				$query->select($db->quoteName('title'))
				->from($db->quoteName('#__social_clusters'))
				->where($db->quoteName('cluster_type') . ' = ' . $db->quote('group'))
				->where($db->quoteName('id') . ' = ' . (int) $gid);
			break;

			case 'com_tjlms.course':
			$query = "SELECT title
			 FROM #__tjlms_courses
			 WHERE id=" . $gid;
			break;
		}

		$db->setQuery($query);
		$title = $db->loadResult();

		// $title = 'Group' . ': ' . $title;

		return $title;
	}

	/**
	 * Returns array of userids of social group members.
	 *
	 * @param   integer  $socialGroupId  Social group id
	 * @param   integer  $nid            Node id
	 * @param   string   $client         Social group type e.g. com_tjlms.course
	 *
	 * @return  array  $newSocialGroupMembers  Userids of social group members
	 *
	 * @since   3.1.4
	 */
	public function getSocialGroupMembers($socialGroupId, $nid, $client)
	{
		$db = JFactory::getDbo();

		$query = 'SELECT DISTINCT nu.user_id AS uid
		 FROM #__jbolo_node_users AS nu
		 WHERE nu.node_id=' . $nid;

		$db->setQuery($query);
		$existingNodeMembers = $db->loadColumn();

		switch ($client)
		{
			case 'com_community.group':
			$query = 'SELECT DISTINCT cgm.memberid AS uid
			 FROM #__community_groups_members AS cgm
			 WHERE cgm.groupid=' . $socialGroupId . '
			 AND cgm.approved=1';

			if (count($existingNodeMembers))
			{
				$existingMembers = implode(',', $existingNodeMembers);
				$query .= ' AND cgm.memberid NOT IN (' . $existingMembers . ')';
			}

			$query .= ' ORDER BY cgm.memberid';
			break;

			case 'com_easysocial.group':
			$query = 'SELECT DISTINCT scn.uid AS uid
			 FROM #__social_clusters_nodes AS scn
			 WHERE scn.cluster_id=' . $socialGroupId . '
			 AND scn.state=1';

			if (count($existingNodeMembers))
			{
				$existingMembers = implode(',', $existingNodeMembers);
				$query .= ' AND scn.uid NOT IN (' . $existingMembers . ')';
			}

			$query .= ' ORDER BY scn.uid';
			break;

			/*case 'com_tjlms.course':
			break;*/
		}

		$db->setQuery($query);
		$newSocialGroupMembers = $db->loadColumn();

		return $newSocialGroupMembers;
	}

	/**
	 * Checks if a given userid is a member of social group.
	 *
	 * @param   integer  $socialGroupId  Social group id
	 * @param   integer  $uid            User id
	 * @param   string   $client         Social group type e.g. com_tjlms.course
	 *
	 * @return  boolean  true or false
	 *
	 * @since   3.1.4
	 */
	public function isSocialGroupUser($socialGroupId, $uid, $client)
	{
		$db = JFactory::getDbo();

		switch ($client)
		{
			case 'com_community.group':
			$query = "SELECT memberid
			 FROM #__community_groups_members
			 WHERE groupid=" . $socialGroupId . "
			 AND memberid=" . $uid . "
			 AND approved=1";
			break;

			// Easysocial group
			case 'com_easysocial.group':
			$query = 'SELECT DISTINCT scn.uid AS uid
			 FROM #__social_clusters_nodes AS scn
			 WHERE scn.cluster_id=' . $socialGroupId . '
			 AND scn.state=1
			 AND scn.uid=' . $uid;
			break;

			case 'com_tjlms.course':
			$query = "SELECT user_id
			 FROM #__tjlms_enrolled_users
			 WHERE course_id=" . $socialGroupId . "
			 AND user_id=" . $uid . "
			 AND state=1";
			break;
		}

		$db->setQuery($query);
		$memberId = $db->loadResult();

		if ($memberId)
		{
			return true;
		}

		return false;
	}
}
