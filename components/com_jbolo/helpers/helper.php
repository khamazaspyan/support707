<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Component Helper
jimport('joomla.database.database');
jimport('joomla.application.component.helper');

/**
 * Class for JBolo Frontend helper
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloFrontendHelper
{
	/**
	 * Get itemid for given link
	 *
	 * @param   string   $link          link
	 * @param   integer  $skipIfNoMenu  Decide to use Itemid from $input
	 *
	 * @return  integer
	 *
	 * @since  3.0
	 */
	public function getItemId($link, $skipIfNoMenu = 0)
	{
		$itemid    = 0;
		$mainframe = JFactory::getApplication();
		$input     = JFactory::getApplication()->input;

		if ($mainframe->issite())
		{
			$JSite = new JSite;
			$menu  = $JSite->getMenu();
			$items = $menu->getItems('link', $link);

			if (isset($items[0]))
			{
				$itemid = $items[0]->id;
			}
		}

		if (!$itemid)
		{
			$db = JFactory::getDbo();

			if (JVERSION >= '3.0')
			{
				$query = "SELECT id FROM #__menu
				 WHERE link LIKE '%" . $link . "%'
				 AND published =1
				 LIMIT 1";
			}
			else
			{
				$query = "SELECT id FROM " . $db->quoteName('#__menu') . "
				 WHERE link LIKE '%" . $link . "%'
				 AND published =1
				 ORDER BY ordering
				 LIMIT 1";
			}

			$db->setQuery($query);
			$itemid = $db->loadResult();
		}

		if (!$itemid)
		{
			if ($skipIfNoMenu)
			{
				$itemid = 0;
			}
			else
			{
				$itemid  = $input->get->get('Itemid', '0', 'INT');
			}
		}

		return $itemid;
	}

	/**
	 * This function return array of js files which is loaded from tjassesloader plugin.
	 *
	 * @param   array    &$jsFilesArray     Array of Javascript files
	 * @param   integer  $firstThingsFirst  Decide these needs to be loaded first before other JS files
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getJBoloJsFiles(&$jsFilesArray, $firstThingsFirst)
	{
		if (!$this->_validateUser())
		{
			return $jsFilesArray;
		}

		// These need to be loaded first before other JS files.
		if ($firstThingsFirst)
		{
			// Load quicksearch jquery plugin.
			$jsFilesArray[] = 'media/com_jbolo/vendors/jquery.quicksearch.js';

			// Load jquery templating plugin.
			$jsFilesArray[] = 'media/com_jbolo/vendors/jquery.tmpl.min.js';

			// Load modernizr js file.
			$jsFilesArray[] = 'media/com_jbolo/vendors/modernizr-latest.js';

			// Load js for jquery ui.
			$jsFilesArray[] = 'media/com_jbolo/vendors/jquery-ui/jquery-ui-1.10.4.custom.min.js';

			// Load jquery tooltip plugin.
			// $jsFilesArray[] = 'media/com_jbolo/vendors/wtooltip.min.js';

			// Load emojione plugin
			$jsFilesArray[] = 'media/com_jbolo/vendors/emojione/emojione.min.js';
		}
		else
		{
			// Load AjaxQ plugin
			$jsFilesArray[] = 'media/com_jbolo/vendors/ajaxq.js';

			// Load jbolo chat js.
			$jsFilesArray[] = 'media/com_jbolo/js/jbolo_chat.js';
		}

		return $jsFilesArray;
	}

	/**
	 * This function return array of css files which is loaded from tjassesloader plugin.
	 *
	 * @return  array
	 *
	 * @since  3.0
	 */
	public function getJBoloCssFiles()
	{
		$cssfilesArray = array();

		if (!$this->_validateUser())
		{
			return $cssfilesArray;
		}

		$template = $this->_getJboloCurrentChatTheme();

		// Load all needed css in array.
		// Set jbolo theme css.
		$cssfilesArray[] = 'media/com_jbolo/themes/' . $template . '/style.css';

		// Load emojione plugin
		$cssfilesArray[] = 'media/com_jbolo/vendors/emojione/assets/css/emojione.min.css';

		// Load css for jquery ui.
		$cssfilesArray[] = 'media/com_jbolo/vendors/jquery-ui/themes/smoothness/jquery-ui-1.10.4.custom.min.css';

		return $cssfilesArray;
	}

	/**
	 * Get current chat theme.
	 *
	 * @return  string
	 *
	 * @since  3.0
	 */
	public function _getJboloCurrentChatTheme()
	{
		$params = JComponentHelper::getParams('com_jbolo');

		// Get current template from config.
		$template = $params->get('template');

		// Get current template from cookie if available.
		if (isset($_COOKIE["jboloTheme"]))
		{
			$template = $_COOKIE["jboloTheme"];
		}

		return $template;
	}

	/**
	 * Get JBolo Dynamic Javascript code
	 *
	 * @return  string  Javascript code inside <script> tag
	 *
	 * @since  3.0
	 */
	public function getJboloDynamicJs()
	{
		if (!$this->_validateUser())
		{
			return false;
		}

		// Load user helper if not loaded
		if (!class_exists('jbolousersHelper'))
		{
			// Helper file path
			$jbolousersHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/users.php';
			JLoader::register('jbolousersHelper', $jbolousersHelperPath);
			JLoader::load('jbolousersHelper');
		}

		$user                = JFactory::getUser();

		$jbolousersHelper    = new jbolousersHelper;
		$user_chat_pref      = $jbolousersHelper->getUserChatSettings();

		$params              = JComponentHelper::getParams('com_jbolo');
		$show_activity       = $params->get('show_activity');
		$maxChatUsers        = $params->get('maxChatUsers');
		$show_activity       = $params->get('show_activity');
		$polltime            = $params->get('polltime') * 1000;
		$jb_minChatHeartbeat = $params->get('polltime') * 1000;
		$jb_maxChatHeartbeat = $params->get('maxChatHeartbeat') * 1000;
		$template            = $this->_getJboloCurrentChatTheme();
		$dynamic_js          = '';

		$sendfile = $params->get('sendfile');

		// Check if user's user group has permission to send files.
		if (!JFactory::getUser()->authorise('core.send_file', 'com_jbolo'))
		{
			$sendfile = 0;
		}

		$groupchat = $params->get('groupchat');

		// Check if user's user group has permission for group chat.
		if (!JFactory::getUser()->authorise('core.group_chat', 'com_jbolo'))
		{
			$groupchat = 0;
		}
		else
		{
			// Check if user has opted out of group chat or not.
			if ($user_chat_pref)
			{
				if ($user_chat_pref->state == 0)
				{
					$groupchat = 0;
				}
			}
		}

		// Check that user group has a permission to add member in group chat.
		if (!JFactory::getUser()->authorise('core.add_member_in_group_chat', 'com_jbolo'))
		{
			$groupchat = 0;
		}

		$chathistory = $params->get('chathistory');

		// Check that user group has a permission to view chat history.
		if (!JFactory::getUser()->authorise('core.view_history', 'com_jbolo'))
		{
			$chathistory = 0;
		}

		$allow_user_blocking = 0;
		$allow_user_blocking = $params->get('allow_user_blocking');

		if (empty($allow_user_blocking))
		{
			$allow_user_blocking = 0;
		}

		$reqURI = JUri::root();

		// Added to fix non www to www redirects.
		// Uncomment following comment to fix non www to www redirects.

		// If host have wwww, but Config doesn't.

		/*if (isset($_SERVER['HTTP_HOST']))
		{
			if((substr_count($_SERVER['HTTP_HOST'], "www.") != 0) && (substr_count($reqURI, "www.") == 0))
			{
				$reqURI = str_replace("://", "://www.", $reqURI);
			}
			else if((substr_count($_SERVER['HTTP_HOST'], "www.") == 0) && (substr_count($reqURI, "www.") != 0))
			{
				$reqURI = str_replace("www.", "", $reqURI);
			}
		}
		*/

		$jbolousersHelper = new jbolousersHelper;

		// Load Jbolo language file
		$lang = JFactory::getLanguage();
		$lang->load('com_jbolo', JPATH_SITE);

		$dynamic_js = '
		<script type="text/javascript">
			var site_link           ="' . $reqURI . '";
			var user_id             =' . $user->id . ';
			var template            ="' . $template . '";
			var sendfile            =' . $sendfile . ';
			var groupchat           =' . $groupchat . ';
			var chathistory         =' . $chathistory . ';
			var is_su               =' . $jbolousersHelper->is_support_user() . ';
			var show_activity       =' . $show_activity . ';
			var maxChatUsers        =' . $maxChatUsers . ';
			var jb_minChatHeartbeat =' . $jb_minChatHeartbeat . ';
			var jb_maxChatHeartbeat =' . $jb_maxChatHeartbeat . ';
			var allow_user_blocking = ' . $allow_user_blocking . ';

			var me_avatar_url ="' . JUri::root() . 'media/com_jbolo/themes/"+template+"/images/me_avatar_default.png";
			var avatar_url    ="' . JUri::root() . 'media/com_jbolo/themes/"+template+"/images/avatar_default.png";

			var jbolo_lang                                  = new Array();
			jbolo_lang["COM_JBOLO_UNAUTHORZIED_REQUEST"]    ="' . JText::_('COM_JBOLO_UNAUTHORZIED_REQUEST') . '";
			jbolo_lang["COM_JBOLO_ME"]                      ="' . JText::_('COM_JBOLO_ME') . '";
			jbolo_lang["COM_JBOLO_GC_MAX_USERS"]            ="' . JText::_('COM_JBOLO_GC_MAX_USERS') . '";
			jbolo_lang["COM_JBOLO_NO_USERS_ONLINE"]         ="' . JText::_('COM_JBOLO_NO_USERS_ONLINE') . '";
			jbolo_lang["COM_JBOLO_SAYS"]                    ="' . JText::_('COM_JBOLO_SAYS') . '";
			jbolo_lang["COM_JBOLO_SET_STATUS"]              ="' . JText::_('COM_JBOLO_SET_STATUS') . '";
			jbolo_lang["COM_JBOLO_CHAT_WINDOW_EMPTY"]       ="' . JText::_('COM_JBOLO_CHAT_WINDOW_EMPTY') . '";
			jbolo_lang["COM_JBOLO_ADD_ACTIVITY_PROMPT_MSG"] ="' . JText::_('COM_JBOLO_ADD_ACTIVITY_PROMPT_MSG') . '";
			jbolo_lang["COM_JBOLO_TICKED_ID_NO_SPACE"]      ="' . JText::_('COM_JBOLO_TICKED_ID_NO_SPACE') . '";
			jbolo_lang["COM_JBOLO_CHAT"]                    ="' . JText::_('COM_JBOLO_CHAT') . '";
			jbolo_lang["COM_JBOLO_SEARCH_PEOPLE"]           ="' . JText::_('COM_JBOLO_SEARCH_PEOPLE') . '";
			jbolo_lang["COM_JBOLO_AVAILABLE"]               ="' . JText::_('COM_JBOLO_AVAILABLE') . '";
			jbolo_lang["COM_JBOLO_AWAY"]                    ="' . JText::_('COM_JBOLO_AWAY') . '";
			jbolo_lang["COM_JBOLO_BUSY"]                    ="' . JText::_('COM_JBOLO_BUSY') . '";
			jbolo_lang["COM_JBOLO_CLEAR_CUSTOM_MSGS"]       ="' . JText::_('COM_JBOLO_CLEAR_CUSTOM_MSGS') . '";
			jbolo_lang["COM_JBOLO_MINIMIZE"]                ="' . JText::_('COM_JBOLO_MINIMIZE') . '";
			jbolo_lang["COM_JBOLO_CLOSE"]                   ="' . JText::_('COM_JBOLO_CLOSE') . '";
			jbolo_lang["COM_JBOLO_INVITE"]                  ="' . JText::_('COM_JBOLO_INVITE') . '";
			jbolo_lang["COM_JBOLO_VIEW_HISTORY"]            ="' . JText::_('COM_JBOLO_VIEW_HISTORY') . '";
			jbolo_lang["COM_JBOLO_SEND_FILE"]               ="' . JText::_('COM_JBOLO_SEND_FILE') . '";
			jbolo_lang["COM_JBOLO_CLEAR_CONVERSATION"]      ="' . JText::_('COM_JBOLO_CLEAR_CONVERSATION') . '";
			jbolo_lang["COM_JBOLO_ADD_USERS"]               ="' . JText::_('COM_JBOLO_ADD_USERS') . '";
			jbolo_lang["COM_JBOLO_ADD_ACTIVITY_TO_TICKET"]  ="' . JText::_('COM_JBOLO_ADD_ACTIVITY_TO_TICKET') . '";
			jbolo_lang["COM_JBOLO_LEAVE_CHAT"]              ="' . JText::_('COM_JBOLO_LEAVE_CHAT') . '";
			jbolo_lang["COM_JBOLO_LEAVE_CHAT_CONFIRM_MSG"]  ="' . JText::_('COM_JBOLO_LEAVE_CHAT_CONFIRM_MSG') . '";
			jbolo_lang["COM_JBOLO_OFFLINE_MSG1"]            ="' . JText::_('COM_JBOLO_OFFLINE_MSG1') . '";
			jbolo_lang["COM_JBOLO_OFFLINE_MSG2"]            ="' . JText::_('COM_JBOLO_OFFLINE_MSG2') . '";
			jbolo_lang["COM_JBOLO_BLOCK_USER"]              ="' . JText::_('COM_JBOLO_BLOCK_USER') . '";
			jbolo_lang["COM_JBOLO_BLOCKED_USER_CONFIRM"]    ="' . JText::_('COM_JBOLO_BLOCKED_USER_CONFIRM') . '";

			techjoomla.jQuery(document).ready(function()
			{
				var cookie;
				var close_cookie;
				chat_window_function();
				outerlist_fun();
				list_opener();
				start_chat_session();
				setTimeout("poll_msg()",' . $polltime . ');
			});
		</script>';

		return $dynamic_js;
	}

	/**
	 * Get Jbolo Jquery Chat Templates
	 *
	 * @return  string  Jquery templates code inside <script> tag
	 *
	 * @since  3.0
	 */
	public function getJboloJqueryChatTemplates()
	{
		if (!$this->_validateUser())
		{
			return false;
		}

		$params        = JComponentHelper::getParams('com_jbolo');
		$show_activity = $params->get('show_activity');
		$template      = $this->_getJboloCurrentChatTheme();
		$jqct_code     = '';

		// Chat Message template.
		$jqct_code .= '<script id="cmessage" type="text/x-jquery-tmpl"></script>';

		// Tool tip template.
		// $jqct_code .= '<script id="tooltip_temp" type="text/x-jquery-tmpl"></script>';

		// Load chat message template into jqury template script tag.
		$jqct_code .= '<script id="chatmessage" type="text/x-jquery-tmpl">';
		$file       = JPATH_SITE . '/media/com_jbolo/themes/' . $template . '/chatmessage.htm';
		$jqct_code .= file_get_contents($file);
		$jqct_code .= '</script>';

		// Load outer list template into jqury template script tag.
		$jqct_code .= '<script id="outerlist" type="text/x-jquery-tmpl">';
		$file       = JPATH_SITE . '/media/com_jbolo/themes/' . $template . '/outerlist.htm';
		$jqct_code .= file_get_contents($file);
		$jqct_code .= '</script>';

		// Load userlist template into jqury template script tag.
		$jqct_code .= '<script id="listtemplate" type="text/x-jquery-tmpl">';
		$file       = JPATH_SITE . '/media/com_jbolo/themes/' . $template . '/list.htm';
		$jqct_code .= file_get_contents($file);
		$jqct_code .= '</script>';

		// Load logged_user template into jqury template script tag.
		$jqct_code .= '<script id="logged_user" type="text/x-jquery-tmpl">';
		$file       = JPATH_SITE . '/media/com_jbolo/themes/' . $template . '/logged_user.htm';
		$jqct_code .= file_get_contents($file);
		$jqct_code .= '</script>';

		// Load chat window template into jqury template script tag.
		$jqct_code .= '<script id="chatwindow" type="text/x-jquery-tmpl">';
		$file       = JPATH_SITE . '/media/com_jbolo/themes/' . $template . '/chatwindow.htm';
		$jqct_code .= file_get_contents($file);
		$jqct_code .= '</script>';

		// Load chat window template into jqury template script tag.
		$jqct_code .= '<script id="pdetails" type="text/x-jquery-tmpl">';
		$file       = JPATH_SITE . '/media/com_jbolo/themes/' . $template . '/pdetails.htm';
		$jqct_code .= file_get_contents($file);
		$jqct_code .= '</script>';

		if ($show_activity)
		{
			// Load activity sream only for FB template.
			if ($template == 'facebook')
			{
				// Load activitystream template into jqury template script tag.
				$jqct_code .= '<script id="activitystream" type="text/x-jquery-tmpl">';
				$file       = JPATH_SITE . '/media/com_jbolo/themes/' . $template . '/activitystream.htm';
				$jqct_code .= file_get_contents($file);
				$jqct_code .= '</script>';
			}
		}

		return $jqct_code;
	}

	/**
	 * Get Jbolo HTML code to be inserted in html on pageoad
	 *
	 * @return  string  Jbolo HTML code to be inserted in html on pageoad
	 *
	 * @since  3.0
	 */
	public function getJboloHtmlCode()
	{
		if (!$this->_validateUser())
		{
			return false;
		}

		$user          = JFactory::getUser();
		$params        = JComponentHelper::getParams('com_jbolo');
		$show_activity = $params->get('show_activity');
		$template      = $this->_getJboloCurrentChatTheme();
		$html_code     = '';

		if ($user->id)
		{
			$html_code = '
			<div style="display: none;">
				<div id="HTML5Audio" style="display: none;">
					<input id="audiofile" type="text" value="" style="display: none;"/>
				</div>
				<audio id="myaudio" src="' . JUri::base() . 'media/com_jbolo/sounds/sample.wav"  style="display: none;">
					<span id="OldSound"></span>
					<input type="button" value="Play Sound" onClick="LegacyPlaySound(\'LegacySound\')">
				</audio>
			</div>

			<button class="listopener" id="listopener">
				<div></div>
				<span id="onlineusers">' . JText::_('COM_JBOLO_CHAT') . '</span>
			</button>

			<div id="jbolouserlist_container" class="jbolouserlist_container" >';

				if ($show_activity)
				{
					// Load activity sream only for FB template.
					if ($template == 'facebook')
					{
						$html_code .= '
						<div class="jboloactivity_container">
							<div id="jboloactivity" class="jboloactivity" ></div>
						</div>
						';
					}
				}

				$html_code .= '
				<div id="jbolouserlist" class="jbolouserlist" ></div>
			</div> <!-- end of <div class="jbolouserlist"> -->

			<div class="jbolochatwin" id="jbolochatwin" style="display:none">
			</div><!-- end of <div class="jbolochatwin"> -->
			';
		}
		else
		{
			$html_code = '<b>' . JText::_('COM_JBOLO_LOGIN_CHAT') . '</b>';
		}

		return $html_code;
	}

	/**
	 * Validate of user is allowed to chat or not
	 *
	 * @return  boolean
	 *
	 * @since  3.0
	 */
	public function _validateUser()
	{
		$app = JFactory::getApplication();

		// Do not run in backend
		if ($app->isAdmin())
		{
			return false;
		}

		$user = JFactory::getUser();

		// Do not run if user is not logged in.
		if (!$user->id)
		{
			return false;
		}

		// Do not run in tmpl=component.
		$input = JFactory::getApplication()->input;
		$tmpl  = $input->get->get('tmpl', '', 'string');

		if ($tmpl == 'component')
		{
			return false;
		}

		$params    = JComponentHelper::getParams('com_jbolo');
		$hide_chat = (int) $params->get('hide_chat');

		// If chat is not to be shown to mobile/tablet users
		if ($hide_chat)
		{
			// Check if user browsing from mobile or tablet
			// Pass $hide_chat to this function
			$validateIsMobile = (int) $this->validateIsMobile($hide_chat);

			if ($validateIsMobile == 1)
			{
				$db    = JFactory::getDbo();
				$query = "SELECT id, user_id
				 FROM #__jbolo_users
				 WHERE user_id=" . $user->id;
				$db->setQuery($query);
				$jboloUser = $db->loadObject();

				$myobj = new stdclass;
				$myobj->is_mobile = 1;

				// If there is no entry for logged in user in jbolo users table, add new entry
				// This is needed for chat status and status message
				if ($jboloUser == null)
				{
					$myobj->user_id     = $user->id;
					$myobj->chat_status = 1;
					$myobj->status_msg  = JText::_('COM_JBOLO_DEFAULT_STATUS_MSG');
					$db->insertObject('#__jbolo_users', $myobj);
				}
				else
				{
					$myobj->id     = $jboloUser->id;
					$db->updateObject('#__jbolo_users', $myobj, 'id');
				}

				return false;
			}
		}

		// Load user helper if not loaded
		if (!class_exists('jbolousersHelper'))
		{
			// Helper file path
			$jbolousersHelperPath = JPATH_SITE . '/components/com_jbolo/helpers/users.php';

			JLoader::register('jbolousersHelper', $jbolousersHelperPath);
			JLoader::load('jbolousersHelper');
		}

		// Check ACL for user to allow chat.
		if (!JFactory::getUser()->authorise('core.chat', 'com_jbolo'))
		{
			return false;
		}
		else
		{
			$jbolousersHelper = new jbolousersHelper;

			// Check if user opted out of group chat.
			$user_chat_pref = $jbolousersHelper->getUserChatSettings();

			if ($user_chat_pref)
			{
				if ($user_chat_pref->state == -1)
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Check if user is  browsing from mobile or tablet device and return 0 or 1 as per allowed settings
	 *
	 * @param   integer  $hide_chat  Decide where to hide chat: 0 = don't hide, 1 = hide on mobile & tablets both, 2 = hide only on mobiles
	 *
	 * @return  integer
	 *
	 * @since  3.2.1
	 */
	public function validateIsMobile($hide_chat)
	{
		/**
		 * The PHP Regex Code to detect browser used below is taken from http://detectmobilebrowsers.com/
		 * License: This is free and unencumbered software released into the public domain.
		 * For more information, please refer to the UNLICENSE <http://unlicense.org/>
		 **/

		// Get user agent
		$useragent = $_SERVER['HTTP_USER_AGENT'];

		// Hide only on mobiles
		if ($hide_chat == 1)
		{
			// Regex for mobiles
			if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)))
			{
				return 1;
			}
		}
		// Hide on mobiles and tablets both
		elseif ($hide_chat == 2)
		{
			// Regex for mobile and tablet both
			if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)))
			{
				return 1;
			}
		}

		return 0;
	}

	/**
	 * Check if user is  browsing from mobile or tablet device
	 *
	 * @return  integer  1 if mobile, 0 otherwise
	 *
	 * @since  3.2.1
	 */
	public function isMobile()
	{
		/**
		 * The PHP Regex Code to detect browser used below is taken from http://detectmobilebrowsers.com/
		 * License: This is free and unencumbered software released into the public domain.
		 * For more information, please refer to the UNLICENSE <http://unlicense.org/>
		 **/

		// Get user agent
		$useragent = $_SERVER['HTTP_USER_AGENT'];

		$return = 0;

		// Regex for mobiles
		if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)))
		{
			$return = 1;
		}

		// Regex for mobile and tablet both
		if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)))
		{
			$return = 2;
		}

		return $return;
	}
}
