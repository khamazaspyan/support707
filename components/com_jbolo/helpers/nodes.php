<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Component Helper
jimport('joomla.application.component.helper');

// For JHtml::date
jimport('joomla.html.html');

/**
 * Class for JBolo chat nodes helper.
 *
 * @package  JBolo
 * @since    3.0
 */
class NodesHelper
{
	/**
	 * This function returns count of all active chat node participants node
	 *
	 * @param   integer  $nid  Node id
	 *
	 * @return  integer
	 *
	 * @since    3.0
	 */
	public function getActiveNodeParticipantsCount($nid)
	{
		$db    = JFactory::getDbo();

		// Get node participants info
		$query = "SELECT DISTINCT u.id AS uid
		FROM #__users AS u
		LEFT JOIN #__jbolo_node_users AS nu ON nu.user_id=u.id
		LEFT JOIN #__jbolo_users AS ju ON ju.user_id=nu.user_id
		WHERE nu.node_id=" . $nid . " AND nu.status=1";
		$db->setQuery($query);
		$participants = $db->loadObjectList();
		$count        = count($participants);

		return $count;
	}

	/**
	 * Get list of Active NodeParticipants
	 *
	 * @param   integer  $nid  e.g. 44
	 *
	 * @return  object
	 *
	 * @since    3.0
	 */
	public function getActiveNodeParticipants($nid)
	{
		$db     = JFactory::getDbo();
		$params = JComponentHelper::getParams('com_jbolo');

		// Show username OR name
		if ($params->get('chatusertitle'))
		{
			$chattitle = 'username';
		}
		else
		{
			$chattitle = 'name';
		}

		// Get node participants info
		$query = "SELECT DISTINCT u.id AS uid, u." . $chattitle . " AS name
		FROM #__users AS u
		LEFT JOIN #__jbolo_node_users AS nu ON nu.user_id=u.id
		LEFT JOIN #__jbolo_users AS ju ON ju.user_id=nu.user_id
		WHERE nu.node_id=" . $nid . " AND nu.status=1";
		$db->setQuery($query);
		$participants = $db->loadObjectList();

		return $participants;
	}

	/**
	 * This function returns info about all nodes where user is participant
	 *
	 * @param   integer  $uid  useid
	 *
	 * @return  array
	 *
	 * @since    3.0
	 */
	public function getActiveChatNodes($uid)
	{
		$db    = JFactory::getDbo();

		$query = "SELECT n.node_id AS nid, n.type AS ctyp
		FROM #__jbolo_nodes AS n
		LEFT JOIN #__jbolo_node_users AS nu ON nu.node_id=n.node_id
		WHERE nu.user_id=" . $uid;
		$db->setQuery($query);
		$nodes = $db->loadObjectList();

		return $nodes;
	}

	/**
	 * Sorts messages in each node
	 *-- INPUT
	 * nodesArray:
	 * 	0:
	 *	 	messages: null, nodeinfo: , participants:
	 *	1:	messages:
	 * 			0: {mid:2, fid:776, msg:11, ts:2012-12-28 12:44:43}
	 *	 			1: {mid:3, fid:777, msg:22, ts:2012-12-28 12:44:48}
	 *	 			2: {mid:5, fid:779, msg:44, ts:2012-12-28 12:44:59}
	 *	 			3: {mid:4, fid:778, msg:33, ts:2012-12-28 12:44:55}
	 *	 		nodeinfo:
	 *	 		participants:
	 *
	 *	-- OUTPUT
	 *	 	nodesArray:
	 *	 		0:
	 *	 			messages: null
	 *	 			nodeinfo:
	 *	 			participants:
	 *			 1:
	 *				 messages:
	 *					 0: {mid:2, fid:776, msg:11, ts:2012-12-28 12:44:43}
	 *					 1: {mid:3, fid:777, msg:22, ts:2012-12-28 12:44:48}
	 *					 3: {mid:4, fid:778, msg:33, ts:2012-12-28 12:44:55}
	 *					 3: {mid:5, fid:779, msg:44, ts:2012-12-28 12:44:59}
	 *				 nodeinfo:
	 *			  	 participants:
	 *
	 * @param   array   $nodesArray  Array of nodes
	 * @param   string  $column      Column based on which sorting will be done
	 * @param   string  $order       Sorting order direction ASC or DESC
	 *
	 * @return  array
	 *
	 * @since    3.0
	 */
	public function sortMessages($nodesArray, $column, $order)
	{
		foreach ($nodesArray as &$node)
		{
			// Pointer
			$array = & $node['messages'];
			$array = $this->multi_d_sort($array, $column, $order);
		}

		return $nodesArray;
	}

	/**
	 * Sorts messages in each node
	 *-- INPUT
	 *	messages:
	 * 		0: {mid:2, fid:776, msg:11, ts:2012-12-28 12:44:43}
	 *		1: {mid:3, fid:777, msg:22, ts:2012-12-28 12:44:48}
	 *		2: {mid:5, fid:779, msg:44, ts:2012-12-28 12:44:59}
	 *		3: {mid:4, fid:778, msg:33, ts:2012-12-28 12:44:55}
	 *
	 *	-- OUTPUT
	 * 	messages:
	 *  	0: {mid:2, fid:776, msg:11, ts:2012-12-28 12:44:43}
	 *		1: {mid:3, fid:777, msg:22, ts:2012-12-28 12:44:48}
	 *		2: {mid:4, fid:778, msg:33, ts:2012-12-28 12:44:55}
	 *		3: {mid:5, fid:779, msg:44, ts:2012-12-28 12:44:59}
	 *
	 * @param   array   $array   Array of nodes
	 * @param   string  $column  Column based on which sorting will be done
	 * @param   string  $order   Sorting order direction ASC or DESC
	 *
	 * @return  array
	 *
	 * @since    3.0
	 */
	public function multi_d_sort($array, $column, $order)
	{
		if (isset($array) && count($array))
		{
			foreach ($array as $key => $row)
			{
				$orderby[$key] = $row->$column;
			}

			if ($order == 'asc')
			{
				array_multisort($orderby, SORT_ASC, $array);
			}
			else
			{
				array_multisort($orderby, SORT_DESC, $array);
			}
		}

		return $array;
	}

	/**
	 * Update Node Participants
	 *
	 * @param   array    $nodesArray  Array of node details
	 * @param   integer  $uid         User id
	 *
	 * @return  array
	 *
	 * @since    3.0
	 */
	public function updateNodeParticipants($nodesArray, $uid)
	{
		foreach ($nodesArray as $index => $node)
		{
			$pdata                              = $this->getNodeParticipants($node['nodeinfo']->nid, $uid);
			$participants                       = $pdata['participants'];
			$nodesArray[$index]['participants'] = $participants;
		}

		return $nodesArray;
	}

	/**
	 * Update Chat window title for given array of nodes
	 *
	 * @param   array    $nodesArray  Array of node details
	 * @param   integer  $uid         User id
	 *
	 * @return  array
	 *
	 * @since    3.0
	 */
	public function updateWindowTitles($nodesArray, $uid)
	{
		foreach ($nodesArray as $index => $node)
		{
			$nodesArray[$index]['nodeinfo']->wt = $this->getNodeTitle($node['nodeinfo']->nid, $uid, $node['nodeinfo']->ctyp);
		}

		return $nodesArray;
	}

	/**
	 * Update Chat window status - online / offline / busy etc
	 *
	 * @param   array    $nodesArray  Array of node details
	 * @param   integer  $uid         User id
	 *
	 * @return  array
	 *
	 * @since    3.0
	 */
	public function updateWindowStatus($nodesArray, $uid)
	{
		foreach ($nodesArray as $index => $node)
		{
			$nodesArray[$index]['nodeinfo']->ns = $this->getNodeStatus($node['nodeinfo']->nid, $uid, $node['nodeinfo']->ctyp);
		}

		return $nodesArray;
	}

	/**
	 * Returns a Node status - (online/offline/busy) for given array of nodes
	 *
	 * @param   array    $nodesArray  Array of node details
	 * @param   integer  $uid         User id
	 *
	 * @return  array
	 *
	 * @since    3.0
	 */
	public function getNodeStatusArray($nodesArray, $uid)
	{
		$nodeStatusArray = array();

		foreach ($nodesArray as $node)
		{
			$nodeStatusArray[$node['nodeinfo']->nid] = $this->getNodeStatus($node['nodeinfo']->nid, $uid, $node['nodeinfo']->ctyp);
		}

		return $nodeStatusArray;
	}

	/**
	 * Check if given user is part of node or not
	 *
	 * @param   integer  $uid  User id
	 * @param   integer  $nid  Node id
	 *
	 * @return  integer
	 *
	 * @since    3.0
	 */
	public function isNodeParticipant($uid, $nid)
	{
		$db    = JFactory::getDbo();
		$query = "SELECT user_id, status
		FROM #__jbolo_node_users
		WHERE node_id=" . $nid . " AND user_id=" . $uid;
		$db->setQuery($query);
		$p = $db->loadObject();

		// Not a valid group chat participant
		if (!$p)
		{
			$isParticipant = 0;
		}
		// Active  participant
		elseif ($p->status == 1)
		{
			$isParticipant = 1;
		}
		// Inactive  participant (who left chat)
		elseif ($p->status == 0)
		{
			$isParticipant = 2;
		}

		return $isParticipant;
	}

	/**
	 * Checks if a 1to1 node exist for given pair of users
	 * Called from -
	 * - models/nodes.php
	 * -- functions initiateNode()
	 *
	 * @param   integer  $uid  e.g. 776 who initiates chat
	 * @param   integer  $pid  e.g. 777 participant
	 *
	 * @return  integer  $node_id_found  e.g. 3 OR 0 (no node found)
	 */
	public function checkNodeExists($uid, $pid)
	{
		$db    = JFactory::getDbo();

		// Check if a node already exists for these 2 users
		// Where - subquery => get all such nodes where uid or participant is a owner
		$query = "SELECT nu.node_id AS nid, GROUP_CONCAT(nu.user_id) AS users
		FROM `#__jbolo_node_users` AS nu
		LEFT JOIN `#__jbolo_nodes` AS nd ON nd.node_id = nu.node_id
		WHERE nu.node_id IN
		(
			SELECT nd.node_id
			FROM `#__jbolo_nodes` AS nd
			WHERE nd.type=1
			AND (nd.owner=" . $uid . " OR nd.owner=" . $pid . ")
		)
		GROUP BY nu.node_id";
		$db->setQuery($query);
		$nodes = $db->loadObjectList();

		// Check if 2 users have shared a node already
		// 776,778
		$users1        = $uid . "," . $pid;

		// 778,776
		$users2        = $pid . "," . $uid;
		$node_id_found = 0;
		$count         = count($nodes);

		for ($i = 0; $i < $count; $i++)
		{
			// If node found for 2 users, exit loop
			if ($nodes[$i]->users == $users1 || $nodes[$i]->users == $users2)
			{
				$node_id_found = $nodes[$i]->nid;
				break;
			}
		}

		return $node_id_found;
	}

	/**
	 * Returns node title to be shown as chat window title
	 * - for group chat shows logged in user name at the end
	 *
	 * Called from -
	 * - models/nodes.php
	 * 	-- functions initiateNode()
	 *
	 * Calls -
	 * - helpers/nodes.php [same file]
	 * 	-- functions => getNodeParticipants()
	 *
	 * @param   integer  $nid   e.g. 2 node id
	 * @param   integer  $uid   e.g. 777 logged in user id
	 * @param   integer  $ctyp  e.g. 1 OR 2 chat node type (1-1to OR 2-group chat)
	 *
	 * @return  string  windowTitle e.g. (3) manoj, dipti, me
	 */
	public function getNodeTitle($nid, $uid, $ctyp)
	{
		$params = JComponentHelper::getParams('com_jbolo');

		// Show username OR name
		if ($params->get('chatusertitle'))
		{
			$chattitle = 'username';
		}
		else
		{
			$chattitle = 'name';
		}

		$pdata        = $this->getNodeParticipants($nid, $uid);
		$participants = $pdata['participants'];
		$count        = count($participants);

		$windowTitle = '';

		// For 1to1 chat
		if ($ctyp == 1)
		{
			foreach ($participants as $p)
			{
				// For 1to1 chat, use other user's name as title
				if ($p->uid != $uid)
				{
					$windowTitle = JFactory::getUser($p->uid)->$chattitle;
				}
			}
		}
		// Group chat
		else
		{
			$db    = JFactory::getDbo();

			// Checks if a group chat node is active.
			$query = "SELECT title
			 FROM `#__jbolo_nodes`
			 WHERE node_id=" . $nid;

			$db->setQuery($query);
			$windowTitle = $db->loadResult();

			if (!$windowTitle)
			{
				foreach ($participants as $p)
				{
					// If participant is active in node, use his name
					if ($p->active == 1)
					{
						if ($p->uid != $uid)
						{
							$windowTitle .= JFactory::getUser($p->uid)->$chattitle . ', ';
						}
					}
					else
					{
						// We are showing count for active users as well, so modify it
						$count--;
					}
				}

				// Append 'me' at the end
				$flag = 0;

				foreach ($participants as $p)
				{
					if (!$flag && $p->active == 1)
					{
						if ($p->uid == $uid)
						{
							$windowTitle .= JText::_('COM_JBOLO_ME');
							$flag = 1;
						}
					}
					elseif ($flag)
						break;
				}
				// Remove  trailing comma
				if (!$flag)
				{
					$windowTitle = trim($windowTitle, ', ');
				}
			}

			// Make it look like - (3) manoj, dipti, me
			$windowTitle = '(' . $count . ') ' . $windowTitle;
		}

		// @TODO might need to change this - done to trim down chat window title
		// Ideally  should be done from CSS

		/*if (strlen($windowTitle) > 17)
		{
			$windowTitle=substr($windowTitle,0,17).' ...';
		}*/

		return $windowTitle;
	}

	/**
	 * Returns node status to be shown as chat window status
	 *
	 * @param   integer  $nid   e.g. 2 node id
	 * @param   integer  $uid   e.g. 777 logged in user id
	 * @param   integer  $ctyp  e.g. 1 OR 2 chat node type (1-1to OR 2-group chat)
	 *
	 * Called from -
	 * - models/nodes.php
	 * 	-- functions initiateNode()
	 *
	 * Calls -
	 * - helpers/nodes.php [same file]
	 * 	-- functions => getNodeParticipants()
	 *
	 * @return integer  windowTitle e.g. (3) manoj, dipti, me
	 *
	 * @since  3.0
	 */
	public function getNodeStatus($nid, $uid, $ctyp)
	{
		$pdata        = $this->getNodeParticipants($nid, $uid);
		$participants = $pdata['participants'];

		// Default online
		$nodeStatus   = 1;

		// For 1to1 chat
		if ($ctyp == 1)
		{
			foreach ($participants as $p)
			{
				if ($p->uid != $uid)
				{
					$nodeStatus = $p->sts;

					// If user is online then check that user has not blocked by other user
					if ($nodeStatus != 4)
					{
						$result = $this->isBlockedBy($p->uid, $uid);

						// If participants blocked logged in user then show offline chat status of participants to logged in user
						if ($result)
						{
							$nodeStatus = 4;
						}
					}
				}
			}
		}
		// Group chat
		else
		{
			$flag       = 1;

			// Default  offline
			$nodeStatus = 4;

			foreach ($participants as $p)
			{
				if ($flag)
				{
					if ($p->uid != $uid)
					{
						if ($p->sts) // If any of the participant is online/active, group chat is active
						{
							// Online
							$nodeStatus = 1;
							$flag       = 0;
						}
					}
				}
			}
		}

		return $nodeStatus;
	}

	/**
	 * This function returns is user blocked by participants
	 *
	 * @param   integer  $pid  Participant user id
	 * @param   integer  $uid  user id
	 *
	 * @return  integer
	 */
	public function isBlockedBy($pid, $uid)
	{
		$db    = JFactory::getDbo();
		$query = "SELECT id FROM
		`#__jbolo_privacy`
		WHERE blocked_by_user_id = " . $pid . " AND blocked_user_id=" . $uid;
		$db->setQuery($query);

		return $result = $db->loadResult();
	}

	/**
	 * This function returns info about all active chat node participants for given user and node
	 *
	 * @param   integer  $nid  e.g. 2 node id
	 * @param   integer  $uid  e.g. 777 logged in user id
	 *
	 * @return  array  $return_data
	 *
	 * @since 3.0
	 *
	 * called from -
	 * - helpers/nodes.php (this file)
	 * 	- functions getNodeTitle() getNodeStatus()
	 *
	 * calls -
	 * - helpers/users.php
	 * 	-- functions => checkOnlineStatus()
	 * - helpers/integrationsHelper.php
	 * 	-- functions => getUserAvatar() getUserProfileUrl()
	 */
	public function getNodeParticipants($nid, $uid)
	{
		$params = JComponentHelper::getParams('com_jbolo');

		// Show username OR name
		if ($params->get('chatusertitle'))
		{
			$chattitle = 'username';
		}
		else
		{
			$chattitle = 'name';
		}

		$db    = JFactory::getDbo();

		// Get node participants info
		$query = "SELECT DISTINCT u.id AS uid, u.$chattitle AS uname, u.name, u.username,
		 ju.chat_status AS sts, ju.status_msg AS stsm, nu.status AS active
		 FROM #__users AS u
		 LEFT JOIN #__jbolo_node_users AS nu ON nu.user_id=u.id
		 LEFT JOIN #__jbolo_users AS ju ON ju.user_id=nu.user_id
		 WHERE nu.node_id=" . $nid . "
		 ORDER BY u.username";
		$db->setQuery($query);
		$participants = $db->loadObjectList();

		$count        = count($participants);

		// Use  integrationsHelper
		$integrationsHelper = new integrationsHelper;

		// Use  users helper
		$jbolousersHelper   = new jbolousersHelper;

		for ($i = 0; $i < $count; $i++)
		{
			$participantsNew[$participants[$i]->uid]         = new stdClass;
			$participantsNew[$participants[$i]->uid]->uid    = $participants[$i]->uid;
			$participantsNew[$participants[$i]->uid]->uname  = $participants[$i]->uname;
			$participantsNew[$participants[$i]->uid]->name   = $participants[$i]->name;
			$participantsNew[$participants[$i]->uid]->stsm   = $participants[$i]->stsm;
			$participantsNew[$participants[$i]->uid]->active = $participants[$i]->active;

			// Get online status
			$onlineStatus                                 = $jbolousersHelper->checkOnlineStatus($participants[$i]->uid);

			if ($onlineStatus)
			{
				// Online
				$participantsNew[$participants[$i]->uid]->sts = $participants[$i]->sts;
			}
			else
			{
				// Offline
				$participantsNew[$participants[$i]->uid]->sts = 4;
			}

			// Get avatar
			$participantsNew[$participants[$i]->uid]->avtr = $integrationsHelper->getUserAvatar($participants[$i]->uid);

			// Get profile url
			$participantsNew[$participants[$i]->uid]->purl = $integrationsHelper->getUserProfileUrl($participants[$i]->uid);

			// Imp to unset old indexes as we are setting new indexes
			unset($participants[$i]);
		}

		$return_data['participants'] = $participantsNew;

		return $return_data;
	}

	/**
	 * This function returns chat node type
	 *
	 * @param   integer  $nid  e.g. 2 node id
	 *
	 * @return  integer  $nodeType e.g. 1 OR 2
	 *
	 * @since  3.0
	 *
	 * called from -
	 * - models/nodes.php (this file)
	 * 	- functions leaveChat() getNodeStatus()
	 */
	public function getNodeType($nid)
	{
		$db    = JFactory::getDbo();
		$query = "SELECT type
		FROM #__jbolo_nodes
		WHERE node_id=" . $nid;
		$db->setQuery($query);
		$nodeType = $db->loadResult();

		// 1 OR 2 i.e. 1to1 or group chat
		return $nodeType;
	}

	/**
	 * Checks if a group chat node exist for given social-group id.
	 *
	 * @param   integer  $gid     e.g. 3 social-group id
	 * @param   string   $client  Social group client e.g. community.groups
	 *
	 * @return  integer  $node_id_found  e.g. 3 OR 0 (no node found)
	 *
	 * @since 3.0.3
	 *
	 * called from -
	 * - models/nodes.php
	 * 	- functions setGroupChat()
	 */
	public function checkSocialGroupNodeExists($gid, $client)
	{
		$db = JFactory::getDbo();

		// Checks if a group chat node exist for given social-group id.
		$query = "SELECT node_id AS nid
		 FROM `#__jbolo_nodes`
		 WHERE type = 2
		 AND group_id=" . $gid . "
		 AND client='" . $client . "'";

		$db->setQuery($query);
		$nid = $db->loadResult();

		return $nid;
	}

	/**
	 * Check if a given group chat node is active
	 *
	 * @param   integer  $nid  Node id
	 *
	 * @return  integer
	 */
	public function isActiveGroupNode($nid)
	{
		$db = JFactory::getDbo();

		// Checks if a group chat node is active.
		$query = "SELECT status
		 FROM `#__jbolo_nodes`
		 WHERE node_id=" . $nid;

		$db->setQuery($query);
		$status = $db->loadResult();

		return $status;
	}

	/**
	 * Check if given node is a Social Group Chat Node
	 *
	 * @param   integer  $nid  Node id
	 *
	 * @return  integer
	 */
	public function isSocialGroupChatNode($nid)
	{
		$db = JFactory::getDbo();

		// Checks if a group chat node is active.
		$query = "SELECT group_id
		 FROM `#__jbolo_nodes`
		 WHERE node_id=" . $nid;

		$db->setQuery($query);
		$status = $db->loadResult();

		return $status;
	}

	/**
	 * Retrieve Social Group Client from given node id
	 *
	 * @param   integer  $nid  Node id
	 *
	 * @return  integer
	 */
	public function getSocialGroupClient($nid)
	{
		$db = JFactory::getDbo();

		// Checks if a group chat node is active.
		$query = "SELECT client
		 FROM `#__jbolo_nodes`
		 WHERE node_id=" . $nid;

		$db->setQuery($query);
		$client = $db->loadResult();

		return $client;
	}

	/**
	 * This function ->
	 * -- pushes a chat mesages sent by user to a node(to server/database actually)
	 * -- it also updates chat xref table so that all participants can get this message when polling is done
	 * -- it also updates session by adding new message data into session
	 * -- and finally echoes json reponse
	 *
	 * INPUT VARIABLES-@POST
	 * uid:776
	 * nid:38
	 * msg:hiiiiii
	 * ts:1355822592
	 * type:1
	 *
	 * OUTPUT VARIABLE-@JSON
	 * pushChat_response - @json array {"nid":"38", "uid":"776", "mid":"5", "sent":"1", "msg":"hiiiiii"}
	 *
	 * @param   integer  $uid           User id
	 * @param   array    $participants  Participants array
	 * @param   integer  $nid           Node id
	 * @param   string   $msg           Chat message
	 *
	 * @return  array
	 */
	public function pushChatToNode($uid, $participants, $nid, $msg)
	{
		// Log pushChatToNode

		/*if (JFactory::getUser()->id == 63)
		{
			$logf = JPATH_SITE . '/components/com_jbolo/' . JFactory::getUser()->id . '.php';
			$f    = @fopen($logf, 'a+');

			if ($f)
			{
				$log = "";

				if (isset($_SESSION['jbolo']))
				{
					$log .= "\n" . 'pushChatToNode helper START session o/p' . "\n";
					$log .= print_r($_SESSION['jbolo'], true);
				}

				@fwrite($f, $log);
				@fclose($f);
			}
		}*/

		// Validate user
		// $pushChat_response = $this->validateUserLogin();
		$pushChat_response['validate'] = new stdclass;

		$db = JFactory::getDBO();

		// Get inputs from posted data

		/*$input = JFactory::getApplication()->input;
		$post = $input->post;
		*/

		// Get msg
		if (get_magic_quotes_gpc())
		{
			$msg = stripslashes($msg);
		}

		// Validate if nid is not specified
		if (!$nid)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_INVALID_NODE_ID');

			return $pushChat_response;
		}

		// Validate if message is not blank
		if ($msg === null)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_EMPTY_MSG');

			return $pushChat_response;
		}

		// Validate if this user is participant of this node
		$isNodeParticipant = $this->isNodeParticipant($uid, $nid);

		// Error handling for inactive user
		if ($isNodeParticipant == 2)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_INACTIVE_MEMBER_MSG');

			return $pushChat_response;
		}

		// Error handling for not member/unauthorized access to this group chat
		if (!$isNodeParticipant)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_NON_MEMBER_MSG');

			return $pushChat_response;
		}

		// Check if curent node is active or not.
		$isActiveGroupNode = $this->isActiveGroupNode($nid);

		// If this is not an active chat node show an error
		if (!$isActiveGroupNode)
		{
			$pushChat_response['validate']->error     = 1;
			$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_NODE_NOT_ACTIVE_MSG');

			return $pushChat_response;
		}

		// Get participants for this node

		/*$query = "SELECT user_id
		FROM #__jbolo_node_users
		WHERE node_id = ".$nid."
		AND user_id <> ".$uid."
		AND status = 1";//status indicates that user is still part of node
		$db->setQuery($query);
		$participants = $db->loadColumn();*/

		// Get Node type
		$ctyp = $this->getNodeType($nid);

		// One to one chat
		if ($ctyp == 1)
		{
			// Check is participants blocked logged in user
			$isBlocked = $this->isBlockedBy($participants[0], $uid);

			if ($isBlocked)
			{
				$pushChat_response['validate']->error     = 1;
				$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_PATICIPANTS_BLOCKED_YOU');

				return $pushChat_response;
			}
		}
		/*else
		{
			$socialGroupChatNode = $this->isSocialGroupChatNode($nid);

			if($socialGroupChatNode)
			{
				$client = $this->getSocialGroupClient($nid);

				$integrationsHelper = new integrationsHelper;

				$isSocialGroupUser = $integrationsHelper->isSocialGroupUser($socialGroupChatNode, $uid, $client);

				$isSocialGroupAdmin = $integrationsHelper->isSocialGroupAdmin($socialGroupChatNode, $uid, $client);

				if (!$isSocialGroupUser && !$isSocialGroupAdmin)
				{
					$pushChat_response['validate']->error     = 1;
					$pushChat_response['validate']->error_msg = JText::_('COM_JBOLO_NON_GROUP_MEMBER_MSG');

					return $pushChat_response;
				}
			}
		}*/

		/*@TODO decide what's better? - store first and then process message
		OR - process first and then display
		Trigger plugins to process message text
		*/

		// Backup original message, needed for social sync
		// $originalMsg = $msg;

		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('jbolo', 'plg_jbolo_textprocessing');

		// Process urls
		$processedText = $dispatcher->trigger('processUrls', array($msg));
		$msg           = $processedText[0];

		/*// Process smilies
		$processedText = $dispatcher->trigger('processSmilies', array($msg));
		$msg           = $processedText[0];*/

		// Process bad words
		$processedText = $dispatcher->trigger('processBadWords', array($msg));
		$msg           = $processedText[0];

		// Add msg to database
		$msgObj             = new stdclass;
		$msgObj->from       = $uid;
		$msgObj->to_node_id = $nid;
		$msgObj->msg        = $msg;
		$msgObj->msg_type   = 'txt';

		// //NOTE - date format date("Y-m-d H:i:s");
		// $msgObj->time  = date("Y-m-d H:i:s");
		$date = JFactory::getDate();
		$msgObj->time       = $date->toSql();

		// @TODO need to chk if this is really used or is it for future proofing
		$msgObj->sent = 1;
		$db->insertObject('#__jbolo_chat_msgs', $msgObj);

		// Get last insert id
		$new_mid = $db->insertid();

		// Update msg xref table
		if ($new_mid)
		{
			$count = count($participants);

			// Add entry for all users against this msg
			for ($i = 0; $i < $count; $i++)
			{
				$xrefMsgObj             = new stdclass;
				$xrefMsgObj->msg_id     = $new_mid;
				$xrefMsgObj->node_id    = $nid;
				$xrefMsgObj->to_user_id = $participants[$i];
				$xrefMsgObj->delivered  = 0;
				$xrefMsgObj->read       = 0;

				$db->insertObject('#__jbolo_chat_msgs_xref', $xrefMsgObj);
			}
		}

		// Prepare json response
		$query = "SELECT chm.to_node_id AS nid, chm.from AS uid, chm.msg_id AS mid, chm.sent, chm.msg
	 	FROM #__jbolo_chat_msgs AS chm
	 	WHERE chm.msg_id=" . $new_mid;
		$db->setQuery($query);
		$node_d                                 = $db->loadObject();
		$pushChat_response['pushChat_response'] = $node_d;

		// Comment from here
		// Add this msg to session
		$query = "SELECT m.msg_id AS mid, m.from AS fid, m.msg, m.time AS ts
		FROM #__jbolo_chat_msgs AS m
		LEFT JOIN #__jbolo_chat_msgs_xref AS mx ON mx.msg_id=m.msg_id
		WHERE m.msg_id=" . $new_mid . " AND m.sent=1";
		$db->setQuery($query);
		$msg_dt     = $db->loadObject();
		$msg_dt->ts = JHtml::date($msg_dt->ts, JText::_('COM_JBOLO_SENT_AT_FORMAT'), true);

		// Update session by adding this msg against corresponding node
		// If jbolo nodes array is set
		if (isset($_SESSION['jbolo']['nodes']))
		{
			// Count nodes in session
			$nodecount = count($_SESSION['jbolo']['nodes']);

			// Loop through all nodes
			for ($k = 0; $k < $nodecount; $k++)
			{
				// If k'th node is set
				if (isset($_SESSION['jbolo']['nodes'][$k]))
				{
					// If nodeinfo is set
					if (isset($_SESSION['jbolo']['nodes'][$k]['nodeinfo']))
					{
						// If the required node is found in session
						if ($_SESSION['jbolo']['nodes'][$k]['nodeinfo']->nid == $nid)
						{
							// Initialize mesasge count for node found to 0
							$mcnt = 0;

							// Check if the node found has messages stored in session
							if (isset($_SESSION['jbolo']['nodes'][$k]['messages']))
							{
								// If yes count msgs
								$mcnt = count($_SESSION['jbolo']['nodes'][$k]['messages']);

								// Add new mesage at the end
								$_SESSION['jbolo']['nodes'][$k]['messages'][$mcnt] = $msg_dt;
							}
							else
							{
								// Add new mesage at the start
								$_SESSION['jbolo']['nodes'][$k]['messages'][0] = $msg_dt;
							}
						}
					}
				}
				else
				{
					// If node is not present in session
					// This situation is not expected ideally
				}
			}
		}

		// Comment ends here

		// Log polling

		/*if (JFactory::getUser()->id == 63)
		{
			$logf = JPATH_SITE . '/components/com_jbolo/' . JFactory::getUser()->id . '.php';
			$f    = @fopen($logf, 'a+');

			if ($f)
			{
				$log = "";

				if (isset($_SESSION['jbolo']))
				{
					$log .= "\n" . 'pushChatToNode helper END session o/p' . "\n";
					$log .= print_r($_SESSION['jbolo'], true);
				}

				@fwrite($f, $log);
				@fclose($f);
			}
		}*/

		return $pushChat_response;
	}

	/**
	 * This function ->
	 * -- creates a new chat node or opens an existing node for given set of users
	 * -- if a new node is created it adds participants against this node
	 * -- it also updates session by adding new node data into session
	 * -- and finally echoes json reponse
	 *
	 * OUTPUT VARIABLE-@JSON
	 * nodeinfo - json array nodeinfo: {ctyp: "1", nid: "1", wt: "user1"}
	 *
	 * called from -
	 * - contoller.php
	 * 	-- functions => initiateNode()
	 *
	 * calls -
	 * - models/nodes.php [same file]
	 * 	-- functions => validateUser()
	 * - helpers/nodes.php
	 * 	-- functions => checkNodeExists() getNodeTitle() getNodeStatus() getNodeParticipants()
	 *
	 * @param   integer  $uid           User id
	 * @param   array    $participants  Participants array
	 *
	 * @return  array
	 */
	public function initiateNode($uid, $participants)
	{
		// Validate user
		$ini_node['validate'] = new stdclass;

		/*if(!$uid)
		{
		$response['validate']->error=1;
		$response['validate']->error_msg=JText::_('COM_JBOLO_UNAUTHORZIED_REQUEST');
		header('Content-type: application/json');
		echo json_encode($response);
		jexit();
		}
		$ini_node = $this->validateUserLogin();*/

		$db   = JFactory::getDBO();
		$user = JFactory::getUser();

		// $uid  = $user->id;
		// Validate uid
		if (!$uid)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_PARTICIPANT');

			return $ini_node;
		}

		// Get participant from post

		/*$input = JFactory::getApplication()->input;
		$post  = $input->post;
		$pid = $input->post->get('pid','','INT');//participant id e.g. 778
		*/

		// Validate participants
		if (!count($participants))
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_PARTICIPANT');

			return $ini_node;
		}

		$pid = $participants[0];

		// Check if node exists
		// Use  nodesHelper
		$node_id_found = $this->checkNodeExists($uid, $pid);

		// If no existing node found
		if (!$node_id_found)
		{
			// Create new node
			$myobj        = new stdclass;
			$myobj->title = null;
			$myobj->type  = 1;
			$myobj->owner = $uid;

			// //NOTE - date format date("Y-m-d H:i:s");
			// $myobj->time  = date("Y-m-d H:i:s");
			$date = JFactory::getDate();
			$myobj->time       = $date->toSql();

			$db->insertObject('#__jbolo_nodes', $myobj);

			// Get last insert id
			$new_node_id = $db->insertid();

			if ($db->insertid())
			{
				// Add participants against newly created node
				for ($i = 0; $i < 2; $i++)
				{
					$myobj          = new stdclass;
					$myobj->node_id = $new_node_id;

					// Add entry for both users one after other
					$myobj->user_id = ($i == 0) ? $uid : $pid;
					$myobj->status  = 1;
					$db->insertObject('#__jbolo_node_users', $myobj);
				}
			}
		}
		// Node already exists
		else
		{
			$new_node_id = $node_id_found;
		}

		// Prepare json response
		$query = "SELECT node_id AS nid, type AS ctyp
		FROM #__jbolo_nodes
		WHERE node_id=" . $new_node_id;
		$db->setQuery($query);
		$node_d = $db->loadObject();

		$ini_node['nodeinfo']     = $node_d;

		// Get chat window title(wt)
		$ini_node['nodeinfo']->wt = $this->getNodeTitle($new_node_id, $uid, $ini_node['nodeinfo']->ctyp);

		// Get chatbox status (node status - ns)
		$ini_node['nodeinfo']->ns = $this->getNodeStatus($new_node_id, $uid, $ini_node['nodeinfo']->ctyp);

		// Use  this
		// Get participants list
		$participants = $this->getNodeParticipants($new_node_id, $uid);

		// Update  this node info in session
		$d = 0;

		// Check if 'nodes' array is set
		if (!isset($_SESSION['jbolo']['nodes']))
		{
			// If nodes array is not set, push new node at the start in nodes array
			$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];
			$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
		}
		else // If nodes array is set
		{
			$node_ids  = array();
			$nodecount = count($_SESSION['jbolo']['nodes']);

			for ($d = 0; $d < $nodecount; $d++)
			{
				if (isset($_SESSION['jbolo']['nodes'][$d]))
				{
					// Get all node ids set in session
					$node_ids[$d] = $_SESSION['jbolo']['nodes'][$d]['nodeinfo']->nid;
				}
			}
			// If the current node is not present in session,
			// We add it into session
			if (!in_array($ini_node['nodeinfo']->nid, $node_ids))
			{
				if ($nodecount) // If nodecount is >0, push new node data at the end of array
				{
					$_SESSION['jbolo']['nodes'][$nodecount]['nodeinfo']     = $ini_node['nodeinfo'];
					$_SESSION['jbolo']['nodes'][$nodecount]['participants'] = $participants['participants'];
				}
				else // If nodecount is 0, push this at start i.e. 0th position in array
				{
					$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];
					$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
				}
			}
		}

		return $ini_node;
	}

	/**
	 * Checks if a group node exist for given pair of users
	 *
	 * @param   integer  $uid               e.g. 776 who initiates chat
	 * @param   integer  $owner             e.g. 777 owner here is chat message owner(who is sending current chat) in most cases
	 * @param   array    $participants      e.g. 777 participant
	 * @param   array    $respectNodeOwner  e.g. 777 make sure conversation owner is the node owner
	 *
	 * @return integer node_id_found  e.g. 3 OR 0 (no node found)
	 *
	 * @since JBolo 3.0
	 *
	 * called from -
	 * - models/nodes.php
	 * 	- functions initiateNode()
	 */
	public function checkGroupChatNodeExists($uid, $owner, $participants, $respectNodeOwner = 0)
	{
		// Add sender into plist 64 65 66 => 63 64 65 66
		$participants[] = $uid;

		$db = JFactory::getDbo();

		// Check if a node already exists for these 2 users
		// Get a list of nodes with participants in ascending userid order
		$query = "SELECT nu.node_id AS nid,
		 GROUP_CONCAT(nu.user_id) AS users
		 FROM
		(
			 SELECT nu2.user_id, nu2.node_id
			 FROM `#__jbolo_node_users` AS nu2
			 WHERE status=1
			 ORDER BY nu2.node_id, nu2.user_id ASC
		) AS nu
		 LEFT JOIN `#__jbolo_nodes` AS nd ON nd.node_id = nu.node_id
		 WHERE nd.type=2";

		if ($respectNodeOwner)
		{
			$query .= " AND nd.owner=" . $owner;
		}
		else
		{
			$query .= " AND nd.owner IN (" . implode(',', $participants) . ")";
		}

		// Order by node id in desc order to get latest node
		$query .= " GROUP BY nu.node_id
		 ORDER BY nu.node_id DESC";

		$db->setQuery($query);
		$nodes = $db->loadObjectList();

		/*
		 * nid  users
		 * 3    63,64,65,66
		 */

		// Check if participants have shared a node already

		// Get comma separated pid list sorted in ascending order
		// Sort array - 63 64 65 66
		asort($participants);

		// Generate comma separated list - 63, 64, 65, 66
		$participantsCS = implode(',', $participants);

		$node_id_found = 0;
		$count         = count($nodes);

		if ($count)
		{
			for ($i = 0; $i < $count; $i++)
			{
				// If node found for 2 users, exit loop
				if ($nodes[$i]->users == $participantsCS)
				{
					$node_id_found = $nodes[$i]->nid;
					break;
				}
			}
		}

		return $node_id_found;
	}

	/**
	 * This function ->
	 * -- creates a new chat node or opens an existing node for given set of users
	 * -- if a new node is created it adds participants against this node
	 * -- it also updates session by adding new node data into session
	 * -- and finally echoes json reponse
	 *
	 * OUTPUT VARIABLE-@JSON
	 * nodeinfo - json array nodeinfo: {ctyp: "1", nid: "1", wt: "user1"}
	 *
	 * called from -
	 * - contoller.php
	 * 	-- functions => initiateNode()
	 *
	 * calls -
	 * - models/nodes.php [same file]
	 * 	-- functions => validateUser()
	 * - helpers/nodes.php
	 * 	-- functions => checkNodeExists() getNodeTitle() getNodeStatus() getNodeParticipants()
	 *
	 * @param   integer  $owner         Node owner
	 * @param   array    $participants  Participants array
	 *
	 * @return  array
	 */
	public function initiateGroupNode($owner, $participants)
	{
		// $uid = $owner;

		// Validate user
		$ini_node['validate'] = new stdclass;

		/*if(!$uid)
		{
		$response['validate']->error=1;
		$response['validate']->error_msg=JText::_('COM_JBOLO_UNAUTHORZIED_REQUEST');
		header('Content-type: application/json');
		echo json_encode($response);
		jexit();
		}
		$ini_node = $this->validateUserLogin();*/

		$db   = JFactory::getDBO();
		$user = JFactory::getUser();
		$uid  = $user->id;

		// Validate uid
		if (!$uid)
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_PARTICIPANT');

			return $ini_node;
		}

		// Get participant from post

		/*$input = JFactory::getApplication()->input;
		$post  = $input->post;
		$pid = $input->post->get('pid','','INT');//participant id e.g. 778
		*/

		// Validate participants
		if (!count($participants))
		{
			$ini_node['validate']->error     = 1;
			$ini_node['validate']->error_msg = JText::_('COM_JBOLO_INVALID_PARTICIPANT');

			return $ini_node;
		}

		// $pid = $participants[0];

		// Check if node exists

		// $node_id_found = $this->checkNodeExists($uid, $pid);
		// $node_id_found = $nodesHelper->checkGroupChatNodeExists($owner, $participants);

		// Create new node
		$myobj        = new stdclass;
		$myobj->title = null;
		$myobj->type  = 2;
		$myobj->owner = $owner;

		// //NOTE - date format date("Y-m-d H:i:s");
		// $myobj->time  = date("Y-m-d H:i:s");
		$date = JFactory::getDate();
		$myobj->time       = $date->toSql();

		$db->insertObject('#__jbolo_nodes', $myobj);

		// Get last insert id
		$new_node_id = $db->insertid();

		if ($db->insertid())
		{
			if ($owner != $uid)
			{
				$participants[] = $owner;
			}

			if (!in_array($uid, $participants))
			{
				$participants[] = $uid;
			}

			$participants = array_unique($participants);

			// Add participants against newly created node
			foreach ($participants as $participant)
			{
				$myobj          = new stdclass;
				$myobj->node_id = $new_node_id;
				$myobj->user_id = $participant;
				$myobj->status  = 1;
				$db->insertObject('#__jbolo_node_users', $myobj);
			}
		}
		// Node already exists

		/*else
		{
			$new_node_id=$node_id_found;
		}*/

		// Prepare json response
		$query = "SELECT node_id AS nid, type AS ctyp
		FROM #__jbolo_nodes
		WHERE node_id=" . $new_node_id;
		$db->setQuery($query);
		$node_d = $db->loadObject();

		$ini_node['nodeinfo'] = $node_d;

		// Get chat window title(wt)
		$ini_node['nodeinfo']->wt = $this->getNodeTitle($new_node_id, $uid, $ini_node['nodeinfo']->ctyp);

		// Get chatbox status (node status - ns)
		$ini_node['nodeinfo']->ns = $this->getNodeStatus($new_node_id, $uid, $ini_node['nodeinfo']->ctyp);

		// Use  this
		// Get participants list
		$participants = $this->getNodeParticipants($new_node_id, $uid);

		// Update  this node info in session
		$d = 0;

		// Check if 'nodes' array is set
		if (!isset($_SESSION['jbolo']['nodes']))
		{
			// If nodes array is not set, push new node at the start in nodes array
			$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];
			$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
		}
		else // If nodes array is set
		{
			$node_ids  = array();
			$nodecount = count($_SESSION['jbolo']['nodes']);

			for ($d = 0; $d < $nodecount; $d++)
			{
				if (isset($_SESSION['jbolo']['nodes'][$d]))
				{
					// Get all node ids set in session
					$node_ids[$d] = $_SESSION['jbolo']['nodes'][$d]['nodeinfo']->nid;
				}
			}
			// If the current node is not present in session,
			// We add it into session
			if (!in_array($ini_node['nodeinfo']->nid, $node_ids))
			{
				if ($nodecount) // If nodecount is >0, push new node data at the end of array
				{
					$_SESSION['jbolo']['nodes'][$nodecount]['nodeinfo']     = $ini_node['nodeinfo'];
					$_SESSION['jbolo']['nodes'][$nodecount]['participants'] = $participants['participants'];
				}
				else // If nodecount is 0, push this at start i.e. 0th position in array
				{
					$_SESSION['jbolo']['nodes'][0]['nodeinfo']     = $ini_node['nodeinfo'];
					$_SESSION['jbolo']['nodes'][0]['participants'] = $participants['participants'];
				}
			}
		}

		return $ini_node;
	}

	/**
	 * Returns node details
	 *
	 * @param   integer  $nid  Node id
	 *
	 * @return  object  $node
	 *
	 * @since   3.1.4
	 */
	public function getNodeInfo($nid)
	{
		$db = JFactory::getDbo();

		if (!$nid)
		{
			return false;
		}

		// Get query object and write query
		$query = $db->getQuery(true)
		->select('node_id, title, type, owner')
		->from($db->quoteName('#__jbolo_nodes'))
		->where("node_id=" . $nid);

		// Set query and get result
		$db->setQuery($query);
		$node = $db->loadObject();

		if ($node)
		{
			return $node;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Sorts messages in each node
	 *-- INPUT
	 * nodesArray:
	 * 	0:
	 *	 	messages: null, nodeinfo: , participants:
	 *	1:	messages:
	 * 			0: {mid:2, fid:776, msg:11, ts:2012-12-28 12:44:43}
	 *	 			1: {mid:3, fid:777, msg:22, ts:2012-12-28 12:44:48}
	 *	 			2: {mid:5, fid:779, msg:44, ts:2012-12-28 12:44:59}
	 *	 			3: {mid:4, fid:778, msg:33, ts:2012-12-28 12:44:55}
	 *	 		nodeinfo:
	 *	 		participants:
	 *
	 *	-- OUTPUT
	 *	 	nodesArray:
	 *	 		0:
	 *	 			messages: null
	 *	 			nodeinfo:
	 *	 			participants:
	 *			 1:
	 *				 messages:
	 *					 0: {mid:2, fid:776, msg:11, ts:2012-12-28 12:44:43}
	 *					 1: {mid:3, fid:777, msg:22, ts:2012-12-28 12:44:48}
	 *					 3: {mid:4, fid:778, msg:33, ts:2012-12-28 12:44:55}
	 *					 3: {mid:5, fid:779, msg:44, ts:2012-12-28 12:44:59}
	 *				 nodeinfo:
	 *			  	 participants:
	 *
	 * @param   array  $nodesArray  Array of nodes
	 *
	 * @return  array
	 *
	 * @since    3.0
	 */
	public function removeDuplicateMessages($nodesArray)
	{
		foreach ($nodesArray as &$node)
		{
			$nodeMsgs  = $node['messages'];
			$msgIds    = array();
			$msgsArray = array();

			foreach ($nodeMsgs as $key => $val)
			{
				if (in_array($val->mid, $msgIds))
				{
					unset($msg[$key]);
				}
				else
				{
					$msgIds[]    = $val->mid;
					$msgsArray[] = $val;
				}
			}

			$node['messages'] = $msgsArray;
		}

		return $nodesArray;
	}
}
