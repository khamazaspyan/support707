<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Component Helper
jimport('joomla.application.component.helper');

/**
 * Class for JBolo Chat Broadcast helper
 *
 * @package  JBolo
 * @since    3.0
 */
class ChatBroadcastHelper
{
	/**
	 * Pushes chat broadcast nessages to all/particular group chat members.
	 *
	 * @param   string  $msgType        Chat Message type
	 * @param   int     $nid            Node id
	 * @param   string  $msg            Chat Message
	 * @param   int     $particularUID  If msg is to be sent to particular user that - user id
	 * @param   int     $sendToActor    If msg is to be sent to actor himself
	 *
	 * @return  boolean
	 *
	 * @since  3.0
	 */
	public function pushChat($msgType, $nid, $msg, $particularUID = 0, $sendToActor = 0)
	{
		$actorid = JFactory::getUser()->id;
		$db      = JFactory::getDBO();

		// Process text for urls & download links
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('jbolo', 'plg_jbolo_textprocessing');

		if ($msgType == 'file')
		{
			// Process download link
			// Note - another parameter passed here - particularUID
			$processedText = $dispatcher->trigger('processDownloadLink', array($msg, $particularUID));
		}
		else
		{
			// Process urls
			$processedText = $dispatcher->trigger('processUrls', array($msg));
		}

		$msg           = $processedText[0];

		/*// Process smilies
		$processedText = $dispatcher->trigger('processSmilies', array($msg));
		$msg           = $processedText[0];*/

		// Process bad words
		$processedText = $dispatcher->trigger('processBadWords', array($msg));
		$msg           = $processedText[0];

		// Add msg to database
		$myobj = new stdclass;

		if ($msgType == 'gbc')
		{
			// Set userid to 0 for Group Chat Broadcast messages
			$myobj->from = 0;
		}
		else
		{
			// Set userid to $actorid for Group Chat Broadcast messages
			$myobj->from = $actorid;
		}

		$myobj->to_node_id = $nid;
		$myobj->msg        = $msg;
		$myobj->msg_type   = $msgType;
		$myobj->time       = date("Y-m-d H:i:s");
		$myobj->sent       = 1;
		$db->insertObject('#__jbolo_chat_msgs', $myobj);

		// Get last insert id
		$new_mid = $db->insertid();

		// Update msg xref table
		if ($new_mid)
		{
			if ($particularUID)
			{
				$myobj = new stdclass;
				$myobj->msg_id = $new_mid;
				$myobj->node_id = $nid;
				$myobj->to_user_id = $particularUID;
				$myobj->delivered = 0;
				$myobj->read = 0;
				$db->insertObject('#__jbolo_chat_msgs_xref', $myobj);
			}
			else
			{
				// Here, status indicates of user is still part of node (only active users)
				$query = "SELECT user_id
				FROM #__jbolo_node_users
				WHERE node_id = " . $nid . "
				AND status=1";

				if (!$sendToActor)
				{
					$query .= " AND user_id <> " . $actorid;
				}

				$db->setQuery($query);
				$participant = $db->loadColumn();
				$count = count($participant);

				for ($i = 0; $i < $count; $i++)
				{
					$myobj = new stdclass;
					$myobj->msg_id = $new_mid;
					$myobj->node_id = $nid;
					$myobj->to_user_id = $participant[$i];
					$myobj->delivered = 0;
					$myobj->read = 0;
					$db->insertObject('#__jbolo_chat_msgs_xref', $myobj);
				}
			}

			return 1;

			// Prepare json response //@TODO not used?

			/*
			$query="SELECT chm.to_node_id AS nid, chm.from AS uid, chm.msg_id AS mid, chm.sent, chm.msg
			FROM #__jbolo_chat_msgs AS chm
			WHERE chm.msg_id=".$new_mid;
			$db->setQuery($query);
			$node_d=$db->loadObject();
			$pushChat_response['pushChat_response']=$node_d;
			*/

			// Add this msg to session
			$query = "SELECT m.msg_id AS mid, m.from AS fid, m.msg, m.time AS ts
			FROM #__jbolo_chat_msgs AS m
			LEFT JOIN #__jbolo_chat_msgs_xref AS mx ON mx.msg_id=m.msg_id
			WHERE m.msg_id=" . $new_mid . " AND m.sent=1";
			$db->setQuery($query);
			$msg_dt = $db->loadObject();

			// Update session by adding this msg against corresponding node
			// If jbolo nodes array is set
			if (isset($_SESSION['jbolo']['nodes']))
			{
				// Count nodes in session
				$nodecount = count($_SESSION['jbolo']['nodes']);

				// Loop through all nodes
				for ($k = 0; $k < $nodecount; $k++)
				{
					// If k'th node is set
					if (isset($_SESSION['jbolo']['nodes'][$k]))
					{
						// If nodeinfo is set
						if (isset($_SESSION['jbolo']['nodes'][$k]['nodeinfo']))
						{
							// If the required node is found in session
							if ($_SESSION['jbolo']['nodes'][$k]['nodeinfo']->nid == $nid)
							{
								// Initialize mesasge count for node found to 0
								$mcnt = 0;

								// Check if the node found has messages stored in session
								if (isset($_SESSION['jbolo']['nodes'][$k]['messages']))
								{
									// If yes count msgs
									$mcnt = count($_SESSION['jbolo']['nodes'][$k]['messages']);

									// Add new mesage at the end
									$_SESSION['jbolo']['nodes'][$k]['messages'][$mcnt] = $msg_dt;
								}
								else
								{
									// Add new mesage at the start
									$_SESSION['jbolo']['nodes'][$k]['messages'][0] = $msg_dt;
								}
							}
						}
					}
					// @TODO remaining...
					else
					{
						// If node is not present in session
						// This situation is not expected ideally
					}
				}
			}
		}
	}
}
