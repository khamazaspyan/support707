<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Component Helper
jimport('joomla.application.component.helper');

/**
 * Class for JBolo users helper
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloUsersHelper
{
	/**
	 * Checks if user has active session
	 *
	 * Called from -
	 * - helpers/nodes.php
	 * 	- functions getNodeParticipants()
	 *
	 * @param   integer  $uid  777
	 *
	 * @return  integer  0 or 1
	 *
	 * @since  3.0
	 */
	public function checkOnlineStatus($uid)
	{
		$params           = JComponentHelper::getParams('com_jbolo');
		$hideChatOnMobile = $params->get('hide_chat');

		$db    = JFactory::getDbo();
		$query = "SELECT s.userid, ju.is_mobile
		 FROM #__session AS s
		 LEFT JOIN #__jbolo_users AS ju ON ju.user_id=s.userid
		 WHERE userid = " . $uid;
		$db->setQuery($query);
		$user = $db->loadObject();

		if ($user && $user->userid)
		{
			// Trick-trick ;) If chat is disabled for mobile devices, check user device and mark offline
			if (($hideChatOnMobile == 1 && $user->is_mobile == 1) || ($hideChatOnMobile == 2 && $user->is_mobile > 0))
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	/**
	 * This function returns info about loggedin ser
	 *
	 * @param   integer  $uid  user id
	 *
	 * @return  array  $u_data OR null
	 */
	public function getLoggedinUserInfo($uid)
	{
		$params    = JComponentHelper::getParams('com_jbolo');
		$chattitle = $params->get('chatusertitle');

		// Show username OR name
		if ($params->get('chatusertitle'))
		{
			$chattitle = 'username';
		}
		else
		{
			$chattitle = 'name';
		}

		$db = JFactory::getDbo();
		$query = "SELECT DISTINCT u.id AS uid, u.$chattitle AS uname, u.name, u.username,
		ju.chat_status AS sts, ju.status_msg As stsm
		FROM #__users AS u, #__session AS s
		LEFT JOIN #__jbolo_users AS ju ON ju.user_id=" . $uid . "
		WHERE u.id IN (s.userid) AND s.client_id = 0 AND u.id =" . $uid;
		$db->setQuery($query);
		$u_data = $db->loadObject();

		if (isset($u_data))
		{
			// Use integrationsHelper
			$integrationsHelper = new integrationsHelper;
			$u_data->avtr = $integrationsHelper->getUserAvatar($u_data->uid);
			$u_data->purl = $integrationsHelper->getUserProfileUrl($u_data->uid);

			return $u_data;
		}
		else
		{
			return null;
		}
	}

	/**
	 * This function returns info about all of the currently logged in users' info execpt the
	 * uid passed to this function
	 *
	 * @param   integer  $uid  User id
	 *
	 * @return  array
	 *
	 * @since   3.0
	 */
	public function getOnlineUsersInfo($uid)
	{
		$db = JFactory::getDbo();

		// Use integrationsHelper
		$integrationsHelper = new integrationsHelper;

		// Get users who blocked & blocked by logged in user
		$blockedUsers   = $this->getBlockedUser($uid);
		$blockedByUsers = $this->getBlockedByUsers($uid);

		$usersNotToInclude = array();

		foreach ($blockedUsers as $blockedUser)
		{
			$usersNotToInclude[] = $blockedUser->blocked_user_id;
		}

		foreach ($blockedByUsers as $blockedByUser)
		{
			$usersNotToInclude[] = $blockedByUser->blocked_by_user_id;
		}

		$online_users = $integrationsHelper->getOnlineUsersList($uid, $usersNotToInclude);

		$count        = count($online_users);

		for ($i = 0; $i < $count; $i++)
		{
			$online_users[$i]->avtr = $integrationsHelper->getUserAvatar($online_users[$i]->uid);
			$online_users[$i]->purl = $integrationsHelper->getUserProfileUrl($online_users[$i]->uid);
		}

		return $online_users;
	}

	/**
	 * Get a list of online users for group chat auto completion
	 *
	 * @param   integer  $uid         User id
	 * @param   string   $filterText  Search string for filtering users
	 *
	 * @return  array
	 *
	 * @since   3.0
	 */
	public function getAutoCompleteUserList($uid, $filterText)
	{
		$db = JFactory::getDbo();

		// Use integrationsHelper
		$integrationsHelper = new integrationsHelper;
		$online_users       = $integrationsHelper->getAutoCompleteUserList($uid, $filterText);

		/*
		$count=count($online_users);
		for($i=0;$i<$count;$i++)
		{
			$online_users[$i]->avtr=$integrationsHelper->getUserAvatar($online_users[$i]->uid);
			$online_users[$i]->purl=$integrationsHelper->getUserProfileUrl($online_users[$i]->uid);
		}
		*/

		return $online_users;
	}

	/**
	 * Get list of blocked user block by uid
	 *
	 * @param   integer  $uid  User id
	 *
	 * @return  object
	 *
	 * @since  3.0
	 */
	public function getBlockedUser($uid)
	{
		$db = JFactory::getDbo();

		$query = "SELECT id, blocked_user_id, blocked_in_node_id
		FROM `#__jbolo_privacy`
		WHERE blocked_by_user_id = " . $uid;

		$db->setQuery($query);

		return $result = $db->loadObjectList();
	}

	/**
	 * Get list of userids who blocked logged in user
	 *
	 * @param   integer  $uid  User id
	 *
	 * @return  object
	 *
	 * @since  3.0
	 */
	public function getBlockedByUsers($uid)
	{
		$db = JFactory::getDbo();

		$query = "SELECT id, blocked_by_user_id, blocked_in_node_id
		 FROM `#__jbolo_privacy`
		 WHERE blocked_user_id=" . $uid;

		$db->setQuery($query);

		return $result = $db->loadObjectList();
	}

	/**
	 * Check that user has permission to give support (only authorized user has permission to give support)
	 *
	 * @return  integer  0 or 1
	 *
	 * @since  3.0
	 */
	public function is_support_user()
	{
		$is_su = 0;
		$params = JComponentHelper::getParams('com_jbolo');

		$jbolo_helpdesk = $params->get('jbolo_helpdesk');

		if ($jbolo_helpdesk)
		{
			jimport('joomla.filesystem.file');

			// Check if helpdesk is installed
			if (!JFile::exists(JPATH_ROOT . '/components/com_maqmahelpdesk/maqmahelpdesk.php'))
			{
				return $is_su = 0;
			}

			$database = JFactory::getDbo();
			$user = JFactory::getUser();
			$sql = "SELECT `id` FROM `#__support_permission` WHERE `id_user`=" . $user->id;
			$database->setQuery($sql);

			if ($database->loadResult())
			{
				$is_su = 1;
			}
		}

		return $is_su;
	}

	/**
	 * Get user preferences for chat (chat settings)
	 *
	 * @return  object
	 *
	 * @since  3.0
	 */
	public function getUserChatSettings()
	{
		$user = JFactory::getUser();

		$db = JFactory::getDbo();
		$query = "SELECT state
		 FROM `#__jbolo_users`
		 WHERE user_id = " . $user->id;

		$db->setQuery($query);

		return $db->loadObject();
	}
}
