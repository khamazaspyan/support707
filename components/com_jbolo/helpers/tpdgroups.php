<?php
/**
 * @version     SVN: <svn_id>
 * @package     JBolo
 * @subpackage  com_jbolo
 * @author      Techjoomla <extensions@techjoomla.com>
 * @copyright   Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

/**
 * Helper for third party developers(tpd) groups like jomsocial or easysocial groups
 *
 * @package     JBolo
 * @subpackage  com_jbolo
 * @since       3.2.5
 */
class JBoloTpdGroupsHelper
{
	/**
	 * Retrieve a social group owner userid
	 *
	 * @param   integer  $groupid  Social group id
	 * @param   string   $client   Social group type e.g. com_tjlms.course
	 *
	 * @return  integer  $groupOwner  Group owner userid
	 *
	 * @since   3.2.5
	 */
	public static function getTpdGroupOwnerId($groupid, $client)
	{
		// Get the dbo
		$db = JFactory::getDbo();

		// Get query object
		$query = $db->getQuery(true);

		switch ($client)
		{
			// Jomsocial group
			case 'com_community.group':
				$query->select($db->quoteName('ownerid'))
				->from($db->quoteName('#__community_groups'))
				->where($db->quoteName('id') . ' = ' . (int) $groupid);
			break;

			// Easysocial group
			case 'com_easysocial.group':
				$query->select($db->quoteName('creator_uid'))
				->from($db->quoteName('#__social_clusters'))
				->where($db->quoteName('cluster_type') . ' = ' . $db->quote('group'))
				->where($db->quoteName('id') . ' = ' . (int) $groupid);
			break;

			// LMS course
			case 'com_tjlms.course':
				$query->select($db->quoteName('created_by'))
				->from($db->quoteName('#__tjlms_courses'))
				->where($db->quoteName('id') . ' = ' . (int) $groupid);
			break;
		}

		// Set query and get result
		$db->setQuery($query);
		$groupOwner = $db->loadResult();

		return $groupOwner;
	}

	/**
	 * Retrieve details for a group chat
	 *
	 * @param   integer  $groupid  Social group id
	 * @param   string   $client   Social group type e.g. com_tjlms.course
	 *
	 * @return  object  $groupChatDetails  Details for a group chat
	 *
	 * @since   3.1.4
	 */
	public static function getTpdGroupChatDetails($groupid, $client)
	{
		// Get the dbo
		$db = JFactory::getDbo();

		// Get query object and write query
		$query = $db->getQuery(true)
			->select('node_id, status, owner')
			->from($db->quoteName('#__jbolo_nodes'))
			->where('group_id = ' . (int) $groupid)
			->where('client = ' . $db->quote($client));

		// Set query and get result
		$db->setQuery($query);
		$groupChatDetails = $db->loadObject();

		return $groupChatDetails;
	}
}
