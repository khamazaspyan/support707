<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.controller');

/**
 * JBolo Component Controller
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   array    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController  This object to support chaining.
	 *
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		parent::display();
	}

	/**
	 * This initiates/updates chat data in session on every page refresh.
	 *
	 * @return  json   $startChatSession  json response aray
	 *
	 * @since  3.0
	 */
	public function startChatSession()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - startChatSession
		$startChatSession = $nodesModel->startChatSession();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($startChatSession);
		jexit();
	}

	/**
	 * This pushes chat to node.
	 *
	 * @return  json   $pushChatToNode  json response aray
	 *
	 * @since  3.0
	 */
	public function pushChatToNode()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - pushChatToNode
		$pushChatToNode = $nodesModel->pushChatToNode();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($pushChatToNode);
		jexit();
	}

	/**
	 * This pulls latest chats and userlist updates.
	 *
	 * @return  json   $polling  json response aray
	 *
	 * @since  3.0
	 */
	public function polling()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - polling
		$polling = $nodesModel->polling();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($polling);
		jexit();
	}

	/**
	 * This clears up chat from user session for a node.
	 *
	 * @return  json   $clearchat  json response aray
	 *
	 * @since  3.0
	 */
	public function clearchat()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - clearchat
		$clearchat = $nodesModel->clearchat();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($clearchat);
		jexit();
	}

	/**
	 * This changes user status and status message.
	 *
	 * @return  json   $change_status  json response aray
	 *
	 * @since  3.0
	 */
	public function change_status()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - change_status
		$change_status = $nodesModel->changeStatus();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($change_status);
		jexit();
	}

	/**
	 * This gets a list of online users for groupchat autocomplete.
	 *
	 * @return  json   $getAutoCompleteUserList  json response aray
	 *
	 * @since  3.0
	 */
	public function getAutoCompleteUserList()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - getAutoCompleteUserList
		$getAutoCompleteUserList = $nodesModel->getAutoCompleteUserList();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($getAutoCompleteUserList);
		jexit();
	}

	/**
	 * This adds a new user to current node for group chat.
	 *
	 * @return  json   $ini_node  json response aray
	 *
	 * @since  3.0
	 */
	public function addNodeUser()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - addNodeUser
		$ini_node = $nodesModel->addNodeUser();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($ini_node);
		jexit();
	}

	/**
	 * This is needed to check when user goes offline.
	 *
	 * @return  json   $userNodes  json response aray
	 *
	 * @since  3.0
	 */
	public function getUserNodes()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - getUserNodes
		$userNodes = $nodesModel->getUserNodes();

		// Output json response
		header('Content-type: application/json');

		if (isset($userNodes))
		{
			echo json_encode(array('nodes' => $userNodes));
		}
		else
		{
			echo json_encode(array('error' => JText::_('COM_JBOLO_NO_NODES_FOUND')));
		}

		jexit();
	}

	/**
	 * This is a cronjob function
	 * - this clears up chats and files as per perid set in cron job settings.
	 *
	 * @return  void
	 *
	 * @since  3.0
	 */
	public function purgeChats()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - purgeChats
		$purgeChats = $nodesModel->purgeChats();
	}

	/**
	 * This initiates a new chat node.
	 *
	 * @return  json   $ini_node  json response aray
	 *
	 * @since  3.0
	 */
	public function initiateNode()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - initiateNode
		$ini_node = $nodesModel->initiateNode();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($ini_node);
		jexit();
	}

	/**
	 * This lets user leave a group chat
	 *
	 * @return  json   $leavechat  json response aray
	 *
	 * @since  3.0
	 */
	public function leavechat()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - leavechat
		$leavechat = $nodesModel->leavechat();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($leavechat);
		jexit();
	}

	/**
	 * This blocks a selected user.
	 *
	 * @return  json   $block_user  json response aray
	 *
	 * @since  3.0
	 */
	public function block_user()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - block_user
		$block_user = $nodesModel->blockUser();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($block_user);
		jexit();
	}

	/**
	 * This creates or disables a persistent group chat for social community groups.
	 *
	 * @return  json   $setGroupChat  json response aray
	 *
	 * @since  3.1.5
	 */
	public function setGroupChat()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - setGroupChat
		$setGroupChat = $nodesModel->setGroupChat();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($setGroupChat);
		jexit();
	}

	/**
	 * This lets a social group user join / rejoin a persistent group chat for social community groups.
	 *
	 * @return  json   $joinSocialGroupChat  json response aray
	 *
	 * @since  3.1.5
	 */
	public function joinSocialGroupChat()
	{
		// Get nodes model
		$nodesModel = $this->getModel('nodes');

		// Call model function - joinSocialGroupChat
		$joinSocialGroupChat = $nodesModel->joinSocialGroupChat();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($joinSocialGroupChat);
		jexit();
	}

	/**
	 * This returns list of emojis and emoji categories
	 *
	 * @return  json   $joinSocialGroupChat  json response array
	 *
	 * @since  3.1.5
	 */
	public function getEmojis()
	{
		// Get nodes model
		$emojiModel = $this->getModel('emoji');

		// Call model function
		$emojis = $emojiModel->getEmojis();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($emojis);
		jexit();
	}
}
