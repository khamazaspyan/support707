<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

/**
 * Class for JBolo settings controller
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloControllerSettings extends JboloController
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   array    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController  This object to support chaining.
	 *
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		parent::display();
	}

	/**
	 * Saves a chat setting for a user.
	 *
	 * @return  void
	 *
	 * @since  3.0
	 */
	public function save()
	{
		$model  = $this->getModel('settings');
		$result = $model->save();

		$jboloFrontendHelper = new jboloFrontendHelper;
		$itemid   = $jboloFrontendHelper->getItemId('index.php?option=com_jbolo&view=settings&layout=default');
		$redirect = JRoute::_('index.php?option=com_jbolo&view=settings&layout=default&Itemid=' . $itemid, false);

		if ($result)
		{
			$msg = JText::_('COM_JBOLO_SETTINGS_SAVED');
		}
		else
		{
			$msg = JText::_('COM_JBOLO_ERROR_IN_SAVING_SETTINGS');
		}

		$this->setRedirect($redirect, $msg);
	}

	/**
	 * This unblock selected user.
	 *
	 * @return  json   $startChatSession  json response aray
	 *
	 * @since  3.0
	 */
	public function unblockUser()
	{
		// Get settings model.
		$settingsModel = $this->getModel('settings');

		// Call model function - unblockUser.
		$unblockUser = $settingsModel->unblockUser();

		// Output json response.
		header('Content-type: application/json');
		echo json_encode($unblockUser);
		jexit();
	}
}
