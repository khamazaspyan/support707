<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

/**
 * Class for JBolo File Handing Controller
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloControllerSendfile extends JboloController
{
	/**
	 * Uploads a file to the configured path
	 * JSON reponse is outputed by model function itself
	 *
	 * @return  void
	 *
	 * @since  3.0
	 */
	public function uploadFile()
	{
		$sendfileModel = $this->getModel('sendfile');
		$sendfileModel->uploadFile();
	}

	/**
	 * Downloads the file requested by user
	 *
	 * @return  void
	 *
	 * @since  3.0
	 */
	public function downloadFile()
	{
		$sendfileModel = $this->getModel('sendfile');
		$sendfileModel->downloadFile();
	}
}
