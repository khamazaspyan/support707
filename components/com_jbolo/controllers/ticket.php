<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

/**
 * Class for JBolo tickets controller
 *
 * @package  JBolo
 * @since    3.0
 */
class JboloControllerTicket extends JboloController
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   array    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController  This object to support chaining.
	 *
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		parent::display();
	}

	/**
	 * This adds current chat as activity into the ticket as a note.
	 *
	 * @return  void
	 *
	 * @since  3.0
	 */
	public function addActivityToTicket()
	{
		// Get the model
		$model   = $this->getModel('ticket');
		$success = $model->addActivityToTicket();

		if ($success)
		{
			$msg = JText::_('COM_JBOLO_CHATLOG_SAVE_SUCEESS_MSG');
		}
		else
		{
			$msg = JText::_('COM_JBOLO_CHATLOG_SAVE_FAIL_MSG');
		}

		$this->setRedirect("index.php?option=com_jbolo&view=ticket&success=0&tmpl=component", $msg);
	}
}
