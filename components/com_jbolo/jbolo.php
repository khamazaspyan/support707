<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

// Define directory separator
if (!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

// Define wrapper class
if (JVERSION < '3.0')
{
	define('JBOLO_WRAPPER_CLASS', "jbolo-wrapper techjoomla-bootstrap");
}
else
{
	define('JBOLO_WRAPPER_CLASS', "jbolo-wrapper");
}


// Load various helpers
$nodesHelperPath         = dirname(__FILE__) . '/helpers/nodes.php';
$jbolousersHelperPath    = dirname(__FILE__) . '/helpers/users.php';
$integrationsHelperPath  = dirname(__FILE__) . '/helpers/integrations.php';
$jboloHelperPath         = dirname(__FILE__) . '/helpers/jbolo.php';
$chatBroadcastHelperPath = dirname(__FILE__) . '/helpers/chatBroadcast.php';
$jboloFrontendHelper     = dirname(__FILE__) . '/helpers/helper.php';

if (!class_exists('nodesHelper'))
{
	JLoader::register('nodesHelper', $nodesHelperPath);
	JLoader::load('nodesHelper');
}

if (!class_exists('jbolousersHelper'))
{
	JLoader::register('jbolousersHelper', $jbolousersHelperPath);
	JLoader::load('jbolousersHelper');
}

if (!class_exists('integrationsHelper'))
{
	JLoader::register('integrationsHelper', $integrationsHelperPath);
	JLoader::load('integrationsHelper');
}

if (!class_exists('jboloHelper'))
{
	JLoader::register('jboloHelper', $jboloHelperPath);
	JLoader::load('jboloHelper');
}

if (!class_exists('chatBroadcastHelper'))
{
	JLoader::register('chatBroadcastHelper', $chatBroadcastHelperPath);
	JLoader::load('chatBroadcastHelper');
}

if (!class_exists('jboloFrontendHelper'))
{
	JLoader::register('jboloFrontendHelper', $jboloFrontendHelper);
	JLoader::load('jboloFrontendHelper');
}

// Require the base controller
require_once JPATH_COMPONENT . '/controller.php';

// Require specific controller if requested
if ($controller = JFactory::getApplication()->input->get('controller', '', 'STRING'))
{
	$path = JPATH_COMPONENT . '/controllers/' . $controller . '.php';

	if (file_exists($path))
	{
		require_once $path;
	}
	else
	{
		$controller = '';
		require_once JPATH_COMPONENT . '/controller.php';
	}
}

// Create the controller
$classname  = 'JboloController' . ucfirst($controller);
$controller = new $classname;

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('action', '', 'STRING'));

// Redirect if set by the controller
$controller->redirect();
