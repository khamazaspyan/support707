<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.view');

/**
 * Ticket View class for the JBolo.
 *
 * @package     JBolo
 * @subpackage  com_jbolo
 * @since       3.0
 */
class JboloViewTicket extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$input         = JFactory::getApplication()->input;
		$this->success = $input->get->get('success', '', 'INT');

		// Generate data from chatlog to show it for editing
		if (!$this->success)
		{
			$chatlog   = $input->get->get('chatlog', '', 'STRING');
			$this->nid = $input->get->get('nid', '', 'INT');

			if ($chatlog)
			{
				$model             = $this->getModel('ticket');
				$chatlog_ticketids = $model->getChatlog($chatlog, $this->nid);
				$this->chatlog     = $chatlog_ticketids['chatlog'];
				$this->ticketids   = $chatlog_ticketids['ticketids'];
			}
		}

		parent::display($tpl);
	}
}
