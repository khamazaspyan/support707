<?php
/**
 * @package		JBolo
 * @version		$versionID$
 * @author		TechJoomla
 * @author mail	extensions@techjoomla.com
 * @website		http://techjoomla.com
 * @copyright	Copyright © 2009-2013 TechJoomla. All rights reserved.
 * @license		GNU General Public License version 2, or later
*/
//no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$document = JFactory::getDocument();

$params         = JComponentHelper::getParams('com_jbolo');
// Load CSS & JS resources.
if (JVERSION > '3.0')
{
	// Load jQuery.
	JHtml::_('jquery.framework');

	$load_bootstrap = $params->get('force_load_bootstrap');

	if($load_bootstrap)
	{
		// Load bootstrap CSS and JS.
		JHtml::_('bootstrap.loadcss');
		JHtml::_('bootstrap.framework');
	}
}
else
{
	// Load bootstrap CSS.
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/bootstrap.min.css');
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/bootstrap-responsive.min.css');
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/strapper.css');

	// Load TJ jQuery.
	//$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/akeebajq.js');

	// Load bootstrap JS.
	//$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/bootstrap.min.js');
}

// Load TJ namespace JS.
// $document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/namespace.js');

// Load jbolo css.
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/css/jbolo.css');

$site_link=JUri::root();
?>

<script type="text/javascript">
	techjoomla.jQuery(document).ready(function(){
		OptioHideShow();
	});

	function OptioHideShow(){
		var hide_group_chat_option=parseInt(techjoomla.jQuery('input:radio[name="opt_out_of_chat"]:checked').val());
		if(hide_group_chat_option){
			techjoomla.jQuery('#jbolo_group_chat_off_option').hide();
		}else{
			techjoomla.jQuery('#jbolo_group_chat_off_option').show();
		}
	}

	/**
	Function to unblock blocked user
	*/
	function unblockUser(rowid){
		var site_url='<?php echo $site_link; ?>';

		if(confirm("<?php echo JText::_('COM_JBOLO_UNBLOCK_USER_CONFIRM'); ?>")){
			techjoomla.jQuery.ajax({
				url:site_url+'index.php?option=com_jbolo&controller=settings&action=unblockUser',
				cache:false,
				type:'POST',
				dataType:"json",
				data:{
					rowid:rowid
				},
				success: function (data) {
					var jboloError = 0;
					if (data.validate.error) {
						jboloError = data.validate.error;
					}
					if (jboloError === 1) {
						alert(data.validate.error_msg);
						return 0;
					}
					location.reload();
				}
			});
		}
	}

</script>

<div class="<?php echo JBOLO_WRAPPER_CLASS;?>" id="jbolo-settings">
	<!--row-fluid -->
	<div class="row-fluid">
		<!--Span12 -->
		<div class="span12">
			<!--Page Header -->
			<div>
				<h2 class="componentheading">
					<?php echo JText::_('COM_JGIVE_CHAT_SETTINGS');?>
				</h2>
			</div>
			<!--End Page Header -->

			<?php if (JVERSION>=3.0){  ?>
				<?php echo JHtml::_('bootstrap.startTabSet', 'ChatSettings', array('active' => 'general_settings')); ?>

			<?php } else { ?>
				<ul id="myTab" class="nav nav-tabs">
					<li class="active">
						<a href="#general_settings" data-toggle="tab"><?php echo JText::_('COM_JBOLO_GENERAL_SETTINGS');?></a>
					</li>
					<li>
						<a href="#block_users" data-toggle="tab"><?php echo JText::_('COM_JBOLO_BLOCKED_USERS');?></a>
					</li>
				</ul>
			<?php } ?>

				<!-- General settings -->
				<?php if (JVERSION>=3.0) { ?>
					<?php echo JHtml::_('bootstrap.addTab', 'ChatSettings', 'general_settings', JText::_('COM_JBOLO_GENERAL_SETTINGS', true)); ?>

				<?php } else { ?>
					<div id="myTabContent" class="tab-content">
					<!--tab1-->
						<div class="tab-pane fade in active" id="general_settings">
				<?php } ?>
							<form action="" method="post" name="jbolo_settings" id="jbolo_settings" enctype="multipart/form-data"
							class="form-horizontal form-validate" >
								<?php
									//echo $this->data->state;
									$opt_out_of_chat1=$opt_out_of_chat2='';
									$opt_out_of_group_chat1=$opt_out_of_group_chat2='';
									if(isset($this->data->state))
									{
										//echo $this->data->state;
										//Opt out of chat means opt out of group chat also
										if($this->data->state==-1)
										{
											$opt_out_of_chat1='checked';
											$opt_out_of_group_chat1='checked';
										} //opt out of group chat only
										else if($this->data->state==0)
										{
											$opt_out_of_chat2='checked';
											$opt_out_of_group_chat1='checked';
										}//nothing opt out
										else if($this->data->state==1)
										{
											$opt_out_of_chat2='checked';
											$opt_out_of_group_chat2='checked';
										}
									} //nothing opt out
									else
									{
										$opt_out_of_chat2='checked';
										$opt_out_of_group_chat2='checked';
									}
								?>

								<div class="control-group">
									<label class="control-label" title="<?php echo JText::_('COM_JBOLO_OPT_OUT_OF_CHAT');?>">
										<?php echo JText::_('COM_JBOLO_OPT_OUT_OF_CHAT');?>
									</label>
									<div class="controls">
										<label class="radio inline">
											<input type="radio" name="opt_out_of_chat" id="opt_out_of_chat1" value="1" <?php echo $opt_out_of_chat1;?> onclick="OptioHideShow()" >
												<?php echo JText::_('COM_JBOLO_YES');?>
										</label>
										<label class="radio inline">
											<input type="radio" name="opt_out_of_chat" id="opt_out_of_chat2" value="0" <?php echo $opt_out_of_chat2;?> onclick="OptioHideShow()">
												<?php echo JText::_('COM_JBOLO_NO');?>
										</label>
									</div>
								</div>

								<?php if($params->get('allow_optout_group_chat')) {  ?>
									<div class="control-group" id="jbolo_group_chat_off_option">
										<label class="control-label" title="<?php echo JText::_('COM_JBOLO_OPT_OUT_OF_GROUPCHAT');?>">
											<?php echo JText::_('COM_JBOLO_OPT_OUT_OF_GROUPCHAT');?>
										</label>
										<div class="controls">
											<label class="radio inline">
												<input type="radio" name="opt_out_of_group_chat" id="opt_out_of_group_chat1" value="1" <?php echo $opt_out_of_group_chat1;?> >
													<?php echo JText::_('COM_JBOLO_YES');?>
											</label>
											<label class="radio inline">
												<input type="radio" name="opt_out_of_group_chat" id="opt_out_of_group_chat2" value="0" <?php echo $opt_out_of_group_chat2;?>>
													<?php echo JText::_('COM_JBOLO_NO');?>
											</label>
										</div>
									</div>
								<?php } ?>
								<input type="hidden" name="option" value="com_jbolo"/>
								<input type="hidden" name="controller" value="settings"/>
								<input type="hidden" name="action" value="save"/>
								<button class="btn btn-success validate" type="submit" ><?php echo JText::_('COM_JBOLO_SUBMIT_BTN'); ?></button>

								<a class="btn" href="<?php echo JUri::root();?>">
									<?php echo JText::_('COM_JBOLO_CANCEL_BTN');?>
								</a>

							</form>

				<?php if (JVERSION>=3.0){ ?>

					<?php echo JHtml::_('bootstrap.endTab'); ?>
					<!--END General settings -->

				<?php } else { ?>

					</div>

				<?php } ?>


					<!--Block user -->
				<?php if($params->get('allow_user_blocking')) { ?>

					<?php if (JVERSION>=3.0){ ?>

						<?php echo JHtml::_('bootstrap.addTab', 'ChatSettings', 'block_users', JText::_('COM_JBOLO_BLOCKED_USERS', false)); ?>

					<?php } else { ?>

						<div class="tab-pane fade in" id="block_users">

					<?php } ?>

							<form action="" method="post" name="jbolo_blockedUser" id="jbolo_blockedUser" enctype="multipart/form-data"
							class="form-horizontal form-validate" >
								<?php
								if(count($this->blockedUsers))
								{
								?>
								<table class="adminlist table table-striped table-bordered">
									<tr>
										<th><?php echo JText::_('COM_JBOLO_NUMBER');?></th>

										<?php if($params->get('chatusertitle')==0) { ?>
											<th><?php echo JText::_('COM_JBOLO_NAME'); ?></th>
										<?php } else { ?>
											<th><?php echo JText::_('COM_JBOLO_USER_NAME'); ?></th>
										<?php } ?>

										<th><?php echo JText::_('COM_JBOLO_UNBLOCK_USER'); ?></th>
									</tr>
									<?php
									$i=1;
									foreach($this->blockedUsers as $blockedUser)
									{
									 ?>
									<tr>
										<td>
											<?php echo $i; ?>
										</td>

										<?php if($params->get('chatusertitle')==0) { ?>
										<td>
											<?php echo $blockedUser->name; ?>
										</td>
										<?php } else { ?>
										<td>
											<?php echo $blockedUser->username; ?>
										</td>
										<?php } ?>

										<td>
											<button class="btn btn-primary validate com_jgive_button" type="button"
											onclick="unblockUser(<?php echo $blockedUser->id; ?>)">
											<?php echo JText::_('COM_JBOLO_UNBLOCK_USER'); ?>
											</button>
										</td>
									</tr>
									<?php
									$i++;
									}
									?>

								</table>
								<?php
								}
								else
								{
									echo JText::_('COM_JBOLO_NO_DATA_FOUND');
								} ?>
							</form>

					<?php if (JVERSION>=3.0){ ?>

						<?php echo JHtml::_('bootstrap.endTab'); ?>

					<?php } else { ?>

						</div>

					<?php } ?>
				<?php } ?>
					<!--END Block user -->

				<?php if (JVERSION>=3.0){ ?>

				<?php echo JHtml::_('bootstrap.endTabSet'); ?>

			<?php } else { ?>
				</div>
			<?php } ?>
		</div>
		<!--End Span12 -->
	</div>
	<!--End row-fluid -->
</div>
