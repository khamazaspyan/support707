<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.view');

/**
 * Settings View class for the JBolo.
 *
 * @package     JBolo
 * @subpackage  com_jbolo
 * @since       3.0
 */
class JboloViewSettings extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		$user      = JFactory::getUser();

		if (!$user->id)
		{
			$msg = JText::_('COM_JBOLO_LOGIN_MSG');
			$uri = $_SERVER["REQUEST_URI"];
			$url = base64_encode($uri);
			$mainframe->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url, false), $msg);
		}

		$this->data         = $this->get('Settings');
		$this->blockedUsers = $this->get('BlockedUsers');

		$this->lists['order_Dir'] = 0;
		$this->lists['name']      = 0;
		$this->lists['order_Dir'] = 0;
		$this->lists['user_name'] = 0;

		parent::display($tpl);
	}
}
