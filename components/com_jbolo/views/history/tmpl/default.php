<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();

// Load jbolo css.
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/css/jbolo.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/css/history.css');

// Load CSS & JS resources.
if (JVERSION > '3.0')
{
	// Load jQuery.
	JHtml::_('jquery.framework');

	$params         = JComponentHelper::getParams('com_jbolo');
	$load_bootstrap = $params->get('force_load_bootstrap');

	if($load_bootstrap)
	{
		// Load bootstrap CSS and JS.
		JHtml::_('bootstrap.loadcss');
		JHtml::_('bootstrap.framework');
	}
}
else
{
	// Load bootstrap CSS.
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/bootstrap.min.css');
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/bootstrap-responsive.min.css');
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/strapper.css');

	// Load TJ jQuery.
	$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/akeebajq.js');

	// Load bootstrap JS.
	$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/bootstrap.min.js');
}

// Load TJ namespace JS.
$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/namespace.js');

// Load emojione js
$document->addScript(JUri::root(true) . '/media/com_jbolo/vendors/emojione/emojione.min.js');

// Load emojione css
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/vendors/emojione/assets/css/emojione.min.css');
?>

<script>
emojione.imageType = 'png';
emojione.ascii = true;
emojione.imagePathPNG = '<?php echo JUri::root(true);?>' + '/media/com_jbolo/vendors/emojione/assets/png/64/';

techjoomla.jQuery(document).ready(function() {
	processEmoji();
});

function processEmoji(){
	techjoomla.jQuery(".convert-emoji").each(function() {
		var original = techjoomla.jQuery(this).html();
		var converted = emojione.toImage(original);
		techjoomla.jQuery(this).html(converted);
	});
}
</script>

<div class="<?php echo JBOLO_WRAPPER_CLASS;?> jb-chat jbolo_word_break_wrap jbolo_pad_me " id="jbolo-history">
	<?php
		if($this->isParticipant !=1 && $this->isParticipant !=2)
		{
			echo '<div class="well">
					<div class="alert alert-error">';
						if(!$this->isParticipant)//not participant
							echo JText::_('COM_JBOLO_NON_MEMBER_MSG');
						elseif($this->isParticipant==3)//not logged in
							echo JText::_('COM_JBOLO_LOGIN_HISTORY');
						elseif($this->isParticipant==4)//not authorized
							echo JText::_('COM_JBOLO_AUTHORIZE_HISTORY');
			echo '</div>
			</div> <!--End bootsrap div-->';
		}
		else
		{
		?>
			<form action="" method="post" name="adminForm" id="adminForm">
				<input type="hidden" name="option" value="com_jbolo" />
				<input type="hidden" name="view" value="history" />
				<input type="hidden" name="nid" value="<?php echo $this->nid; ?>" />

				<div class="jb-chat-messages">
					<?php
					if(empty($this->history))
					{
						?>
						<p class="text-error" >
							<?php echo JText::_('COM_JBOLO_NO_HISTORY_MSG');?>
						</p>
						<?php
					}
					else
					{
						?>

						<div class="btn-group pull-right">
							<?php echo $this->pagination->getLimitBox(); ?>
						</div>

						<?php
						$date_temp=false;
						foreach($this->history as $h)
						{
							$date=JFactory::getDate($h->ts);
							//j shows day in number from 1 to 31
							if(!$date_temp || $date_temp!=$date->format('j'))
							{
								$date_temp=$date->format('j');
								?>
								<strong>
									<?php echo $date->format(JText::_('COM_JBOLO_HISTORY_HEADER_DATE_FORM'));?>
								</strong>
								<hr/>
								<?php
							}
							$user=JFactory::getUser();
							if($h->fid == $user->id)
							{
								?>
								<div class="jb-chat-msg right">
									<div class="jb-chat-info clearfix">
										<span class="jb-chat-name pull-right"><?php echo JText::_('COM_JBOLO_ME'); ?></span>
										<span class="jb-chat-timestamp pull-left"><?php echo JFactory::getDate($h->ts)->Format(JText::_('COM_JBOLO_HISTORY_SENT_DATE_FORM')); ?></span>
									</div>
									<img class="jb-chat-img" src="<?php echo $h->avatar; ?>" alt="<?php echo $h->uname; ?>">
									<div class="jb-chat-text convert-emoji"><?php echo $h->msg; ?></div>
									<div class="clearfix">&nbsp;</div>
								</div>
								<?php
							}
							else if($h->fid !=0)
							{
								?>
								<div class="jb-chat-msg">
									<div class="jb-chat-info clearfix">
										<span class="jb-chat-name pull-left"><?php echo $h->uname; ?></span>
										<span class="jb-chat-timestamp pull-right"><?php echo JFactory::getDate($h->ts)->Format(JText::_('COM_JBOLO_HISTORY_SENT_DATE_FORM')); ?></span>
									</div>
									<img class="jb-chat-img" src="<?php echo $h->avatar; ?>" alt="<?php echo $h->uname; ?>">
									<div class="jb-chat-text convert-emoji"><?php echo $h->msg; ?></div>
									<div class="clearfix">&nbsp;</div>
								</div>
								<?php
							}
							else
							{
								?>
								<p class="text-warning" style="text-align:center;">
									<?php echo $h->msg; ?>
								</p>
								<?php
							}
							?>
							<?php
						}
							?>
							<!-- Display Pagination -->
							<div class="pagination" style="text-align:center;">
								<p class="counter pull-right">
									<?php echo $this->pagination->getPagesCounter(); ?>
								</p>
								<?php echo $this->pagination->getPagesLinks(); ?>
							</div>
						<?php
					}//end else
						?>
				</div><!--End well div-->
			</form>
		</div> <!--End bootsrap div-->
		<?php
		}
	?>
