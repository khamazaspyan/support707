<?php
/**
 * @version    SVN: <svn_id>
 * @package    JBolo
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die();

jimport('joomla.application.component.view');

/**
 * Send file View class for the JBolo.
 *
 * @package     JBolo
 * @subpackage  com_jbolo
 * @since       3.0
 */
class JboloViewSendfile extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$user     = JFactory::getUser();
		$app      = JFactory::getApplication();
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_JBOLO_SENDFILE') . ' - ' . $app->getCfg('sitename'));

		if ($user->id)
		{
			// User authorized to send file
			if (JFactory::getUser($user->id)->authorise('core.send_file', 'com_jbolo'))
			{
				$this->params        = JComponentHelper::getParams('com_jbolo');
				$this->nodeid        = $app->input->get->get('nid', '', 'INT');
				$nodesHelper         = new nodesHelper;
				$this->isParticipant = $nodesHelper->isNodeParticipant($user->id, $this->nodeid);
			}
			// User not authorized to send file
			else
			{
				$this->isParticipant = 4;
			}
		}
		// Not logged in
		else
		{
			$this->isParticipant = 3;
		}

		parent::display($tpl);
	}
}
