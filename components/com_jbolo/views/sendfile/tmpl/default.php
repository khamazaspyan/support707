<?php
/**
 * @package		JBolo
 * @version		$versionID$
 * @author		TechJoomla
 * @author mail	extensions@techjoomla.com
 * @website		http://techjoomla.com
 * @copyright	Copyright © 2009-2013 TechJoomla. All rights reserved.
 * @license		GNU General Public License version 2, or later
*/
//no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$document=JFactory::getDocument();

// Load CSS & JS resources.
if (JVERSION > '3.0')
{
	// Load jQuery.
	JHtml::_('jquery.framework');

	$params         = JComponentHelper::getParams('com_jbolo');
	$load_bootstrap = $params->get('force_load_bootstrap');

	if($load_bootstrap)
	{
		// Load bootstrap CSS and JS.
		JHtml::_('bootstrap.loadcss');
		JHtml::_('bootstrap.framework');
	}
}
else
{
	// Load bootstrap CSS.
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/bootstrap.min.css');
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/bootstrap-responsive.min.css');
	//$document->addStylesheet(JUri::root(true) . '/media/techjoomla_strapper/css/strapper.css');

	// Load TJ jQuery.
	//$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/akeebajq.js');

	// Load bootstrap JS.
	//$document->addScript(JUri::root(true) . '/media/techjoomla_strapper/js/bootstrap.min.js');
}

// Load required CSS and JS files for file upload.
// Load jbolo css.
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/css/jbolo.css');

// Generic page styles.
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/vendors/jquery-file-upload/css/style.css');

// CSS to style the file input field as button and adjust the Bootstrap progress bars
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/vendors/jquery-file-upload/css/jquery.fileupload.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jbolo/vendors/jquery-file-upload/css/jquery.fileupload-ui.css');
?>

<div class="<?php echo JBOLO_WRAPPER_CLASS;?> jbolo_word_break_wrap jbolo_pad_me" id="jbolo-sendfile">
	<?php
		if($this->isParticipant !=1 )
		{
			echo '<div class="well">
					<div class="alert alert-error">';
						if(!$this->isParticipant)//not participant
							echo JText::_('COM_JBOLO_NON_MEMBER_MSG');
						elseif($this->isParticipant==2)//inactive participant
							echo JText::_('COM_JBOLO_INACTIVE_MEMBER_MSG');
						elseif($this->isParticipant==3)//not logged in
							echo JText::_('COM_JBOLO_LOGIN_SENDFILE');
						elseif($this->isParticipant==4)//not authorized to send file
							echo JText::_('COM_JBOLO_AUTHORIZE_SENDFILE');
			echo '</div>
			</div>
			<!--End bootsrap div-->';
		}
		else
		{
		?>
			<div class="well">
				<div class="alert alert-info">
					<?php
					echo JText::_("COM_JBOLO_FILESIZE_WARNING");
					echo "<br/>";
					echo sprintf(JText::_("COM_JBOLO_FILESIZE_LIMIT"), $this->params->get('maxSizeLimit'));
					echo "<br/>";
					echo sprintf(JText::_("COM_JBOLO_ALLOWED_EXTENSIONS"), $this->params->get('allowedFileExtensions'));
					?>
				</div>
			</div>

			<!-- The file upload form used as target for the file upload widget -->
			<form id="fileupload" method="POST" enctype="multipart/form-data">

				<input type="hidden" name="option" value="com_jbolo">
				<input type="hidden" name="controller" value="sendfile">
				<input type="hidden" name="action" value="uploadFile">

				<script>
					var fileUpload_nid=<?php echo $this->nodeid;?>;
					var fileUpload_maxFileSize=<?php echo $this->params->get('maxSizeLimit')*1024*1024;?>;
					var fileUpload_acceptFileTypes=/(\.|\/)(<?php
							$arr=explode(',', $this->params->get('allowedFileExtensions'));
							echo implode("|",$arr);
					?>)$/i;
				</script>

				<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				<div class="row-fluid fileupload-buttonbar">

					<div class="span12">
						<!-- The fileinput-button span is used to style the file input field as button -->
						<span class="btn btn-success fileinput-button">
							<i class="icon-plus icon-white"></i>
							<span><?php echo JText::_("COM_JBOLO_SENDFILE_ADD_FILES"); ?></span>
							<input type="file" name="files[]" multiple >
						</span>
						<button type="submit" class="btn btn-primary start">
							<i class="icon-upload icon-white"></i>
							<span><?php echo JText::_("COM_JBOLO_SENDFILE_START_UPLOAD"); ?></span>
						</button>
						<button type="reset" class="btn btn-warning cancel">
							<i class="icon-ban-circle icon-white"></i>
							<span><?php echo JText::_("COM_JBOLO_SENDFILE_CANCEL_UPLOAD"); ?></span>
						</button>

						<button type="reset" class="btn btn-danger" onclick="self.close()">
							<i class="icon-ban-circle icon-white"></i>
							<span><?php echo JText::_("COM_JBOLO_SENDFILE_CLOSE_WINDOW"); ?></span>
						</button>

					</div>
				</div>

				<div>&nbsp;</div>

				<div class="row-fluid fileupload-buttonbar">
					<!-- The global progress information -->
					<div class="span12 fileupload-progress fade">
						<!-- The global progress bar -->
						<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
							<div class="bar progress-bar progress-bar-success" style="width:0%;"></div>
						</div>
						<!-- The extended global progress information -->
						<div class="progress-extended">&nbsp;</div>
					</div>

				</div>
				<!-- The loading indicator is shown during file processing -->
				<div class="fileupload-loading"></div>
				<br>
				<!-- The table listing the files available for upload/download -->
				<table role="presentation" class="table table-striped">
					<tbody class="files"></tbody>
				</table>

				<div id="result" class="text-success">
					<div id="msgheader" style="display:none;"><?php echo JText::_("COM_JBOLO_SENDFILE_FILES_SENT"); ?></div>
				</div>
			</form>
		</div> <!--End bootsrap div-->

		<!-- The template to display files available for upload -->
		<script id="template-upload" type="text/x-tmpl">
		{% for (var i=0, file; file=o.files[i]; i++) { %}
			<tr class="template-upload fade">
				<td>
					<span class="preview"></span>
				</td>
				<td>
					<p class="name">{%=file.name%}</p>
					<strong class="error text-danger"></strong>
				</td>
				<td>
					<p class="size"><?php echo JText::_("COM_JBOLO_SENDFILE_PROCESSING"); ?></p>
					<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar progress-bar progress-bar-success" style="width:0%;"></div></div>
				</td>
				<td>
					{% if (!i && !o.options.autoUpload) { %}
						<button class="btn btn-primary start" disabled>
							<i class="icon-upload icon-white"></i>
							<span><?php echo JText::_("COM_JBOLO_SENDFILE_START"); ?></span>
						</button>
					{% } %}
					{% if (!i) { %}
						<button class="btn btn-warning cancel">
							<i class="icon-ban-circle icon-white"></i>
							<span><?php echo JText::_("COM_JBOLO_SENDFILE_CANCEL"); ?></span>
						</button>
					{% } %}
				</td>
			</tr>
		{% } %}
		</script>

		<!-- The template to display files available for download -->
		<script id="template-download" type="text/x-tmpl">
		{% for (var i=0, file; file=o.files[i]; i++) { %}
			<tr class="template-download fade">
				{% if (file.error) { %}
					<td></td>
					<td class="name"><span>{%=file.name%}</span></td>
					<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
					<td class="error" colspan="2"><span class="label label-important"><i class=" icon-ban-circle icon-white"></i> Error</span> {%=file.error%}</td>
				{% } else { %}
					<td>
						<span class="preview">
							{% if (file.thumbnail_url) { %}
								<img src="{%=file.thumbnail_url%}">
							{% } else { %}
								<span class="label label-warning"><i class="icon-info-sign icon-white"></i></span>
								<?php echo JText::_("COM_JBOLO_SENDFILE_NO_PREVIEW"); ?>
							{% } %}
						</span>
					</td>
					<td>
						<p class="name">
							{%=file.name%}
						</p>
						{% if (file.error) { %}
							<div><span class="label label-danger">Error</span> {%=file.error%}</div>
						{% } %}
					</td>
					<td>
						<span class="size">{%=o.formatFileSize(file.size)%}</span>
					</td>
					<td colspan="2">
						<span class="label label-success">
							<i class="icon-publish icon-white"></i>
							<?php //echo JText::_("COM_JBOLO_SENDFILE_SUCCESS"); ?>
							<?php echo JText::_("COM_JBOLO_SENDFILE_FILE_SENT"); ?>
						</span>
					</td>
				{% } %}
			</tr>
		{% } %}
		</script>

	<?php
	}
?>

<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo JUri::root(true);?>/media/com_jbolo/vendors/jquery-file-upload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo JUri::root(true);?>/media/com_jbolo/vendors/jquery-file-upload/css/jquery.fileupload-ui-noscript.css"></noscript>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--
<script src="components/com_jbolo/jbolo/model/jquery-1.8.3.min.js"></script>
-->

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.ui.widget.js"></script>

<!-- The Templates plugin is included to render the upload/download listings -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/tmpl.min.js"></script>

<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/load-image.min.js"></script>

<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/canvas-to-blob.min.js"></script>

<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.iframe-transport.js"></script>

<!-- The basic File Upload plugin -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.fileupload.js"></script>

<!-- The File Upload file processing plugin -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.fileupload-process.js"></script>

<!-- The File Upload user interface plugin -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.fileupload-ui.js"></script>

<!-- The File Upload image preview & resize plugin -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.fileupload-image.js"></script>

<!-- The File Upload audio preview plugin -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.fileupload-audio.js"></script>

<!-- The File Upload video preview plugin -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.fileupload-video.js"></script>

<!-- The File Upload validation plugin -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/jquery.fileupload-validate.js"></script>

<!-- The main application script -->
<script src="media/com_jbolo/vendors/jquery-file-upload/js/main.js"></script>

<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="<?php echo JUri::root(true);?>/media/com_jbolo/vendors/jquery-file-upload/js/jquery.xdr-transport.js"></script>
<![endif]-->

