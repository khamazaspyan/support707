<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com
 * @copyright  Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die(';)');

jimport('joomla.application.component.controller');

require_once JPATH_COMPONENT . '/controller.php';

$params = JComponentHelper::getParams('com_jgive');

// Icon constants.
define('JGIVE_ICON_CHECKMARK', " icon-checkmark");
define('JGIVE_ICON_MINUS', " icon-minus-2");
define('JGIVE_ICON_PLUS', " icon-plus-2");
define('JGIVE_ICON_EDIT', " icon-pencil-2");
define('JGIVE_ICON_REMOVE', " icon-cancel-2");
define('JGIVE_TOOLBAR_ICON_SETTINGS', "icon-cog");

// Load techjoomla bootstrapper
jimport('joomla.filesystem.file');
$tjStrapperPath = JPATH_SITE . '/media/techjoomla_strapper/tjstrapper.php';

if (JFile::exists($tjStrapperPath))
{
	require_once $tjStrapperPath;
	TjStrapper::loadTjAssets('com_jgive');
}

$document = JFactory::getDocument();

// Load CSS & JS resources.
$load_bootstrap = $params->get('load_bootstrap');

if ($load_bootstrap)
{
	$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/bs3/css/bootstrap.min.css');
	$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/bs3/css/bootstrap.css');
	$document->addStyleSheet(JUri::root(true) . '/media/techjoomla_strapper/vendors/font-awesome/css/font-awesome.min.css');
}

// Load css
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive.css');

// Responsive tables
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive-tables.css');

// Frontend css
$document->addScript(JUri::root(true) . '/media/com_jgive/javascript/jgive.js');
$document->addscript(JUri::root(true) . '/media/com_jgive/javascript/jgive_layouts.js');

$helperPath = dirname(__FILE__) . '/helper.php';

if (!class_exists('jgiveFrontendHelper'))
{
	// Require_once $path;
	JLoader::register('jgiveFrontendHelper', $helperPath);
	JLoader::load('jgiveFrontendHelper');
}

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

if (!class_exists('donationsHelper'))
{
	// Require_once $path;
	JLoader::register('donationsHelper', $helperPath);
	JLoader::load('donationsHelper');
}

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

if (!class_exists('donationsHelper'))
{
	// Require_once $path;
	JLoader::register('donationsHelper', $helperPath);
	JLoader::load('donationsHelper');
}

$JgiveIntegrationsHelperPath = dirname(__FILE__) . '/helpers/integrations.php';

// Load integrations helper file
if (!class_exists('JgiveIntegrationsHelper'))
{
	// Require_once $path;
	JLoader::register('JgiveIntegrationsHelper', $JgiveIntegrationsHelperPath);
	JLoader::load('JgiveIntegrationsHelper');
}

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/reports.php';

if (!class_exists('reportsHelper'))
{
	JLoader::register('reportsHelper', $helperPath);
	JLoader::load('reportsHelper');
}

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

if (!class_exists('campaignHelper'))
{
	JLoader::register('campaignHelper', $helperPath);
	JLoader::load('campaignHelper');
}

// Load media helper
$helperPath = JPATH_SITE . '/components/com_jgive/helpers/media.php';

if (!class_exists('jgivemediaHelper'))
{
	JLoader::register('jgivemediaHelper', $helperPath);
	JLoader::load('jgivemediaHelper');
}

// Load media helper
$helperPath = JPATH_SITE . '/components/com_jgive/helpers/video/vimeo.php';

if (!class_exists('helperVideoVimeo'))
{
	JLoader::register('helperVideoVimeo', $helperPath);
	JLoader::load('helperVideoVimeo');
}

// Load Global language constants to in .js file
jgiveFrontendHelper::getLanguageConstant();

JLoader::register('JgiveFrontendHelper', JPATH_COMPONENT . '/helpers/jgive.php');

// Execute the task.
$controller = JControllerLegacy::getInstance('Jgive');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
