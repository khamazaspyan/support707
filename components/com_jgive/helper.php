<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access

defined('_JEXEC') or die('Restricted access');

// Component Helper
jimport('joomla.database.database');
jimport('joomla.application.component.helper');
jimport('techjoomla.tjmail.mail');
/**
 * Controller
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.7
 */
class JgiveFrontendHelper
{
	/**
	 * Constructor
	 *
	 * @since   1.0
	 */
	public function __construct()
	{
		$TjGeoHelper = JPATH_ROOT . '/components/com_tjfields/helpers/geo.php';

		if (!class_exists('TjGeoHelper'))
		{
			JLoader::register('TjGeoHelper', $TjGeoHelper);
			JLoader::load('TjGeoHelper');
		}

		$this->TjGeoHelper = new TjGeoHelper;
	}

	/**
	 * Methods to get Menu item id.
	 *
	 * @param   String  $link          Menu link
	 * @param   Int     $skipIfNoMenu  Flag
	 *
	 * @return  Int  menuid
	 *
	 * @since       1.7
	 */
	public function getItemId($link, $skipIfNoMenu = 0)
	{
		$itemid    = 0;
		$mainframe = JFactory::getApplication();

		if ($mainframe->issite())
		{
			$JSite = new JSite;
			$menu  = $JSite->getMenu();
			$items = $menu->getItems('link', $link);

			if (isset($items[0]))
			{
				$itemid = $items[0]->id;
			}
		}

		if (!$itemid)
		{
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			$query = "SELECT id FROM #__menu
			WHERE link LIKE '%" . $link . "%'
			AND published =1
			LIMIT 1";

			$db->setQuery($query);
			$itemid = $db->loadResult();
		}

		if (!$itemid)
		{
			if ($skipIfNoMenu)
			{
				$itemid = 0;
			}
			else
			{
				$itemid = JRequest::getInt('Itemid', 0);
			}
		}

		return $itemid;
	}

	/**
	 * Methods to get countries
	 *
	 * @return  countries
	 *
	 * @since       1.7
	 */
	public function getCountries()
	{
		$rows = $this->TjGeoHelper->getCountryList('com_jgive');

		return $rows;
	}

	/**
	 * Loads states for given country
	 *
	 * @param   INT  $country_id  Country Id
	 *
	 * @return  countries
	 *
	 * @since       1.7
	 */
	public function getState($country_id)
	{
		if (!$country_id)
		{
			return;
		}

		$rows = $this->TjGeoHelper->getRegionList($country_id, 'com_jgive');

		return $rows;
	}

	/**
	 * Loads cities for given cities
	 *
	 * @param   INT  $country_id  Country Id
	 *
	 * @return  countries
	 *
	 * @since       1.7
	 */
	public function getCity($country_id)
	{
		if (!$country_id)
		{
			return;
		}

		$rows = $this->TjGeoHelper->getCityList($country_id, 'com_jgive');

		return $rows;
	}

	/**
	 * Loads country name from country id, used when saving campaign
	 *
	 * @param   INT  $country_id  Country Id
	 *
	 * @return  countries
	 *
	 * @since       1.7
	 */
	public function getCountryNameFromId($country_id)
	{
		if (!$country_id)
		{
			return;
		}

		$db    = JFactory::getDBO();
		$query = $db->getQuery(true)
				->select($db->qn('country'))
				->from($db->qn('#__tj_country'))
				->where($db->qn('id') . ' = ' . $db->quote($country_id) . ' AND ' . $db->qn('com_jgive') . ' = ' . '1');

		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Loads country id from country name, used when showing campaign details
	 *
	 * @param   INT  $country_name  Country name
	 *
	 * @return  countries
	 *
	 * @since       1.7
	 */
	public function getCountryIdFromName($country_name)
	{
		if (!$country_name)
		{
			return;
		}

		$db = JFactory::getDBO();
		$query = $db->getQuery(true)
			->select($db->qn('id'))
			->from($db->qn('#__tj_country'))
			->where($db->qn('country') . ' = ' . $db->quote($country_name));

		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Loads region name from region id, country id, used when saving campaign
	 *
	 * @param   INT  $region_id   Region Id
	 * @param   INT  $country_id  Country Id
	 *
	 * @return  countries
	 *
	 * @since       1.7
	 */
	public function getRegionNameFromId($region_id, $country_id)
	{
		if (!$region_id)
		{
			return;
		}

		if (!$country_id)
		{
			return;
		}

		$db    = JFactory::getDBO();
		$query = $db->getQuery(true)
			->select($db->qn('r.region'))
			->from($db->qn('#__tj_region', 'r'))
			->join('LEFT', $db->qn('#__tj_country', 'c') . ' ON (' . $db->qn('r.country_id') . ' = ' . $db->qn('c.id') . ')')
			->where($db->qn('c.id') . ' = ' . $db->quote($country_id) . ' AND ' . $db->qn('r.id') . ' = ' . $db->quote($region_id));

		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Loads city name from city id, country id, used when saving campaign
	 *
	 * @param   INT  $city_id     City Id
	 * @param   INT  $country_id  Country Id
	 *
	 * @return  countries
	 *
	 * @since       1.7
	 */
	public function getCityNameFromId($city_id, $country_id)
	{
		if (!$city_id)
		{
			return;
		}

		if (!$country_id)
		{
			return;
		}

		$db    = JFactory::getDBO();

		$query = $db->getQuery(true)
			->select($db->qn('c.city'))
			->from($db->qn('#__tj_city', 'c'))
			->join('LEFT', $db->qn('#__tj_country', 'con') . 'ON (' . $db->qn('c.country_id') . ' = ' . $db->qn('con.id') . ')')
			->where($db->qn('con.id') . ' = ' . $db->quote($country_id) . ' AND' . $db->qn('c.id') . ' = ' . $db->quote($city_id));

		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * To sort the column which are not in table
	 *
	 * @param   array   $array   Array to sort
	 * @param   string  $column  Column to sory by
	 * @param   string  $order   Asc or desc
	 *
	 * @return  array  Sorted array
	 *
	 * @since       1.7
	 */
	public function multi_d_sort($array, $column, $order)
	{
		if (isset($array) && count($array))
		{
			foreach ($array as $key => $row)
			{
				$orderby[$key] = $row->$column;
			}

			if ($order == 'asc')
			{
				array_multisort($orderby, SORT_ASC, $array);
			}
			else
			{
				array_multisort($orderby, SORT_DESC, $array);
			}
		}

		return $array;
	}

	/**
	 * To sort the column which are not in table
	 *
	 * @param   string  $ad_url             Campaign link
	 * @param   int     $id                 Campaign id
	 * @param   string  $title              Campaign title
	 * @param   int     $show_comments      Flag to show or not to show commets
	 * @param   int     $show_like_buttons  Flag to show or not to show like buttons
	 *
	 * @return  array  Sorted array
	 *
	 * @since       1.7
	 */
	public function DisplayjlikeButton($ad_url, $id, $title, $show_comments, $show_like_buttons)
	{
		$jlikeparams               = array();
		$jlikeparams['url']        = $ad_url;
		$jlikeparams['campaignid'] = $id;
		$jlikeparams['title']      = $title;
		$dispatcher                = JDispatcher::getInstance();
		JPluginHelper::importPlugin('content', 'jlike_jgive');

		$grt_response = $dispatcher->trigger('onBeforeDisplaylike', array('com_jgive.compaign', $jlikeparams, $show_comments, $show_like_buttons));

		if (!empty($grt_response['0']))
		{
			return $grt_response['0'];
		}
		else
		{
			return '';
		}
	}

	/**
	 * Push to activity stream
	 *
	 * @param   array  $contentdata  Data required for stream
	 *
	 * @return  array  Sorted array
	 *
	 * @since       1.7
	 */
	public function pushtoactivitystream($contentdata)
	{
		$a_id           = $contentdata['user_id'];
		$i_opt = $contentdata['integration_option'];
		$a_acc         = 0;
		$a_des    = $contentdata['act_description'];
		$a_type           = '';
		$act_subtype        = '';
		$act_link           = '';
		$act_title          = '';
		$act_access         = 0;

		$activityintegrationstream = new activityintegrationstream;

		$result                    = $activityintegrationstream->pushActivity($a_id, $a_type, $act_subtype, $a_des, $act_link, $act_title, $a_acc, $i_opt);

		if (!$result)
		{
			return false;
		}

		return true;
	}

	/**
	 * Push to activity stream
	 *
	 * @param   float   $price  Amount
	 * @param   string  $curr   Currency
	 *
	 * @return formatted price-currency string
	 *
	 * @since       1.7
	 */
	public function getFormattedPrice($price, $curr = null)
	{
		$price                      = @number_format($price, 2, '.', ',');

		$curr_sym                   = $this->getCurrencySymbol();
		$params                     = JComponentHelper::getParams('com_jgive');
		$currency                   = $params->get('currency');
		$currency_display_format    = $params->get('currency_display_format');
		$currency_display_formatstr = '';
		$currency_display_formatstr = str_replace('{AMOUNT}', "&nbsp;" . $price, $currency_display_format);
		$currency_display_formatstr = str_replace('{CURRENCY_SYMBOL}', "&nbsp;" . $curr_sym, $currency_display_formatstr);
		$currency_display_formatstr = str_replace('{CURRENCY}', "&nbsp;" . $currency, $currency_display_formatstr);
		$html                       = '';
		$html                       = "<span>" . $currency_display_formatstr . " </span>";

		return $html;
	}

	/**
	 * Get currency symbol
	 *
	 * @param   string  $currency  Currency
	 *
	 * @return  currency symbol
	 *
	 * @since       1.7
	 */
	public function getCurrencySymbol($currency = '')
	{
		$params   = JComponentHelper::getParams('com_jgive');
		$curr_sym = $params->get('currency_symbol');

		if (empty($curr_sym))
		{
			$curr_sym = $params->get('currency');
		}

		return $curr_sym;
	}

	/**
	 * Get jomsocial toobar html
	 *
	 * @return  Js Toolbar
	 *
	 * @since       1.7
	 */
	public function jomsocailToolbarHtml()
	{
		$params = JComponentHelper::getParams('com_jgive');
		$html   = '';

		if (($params->get('integration') == 'jomsocial') && $params->get('jomsocial_toolbar'))
		{
			// Added for JS toolbar inclusion.
			if (JFolder::exists(JPATH_SITE . '/components/com_community'))
			{
				require_once JPATH_ROOT . '/components/com_community/libraries/toolbar.php';
				$toolbar = CFactory::getToolbar();
				$tool    = CToolbarLibrary::getInstance();

				$html .= '<div id="community-wrap">';
				$html .= $tool->getHTML();
				$html .= '</div>';
			}
		}

		return $html;
	}

	/** Checks for view override
	 *
	 * @param   String  $viewname       Name of view
	 * @param   String  $layout         Layout name eg order
	 * @param   String  $searchTmpPath  It may be admin or site. it is side(admin/site) where to search override view
	 * @param   String  $useViewpath    It may be admin or site. it is side(admin/site) which VIEW shuld be use IF OVERRIDE IS NOT FOUND
	 *
	 * @return Return path
	 *
	 * @since  1.7
	 */
	public function getViewpath($viewname, $layout = "", $searchTmpPath = 'SITE', $useViewpath = 'SITE')
	{
		$searchTmpPath = ($searchTmpPath == 'SITE') ? JPATH_SITE : JPATH_ADMINISTRATOR;
		$useViewpath   = ($useViewpath == 'SITE') ? JPATH_SITE : JPATH_ADMINISTRATOR;
		$app           = JFactory::getApplication();

		if (!empty($layout))
		{
			$layoutname = $layout . '.php';
		}
		else
		{
			$layoutname = "default.php";
		}

		$override = $searchTmpPath . '/templates/' . $app->getTemplate() . '/html/com_jgive/' . $viewname . '/' . $layoutname;

		if (JFile::exists($override))
		{
			return $view = $override;
		}
		else
		{
			return $view = $useViewpath . '/components/com_jgive/views/' . $viewname . '/tmpl/' . $layoutname;
		}
	}

	/**
	 * Declare language constants to use in .js file
	 *
	 * @params  void
	 *
	 * @return  void
	 *
	 * @since   1.7
	 */
	public static function getLanguageConstant()
	{
		JText::script('COM_JGIVE_MINIMUM_DONATION_AMOUNT');
		JText::script('COM_JGIVE_ENTER_DONATION_AMOUNT');
		JText::script('COM_JGIVE_AMOUNT_SHOULD_BE');
		JText::script('COM_JGIVE_FILL_MANDATORY_FIELDS_DATA');
		JText::script('COM_JGIVE_ORDER_PLACING_ERROR');
		JText::script('COM_JGIVE_PLEASE_SELECT_DEFAULT_VIDEO');
		JText::script('COM_JGIVE_DELETE_VIDEO_CONFIRM_MSG');
		JText::script('COM_JGIVE_EMAIL_VALIDATION');
		JText::script('COM_JGIVE_VIDEO_DELETED');
		JText::script('COM_JGIVE_VIDEO_SET_DEFAULT');
		JText::script('COM_JGIVE_POST_TEXT_ACTIVITY_REMAINING_TEXT_LIMIT');

		JText::script('COM_JGIVE_FIRST_NAME_VALIDATION');
		JText::script('COM_JGIVE_LAST_NAME_VALIDATION');
		JText::script('COM_JGIVE_EMAIL_VALIDATION');
		JText::script('COM_JGIVE_ZIP_VALIDATION');
		JText::script('COM_JGIVE_PHONE_NUMBER_VALIDATION');
		JText::script('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_PROCESSING');
		JText::script('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_DONE');
		JText::script('COM_JGIVE_DASHBOARD_CREATE_ACTIVITIES_ERROR');
		JText::script('COM_JGIVE_GALLERY_IMAGE_TEXT');
		JText::script('COM_JGIVE_GALLERY_VIDEO_TEXT');
		JText::script('COM_JGIVE_CAMPAIGN_MAIN_IMAGE_TYPE_VALIDATION');
		JText::script('COM_JGIVE_CAMPAIGN_MAIN_IMAGE_DIMES_INFO');
	}

	/**
	 * Pass js file which are needed to load on selected view.
	 *
	 * @param   array  &$jsFilesArray                  Js file's array.
	 * @param   array  &$firstThingsScriptDeclaration  Javascript to be declared first.
	 *
	 * @return  filled up file array
	 */
	public function getJGiveJsFiles(&$jsFilesArray, &$firstThingsScriptDeclaration)
	{
		$input  = JFactory::getApplication()->input;
		$option = $input->get('option', '');
		$view   = $input->get('view', '');
		$layout = $input->get('layout', '');
		$params = JComponentHelper::getParams('com_jgive');

		if ($option === 'com_jgive')
		{
			// For frontend
			if (JFactory::getApplication()->isSite())
			{
				switch ($layout)
				{
					case 'all':
						$jsFilesArray[] = 'media/com_jgive/vendors/js/masonry.pkgd.min.js';
					break;

					case 'single':
						$jsFilesArray[] = 'media/com_jgive/vendors/js/jquery.magnific-popup.min.js';
					break;
				}
			}
		}

		return $jsFilesArray;
	}

	/**
	 * getLineChartFormattedData
	 *
	 * @param   ARRAY  $data  data
	 *
	 * @return  Chart array
	 */
	public function getLineChartFormattedData($data)
	{
		$app        = JFactory::getApplication();
		$backdate   = $app->getUserStateFromRequest('from', 'from', '', 'string');
		$todate     = $app->getUserStateFromRequest('to', 'to', '', 'string');
		$backdate   = !empty($backdate) ? $backdate : (date('Y-m-d', strtotime(date('Y-m-d') . ' - 30 days')));
		$todate     = !empty($todate) ? $todate : date('Y-m-d');

		$donationData = "[";
		$ordersData = "[";
		$firstdate  = $backdate;

		// Will be
		$keydate    = "";

		foreach ($data as $key => $donation)
		{
			$keydate = date('Y-m-d', strtotime($key));

			if ($firstdate < $keydate)
			{
				while ($firstdate < $keydate)
				{
					$donationData .= " { period:'" . $firstdate . "', amount:0 },
					";
					$ordersData .= " { period:'" . $firstdate . "', orders:0 },
					";
					$firstdate = $this->add_date($firstdate, 1);
				}
			}

			$donationData .= " { period:'" . $donation->cdate . "', amount:" . $donation->donation_amount . "},
		";
			$ordersData .= " { period:'" . $donation->cdate . "', orders:" . $donation->orders_count . "},
		";
			$firstdate = $keydate;
		}

		// Vm: remaing date to last date
		while ($keydate < $todate)
		{
			$keydate = $this->add_date($keydate, 1);
			$donationData .= " { period:'" . $keydate . "', amount:0 },
		";
			$ordersData .= " { period:'" . $keydate . "', orders:0 },
		";
		}

		$donationData .= '
		]';
		$ordersData .= '
		]';
		$returnArray    = array();
		$returnArray[0] = $donationData;
		$returnArray[1] = $ordersData;

		return $returnArray;
	}

	/**
	 * add_date
	 *
	 * @param   ARRAY  $givendate  givendate
	 * @param   INT    $day        day
	 * @param   INT    $mth        month
	 * @param   INT    $yr         year
	 *
	 * @return html
	 */
	public function add_date($givendate, $day = 0, $mth = 0, $yr = 0)
	{
		$cd      = strtotime($givendate);

		$newdate = date('Y-m-d H:i:s',
					mktime(
						date('H', $cd),
						date('i', $cd), date('s', $cd), date('m', $cd) + $mth, date('d', $cd) + $day, date('Y', $cd) + $yr
						)
					);

		// Convert to y-m-d format
		$newdate = date('Y-m-d H:i:s', strtotime($newdate));

		return $newdate;
	}

	/**
	 * Get sites/administrator default template
	 *
	 * @param   mixed  $client  0 for site and 1 for admin template
	 *
	 * @return  json
	 *
	 * @since   1.5
	 */
	public static function getSiteDefaultTemplate($client = 0)
	{
		try
		{
			$db    = JFactory::getDBO();

			// Get current status for Unset previous template from being default
			// For front end => client_id=0
			$query = $db->getQuery(true)
						->select('template')
						->from($db->quoteName('#__template_styles'))
						->where('client_id=' . $client)
						->where('home=1');
			$db->setQuery($query);

			return $db->loadResult();
		}
		catch (Exception $e)
		{
			return '';
		}
	}
}
