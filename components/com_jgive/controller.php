<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
/**
 * Jgive Controller
 *
 * @package     Jgive
 * @subpackage  Jgive controller
 * @since       1.0
 */
class JgiveController extends JControllerLegacy
{
	/**
	 * loads region/states according to selected country
	 * called via jquery ajax
	 *
	 * @return  void.
	 */
	public function loadState()
	{
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$country = JFactory::getApplication()->input->get('country');

		// Use helper file function
		$state = $jgiveFrontendHelper->getState($country);
		echo json_encode($state);
		jexit();
	}

	/**
	 * loads city according to selected country
	 * called via jquery ajax
	 *
	 * @return  void.
	 */
	public function loadCity()
	{
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$country = JFactory::getApplication()->input->get('country');

		// Use helper file function
		$city = $jgiveFrontendHelper->getCity($country);
		echo json_encode($city);
		jexit();
	}

	/**
	 * refreshDashboard
	 *
	 * @return  json data
	 */
	public function refreshDashboard()
	{
		$jinput = JFactory::getApplication()->input;

		$fromDate = $jinput->get('fromDate', '', 'STRING');

		if ($fromDate)
		{
			$fromDate = date('Y-m-d H:i:s', strtotime($fromDate));
		}

		$toDate = $jinput->get('toDate', '', 'STRING');

		if ($fromDate)
		{
			$toDate = date('Y-m-d H:i:s', strtotime($toDate));
		}

		$app = JFactory::getApplication();
		$app->setUserState('from', $fromDate);
		$app->setUserState('to', $toDate);

		$status = array();
		$status['status'] = 1;

		echo json_encode($status);
		jexit();
	}
}
