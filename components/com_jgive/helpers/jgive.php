<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * JgiveFrontendHelper helper class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveFrontendHelper
{
	/**
	 * Method getCategoryNameByCategoryId.
	 *
	 * @param   Integer  $category_id  Category Id
	 *
	 * @return row
	 *
	 * @since    1.8.1
	 */
	public static function getCategoryNameByCategoryId($category_id)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select('title')
			->from('#__categories')
			->where('id = ' . intval($category_id));

		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Method getModel.
	 *
	 * @param   String  $name  Name
	 *
	 * @return model
	 *
	 * @since    1.8.1
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_jgive/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_jgive/models/' . strtolower($name) . '.php';
			$model = JModelLegacy::getInstance($name, 'JgiveModel');
		}

		return $model;
	}
}
