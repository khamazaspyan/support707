<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
jimport('techjoomla.tjmedia.media');

/**
 * JgiveModelVideos for campaign
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JgiveVideos
{
	/**
	 * Save & update campaign videos database
	 *
	 * @param   array    $data  Campaign data
	 * @param   integer  $cid   Campaign ID
	 *
	 * @return  boolean  True/false
	 */
	public static function videoGallery($data, $cid)
	{
		$app               = JFactory::getApplication();
		$jgivemediaHelper  = new jgivemediaHelper;
		$params            = JComponentHelper::getParams('com_jgive');
		$db                = JFactory::getDBO();
		$is_error_flag_set = 0;

		// Check video gallery is enabled
		$video_gallery = $params->get('video_gallery', '1');

		if ($video_gallery)
		{
			$upload_options    = $data->get('upload_options', '', 'ARRAY');
			$video_urls        = $data->get('video_urls', '', 'ARRAY');
			$video_files_name  = $data->get('video_files_name', '', 'ARRAY');
			$video_exiting_ids = $data->get('video_exiting_ids', '', 'ARRAY');
			$video_ids         = $data->get('video_ids', '', 'ARRAY');
			$default_marked    = $data->get('default_marked', '', 'ARRAY');

			// Check if video upload and hence thumbnail generation option is enabled - condition Added by Nidhi
			$video_upload = $params->get('video_upload', '1');

			if ($video_upload)
			{
				if (isset($upload_options))
				{
					// For each choose option
					foreach ($upload_options as $key => $upload_option)
					{
						try
						{
							$vids             = new stdclass;
							$vids->id         = !empty($video_ids[$key]) ? $video_ids[$key] : '';
							$vids->content_id = $cid;
							$vids->url        = null;

							// If video not marked as default means this video if for gallery.
							if ($default_marked[$key] == 0)
							{
								$vids->default = 0;
							}
							elseif (empty($vids->id)) // Only if video is new becoz for existing video we have added ajax to set video as default
							{
								// Reset existing default marked video
								$query = $db->getQuery(true);

								// Fields to update.
								$fields = array($db->quoteName('default') . ' = 0');

								// Conditions for which records should be updated.
								$conditions = array($db->quoteName('content_id') . ' = ' . $cid);

								$query->update($db->quoteName('#__jg_campaigns_media'))->set($fields)->where($conditions);

								$db->setQuery($query);
								$db->execute();

								$vids->default = 1;
							}

							switch ($upload_option)
							{
								case 'url':
									if (!empty($video_urls[$key]))
									{
										$video_provider = $jgivemediaHelper->getProvider($video_urls[$key]);

										if ($video_provider != 'invalid')
										{
											$vids->type = $video_provider;
											$vids->url  = $jgivemediaHelper->geturl($video_provider, $video_urls[$key]);
										}
										else
										{
											$vids->url = $video_urls[$key];
										}
									}

									if ($vids->id)
									{
										$db->updateObject('#__jg_campaigns_media', $vids, 'id');
									}
									elseif (!empty($vids->url))
									{
										$db->insertObject('#__jg_campaigns_media', $vids, 'id');
									}

									break;

								case 'video':
									$file_name = $video_files_name[$key];

									if (!empty($file_name))
									{
										$tempsrc       = JPATH_SITE . '/tmp/' . $file_name;
										$destpath      = JPATH_SITE . '/media/com_jgive/videos';
										$videoFilePath = $destpath . '/' . $file_name;

										// If folder path not exist
										if (!JFolder::exists($destpath))
										{
											$status = JFolder::create($destpath);
										}

										// Copy uploaded file from tmp location to new location
										$iscopied = copy($tempsrc, $videoFilePath);

										// Throw error if error while coping file
										if (!$iscopied)
										{
											$is_error_flag_set = 1;
										}

										$vids->path = $file_name;

										if (file_exists($videoFilePath))
										{
											$ffmpeg_path      = $params->get('ffmpeg_path');
											$thumbnail_height = $params->get('thumbnail_height');
											$thumbnail_width  = $params->get('thumbnail_width');
											$thumbnail_path   = $params->get('video_thumbnail_path');

											$tjmedia = TJMedia::getInstance('tjmedia', $file_name, $videoFilePath, $ffmpeg_path, $thumbnail_height, $thumbnail_width, $thumbnail_path);

											if ($tjmedia)
											{
												$vids->filename       = $tjmedia['videoname'];
												$vids->path           = '/media/com_jgive/videos/' . $file_name;
												$vids->type           = 'video';
												$vids->display        = '1';
												$vids->filetype       = $tjmedia['filetype'];
												$vids->orig_filename  = null;
												$vids->thumb_filename = $tjmedia['videoname'] . '.jpg';
												$vids->thumb_path     = $params->get('video_thumbnail_path') . $tjmedia['videoname'] . '.jpg';

												if ($vids->id)
												{
													$db->updateObject('#__jg_campaigns_media', $vids, 'id');
												}
												elseif (!empty($vids->path))
												{
													$db->insertObject('#__jg_campaigns_media', $vids, 'id');
												}
											}
										}
									}
									break;
							}
						}
						catch (Exception $e)
						{
							$is_error_flag_set = 1;
							$app->enqueueMessage($e->getMessage(), 'error');
						}
					}
				}
			}
		}

		return $is_error_flag_set;
	}

	/**
	 * Upload Video
	 *
	 * @return  Array  Response
	 */
	public static function videoUpload()
	{
		$response['validate']        = new stdclass;
		$response['validate']->error = 0;
		$response['fileUpload']      = new stdclass;

		// Check if request is GET and the requested chunk exists or not. this makes testChunks work
		if ($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			$temp_dir   = JPATH_SITE . '/tmp/' . $_GET['resumableIdentifier'];
			$chunk_file = $temp_dir . '/' . $_GET['resumableFilename'] . '.part' . $_GET['resumableChunkNumber'];

			if (file_exists($chunk_file))
			{
				header("HTTP/1.0 200 Ok");
			}
			else
			{
				header("HTTP/1.0 404 Not Found");
			}
		}

		if (!empty($_FILES))
		{
			foreach ($_FILES as $file)
			{
				// Check the error status
				if ($file['error'] != 0)
				{
					$response['validate']->error = 1;
					continue;
				}

				// Init the destination file (format <filename.ext>.part<#chunk> The file is stored in a temporary directory
				$temp_dir  = JPATH_SITE . '/tmp/' . $_POST['resumableIdentifier'];
				$dest_file = $temp_dir . '/' . $_POST['resumableFilename'] . '.part' . $_POST['resumableChunkNumber'];

				// Create the temporary directory
				if (!is_dir($temp_dir))
				{
					mkdir($temp_dir, 0744, true);
				}

				// Move the temporary file
				if (!move_uploaded_file($file['tmp_name'], $dest_file))
				{
					$response['validate']->error = 1;
				}
				else
				{
					// Check if all the parts present, and create the final destination file
					$filePath = self::createFileFromChunks($temp_dir, $_POST['resumableFilename'], $_POST['resumableChunkSize'], $_POST['resumableTotalSize']);

					if ($filePath)
					{
						$response['fileUpload']->complete = 1;
						$response['fileUpload']->filePath = $filePath;
					}
					else
					{
						$response['fileUpload']->complete = 0;
					}
				}
			}
		}

		return $response;
	}

	/**
	 * Check if all the parts exist, and gather all the parts of the file together
	 *
	 * @param   String  $temp_dir   The temporary directory holding all the parts of the file
	 * @param   String  $fileName   The original file name
	 * @param   String  $chunkSize  Each chunk size (in bytes)
	 * @param   String  $totalSize  Original file size (in bytes)
	 *
	 * @return void
	 */
	public static function createFileFromChunks($temp_dir, $fileName, $chunkSize, $totalSize)
	{
		// Count all the parts of this file
		$total_files = 0;

		foreach (scandir($temp_dir) as $file)
		{
			if (stripos($file, $fileName) !== false)
			{
				$total_files++;
			}
		}

		// Check that all the parts are present
		// The size of the last part is between chunkSize and 2*$chunkSize
		if ($total_files * $chunkSize >= ($totalSize - $chunkSize + 1))
		{
			// Create the final destination file
			if (($fp = fopen(JPATH_SITE . '/tmp/' . $fileName, 'w')) !== false)
			{
				for ($i = 1; $i <= $total_files; $i++)
				{
					fwrite($fp, file_get_contents($temp_dir . '/' . $fileName . '.part' . $i));
				}

				fclose($fp);
			}
			else
			{
				return false;
			}

			// Rename the temporary directory (to avoid access from other
			// Concurrent chunks uploads) and than delete it
			if (rename($temp_dir, $temp_dir . '_UNUSED'))
			{
				self::rrmdir($temp_dir . '_UNUSED');
			}
			else
			{
				self::rrmdir($temp_dir);
			}
		}

		// Lets make a unique safe file name for each upload
		$name     = JPATH_SITE . '/tmp/' . $fileName;
		$fileInfo = pathinfo($name);

		// File extension
		$fileExt = $fileInfo['extension'];

		// Base name
		$fileBase = $fileInfo['filename'];

		// Add logggedin userid to file name
		$fileBase = JFactory::getUser()->id . '_' . $fileBase;

		/* Add timestamp to file name
		 * http://www.php.net/manual/en/function.microtime.php
		 * http://php.net/manual/en/function.uniqid.php
		 * http://php.net/manual/en/function.uniqid.php
		 * Microtime â�� Return current Unix timestamp with microseconds
		 * Uniqid â�� Generate a unique ID
		 */

		$timestamp = microtime();

		$fileBase = $fileBase . '_' . $timestamp;

		// Clean up filename to get rid of strange characters like spaces etc
		$fileBase = JFile::makeSafe($fileBase);

		// Lose any special characters in the filename
		$fileBase = preg_replace("/[^A-Za-z0-9]/i", "_", $fileBase);

		// Use lowercase
		$fileBase = strtolower($fileBase);

		$fileName = $fileBase . '.' . $fileExt;

		rename($name, JPATH_SITE . '/tmp/' . $fileName);

		return $fileName;
	}

	/**
	 * Delete a directory RECURSIVELY
	 *
	 * @param   String  $dir  directory path
	 *
	 * @return  void
	 */
	public static function rrmdir($dir)
	{
		if (is_dir($dir))
		{
			$objects = scandir($dir);

			foreach ($objects as $object)
			{
				if ($object != "." && $object != "..")
				{
					if (filetype($dir . "/" . $object) == "dir")
					{
						self::rrmdir($dir . "/" . $object);
					}
					else
					{
						unlink($dir . "/" . $object);
					}
				}
			}

			reset($objects);
			rmdir($dir);
		}
	}
}
