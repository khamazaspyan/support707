<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

// Component Helper
jimport('joomla.application.component.helper');

/**
 * JgiveControllerCampaign form controller class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class CampaignHelper
{
	/**
	 * Class constructor.
	 *
	 * @since   1.6
	 */
	public function __construct()
	{
		$this->params = JComponentHelper::getParams('com_jgive');
		$this->app    = JFactory::getApplication();
	}

	/**
	 * Get Campaign Campaign Promoter
	 *
	 * @param   INT  $orderid  Order Id
	 *
	 * @return  email
	 */
	public function getCampaignPromoterPaypalId($orderid)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('c.paypal_email');
		$query->from($db->qn('#__jg_campaigns', 'c'));
		$query->join('LEFT', $db->qn('#__jg_orders', 'o') . ' ON (' . $db->qn('o.campaign_id') . ' = ' . $db->qn('c.id') . ')');
		$query->where($db->qn('o.id') . ' = ' . $db->quote($orderid));
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get Campaign Title
	 *
	 * @param   INT  $orderid  Order id
	 *
	 * @return  STRING  Title
	 */
	public function getCampaignTitle($orderid)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('c.title');
		$query->from($db->qn('#__jg_campaigns', 'c'));
		$query->join('LEFT', $db->qn('#__jg_orders', 'o') . ' ON (' . $db->qn('o.campaign_id') . ' = ' . $db->qn('c.id') . ')');
		$query->where($db->qn('o.id') . ' = ' . $db->quote($orderid));
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get Campaign Title From Cid
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  array  title
	 */
	public function getCampaignTitleFromCid($cid)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('c.title');
		$query->from($db->qn('#__jg_campaigns', 'c'));
		$query->where($db->qn('c.id') . ' = ' . $db->quote($cid));
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get All Campaign Options [used in backend - reports view]
	 *
	 * @return  Array  Campaign id & title
	 */
	public function getAllCampaignOptions()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('c.id');
		$query->select('c.title');
		$query->from($db->qn('#__jg_campaigns', 'c'));
		$query->order($db->qn('c.title'));
		$db->setQuery($query);
		$campaigns = $db->loadObjectList();

		return $campaigns;
	}

	/**
	 * Get Campaign Amounts
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return amount
	 */
	public function getCampaignAmounts($cid)
	{
		$db = JFactory::getDBO();

		$query = "SELECT c.goal_amount
		FROM `#__jg_campaigns` AS c
		WHERE c.id=" . $cid;
		$db->setQuery($query);
		$goal_amount = $db->loadResult();

		$query = "SELECT SUM(o.amount) AS amount_received
		FROM `#__jg_orders` AS o
		WHERE o.campaign_id=" . $cid . "
		AND o.status='C'";
		$db->setQuery($query);
		$amounts                    = array();
		$amounts['amount_received'] = $db->loadResult();

		// If no donations, set receved amount as zero
		if ($amounts['amount_received'] == '')
		{
			$amounts['amount_received'] = 0;
		}

		// Calculate remaining amount
		$amounts['remaining_amount'] = ($goal_amount) - ($amounts['amount_received']);

		return $amounts;
	}

	/**
	 * Get Campaign Images
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return Images
	 */
	public function getCampaignImages($cid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT *
		FROM `#__jg_campaigns_images`
		WHERE `campaign_id`=" . $cid;
		$db->setQuery($query);
		$images = $db->loadObjectList();

		return $images;
	}

	/**
	 * Get Campaign Main Image
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  Image
	 */
	public function getCampaignMainImage($cid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT *
		FROM `#__jg_campaigns_images`
		WHERE `campaign_id`=" . $cid . " AND gallery=0";
		$db->setQuery($query);
		$images = $db->loadObject();

		return $images;
	}

	/**
	 * Get Campaign Givebacks
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  Givebacks
	 */
	public function getCampaignGivebacks($cid)
	{
		$db    = JFactory::getDBO();

		$query = $db->getQuery(true);

		$query->select('*');
		$query->from($db->qn('#__jg_campaigns_givebacks', 'cg'));
		$query->where($db->qn('cg.campaign_id') . ' = ' . $db->quote($cid));

		$db->setQuery($query);
		$givebacks = $db->loadObjectList();

		$giveback_tooltip = JText::_('COM_JGIVE_BY_GIVEBACK');

		foreach ($givebacks as $giveback)
		{
			$sold_giveback = 0;
			$query = $db->getQuery(true);

			$query->select('COUNT(d.giveback_id) as sold_giveback');
			$query->from($db->qn('#__jg_donations', 'd'));

			$query->join('LEFT', $db->qn('#__jg_campaigns', 'c') . 'ON (' . $db->qn('c.id') . ' = ' . $db->qn('d.campaign_id') . ')');
			$query->join('LEFT', $db->qn('#__jg_orders', 'o') . 'ON (' . $db->qn('o.donation_id') . ' = ' . $db->qn('d.id') . ')');
			$query->where($db->qn('d.campaign_id') . ' = ' . $db->quote($giveback->campaign_id));
			$query->where($db->qn('d.giveback_id') . ' = ' . $db->quote($giveback->id));
			$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));

			$db->setQuery($query);

			$giveback->sold_giveback = $sold_giveback = $db->loadResult();

			$give_back_flag = 0;

			// Sold out flag @TODO correct this flag instead of sold it should be sold_out
			$giveback->sold = 0;

			if ($sold_giveback == $giveback->total_quantity || $sold_giveback > $giveback->total_quantity)
			{
				// Set sold out flag
				$giveback->sold = 1;
			}
		}

		return $givebacks;
	}

	/**
	 * Get Campaign Updates
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  Updates
	 */
	public function getCampaignUpdates($cid)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select('*');
		$query->from($db->qn('#__jg_updates', 'ud'));
		$query->where($db->qn('ud.campaign_id') . ' = ' . $db->quote($cid));
		$query->order($db->qn('ud.cdate') . 'DESC');

		$db->setQuery($query);
		$updates = $db->loadObjectList();

		return $updates;
	}

	/**
	 * Get Campaign Donors
	 *
	 * @param   integer  $cid          Camp Id
	 * @param   integer  $limit_start  Limit start
	 * @param   integer  $limit        Limit
	 * @param   integer  $getCsvData   Flag to get data for csv
	 *
	 * @return  Donors
	 */
	public function getCampaignDonors($cid, $limit_start = 0, $limit = 4, $getCsvData = 0)
	{
		$db    = JFactory::getDBO();

		// Added 2 columns for groupby clause because if same user doing donation to same
		// Campaign, one as Anonymous 'yes' and another one is Anonymous 'no', there should be way to differentiate this

		// Get campaign donors
		$query = "SELECT du.id,du.user_id, du.email, du.first_name, du.last_name, du.address, du.address2, du.city as othercity, du.zip,
		ds.annonymous_donation, ds.giveback_id, sum(o.amount) as amount, o.cdate, o.fee,o.processor,
		gb.description as gb_description, gb.amount as giveback_value,
		city.city, region.region as state, country.country
		FROM `#__jg_donors` AS du
		LEFT JOIN `#__jg_donations` AS ds on ds.donor_id=du.id
		LEFT JOIN `#__jg_orders` AS o on o.donation_id=ds.id
		LEFT JOIN `#__jg_campaigns_givebacks` AS gb on gb.id=ds.giveback_id
		LEFT JOIN #__tj_city as city ON city.id = du.city
		LEFT JOIN #__tj_region as region ON region.id = du.state
		LEFT JOIN #__tj_country as country ON country.id = du.country

		WHERE ds.campaign_id=" . $cid . "
		AND o.status='C' GROUP BY du.email, ds.annonymous_donation ORDER BY o.mdate desc ";

		// In the case of csv limit should not be applied
		if ($getCsvData == 0)
		{
			$query .= " LIMIT " . $limit_start . ", " . $limit;

			$db->setQuery($query);
			$donors = $db->loadObjectList();
		}
		else
		{
			$db->setQuery($query);
			$donors = $db->loadAssocList();
		}

		foreach ($donors as $key => $donor)
		{
			$donorEmail = $donor->email;
			$query = $db->getQuery(true);

			$query->select('o.amount AS donation_amount');
			$query->from($db->qn('#__jg_orders', 'o'));
			$query->join('INNER', $db->qn('#__jg_donors', 'du') . ' ON (' . $db->qn('du.id') . ' = ' . $db->qn('o.donor_id') . ')');
			$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));
			$query->where($db->qn('du.email') . ' = ' . $db->quote($donorEmail));
			$query->group('DATE(' . $db->qn('o.cdate') . ')');
			$query->order($db->qn('o.cdate') . 'DESC');

			$db->setQuery($query);
			$results = $db->loadResult();

			$donors[$key]->recentDonation = $results;
		}

		// In the case of csv this data is not required. Added this condition to reduce overhead.
		if ($getCsvData == 0)
		{
			if (isset($donors))
			{
				foreach ($donors as $donor)
				{
					$helperPath = JPATH_SITE . '/components/com_jgive/helpers/integrations.php';

					if (!class_exists('JgiveIntegrationsHelper'))
					{
						JLoader::register('JgiveIntegrationsHelper', $helperPath);
						JLoader::load('JgiveIntegrationsHelper');
					}

					$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
					$donor->avatar      = $JgiveIntegrationsHelper->getUserAvatar($donor->user_id);
					$donor->profile_url = $JgiveIntegrationsHelper->getUserProfileUrl($donor->user_id);
				}
			}
		}

		return $donors;
	}

	/**
	 * Get Campaign Donors count
	 *
	 * @param   integer  $cid  Camp Id
	 *
	 * @return  Donors count
	 */
	public function getCampaignDonorsCount($cid)
	{
		$db = JFactory::getDBO();

		$query = "SELECT COUNT(o.amount) AS donors_count
			FROM `#__jg_orders` AS o
			WHERE o.campaign_id=" . $cid . "
			AND o.status='C'";
		$db->setQuery($query);

		$donors_count = $db->loadResult();

		if ($donors_count == '')
		{
			$donors_count = 0;
		}

		return $donors_count;
	}

	/**
	 * Get Area-Wise Campaign Donors Count
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return donors
	 */
	public function getCampaignDonorsCountAreaWise($cid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT COUNT(d.id) AS count, d.country
				FROM `#__jg_donors` AS d
				INNER JOIN `#__jg_orders` as o ON o.donor_id = d.id
				WHERE d.campaign_id=" . $cid . "
				AND o.status='C'
				GROUP BY d.campaign_id, d.country";

		$db->setQuery($query);
		$donors_count = $db->loadObjectlist();

		return $donors_count;
	}

	/**
	 * Get Campaign Orders Count
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  order record count
	 */
	public function getCampaignOrdersCount($cid)
	{
		$db = JFactory::getDBO();

		$query = "SELECT COUNT(*) as orders_count
			FROM #__jg_orders as o
			WHERE o.campaign_id =" . $cid . "
			AND status = 'C' GROUP BY  o.campaign_id ";
		$db->setQuery($query);

		return $orders_count = $db->loadResult();
	}

	/**
	 * Upload Image
	 *
	 * @param   integer  $camp_id     Campaign id
	 * @param   string   $file_field  File field name
	 * @param   integer  $img_id      Image rec id
	 * @param   integer  $index       Index
	 *
	 * @return  Added image record id
	 */
	public function uploadImage($camp_id, $file_field = 'camp_image', $img_id = 0, $index = 0)
	{
		$db = JFactory::getDBO();

		/* save uploaded image
		check the file extension is ok */

		if ($file_field == 'jgive_img_gallery')
		{
			$file_name = $_FILES[$file_field]['name'][$index];
			$file_size = $_FILES[$file_field]['size'][$index];
		}
		else
		{
			$file_name = $_FILES[$file_field]['name'];
			$file_size = $_FILES[$file_field]['size'];
		}

		// Check for max media size allowed for upload
		$JgiveMediaHelper = new JgiveMediaHelper;
		$max_size_exceed = $JgiveMediaHelper->check_max_size($file_size);

		if ($max_size_exceed)
		{
			$max_size = $this->params->get('max_size');

			if (!$max_size)
			{
				// KB
				$max_size = 1024;
			}

			$errorList[] = JText::_('FILE_BIG') . " " . $max_size . "KB<br>";
			$this->app->enqueueMessage(JText::_('COM_JGIVE_MAX_FILE_SIZE') . ' ' . $max_size . 'KB<br>', 'error');

			return false;
		}

		$media_info            = pathinfo($file_name);

		$uploadedFileName      = $media_info['filename'];
		$uploadedFileExtension = $media_info['extension'];
		$validFileExts         = explode(',', 'jpeg,jpg,png,gif');

		// Assume the extension is false until we know its ok
		$extOk = false;

		/* go through every ok extension, if the ok extension matches the file extension (case insensitive)
		then the file extension is ok */
		foreach ($validFileExts as $key => $value)
		{
			if (preg_match("/$value/i", $uploadedFileExtension))
			{
				$extOk = true;
			}
		}

		if ($extOk == false)
		{
			echo JText::_('COM_JGIVE_INVALID_IMAGE_EXTENSION');

			return;
		}

		// The name of the file in PHP's temp directory that we are going to move to our folder
		if ($file_field == 'jgive_img_gallery')
		{
			$file_temp = $_FILES[$file_field]['tmp_name'][$index];
		}
		else
		{
			$file_temp = $_FILES[$file_field]['tmp_name'];
		}

		/* for security purposes, we will also do a getimagesize on the temp file (before we have moved it
		to the folder) to check the MIME type of the file, and whether it has a width and height */
		$image_info     = getimagesize($file_temp);

		/* we are going to define what file extensions/MIMEs are ok, and only let these ones in (whitelisting), rather than try to scan for bad
		types, where we might miss one (whitelisting is always better than blacklisting) */
		$okMIMETypes    = 'image/jpeg,image/pjpeg,image/png,image/x-png,image/gif';
		$validFileTypes = explode(",", $okMIMETypes);

		// If the temp file does not have a width or a height, or it has a non ok MIME, return
		if (!is_int($image_info[0]) || !is_int($image_info[1]) || !in_array($image_info['mime'], $validFileTypes))
		{
			echo JText::_('COM_JGIVE_INVALID_IMAGE_EXTENSION');

			return;
		}

		// Clean up filename to get rid of strange characters like spaces etc
		$file_name = JFile::makeSafe($uploadedFileName);

		// Lose any special characters in the filename
		$file_name = preg_replace("/[^A-Za-z0-9]/i", "-", $file_name);

		// Use lowercase
		$file_name = strtolower($file_name);

		// Add timestamp to file name
		$timestamp = time();
		$file_name = $file_name . '_' . $timestamp . '.' . $uploadedFileExtension;

		// Always use constants when making file paths, to avoid the possibilty of remote file inclusion
		$upload_path_folder       = JPATH_SITE . '/images/jGive';
		$image_upload_path_for_db = 'images/jGive';

		// If folder is not present create it
		if (!file_exists($upload_path_folder))
		{
			@mkdir($upload_path_folder);
		}

		$upload_path = $upload_path_folder . DS . $file_name;
		$image_upload_path_for_db .= '/' . $file_name;

		if (!JFile::upload($file_temp, $upload_path))
		{
			echo JText::_('COM_JGIVE_ERROR_MOVING_FILE');

			return false;
		}
		else
		{
			$obj = new stdClass;

			if ($img_id)
			{
				$obj->id = $img_id;
			}
			else
			{
				$obj->id = '';
			}

			$obj->campaign_id = $camp_id;
			$obj->path        = $image_upload_path_for_db;

			if ($file_field == 'camp_image')
			{
				$obj->gallery = 0;
			}
			else
			{
				$obj->gallery = 1;
			}

			$obj->order = '';

			if ($obj->id)
			{
				if (!$db->updateObject('#__jg_campaigns_images', $obj, 'id'))
				{
					echo $db->stderr();

					return false;
				}

				return $obj->id;
			}
			elseif (!$db->insertObject('#__jg_campaigns_images', $obj, 'id'))
			{
				echo $db->stderr();

				return false;
			}

			return $db->insertid();
		}
	}

	/**
	 * Upload Image For Giveback
	 *
	 * @param   string  $coupon_id  Field name
	 * @param   INT     $id         Primary key of giveback table
	 * @param   INT     $cid        Camp id
	 *
	 * @return boolean
	 */
	public function uploadImageForGiveback($coupon_id, $id, $cid)
	{
		$db         = JFactory::getDBO();

		/* Save uploaded image
		check the file extension is ok */
		$file_field = 'coupon_image';
		$file_name  = $_FILES[$file_field]['name'][$coupon_id];
		$media_info = pathinfo($file_name);

		$uploadedFileName      = $media_info['filename'];
		$uploadedFileExtension = $media_info['extension'];
		$validFileExts         = explode(',', 'jpeg,jpg,png,gif');

		// Assume the extension is false until we know its ok
		$extOk                 = false;

		/* Go through every ok extension, if the ok extension matches the file extension (case insensitive)
		then the file extension is ok */
		foreach ($validFileExts as $key => $value)
		{
			if (preg_match("/$value/i", $uploadedFileExtension))
			{
				$extOk = true;
			}
		}

		if ($extOk == false)
		{
			echo JText::_('COM_JGIVE_INVALID_IMAGE_EXTENSION');

			return;
		}

		// The name of the file in PHP's temp directory that we are going to move to our folder
		$file_temp      = $_FILES[$file_field]['tmp_name'][$coupon_id];

		/* For security purposes, we will also do a getimagesize on the temp file (before we have moved it
		to the folder) to check the MIME type of the file, and whether it has a width and height */

		$image_info     = getimagesize($file_temp);

		/* We are going to define what file extensions/MIMEs are ok, and only let these ones in (whitelisting), rather than try to scan for bad
		 types, where we might miss one (whitelisting is always better than blacklisting)
		*/
		$okMIMETypes    = 'image/jpeg,image/pjpeg,image/png,image/x-png,image/gif';
		$validFileTypes = explode(",", $okMIMETypes);

		// If the temp file does not have a width or a height, or it has a non ok MIME, return
		if (!is_int($image_info[0]) || !is_int($image_info[1]) || !in_array($image_info['mime'], $validFileTypes))
		{
			echo JText::_('COM_JGIVE_INVALID_IMAGE_EXTENSION');

			return;
		}

		// Clean up filename to get rid of strange characters like spaces etc
		$file_name = JFile::makeSafe($uploadedFileName);

		// Lose any special characters in the filename
		$file_name = preg_replace("/[^A-Za-z0-9]/i", "-", $file_name);

		// Use lowercase
		$file_name = strtolower($file_name);

		// Add timestamp to file name
		$timestamp = time();
		$file_name = $file_name . '_' . $timestamp . '.' . $uploadedFileExtension;

		// Always use constants when making file paths, to avoid the possibilty of remote file inclusion
		$upload_path_folder       = JPATH_SITE . '/images/jGive';
		$image_upload_path_for_db = 'images/jGive';

		// If folder is not present create it
		if (!file_exists($upload_path_folder))
		{
			@mkdir($upload_path_folder);
		}

		$upload_path = $upload_path_folder . DS . $file_name;
		$image_upload_path_for_db .= '/' . $file_name;

		if (!JFile::upload($file_temp, $upload_path))
		{
			echo JText::_('COM_JGIVE_ERROR_MOVING_FILE');

			return false;
		}
		else
		{
			$obj = new stdClass;
		}

		$obj->id          = $id;
		$obj->campaign_id = $cid;
		$obj->image_path  = $image_upload_path_for_db;

		if (!$db->updateObject('#__jg_campaigns_givebacks', $obj, 'id'))
		{
			echo $db->stderr();

			return false;
		}

		return true;
	}

	/**
	 * Get All Category Options
	 *
	 * @return  Cat Option
	 */
	public function getAllCategoryOptions()
	{
		$db    = JFactory::getDBO();
		$query = "SELECT c.category_id,cat.title
		FROM `#__jg_campaigns` AS c
		INNER JOIN #__categories as cat ON cat.id=c.category_id
		ORDER BY cat.title";

		$db->setQuery($query);
		$campaigns = $db->loadObjectList();

		return $campaigns;
	}

	/**
	 * Get Campaigns Cats
	 *
	 * @return  Categories
	 */
	public function getCampaignsCats()
	{
		$mainframe = JFactory::getApplication();
		$db        = JFactory::getDBO();
		$query     = "SELECT id,title FROM #__categories WHERE extension='com_jgive' && parent_id=1";
		$db->setQuery($query);
		$categories = $db->loadObjectList();
		$default    = '';
		$options[]  = JHtml::_('select.option', '0', '-Select Category-');

		foreach ($categories as $cat_obj)
		{
			$options[] = JHtml::_('select.option', $cat_obj->id, $cat_obj->title);
		}

		if (JFactory::getApplication()->input->get('cid'))
		{
			$details  = $this->getCampaignDetails(JFactory::getApplication()->input->get('cid'));
			$camp_cat = $details->category_id;

			$cid   = JFactory::getApplication()->input->get('cid');
			$db    = JFactory::getDBO();
			$query = "SELECT * FROM #__jg_campaigns WHERE id='" . $cid . "'";
			$db->setQuery($query);

			$options  = array_merge($options, $db->loadObjectlist());
			$dropdown = JHtml::_('list.category', 'campaign_category', "com_jgive", intval($camp_cat));
		}
		else
		{
			$dropdown = JHtml::_('list.category', 'campaign_category', "com_jgive", intval(1));
		}

		return $dropdown;
	}

	/**
	 * Get Campaign Details
	 *
	 * @param   INT  $c_id  Camp ID
	 *
	 * @return  Campaign data
	 */
	public function getCampaignDetails($c_id)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT * FROM #__jg_campaigns WHERE id='" . $c_id . "'";
		$db->setQuery($query);

		return ($db->loadObject());
	}

	/**
	 * Get campaign category filter
	 *
	 * @param   string  $firstOption  Filter options
	 *
	 * @return  Options
	 */
	public function getCampaignsCategories($firstOption = '')
	{
		$app  = JFactory::getApplication();
		$lang = JFactory::getLanguage();
		$tag  = $lang->gettag();

		$categories  = JHtml::_('category.options', 'com_jgive', $config = array('filter.published' => array(1), 'filter.language' => array('*', $tag)));

		$cat_options = array();

		/*if ($app->isSite() OR JVERSION < 3.0)
		{
			$cat_options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_ALL'));
		}*/

		if (!empty($categories))
		{
			foreach ($categories as $category)
			{
				if (!empty($category))
				{
					$cat_options[] = JHtml::_('select.option', $category->value, $category->text);
				}
			}
		}

		return $cat_options;
	}

	/**
	 * [getCampaignTypeFilterOptions description]
	 *
	 * @return [type] [description]
	 */
	public function getCampaignTypeFilterOptions()
	{
		$mainframe            = JFactory::getApplication();
		$filter_campaign_type = $mainframe->getUserStateFromRequest('com_jgive.filter_campaign_type', 'filter_campaign_type');

		$options = array();
		$options[] = JHtml::_('select.option', 'donation', JText::_('COM_JGIVE_CAMPAIGN_TYPE_DONATION'));
		$options[] = JHtml::_('select.option', 'investment', JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT'));

		return $options;
	}

	/**
	 * Check the campaign marks as featured
	 *
	 * @param   INT  $contentId  ContentId
	 *
	 * @return boolean
	 */
	public function isFeatured($contentId)
	{
		$db    = JFactory::getDBO();
		$query = 'SELECT featured FROM `#__jg_campaigns` WHERE `id` = ' . $contentId;
		$db->setQuery($query);
		$result = $db->loadResult();

		return $result = (empty($result)) ? 0 : $result;
	}

	/**
	 * Get Campaign type Donate/Invest
	 *
	 * @param   INT  $campid  Camp Id
	 *
	 * @return  void
	 */
	public function getCampaignType($campid)
	{
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select($db->qn(array('id', 'creator_id', 'type')))
			->from($db->qn('#__jg_campaigns'))
			->where($db->qn('#__jg_campaigns.id') . ' = ' . $db->quote($campid));

		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}

	/**
	 * Send email to site admin when campaigns is created
	 *
	 * @param   Object  $camp_details  Camp details
	 * @param   INT     $camp_id       Camp Id
	 *
	 * @return void
	 */
	public function sendCmap_create_mail($camp_details, $camp_id)
	{
		$params    = JComponentHelper::getParams('com_jgive');
		$billemail = $params->get('email');

		if (!empty($billemail))
		{
			$userid          = JFactory::getUser()->id;
			$body            = JText::_('COM_JGIVE_CAMP_AAPROVAL_BODY');
			$body            = str_replace('{title}', $camp_details->get('title', '', 'STRING'), $body);
			$body            = str_replace('{campid}', ':' . $camp_id, $body);
			$body            = str_replace('{username}', $camp_details->get('first_name', '', 'STRING'), $body);
			$body            = str_replace('{userid}', $userid, $body);
			$body            = str_replace('{link}', JUri::base() . 'administrator/index.php?option=com_jgive&view=campaigns&layout=default&approve=1', $body);
			$subject         = JText::sprintf('COM_JGIVE_CAMP_CREATED_EMAIL_SUBJECT', $camp_details->get('title', '', 'STRING'));
			$donationsHelper = new donationsHelper;
			$donationsHelper->sendmail($billemail, $subject, $body, $params->get('email'), 'campaign.create');
		}
	}

	/**
	 * Send email to promoter when campaigns is created
	 *
	 * @param   Object  $camp_details  Camp Details
	 * @param   INT     $camp_id       Camp Id
	 *
	 * @return void
	 */
	public function sendCmapCreateMailToPromoter($camp_details, $camp_id)
	{
		$user                = JFactory::getUser();
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$donationsHelper     = new donationsHelper;
		$body                = JText::_('COM_JGIVE_CAMP_AAPROVAL_BODY_PROMOTER');
		$body                = str_replace('{title}', $camp_details->get('title', '', 'STRING'), $body);

		// Embed campid
		$body                = str_replace('{campid}', ':' . $camp_id, $body);

		// Get item id
		$itemid              = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');
		$body                = str_replace('{link}', JUri::base() . 'index.php?option=com_jgive&view=campaigns&layout=my&itemid=' . $itemid, $body);

		// To send email
		$billemail           = $user->email;
		$subject             = JText::sprintf('COM_JGIVE_CAMP_CREATED_EMAIL_SUBJECT_PROMOTER', $camp_details->get('title', '', 'STRING'));
		$donationsHelper->sendmail($billemail, $subject, $body, $user->email, 'campaign.create');
	}

	/**
	 * Send email to promoter after approved campaign
	 *
	 * @param   Object  $camp_info  Camp Info
	 * @param   INT     $state      Camp State
	 *
	 * @return  void
	 */
	public function sendemailCampaignApprovedReject($camp_info, $state)
	{
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$donationsHelper     = new donationsHelper;
		$objectcount         = count($camp_info);

		foreach ($camp_info as $each_creator)
		{
			$html = JText::_('COM_JGIVE_CAMPAIGN_DETAILS_MAIL');
			$html .= '<table cellspacing="15">
					<th>' . JText::sprintf('COM_JGIVE_CAMPAIGN_ID') . '</th>
					<th>' . JText::sprintf('COM_JGIVE_CAMPAIGN_NAME') . '</th> ';

			foreach ($each_creator as $row)
			{
				$html .= '<tr>
							<td>' . $row->id . '</td>
							<td>' . $row->title . '</td>
						</tr>';
				$billemail = $row->email;
			}

			$html .= '</table>';

			$subject = JText::_('COM_JGIVE_CAMP_CREATED_EMAIL_SUBJECT_PROMOTER_APPROVED');

			// According to state get approved / rejected /delete language constant
			if ($state == 0)
			{
				$campaign_status = JText::sprintf('COM_JGIVE_CAMAPIGN_REJECTED');
			}
			elseif ($state == 1)
			{
				$campaign_status = JText::sprintf('COM_JGIVE_CAMAPIGN_APPROVED');
			}
			else
			{
				$campaign_status = JText::sprintf('COM_JGIVE_CAMAPIGN_DELETED');
			}

			$subject = str_replace('{status}', $campaign_status, $subject);

			// My campaign link for user only when campaign is approved
			if ($state == 1 or $state == 0)
			{
				$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');
				$html .= JText::_('COM_JGIVE_CLICK_HERE');
				$html .= JUri::root() . 'index.php?option=com_jgive&view=campaigns&layout=my&itemid=' . $itemid;
			}

			$donationsHelper->sendmail($billemail, $subject, $html, $billemail, 'campaign.statuschange');
		}
	}

	/**
	 * Get cat name
	 *
	 * @param   INT  $catid  Cat id
	 *
	 * @return  category name
	 */
	public function getCatname($catid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT title FROM #__categories as cat
				WHERE cat.id=" . $catid . " AND `extension`='com_jgive'";
		$db->setquery($query);

		return $result = $db->loadResult();
	}

	/**
	 * Function to add the organization/individual type field Individuals
	 *
	 * @return Options
	 */
	public function organization_individual_type()
	{
		$options   = array();

		$options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_ALL'));
		$options[] = JHtml::_('select.option', 'non_profit', JText::_('COM_JGIVE_ORG_NON_PROFIT'));
		$options[] = JHtml::_('select.option', 'self_help', JText::_('COM_JGIVE_SELF_HELP'));
		$options[] = JHtml::_('select.option', 'individuals', JText::_('COM_JGIVE_SELF_INDIVIDUALS'));

		return $options;
	}

	/**
	 * Campaigns To ShowOptions
	 *
	 * @return  Options
	 */
	public function campaignsToShowOptions()
	{
		$options = array();
		$app     = JFactory::getApplication();

		if ($app->isSite() OR JVERSION < 3.0)
		{
			$options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_CAM_TO_SHOW'));
		}

		$options[] = JHtml::_('select.option', 'featured', JText::_('COM_JGIVE_FEATURED_CAMP'));
		$options[] = JHtml::_('select.option', '1', JText::_('COM_JGIVE_SUCCESSFUL_CAMP'));
		$options[] = JHtml::_('select.option', '0', JText::_('COM_JGIVE_FILTER_ONGOING'));
		/*$options[] = JHtml::_('select.option', '-1', JText::_('COM_JGIVE_FILTER_FAILD'));*/

		return $options;
	}

	/**
	 * Get Campaign Status
	 *
	 * @param   Object  $data  Camp data
	 *
	 * @return  Campaign Status
	 */
	public function getCampaignStatus($data)
	{
		/* Check if exeeding goal amount is allowed
		If not check for received amount to decide about hiding donate button */
		foreach ($data as $cdata)
		{
			$flag        = 0;
			$date_expire = 0;

			if ($cdata['campaign']->allow_exceed == 0)
			{
				if ($cdata['campaign']->amount_received >= $cdata['campaign']->goal_amount)
				{
					$flag = 1;
				}
			}

			if ($cdata['campaign']->max_donors > 0)
			{
				if ($cdata['campaign']->donor_count >= $cdata['campaign']->max_donors)
				{
					$flag = 1;
				}
			}

			// If both start date, and end date are present
			$curr_date = '';

			// (int) typecasting is important
			if ((int) $cdata['campaign']->start_date && (int) $cdata['campaign']->end_date)
			{
				$start_date = JFactory::getDate($cdata['campaign']->start_date)->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
				$end_date   = JFactory::getDate($cdata['campaign']->end_date)->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
				$curr_date  = JFactory::getDate()->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));

				// If current date is less than start date, don't show donate button
				if ($curr_date < $start_date)
				{
					$flag = 1;
				}

				// If current date is more than end date, don't show donate button
				if ($curr_date > $end_date)
				{
					$flag = 1;
				}
			}

			// Campaign is close
			if ($flag == 1)
			{
				$cdata['campaign']->status = 'closed';
			}
			else
			{
				if ($cdata['campaign']->amount_received >= $cdata['campaign']->goal_amount)
				{
					$cdata['campaign']->status = 'successful';
				}
				else
				{
					$cdata['campaign']->status = 'active';
				}
			}
		}

		return $data;
	}

	/**
	 * Function to delete gallery files
	 *
	 * @param   integer  $cid                Campaign id
	 * @param   Array    $existing_imgs_ids  Do not delete record primary key
	 * @param   integer  $isvideo            Delete video file related record or image file related record
	 * @param   Array    $gallery            Flag is gallery record
	 *
	 * @return  void
	 */
	public function deleteGalleryRecords($cid, $existing_imgs_ids, $isvideo = 0, $gallery = array(1))
	{
		$gallery = implode(',', $gallery);

		if ($existing_imgs_ids)
		{
			$existing_imgs_ids = implode(',', $existing_imgs_ids);

			$db = JFactory::getDBO();

			$query = "SELECT path FROM #__jg_campaigns_images WHERE id not in (" . $existing_imgs_ids . ")
			AND campaign_id=" . $cid . " AND isvideo = " . $isvideo . " AND gallery IN (" . $gallery . ")";
			$db->setQuery($query);
			$galleryImgToDelPath = $db->loadColumn();

			// Delete image from db
			$query = "DELETE FROM #__jg_campaigns_images WHERE id not in (" . $existing_imgs_ids . ")
			AND campaign_id=" . $cid . " AND isvideo = " . $isvideo . " AND gallery IN (" . $gallery . ")";
			$db->setQuery($query);

			if (!$db->execute())
			{
				echo $db->stderr();

				return false;
			}

			// Delete image physically
			$this->deletFile($galleryImgToDelPath, $isvideo);
		}
		else
		{
			$db    = JFactory::getDBO();
			$query = "SELECT path FROM #__jg_campaigns_images WHERE campaign_id=" . $cid . " AND isvideo = " . $isvideo . " AND gallery IN (" . $gallery . ")";
			$db->setQuery($query);
			$galleryImgToDelPath = $db->loadColumn();

			// Delete image from db
			$query = "DELETE FROM #__jg_campaigns_images WHERE campaign_id=" . $cid . " AND isvideo = " . $isvideo . " AND gallery IN (" . $gallery . ")";
			$db->setQuery($query);

			if (!$db->execute())
			{
				echo $db->stderr();

				return false;
			}

			// Delete image physically
			$this->deletFile($galleryImgToDelPath, $isvideo);
		}
	}

	/**
	 * Delet File
	 *
	 * @param   Array  $imgarray  Image file array
	 * @param   INT    $isvideo   Flag
	 *
	 * @return  void
	 */
	public function deletFile($imgarray, $isvideo)
	{
		if (!empty($imgarray))
		{
			foreach ($imgarray as $img)
			{
				$this->deleteFile($img, $isvideo);
			}
		}
	}

	/**
	 * Send email to site admin after editing campaigns
	 *
	 * @param   Object  $camp_details  Camp details
	 * @param   int     $camp_id       Camp Id
	 *
	 * @return  void
	 */
	public function send_mail_on_edit($camp_details, $camp_id)
	{
		$params    = JComponentHelper::getParams('com_jgive');
		$billemail = $params->get('email');

		if (!empty($billemail))
		{
			$userid   = JFactory::getUser()->id;
			$username = JFactory::getUser()->name;
			$body     = JText::_('COM_JGIVE_CAMP_EDIT_BODY');
			$body     = str_replace('{editor}', $username, $body);
			$body     = str_replace('{img}', JUri::ROOT() . $camp_details->get('img_path', '', 'STRING'), $body);
			$body     = str_replace('{title}', $camp_details->get('title', '', 'STRING'), $body);
			$body     = str_replace('{cat}', $camp_details->get('campaign_category', '', 'STRING'), $body);
			$body     = str_replace('{goal}', $camp_details->get('goal_amount', '', 'STRING'), $body);
			$body     = str_replace('{username}', $camp_details->get('first_name', '', 'STRING'), $body);
			$body     = str_replace('{userid}', $userid, $body);

			$subject         = JText::sprintf('COM_JGIVE_CAMP_EDIT_SUBJECT', $camp_details->get('title', '', 'STRING'));
			$donationsHelper = new donationsHelper;
			$donationsHelper->sendmail($billemail, $subject, $body, $params->get('email'), 'campaign.edit');
		}
	}

	/**
	 * Delete Campaign Main Image
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return void
	 */
	public function deleteCampaignMainImage($cid)
	{
		$result = $this->getCampaignMainImage($cid);

		if ($result->path)
		{
			$path = 'images/jGive/';

			// Get original image name to find it resize images (S,M,L)
			$org_file_after_removing_path = trim(str_replace($path, '', $result->path));

			// Delete original file
			$this->deleteFile($result->path);

			// Delete large image
			$this->deleteFile($path . 'L_' . $org_file_after_removing_path);

			// Delete Medium image
			$this->deleteFile($path . 'M_' . $org_file_after_removing_path);

			// Delete Small image
			$this->deleteFile($path . 'S_' . $org_file_after_removing_path);
		}
	}

	/**
	 * Delete File
	 *
	 * @param   string  $dfinepath  File path to delete
	 * @param   INT     $isvideo    For video flag
	 *
	 * @return void
	 */
	public function deleteFile($dfinepath, $isvideo = 0)
	{
		/* If to delete video file then we will need to specify video file location
		but in-case of image full location we are getting from db itself*/
		if ($isvideo == 1)
		{
			$dfinepath = 'media/com_jgive/videos/' . $dfinepath;
		}

		if (JFile::exists($dfinepath))
		{
			JFile::delete($dfinepath);
		}
	}

	/**
	 * Function to idetify passed field hidden or not from component config.
	 *
	 * @param   string  $field_name  Field Name
	 *
	 * @return  true/false
	 */
	public function filedToShowOrHide($field_name)
	{
		$params       = JComponentHelper::getParams('com_jgive');
		$creatorfield = array();
		$creatorfield = $params->get('creatorfield');
		$show_selected_fields = $params->get('show_selected_fields');

		if ($show_selected_fields AND (!empty($creatorfield)))
		{
			// If field is hidden & not to show on form
			if (in_array($field_name, $creatorfield))
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Get Campaign Success Status Array
	 *
	 * @return  Options
	 */
	public function getCampaignSuccessStatusArray()
	{
		$campaignSuccessStatus   = array();
		$campaignSuccessStatus[] = JHtml::_('select.option', 0, JText::_('COM_JGIVE_SUCCESS_STATUS_ONGOING'));
		$campaignSuccessStatus[] = JHtml::_('select.option', 1, JText::_('COM_JGIVE_SUCCESS_STATUS_SUCCESSFUL'));
		$campaignSuccessStatus[] = JHtml::_('select.option', -1, JText::_('COM_JGIVE_SUCCESS_STATUS_FAILED'));

		return $campaignSuccessStatus;
	}

	/**
	 * Get Campaigns By Success Status
	 *
	 * @param   string  $successStatus  State
	 *
	 * @return campaign state
	 */
	public function getCampaignsBySuccessStatus($successStatus)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT c.id
		 FROM `#__jg_campaigns` AS c
		 WHERE c.success_status= " . $successStatus . "
		 ORDER BY c.id";
		$db->setQuery($query);
		$campaigns = $db->loadColumn();

		return $campaigns;
	}

	/**
	 * Generate Campaign Sucess Status
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  Campaign state
	 */
	public function generateCampaignSucessStatus($cid)
	{
		$cdata['campaign'] = new stdclass;

		// Get campaigns details.
		$cdata['campaign'] = $this->getCampaignDetails($cid);

		// Get campaign amounts.
		$amounts                             = $this->getCampaignAmounts($cid);
		$cdata['campaign']->amount_received  = $amounts['amount_received'];
		$cdata['campaign']->remaining_amount = $amounts['remaining_amount'];

		/* 0 - Ongoing.
		 1 - Successful.
		 -1 - Failed.
		*/

		$campaignSuccessStatus = 0;

		if ($cdata['campaign']->amount_received >= $cdata['campaign']->goal_amount)
		{
			/* Changed by deepa
			1 - Successful.*/
			$campaignSuccessStatus = 1;

			/*if (date('Y-m-d') > $cdata['campaign']->end_date)
			{
				$campaignSuccessStatus = 1;
			}
			elseif ($cdata['campaign']->allow_exceed)
			{
				$campaignSuccessStatus = 0;
			}*/
		}
		else
		{
			if (date('Y-m-d') > $cdata['campaign']->end_date)
			{
				// -1 - Failed.
				$campaignSuccessStatus = -1;
			}
			else
			{
				// 0 - Ongoing.
				$campaignSuccessStatus = 0;
			}
		}

		return $campaignSuccessStatus;
	}

	/**
	 * Update CampaignSuccessStatus
	 *
	 * @param   integer  $cid                    Cid
	 * @param   string   $campaignSuccessStatus  Stae
	 * @param   integer  $orderId                Orderid
	 *
	 * @return boolean true/false
	 */
	public function updateCampaignSuccessStatus($cid = 0, $campaignSuccessStatus = null, $orderId = 0)
	{
		// If cid not passed, get cid from orderid.
		if (!$cid && $orderId)
		{
			$donationsHelper = new donationsHelper;
			$cid             = $donationsHelper->getCidFromOrderId($orderId);
		}

		// If cid not found, return.
		if (!$cid)
		{
			return false;
		}

		// If campaign success status is not passed.
		if ($campaignSuccessStatus === null)
		{
			// Get campaign success status.
			$campaignSuccessStatus = $this->generateCampaignSucessStatus($cid);
		}

		$db = JFactory::getDBO();

		// Update campaign success status.
		// Create an object.
		$object = new stdClass;

		// Must be a valid primary key value.
		$object->id             = $cid;
		$object->success_status = $campaignSuccessStatus;

		// Update record.
		$result = $db->updateObject('#__jg_campaigns', $object, 'id');

		if (!$result)
		{
			return false;
		}

		// Start - Plugin trigger OnAfterJGiveCampaignSuccessStatusChange.
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');
		$result = $dispatcher->trigger('OnAfterJGiveCampaignSuccessStatusChange', array($cid, $campaignSuccessStatus));

		return true;
	}

	/**
	 * Update Campaign Processed Flag
	 *
	 * @param   integer  $cid                    Cid
	 * @param   STRING   $campaignProcessedFlag  Flag
	 * @param   integer  $orderId                Order Id
	 *
	 * @return  Boolean  true/false
	 */
	public function updateCampaignProcessedFlag($cid = 0, $campaignProcessedFlag = null, $orderId = 0)
	{
		// If cid not passed, get cid from orderid.
		if (!$cid && $orderId)
		{
			$donationsHelper = new donationsHelper;
			$cid             = $donationsHelper->getCidFromOrderId($orderId);
		}

		// If cid not found, return.
		if (!$cid)
		{
			return false;
		}

		// If campaign success status is not passed.
		if ($campaignProcessedFlag === null)
		{
			// Set default campaign success status.
			$campaignProcessedFlag = 'NA';
		}

		$db = JFactory::getDBO();

		// Create an object.
		$object = new stdClass;

		// Must be a valid primary key value.
		$object->id             = $cid;
		$object->processed_flag = $campaignProcessedFlag;

		// Update record.
		$result = $db->updateObject('#__jg_campaigns', $object, 'id');

		if ($result)
		{
			return true;
		}
		else
		{
			return false;
		}

		return true;
	}

	/**
	 * Get Campaign id from title
	 *
	 * @param   STRING  $title  Title
	 *
	 * @return  Array  Camp Id
	 */
	public function getCampaignidFromtitle($title)
	{
		$db    = JFactory::getDBO();
		$query = 'SELECT c.id
			FROM #__jg_campaigns AS c
			WHERE c.title="' . $title . '"';
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 *  Get cat alias
	 *
	 * @param   INT  $catid  Cat id
	 *
	 * @return  cat alias
	 */
	public function getCatalias($catid)
	{
		if ($catid)
		{
			$db    = JFactory::getDBO();
			$query = "SELECT alias FROM #__categories as cat
					WHERE cat.id=" . $catid . " AND `extension`='com_jgive'";
			$db->setquery($query);

			return $result = $db->loadResult();
		}
	}

	/**
	 * Get cat alias
	 *
	 * @param   STRING  $catalias  Cat alias
	 *
	 * @return  Array   Categories
	 */
	public function getCatidbyalias($catalias)
	{
		if ($catalias)
		{
			$db    = JFactory::getDBO();
			$query = "SELECT id FROM #__categories as cat
					WHERE cat.alias='" . $catalias . "' AND `extension`='com_jgive'";
			$db->setquery($query);

			return $result = $db->loadResult();
		}
	}

	/**
	 * Get date difference in days
	 *
	 * @param   Date  $date1  Date 1
	 * @param   Date  $date2  Date 2
	 *
	 * @return  INT  days
	 */
	public function getDateDiffInDays($date1, $date2)
	{
		$datetime1 = new DateTime($date1);
		$datetime2 = new DateTime($date2);

		$interval = date_diff($datetime1, $datetime2);

		return $interval->days;
	}

	/**
	 * Get campaign Caculation like - main image path, days left, progress percentage
	 *
	 * @param   Object  $cdata                 Campaign info Object
	 * @param   INT     $singleCampaignItemid  Item Id
	 *
	 * @return  Object  Mapped Data
	 */
	public function mapData($cdata, $singleCampaignItemid)
	{
		// Get Campaign main image
		foreach ($cdata['images'] as $img)
		{
			$path      = 'images/jGive/';
			$fileParts = pathinfo($img->path);

			if ($img->gallery == 0)
			{
				// Get original image name to find it resize images (S,M,L)
				// If loop for old version compatibility (where img resize not available means no L , M ,S before image name)
				if (file_exists($path . $fileParts['basename']))
				{
					$cdata['campaign']->campaign_thumb = JUri::root() . $path . $fileParts['basename'];
					break;
				}
				else
				{
					$cdata['campaign']->campaign_thumb = JUri::root() . $path . 'L_' . $fileParts['basename'];
					break;
				}
			}
		}

		// Calculate days left
		$curr_date                    = JFactory::getDate()->Format('Y-m-d');
		$time_curr_date               = strtotime($curr_date);
		$time_end_date                = strtotime($cdata['campaign']->end_date);
		$interval                     = ($time_end_date - $time_curr_date);
		$cdata['campaign']->days_left = floor($interval / (60 * 60 * 24));

		// Calculate Progress Percentage
		$goal_amount = (float) $cdata['campaign']->goal_amount;

		if (!empty($cdata['campaign']->amount_received) && $goal_amount > 0)
		{
			$cdata['campaign']->progress_per = number_format(($cdata['campaign']->amount_received / $cdata['campaign']->goal_amount) * 100, 2) . '%';
		}
		else
		{
			$cdata['campaign']->progress_per = '0.00%';
		}

		$camplink = 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cdata['campaign']->id . '&Itemid=' . $singleCampaignItemid;
		$cdata['campaign']->link = JUri::root() . substr(JRoute::_($camplink), strlen(JUri::base(true)) + 1);

		return $cdata;
	}

	/**
	 * Format campaign title for alias
	 *
	 * @param   STRING  $title      Campaign title
	 * @param   INT     $oldCid_id  Campaign id
	 *
	 * @return  Formated campaign title
	 */
	public function formatttedTitle($title, $oldCid_id = '')
	{
		$db          = JFactory::getDBO();
		$user        = JFactory::getUser();
		$i           = 1;
		$final_title = $title;

		do
		{
			if ($i == 1)
			{
				$status = self::ckUniqueCampaignTitle($title, $oldCid_id);
			}
			else
			{
				$final_title = $title . $i;
				$status      = self::ckUniqueCampaignTitle($final_title);
			}

			// Generate new vanity url
			$i++;
		}
		while ($status != 0);

		return $db->escape(trim($final_title), true);
	}

	/**
	 * Check Unique Campaign Title
	 *
	 * @param   STRING  $title      Campaign title
	 * @param   INT     $oldCid_id  Campaign id
	 *
	 * @return  1:IF vanity already exist & 0:IF vanity is ot exist
	 */
	public function ckUniqueCampaignTitle($title, $oldCid_id = '')
	{
		$db      = JFactory::getDBO();
		$where   = array();
		$title   = $db->quote($db->escape(trim($title), true));
		$where[] = "`alias`= $title ";

		if (!empty($oldCid_id))
		{
			$where[] = ' `id`!=\'' . $oldCid_id . '\' ';
		}

		$where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');
		$query = 'SELECT `id` FROM `#__jg_campaigns` ' . $where;
		$db->setQuery($query);
		$id = $db->loadResult();

		if (!empty($id))
		{
			// Present title URL
			return 1;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * This function gives formatted vanity url
	 *
	 * @param   STRING  $vanityurl  Campaign alias
	 * @param   STRING  $title      Campaign title
	 * @param   INT     $oldCid_id  Campaign id
	 *
	 * @return  1:IF vanity already exist & 0:IF vanity is ot exist
	 */
	public function formatttedVanityURL($vanityurl, $title, $oldCid_id = '')
	{
		$user       = JFactory::getUser();
		$title = trim($title);

		if (trim($vanityurl) == '')
		{
			$vanityurl = $title;
		}

		$final_vanity = $vanityurl;

		// Remove all space, tab, new line
		$final_vanity = JApplication::stringURLSafe($final_vanity);

		if (trim(str_replace('-', '', $final_vanity)) == '')
		{
			$final_vanity = $user->id . '-' . JFactory::getDate()->format('Y-m-d-H-i-s');
		}

		$i = 1;

		do
		{
			if ($i == 1)
			{
				$status = self::ckUniqueVanityURL($vanityurl, $oldCid_id);
			}
			else
			{
				// Remove userid: from vanity url if exist AS WE R GOING TO APPEND NEXT
				$vanityurl    = preg_replace('/' . $user->id . '-' . '/', '', $vanityurl, 1);
				$final_vanity = $newvanity = $user->id . '-' . $vanityurl . $i;

				// Pattern, replacement, string, limit
				$status = self::ckUniqueVanityURL($newvanity);
			}

			// Generate new vanity url
			$i++;
		}
		while ($status != 0);

		return $final_vanity;
	}

	/**
	 * Get date difference in days
	 *
	 * @param   INT  $alias       Campaign alias
	 * @param   INT  $oldcamp_id  Campaign id
	 *
	 * @return  1:IF alias already exist & 0:IF alias is ot exist
	 */
	public function ckUniqueVanityURL($alias, $oldcamp_id = '')
	{
		$db      = JFactory::getDBO();
		$where   = array();
		$alias  = $db->quote($db->escape($alias, true));
		$where[] = "`alias`= $alias ";

		if (!empty($oldcamp_id))
		{
			$where[] = ' `id`!=\'' . $oldcamp_id . '\' ';
		}

		$where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');
		$query = 'SELECT `id` FROM `#__jg_campaigns` ' . $where;
		$db->setQuery($query);
		$id = $db->loadResult();

		if (!empty($id))
		{
			// Present alias URL
			return 1;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Method for formatting time
	 *
	 * @param   array  $post  Array
	 *
	 * @return  void
	 *
	 * @since   3.0
	 */
	public function getFormattedTime($post)
	{
		$params = JComponentHelper::getParams('com_jgive');
		$campaign_period_in_days = $params->get('campaign_period_in_days');

		// Get all non-jform data for campaign start time fields.
		$campaign_start_time_ampm = strtolower($post->get('campaign_start_time_ampm', '', 'string'));
		$campaign_start_time_hour = $post->get('campaign_start_time_hour', '', 'int');
		$campaign_start_time_min  = $post->get('campaign_start_time_min', '', 'int');
		$campaign_start_time      = $campaign_start_time_hour;

		// Convert hours into 24 hour format.
		if (($campaign_start_time_ampm == 'pm') && ($campaign_start_time_hour != '12'))
		{
			$campaign_start_time = $campaign_start_time_hour + 12;
		}
		elseif (($campaign_start_time_ampm == 'am') && ($campaign_start_time_hour == '12'))
		{
			$campaign_start_time = $campaign_start_time_hour - 12;
		}

		// Get minutes and attach seconds.
		$campaign_start_time .= ":" . $campaign_start_time_min;
		$campaign_start_time .= ":" . '00';

		if (!empty($campaign_period_in_days) || $campaign_period_in_days != 0)
		{
			// If days is set then set default value
			$campaign_end_time_ampm = 'pm';
			$campaign_end_time_hour = 11;
			$campaign_end_time_min  = 59;
			$campaign_end_time      = $campaign_end_time_hour;
		}
		else
		{
			// Get all non-jform data for campaign start time fields.
			$campaign_end_time_ampm = strtolower($post->get('campaign_end_time_ampm', '', 'string'));
			$campaign_end_time_hour = $post->get('campaign_end_time_hour', '', 'int');
			$campaign_end_time_min  = $post->get('campaign_end_time_min', '', 'int');
			$campaign_end_time      = $campaign_end_time_hour;
		}

		// Convert hours into 24 hour format.
		if (($campaign_end_time_ampm == 'pm') && ($campaign_end_time_hour != '12'))
		{
			$campaign_end_time = $campaign_end_time_hour + 12;
		}
		elseif (($campaign_end_time_ampm == 'am') && ($campaign_end_time_hour == '12'))
		{
			$campaign_end_time = $campaign_end_time_hour - 12;
		}

		// Get minutes and attach seconds.
		$campaign_end_time .= ":" . $campaign_end_time_min;
		$campaign_end_time .= ":" . '00';

		$formattedTime = array();

		// Set return values.
		$formattedTime['campaign_start_time'] = $campaign_start_time;
		$formattedTime['campaign_end_time']   = $campaign_end_time;

		return $formattedTime;
	}
}
