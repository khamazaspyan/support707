<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

// Component Helper
jimport('joomla.application.component.helper');
jimport('techjoomla.tjmail.mail');

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

if (!class_exists('campaignHelper'))
{
	JLoader::register('campaignHelper', $helperPath);
	JLoader::load('campaignHelper');
}

/**
 * DonationsHelper form controller class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class DonationsHelper
{
	/**
	 * Process Refund
	 *
	 * @param   Array  $orderData  Description
	 *
	 * @return  0/1
	 */
	public function processRefund($orderData)
	{
		/*[id] => 13
		 *
		[order_id] => JGOID-00013
		[fund_holder] => 0
		[status] => C
		[processor] => ewallet
		[amount] => 1.00
		[fee] => 1.10
		[cdate] => 2013-09-25 10:26:47
		[donor_id] => 641
		[cid] => 10
		[title] => Test 2 Campaigns11
		*/

		$data                        = array();
		$data['order_id']            = $orderData->id;
		$data['user_id']             = $orderData->donor_id;
		$data['total']               = $orderData->amount;
		$data['client']              = 'com_jgive';
		$data['payment_description'] = JText::_('COM_JGIVE_PROCESS_REFUND_DEFAULT_MSG') . ' ' . $orderData->title;
		$data['return']              = '';

		if ($orderData->processor == 'ewallet')
		{
			JPluginHelper::importPlugin('payment', $orderData->processor);
			$dispatcher = JDispatcher::getInstance();
			$result     = $dispatcher->trigger('onTP_ProcessRefund', array($data));

			if ($result[0]['status'] == 'C')
			{
				$comment = JText::_('COM_JGIVE_PROCESS_REFUND_DEFAULT_MSG') . ' ' . $orderData->title;
				$this->updatestatus($result[0]['order_id'], 'RF', $comment, 1);

				// Start - Plugin trigger OnAfterJGivePaymentProcess.
				$dispatcher = JDispatcher::getInstance();
				JPluginHelper::importPlugin('system');

				// Params - orderId, newStatus, comment, sendEmail
				$result = $dispatcher->trigger('OnAfterJGivePaymentStatusChange', array($result[0]['order_id'],'RF',$comment,1));

				return 1;
			}
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Get Donations By Status
	 *
	 * @param   integer  $cid          Cid
	 * @param   string   $orderStatus  Order status
	 *
	 * @return  Get data according to status
	 */
	public function getDonationsByStatus($cid, $orderStatus)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT i.id, i.order_id, i.fund_holder, i.status, i.processor, i.amount, i.fee, i.cdate,
		d.user_id AS donor_id,
		c.id AS cid, c.title
		FROM #__jg_orders AS i
		LEFT JOIN #__jg_campaigns AS c ON c.id=i.campaign_id
		LEFT JOIN #__jg_donors AS d on d.id=i.donor_id
		WHERE i.status = " . $db->Quote($orderStatus) . "
		AND c.id = " . $cid;
		$db->setQuery($query);
		$donations = $db->loadObjectList();

		return $donations;
	}

	/**
	 * Get status Array
	 *
	 * @return  options
	 */
	public function getPStatusArray()
	{
		$pstatus   = array();
		$pstatus[] = JHtml::_('select.option', 'P', JText::_('COM_JGIVE_PENDING'));
		$pstatus[] = JHtml::_('select.option', 'C', JText::_('COM_JGIVE_CONFIRMED'));
		$pstatus[] = JHtml::_('select.option', 'RF', JText::_('COM_JGIVE_REFUND'));
		$pstatus[] = JHtml::_('select.option', 'E', JText::_('COM_JGIVE_CANCELED'));
		$pstatus[] = JHtml::_('select.option', 'D', JText::_('COM_JGIVE_DENIED'));

		return $pstatus;
	}

	/**
	 * Get S Status Array
	 *
	 * @return options
	 */
	public function getSStatusArray()
	{
		$sstatus = array();
		$app     = JFactory::getApplication();

		if ($app->issite() OR JVERSION < 3.0)
		{
			$sstatus[] = JHtml::_('select.option', '-1', JText::_('COM_JGIVE_APPROVAL_STATUS'));
		}

		$sstatus[] = JHtml::_('select.option', 'E', JText::_('COM_JGIVE_CANCELED'));
		$sstatus[] = JHtml::_('select.option', 'D', JText::_('COM_JGIVE_DENIED'));
		$sstatus[] = JHtml::_('select.option', 'P', JText::_('COM_JGIVE_PENDING'));
		$sstatus[] = JHtml::_('select.option', 'C', JText::_('COM_JGIVE_CONFIRMED'));
		$sstatus[] = JHtml::_('select.option', 'RF', JText::_('COM_JGIVE_REFUND'));

		return $sstatus;
	}

	/**
	 * Send Order Email
	 *
	 * @param   INT     $orders_key  Order id
	 * @param   string  $campid      Camp id
	 *
	 * @return void
	 */
	public function sendOrderEmail($orders_key, $campid = '')
	{
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$db                  = JFactory::getDBO();

		$session     = JFactory::getSession();
		$guest_email = '';
		$Itemid      = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
		$params      = JComponentHelper::getParams('com_jgive');
		JRequest::setVar('donationid', $orders_key);

		$params              = JComponentHelper::getParams('com_jgive');
		$this->currency_code = $params->get('currency');
		$pstatus             = array();
		$pstatus[]           = JHtml::_('select.option', 'P', JText::_('COM_JGIVE_PENDING'));
		$pstatus[]           = JHtml::_('select.option', 'C', JText::_('COM_JGIVE_CONFIRMED'));
		$pstatus[]           = JHtml::_('select.option', 'RF', JText::_('COM_JGIVE_REFUND'));
		$pstatus[]           = JHtml::_('select.option', 'E', JText::_('COM_JGIVE_CANCELED'));
		$pstatus[]           = JHtml::_('select.option', 'D', JText::_('COM_JGIVE_DENIED'));

		$this->pstatus = $pstatus;

		$query = "SELECT o.id, o.order_id, o.amount, d.email, d.first_name, d.last_name
		FROM #__jg_orders AS o
		LEFT JOIN #__jg_donors as d ON d.id=o.donor_id
		WHERE o.id =" . $orders_key;
		$db->setQuery($query);
		$orderuser = $db->loadObjectList();

		$this->donation_details = $this->getSingleDonationInfo($orders_key);

		$this->donations_site  = 1;
		$this->donations_email = 1;

		$mainframe = JFactory::getApplication();
		$site      = $mainframe->getCfg('sitename');

		if ($this->donation_details['campaign']->type == 'donation')
		{
			$html = '<br/><div>' . JText::sprintf('COM_JGIVE_ORDER_MAIL_MSG', $site) . '</div>';
		}
		else
		{
			$html = '<br/><div>' . JText::sprintf('COM_JGIVE_INVESTMENT_ORDER_MAIL_MSG', $site) . '</div>';
		}

		$guest_email = $billemail = $this->donation_details['donor']->email;
		$guest_email = md5($guest_email);

		$this->mailContent = 1;

		$this->retryPayment = new StdClass;
		$this->retryPayment->status = '';
		$this->retryPayment->msg = '';

		ob_start();
		include JPATH_SITE . '/components/com_jgive/views/donations/tmpl/details.php';
		$html .= ob_get_contents();
		ob_end_clean();

		$order_id = $this->getOrderIdFromOrderIdKey($orders_key);
		$body     = $html;

		$reclink  = 'index.php?option=com_jgive&view=donations&layout=details&donationid=' . $orders_key . '&email=' . $guest_email . '&Itemid=' . $Itemid;
		$link     = JUri::root() . substr(JRoute::_($reclink), strlen(JUri::base(true)) + 1);
		$link     = '<a href="' . $link . '">' . $link . '</a>';
		$body .= $link;

		if ($this->donation_details['campaign']->type == 'donation')
		{
			$subject = JText::sprintf('COM_JGIVE_ORDER_MAIL_SUB', $site, $order_id);
		}
		else
		{
			$subject = JText::sprintf('COM_JGIVE_INVESTMENT_ORDER_MAIL_SUB', $site, $order_id);
		}

		// Check if email is to be sent for new orders to donor.
		$mail_recipients_new_order = $params->get('mail_recipients_new_order');

		$body = TjMail::TagReplace($body, $this->donation_details);

		if (is_array($mail_recipients_new_order))
		{
			if (in_array('donor', $mail_recipients_new_order))
			{
				$this->sendmail($billemail, $subject, $body, '', 'campaign.donation.pending');
			}
		}

		// Send email donation email to campaign promoter & site admin
		$query = "SELECT u.email FROM #__jg_campaigns as camp
		LEFT JOIN #__users as u on u.id = camp.creator_id
		where camp.id=" . $campid;
		$db->setQuery($query);
		$promoteremail = $db->loadResult();

		// Email body
		if (!empty($promoteremail))
		{
			if ($this->donation_details['campaign']->type == 'donation')
			{
				$html = '<br/><div>' . JText::sprintf('COM_JGIVE_ORDER_MAIL_PROMOTER_MSG', $site) . '</div>';
			}
			else
			{
				$html = '<br/><div>' . JText::sprintf('COM_JGIVE_INVESTMENT_ORDER_MAIL_PROMOTER_MSG', $site) . '</div>';
			}

			include JPATH_SITE . '/components/com_jgive/views/donations/tmpl/details.php';
			$html .= ob_get_contents();
			ob_end_clean();

			$order_id = $this->getOrderIdFromOrderIdKey($orders_key);
			$body     = $html;
			$campLink = 'index.php?option=com_jgive&view=donations&layout=details&donationid=' . $orders_key . '&email=' . $guest_email . '&Itemid=' . $Itemid;
			$link     = JUri::root() . substr(JRoute::_($campLink), strlen(JUri::base(true)) + 1);
			$link     = '<a href="' . $link . '">' . $link . '</a>';
			$body .= $link;

			$body = TjMail::TagReplace($body, $this->donation_details);

			// Check if email is to be sent for new orders to promoter.
			if (is_array($mail_recipients_new_order))
			{
				if (in_array('promoter', $mail_recipients_new_order))
				{
					$this->sendmail($promoteremail, $subject, $body, $params->get('email'), 'campaign.donation.pending');
				}
			}
		}
	}

	/**
	 * Sendmail
	 *
	 * @param   string  $recipient   Email
	 * @param   string  $subject     Email sub
	 * @param   string  $body        Email body
	 * @param   string  $bcc_string  Email bcc
	 * @param   string  $action      Mail to send for action e.g donation.made, campaign.create etc
	 *
	 * @return void
	 */
	public function sendmail($recipient, $subject, $body, $bcc_string = '', $action = "donation.made")
	{
		global $mainframe;
		$mainframe   = JFactory::getApplication();
		$from        = $mainframe->getCfg('mailfrom');
		$fromname    = $mainframe->getCfg('fromname');
		$recipient   = trim($recipient);
		$mode        = 1;
		$cc          = null;
		$bcc         = explode(',', $bcc_string);
		$attachment  = null;
		$replyto     = null;
		$replytoname = null;

		$mailer = JFactory::getMailer();
		$mailer->setSender(array($from, $fromname));
		$mailer->setSubject($subject);
		$mailer->setBody($body);
		$mailer->addRecipient($recipient);
		$mailer->AddCC($cc);

		if ($bcc_string != null)
		{
			if (count($bcc))
			{
				if ($bcc[0])
				{
					$mailer->addBCC($bcc);
				}
			}
		}

		$mailer->addAttachment($attachment);

		if (is_array($replyto))
		{
			$numReplyTo = count($replyto);

			for ($i = 0; $i < $numReplyTo; $i++)
			{
				$mailer->addReplyTo(array($replyto[$i], $replytoname[$i]));
			}
		}
		elseif (isset($replyto))
		{
			$mailer->addReplyTo(array($replyto, $replytoname));
		}

		$mailer->isHtml(true);
		$mailer->AddCustomHeader("X-Custom-Header:" . $action);

		$mail_result = $mailer->Send();

		return $mail_result;
	}

	/**
	 * Used in donation details view
	 *
	 * @param   string  $order_id_key  Orderid key
	 *
	 * @return void
	 */
	public function getSingleDonationInfo($order_id_key = '')
	{
		$db = JFactory::getDBO();

		if (!$order_id_key)
		{
			$order_id_key = JFactory::getApplication()->input->get('donationid');
		}

		if (!$order_id_key)
		{
			return;
		}

		$query = "SELECT donation_id
		FROM `#__jg_orders`
		WHERE `id`=" . $order_id_key;
		$db->setQuery($query);
		$donation_id = $db->loadResult();

		// Since jGive version 1.6
		if ($donation_id)
		{
			$query = "SELECT *
			FROM `#__jg_donations`
			WHERE `id`=" . $donation_id;
			$db->setQuery($query);
			$donation = $db->loadObject();
		}
		else // Support earlier version of jGive upto 1.5
		{
			$query = "SELECT *
			FROM `#__jg_donations`
			WHERE `order_id`=" . $order_id_key;
			$db->setQuery($query);
			$donation = $db->loadObject();
		}

		$query = "SELECT campaign_id
		FROM `#__jg_orders`
		WHERE `id`=" . $order_id_key;
		$db->setQuery($query);
		$cid = $db->loadResult();

		$query = "SELECT c.*
		FROM `#__jg_campaigns` AS c
		WHERE c.id=" . $cid;
		$db->setQuery($query);
		$campaign          = $db->loadObject();
		$cdata['campaign'] = $campaign;

		$query = "SELECT SUM(o.amount) AS amount_received
		FROM `#__jg_orders` AS o
		WHERE o.campaign_id=" . $cid . "
		AND o.status='C'";
		$db->setQuery($query);
		$cdata['campaign']->amount_received = $db->loadResult();

		// For Donation Receipt
		$query   = $db->getQuery(true);
		$query->select('o.amount AS donation_amount');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->where(
						$db->qn('o.campaign_id') . ' = ' . $db->quote($cid) . ' AND ' .
						$db->qn('o.status') . ' = ' . $db->quote('C') . ' AND ' .
						$db->qn('o.id') . ' = ' . $db->quote($order_id_key)
						);

		$db->setQuery($query);
		$cdata['campaign']->donation_amount = $db->loadResult();

		// If no donations, set receved amount as zero
		if ($cdata['campaign']->amount_received == '')
		{
			$cdata['campaign']->amount_received = 0;
		}

		// Calculate remaining amount
		$cdata['campaign']->remaining_amount = ($cdata['campaign']->goal_amount) - ($cdata['campaign']->amount_received);

		$donation_details                    = array();
		$donation_details['campaign']        = $cdata['campaign'];

		if ($donation->donor_id)
		{
			$donor = $this->getDonorDetails($donation->donor_id);
		}

		// Needed for loading states
		$jgiveFrontendHelper = new jgiveFrontendHelper;

		$donor->country_name = $jgiveFrontendHelper->getCountryNameFromId($donor->country);
		$donor->state_name   = $jgiveFrontendHelper->getRegionNameFromId($donor->state, $donor->country);

		$donor->city_name = $jgiveFrontendHelper->getCityNameFromId($donor->city, $donor->country);

		// If name is missing means it may be other city enter by user by selecting other city check
		if (!$donor->city_name)
		{
			// So add this city
			$donor->city_name = $donor->city;
		}

		$donation_details['donor']                        = $donor;
		$payment                                          = $this->getPaymentDetails($order_id_key);
		$donation_details['payment']                      = $payment;
		$donation_details['payment']->annonymous_donation = $donation->annonymous_donation;

		return $donation_details;
	}

	/**
	 * Get Donor details
	 *
	 * @param   INT  $donor_id  donor_id
	 *
	 * @return donors
	 */
	public function getDonorDetails($donor_id)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT *
		FROM `#__jg_donors`
		WHERE `id`=" . $donor_id;
		$db->setQuery($query);
		$donor = $db->loadObject();

		return $donor;
	}

	/**
	 * Used in donation info view
	 *
	 * @param   INT  $order_id_key  Order id key
	 *
	 * @return  Donors
	 */
	public function getPaymentDetails($order_id_key)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT o.*, d.giveback_id, d.is_recurring, d.recurring_frequency,d.recurring_count, g.description as giveback_desc

		FROM `#__jg_orders` as o
		LEFT JOIN `#__jg_donations` as d ON o.donation_id = d.id
		LEFT JOIN `#__jg_campaigns_givebacks` as g ON d.giveback_id = g.id
		WHERE o.`id`=" . $order_id_key;
		$db->setQuery($query);
		$donor = $db->loadObject();

		return $donor;
	}

	/**
	 * Function to update status of order
	 *
	 * @param   INT      $order_id_key        Order id
	 * @param   String   $status              Order status
	 * @param   string   $comment             Comment
	 * @param   integer  $send_mail           Send mail
	 * @param   integer  $duplicate_response  Duplicate response flag
	 *
	 * @return  void
	 */
	public function updatestatus($order_id_key, $status, $comment = '', $send_mail = 1, $duplicate_response = 0)
	{
		global $mainframe;
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$session             = JFactory::getSession();
		$guest_email         = '';
		$guest_email         = $session->get('order_link_guestemail');

		if (empty($guest_email))
		{
			$db    = JFactory::getDBO();
			$query = "SELECT email FROM `#__jg_donors` as d
					LEFT JOIN `#__jg_orders` as o ON o.donor_id =d.id
					WHERE o.id=" . $order_id_key;
			$db->setQuery($query);
			$guest_email = md5($db->loadResult());
		}

		$session->clear('order_link_guestemail');

		$mainframe   = JFactory::getApplication();
		$db          = JFactory::getDBO();
		$res         = new stdClass;
		$res->id     = $order_id_key;
		$res->status = $status;

		if (!$db->updateObject('#__jg_orders', $res, 'id'))
		{
			return 2;
		}

		if ($send_mail == 1 AND ($duplicate_response == 0))
		{
			$params = JComponentHelper::getParams('com_jgive');

			$query = "SELECT o.id, o.order_id,o.campaign_id, o.amount, d.email, d.first_name, d.last_name
			FROM #__jg_orders AS o
			LEFT JOIN #__jg_donors as d ON d.id=o.donor_id
			WHERE o.id =" . $order_id_key;
			$db->setQuery($query);
			$orderuser = $db->loadObjectList();

			$input = JFactory::getApplication()->input;

			$orderuser = $orderuser[0];

			switch ($status)
			{
				case 'C':
					$orderstatus = JText::_('COM_JGIVE_CONFIRMED');
					break;
				case 'RF':
					$orderstatus = JText::_('COM_JGIVE_REFUND');
					break;

				case 'P':
					$orderstatus = JText::_('COM_JGIVE_PENDING');
					break;
				case 'E':
					$orderstatus = JText::_('COM_JGIVE_CANCELED');
					break;
				case 'D':
					$orderstatus = JText::_('COM_JGIVE_DENIED');
					break;
			}

			$this->donation_details = $this->getSingleDonationInfo($order_id_key);

			$emaillink = JPATH_ADMINISTRATOR . '/components/com_jgive/models/email_template.php';

			if (!class_exists('JgiveModelEmail_Template'))
			{
				JLoader::register('JgiveModelEmail_Template', $emaillink);
				JLoader::load('JgiveModelEmail_Template');
			}

			$email_temp = new JgiveModelEmail_Template;

			if ($this->donation_details['payment']->status == 'C')
			{
				$emailmodel = $email_temp->generateDonationReceipt($this->donation_details['payment']->donation_id);
			}

			if ($this->donation_details['campaign']->type == 'donation')
			{
				$body = JText::_('COM_JGIVE_STATUS_CHANGE_BODY');
			}
			else
			{
				$body = JText::_('COM_JGIVE_INVESTMENT_STATUS_CHANGE_BODY');
			}

			$site = $mainframe->getCfg('sitename');

			if ($comment)
			{
				$comment = str_replace('{COMMENT}', $comment, JText::_('COM_JGIVE_COMMENT_TEXT'));
				$find    = array('{ORDERNO}','{STATUS}','{SITENAME}','{NAME}','{COMMENTTEXT}');
				$replace = array(
					$orderuser->order_id,
					$orderstatus,
					$site,
					$orderuser->first_name,
					$comment
				);
			}
			else
			{
				$find = array('{ORDERNO}','{STATUS}','{SITENAME}','{NAME}','{COMMENTTEXT}');
				$replace = array($orderuser->order_id,$orderstatus,$site,$orderuser->first_name,'');
			}

			$body       = str_replace($find, $replace, $body);
			$Itemid     = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
			$campLink   = 'index.php?option=com_jgive&view=donations&layout=details&donationid=' . $orderuser->id;
			$campLink .= '&email=' . $guest_email . '&Itemid=' . $Itemid;
			$link       = JUri::root() . substr(JRoute::_($campLink), strlen(JUri::base(true)) + 1);
			$order_link = '<a href="' . $link . '">' . $link . '</a>';
			$body       = str_replace('{LINK}', $order_link, $body);
			$body       = nl2br($body);

			if ($this->donation_details['campaign']->type == 'donation')
			{
				$subject = JText::sprintf('COM_JGIVE_STATUS_CHANGE_SUBJECT', $orderuser->order_id);
			}
			else
			{
				$subject = JText::sprintf('COM_JGIVE_INVESTMENT_STATUS_CHANGE_SUBJECT', $orderuser->order_id);
			}

			// Send mail to campaign donor && Check if email is to be sent for order status change.
			$send_mail_order_status_change = $params->get('send_mail_order_status_change', 1);

			if ($send_mail_order_status_change)
			{
				// Check if email is to be sent for order status change to donor.
				$mail_recipients_order_status_change = $params->get('mail_recipients_new_order');

				if (is_array($mail_recipients_order_status_change))
				{
					if (in_array('donor', $mail_recipients_order_status_change))
					{
						$this->sendmail($orderuser->email, $subject, $body, $params->get('mail'), 'campaign.donation.statuschange');
					}
				}
			}

			// Send mail to campaig promoter
			$campaignHelper  = new campaignHelper;
			$campaignDetails = $campaignHelper->getCampaignDetails($orderuser->campaign_id);
			$promoteremailId = JFactory::getUser($campaignDetails->creator_id)->email;
			$promoterName    = JFactory::getUser($campaignDetails->creator_id)->name;

			// Email subject
			if ($this->donation_details['campaign']->type == 'donation')
			{
				$subject = JText::sprintf('COM_JGIVE_STATUS_CHANGE_SUBJECT_PROMOTER', $orderuser->order_id);
			}
			else
			{
				$subject = JText::sprintf('COM_JGIVE_INVESTMENT_STATUS_CHANGE_SUBJECT_PROMOTER', $orderuser->order_id);
			}

			// Email content
			if ($this->donation_details['campaign']->type == 'donation')
			{
				$body = JText::_('COM_JGIVE_STATUS_CHANGE_BODY_PROMOTER');
			}
			else
			{
				$body = JText::_('COM_JGIVE_INVESTMENT_STATUS_CHANGE_BODY_PROMOTER');
			}

			$site = $mainframe->getCfg('sitename');

			if ($comment)
			{
				$comment = str_replace('{COMMENT}', $comment, JText::_('COM_JGIVE_COMMENT_TEXT'));
				$find    = array('{ORDERNO}','{STATUS}','{SITENAME}','{NAME}','{COMMENTTEXT}');
				$replace = array($orderuser->order_id,$orderstatus,$site,$promoterName,$comment);
			}
			else
			{
				$find    = array('{ORDERNO}','{STATUS}','{SITENAME}','{NAME}','{COMMENTTEXT}');
				$replace = array($orderuser->order_id,$orderstatus,$site,$promoterName,'');
			}

			$body = str_replace($find, $replace, $body);
			$body = nl2br($body);

			// Send mail to campaign promoter. && Check if email is to be sent for order status change.
			$send_mail_order_status_change = $params->get('send_mail_order_status_change', 1);

			if ($send_mail_order_status_change)
			{
				// Check if email is to be sent for order status change to donor.
				$mail_recipients_order_status_change = $params->get('mail_recipients_new_order');

				if (is_array($mail_recipients_order_status_change))
				{
					if (in_array('promoter', $mail_recipients_order_status_change))
					{
						$this->sendmail($promoteremailId, $subject, $body, $params->get('mail'), 'campaign.donation.statuschange');
					}
				}
			}
		}

		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');
		$result = $dispatcher->trigger('OnAfterJGivePaymentStatusChange', array($order_id_key, $status ,$comment, 0));
	}

	/**
	 * Get Cid From OrderId, Used in plugin trigger
	 *
	 * @param   INT  $orderid  Order id
	 *
	 * @return  cid
	 */
	public function getCidFromOrderId($orderid)
	{
		$db = JFactory::getDBO();
		$query   = $db->getQuery(true);
		$query->select('o.campaign_id');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->where($db->qn('o.id') . ' = ' . $db->quote($orderid));
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get Donor Id From OrderId
	 *
	 * @param   INT  $orderid  order id
	 *
	 * @return donor id
	 */
	public function getDonorIdFromOrderId($orderid)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT d.user_id
		FROM #__jg_orders AS o
		LEFT JOIN #__jg_donors AS d ON d.id=o.donor_id
		WHERE o.id=" . $orderid;
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get OrderIdKey From OrderId
	 *
	 * @param   INT  $order_id  Order id
	 *
	 * @return Orderid
	 */
	public function getOrderIdKeyFromOrderId($order_id)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT o.id
		FROM #__jg_orders AS o
		WHERE o.order_id='" . $order_id . "'";
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get master primary key id from prefix master id
	 *
	 * @param   INT  $prefix_id  Prefix master order id
	 *
	 * @return master id
	 */
	public function getMasterIdFromPrefixMasterId($prefix_id)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT om.id
		FROM #__jg_order_master AS om
		WHERE om.prefix_id='" . $prefix_id . "'";
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Get OrderId From OrderId Key
	 *
	 * @param   INT  $order_id_key  Order id
	 *
	 * @return Order id primary key
	 */
	public function getOrderIdFromOrderIdKey($order_id_key)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT o.order_id
		FROM #__jg_orders AS o
		WHERE o.id='" . $order_id_key . "'";
		$db->setQuery($query);

		return $db->loadResult();
	}

	/**
	 * Send email to site admin when campaigns is created
	 *
	 * @param   Object  $camp_details  Camp details
	 * @param   INT     $camp_id       Camp id
	 *
	 * @return void
	 */
	public function sendCmap_create_mail($camp_details, $camp_id)
	{
		$userid    = JFactory::getUser()->id;
		$params    = JComponentHelper::getParams('com_jgive');
		$body      = JText::_('COM_JGIVE_CAMP_AAPROVAL_BODY');
		$body      = str_replace('{title}', $camp_details['title'], $body);
		$body      = str_replace('{campid}', ':' . $camp_id, $body);
		$body      = str_replace('{username}', $camp_details['first_name'], $body);
		$body      = str_replace('{userid}', $userid, $body);
		$body      = str_replace('{link}', JUri::base() . 'administrator/index.php?option=com_jgive&view=campaigns&layout=default&approve=1', $body);
		$billemail = $params->get('email');
		$subject   = JText::sprintf('COM_JGIVE_CAMP_CREATED_EMAIL_SUBJECT', $camp_details['title']);

		$this->sendmail($billemail, $subject, $body, $params->get('email'), 'campaign.create');
	}

	/**
	 * This function Checks whether order user and current logged use is same or not
	 *
	 * @param   INT  $orderuser  User id
	 *
	 * @return boolean
	 */
	public function getorderAuthorization($orderuser)
	{
		$user = JFactory::getUser();

		if ($user->id == $orderuser)
		{
			return 1;
		}

		return 0;
	}

	/**
	 * Get Order details
	 *
	 * @param   INT  $order_id_key  Order id
	 *
	 * @return Order Transaction Id And Status
	 */
	public function getOrderTransactoionIdAndStatus($order_id_key)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT o.transaction_id,o.status
		FROM #__jg_orders as o
		WHERE o.id=" . $order_id_key;
		$db->setQuery($query);

		return $db->loadObject();
	}

	/**
	 * This function gives plugin name from plugin parameter
	 *
	 * @param   string  $plgname  Plg name
	 *
	 * @return  plg name
	 */
	public function getPluginName($plgname)
	{
		$plugin = JPluginHelper::getPlugin('payment', $plgname);
		@$params = json_decode($plugin->params);

		return @$params->plugin_name;
	}

	/**
	 * Get administration fee form order id
	 *
	 * @param   string  $order_id  order id
	 *
	 * @return  fee
	 */
	public function getFee($order_id)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT fee FROM #__jg_orders
		WHERE order_id='" . $order_id . "'";
		$db->setQuery($query);

		return $result = $db->loadResult();
	}

	/**
	 * Get Sold Givebacks
	 *
	 * @param   INT     $order_id_key  Order id
	 * @param   string  $status        Order status
	 *
	 * @return  boolean
	 */
	public function getSoldGivebacks($order_id_key, $status = '')
	{
		// Get Donation id from order id
		$donationid = $this->getDonationIdFromOrderId($order_id_key);

		$db    = JFactory::getDBO();
		$query = "SELECT giveback_id
		FROM #__jg_donations
		where id=" . $donationid;
		$db->setQuery($query);
		$giveback_id = $db->loadResult();

		$query = "SELECT g.quantity as quantity
		FROM `#__jg_campaigns_givebacks` AS g
		WHERE g.id=" . $giveback_id;
		$db->setQuery($query);
		$quantity = $db->loadResult();

		$quantity++;

		$res           = new stdClass;
		$res->id       = $giveback_id;
		$res->quantity = $quantity;
		$db->updateObject('#__jg_campaigns_givebacks', $res, 'id');

		return true;
	}

	/**
	 * Get Donation Id From OrderId
	 *
	 * @param   INT  $order_id_key  Order id key
	 *
	 * @return  order id key
	 */
	public function getDonationIdFromOrderId($order_id_key)
	{
		$db    = JFactory::getDBO();
		$query = "SELECT `donation_id`
		FROM `#__jg_orders`
		where id=" . $order_id_key;
		$db->setQuery($query);

		return $donationid = $db->loadResult();
	}

	/**
	 * Check is anonymous donation
	 *
	 * @param   INT  $order_id_key  Order id key
	 *
	 * @return  1/0 Annonymous Yes/No
	 */
	public function isAnonymousDonation($order_id_key)
	{
		$db    = JFactory::getDBO();

		$query = "SELECT d.`annonymous_donation`
		FROM `#__jg_donations` as d
		LEFT JOIN `#__jg_orders` as o ON o.donation_id = d.id
		where o.id=" . $order_id_key;
		$db->setQuery($query);

		return $db->loadResult();
	}
}
