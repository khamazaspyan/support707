<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.user.helper');
jimport('joomla.utilities.arrayhelper');

/**
 * jgiveViewCampaign class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JgiveViewCampaign extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$mainframe            = JFactory::getApplication();
		$callback             = JFactory::getApplication()->input->get('callback', '');
		$this->campaignHelper = new campaignHelper;

		// Get logged in user id
		$user                = JFactory::getUser();
		$this->logged_userid = $user->id;

		// Get params
		$params                       = JComponentHelper::getParams('com_jgive');
		$this->currency_code          = $params->get('currency');
		$this->commission_fee         = $params->get('commission_fee');
		$this->send_payments_to_owner = $params->get('send_payments_to_owner');
		$this->default_country        = $params->get('default_country');
		$this->admin_approval         = $params->get('admin_approval');

		// Create is a default layout
		$layout = JFactory::getApplication()->input->get('layout', 'create');
		$this->setLayout($layout);

		// Create jgive helper object
		$jgiveFrontendHelper        = new jgiveFrontendHelper;
		$this->jomsocailToolbarHtml = $jgiveFrontendHelper->jomsocailToolbarHtml();

		if ($layout == 'create')
		{
			if (!$this->logged_userid)
			{
				$msg = JText::_('COM_JGIVE_LOGIN_MSG');
				$uri = $_SERVER["REQUEST_URI"];
				$url = base64_encode($uri);
				$mainframe->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url, false), $msg);
			}

			// Get country list options use helper function
			$countries               = $jgiveFrontendHelper->getCountries();
			$this->countries         = $countries;

			// Default task is save
			$this->task              = 'save';
			$cid                     = JRequest::getInt('cid', '');

			// Get all campaigns email id
			$this->allCampsitemid    = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
			$this->myCampaignsItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');
			$itemid                  = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');

			// If cid is passed task is - edit
			if ($cid)
			{
				$this->task = 'edit';
				$cdata['campaign'] = new Stdclass;

				$cdata       = $this->get('Campaign');
				$this->cdata = $cdata;

				// Only owner can edit campaign if not owner redirect to all campaigns
				if ($cdata['campaign']->creator_id != $this->logged_userid)
				{
					$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
					$link   = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&Itemid=' . $itemid, false);
					$msg    = JText::_('COM_JGIVE_CAMPAIGN_NO_EDIT_PERMISSIONS');
					$mainframe->redirect($link, $msg);
				}
			}
			else
			{
				$cdata['campaign'] = new Stdclass;

				$integration             = $params->get('integration');

				// Joomla profile import
				$profile_import          = $params->get('profile_import');

				// If profie import is on the call profile import function
				$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;

				if ($profile_import)
				{
					$cdata = $JgiveIntegrationsHelper->profileImport();
				}

				// Check is user profile completed to allow create campaign ?
				$profile_complete = $params->get('profile_complete');
				$profile_check    = array();

				if ($profile_complete)
				{
					$profile_check = $JgiveIntegrationsHelper->profileChecking();

					if (!empty($profile_check))
					{
						if ($integration == 'joomla')
						{
							$msg    = JText::_('COM_JGIVE_PROFILE_COMPLETE_MSG');
							$uri    = JFactory::getApplication()->input->get('REQUEST_URI', '', 'server', 'string');
							$url    = base64_encode($uri);
							$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_users&view=profile&layout=edit');
							$mainframe->redirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit&itemid=' . $itemid . '&return=' . $url), $msg);
						}
					}
				}

				$this->cdata = $cdata;
			}

			// Get js group for loggend in user
			$this->js_groups = $this->get('JS_usergroup');

			// Get the campaigns categeory
			$cats            = $this->get('CampaignsCats');
			$this->assignRef('cats', $cats);
		}

		// Show a single campaign
		if ($layout == 'single')
		{
			$this->allCampsitemid       = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
			$this->createCampaignItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=create');
			$this->singleCampaignItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');

			// Get campaign details
			$cdata       = $this->get('Campaign');
			$this->cdata = $cdata;

			// Do not show campaign if it is unpublished and redirect to all campaigns
			if (!$cdata['campaign']->published)
			{
				$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
				$link   = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&Itemid=' . $itemid, false);
				$msg    = JText::_('COM_JGIVE_CAMPAIGN_NOT_PUBLISHED');
				$mainframe->redirect($link, $msg);
			}

			// Breadcrumbs
			$app     = JFactory::getApplication();
			$pathway = $app->getPathway();
			$pathway->addItem($cdata['campaign']->title, '');

			// Function to get campaign main image, progress percentage, days left etc
			$cdata = $this->campaignHelper->mapData($this->cdata, $this->singleCampaignItemid);

			$mapped_data = Array();
			$mapped_data = $this->cdata;

			// Add component params
			$mapped_data['site_root_link']   = JUri::root();
			$mapped_data['com_jgive_params'] = $params;

			if (!count($mapped_data))
			{
				echo $callback ? $callback . '(' . json_encode(array()) . ')' : json_encode(array());
				jexit();
			}

			echo $callback ? $callback . '(' . json_encode($mapped_data) . ')' : json_encode($mapped_data);
			jexit();
		}

		parent::display($tpl);
	}
}
