<div class="row-fluid">
	<fieldset>

		<?php
			// Load images code
			echo $this->loadTemplate('images');
		?>

		<input type="hidden" name="option" value="com_jgive"/>
		<!--<input type="hidden" name="controller" value="campaign"/>-->
		<input type="hidden" name="task" value="campaign.<?php echo $this->task;?>"/>

		<?php
		//print_r($this->cdata);
			if(isset($this->cdata['campaign']->id))
			{
				?>
				<input type="hidden" name="cid" value="<?php echo $this->cdata['campaign']->id;?>"/>
				<input type="hidden" name="img_id" value="<?php echo $this->cdata['images'][0]->id;?>"/>
				<?php
			}
		?>

		<?php
		if(!$this->send_payments_to_owner && $this->commission_fee>0)
		{
			?>
			<div class="alert alert-info">
				<em><i>
					<?php
						echo JText::sprintf('COM_JGIVE_COMMISSION_FEE_NOTICE',$this->commission_fee.'%');
					?>
				</i></em>
			</div>
			<?php
		}
		if($this->admin_approval)
		{
			?>
			<br/>
			<div class="alert">
				<em><i>
					<?php
						echo JText::_('COM_JGIVE_ADMIN_APPROVAL_NOTICE');
					?>
				</i></em>
			</div>
		<?php
		}
		?>

		<?php

		if($this->params->get('terms_condition'))
		{
			$link='';
			if($this->params->get('camp_create_terms_article'))
				$link = JRoute::_(JUri::root()."index.php?option=com_content&view=article&id=".$this->params->get('camp_create_terms_article')."&tmpl=component" );
			if($link)
			{
			?>
			<div class="control-group">
				<div class="controls">
					<div class="terms_condition_div" >
						<a rel="{handler: 'iframe', size: {x: 600, y: 600}}" href="<?php echo $link; ?>" class="modal jgive-override-modal">
							<?php echo JText::_( 'COM_JGIVE_ACCEPT_TERMS' ); ?>
						</a>&nbsp;&nbsp;
						<input  type="checkbox"  id="terms_condition" size="30"/>&nbsp;&nbsp;<?php echo JText::_( 'COM_JGIVE_YES' ); ?>
					</div>
				</div>
			</div>
			<?php
			}
		}
		?>
	</fieldset>
</div>
