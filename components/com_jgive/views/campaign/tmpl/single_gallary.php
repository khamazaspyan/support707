<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive-tables.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/vendors/css/magnific-popup.css');

if (count($this->cdata['video']) > 0)
{
?>
	<div class="row jgive_search_filter">
		<div class="col-xs-12 col-sm-6 videosText">
			<h5><?php echo strtoupper(JText::_('COM_JGIVE_SINGLE_GALLERY_VIDEOS'));?></h5>
		</div>
		<?php
		if (count($this->cdata['video']) > 0 && count($this->cdata['images']) > 1)
		{
		?>
			<div class="col-xs-12 col-sm-6 gallary-filters">
				<select id="gallary_filter" class="pull-right">
					<option value="0"><?php echo JText::_('COM_JGIVE_CAMP_TYPE');?></option>
					<option value="1"><?php echo JText::_('COM_JGIVE_SINGLE_GALLERY_VIDEOS');?></option>
					<option value="2"><?php echo JText::_('COM_JGIVE_SINGLE_GALLERY_IMAGES');?></option>
				</select>
			</div>
		<?php
		}?>
	</div>
	<div id="videos">
		<?php
			echo $this->loadTemplate("video");
		?>
	</div>
<?php
}

if (count($this->cdata['images']) > 1)
{
?>
	<div id="images">
		<div class="row">
			<div class="col-xs-12 col-sm-12 imagesText">
				<h5><?php echo strtoupper(JText::_('COM_JGIVE_SINGLE_GALLERY_IMAGES'));?></h5>
			</div>
		</div>
		<?php
			echo $this->loadTemplate("image");
		?>
	</div>
<?php
}?>

<script type="text/javascript">
	jQuery(document).ready(function()
	{
		jgive.campaign.onChangefun();
	});
</script>


