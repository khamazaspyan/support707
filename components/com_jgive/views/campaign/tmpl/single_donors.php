<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
$jgiveFrontendHelper = new jgiveFrontendHelper;
$j = 1;
$donor_records_config = 5;

if (count($this->cdata['donors']) > 0)
{
?>
	<div class="col-xs-12 col-sm-4">
		<h5>
			<?php
				if ($this->cdata['campaign']->type == 'donation')
				{
					echo strtoupper(JText::_('COM_JGIVE_SINGLE_ALL_DONORS'));
				}
				elseif ($this->cdata['campaign']->type == 'investment')
				{
					echo strtoupper(JText::_('COM_JGIVE_SINGLE_ALL_INVESTORS'));
				}
			?>
		</h5>
	</div>
	<?php
	if ($this->cdata['orders_count'] > $donor_records_config)
	{
	?>
		<div class="col-xs-12 col-sm-8 ">
			<ul class="list-inline pull-right">
				<li>
					<a id="searchCampBtn" class="pull-right" href="#" onclick="jgive.campaign.searchCampDonors()"><i class="fa fa-search" ></i></a>
					<span class="pull-right searchDonor" id="SearchDonorsinputbox">
						<input type="text" id="donorInput" onkeyup="jgive.campaign.searchDonor()" placeholder="<?php echo JTEXT::_('COM_JGIVE_SINGLE_DONORS_SEARCH_PLACEHOLDER');?>" title="searchDonorRecord">
					</span>
				</li>
				<li>
					<select id="donorsFilterByAmount" class="pull-right">
						<option value="0"><?php echo JText::_('COM_JGIVE_SINGLE_DONORS_AMOUNT');?></option>
						<option value="1"><?php echo JText::_('COM_JGIVE_SINGLE_DONORS_HIGHEST_AMOUNT');?></option>
						<option value="2"><?php echo JText::_('COM_JGIVE_SINGLE_DONORS_LOWEST_AMOUNT');?></option>
					</select>
				</li>
			</ul>
		</div>
	<?php
	}?>

	<div class="row">
		<div class="col-sm-12 no-more-tables">
			<table class="table user-list" id="singlecampaignDonor">
				<thead>
					<tr>
						<th>
							<span>
							<?php
								if ($this->cdata['campaign']->type == 'donation')
								{
									echo JText::_("COM_JGIVE_DONORNAME");;
								}
								elseif ($this->cdata['campaign']->type == 'investment')
								{
									echo JText::_("COM_JGIVE_INVESTORNAME");;
								}

							?>
							</span>
						</th>
						<th><span><?php echo JText::_("COM_JGIVE_TOTAL");?></span></th>
						<th><span><?php echo JText::_("COM_JGIVE_SINGLE_DONORS_RECENT_DONATION");?></span></th>
						<th><span><?php echo JText::_("COM_JGIVE_SINGLE_DONORS_PAYMENT_MODE"); ?></span></th>
					</tr>
				</thead>
				<tbody id="jgive_donors_pic">

				<?php
					$j = 1;

					foreach ($this->cdata['donors'] as $this->donor)
					{
						echo $this->loadTemplate("donorslist");

						$j++;
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
<?php
}
else
{
	echo ($this->cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_NO_DONATIONS') : JText::_('COM_JGIVE_NO_INVESTMENTS');
	echo "<br/>";
	echo (($this->cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_BE_THE_FIRST_DONOR') : JText::_('COM_JGIVE_BE_THE_FIRST_INVESTOR'));
}
?>
<input type="hidden" id="donors_pro_pic_index" value="<?php echo $j; ?>" />
<input type="hidden" id="camp_id" name="camp_id" value="<?php echo $this->cdata['campaign']->id; ?>" />

<?php

if ($this->cdata['orders_count'] > $donor_records_config  && $this->cdata['campaign']->allow_view_donations)
{
?>
	<button id="btn_showMorePic" class="btn btn-info btn-md" type="button" onclick="jgive.campaign.viewMoreDonorProPic()">
		<?php
			echo JText::_('COM_JGIVE_SHOW_MORE_DONORS');
		?>
	</button>
<?php
} ?>

<script type="text/javascript">
	var gbl_jgive_index = 0 ;
	var orders_count = <?php
							if (!empty($this->cdata['orders_count']))
							{
								echo $this->cdata['orders_count'];
							}
							else
							{
								echo 0;
							}
							?>;

	var gbl_jgive_pro_pic = 0 ;
	var jgive_baseurl = "<?php echo JUri::root(); ?>";
</script>
