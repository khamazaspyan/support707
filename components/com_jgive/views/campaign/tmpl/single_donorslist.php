<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// Calling FrontendHelper
$jgiveFrontendHelper = new jgiveFrontendHelper;
?>
<tr>
	<td data-title="<?php echo JText::_("COM_JGIVE_DONORNAME");?>">
		<?php
			if($this->donor->annonymous_donation)
			{
				echo JText::_("COM_JGIVE_DONOR_ANNONYMOUS_NAME");
			}
			else
			{
				echo $this->donor->first_name . ' ' . $this->donor->last_name;
			}?>
	</td>
	<td data-title="<?php echo JText::_("COM_JGIVE_TOTAL"); ?>">
	<?php
		$diplay_amount = $jgiveFrontendHelper->getFormattedPrice($this->donor->amount);
		echo $diplay_amount;
	?>
	</td>
	<td data-title="<?php echo JText::_("COM_JGIVE_SINGLE_DONORS_RECENT_DONATION"); ?>">
		<?php echo $jgiveFrontendHelper->getFormattedPrice($this->donor->recentDonation); ?>
	</td>
	<td data-title="<?php echo JText::_("COM_JGIVE_SINGLE_DONORS_PAYMENT_MODE")?>">
		<?php echo ucfirst($this->donor->processor);?>
	</td>
</tr>
