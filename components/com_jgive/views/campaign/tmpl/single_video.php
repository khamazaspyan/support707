<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive-tables.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/vendors/css/magnific-popup.css');

// Get media table entries count
$count = count($this->cdata['video']);
?>
<div class="row">
	<div class="media" id="jgive_video_gallery">
		<?php
			$video_found = 0;

			if ($count > 0)
			{
				// Show videos thumbnails
				foreach ($this->cdata['video'] as $video)
				{
					$link = JRoute::_(
					JUri::root() . "index.php?option=com_jgive&view=campaign&layout=single_playvideo&vid=" .
					$video->id . "&type=" . trim($video->type) . "&tmpl=component"
					);

					switch ($video->type)
					{
						case 'video':
							$video_found = 1;
							$thumbSrc    = JUri::root() . $video->thumb_path;

							if (!file_exists(JPATH_SITE . $video->thumb_path))
							{
								$thumbSrc = JUri::root(true) . '/media/com_jgive/images/no_thumb.png';
							}
						break;

						case 'youtube' || 'vimeo':
							$video_found = 1;
							$videoId  = JgiveMediaHelper::videoId($video->type, $video->url);
							$thumbSrc = JgiveMediaHelper::videoThumbnail($video->type, $videoId);
						break;
					} ?>

					<div class="col-sm-3 jgive_gallery_image_item" >
						<a rel="{handler: 'iframe', size: {x: 600, y: 600}}" href="<?php echo $link; ?>" class="modal jgive-bs3-modal">

							<img src="<?php echo JUri::root(true) . '/media/com_jgive/images/play_icon.png'; ?>"
							class="play_icon"/>

							<img src="<?php echo $thumbSrc; ?>" width="100%"/>
						</a>
					</div>
					<?php
				}
			}

			if ($video_found == 0)
			{
				echo JText::_("COM_JGIVE_NO_VIDEO_REC_AVAILABLE");
			}
		?>
	</div>
</div>
