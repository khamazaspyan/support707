<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
// No direct access

defined('_JEXEC') or die;

if ($this->extraData)
{
	if (count($this->extraData))
	{
	?>
		<table class="table table-striped table-bordered table-hover">
			<?php
			foreach ($this->extraData as $f)
			{
			?>
				<tr>
					<td>
						<strong><?php echo $f->label;?></strong>
					</td>
					<td>
						<?php
						if (!is_array($f->value))
						{
						?>
							<?php echo $f->value; ?>
						<?php
						}
						else
						{
						?>
							<?php
							foreach ($f->value as $option)
							{
							?>
								<?php echo $option->options; ?>
								<br/>
							<?php
							}
							?>
						<?php
						}
						?>
					</td>
				</tr>
			<?php
			}
			?>
		</table>
	<?php
	}
}
