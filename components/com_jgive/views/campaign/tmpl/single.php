<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;
JHtml::_('behavior.modal', 'a.modal');

// Creating Object of FrontendHelper class
$jgiveFrontendHelper = new jgiveFrontendHelper;
$campaignHelper = new campaignHelper;

// Set Title by campaign name
$document = JFactory::getDocument();

// Load Chart Javascript Files.
$document->addScript(JUri::root(true) . '/media/com_jgive/vendors/js/Chart.js');
$document->addScript(JUri::root(true) . '/media/com_jgive/vendors/js/Chart.min.js');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive.css');

echo '<div id="fb-root"></div>';
	$fblike_tweet = JUri::root(true) . '/media/com_jgive/javascript/fblike.js';
echo "<script type='text/javascript' src='" . $fblike_tweet . "'></script>";

$browserbar_title = $this->cdata['campaign']->title;
$document->setTitle($browserbar_title);
?>

<div class="tjBs3">
	<!--Campaign Information-->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<?php
			if ($this->cdata['campaign']->creator_id == $this->cdata['otherData']->loggedUserId && $this->cdata['otherData']->canEdit)
			{
				$dashboardUrl = JUri::root() . substr(
				JRoute::_('index.php?option=com_jgive&view=dashboard&Itemid=' . $this->cdata['otherData']->dashboardCampItemid),
				strlen(JUri::base(true)) + 1
				);
			?>
				<a href="<?php echo $dashboardUrl;?>">
					<i class="fa fa-angle-left" aria-hidden="true"></i>
					<span><?php echo JText::_('COM_JGIVE_SINGLE_BACKE_TO_DASHBOARD');?></span>
				</a>
			<?php
			}
			else
			{
				$allCampUrl = JUri::root() . substr(
				JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&Itemid=' . $this->cdata['otherData']->allCampItemid),
				strlen(JUri::base(true)) + 1
				);
			?>
				<a href="<?php echo $allCampUrl;?>">
					<i class="fa fa-angle-left" aria-hidden="true"></i>
					<span><?php echo JText::_('COM_JGIVE_SINGLE_BACKE_TO_ALL_CAMP');?></span>
				</a>
			<?php
			}?>
			<h2><strong><?php echo $this->cdata['campaign']->title;?></strong></h2>
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="row singleCampaignMainImage">
			<?php
			if ($this->cdata['campaign']->creator_id == $this->cdata['otherData']->loggedUserId && $this->cdata['otherData']->canEdit)
			{
			?>
				<div class="col-xs-12 col-sm-6">
					<?php
						$goal_amount = $jgiveFrontendHelper->getFormattedPrice($this->cdata['campaign']->goal_amount);
						echo JText::_('COM_JGIVE_GOAL_AMOUNT') . ':'?><?php echo $goal_amount;
					?>
				</div>
				<div class="col-xs-12 col-sm-6">
					<select id='campaigns_graph_period' class="pull-right">
						<option value = '0'><?php echo JText::_('COM_JGIVE_FILTER_LATEST');?></option>
						<option value = '1'><?php echo JText::_('COM_JGIVE_FILTER_LAST_MONTH');?></option>
						<option value = '2'><?php echo JText::_('COM_JGIVE_FILTER_LAST_YEAR');?></option>
					</select>
				</div>
				<div class="col-xs-12 col-sm-12">
					<canvas id="mycampaign_graph"></canvas>
				</div>
			<?php
			}
			else
			{
			?>
				<div class="col-xs-12 camp-main-img">
					<?php
					if ($this->cdata['campaign']->video_on_details_page == 1 && (!empty($this->cdata['campaign']->videoPlgParams["file"]) || !empty($this->cdata['campaign']->videoPlgParams["videoId"])))
					{
						$this->videoPlgParams          = $this->cdata['campaign']->videoPlgParams;
						$this->videoPlgParams['divId'] = "jgiveMainPagevideo";

						$input = JFactory::getApplication()->input;
						$this->videoPlgParams['client'] = $input->get('option', '', 'STRING');
						$this->videoPlgParams['height'] = '380px';

						$dispatcher = JDispatcher::getInstance();
						JPluginHelper::importPlugin('tjvideo', $this->videoPlgParams['plugin']);

						$result = $dispatcher->trigger('renderPluginHTML', array($this->videoPlgParams));

						echo $result[0];
					}
					else
					{
						foreach ($this->cdata['images'] as $img)
						{
							if ($img->gallery == 0)
							{
								$path = 'images/jGive/';
								$fileParts = pathinfo($img->path);

								// If loop for old version compability (where img resize not available means no L , M ,S before image name)
								if (file_exists($path . $fileParts['basename']))
								{
									echo "<img class='img-responsive' src='" . JUri::base() . $path . $fileParts['basename'] . "'/>";
									break;
								}
								else
								{
									echo "<img class='img-responsive' style='' src='" . JUri::base() . $path . 'L_' . $fileParts['basename'] . "'/>";
									break;
								}
							}
						}
					}
					?>
				</div>
			<?php
			}
			?>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="row">
				<div class="col-xs-12">
				<?php
					if ($this->cdata['campaign']->creator_id == $this->cdata['otherData']->loggedUserId && $this->cdata['otherData']->canEdit)
					{
					?>
						<div class="col-xs-12 pull-right">
							<?php $url = JUri::root() . substr(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $this->cdata['campaign']->id . '&Itemid=' .
							$this->cdata['otherData']->createCampItemid), strlen(JUri::base(true)) + 1);?>
							<a id="donation-edit" class="pull-right" href="<?php echo $url?>">
								<i class="fa fa-pencil" aria-hidden="true"></i>
								<?php echo JText::_('COM_JGIVE_EDIT_CAMPAIGN_HEADER');?>
							</a>
						</div>
				<?php
					}
					else
					{
						if ($this->cdata['campaign']->donateBtnShowStatus == 0 || $this->cdata['campaign']->donateBtnShowStatus == 1)
						{
						?>
							<div class="row campaign-detail-donor">
								<div class="col-sm-4">
									<p>
										<h3><b><?php echo $this->cdata['campaign']->totalNoOfDonors;?></b></h3>
									</p>
									<p>
										<?php echo ($this->cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_NO_OF_DONORS') : JText::_('COM_JGIVE_INVESTORS');?>
									</p>
								</div>
								<div class="col-sm-8">
									<p><h3><b><?php echo $jgiveFrontendHelper->getFormattedPrice($this->cdata['campaign']->amount_received);?></b></h3></p>
									<p>
										<?php echo ($this->cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_SINGLE_DONATED_SO_FAR') : JText::_('COM_JGIVE_SINGLE_INVESTED_SO_FAR');?>
									</p>
								</div>
							</div>
							<div class="row campaign-detail-donor">
								<div class="col-sm-4">
									<p><h3><b><?php echo $this->cdata['campaign']->goneDays;?></b></h3></p>
									<p><?php echo JText::_('COM_JGIVE_SINGLE_DAYS_TO_GO');?></p>
								</div>
								<div class="col-sm-8">
									<p><h3><b><?php echo $jgiveFrontendHelper->getFormattedPrice($this->cdata['campaign']->goal_amount);?></b></h3></p>
									<p><?php echo JText::_('COM_JGIVE_GOAL_AMOUNT_PH');?></p>
								</div>
							</div>
					<?php
						}?>
				<?php
					}
				?>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-3 col-sm-3">
					<?php
						$btn_block = '';
						$class = '';
						$close_btn_style = '';

						if ($this->cdata['campaign']->donateBtnShowStatus == 0)
						{
						?>
							<input type="button"
							class="btn btn-default btn-md disabled <?php echo $btn_block; ?>"
							style="<?php echo $close_btn_style; ?>"
							value="<?php echo (($this->cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_DONATIONS_CLOSED') : JText::_('COM_JGIVE_INVESTMENTS_CLOSED'));?>"
							/>
						<?php
						}
						elseif ($this->cdata['campaign']->donateBtnShowStatus == -1)
						{
						?>
							<input type="button"
							class="btn btn-default btn-md disabled <?php echo $btn_block; ?>"
							style="<?php echo $close_btn_style; ?>"
							value="<?php echo JText::_("COM_JGIVE_WILL_START_SOON"); ?>"/>
						<?php
						}
						elseif($this->cdata['campaign']->donateBtnShowStatus == 1)
						{
						?>
							<!--Working Campaign -->
							<form action="" method="post" name="donationform" id="donationform">
								<input type="hidden" name="cid" id="cid" value="<?php echo $this->cdata['campaign']->id;?>">

								<button type="submit" class="btn btn-primary btn-md" id="donate-now"
									title="<?php echo ($this->cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_BUTTON_DONATE_TOOLTIP') : JText::_('COM_JGIVE_BUTTON_INVEST_TOOPTIP');?>"><i class="fa fa-usd" aria-hidden="true"></i>
									<?php echo strtoupper(($this->cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_BUTTON_DONATE') : JText::_('COM_JGIVE_BUTTON_INVEST'));?>
								</button>
								<input type="hidden" name="option" value="com_jgive">
								<input type="hidden" name="task" value="donations.donate">
							</form>
						<?php
						}
					?>
				</div>

				<?php
				// Generate unique ad url for social sharing
				require_once JPATH_SITE . "/components/com_jgive/helpers/integrations.php";

				$campaign_link = 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $this->cdata['campaign']->id;
				$campaign_link = JUri::root() . substr(JRoute::_($campaign_link), strlen(JUri::base(true)) + 1);
				$add_this_share = '';
				$pid = $this->cdata['params']['addthis_publishid'];

				if (file_exists(JPATH_SITE . '/' . 'components/com_jlike/helper.php') || JFile::exists(JPATH_ROOT . '/components/com_invitex/invitex.php'))
				{
					// Check for component
					if (JFile::exists(JPATH_ROOT . '/components/com_invitex/invitex.php'))
					{
						if (JComponentHelper::isEnabled('com_invitex', true))
						{
							if (JPluginHelper::isEnabled('system', 'jgive_invitex_email'))
							{
								$helperPath = JPATH_SITE . '/components/com_invitex/helper.php';

								if (!class_exists('cominvitexHelper'))
								{
									JLoader::register('cominvitexHelper', $helperPath);
									JLoader::load('cominvitexHelper');
								}

								$cominvitexHelper = new cominvitexHelper;
								$invite_type      = $cominvitexHelper->geTypeId_By_InernalName('jgive_email');

								$invite_url = 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $this->cdata['campaign']->id . '&Itemid=' .
								$this->cdata['otherData']->allCampItemid;

								$invite_url = JUri::root() . substr(JRoute::_($invite_url), strlen(JUri::base(true)) + 1);
								$invite_url = urlencode(base64_encode($invite_url));

								$link = JUri::root(true) . '/index.php?option=com_invitex&view=invites&catch_act=&invite_type=' .
								$invite_type . '&invite_url=' . $invite_url . '&invite_anywhere=1&tag=[name=' . $this->cdata['campaign']->title . '|cid=' .
								$this->cdata['campaign']->id . ']';

								$linkAfterRoute = JUri::root() . substr(JRoute::_($link), strlen(JUri::base(true)) + 1);?>

								<div class="col-xs-3 col-sm-3">
									<a type="button" class="button subbutton btn btn-primary btn-md" href ="<?php echo $linkAfterRoute; ?>" target="_self" rel="" name="invite_anywhere">
										<i class="fa fa-envelope"></i>
										<?php echo JText::_('COM_JGIVE_INVITE_PEOPLE_TO_DONATE'); ?>
									</a>
								</div>
								<?php
							}
						}
					}

					if (file_exists(JPATH_SITE . '/' . 'components/com_jlike/helper.php'))
					{
						$show_comments = -1;
						$show_like_buttons = 1;

						$jlikehtml = $jgiveFrontendHelper->DisplayjlikeButton(
						$campaign_link, $this->cdata['campaign']->id, $this->cdata['campaign']->title, $show_comments, $show_like_buttons
						);

						if ($jlikehtml)
						{
						?>
							<div class="col-xs-4 col-sm-6">
								<?php echo $jlikehtml;?>
							</div>
						<?php
						}
					}
				}
				?>
			</div>
			<?php
			if ($this->cdata['campaign']->creator_id == $this->cdata['otherData']->loggedUserId && $this->cdata['otherData']->canEdit)
			{
			?>
				<h5 class="text-uppercase">
					<?php
					$type = ($this->cdata['campaign']->type == 'donation') ? strtoupper(JText::_('COM_JGIVE_CAMPAIGN_TYPE_DONATION')) : strtoupper(JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT'));
					echo '<b>' . strtoupper(JTEXT::_("COM_JGIVE_CAMP_TYPE")) . ": " . $type . '</b>' . " | ";

					if ($this->cdata['campaign']->donateBtnShowStatus == 0)
					{
						echo JTEXT::_("COM_JGIVE_DONATIONS_CLOSED");
					}
					elseif ($this->cdata['campaign']->donateBtnShowStatus == -1)
					{
						echo JTEXT::_("COM_JGIVE_WILL_START_SOON");
					}
					elseif ($this->cdata['campaign']->donateBtnShowStatus == 1)
					{
						echo $this->cdata['campaign']->goneDays . " " . JTEXT::_("COM_JGIVE_SINGLE_DAYS_TO_GO");
					}
					?>
				</h5>
			<?php
			}
			else
			{
			?>
				<h5 class="text-uppercase">
					<?php
						if ($this->cdata['campaign']->org_ind_type == 'non_profit')
						{
							$org_ind_type = JText::_('COM_JGIVE_ORG_NON_PROFIT');
						}
						elseif ($this->cdata['campaign']->org_ind_type == 'self_help')
						{
							$org_ind_type = JText::_('COM_JGIVE_SELF_HELP');
						}
						else
						{
							$org_ind_type = JText::_('COM_JGIVE_SELF_INDIVIDUALS');
						}

						$type = ($this->cdata['campaign']->type == 'donation') ? strtoupper(JText::_('COM_JGIVE_CAMPAIGN_TYPE_DONATION')) : strtoupper(JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT'));
					?>
					<?php echo '<b>' . $type . '</b>' . ' ';?> |
					<?php echo ' ' . '<b>' . strtoupper($this->cdata['campaign']->catname) . '</b>' . ' '; ?>|
					<?php echo ' ' . '<b>' . strtoupper($org_ind_type) . '</b>' . ' '; ?>
				</h5>
			<?php
			}
			?>
			<!--Short and Long description display here-->
			<p class="text-justify text-muted">
				<?php
					$long_desc_char = $this->cdata['params']['pin_short_desc_char'] ? $this->cdata['params']['pin_short_desc_char']:100;

					echo strip_tags($this->cdata['campaign']->short_description) . '<br>';

					if (strlen($this->cdata['campaign']->long_description) > $long_desc_char)
					{
						echo substr(strip_tags($this->cdata['campaign']->long_description), 0, $long_desc_char);?>
						<a href="#myModal" data-toggle="modal" data-target="#myModal">...Read more</a>
					<?php
					}
					else
					{
						echo strip_tags($this->cdata['campaign']->long_description);
					}
				?>
			</p>
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->cdata['campaign']->long_description;?></p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>

			<!--Promoter Details-->
			<h5 class="text-uppercase"><strong><?php echo strtoupper(JText::_('COM_JGIVE_CAMPAIGN_PROMOTER'));?></strong></h5>
			<div class="row">
				<?php
				$fields_to_hide_details_view = $this->cdata['params']['fields_to_hide_details_view'];
				$hide_email = 0;
				$hide_phone_number = 0;

				if (isset($fields_to_hide_details_view))
				{
					foreach ($fields_to_hide_details_view as $field)
					{
						switch ($field)
						{
							case 'hide_email':
								$hide_email = 1;
							break;

							case 'hide_phone_number':
								$hide_phone_number = 1;
							break;
						}
					}
				}
				?>
				<?php
				if ($this->cdata['campaign']->creator_avatar)
				{
					$promoterAvatar = $this->cdata['campaign']->creator_avatar;
				}
				else
				{
					// If no avatar, use default avatar
					$promoterAvatar = JUri::root(true) . '/media/com_jgive/images/default_avatar.png';
				}
				?>
				<div class="col-xs-1 col-sm-2">
					<img src="<?php echo $promoterAvatar; ?>" class="img-circle" alt="<?php echo JText::_("COM_JGIVE_PROMOTOR_AVATAR")?>" width="60%">
				</div>
				<div class="col-xs-10 col-sm-4 text-muted">
					<?php 
						echo $this->cdata['campaign']->first_name . ' ' . $this->cdata['campaign']->last_name . '<br>';

						if ($hide_email == 0 )
						{
							$userinfo = JFactory::getUser($this->cdata['campaign']->creator_id);
							echo $userinfo->email . '<br>';
						}

						if ($this->cdata['campaign']->creator_profile_url)
						{
							$profile_link = $this->cdata['campaign']->creator_profile_url;
					?>
							<a href="<?php echo $profile_link;?>">
								<?php echo ucfirst(JText::_('COM_JGIVE_SINGLE_PROMOTER_DETAILS'));?>
							</a>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
	<!--Campaign Information end-->
	<hr>
	<?php

	if ($this->cdata['params']['social_sharing'])
	{
	?>
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<?php
				$doc = JFactory::getDocument();

				foreach ($this->cdata['images'] as $img)
				{
					break;
				}

				$path = 'images/jGive/';

				// Get original image name to find it resize images (S,M,L)
				$org_file_after_removing_path = trim(str_replace($path, '', $img->path));

				// Set metadata
				$config = JFactory::getConfig();
				$site_name = $config->get('sitename');

				$doc->addCustomTag('<meta property="og:title" content="' . $this->cdata['campaign']->title . '" />');
				$doc->addCustomTag('<meta property="og:image" content="' . JUri::base() . $path . 'L_' . $org_file_after_removing_path . '" />');
				$doc->addCustomTag('<meta property="og:url" content="' . $campaign_link . '" />');
				$doc->addCustomTag('<meta property="og:description" content="' . nl2br($this->cdata['campaign']->short_description) . '" />');
				$doc->addCustomTag('<meta property="og:site_name" content="' . $site_name . '" />');
				$doc->addCustomTag('<meta property="og:type" content="article" />');

				if ($this->cdata['params']['social_shring_type'] == 'addthis')
				{
					$add_this_share = '
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style">

					<a class="addthis_button_facebook_like" fb:like:layout="button_count" class="addthis_button" addthis:url="' . $campaign_link . '"></a>

					<a class="addthis_button_google_plusone" g:plusone:size="medium" class="addthis_button" addthis:url="' . $campaign_link . '"></a>

					<a class="addthis_button_tweet" class="addthis_button" addthis:url="' . $campaign_link . '"></a>

					<a class="addthis_button_pinterest_pinit" class="addthis_button" addthis:url="' . $campaign_link . '"></a>

					<a class="addthis_counter addthis_pill_style" class="addthis_button" addthis:url="' . $campaign_link . '"></a>
					</div>

					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid="' . $pid . '"></script>
					<!-- AddThis Button END -->';

					$add_this_js = 'https://s7.addthis.com/js/300/addthis_widget.js';
					$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
					$JgiveIntegrationsHelper->loadScriptOnce($add_this_js);

					// Output all social sharing buttons
					echo' <div id="rr" style=""><div class="social_share_container"><div class="social_share_container_inner">' . $add_this_share . '</div></div></div>';
				}
				else
				{
					echo '<div class="com_jgive_horizontal_social_buttons">';
					echo '<div class="com_jgive_float_left">
							<div class="fb-like" data-href="' . $campaign_link . '" data-send="true" data-layout="button_count" data-width="450" data-show-faces="true">
							</div>
						</div>';
					echo '

						<div class="com_jgive_float_left">
								&nbsp; <div class="g-plus" data-action="share" data-annotation="bubble" data-href="' . $campaign_link . '">
									</div>
						</div>';
					echo '<div class="com_jgive_float_left">
							&nbsp; <a href="https://twitter.com/share" class="twitter-share-button"  data-url="' . $campaign_link . '" data-counturl="' . $campaign_link . '">Tweet</a>
						</div>';
					echo '</div>
						<div class="clearfix"></div>';
				}
				?>
			</div>
		</div>
		<hr>
	<?php
	}?>
	<!--Tab -->
	<div class="row">
		<div class="col-xs-12">
			<ul id="myTab" class="nav nav-tabs text-uppercase">
				<li class="active">
					<a data-toggle="tab" href="#camp_activity">
						<?php echo strtoupper(JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_ACTIVITY'));?>
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#camp_donors">
						<?php echo ($this->cdata['campaign']->type == 'donation') ? strtoupper(JText::_('COM_JGIVE_DONORS')) : strtoupper(JText::_('COM_JGIVE_INVESTORS'));?>
					</a>
				</li>

				<?php
				if (count($this->cdata['images']) > 1 OR count($this->cdata['video']) > 0)
				{
				?>
					<li>
						<a data-toggle="tab" href="#camp_gallery">
							<?php echo strtoupper(JText::_('COM_JGIVE_SINGLE_GALLERY'));?>
						</a>
					</li>
				<?php
				}?>
				<?php
				if (count($this->extraData))
				{
				?>
					<li>
						<a data-toggle="tab" href="#additional_info">
							<?php echo strtoupper(JText::_('COM_JGIVE_EXTA_FIELDS'));?>
						</a>
					</li>
				<?php
				}
				?>
			</ul>
		</div>

		<div class="col-xs-12">
			<!--Tab content started here-->
			<div class="tab-content">
				<!---Tab-Activity---->
				<div id="camp_activity" class="tab-pane active">
					<?php
						echo $this->loadTemplate("activity");
					?>
				</div>

				<!---Tab-DONORS---->
				<div id="camp_donors" class="tab-pane">
					<?php
					if ($this->cdata['otherData']->loggedUserId == $this->cdata['campaign']->creator_id || $this->cdata['campaign']->allow_view_donations)
					{
						echo $this->loadTemplate("donors");
					}
					else
					{
						echo JText::_('COM_JGIVE_DONATIONS_ACCESS_LOCKED');
					}
					?>
				</div>

				<!---Tab-GALLERY---->
				<div id="camp_gallery" class="tab-pane">
					<?php
						echo $this->loadTemplate("gallary");
					?>
				</div>

				<!--Additional Information-->
				<div id="additional_info" class="tab-pane">
					<?php
						echo $this->loadTemplate("extrafields");
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="camp_id" name="camp_id" value="<?php echo $this->cdata['campaign']->id; ?>" />
<script type="text/javascript">
	jQuery(document).ready(function()
	{
		jgive.campaign.onChangeGetDonationData();
	});
</script>
