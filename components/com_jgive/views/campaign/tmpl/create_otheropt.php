<?php

	if ($this->admin_approval)
	{
		?>
		<br/>
		<div class="alert">
			<em><i>
				<?php
					echo JText::_('COM_JGIVE_ADMIN_APPROVAL_NOTICE');
				?>
			</i></em>
		</div>
	<?php
	}

	if ($this->params->get('terms_condition'))
	{
		$link = '';

		if ($this->params->get('camp_create_terms_article'))
		{
			$link = JRoute::_(JUri::root()."index.php?option=com_content&view=article&id=".$this->params->get('camp_create_terms_article')."&tmpl=component" );
		}

		if ($link)
		{
		?>
			<div class="form-group">
				<label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label">
						<a rel="{handler: 'iframe', size: {x: 600, y: 600}}" href="<?php echo $link; ?>" class="modal jgive-bs3-modal">
							<?php echo JHtml::tooltip(JText::_( 'COM_JGIVE_PUBLISH_TOOLTIP' ), JText::_( 'COM_JGIVE_ACCEPT_TERMS' ), '', JText::_( 'COM_JGIVE_ACCEPT_TERMS' )); ?>
						</a>
				</label>
				<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
					<div class="input-group">
						<input  type="checkbox"  id="terms_condition" size="30"/>&nbsp;<?php echo JText::_( 'COM_JGIVE_YES' ); ?>
					</div>
				</div>
			</div>
		<?php
		}
	}

