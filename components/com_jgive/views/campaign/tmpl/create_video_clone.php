<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

?>
<div class="new-video col-lg-12 col-md-12 col-sm-12 col-xs-12" name="<?php echo JText::_("COM_JGIVE_ADD_NEW_VIDE"); ?>">
	<div id="jgive_mediaContainer" class="jgive_mediaContainer row jgive_disply_none">
		<div class="col-lg-10 col-md-10 col-sm-9 col-xs-8">

			<div class="form-group">
				<label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 control-label" for="upload_options" title="<?php echo JText::_('COM_JGIVE_UPLOAD_OPTION_TOOLTIP');?>">
					<?php echo JHtml::tooltip(JText::_('COM_JGIVE_UPLOAD_OPTION_TOOLTIP'), JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION'), '', JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION'));?>
				</label>

				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<select id="upload_options" class="form-control" name="upload_options[]" onchange="videoUploadOption(this);" aria-invalid="false">
						<option value="url"><?php echo JText::_("COM_JGIVE_ENTER_VIDEO_URL"); ?></option>
						<option value="video" selected="selected"><?php echo JText::_("COM_JGIVE_ENTER_VIDEO_UPLOAD") ?></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-right">
					<div id="upload_file" class="fileupload fileupload-new" data-provides="fileupload">
						<input class="jgiveMediaFileUploadEle" class="form-control" id="jgiveMediaFile" type="file" name="jgiveMediaFile" />
						<span class="fileupload-preview text-info"><?php echo str_ireplace("'","",$this->extArrString); ?></span>

						<div class="progress progress-bar-striped">
							<div class="progress-bar progress-bar-success"
								id="jgive_progress-bar"
								role="progressbar"
								aria-valuenow="0"
								aria-valuemin="0"
								aria-valuemax="0"
								style="width: 0%;">
							</div>
						</div>
					</div>

					<!-- Enter video URL -->
					<input id="video_url"
						type="text"
						name="video_urls[]"
						class="jgive_disply_none form-control"
						placeholder="<?php echo JText::_('COM_JGIVE_ENTER_VIDEO_URL');?>"
						value=""/>
				</div>
			</div>

			<input type="hidden" name="existing_imgs_ids[]" value="0" />
			<input type="hidden" id="ajax_upload_hidden" name="video_files_name[]" value="" />
			<input type="hidden" name="video_ids[]" value="" />
			<!-- file upload END -->
		</div>

		<div id="action_btn" class="col-lg-2 col-md-2 col-sm-3 col-xs-4" style="padding-right:10px">
			<input type="hidden" name="default_marked[]"  value="0" />

			<label class="radio inline btn btn-sm"
				title="<?php echo JText::_("COM_JGIVE_SET_THIS_VIDEO_AS_DEFAULT"); ?>"
				for="radio">
					<i class="fa fa-star-o fa-lg"></i>
			</label>

			<input class="jgive_display_none"
				id="radio"
				type="radio"
				name="marked_as_default"
				value="0"
				onclick="setDefault(this)" />
		</div>

	</div>

	<?php
		if($this->video_container <= $this->maxVideoFilesLimit)
		{ ?>
			<div id="jgive_mediaContainer0" class="jgive_mediaContainer row">
				<div class="col-lg-10 col-md-10 col-sm-9 col-xs-8">
					<div class="form-group">
						<label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 control-label" for="upload_options">
							<?php echo JHtml::tooltip(JText::_('COM_JGIVE_UPLOAD_OPTION_TOOLTIP'), JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION'), '', JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION'));?>
						</label>

						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<select id="upload_options0"
								name="upload_options[]"
								class="form-control"
								onchange="videoUploadOption(this);" aria-invalid="false">
								<option value="url"><?php echo JText::_("COM_JGIVE_ENTER_VIDEO_URL"); ?></option>
								<option value="video" selected="selected"><?php echo JText::_("COM_JGIVE_ENTER_VIDEO_UPLOAD") ?></option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-right">
							<div id="upload_file0" class="fileupload fileupload-new" data-provides="fileupload">
								<input class="jgiveMediaFileUploadEle" class="form-control" id="jgiveMediaFile0" type="file" name="jgiveMediaFile" >

								<span class="fileupload-preview text-info"><?php echo str_ireplace("'","",$this->extArrString); ?></span>


								<div class="progress progress-bar-striped">
									<div class="progress-bar progress-bar-success"
										id="jgive_progress-bar0"
										role="progressbar"
										aria-valuenow="0"
										aria-valuemin="0"
										aria-valuemax=""
										style="width: 0%;">
									</div>
								</div>
							</div>

							<!-- Enter video URL -->
							<input id="video_url0"
								type="text"
								name="video_urls[]"
								class="jgive_disply_none form-control"
								placeholder="<?php echo JText::_('COM_JGIVE_ENTER_VIDEO_URL');?>"
								value=""/>
						</div>
					</div>

				</div>

				<div id="action_btn0" class="col-lg-2 col-md-2 col-sm-3 col-xs-4" style="padding-right:10px">
					<input type="hidden" name="default_marked[]"  value="0" />

					<label class="radio inline btn btn-sm"
					title="<?php echo JText::_("COM_JGIVE_SET_THIS_VIDEO_AS_DEFAULT"); ?>"
						for="radio0">
							<i class="fa fa-star-o fa-lg"></i>
					</label>
					<input id="radio0" class="jgive_display_none" type="radio" name="marked_as_default"  value="0" onclick="setDefault(this)" />
				</div>

				<input type="hidden" name="existing_imgs_ids[]" value="0" />
				<input type="hidden" id="ajax_upload_hidden0" name="video_files_name[]" value="" />
				<input type="hidden" name="video_ids[]" value="" />
				<!-- file upload END -->

			</div>

		<?php
		} ?>
	<button class="btn btn-primary pull-right"
		type="button"
		id='add'
		onclick="addCloneMedia('jgive_mediaContainer','jgive_mediaContainer');" >
			<i class="fa fa-plus"></i>
			<?php echo JText::_('COM_JGIVE_ADD_MORE'); ?>
	</button>
</div>
