<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/vendors/css/magnific-popup.css');

if ($this->cdata['params']['img_gallery'])
{
?>
	<div class="row">
		<div class="media" id="jgive_image_gallery">
			<div class="popup-gallery">
			<?php
				$img_path_array = array();
				$i = 0;
				$gallery_items = 0;

				foreach ($this->cdata['images'] as $img)
				{
					$img_path_array[] = JUri::base() . $img->path;

					if ($img->gallery == 1 & $img->isvideo == 0)
					{
						$gallery_items = 1;
						?>
							<div class="col-xs-6 col-sm-3 jgive_image_item">
								<a href="<?php echo JUri::base() . $img->path; ?>" title="" class="" >
								<div class="jgive-image-gallery-inner" style="background-size:contain;background-repeat:no-repeat; background-position:center center;background-image: url('<?php echo JUri::base() . $img->path; ?>');">
								</div>
								</a>
							</div>
						<?php
						$i++;
					}
				}

				if ($gallery_items == 0)
				{
					echo JText::_("COM_JGIVE_NO_IMAGES_FOUND");
				}
			?>
			</div>
		</div>
	</div>
<?php
}
?>
<script type="text/javascript">
	jQuery(document).ready(function()
	{
		jgive.campaign.campaignImgPopup();
	});
</script>

