<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

/**
 * @TO DO, get data into media array instead of images
 * $k = 0;
 */

$count = 0;

if (!empty($this->cdata['video']))
{
	$count = count($this->cdata['video']);
}


$no_video_found = 0;

// Video thumbnail
if (!empty($this->cdata['campaign']->id) )
{ ?>
	<div class="existing-videos-files-urls" name="<?php echo JText::_("COM_JGIVE_EXISTING_VIDEOS"); ?>" >
		<div>

			<?php
			if ($count > 0)
			{
				for ($j = 1; $j <= $count; $j++)
				{
					$video = $this->cdata['video'][$j-1];

					switch($video->type)
					{
						case 'video':
							$no_video_found = 1; ?>

							<div id="videoid<?php echo $this->video_container; ?>" class="jgive_container_border">
								<div class="row">
									<?php
									// Code to display thumbanil of existing video -Added by Nidhi
									if(($video->id)  && ($video->type))
									{
										if ($video->thumb_path)
										{
											//  Show thumbnail
											$thumbSrc = JUri::root().$video->thumb_path;
											$fileInfo = new SplFileInfo($video->path);

											if (!file_exists(JPATH_SITE . $video->thumb_path))
											{
												$thumbSrc = JUri::root(true) . '/media/com_jgive/images/no_thumb.png';
											}

											$link = JRoute::_(JUri::root() . "index.php?option=com_jgive&view=campaign&layout=single_playvideo&vid=" . $video->id . "&type=" .trim($video->type)."&tmpl=component"); ?>

											<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
												<a rel="{handler: 'iframe', size: {x: 600, y: 600}}"
													href="<?php echo $link; ?>"
													data-toggle="modal">
														<img src="<?php echo $thumbSrc; ?>"
															data-src="holder.js/300x200"
															class="media-object com_jgive_img_96_96 thumbnail"
															title="<?php echo substr(basename($video->path), 0, 15) . '...' . $fileInfo->getExtension(); ?>" />
												</a>
											</div>

										<?php
										} ?>

										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">

											<label class="radio inline btn btn-sm"
											title="<?php echo JText::_("COM_JGIVE_SET_THIS_VIDEO_AS_DEFAULT"); ?>"
												for="radio<?php echo $this->video_container; ?>">
												<?php
												if ($video->default == 0)
												{
													?>
													<i class="fa fa-star-o fa-lg"></i>
													<?php
												}
												else
												{
													?>
													<img src="<?php echo JUri::root(true); ?>/media/com_jgive/images/featured.png" width="13" height="13" border="0" >
												<?php
												} ?>
											</label>

											<input class="jgive_display_none"
												type="radio"
												id="radio<?php echo $this->video_container; ?>"
												name="marked_as_default"
												<?php echo $video->default == 1 ? 'checked="checked"': ''; ?>
												onclick="setDefault(this,'<?php echo $video->id; ?>','<?php echo $this->cdata['campaign']->id; ?>','video')" />

											<button class="btn btn-sm btn-danger"
												type="button"
												id="removeClone<?php echo $this->video_container; ?>"
												onclick="deleteVideo('<?php echo $video->type; ?>','<?php echo $video->id; ?>','videoid<?php echo $this->video_container; ?>');"
												title="<?php echo JText::_('COM_JGIVE_DELETE_VIDEO'); ?>">
												<i class="fa fa-trash-o"></i>
											</button>

											<input type="hidden" name="existing_imgs_ids[]" value="<?php echo $video->id; ?>" />
											<input type="hidden" name="upload_options[]" value="video" />

											<input type="hidden" name="default_marked[]" value="<?php echo $video->default; ?>" />

											<!-- Enter video URL -->
											<input type="hidden" name="video_urls[]" value="" />
											<input type="hidden" name="video_files_name[]" value="" />
											<input type="hidden" name="video_ids[]" value="<?php echo $video->id; ?>" />

										</div>
										<?php
									} ?>

								</div>
							</div>
								<?php
							$this->video_container++;
						break;

						case 'youtube' || 'vimeo':
							$no_video_found = 1; ?>

							<div  id="jgive_mediaContainer<?php echo $this->video_container; ?>" class="jgive_mediaContainer jgive_container_border">
								<div class="row">
									<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
										<div class="form-group">
											<label class="col-lg-3 col-md-3 col-sm-3 col-xs-12 control-label"
												for="upload_options">
													<?php echo JHtml::tooltip(JText::_('COM_JGIVE_UPLOAD_OPTION_TOOLTIP'), JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION'), '', JText::_('COM_JGIVE_SELECT_UPLOAD_OPTION'));?>
											</label>

											<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
												<select id="upload_options<?php echo $this->video_container; ?>"
													name="upload_options[]"
													onchange="videoUploadOption(this, <?php echo $this->video_container; ?>);"
													aria-invalid="false"
													class="form-control" >
														<option value="url" selected="selected" >
																<?php echo JText::_("COM_JGIVE_ENTER_VIDEO_URL"); ?>
														</option>
														<option value="video" >
															<?php echo JText::_("COM_JGIVE_ENTER_VIDEO_UPLOAD") ?>
														</option>
												</select>

											</div>
										</div>

										<div class="form-group">
											<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-right">
												<!--Choose video -->
												<div id="upload_file<?php echo $this->video_container; ?>"
													class="fileupload fileupload-new pull-left  jgive_disply_none"
													data-provides="fileupload" >

													<input class="jgiveMediaFileUploadEle form-control"
														id="jgiveMediaFile<?php echo $this->video_container; ?>"
														type="file"
														class="form-control"
														name="jgiveMediaFile" />

													<span class="fileupload-preview text-info"><?php echo str_ireplace("'", "", $this->extArrString); ?></span>

													<div class="progress progress-bar-striped">
														<div class="progress-bar progress-bar-success"
															id="jgive_progress-bar<?php echo $this->video_container; ?>"
															role="progressbar"
															aria-valuenow="0"
															aria-valuemin="0"
															aria-valuemax=""
															style="width: 0%; ">
													  </div>
													</div>
												</div>

												<!-- Enter video URL -->
												<input id="video_url<?php echo $this->video_container; ?>"
													type="text"
													name="video_urls[]"
													class="form-control <?php echo $video->type === 'video' ? 'jgive_disply_none' : ""; ?>"
													placeholder="<?php echo JText::_('COM_JGIVE_ENTER_VIDEO_URL');?>"
													value="<?php echo $video->url; ?>" />

													<?php
													// Show video preview link
													$prew_link = JRoute::_(JUri::root(true) . "/index.php?option=com_jgive&view=campaign&layout=single_playvideo&vid=" . $video->id . "&type= ". trim($video->type) . "&tmpl=component");
													?>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
										<label class="radio inline btn btn-sm"
											for="radio<?php echo $this->video_container; ?>"
											title="<?php echo JText::_("COM_JGIVE_SET_THIS_VIDEO_AS_DEFAULT"); ?>">
												<?php
												if ($video->default == 0)
												{
													?>
													<i class="fa fa-star-o fa-lg"></i>
													<?php
												}
												else
												{
													?>
													<img src="<?php echo JUri::root(true); ?>/media/com_jgive/images/featured.png" width="13" height="13" border="0" >
													<?php
												} ?>
										</label>

										<input class="jgive_display_none" id="radio<?php echo $this->video_container; ?>" type="radio" name="marked_as_default" <?php echo $video->default == 1 ? 'checked="checked"': ''; ?> onclick="setDefault(this,'<?php echo $video->id; ?>','<?php echo $this->cdata['campaign']->id; ?>','url', '<?php echo $this->video_container; ?>')" />

										<button class="btn btn-sm btn-danger" type="button" id="removeClone<?php echo $this->video_container; ?>" onclick="deleteVideo('url',<?php echo $video->id; ?>, 'jgive_mediaContainer<?php echo $this->video_container; ?>');" title="<?php echo JText::_('COM_JGIVE_DELETE_VIDEO'); ?>">
												<i class="fa fa-trash-o"></i>
										</button>


									<input type="hidden"
										id="ajax_upload_hidden<?php echo $this->video_container; ?>"
										name="video_files_name[]"
										value="" />

									<input type="hidden"
										name="existing_imgs_ids[]"
										value="<?php echo $video->id; ?>" />

									<input type="hidden"
										name="default_marked[]"
										value="<?php echo $video->default; ?>" />

									<input type="hidden"
										name="video_ids[]"
										value="<?php echo $video->id; ?>" />
								</div>
								</div>
							</div>
								<?php
							$this->video_container++;

						break;
					}

				}
			}?>
		</div>


		<?php

		if($no_video_found == 0)
		{
			echo JText::_("COM_JGIVE_NO_VIDEO_UPLOADED");
		}
		?>
	</div>
<?php

}
