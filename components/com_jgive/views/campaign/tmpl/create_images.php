<div class="form-group">
	<label class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label"
		for="camp_image">
		<?php echo JHtml::tooltip(JText::_('COM_JGIVE_IMAGE_TOOLTIP'), JText::_('COM_JGIVE_IMAGE'), '', JText::_('COM_JGIVE_IMAGE').' * ');?>
	</label>

	<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
		<div class="input-group">
				<?php
				if (isset($this->cdata['images']) && count($this->cdata['images']))
				{
				 ?>
					<input type="file"
					id="camp_image"
					name="camp_image"
					placeholder="<?php echo JText::_('COM_JGIVE_IMAGE');?>"
					accept="image/*"
					class="form-control">
					<br/>
					<span class="text-warning pull-left">
						<?php echo JText::_('COM_JGIVE_EXISTING_IMAGE_MSG');?>
					</span>
				<?php
				}
				else
				{
				?>
					<input type="file"
					id="camp_image"
					name="camp_image"
					placeholder="<?php echo JText::_('COM_JGIVE_IMAGE');?>"
					accept="image/*"
					class="form-control required">
					<input type='hidden' name='main_img_id' value="">
				<?php
				}
				?>
				<br/>
				<!-- File extensions & size -->
				<span class="text-info pull-left">
					<?php echo JText::sprintf('COM_JGIVE_IMAGE_SIZE', $this->params->get('large_width'), $this->params->get('large_height'));?>
					<br/>
					<?php echo JText::_("COM_JGIVE_IMAGE_EXTENSIONS"); ?>
					&nbsp;
					<?php echo JText::sprintf('COM_JGIVE_MAX_ALLOWED_FILE_SIZE', $this->params->get('max_size')); ?>
				</span>
		</div>
		<?php $camp_main_image = 'Campaign Main Image';?>
		<?php
		if (isset($this->cdata['images']) && count($this->cdata['images']))
		{ ?>
			<span class="text-info pull-left">
				<?php echo JText::_('COM_JGIVE_EXISTING_IMAGE');?>
			</span>
			<div class="clearfix"></div>
			<?php
			foreach($this->cdata['images'] as $img)
			{
				if($img->gallery==0)
				{
					if(file_exists($img->path))
					{
						echo "<input type='hidden' name='main_img_id' value=".$img->id.">";
						echo "<img class='img-rounded com_jgive_img_128_128 pull-left' src='".JUri::root().$img->path."' alt='". $camp_main_image ."' />";
						break;//print only 1 image
					}
					else
					{
						$path='images'.DS.'jGive'.DS;
						//get original image name to find it resize images (S,M,L)
						$org_file_after_removing_path=trim(str_replace($path,'S_',$img->path));
						$img_link= JUri::base().$path.$org_file_after_removing_path;

						echo "<input type='hidden' name='main_img_id' value=".$img->id.">";
						echo "<img class='img-rounded com_jgive_img_128_128 pull-left' src='".$img_link."' alt='". $camp_main_image ."'/>";
						break;//print only 1 image
					}
				}
			}
		} ?>

	</div>
</div>

<?php
if($this->params->get('img_gallery'))
{ ?>
<div class="form-group imagediv" id="imagediv">
	<label for="avatar"
		class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label">
			<?php echo JHtml::tooltip(JText::_('COM_JGIVE_PROD_IMG_TOOLTIP'), JText::_('COM_JGIVE_PROD_IMG'), '',JText::_('COM_JGIVE_PROD_IMG'));?>
	</label>
	<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
		<?php
		if(!empty($this->cdata['images']))
		{
		?>
			<span class="text-info pull-left">
				<?php echo JText::_('COM_JGIVE_UNCHECK_TO_REMOVE_EXISTING_IMAGE');?>
			</span>
			<div class="clearfix"></div>

			<?php
				foreach($this->cdata['images'] as $img)
				{
					if($img->gallery && $img->isvideo == 0)
					{
					?>
						<div class="pull-left jgive_images">
							<input type='checkbox' class="img_checkbox" name='existing_imgs_ids[]' value='<?php echo $img->id;?>' checked>
							<?php echo "<img class='img-rounded com_jgive_img_128_128' src='".JUri::root().$img->path."' />";?>
						</div>
					<?php
					}
				}
			?>

		<?php
		}
		?>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 addMoreImage">

			<!-- @TODO JUGAD done for add more images display -->
			<div class="filediv row com_jgive_campaign_image_block" id="filediv" >
				<input  type="file" name="jgive_img_gallery[]"  id="avatar" placeholder="" class="gallery_choose_btn col-lg-10 col-md-10 col-sm-10 col-xs-10"  accept="image/*">
			</div>

			<!-- File extensions & size -->
			<span class=" text-info pull-left">
				<?php echo JText::_("COM_JGIVE_GALLERY_IMAGE_EXT");?> &nbsp;<?php echo JText::sprintf('COM_JGIVE_MAX_ALLOWED_FILE_SIZE', $this->params->get( 'max_size' )); ?>
			</span>

		</div>

		<!-- ADD MORE BTN-->
		<div class="addmore col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="addmoreid" >
			<button onclick="addmoreImg('filediv','filediv');" type="button"
				class="btn btn-mini btn-primary pull-right btn-sm"
				title="<?php echo JText::_('COM_JGIVE_IMAGE_ADD_MORE');?>">
				<i class="<?php echo COM_JGIVE_ICON_PLUS;?> icon-white "></i>
			</button>
		</div>


	</div>
	<!-- END OF CONTROL-->
</div>
<!-- END OF control-group -->
<?php
} ?>
