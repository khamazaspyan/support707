<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;

// Creating Object of FrontendHelper class
$jgiveFrontendHelper = new jgiveFrontendHelper;

$videoTheme = $imageTheme = $givebackTheme = "";

if ($this->cdata['params']['video_gallery'])
{
	$videoTheme = "," . "'campaign.addvideo'";
}

if ($this->cdata['params']['img_gallery'])
{
	$imageTheme = "," . "'campaign.addimage'";
}

if ($this->cdata['params']['show_selected_fields'] == 1 )
{
	if (!in_array("give_back", $this->cdata['params']['creatorfield']))
	{
		$givebackTheme = "," . "'campaign.addgiveback'";
	}
}
else
{
	$givebackTheme = "," . "'campaign.addgiveback'";
}

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');

$user = JFactory::getUser();
$limit = 10;

?>
<script>
	jgive.campaign.loadActivity();
</script>
<div class="col-xs-12 col-sm-8">
	<form name="post-activity" method="post">
		<?php 
		if ($user->id == $this->cdata['campaign']->creator_id)
		{
		?>
		<div class="feed-item-cover campaigns todays-activity">
			<div class="date col-xs-3 col-sm-1 col-lg-1">
				<?php echo JText::_("COM_JGIVE_ACTIVITY_TODAY");?>
				</br>
				<?php echo JHtml::Date('now', 'd, M');?>
			</div>
			<div class="feed-item col-xs-9 col-sm-11 col-lg-11">
				<div class="feed-item-inner">
					<div class="form-group">
						<input
							class="form-control input-lg"
							id="activity-post-text"
							name="activity-post-text"
							placeholder="<?php echo JText::_("COM_JGIVE_ACTIVITY_TODAY_TEXT");?>"
							maxlength="300">
						</input>
						<div id="activity-post-text-length" class="pull-right clearfix"></div>
					</div>
					<div class="form-group">
						<button
							type="submit"
							id="postactivity"
							class="btn btn-primary pull-right clearfix">
							<?php echo JText::_("COM_JGIVE_TEXT_ACTIVITY_POST");?>
						</button>
					</div>
				</div>
			</div>
		</div>
		<?php 
		}
		?>
		<div id="tj-activitystream" tj-activitystream-widget 
		tj-activitystream-theme="campaignfeed" tj-activitystream-bs="bs3" 
		tj-activitystream-type= "'campaign.extended','jgive.donation','campaign.completed','jgive.addcampaign','jgive.textpost'<?php echo $videoTheme . $imageTheme . $givebackTheme;?>" 
		tj-activitystream-target-id="<?php echo $this->cdata['campaign']->id;?>" tj-activitystream-limit="<?php echo $limit;?>" tj-activitystream-language="<?php echo $this->cdata['language']->lang;;?>">
		</div>
		<input type="hidden" name="option" value="com_jgive"></input>
		<input type="hidden" name="task" value="campaign.addPostedActivity"></input>
	</form>
</div>
<?php
if ($this->cdata['CampaingHideField']->show_field == 1 OR $this->cdata['CampaingHideField']->give_back_cnf == 0)
{
	if (count($this->cdata['givebacks']) >= 1)
	{
	?>
		<div class="col-xs-12 col-sm-4" style="border-left: 1px solid #E3DADA;">
			<strong><?php echo strtoupper(JText::_('COM_JGIVE_SINGLE_GIVEBACK'));?></strong>
			<div class="clearfix">&nbsp;</div>
			<?php
				$i = 0;

				foreach ($this->cdata['givebacks'] as $giveback)
				{
					$sold_givebacks = $giveback->sold_giveback;
					$give_back_flag = 0;
					$giveback_tooltip = JText::_('COM_JGIVE_BY_GIVEBACK');
					$giveback_id = $giveback->id;

					if ($sold_givebacks == $giveback->total_quantity || $sold_givebacks > $giveback->total_quantity)
					{
						$give_back_flag   = 1;
						$giveback_tooltip = JText::_('COM_JGIVE_GIVEBACK_SOLD_OUT');
					}

					$url = JUri::root(true) . '/index.php?option=com_jgive&task=donations.donate&cid=' . $this->cdata['campaign']->id . '&giveback_id=' .
					$giveback_id . '&Itemid=' . $this->cdata['otherData']->createCampItemid;

					$onclick = 'onclick="callDonateForm(\'' . $url . '\')"';

					// Disable onlclick
					if ($give_back_flag == 1)
					{
						$onclick = '';
					}
			?>
					<div class="row">
						<a class="pull-left" <?php if ($onclick) echo 'href="' . $url . '"';?> title="<?php echo  $giveback_tooltip; ?>">
						<div class="col-xs-12 giveback">
							<?php
								if ($giveback->image_path)
								{
									if (file_exists(JPATH_ROOT . '/' . $giveback->image_path))
									{
									?>
										<a class="pull-left" <?php if ($onclick) echo 'href="' . $url . '"';?> title="<?php echo  $giveback_tooltip; ?>">
											<img class="" data-src="holder.js/64x64" alt="64x64"  src="<?php echo JUri::base() . $giveback->image_path; ?>">
										</a>
								<?php
									}
								}
							?>
						</div>
						<div class="col-xs-12">
							<?php echo wordwrap($giveback->description, 70, "<br>\n");?>
						</div>
						<div class="col-xs-12">
							<strong>
								<?php
									echo $jgiveFrontendHelper->getFormattedPrice($giveback->amount) . ' | ';

									if ($give_back_flag == 1)
									{
										echo JText::_('COM_JGIVE_GIVEBACK_SOLD');
									}
									else
									{
										echo $sold_givebacks;
									}
								?>
							</strong>
						</div>
						</a>
					</div>
					<div class="clearfix">&nbsp;</div>
			<?php
				$i ++;
				}
			?>
		</div>
<?php
	}
}
