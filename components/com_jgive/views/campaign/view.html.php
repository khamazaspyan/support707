<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.user.helper');
jimport('joomla.utilities.arrayhelper');

/**
 * JgiveViewCampaign form view class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JgiveViewCampaign extends JViewLegacy
{
	protected $form_extra;

	/**
	 * Class constructor.
	 *
	 * @param   Array  $config  Config
	 *
	 * @since   1.8
	 */
	public function __construct($config = array())
	{
		$this->app  = JFactory::getApplication();
		parent::__construct($config = array());
	}

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$user       = JFactory::getUser();

		$canCreate  = $user->authorise('core.create', 'com_jgive');
		$canEdit    = $user->authorise('core.edit', 'com_jgive');
		$canCheckin = $user->authorise('core.manage', 'com_jgive');
		$canChange  = $user->authorise('core.edit.state', 'com_jgive');

		// Get logged in user id
		$user = JFactory::getUser();
		$this->logged_userid = $user->id;

		// Get params
		$this->params				  = JComponentHelper::getParams('com_jgive');
		$this->currency_code 		  = $this->params->get('currency');
		$this->commission_fee		  = $this->params->get('commission_fee');
		$this->send_payments_to_owner = $this->params->get('send_payments_to_owner');
		$this->default_country		  = $this->params->get('default_country');
		$this->admin_approval		  = $this->params->get('admin_approval');

		// Create is a default layout
		$layout = $this->app->input->get('layout', 'create');
		$this->setLayout($layout);

		// Create jgive helper object
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$this->jomsocailToolbarHtml = $jgiveFrontendHelper->jomsocailToolbarHtml();

		if ($layout === 'create')
		{
			if (!$this->logged_userid)
			{
				$input      = $this->app->input;
				$itemid = $input->get('Itemid');
				$msg = JText::_('COM_JGIVE_LOGIN_MSG');
				$uri    = 'index.php?option=com_jgive&view=campaign&layout=create&Itemid=' . $itemid;
				$url = base64_encode($uri);
				$this->app->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url, false), $msg);
			}

			// Get country list options
			$countries = $jgiveFrontendHelper->getCountries();
			$this->countries = $countries;

			// Default task is save
			$this->task = 'save';
			$cid = JRequest::getInt('cid', '');

			// Get all campaigns email id
			$this->allCampsitemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
			$this->myCampaignsItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');
			$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');

			// If cid is passed task is - edit
			if ($cid)
			{
				if (!$canEdit)
				{
					$this->app->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

					return false;
				}

				$this->task = 'edit';

				$cdata['campaign'] = new Stdclass;

				$cdata = $this->get('Campaign');
				$this->cdata = $cdata;

				// Get form for extra fields.
				$this->form_extra = array();
				$ModelCampaign = $this->getModel('campaign');

				$input  = $this->app->input;
				$content_id = $this->cdata['campaign']->id;
				$input->set("content_id", $content_id);

				// Call to extra fields
				$this->form_extra = $ModelCampaign->getFormExtra(
				array("category" => $this->cdata['campaign']->category_id,
					"clientComponent" => 'com_jgive',
					"client" => 'com_jgive.campaign',
					"view" => 'campaign',
					"layout" => 'create')
					);

				$this->form_extra = array_filter($this->form_extra);

				// Only owner can edit campaign if not owner redirect to all campaigns
				if ($cdata['campaign']->creator_id != $this->logged_userid)
				{
					$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
					$link = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&Itemid=' . $itemid, false);
					$msg = JText::_('COM_JGIVE_CAMPAIGN_NO_EDIT_PERMISSIONS');
					$this->app->redirect($link, $msg);
				}
			}
			else
			{
				if (!$canCreate)
				{
					$this->app->enqueueMessage(JText::_('COM_JGIVE_AUTH_ERROR'), 'error');

					return false;
				}

				$cdata['campaign'] = new Stdclass;

				$integration = $this->params->get('integration');

				// Joomla profile import
				$profile_import = $this->params->get('profile_import');

				// If profie import is on the call profile import function
				$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;

				if ($profile_import)
				{
					$cdata = $JgiveIntegrationsHelper->profileImport();
				}

				// Check is user profile completed to allow create campaign
				$profile_complete = $this->params->get('profile_complete');
				$profile_check = array();

				if ($profile_complete)
				{
					$profile_check = $JgiveIntegrationsHelper->profileChecking();

					if (!empty($profile_check))
					{
						if ($integration === 'joomla')
						{
							$msg    = JText::_('COM_JGIVE_PROFILE_COMPLETE_MSG');
							$uri    = $this->app->input->get('REQUEST_URI', '', 'server', 'string');
							$url    = base64_encode($uri);
							$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_users&view=profile&layout=edit');
							$this->app->redirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit&itemid=' . $itemid . '&return=' . $url), $msg);
						}
					}
				}

				$this->cdata = $cdata;
			}

			// Get js group for loggend in user
			$this->js_groups = $this->get('JS_usergroup');

			// Get the campaigns categeory
			$cats = $this->get('CampaignsCats');

			$this->assignRef('cats', $cats);
		}

		// Show a single campaign
		if ($layout == 'single')
		{
			$this->singleCampaign();
		}

		if ($layout == 'single_playvideo')
		{
			$this->_playVideo();
		}

		$this->createCampaignTime();
		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Function to Fetching Single campaign data
	 *
	 * @return  array
	 * since 2.0
	 */
	protected function singleCampaign()
	{
		$user     = JFactory::getUser();
		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$this->params = JComponentHelper::getParams('com_jgive');

		$plgDatat = JPluginHelper::importPlugin('system');
		$dispatcher = JDispatcher::getInstance();
		$result = $dispatcher->trigger('getActivityScript', array('campaignfeed'));

		// Get campaign details
		$cdata = $this->get('Campaign');
		$cdata['CampaingHideField'] = new stdClass;

		$cdata['CampaingHideField']->show_field = 0;
		$this->show_selected_fields = $this->params['show_selected_fields'];

		// Campaign hiden field configuration array
		if ($this->show_selected_fields)
		{
			$creatorfield = $this->params['creatorfield'];

			if (isset($creatorfield))
			{
				foreach ($creatorfield as $tmp)
				{
					switch ($tmp)
					{
						case 'max_donation':
							$cdata['CampaingHideField']->max_donation_cnf = 1;
						break;

						case 'long_desc':
							$cdata['CampaingHideField']->long_desc_cnf = 1;
						break;

						case 'show_public':
							$cdata['CampaingHideField']->show_public_cnf = 1;
						break;

						case 'address':
							$cdata['CampaingHideField']->address_cnf = 1;
						break;

						case 'address2':
							$cdata['CampaingHideField']->address2_cnf = 1;
						break;

						case 'zip':
							$cdata['CampaingHideField']->zip_cnf = 1;
						break;

						case 'phone':
							$cdata['CampaingHideField']->phone_cnf = 1;
						break;

						case 'group_name':
							$cdata['CampaingHideField']->group_name_cnf = 1;
						break;

						case 'website_address':
							$cdata['CampaingHideField']->website_address_cnf = 1;
						break;

						case 'give_back':
							$cdata['CampaingHideField']->give_back_cnf = 1;
						break;

						case 'goal_amount':
							$cdata['CampaingHideField']->goal_amount = 1;
						break;
					}
				}
			}
		}
		else
		{
			$cdata['CampaingHideField']->max_donation_cnf = 0;
			$cdata['CampaingHideField']->long_desc_cnf    = 0;
			$cdata['CampaingHideField']->show_public_cnf  = 0;
			$cdata['CampaingHideField']->address_cnf      = 0;
			$cdata['CampaingHideField']->address2_cnf     = 0;
			$cdata['CampaingHideField']->zip_cnf          = 0;
			$cdata['CampaingHideField']->phone_cnf        = 0;
			$cdata['CampaingHideField']->group_name_cnf   = 0;
			$cdata['CampaingHideField']->website_address_cnf = 0;
			$cdata['CampaingHideField']->give_back_cnf       = 0;
			$cdata['CampaingHideField']->goal_amount         = 0;
			$cdata['CampaingHideField']->show_field = 1;
		}

		// Added here whatever data need for Campaign detail page
		$cdata['otherData'] = new stdClass;
		$cdata['otherData']->singleCampItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');
		$cdata['otherData']->allCampItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
		$cdata['otherData']->createCampItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=create');
		$cdata['otherData']->dashboardCampItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=dashboard&layout=default');
		$cdata['otherData']->loggedUserId = $user->id;
		$cdata['otherData']->canEdit = $user->authorise('core.edit', 'com_jgive');
		$cdata['params'] = $this->params;
		$cdata['language'] = new stdClass;
		$cdata['language']->lang = JFactory::getLanguage()->getTag();

		$this->cdata = $cdata;

		// Do not show campaign if it is unpublished And redirect to all campaigns
		if (!$cdata['campaign']->published)
		{
			$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
			$link = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&Itemid=' . $itemid, false);
			$msg = JText::_('COM_JGIVE_CAMPAIGN_NOT_PUBLISHED');
			$this->app->redirect($link, $msg);
		}

		$pathway = $this->app->getPathway();
		$pathway->addItem($cdata['campaign']->title, '');
		$ModelCampaign = $this->getModel('campaign');
		$input  = $this->app->input;
		$content_id = $this->cdata['campaign']->id;
		$input->set("content_id", $content_id);

		// Call to extra fields
		$data = array("category" => $this->cdata['campaign']->category_id,
				"clientComponent" => 'com_jgive',
				"client" => 'com_jgive.campaign',
				"view" => 'campaign',
				"layout" => 'create');

		$this->extraData = $ModelCampaign->getDataExtra($data);
	}

	/**
	 * Function to Build data to call video plugin
	 *
	 * @parmas
	 *
	 * @return  void
	 */
	protected function _playVideo()
	{
		$input = JFactory::getApplication()->input;
		$vid   = $input->get('vid', '', 'INT');
		$type   = $input->get('type', '', 'STRING');
		$model = $this->getModel('campaign');

		if (!empty($vid))
		{
			// Get video data
			$this->video = $model->getVideoData($vid, $type);

			// Build needed params to call video player plugin
			if (!empty($this->video))
			{
				$this->video_params = array();

				// Added by Nidhi
				$jgivemediaHelper  = new jgivemediaHelper;
				$params			= JComponentHelper::getParams('com_jgive');
				$video_upload = $params->get('video_upload', '1');

				if ($video_upload == 1)
				{
					if (!empty($type))
					{
						// Set params according to upload option
						switch (trim($type))
						{
							// For uploaded files - Modified as per media table
							case 'video':
								// File path
								$this->video_params['file'] = JUri::root() . $this->video->path;

								// Plugin to call to play video
								$this->video_params['plugin'] = 'jwplayer';
							break;

							// For URLs like youtube video url etc.
							case 'youtube' || 'vimeo':
								switch ($this->video->type)
								{
									// Video provider youtube
									case 'youtube':
										// Get youtube video ID from embed url, after explode in array 4th index contain actual video id
										$explodedUrl = explode('/', $this->video->url);

										if (!empty($explodedUrl))
										{
											$videoId = end($explodedUrl);
											$this->video_params['file'] = 'https://www.youtube.com/watch?v=' . $videoId;
											$this->video_params['videoId'] = $videoId;

											// Plugin to call to pay video
											$this->video_params['plugin'] = 'jwplayer';
										}
									break;

									// Video provider vimeo
									case 'vimeo':
										$explodedUrl = explode('/', $this->video->url);

										if (!empty($explodedUrl))
										{
											$videoId = end($explodedUrl);

											// Get youtube video ID from embed url, after explode in array 4th index contain actual video id
											$this->video_params['videoId'] = $videoId;

											// Plugin to call to pay video
											$this->video_params['plugin'] = 'vimeo';
										}
									break;

									// Other video provider than above
									default:
										// For future
									break;
								}
							break;
						}
					}

					// Set calling client
					$this->video_params['client'] = $input->get('option', '', 'STRING');
				}
			}
		}
	}

	/**
	 * Function For adding meta tags
	 *
	 * @return void
	 */
	protected function _prepareDocument()
	{
		if (isset($this->cdata['campaign']->meta_data))
		{
			// Get meta_data value
			if ($this->cdata['campaign']->meta_data)
			{
				$this->document->setMetadata('keywords', $this->cdata['campaign']->meta_data);
			}
			elseif (!$this->cdata['campaign']->meta_data && $this->params->get('menu-meta_keywords'))
			{
				// If the meta data is empty get the default menu meta_data value
				$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
			}
		}

		if (isset($this->cdata['campaign']->meta_desc))
		{
			if ($this->cdata['campaign']->meta_desc)
			{
				$this->document->setDescription($this->cdata['campaign']->meta_desc);
			}
			elseif (!$this->cdata['campaign']->meta_desc && $this->params->get('menu-meta_description'))
			{
				$this->document->setDescription($this->params->get('menu-meta_description'));
			}
		}

		// This robots tag is used for tells to search engine what link is follow
		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Method for setting hr and min
	 *
	 * @return  array
	 */
	public function createCampaignTime()
	{
		for ($i = 1; $i <= 12; $i++)
		{
			$hours[] = JHtml::_('select.option', $i, $i);
		}

		$minutes   = array();
		$minutes[] = JHtml::_('select.option', '00', '00');
		$minutes[] = JHtml::_('select.option', 15, '15');
		$minutes[] = JHtml::_('select.option', 30, '30');
		$minutes[] = JHtml::_('select.option', 45, '45');

		$amPmSelect   = array();
		$amPmSelect[] = JHtml::_('select.option', 'AM', 'am');
		$amPmSelect[] = JHtml::_('select.option', 'PM', 'pm');

		$this->campaignTimeData['hours'] = $hours;
		$this->campaignTimeData['minutes'] = $minutes;
		$this->campaignTimeData['amPmSelect'] = $amPmSelect;

		return $this->campaignTimeData;
	}
}
