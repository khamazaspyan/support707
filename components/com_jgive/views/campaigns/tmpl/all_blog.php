<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access

defined('_JEXEC') or die('Restricted access');
echo '<div id="fb-root"></div>';

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');

$fblike_tweet = JUri::root(true) . '/media/com_jgive/javascript/fblike.js';
echo "<script type='text/javascript' src='" . $fblike_tweet . "'></script>";

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
//~ JHtml::_('formbehavior.chosen', 'select');

$setdata = JRequest::get('get');
$core_js = JUri::root() . 'media/system/js/core.js';

$flg = 0;
$document = JFactory::getDocument();

$campaignHelper = new campaignHelper;

foreach ($document->_scripts as $name => $ar)
{
	if ($name == $core_js )
	{
		$flg = 1;
	}
}

if ($flg == 0)
{
	echo "<script type='text/javascript' src='" . $core_js . "'></script>";
}

$params     = JComponentHelper::getParams('com_jgive');
$show_field = 0;
$max_donation_cnf = 0;
$goal_amount      = 0;
$show_selected_fields = $params->get('show_selected_fields');

if ($show_selected_fields)
{
	$creatorfield = $params->get('creatorfield');

	if (isset($creatorfield))

	foreach ($creatorfield as $tmp)
	{
		switch ($tmp)
		{
			case 'max_donation':
				$max_donation_cnf = 1;
			break;

			case 'long_desc':
				$long_desc_cnf = 1;
			break;

			case 'goal_amount':
				$goal_amount = 1;
			break;
		}
	}
}
else
{
	$show_field = 1;
}
?>

<div class="tjBs3" id="all">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-3">
				<ul class="list-inline">
					<li><h5><strong><?php echo strtoupper(JText::_('COM_JGIVE_ALL_CAMPAIGNS'));?></strong></h5></li>
				</ul>
			</div>
			<div class="col-xs-12 col-md-9 campaignBolgFilters ">
				<form action="" method="post" name="adminForm3" id="adminForm3">
					<input type="hidden" name="option" value="com_jgive" />
					<input type="hidden" name="view" value="campaigns" />
					<input type="hidden" name="layout" value="all" />
					<ul class="pull-right list-inline">
						<?php
							$launch_camp_url = JUri::root() . substr(
							JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&Itemid=' . $this->otherData['createCampItemid']),
							strlen(JUri::base(true)) + 1
							);?>


						<?php
						if ($this->params->get('show_search_filter'))
						{?>
						<li>
							<span class="pull-left searchDonor" id="SearchFilterInputBox">
								<input type="text"
									placeholder="<?php echo JText::_('COM_JGIVE_ENTER_CAMPAIGN_NAME'); ?>"
									name="search_list"
									id="search_list"
									value="<?php echo $this->lists['search_list']; ?>"
									class="form-control col-xs-5"
									onchange="document.adminForm.submit();" />

									<button id ="campaignBlogSearchBtn" type="button" onclick="this.form.submit();" class="button btn btn-default col-xs-1" data-original-title="Search"
									title="<?php echo JText::_('COM_JGIVE_SEARCH_TOOLTIP');?>">
										<i class="fa fa-search" ></i>
									</button>
									<button
										onclick="document.getElementById('search_list').value='';this.form.submit();"
										type="button"
										class="button btn btn-default col-xs-2"
										data-original-title="Clear"
										title="<?php echo JText::_('COM_JGIVE_CLEAR_TOOLTIP');?>">
											<i class="fa fa-close"></i>
									</button>
							</span>
							<a id="searchCampBtn" href="javascript:void(0)" onclick="jgive.searchFilter()" title="<?php echo JText::_('COM_JGIVE_ENTER_CAMPAIGN_NAME')?>">
								<i class="fa fa-search" ></i>
							</a>
						</li>
						<li>|</li>
						<?php
						}
						if ($this->params->get('show_filters'))
						{
							if ($this->params->get('show_sorting_options'))
							{?>
								<li>
									<div class="dropdown">
										<a class="dropdown-toggle" type="" data-toggle="dropdown" title="<?php echo JText::_('COM_JGIVE_ORDERING_OPTIONS');?>"><i class="fa fa-sort" aria-hidden="true"></i></a>
										<?php
											echo JHtml::_('select.genericlist', $this->ordering_options, "filter_order", ' size="1"
											onchange="this.form.submit();"
											class="dropdown-menu form-control" name="filter_order"',"value", "text", $this->lists['filter_order']);
										?>
									</div>
								</li>
							<li>|</li>
							<?php
							}
							?>
							<li>
								<a id="displayFilter" href="javascript:void(0)" onclick="jgive.campaigns.filterCamp()" title="<?php echo JText::_('COM_JGIVE_FILTER_CAMPAIGN');?>">
									<i class="fa fa-filter"></i>
								</a>
							</li>
						<li>|</li>
						<?php
						}?>
						<li><?php echo $this->pagination->getLimitBox(); ?></li>
					</ul>
				</form>
			</div>
			<!-- Filters Div-->
			<div class="col-xs-12 searchDonor" id="displayFilterText">
				<?php
					$displayData['campData'] = $this->data;
					$displayData['pagination'] = $this->pagination;
					$displayData['params'] = $this->params;
					$displayData['otherData'] = $this->otherData;
					$displayData['campTypeOption'] = $this->campaign_type_filter_options;
					$displayData['categories'] = $this->cat_options;
					$displayData['org_ind_type'] = $this->filter_org_ind_type;
					$displayData['list'] = $this->lists;

					$html = "";
					$layout = new JLayoutFile('filters', $basePath = JPATH_SITE . '/components/com_jgive/layouts/campaigns');
					$html .= $layout->render($displayData);
					$result = $html;
					echo $result;
				?>
				<div class="clearfix">&nbsp;</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
				<!--Form code here-->
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php
					if (empty($this->data))
					{?>
						<div class="alert alert-warning">
							<?php echo JText::_('COM_JGIVE_NO_CAMPAIGN_FOUND');?>
						</div>
					<?php
					}
					else
					{
						foreach($this->data as $cdata)
						{
						?>
							<div class="com_jgive_border">
								<div class='com_jgive_campaign_title'>
									<h2>
										<?php
										// Show the star to know the campaigns marked as Featured
										$title = JText::_('COM_JGIVE_FEATURED');
										$result = $campaignHelper->isFeatured($cdata['campaign']->id);
										echo $result ? $imgpath = '<img src="' . JUri::root(true) . '/media/com_jgive/images/featured.png"  title="' . $title . '">':'';
										?>
										<a
										target="_blank"
										href="<?php echo JUri::root() . substr(
										JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cdata['campaign']->id . '&Itemid=' .
										$this->singleCampaignItemid
										), strlen(JUri::base(true)) + 1
										);?>">
											<?php echo $cdata['campaign']->title;?>
										</a>
									</h2>

									<?php
									// Generate unique ad url for social sharing
									require_once JPATH_SITE . "/components/com_jgive/helpers/integrations.php";
									$ad_url = 'index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cdata['campaign']->id;

									// Integration with Jlike
									if (file_exists(JPATH_SITE . '/' . 'components/com_jlike/helper.php'))
									{
										$show_comments = -1;
										$show_like_buttons = 1;

										$jlikehtml = $this->jgiveFrontendHelper->DisplayjlikeButton(
										$ad_url, $cdata['campaign']->id, $cdata['campaign']->title, $show_comments, $show_like_buttons
										);

										if ($jlikehtml)
										{
											echo $jlikehtml;
										}
									}

									// Integration with Jlike
									$ad_url = JUri::root() . substr(JRoute::_($ad_url), strlen(JUri::base(true)) + 1);
									$add_this_share = '';
									$params = JComponentHelper::getParams('com_jgive');
									$pid = $params->get('addthis_publishid', 'GET', 'STRING');

									if ($params->get('social_sharing'))
									{
										if ($params->get('social_shring_type') == 'addthis')
										{
											$add_this_share = '
											<!-- AddThis Button BEGIN -->
											<div class="addthis_toolbox addthis_default_style">

											<a class="addthis_button_facebook_like" fb:like:layout="button_count" class="addthis_button" addthis:url="' . $ad_url . '"></a>
											<a class="addthis_button_google_plusone" g:plusone:size="medium" class="addthis_button" addthis:url="' . $ad_url . '"></a>
											<a class="addthis_button_tweet" class="addthis_button" addthis:url="' . $ad_url . '"></a>
											<a class="addthis_button_pinterest_pinit" class="addthis_button" addthis:url="' . $ad_url . '"></a>
											<a class="addthis_counter addthis_pill_style" class="addthis_button" addthis:url="' . $ad_url . '"></a>
											</div>
											<script type="text/javascript">
												var addthis_config ={ pubid: "' . $pid . '"};
											</script>
											<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid="' . $pid . '"></script>
											<!-- AddThis Button END -->';

											$add_this_js = 'http://s7.addthis.com/js/300/addthis_widget.js';
											$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
											$JgiveIntegrationsHelper->loadScriptOnce($add_this_js);

											// Output all social sharing buttons
											foreach ($cdata['images'] as $img)
											{
												break;
											}

											if (file_exists($img->path))
											{
												$image_linnk = $img->path;
											}
											else
											{
												$path = 'images/jGive/';

												// Get original image name to find it resize images (S,M,L)
												$org_file_after_removing_path = trim(str_replace($path, 'M_', $img->path));
												$image_linnk = $path . $org_file_after_removing_path;
											}

											echo' <div id="rr" style="">
												<div class="social_share_container">
												<meta property="og:title" content="The Rock"/>
												<div class="social_share_container_inner" onmouseover="onmouseoverfn(\'' . $cdata['campaign']->id . '\',\'' .
												$cdata['campaign']->title . '\',\'' . JUri::base() . $image_linnk . '\' )">' . $add_this_share . '</div></div></div>';
										}
										else
										{
											echo '<div class="com_jgive_horizontal_social_buttons">';
											echo '<div class="com_jgive_float_left">
													<div class="fb-like" data-href="' . $ad_url . '" data-send="true" data-layout="button_count" data-width="450" data-show-faces="true"></div>
												</div>';
											echo '<div class="com_jgive_float_left">
													&nbsp; <div class="g-plus" data-action="share" data-annotation="bubble" data-href="' . $ad_url . '"></div>
												</div>';
											echo '<div class="com_jgive_float_left">
													&nbsp; <a href="https://twitter.com/share" class="twitter-share-button" data-url="' . $ad_url . '" data-counturl="' .
													$ad_url . '"  data-lang="en">Tweet</a>
												</div>';
											echo '</div>
												<div class="com_jgive_clear_both"></div>';
										}
									}
									?>
								</div>

									<div class="row">
										<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 com_jgive_campaign_image_block">
											<?php
												$path = 'images/jGive/';

												foreach ($cdata['images'] as $img)
												{
													$fileParts = pathinfo($img->path);

													if ($img->gallery == 0)
													{
														// If loop for old version compability (where img resize not available means no L , M ,S before image name)
														if (file_exists($path . $fileParts['basename']))
														{
															$img_path = JUri::base() . $path . $fileParts['basename'];
														}
														else
														{
															$img_path = JUri::base() . $path . 'M_' . $fileParts['basename'];
														}

														echo "<img class='img-thumbnail com_jgive_img_96_96'src='" . $img_path . "' />";
														break;
													}
												}
											?>
										</div>

										<div class="col-lg-10 col-md-9 col-sm-9 col-xs-12 com_jgive_justify">
											<?php echo nl2br($cdata['campaign']->short_description);?>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<table class="table table-bordered">
												<tr>
													<!--if condition added by Sneha, to goal amount-->
													<?php
													if ($show_field == 1 OR $goal_amount == 0 ):
													?>
														<td width="25%" class="com_jgive_td_amt com_jgive_td_center">
															<?php echo JText::_('COM_JGIVE_GOAL_AMOUNT');?>
														</td>

														<td width="25%" class="com_jgive_td_amt_right com_jgive_td_center">
															<?php
															$diplay_amount_with_format = $this->jgiveFrontendHelper->getFormattedPrice($cdata['campaign']->goal_amount);
															echo $diplay_amount_with_format;
															?>
														</td>
													<?php
													endif; ?>

													<?php
														$css_start_date = '';
														$css_end_date = '';
														$css_max_donors = '';

														// Check if exeeding goal amount is allowed
														// If not check for received amount to decide about hiding donate button
														$flag = 0;
														$date_expire = 0;

														if ($cdata['campaign']->allow_exceed == 0)
														{
															if ($cdata['campaign']->amount_received >= $cdata['campaign']->goal_amount)
															{
																$flag = 1;
															}
														}

														if ($cdata['campaign']->max_donors > 0)
														{
															if ($cdata['campaign']->donor_count >= $cdata['campaign']->max_donors)
															{
																$flag = 1;
																$css_max_donors = "class='text-error'";
															}
														}

														// If both start date, and end date are present
														$curr_date = '';

														if ((int) $cdata['campaign']->start_date && (int) $cdata['campaign']->end_date)
														{
															$start_date = JFactory::getDate($cdata['campaign']->start_date)->Format(JText::_('Y-m-d'));
															$end_date = JFactory::getDate($cdata['campaign']->end_date)->Format(JText::_('Y-m-d'));
															$curr_date = JFactory::getDate()->Format(JText::_('Y-m-d'));

															// If current date is less than start date, don't show donate button
															if ($curr_date < $start_date)
															{
																$flag = 1;
																$css_start_date = "class='text-error'";
															}

															// If current date is more than end date, don't show donate button
															if ($curr_date > $end_date)
															{
																$flag = 1;
																$date_expire = 1;
																$css_end_date = "class='text-error'";
															}
														}


														// Calculate progress progress-progress-bar data
														$recPer = intval((100 * $cdata['campaign']->amount_received) / $cdata['campaign']->goal_amount);

														if ($recPer > 100)
														{
															$recPer = 100;
															$progresslabel = JText::_('COM_JGIVE_MORE_THAN_HUNDRED') . ' %';
														}
														else
														{
															$progresslabel = $recPer . '%';
														}
													?>
												</tr>

												<tr>
													<td width="25%" class="com_jgive_td_amt com_jgive_td_center">
														<?php echo JText::_('COM_JGIVE_AMOUNT_RECEIVED');?>
													</td>
													<td width="25%" class="com_jgive_td_amt_right com_jgive_td_center">
														<?php

														$diplay_amount_with_format = $this->jgiveFrontendHelper->getFormattedPrice($cdata['campaign']->amount_received);
														echo $diplay_amount_with_format;
														?>
													</td>
												</tr>

												<tr>
													<td width="25%" class="com_jgive_td_amt com_jgive_td_center">
														<?php echo JText::_('COM_JGIVE_REMAINING_AMOUNT');?>
													</td>
													<td width="25%" class="com_jgive_td_amt_right com_jgive_td_center">
														<?php
														if ($cdata['campaign']->amount_received > $cdata['campaign']->goal_amount)
														{
															echo JText::_('COM_JGIVE_NA');
														}
														else
														{
															$diplay_amount_with_format = $this->jgiveFrontendHelper->getFormattedPrice($cdata['campaign']->remaining_amount);

															echo $diplay_amount_with_format;
														}
														?>
													</td>
												</tr>
												<tr>
													<td width="25%" class="com_jgive_td_amt com_jgive_td_center">
														<?php
															$time_curr_date = strtotime($curr_date);
															$time_end_date  = strtotime($cdata['campaign']->end_date);
															$interval       = $time_end_date - $time_curr_date;
															$days_left      = floor($interval / (60 * 60 * 24));

															if ((int) ($time_curr_date) && (int) ($time_end_date))
															{
																echo JText::_('COM_JGIVE_DAYS_LEFT');
															}
														?>
													</td>
													<td width="25%" class="com_jgive_td_amt_right com_jgive_td_center">
														<?php
															if ($date_expire)
															{
																echo JText::_('COM_JGIVE_NA');
															}
															elseif ((int) ($time_curr_date) && (int) ($time_end_date))
															{
																echo $days_left > 0 ? $days_left : JText::_('COM_JGIVE_NA');
															}
														?>
													</td>
												</tr>
												<tr>
													<td width="25%" class="com_jgive_td_amt com_jgive_td_center" colspan="2">
														<?php
														echo JText::_('COM_JGIVE_TOTAL') . (($cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_DONATIONS') : JText::_('COM_JGIVE_INVESTMENTS'));

														if ($show_field == 1 OR $max_donation_cnf == 0 )
															echo ' / ' . JText::_('COM_JGIVE_MAX_ALLOWED') . (($cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_DONATIONS') : JText::_('COM_JGIVE_INVESTMENTS'));

														?>
														<br/>
														<span <?php echo $css_max_donors;?>>
															<?php

															echo $cdata['campaign']->donor_count;

															if ($show_field == 1 OR $max_donation_cnf == 0 )
															{
																if ($cdata['campaign']->max_donors > 0)
																{
																	echo ' / ' . $cdata['campaign']->max_donors;
																}
																else
																{
																	echo ' / ' . JText::_('COM_JGIVE_NA');
																}
															}
															?>
														</span>
													</td>
												</tr>
											</table>
										</div>

										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 campaignBlogProgress">

											<div class="row">
												<div class="form-group">
													<label class="col-lg-3 col-md-4 col-sm-5 col-xs-6 control-label"><span <?php echo $css_start_date;?> ><?php echo JText::_('COM_JGIVE_START_DATE');?>:</span></label>

													<div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
														<?php echo JHtml::_('date', $cdata['campaign']->start_date, JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));?>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="form-group">
													<label class="col-lg-3 col-md-4 col-sm-5 col-xs-6 control-label"><span <?php echo $css_end_date;?>> <?php echo JText::_('COM_JGIVE_END_DATE');?>:</span></label>

													<div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
														<?php echo JHtml::_('date', $cdata['campaign']->end_date, JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));?>
													</div>
												</div>
											</div>

										<?php
										if ($show_field == 1 OR $goal_amount == 0 )
										{
										?>
												<div class="progress" >
													<div class="progress-bar progress-bar-success " style="width:<?php echo $recPer;?>%; min-width: 2em;" >
														<?php echo $progresslabel;?>
													</div>
												</div>
										<?php
										}?>

											<?php
											if ($flag == 0)
											{
											?>
												<div class="row">
													<form action="" method="post" name="adminForm" id="adminForm">
														<input type="hidden" name="cid" value="<?php echo $cdata['campaign']->id; ?>" />

														<div class="form-group">
															<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																<button class="btn btn-success form-control" type="submit">
																	<?php
																	echo (($cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_BUTTON_DONATE') : JText::_('COM_JGIVE_BUTTON_INVEST'));
																	?>
																</button>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																	<a
																	target="_blank"
																	class="btn btn-primary form-control"
																	href="<?php echo JUri::root() . substr(
																	JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cdata['campaign']->id . '&Itemid=' .
																	$this->singleCampaignItemid
																	), strlen(JUri::base(true)) + 1
																	);?>">
																			<?php echo JText::_('COM_JGIVE_READ_MORE');?>
																	</a>
															</div>
														</div>

														<input type="hidden" name="option" value="com_jgive" />
														<!--<input type="hidden" name="controller" value="donations" />-->
														<input type="hidden" name="task" value="donations.donate" />
													</form>
												</div>
												<?php
											}
											else
											{
												?>
												<input type="button" class="btn disabled com_jgive_button" value="<?php
													echo (($cdata['campaign']->type == 'donation') ? JText::_('COM_JGIVE_DONATIONS_CLOSED') : JText::_('COM_JGIVE_INVESTMENTS_CLOSED'));
												?>"/>
												<a
												class="btn btn-primary com_jgive_button"
												href="<?php echo JUri::root() . substr(
												JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cdata['campaign']->id . '&Itemid=' .
												$this->singleCampaignItemid
												), strlen(JUri::base(true)) + 1
												);?>">
													<?php echo JText::_('COM_JGIVE_READ_MORE');?>
												</a>
												<?php
											}
											?>
										</div>
									</div>

								</div>

							<?php
						}
					}
					?>
				</div>
			</div>

			<form action="" method="post" name="adminForm" id="adminForm">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php $class_pagination = ''; ?>
						<div class="<?php echo $class_pagination; ?> com_jgive_align_center pull-right">
							<?php echo $this->pagination->getListFooter(); ?>
						</div>
					</div>
				</div>
				<input type="hidden" name="option" value="com_jgive" />
				<input type="hidden" name="view" value="campaigns" />
				<input type="hidden" name="layout" value="all" />
				<input type="hidden" name="defaltevent" value="<?php echo $this->lists['filter_campaign_cat'];?>" />
			</form>
		</div>

	</div>
	</div>
</div>

