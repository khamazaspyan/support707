<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

// Jomsocial toolbar
echo $this->jomsocailToolbarHtml;

if ($this->params['layout_to_load'] == 'pin_layout')
{
	echo $this->loadTemplate('pin');
}
else
{
	echo $this->loadTemplate('blog');
}
