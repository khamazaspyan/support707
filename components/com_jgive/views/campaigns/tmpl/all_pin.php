<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

$displayData['campData'] = $this->data;
$displayData['pagination'] = $this->pagination;
$displayData['params'] = $this->params;
$displayData['otherData'] = $this->otherData;
$displayData['campTypeOption'] = $this->campaign_type_filter_options;
$displayData['categories'] = $this->cat_options;
$displayData['org_ind_type'] = $this->filter_org_ind_type;
$displayData['sort_by'] = $this->ordering_options;
$displayData['orderDir'] = $this->ordering_direction_options;
$displayData['list'] = $this->lists;

jimport('joomla.html.pagination');
?>
<div class="tjBs3">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-3">
				<ul class="list-inline">
					<li><h5><strong><?php echo strtoupper(JText::_('COM_JGIVE_ALL_CAMPAIGNS'));?></strong></h5></li>
				</ul>
			</div>
			<div class="col-xs-12 col-md-9 ">
				<form action="" method="post" name="campaignsform" id="campaignsform">
					<ul class="pull-right list-inline">
						<?php
							$launch_camp_url = JUri::root() . substr(
							JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&Itemid=' . $this->otherData['createCampItemid']),
							strlen(JUri::base(true)) + 1
							);?>
						<li>
							<a href="<?php echo $launch_camp_url;?>" title="<?php echo JText::_('COM_JGIVE_CREATE_NEW_CAMPAIGN')?>">
								<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
								<?php echo JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_LAUNCH_CAMPAIGN');?>
							</a>
						</li>
						<li>|</li>
						<?php
						if ($this->params['show_search_filter'])
						{
						?>
							<li>
								<form action="" method="post" name="adminForm4" id="adminForm4">
									<a id="searchCampBtn" href="javascript:void(0)" onclick="jgive.searchFilter()" title="<?php echo JText::_('COM_JGIVE_ENTER_CAMPAIGN_NAME')?>">
										<i class="fa fa-search" ></i>
									</a>
									<span class="pull-left searchDonor" id="SearchFilterInputBox">
										<input type="text"
											placeholder="<?php echo JText::_('COM_JGIVE_ENTER_CAMPAIGN_NAME'); ?>"
											name="search_list"
											id="search_list"
											value="<?php echo $this->lists['search_list'];?>"
											class="form-control col-xs-5"
											onchange="this.form.submit();"/>
										<button
											type="button"
											onclick="this.form.submit();"
											class="btn btn-mini col-xs-1"
											data-original-title="Clear"
											title="<?php echo JText::_('COM_JGIVE_SEARCH_TOOLTIP');?>">
											<i class="fa fa-search" ></i>
										</button>
										<button
											onclick="document.getElementById('search_list').value='';this.form.submit();"
											type="button" class="btn btn-mini col-xs-2"
											id="searchCampClear"
											data-original-title="Clear"
											title="<?php echo JText::_('COM_JGIVE_CLEAR_TOOLTIP');?>">
											<i class="fa fa-close"></i>
										</button>
									</span>
									<input type="hidden" name="option" value="com_jgive" />
									<input type="hidden" name="view" value="campaigns" />
									<input type="hidden" name="layout" value="all" />
								</form>
							</li>
							<li>|</li>
						<?php
						}

						if ($this->params['show_filters'])
						{
							if ($this->params['show_sorting_options'])
							{
							?>
							<li>
								<div class="dropdown">
									<a class="dropdown-toggle" type="" data-toggle="dropdown" title="<?php echo JText::_('COM_JGIVE_ORDERING_OPTIONS');?>"><i class="fa fa-sort" aria-hidden="true"></i></a>
									<?php
										echo JHtml::_('select.genericlist', $this->ordering_options, "filter_order", ' size="1"
										onchange="this.form.submit();"
										class="dropdown-menu form-control" name="filter_order"',"value", "text", $this->lists['filter_order']);
									?>
								</div>
							</li>
							<li>|</li>
						<?php
							}
						?>
							<li>
								<a id="displayFilter" href="javascript:void(0)" onclick="jgive.campaigns.filterCamp()" title="<?php echo JText::_('COM_JGIVE_FILTER_CAMPAIGN');?>">
									<i class="fa fa-filter"></i>
								</a>
							</li>
							<li>|</li>
						<?php
						}
						?>
						<li><?php echo $this->pagination->getLimitBox(); ?></li>
					</ul>
					<input type="hidden" name="option" value="com_jgive">
					<input type="hidden" name="view" value="campaigns">
					<input type="hidden" name="layout" value="all">
				</form>
			</div>
			<!-- Filters Div-->
			<div class="col-xs-12 searchDonor" id="displayFilterText">
				<?php
					$jgiveLayout = new JLayoutFile('campaigns.filters');
					echo $jgiveLayout->render($displayData);
				?>
				<div class="clearfix">&nbsp;</div>
			</div>
		</div>

		<?php
		if (empty($this->data))
		{?>
			<div class="alert alert-warning">
				<?php echo JText::_('COM_JGIVE_NO_CAMPAIGN_FOUND');?>
			</div>
		<?php
		}
		else
		{
		?>
			<div class="row">
				<?php
					$pin_width = $this->params['pin_width'] ? $this->params['pin_width'] : 275;
					$pin_padding = $this->params['pin_padding'] ? $this->params['pin_padding'] : 5;
					$otherData = $this->otherData;

					foreach ($this->data as $displayData)
					{
					?>
						<style type="text/css">
							.jgive_pin_item { width: <?php echo $pin_width . 'px'; ?> !important; margin-right: <?php echo $pin_padding . 'px'; ?> !important; }
						</style>
						<div class="col-sm-3 col-xs-12 jgive_pin_item">
							<?php
								$displayData['otherData'] = $otherData;
								$jgiveLayout = new JLayoutFile('campaigns.pin');
								echo $jgiveLayout->render($displayData);
							?>
						</div>
					<?php
					}
				?>
			</div>
			<div>
				<div class="pull-right">
					<?php echo $this->pagination->getListFooter(); ?>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>
