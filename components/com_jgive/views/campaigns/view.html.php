<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');
jimport('joomla.html.pagination');

/**
 * Jgive campaign view Controller
 *
 * @package     Jgive
 * @subpackage  Jgive controller
 * @since       1.0
 */
class JgiveViewCampaigns extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		$app = JFactory::getApplication();

		// Take option value
		$com_jgive_option = $mainframe->input->get('option');

		// This is frontend
		$this->issite = 1;

		// Helper object declaration
		$this->campaignHelper = new campaignHelper;
		$this->jgiveFrontendHelper	= new jgiveFrontendHelper;

		// Get component params
		$state = $this->get('State');
		$this->params = $state->params;

		// Get logged in user id
		$user = JFactory::getUser();
		$this->logged_userid = $user->id;

		$layout = JFactory::getApplication()->input->get('layout', 'all');

		if ($layout == 'my')
		{
			if (!$this->logged_userid)
			{
				$msg	= JText::_('COM_JGIVE_LOGIN_MSG');
				$uri	= JFactory::getApplication()->input->get('REQUEST_URI', '', 'server', 'string');
				$url	= base64_encode($uri);
				$mainframe->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url), $msg);
			}
		}

		$this->jomsocailToolbarHtml 	= $this->jgiveFrontendHelper->jomsocailToolbarHtml();

		// Get itemids
		$menu = $app->getMenu();

		$activeMenu = $menu->getActive();

		if (!empty($activeMenu))
		{
			$menuItemId = $activeMenu->id;
		}

		$this->singleCampaignItemid = !empty($menuItemId)?$menuItemId:'';
		$this->allCampaignsItemid = !empty($menuItemId)?$menuItemId:'';

		// Get some data from the models
		$this->currency_code = $this->params->get('currency');

		$this->user_filter_options 			= $this->get('UserFilterOptions');
		$this->ordering_options 			= $this->get('OrderingOptions');
		$this->ordering_direction_options 	= $this->get('OrderingDirectionOptions');

		$this->filter_org_ind_type 			= $this->campaignHelper->organization_individual_type();
		$this->campaigns_to_show			= $this->campaignHelper->campaignsToShowOptions();

		if ($this->params->get('show_place_filter'))
		{
			$this->countries_filter = $this->jgiveFrontendHelper->getCountries();

			// For countries
			$countryarray = array();
			$countryarray[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_SELONE_COUNTRY'));
			$campaign_countries = $this->get('countries');

			foreach ($campaign_countries  as $tmp)
			{
				$value  = $tmp->country_id;
				$option = $tmp->country;
				$countryarray[] = JHtml::_('select.option', $value, $option);
			}

			$this->countryoption = $countryarray;

			// For state
			$statearray   = array();
			$statearray[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_SELECT_STATE'));

			// Get states
			$campaign_states = $this->get('CampaignStates');

			if (isset($campaign_states))
			{
				foreach ($campaign_states  as $tmp)
				{
					$value        = $tmp->id;
					$option       = $tmp->region;
					$statearray[] = JHtml::_('select.option', $value, $option);
				}
			}

			$this->campaign_states = $statearray;

			// For city
			$cityarray   = array();
			$cityarray[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_SELECT_CITY'));

			// Get city
			$campaign_city = $this->get('CampaignCity');

			if (isset($campaign_city))
			{
				foreach ($campaign_city  as $tmp)
				{
					if ($tmp->id )
					{
						$value       = $tmp->id;
						$option      = $tmp->city;
						$cityarray[] = JHtml::_('select.option', $value, $option);
					}
					elseif (empty($tmp->id) && $tmp->othercity)
					{
						$value       = $tmp->othercity;
						$option      = $tmp->othercity;
						$cityarray[] = JHtml::_('select.option', $value, $option);
					}
				}
			}

			$this->campaign_city = $cityarray;
		}

		$sort_by = $this->params->get('default_sort_by_option');
		$this->lists['filter_order'] = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_order", 'filter_order', $sort_by, 'string');

		$orderDir = $this->params->get('filter_order_Dir');
		$orderDir = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_order_Dir", 'filter_order_Dir', $orderDir, 'string');

		$this->lists['filter_order_Dir'] = $orderDir;

		if ($this->params->get('show_search_filter'))
		{
			$this->lists['search_list'] = $mainframe->getUserStateFromRequest("$com_jgive_option.search_list", 'search_list', '', 'string');
		}

		if ($this->params->get('show_filters') == 1 && $this->params->get('show_promoter_filter') == 1)
		{
			$this->lists['filter_user'] = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_user", 'filter_user', '', 'INT');
		}
		elseif (JFactory::getApplication()->input->get('filter_user'))
		{
			$this->lists['filter_user'] = JFactory::getApplication()->input->get('filter_user');
		}

		$this->lists['filter_org_ind_type'] = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_org_ind_type", 'filter_org_ind_type');

		$this->lists['filter_org_ind_type_my'] = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_org_ind_type_my", 'filter_org_ind_type_my');

		if ($this->params->get('show_place_filter'))
		{
			$this->lists['campaign_countries'] = $mainframe->getUserStateFromRequest("$com_jgive_option.campaign_countries", 'campaign_countries');
			$this->lists['campaign_states'] = $mainframe->getUserStateFromRequest("$com_jgive_option.campaign_states", 'campaign_states');
			$this->lists['campaign_city'] = $mainframe->getUserStateFromRequest("$com_jgive_option.campaign_city", 'campaign_city');
		}

		$this->lists['filter_campaign_type'] = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_campaign_type", 'filter_campaign_type');

		$Catid = $this->params->get('defualtCatid');
		$filter_campaign_cat = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_campaign_cat", 'filter_campaign_cat', $Catid, 'INT');

		$this->lists['filter_campaign_cat'] = $filter_campaign_cat;

		// Default value O - Will show 'ongoing' campaigns by default.
		$this->lists['campaigns_to_show'] = $mainframe->getUserStateFromRequest("$com_jgive_option.campaigns_to_show", 'campaigns_to_show', '');

		$this->lists['start_date'] = $mainframe->getUserStateFromRequest("$com_jgive_option.start_date",  'start_date', '', 'string');

		$this->lists['end_date'] = $mainframe->getUserStateFromRequest("$com_jgive_option.end_date", 'end_date', '', 'string');

		// Get data from the model
		$this->data 		= $this->get('Data');
		$this->pagination 	= $this->get('Pagination');

		$this->campaign_type_filter_options = $this->get('CampaignTypeFilterOptions');
		$this->cat_options = $this->campaignHelper->getCampaignsCategories();

		$this->otherData['createCampItemid'] = $this->jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=create');
		$this->otherData['myCampaignsItemid'] = $this->jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');
		$this->otherData['singleCampaignItemid'] = $this->jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');
		$this->otherData['allCampaignsItemid'] = $this->jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');

		$displayData['campData'] = $this->data;
		$displayData['pagination'] = $this->pagination;
		$displayData['params'] = $this->params;
		$displayData['otherData'] = $this->otherData;
		$displayData['campTypeOption'] = $this->campaign_type_filter_options;
		$displayData['categories'] = $this->cat_options;
		$displayData['org_ind_type'] = $this->filter_org_ind_type;
		$displayData['list'] = $this->lists;

		// Default layout is all
		$this->setLayout($layout);

		parent::display($tpl);
	}
}
