<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * jgiveViewReports view class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveViewReports extends JViewLegacy
{
	/**
	 * Function dispaly
	 *
	 * @param   string  $tpl  The name of the template file to parse.
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function display($tpl = null)
	{
		global $mainframe, $option;
		$mainframe = JFactory::getApplication();
		$option    = JFactory::getApplication()->input->get('option');

		$jgiveFrontendHelper        = new jgiveFrontendHelper;
		$this->jomsocailToolbarHtml = $jgiveFrontendHelper->jomsocailToolbarHtml();

		// Get params
		$params              = JComponentHelper::getParams('com_jgive');
		$this->currency_code = $params->get('currency');

		$this->issite = 1;

		// Default layout is default
		$layout = JFactory::getApplication()->input->get('layout', 'mypayouts');
		$this->setLayout($layout);

		// Get logged in user id
		$user                = JFactory::getUser();
		$this->logged_userid = $user->id;

		// Load language file for component backend
		$lang = JFactory::getLanguage();
		$lang->load('com_jgive', JPATH_ADMINISTRATOR);

		if ($layout == 'mypayouts')
		{
			if (!$this->logged_userid)
			{
				$msg = JText::_('COM_JGIVE_LOGIN_MSG');
				$uri = JFactory::getApplication()->input->get('REQUEST_URI', '', 'server', 'string');
				$url = base64_encode($uri);
				$mainframe->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url), $msg);
			}

			$payouts       = $this->get('Payouts');
			$this->payouts = $payouts;

			$filter_order_Dir = $mainframe->getUserStateFromRequest('com_jgive.filter_order_Dir', 'filter_order_Dir', 'desc', 'word');
			$filter_type      = $mainframe->getUserStateFromRequest('com_jgive.filter_order', 'filter_order', 'goal_amount', 'string');

			$lists['order_Dir'] = $filter_order_Dir;
			$lists['order']     = $filter_type;
			$this->lists        = $lists;

			$total       = $this->get('Total');
			$this->total = $total;

			$pagination       = $this->get('Pagination');
			$this->pagination = $pagination;
		}

		parent::display($tpl);
	}
}