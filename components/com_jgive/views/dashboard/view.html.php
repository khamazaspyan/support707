<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * Dashboard view class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveViewDashboard extends JViewLegacy
{
	/**
	 * Function dispaly
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   1.8
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();

		// Take option value
		$com_jgive_option = $app->input->get('option');

		$user = JFactory::getUser();
		$this->logged_userid = $user->id;

		$plgDatat = JPluginHelper::importPlugin('system');
		$dispatcher = JDispatcher::getInstance();
		$result = $dispatcher->trigger('getActivityScript', array('campaignfeed'));

		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$reportsHelper        = new reportsHelper;
		$campaignHelper = new campaignHelper;

		// Check login status
		if (!$this->logged_userid)
		{
			$msg = JText::_('COM_JGIVE_LOGIN_MSG');
			$uri = $_SERVER["REQUEST_URI"];
			$url = base64_encode($uri);
			$app->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url, false), $msg);
		}

		/** JGive Version 2.0 code written here **/
		$totalPaidAmount      = $reportsHelper->getTotalPaidOutAmount($this->logged_userid);
		$totalAmount2BPaidOut = $reportsHelper->getTotalAmount2BPaidOut($this->logged_userid);

		// User Received Amount
		$this->received_amount = $diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($totalPaidAmount);
		$balanceamt1           = $totalAmount2BPaidOut - $totalPaidAmount;
		$balanceamt            = number_format($balanceamt1, 2, '.', '');

		// Campaign Promoter Pending Payout Amount
		if ($balanceamt == '-0.00')
		{
			$balanceamt          = 0;
			$this->pending_amount = $diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($balanceamt);
		}
		else
		{
			$this->pending_amount = $diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($balanceamt1);
		}

		// Get Top Donors List
		$model           = $this->getModel();

		$this->searchMyCamp = $app->getUserStateFromRequest("$com_jgive_option.searchMyCamp", 'searchMyCamp', '', 'string');
		$this->dashboard_filters = $app->getUserStateFromRequest("$com_jgive_option.dashboard_filters", 'dashboard_filters', '', 'integer');

		$this->promoterDashboardData['filterData'] = new stdClass;
		$this->promoterDashboardData['filterData']->searchMyCamp = $this->searchMyCamp;
		$this->promoterDashboardData['filterData']->dashboard_filters = $this->dashboard_filters;
		$this->promoterDashboardData['dashboardFilterOption'] = $model->dashboardDropDownOption();
		$this->promoterDashboardData['payoutData'] = new stdClass;
		$this->promoterDashboardData['activityData'] = $this->get('CampaignIds');
		$this->promoterDashboardData['payoutData']->pendingAmt = $this->pending_amount;
		$this->promoterDashboardData['payoutData']->receivedAmt = $this->received_amount;
		$this->promoterDashboardData['myCampData'] = $model->getMyCampaign();
		$this->promoterDashboardData['topDonorsData'] = $model->getDonorsDetails();

		$this->promoterDashboardData['otherData'] = new stdClass;
		$this->promoterDashboardData['otherData']->logged_userid = $this->logged_userid;
		$this->promoterDashboardData['otherData']->createCampaignItemid = $jgiveFrontendHelper->getItemId(
		'index.php?option=com_jgive&view=campaign&layout=create'
		);
		$this->promoterDashboardData['otherData']->myCampaignItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my'
		);
		$this->promoterDashboardData['otherData']->donorsItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donors');
		$this->promoterDashboardData['otherData']->singleCampItemid = $jgiveFrontendHelper->getItemId(
		'index.php?option=com_jgive&view=campaign&layout=single'
		);
		$this->promoterDashboardData['params'] = JComponentHelper::getParams('com_jgive');
		$this->promoterDashboardData['categories'] = $campaignHelper->getCampaignsCategories();
		$this->promoterDashboardData['filterCatList'] = $app->input->get('cat');
		$this->promoterDashboardData['filterCampStatus'] = $app->input->get('campStatus');
		$this->promoterDashboardData['campaignType'] = $campaignHelper->getCampaignTypeFilterOptions();
		$this->promoterDashboardData['filterCampType'] = $app->input->get('campType');
		$this->promoterDashboardData['organizationType'] = $campaignHelper->organization_individual_type();
		$this->promoterDashboardData['filterOrgTypeList'] = $app->input->get('orgType');
		$this->promoterDashboardData['language'] = new stdClass;
		$this->promoterDashboardData['language']->lang = JFactory::getLanguage()->getTag();

		// Check here user is a Super User or registered user
		$this->isroot = $user->authorise('core.admin');

		parent::display($tpl);
	}
}
