<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

$document=JFactory::getDocument();
$document->addStyleSheet(JUri::root(true).'/media/com_jgive/css/jgive_bs3.css');
?>
<form action="" name="dashboardFilterform" method="post" id="dashboardFilterform">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<p class="text-muted"><?php echo JText::_('COM_JGIVE_FILTERS');?></p>
		</div>
		<div class="col-xs-12 col-sm-3 dashboardCatFilter">
			<p class="text-muted"><?php echo JText::_('COM_JGIVE_CATEGORY');?></p>
			<?php
			if($this->promoterDashboardData['filterCatList'] == null)
			{
				$checkVal = "checked";
			}
			else
			{
				$checkVal = "";
			}?>
			<div class="checkbox">
				<label>
					<input type="radio" name="cat" value=""
					<?php echo $checkVal;?>>
					<?php echo JText::_('COM_JGIVE_ALL');?>
				</label>
			</div>
			<?php
			foreach ($this->promoterDashboardData['categories'] as $category)
			{
				if ($this->promoterDashboardData['filterCatList'] == $category->value)
				{
					$check = "checked";
				}
				else
				{
					$check = "";
				}
			?>
				<div class="checkbox">
					<label>
						<input type="radio" name="cat" value="<?php echo $category->value;?>"
						<?php echo $check;?>>
						<?php echo $category->text;?>
					</label>
				</div>
			<?php
			}
			?>
		</div>

		<div class="col-xs-12 col-sm-3">
			<p class="text-muted"><?php echo JText::_('COM_JGIVE_DONATION_STATUS');?></p>
			<?php

				if (isset($this->promoterDashboardData['filterCampStatus']) && $this->promoterDashboardData['filterCampStatus'] != null)
				{
					if ($this->promoterDashboardData['filterCampStatus'] == 1)
					{
						$checkpublish = "checked";
						$checkUnpublish = "";
					}
					elseif ($this->promoterDashboardData['filterCampStatus'] == 0)
					{
						$checkpublish = "";
						$checkUnpublish = "checked";
					}

					$checkall = "";
				}
				else
				{
					$checkall = "checked";
					$checkpublish = "";
					$checkUnpublish = "";
				}

			?>
			<div class="checkbox">
				<label>
					<input type="radio" name="campStatus" value="" <?php echo $checkall;?>>
					<?php echo JText::_('COM_JGIVE_ALL');?>
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="radio" name="campStatus" value="1" <?php echo $checkpublish;?>>
					<?php echo JText::_('COM_JGIVE_PUBLISHED');?>
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="radio" name="campStatus" value="0" <?php echo $checkUnpublish;?>>
					<?php echo JText::_('COM_JGIVE_UNPUBLISHED');?>
				</label>
			</div>
		</div>

		<?php
		if (count($this->promoterDashboardData['params']['camp_type']) == 2)
		{
		?>
			<div class="col-xs-12 col-sm-3">
				<p class="text-muted"><?php echo JText::_('COM_JGIVE_CAMP_TYPE');?></p>
				<?php
				if ($this->promoterDashboardData['filterCampType'] == null)
				{
					$checkVal = "checked";
				}
				else
				{
					$checkVal = "";
				}?>

				<div class="checkbox">
					<label>
						<input type="radio" name="campType" value="" <?php echo $checkVal;?>>
						<?php echo JText::_('COM_JGIVE_ALL');?>
					</label>
				</div>
				<?php
				foreach ($this->promoterDashboardData['campaignType'] as $campTypearr)
				{
					if ($this->promoterDashboardData['filterCampType'] == $campTypearr->value)
					{
						$check = "checked";
					}
					else
					{
						$check = "";
					}
					?>
					<div class="checkbox">
						<label>
							<input type="radio" name="campType" value="<?php echo $campTypearr->value;?>"
							<?php echo $check;?> />
							<?php echo $campTypearr->text; ?>
						</label>
					</div>
				<?php
				}
				?>
			</div>
		<?php
		}?>

		<div class="col-xs-12 col-sm-3">
			<p class="text-muted"><?php echo JText::_('COM_JGIVE_ORG_IND_TYPE');?></p>

			<?php
			foreach ($this->promoterDashboardData['organizationType'] as $organizationType)
			{
				if ($this->promoterDashboardData['filterOrgTypeList'] == $organizationType->value)
				{
					$check = "checked";
				}
				else
				{
					$check = "";
				}
			?>
				<div class="checkbox">
					<label>
						<input type="radio" name="orgType" value="<?php echo $organizationType->value;?>"
						<?php echo $check;?>/>
						<?php echo $organizationType->text; ?>
					</label>
				</div>
			<?php
			}
			?>
		</div>
		<div class="col-xs-12 col-sm-12">
			<ul class="pull-right list-inline">
				<li>
					<a class="pull-right" onclick="jgive.dashboard.clearFilter()" href="javascript:void(0);">
						<i class="fa fa-repeat" aria-hidden="true"></i>
						<?php echo JText::_('COM_JGIVE_REST_FILTERS');?>
					</a>
				</li>
				<li>
					<button type="button" onclick="document.dashboardFilterform.submit();" class="btn btn-mini btn-primary pull-right">
						<?php echo JText::_('COM_JGIVE_DASHBOARD_APPLY_FILTER'); ?>
					</button>
				</li>
			</ul>
		</div>
	</div>
</form>
