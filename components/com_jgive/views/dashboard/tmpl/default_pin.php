<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

$jgiveFrontendHelper = new JgiveFrontendHelper;

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');

foreach ($this->promoterDashboardData['myCampData'] as $campData)
{
?>
	<div class="col-xs-12 col-md-4 col-lg-4">
		<div class="thumbnail">
			<div class="caption">
				<h5 class="text-uppercase">
					<?php
						$campaignDetailUrl = JUri::root() . substr(
						JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $campData['id'] . '&Itemid=' .
						$this->promoterDashboardData['otherData']->singleCampItemid
						),
						strlen(JUri::base(true)) + 1
						);
					?>
					<?php
					if (strlen($campData['title']) > 25)
					{?>
						<a href="<?php echo  $campaignDetailUrl;?>" title="<?php echo $campData['title'];?>">
							<strong><?php echo strtoupper(substr($campData['title'], 0, 25) . '...');?></strong>
						</a>
					<?php
					}
					else
					{?>
						<a href="<?php echo  $campaignDetailUrl;?>" title="<?php echo $campData['title'];?>">
							<strong><?php echo strtoupper($campData['title']);?></strong>
						</a>
					<?php
					}?>

					<?php
					if ($campData['featured'])
					{
					?>
						<span><i class="fa fa-star pull-right" aria-hidden="true"></i></span>
					<?php
					}
					?>
				</h5>
				<h6 class="text-uppercase dashboard_pin">
					<?php
						if ($campData['published'] == 1)
						{
						?>
							<i class="fa fa-check" aria-hidden="true"></i>
						<?php
							echo JText::_('COM_JGIVE_VENDOR_CAMPAIGN_PUBLISHED_STATUS') .
							JFactory::getDate($campData['start_date'])->Format(JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));
						}
						else
						{
						?>
							<i class="fa fa-floppy-o" aria-hidden="true"></i>
						<?php
							echo JText::_('COM_JGIVE_VENDOR_CAMPAIGN_UNPUBLISHED_STATUS');
						}
					?>
					<span>
							<a href="<?php echo  $campaignDetailUrl;?>"
							title="<?php echo $campData['title'];?>">
								<i class="fa fa-chevron-right pull-right" aria-hidden="true"></i>
							</a>
						</span>
				</h6>
				<h6 class="col-xs-offset-1">
					<?php echo JText::_('COM_JGIVE_CAMP_TYPE') . ':';?>
					<?php echo ($campData['type'] == 'donation') ? JText::_('COM_JGIVE_DONATION') : JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT');?>
				</h6>
				<div class="clearfix"></div>
				<?php
					// Calculate days left
						$date_expire = 0;
						$curr_date = JFactory::getDate()->Format(JText::_('Y-m-d'));
						$end_date  = JFactory::getDate($campData['end_date'])->Format(JText::_('Y-m-d'));

						if ($curr_date > $end_date)
						{
							$date_expire = 1;
						}

						$time_curr_date  = strtotime($curr_date);
						$time_end_date   = strtotime($campData['end_date']);
						$interval        = ($time_end_date - $time_curr_date);
						$days_left = floor($interval / (60 * 60 * 24));
						$days = '';
						$lable = JText::_('COM_JGIVE_DAYS_LEFT');

						if ((int) ($time_curr_date) && (int) ($time_end_date))
						{
							$days = JText::_('COM_JGIVE_DAYS_LEFT');
						}

						if ($date_expire)
						{
							$days = JText::_('COM_JGIVE_NA');
						}
						elseif ((int) ($time_curr_date) && (int) ($time_end_date))
						{
							if ((int) $days_left == 0)
							{
								// Only one day left
								$days = 1;
							}
							else
							{
								$days = $days_left > 0 ?  $days_left: JText::_('COM_JGIVE_NA');
							}
						}
					?>
				<h6 class="col-xs-offset-1">
					<?php
					if ($days === "No")
					{
						echo JText::_('COM_JGIVE_CAMPAIGN_CLOSED');
					}
					else
					{
					?>
						<?php echo $days;?>&nbsp;
						<?php echo JText::_('COM_JGIVE_LAYOUT_DAYS'); ?>&nbsp;
						<?php echo JText::_('COM_JGIVE_DAYS__REMAINING');
					}
					?>
				</h6>

				<div class="col-sm-10 col-sm-offset-1 col-lg-offset-0 col-md-offset-0">

					<?php
					// Calculate days left
						$date_expire = 0;
						$curr_date = JFactory::getDate()->Format(JText::_('Y-m-d'));
						$end_date  = JFactory::getDate($campData['end_date'])->Format(JText::_('Y-m-d'));

						if ($curr_date > $end_date)
						{
							$date_expire = 1;
						}

						$time_curr_date  = strtotime($curr_date);
						$time_end_date   = strtotime($campData['end_date']);
						$interval        = ($time_end_date - $time_curr_date);
						$days_left = floor($interval / (60 * 60 * 24));
						$days = '';
						$lable = JText::_('COM_JGIVE_DAYS_LEFT');

						if ((int) ($time_curr_date) && (int) ($time_end_date))
						{
							$days = JText::_('COM_JGIVE_DAYS_LEFT');
						}

						if ($date_expire)
						{
							$days = JText::_('COM_JGIVE_NA');
						}
						elseif ((int) ($time_curr_date) && (int) ($time_end_date))
						{
							if ((int) $days_left == 0)
							{
								// Only one day left
								$days = 1;
							}
							else
							{
								$days = $days_left > 0 ?  $days_left: JText::_('COM_JGIVE_NA');
							}
						}
					?>
					<!-- Total Donation-->
					<dl>
						<dt>
							<h6>
								<strong>
									<?php echo $jgiveFrontendHelper->getFormattedPrice($campData['amount_received']) . ' / ' . $jgiveFrontendHelper->getFormattedPrice($campData['goal_amount']);?>
								</strong>
							</h6>
						</dt>
						<dd>
							<?php
								$donated_per = 0;

								$goal_amount = (float) $campData['goal_amount'];

								if (!empty($campData['amount_received']) && $goal_amount > 0)
								{
									$donated_per = ($campData['amount_received'] / $campData['goal_amount']) * 100;
								}

								$donated_per = number_format((float) $donated_per, 2, '.', '');
							?>

							<div class="progress">
								<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="50" style="width: <?php echo $donated_per . '%';?>">
								</div>
							</div>
						</dd>
						<dd>
							<h6 class="dashboard_pin">
								<?php echo ($campData['type'] == 'donation') ? JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_TOTAL_DONATION') . ' / ' . JText::_('COM_JGIVE_GOAL_AMOUNT'): JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_TOTAL_INVESTMENT') . ' / ' . JText::_('COM_JGIVE_GOAL_AMOUNT');?>
							</h6>
						</dd>
					</dl>
					<!--Average Donation-->
					<dl>
						<?php
							require_once JPATH_SITE . "/components/com_jgive/models/campaigns.php";
							$jgiveModelCampaigns = new JgiveModelCampaigns;
							$totalNoOfDonorsPerCamp = $jgiveModelCampaigns->getDonorsCountPerCamp($campData['id']);

							// Average Donation or Investment
							if ($totalNoOfDonorsPerCamp > 0)
							{
								$avg_amount = $campData['amount_received'] / $totalNoOfDonorsPerCamp;
							}
							else
							{
								$avg_amount = 0;
							}
						?>

						<dt><h4><strong><?php echo $jgiveFrontendHelper->getFormattedPrice($avg_amount);?></strong></h4></dt>
						<dd><h6 class="dashboard_pin"><?php echo ($campData['type'] == 'donation') ? JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_AVERAGE_DONATION') : JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_AVERAGE_INVESTMENT');?></h6></dd>
					</dl>
					<!-- No. of Donors-->
					<dl>
						<dt><h4 class="dashboard_pin"><strong><?php echo $totalNoOfDonorsPerCamp;?></strong></h4></dt>
						<dd><h6 class="dashboard_pin"><?php echo ($campData['type'] == 'donation') ? JText::_('COM_JGIVE_DONORS') : JText::_('COM_JGIVE_INVESTORS');?></h6></dd>
					</dl>

				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>
<?php
}
