<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;
$limit = 4;

$videoTheme = $imageTheme = $givebackTheme = "";

if ($this->promoterDashboardData['params']['video_gallery'])
{
	// If video uploading configuration on then only show the video activities
	$videoTheme = "," . "'campaign.addvideo'";
}

if ($this->promoterDashboardData['params']['img_gallery'])
{
	// If image uploading configuration on then only show the image activities
	$imageTheme = "," . "'campaign.addimage'";
}

if ($this->promoterDashboardData['params']['show_selected_fields'] == 1)
{
	if (!in_array("give_back", $this->cdata['params']['creatorfield']))
	{
		$givebackTheme = "," . "'campaign.addgiveback'";
	}
}
else
{
	$givebackTheme = "," . "'campaign.addgiveback'";
}


foreach ($this->promoterDashboardData['activityData'] as $activity)
{
	$campIds[] = "'" . $activity['id'] . "'";
}

if (isset($campIds))
{
$campIdstr = implode(',', $campIds);
?>
<div class="col-xs-12 col-sm-6 col-md-3 activity">
	<h6 class="text-uppercase"><strong><?php echo strtoupper(JText::_("COM_JGIVE_VENDOR_CAMPAIGNS_ACTIVITY"));?></strong></h6>
	<div id="tj-activitystream" tj-activitystream-widget tj-activitystream-theme="dashboardfeed" tj-activitystream-bs="bs3" 
	tj-activitystream-type="'campaign.extended','jgive.donation','campaign.completed','jgive.addcampaign', 'jgive.textpost'<?php echo $givebackTheme . $videoTheme . $imageTheme;?>" 
	tj-activitystream-target-id="<?php echo $campIdstr;?>" tj-activitystream-limit="<?php echo $limit;?>" tj-activitystream-language="<?php echo $this->promoterDashboardData['language']->lang;?>">
	</div>
</div>
<?php
}?>
