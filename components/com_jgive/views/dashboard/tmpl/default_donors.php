<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
$jgiveFrontendHelper = new jgiveFrontendHelper;
?>
<?php
if (!empty($this->promoterDashboardData['topDonorsData']))
{?>
<!----Top Donars----->
<div class="col-xs-12 col-sm-6 col-md-3">
	<h6 class="text-uppercase"><strong><?php echo strtoupper(JText::_("COM_JGIVE_DASHBOARD_TOP_5_DONORS"));?></strong>
		<?php
		if (count($this->promoterDashboardData['topDonorsData']) > 4)
		{
			$all_donors_list_path = JURI::root() . substr(JRoute::_('index.php?option=com_jgive&view=donors&Itemid=' . $this->promoterDashboardData['otherData']->donorsItemid), strlen(JUri::base(true)) + 1);
		?>
			<span class="pull-right"><a href="<?php echo $all_donors_list_path;?>">View All</a></span>
		<?php
		}
		?>
	</h6>
	<?php
	if (!empty($this->promoterDashboardData['topDonorsData']))
	{
	?>
		<table class="table">
			<?php
			$i = 1;

			foreach ($this->promoterDashboardData['topDonorsData'] as $donor)
			{
				if (!$donor->avatar)
				{
					// If no avatar, use default avatar
					$donor->avatar=JUri::root(true).'/media/com_jgive/images/default_avatar.png';
				}

				if ($donor->annonymous_donation)
				{
					$title = JText::_("COM_JGIVE_DONOR_ANNONYMOUS_NAME").' - '.$donor->donation_amount . ' ' . $this->promoterDashboardData['params']['currency'];
					$donorName = JText::_("COM_JGIVE_DONOR_ANNONYMOUS_NAME");

					// If annonymous_donation, use annonymous avatar, reset url to blank
					$donor->avatar=JUri::root(true).'/media/com_jgive/images/annonymous.png';
					$donor->profile_url='#';
				}
				else
				{
					$title = $donor->first_name.' '. $donor->last_name . ' - ' . $donor->donation_amount . ' ' . $this->promoterDashboardData['params']['currency'];
					$donorName = $donor->first_name.' '. $donor->last_name;
				}
				?>
				<tr>
					<td>
						<?php
						if(!empty($donor->profile_url) && $donor->user_id!=0)
						{
						?>
							<a href="<?php echo $donor->profile_url; ?>">
								<img src="<?php echo $donor->avatar; ?>" class="img-circle" alt="<?php echo $title;?>" title="<?php echo $title;?>" width="100%"/>
							</a>
						<?php
						}
						else
						{
						?>
							<img src="<?php echo $donor->avatar; ?>" class="img-circle" alt="<?php echo $title;?>" title="<?php echo $title;?>" width="100%"/>
						<?php
						}?>
					</td>
					<td class="text-muted"><?php echo $donorName; ?></td>
					<td><span class="pull-right"><?php echo $jgiveFrontendHelper->getFormattedPrice($donor->donation_amount)?></span></td>
				</tr>
				<?php
				// Show only first 4 top donors
				if ($i++ == 4)
				{
					break;
				}
			}
		?>
		</table>
		<hr>
	<?php
	}
	?>
</div>
<?php
}

