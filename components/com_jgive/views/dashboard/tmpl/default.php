<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2016 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// Creating Object of FrontendHelper class
$jgiveFrontendHelper = new jgiveFrontendHelper;
$campaignHelper = new campaignHelper;

// Set Title by campaign name
$document = JFactory::getDocument();

// Load Chart Javascript Files.
$document->addScript(JUri::root(true) . '/media/com_jgive/vendors/js/Chart.js');
$document->addScript(JUri::root(true) . '/media/com_jgive/vendors/js/Chart.min.js');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');
?>
<div class="tjBs3">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1><strong><?php echo JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_OVERVIEW');?></strong></h1>
			</div>
			<?php
				echo $this->loadTemplate("graph");
				echo $this->loadTemplate("donors");
				echo $this->loadTemplate("activity");
			?>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-12 col-md-2">
				<h5><strong><?php echo strtoupper(JText::_('COM_JGIVE_MY_CAMPAIGNS'));?></strong></h5>
			</div>
			<div class="col-xs-12 col-md-10">
				<form action="" name="dashboardform" method="post" id="dashboardform">
					<ul class="pull-right list-inline">
						<?php
						$launch_camp_url = JUri::root() . substr(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&Itemid=' . $this->promoterDashboardData['otherData']->createCampaignItemid), strlen(JUri::base(true)) + 1);
						?>
						<li><a href="<?php echo $launch_camp_url;?>"><i class="fa fa-paper-plane-o" aria-hidden="true"></i><?php echo JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_LAUNCH_CAMPAIGN');?></a></li>
						<li>|</li>
						<li>
							<a id="searchCampBtn" href="#" onclick="jgive.searchFilter()"><i class="fa fa-search" ></i></a>
							<span style="display:none" class="pull-left" id="SearchFilterInputBox">
								<input type="text" name="searchMyCamp" id="searchMyCamp" value="<?php echo $this->promoterDashboardData['filterData']->searchMyCamp;?>" class="form-control col-xs-5" onchange="document.dashboardform.submit();" />
								<button type="button" onclick="this.form.submit();" class="btn btn-mini col-xs-1" data-original-title="Search" title="<?php echo JText::_('COM_JGIVE_SEARCH_TOOLTIP');?>">
									<i class="fa fa-search" ></i>
								</button>
								<button onclick="document.getElementById('searchMyCamp').value='';this.form.submit();" type="button" class="btn btn-mini col-xs-2" data-original-title="Clear" title="<?php echo JText::_('COM_JGIVE_CLEAR_TOOLTIP');?>">
									<i class="fa fa-close"></i>
								</button>
							</span>
						</li>
						<li>|</li>
						<li>
							<a id="dashboardFilterList" href="javascript:void(0)" onclick="jgive.dashboard.dashboardFilterList()"><i class="fa fa-filter"></i></a>
						</li>
						<li>|</li>
						<li>
							<?php echo JHtml::_('select.genericlist', $this->promoterDashboardData['dashboardFilterOption'], "dashboard_filters", 'class="form-control" size="1" onchange="this.form.submit();" name="dashboard_filters"',"value", "text",$this->promoterDashboardData['filterData']->dashboard_filters); ?>
						</li>
					</ul>
				</form>
			</div>
			<div class="col-xs-12" style="display: none" id="dashboardFilterOptions">
				<?php
					echo $this->loadTemplate("filters");
				?>
			</div>
			<div class="clearfix">&nbsp;</div>
			<div class="col-xs-12">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						<div class="row">
							<?php
							if (empty($this->promoterDashboardData['myCampData']))
							{
							?>
								<div class="alert alert-warning">
									<?php echo JText::_('COM_JGIVE_NO_CAMPAIGN_FOUND');?>
								</div>
							<?php
							}
							else
							{?>
								<div>
									<?php
										echo $this->loadTemplate("pin");
									?>
								</div>
							<?php
							}
							?>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="profile">...</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="user_id" name="user_id" value="<?php echo $this->promoterDashboardData['otherData']->logged_userid; ?>" />
<script type="text/javascript">
	jQuery(document).ready(function()
	{
		jgive.dashboard.onChangeGetDashboardDonationData();
	});
</script>
