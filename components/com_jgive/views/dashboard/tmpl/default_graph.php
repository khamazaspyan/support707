<?php
/**
 * @version    SVN: <svn_id>
 * @package    Jgive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
?>
<div class="col-xs-12 col-md-6">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-xs-12">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-xs-12">
					<h6>
						<strong><?php echo strtoupper(JText::_('COM_JGIVE_DONATIONS'));?></strong>
					</h6>
				</div>
				<div class="col-sm-6 col-md-6 col-xs-12">
					<select id='dashboardcampaignsOption' class="pull-right">
						<option value = '0'><?php echo JText::_('COM_JGIVE_FILTER_LATEST');?></option>
						<option value = '1'><?php echo JText::_('COM_JGIVE_FILTER_LAST_MONTH');?></option>
						<option value = '2'><?php echo JText::_('COM_JGIVE_FILTER_LAST_YEAR');?></option>
					</select>
				</div>
				<canvas id="dashboardCampaign_graph"></canvas>
			</div>
		</div>
		<div class="col-sm-12 col-md-12 col-xs-12">
			<!--Payout-->
			<ul class="list-inline">
				<li>
					<h6><strong><?php echo strtoupper(JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_PAYOUT'));?></strong></h6>
				</li>
				<li>
					<span class="text-muted"><?php echo JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_RECEIVE_AMOUNT');?></span>
					<?php echo $this->promoterDashboardData['payoutData']->receivedAmt;?>
				</li>
				<li>
					<span class="text-muted"><?php echo JText::_('COM_JGIVE_VENDOR_CAMPAIGNS_PENDING_AMOUNT');?></span>
					<?php echo $this->promoterDashboardData['payoutData']->pendingAmt;?>
				</li>
			</ul>
			<!--Payout-->
		</div>
	</div>
</div>
