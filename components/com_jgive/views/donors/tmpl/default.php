<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive-tables.css');
$jgivehelper   = new JgiveFrontendHelper;

// Menu Item Id for redirecting url
$mainframe = JFactory::getApplication();
$input = JFactory::getApplication()->input;
$itemidForDonor = $input->get('Itemid', '', 'INT');

// Check here logged user have campaigns.
if (!$this->campaignsId )
{?>
	<div class="alert alert-warning">
			<?php echo Jtext::_('COM_JGIVE_DASHBOARD_NOT_FOUND_ANY_CAMPAIGN');?>
	</div>
<?php
}
else
{
	JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
	JHtml::_('bootstrap.tooltip');
	JHtml::_('behavior.multiselect');
	JHtml::_('formbehavior.chosen', 'select');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_jgive');
$canEdit    = $user->authorise('core.edit', 'com_jgive');
$canCheckin = $user->authorise('core.manage', 'com_jgive');
$canChange  = $user->authorise('core.edit.state', 'com_jgive');
$canDelete  = $user->authorise('core.delete', 'com_jgive');

$document = JFactory::getDocument();
$document->addScript(JUri::root() . 'libraries/techjoomla/assets/js/tjexport.js');
$document->addScriptDeclaration("var csv_export_url='{$this->csv_url}';");
$document->addScriptDeclaration("var csv_export_success='{$this->messages['success']}';");
$document->addScriptDeclaration("var csv_export_error='{$this->messages['error']}';");
$document->addScriptDeclaration("var csv_export_inprogress='{$this->messages['inprogress']}';");

?>
<div class="tjBs3">
	<div id="jgive_donors" class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="page-header">
				<h2>
					<?php echo JText::_('COM_JGIVE_MY_CAMPAIGN_DONORS');?>
				</h2>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form action="<?php echo JRoute::_('index.php?option=com_jgive&view=donors'); ?>" method="post" name="adminForm" id="adminForm">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<?php echo $this->toolbarHTML;?>
					</div>

					<?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>

					<?php if (empty($this->items))
					{?>
						<div class="alert alert-no-items">
							<?php echo JText::_('COM_JGIVE_NO_MATCHING_RESULTS'); ?>
						</div>
					<?php
					}
					else
					{?>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="no-more-tables">
									<table class="table table-striped" id="donorList">
										<thead>
											<tr>
												<th width="1%" class="">
													<input type="checkbox" name="checkall-toggle" value=""
													title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>"
													onclick="Joomla.checkAll(this)" />
												</th>
												<th class="" width="5%">
													<?php echo JText::_('COM_JGIVE_NO'); ?>
												</th>
												<th class=''>
													<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DASHBOARD_DONOR_NAME', 'a.first_name', $listDirn, $listOrder); ?>
												</th>
												<th class=''>
													<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DASHBOARD_DONOR_EMAIL', 'a.email', $listDirn, $listOrder); ?>
												</th>
												<th class=''>
													<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_ADDRESS', 'a.address', $listDirn, $listOrder); ?>
												</th>
												<th class=''>
													<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_ADDRESS2', 'a.address2', $listDirn, $listOrder); ?>
												</th>
												<th class=''>
													<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_PHONE', 'a.phone', $listDirn, $listOrder); ?>
												</th>
												<th class=''>
													<?php echo JHtml::_('grid.sort',  'COM_JGIVE_CAMPAIGN_DETAILS', 'a.campaign_id', $listDirn, $listOrder); ?>
												</th>
												<th class=''>
													<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_DONATION_AMOUNT', 'o.amount', $listDirn, $listOrder); ?>
												</th>
												<th class=''>
													<?php echo JText::_('COM_JGIVE_DONORS_GIVEBACK_DESC'); ?>
												</th>
												<th class=''>
													<?php echo JHtml::_('grid.sort',  'COM_JGIVE_DONORS_CDATE', 'o.cdate', $listDirn, $listOrder); ?>
												</th>
											</tr>
										</thead>
										<tfoot>
											<tr class="hidden-phone">
												<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
													<div class="pull-right">
														<?php echo $this->pagination->getListFooter(); ?>
													</div>
												</td>
											</tr>
										</tfoot>
										<tbody>
											<?php
											$num = 1;
											foreach ($this->items as $i => $item)
											{?>
												<?php $canEdit = $user->authorise('core.edit', 'com_jgive'); ?>
												<?php if (!$canEdit && $user->authorise('core.edit.own', 'com_jgive')): ?>
														<?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
												<?php endif; ?>
												<tr class="row<?php echo $i % 2; ?>">
													<td class="center hidden-phone">
														<?php echo JHtml::_('grid.id', $i, $item->id); ?>
													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_NO"); ?>">
														<?php echo $num++; ?>
													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_DASHBOARD_DONOR_NAME"); ?>">
														<?php if (isset($item->checked_out) && $item->checked_out) { ?>
															<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'donors.', $canCheckin); ?>
														<?php }?>
														<?php echo $this->escape($item->first_name) . " " . $item->last_name; ?>
													</td>
													<td class="wordbreak" data-title="<?php echo JText::_("COM_JGIVE_DASHBOARD_DONOR_EMAIL"); ?>">
														<?php echo $item->email; ?>
													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_DONORS_ADDRESS"); ?>">
														<?php echo !empty($item->address) ? $item->address . ',</br>' : ''; ?>
														<?php echo !empty($item->city) ? $item->city . ',</br>' : ''; ?>
														<?php echo !empty($item->state) ? $item->state . ',</br>' : ''; ?>
														<?php echo !empty($item->country) ? $item->country . ',</br>' : ''; ?>
														<?php echo !empty($item->zip) ? $item->zip . ',</br>' : '-'; ?>

													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_DONORS_ADDRESS2"); ?>">
														<?php echo $item->address2 ? $item->address2 : " - "; ?>
													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_DONORS_PHONE"); ?>">
														<?php echo $item->phone ? $item->phone : " - "; ?>
													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_CAMPAIGN_DETAILS"); ?>">
														<a href="<?php echo JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $item->campaign_id . '&Itemid=' . $this->itemid, false);?>" title="<?php echo JText::_('COM_JGIVE_DASHBOARD_TOOLTIP_VIEW_ORDER_MSG');?>">
															<?php echo $item->campaign_title;?>
														</a>
													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_DONORS_DONATION_AMOUNT"); ?>">
														<?php echo $jgivehelper->getFormattedPrice($item->donation_amount);?>
													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_DONORS_GIVEBACK_DESC"); ?>">
														<?php echo $item->gdesc ? $item->gdesc : ' - ';?>
													</td>
													<td data-title="<?php echo JText::_("COM_JGIVE_DONORS_CDATE"); ?>">
														<?php echo JHtml::_('date', $item->cdate, JText::_('COM_JGIVE_DASHBOARD_DATE_FORMAT_SHOW_SHORT'));?>
													</td>
												</tr>
											<?php
											}?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					<?php
					}?>

					<input type="hidden" name="task" id="task" value="" />
					<input type="hidden" name="boxchecked" value="0" />
					<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
					<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
}?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if(task == 'donors.redirectforEmail')
		{
			if(document.adminForm.boxchecked.value == 0)
			{
				var msg = "<?php echo JText::_('COM_JGIVE_MSG_FOR_SELECT_USER'); ?>";
				alert(msg);
				return false;
			}
			else
			{
				Joomla.submitform(task);
			}
		}
		else
		{
			Joomla.submitform(task);
		}
	}
</script>
