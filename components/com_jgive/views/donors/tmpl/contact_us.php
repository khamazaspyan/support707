<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */
// No direct access
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive.css');
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive-tables.css');

// Menu Item Id for redirecting url
$mainframe = JFactory::getApplication();
$input = JFactory::getApplication()->input;
$itemid = $input->get('Itemid', '', 'INT');

?>
<div class="tjBs3">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="page-header">
				<h2>
					<?php echo JText::_('COM_JGIVE_MASS_MAIL');?>
				</h2>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<form action="" method="post" name="adminForm" id="adminForm">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-sm-2">
								<?php echo  JHtml::tooltip(
								JText::_('COM_JGIVE_ENTER_EMAIL_ID_TOOLTIP'),
								JText::_('COM_JGIVE_ENTER_EMAIL_ID'),
								'',
								JText::_('COM_JGIVE_ENTER_EMAIL_ID')) . '*'; ?></label>
							<div class="col-sm-10">
								<textarea class="form-control" id="selected_emails" name="selected_emails" readonly="true" ><?php echo implode(",", $this->selected_emails);?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">
								<?php echo  JHtml::tooltip(
								JText::_('COM_JGIVE_ENTER_EMAIL_SUBJECT_TOOLTIP'),
								JText::_('COM_JGIVE_ENTER_EMAIL_SUBJECT'),
								'',
								JText::_('COM_JGIVE_ENTER_EMAIL_SUBJECT')); ?>
							</label>
							<div class="col-sm-10">
								<input type="text" id="jgive_subject"
								name="jgive_subject"  class="form-control"
								placeholder="<?php echo  JText::_('COM_JGIVE_ENTER_EMAIL_SUBJECT') ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">
								<?php echo  JHtml::tooltip(
								JText::_('COM_JGIVE_ENTER_EMAIL_BODY_TOOLTIP'),
								JText::_('COM_JGIVE_ENTER_EMAIL_BODY'),
								'',
								JText::_('COM_JGIVE_ENTER_EMAIL_BODY')); ?>
							</label>
							<div class="col-sm-10">
								<?php
								$editor      = JFactory::getEditor();
								echo $editor->display("jgive_message", "", 670, 600, 60, 20, false);
								?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<?php echo $this->toolbarHTML;?>
						</div>
					</div>

					<input type="hidden" name="option" value="com_jgive" />
					<input type="hidden" name="task" id="task" value="" />
					<input type="hidden" name="view" value="donors" />
					<input type="hidden" name="layout" value="contact_us" />
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if(task == 'donors.emailtoSelected')
		{
			// Check if Email Subject is empty
			if(document.getElementById("jgive_subject").value == 0 && document.getElementById("jgive_message").value == 0)
			{
				// If user want to send mail without subject or text
				var msg = "<?php echo JText::_('COM_JGIVE_CONFIRM_MSG_FOR_SEND_MAIL_WITHOUT_SUB_AND_TEXT'); ?>";

				alert(msg);
				return false;
			}
			else
			{
				Joomla.submitform(task);
			}
		}
		else
		{
			Joomla.submitform(task);
		}
	}
</script>
