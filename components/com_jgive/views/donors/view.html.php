<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('techjoomla.tjtoolbar.button.csvexport');

/**
 * JgiveViewDonors view class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveViewDonors extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	protected $params;

	/**
	 * Function dispaly
	 *
	 * @param   string  $tpl  The name of the template file to parse.
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function display($tpl = null)
	{
		$app                 = JFactory::getApplication();
		$input               = $app->input;
		$this->logged_userid = JFactory::getUser()->id;

		$input               = JFactory::getApplication()->input;
		$layout              = JFactory::getApplication()->input->get('layout', 'default');

		$this->messages = array();
		$this->messages['success'] = JText::_("COM_JGIVE_EXPORT_FILE_SUCCESS");
		$this->messages['error'] = JText::_("COM_JGIVE_EXPORT_FILE_ERROR");
		$this->messages['inprogress'] = JText::_("COM_JGIVE_EXPORT_FILE_NOTICE");

		$input = JFactory::getApplication()->input;
		$this->csv_url = JURI::root() . 'index.php?option=' . $input->get('option') . '&view=' . $input->get('view') . '&format=csv';

		// Check login status
		if (!$this->logged_userid)
		{
			$msg = JText::_('COM_JGIVE_LOGIN_MSG');
			$uri = $input->server->get('REQUEST_URI', '', 'STRING');
			$url = base64_encode($uri);
			$app->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url, false), $msg);
		}

		// Get itemids
		$menu = $app->getMenu();
		$menuItemId = $menu->getActive()->id;
		$this->itemid = $menuItemId;

		$this->state         = $this->get('State');
		$this->items         = $this->get('Items');

		$this->pagination    = $this->get('Pagination');
		$this->params        = $app->getParams('com_jgive');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');

		$this->campaignsId   = $this->get('CampaignId');

		if ($layout == 'contact_us')
		{
			$session                 = JFactory::getSession();
			$selected_donor_item_ids = $session->get('selected_donor_item_ids');
			$this->selected_emails   = $this->getModel()->getDonorsEmail($selected_donor_item_ids);
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		// Setup toolbar
		$this->addTJtoolbar();

		$this->_prepareDocument();
		parent::display($tpl);
	}

	/**
	 * Setup ACL based tjtoolbar
	 *
	 * @return  void
	 *
	 * @since   2.2
	 */
	protected function addTJtoolbar ()
	{
		$input               = JFactory::getApplication()->input;
		$layout              = JFactory::getApplication()->input->get('layout', 'default');

		// Add toolbar buttons
		jimport('techjoomla.tjtoolbar.toolbar');
		$tjbar = TJToolbar::getInstance('tjtoolbar');

		if ($layout == 'contact_us')
		{
			// Send button code on contact us file
			$tjbar->appendButton('donors.emailtoSelected',
								'COM_JGIVE_EMAIL_SEND',
								'glyphicon glyphicon-envelope icon icon-envelope',
								'class="btn btn-primary btn-md"
								title="' . JText::_("COM_JGIVE_EMAIL_SEND_TOOLTIP") . '"');

			$tjbar->appendButton('donors.cancelToMail',
								'COM_JGIVE_CANCEL_EMAIL',
								'glyphicon glyphicon-remove icon icon-remove',
								'class="btn btn-default btn-md"
								title="' . JText::_("COM_JGIVE_CANCEL_EMAIL") . '"');
		}
		else
		{
			if (count($this->items) > 0)
			{
				$tjbar->appendButton('donors.redirectforEmail',
									'COM_JGIVE_EMAIL_TO_DONORS',
									'glyphicon glyphicon-envelope icon icon-envelope',
									'class="btn btn-default btn-md"
									title="' . JText::_("COM_JGIVE_EMAIL_TO_DONORS_TOOLTIP") . '"');

				$tjbar->appendButton('tjtoolbar.custom',
									'COM_JGIVE_CSV_EXPORT',
									'glyphicon glyphicon-download-alt icon-download',
									'onclick="tjexport.exportCsv(0)" class="btn btn-default btn-md"
									title="' . JText::_("COM_JGIVE_EMAIL_TO_DONORS_TOOLTIP") . '"');
			}
		}

		$this->toolbarHTML = $tjbar->render();
	}

	/**
	 * Function _prepareDocument
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_JGIVE_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Function getState
	 *
	 * @param   String  $state  State
	 *
	 * @return  boolean
	 *
	 * @since   1.8.1
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
