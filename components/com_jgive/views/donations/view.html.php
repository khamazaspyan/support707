<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * JgiveViewDonations form view class.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JgiveViewDonations extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		global $mainframe, $option;
		$mainframe  = JFactory::getApplication();
		$option     = JFactory::getApplication()->input->get('option');
		$dispatcher = JDispatcher::getInstance();

		$session       = JFactory::getSession();
		$this->session = $session;

		// Get component params
		$this->params          = $params = JComponentHelper::getParams('com_jgive');
		$this->currency_code   = $params->get('currency');
		$this->default_country = $params->get('default_country');

		// Imp This is frontend
		$this->donations_site = 1;
		$this->retryPayment_show = 0;

		$this->retryPayment = new StdClass;
		$this->retryPayment->status = '';
		$this->retryPayment->msg = '';

		// Default layout is payment
		$this->layout = $layout = JFactory::getApplication()->input->get('layout', 'payment');
		$this->setLayout($layout);

		// Get logged in user id
		$user                = JFactory::getUser();
		$this->logged_userid = $user->id;

		$donationid = JFactory::getApplication()->input->get('donationid');

		$input      = JFactory::getApplication()->input;
		$guestemail = $input->get('email');
		$this->cid = $cid = $input->get('cid', '', 'INT');
		$jgiveFrontendHelper        = new jgiveFrontendHelper;
		$this->jomsocailToolbarHtml = $jgiveFrontendHelper->jomsocailToolbarHtml();

		if (!(($layout == 'details') AND $donationid AND $guestemail))
		{
			$session->clear('JGIVE_order_id');

			if (!$this->logged_userid)
			{
				$this->guest_donation = $params->get('guest_donation');

				if ($this->guest_donation)
				{
					$msg         = JText::_('COM_JGIVE_LOGIN_MSG_SILENT');
					$uri         = JFactory::getApplication()->input->get('REQUEST_URI', '', 'server', 'string');
					$url         = base64_encode($uri);
					$guest_login = $session->get('quick_reg_no_login');
					$session->clear('quick_reg_no_login');
				}
				else
				{
					$itemid = $input->get('Itemid');
					$msg = JText::_('COM_JGIVE_LOGIN_MSG');
					$uri    = 'index.php?option=com_jgive&view=donations&layout=payment&cid=' . $cid . '&Itemid=' . $itemid;
					$url    = base64_encode($uri);
					$mainframe->redirect(JRoute::_('index.php?option=com_users&return=' . $url, false), $msg);
				}
			}
		}

		$path = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$donationsHelper = new donationsHelper;
		$this->pstatus   = $donationsHelper->getPStatusArray();

		// Used on donations view for payment status filter
		$this->sstatus   = $donationsHelper->getSStatusArray();

		if ($layout == 'payment')
		{
			// Get country list options
			// Use helper function
			$countries       = $jgiveFrontendHelper->getCountries();
			$this->countries = $countries;

			// Get campaign id
			$cid             = $this->get('CampaignId');
			$this->cid       = $cid;

			$cdata = $this->get('Campaign');
			$this->assignRef('cdata', $cdata);

			// Joomla profile import
			$nofirst = '';

			// If no user data set in session then only import the user profile or only first time after login
			$nofirst = $session->get('No_first_donation');

			if (empty($nofirst))
			{
				if ($this->logged_userid)
				{
					// If recurring donor then get donor Infomation
					$profiledata = $this->get('RecurringDonorInfo');

					if (!$profiledata)
					{
						$profile_import = $params->get('profile_import');

						// If profie import is on the call profile import function
						if ($profile_import)
						{
							$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
							$profiledata             = $JgiveIntegrationsHelper->profileImport(1);
						}
					}

					$this->_setDonorData($profiledata);
				}
			}

			JPluginHelper::importPlugin('payment');

			$this->gateways = $params->get('gateways');

			$gateways = array();

			if (!empty($this->gateways))
			{
				$gateways = $dispatcher->trigger('onTP_GetInfo', array((array) $this->gateways));
			}

			$this->assignRef('gateways', $gateways);

			// Recurring payment gateway
			$this->recurringGateways = array();

			foreach ($gateways as $gateway)
			{
				// Add recurring supported payment gateways html
				if ($gateway->id == "paypal" || $gateway->id == "stripe")
				{
					$this->recurringGateways[] = $gateway;
				}
			}

			// Get campaign givebacks
			$this->giveback_id = $session->get('JGIVE_giveback_id');
			$JGIVE_cid         = $session->get('JGIVE_cid');

			$campaignHelper          = new campaignHelper;
			$this->campaignGivebacks = $campaignHelper->getCampaignGivebacks($JGIVE_cid);
		}

		if ($layout == 'confirm')
		{
			// @TODO save all posted data somewhere
			$session = JFactory::getSession();
			$this->assignRef('session', $session);
			$cid = $this->get('CampaignId');
			$this->assignRef('cid', $cid);
			$cdata = $this->get('Campaign');
          
			$this->assignRef('cdata', $cdata);

			JPluginHelper::importPlugin('payment');

			$this->gateways = $params->get('gateways');

			$gateways = array();

			if (!empty($this->gateways))
			{
				$gateways = $dispatcher->trigger('onTP_GetInfo', array((array) $this->gateways));
			}

			$this->assignRef('gateways', $gateways);
		}

		if ($layout == 'details')
		{
			$donation_id      = JFactory::getApplication()->input->get('donationid');
			$donation_details = $this->get('SingleDonationInfo');

			$this->retryPayment = $this->get('AllowedRetryPayment');

			// TO DO check the email id against orderid except below
			$this->logged_userid;
			$donation_details['donor']->user_id;
			$this->guest_donation = $params->get('guest_donation');

			if ($this->guest_donation)
			{
				if (!$this->logged_userid)
				{
					$input       = JFactory::getApplication()->input;
					$guest_email = $input->get('email');
					$donar_email = md5($donation_details['donor']->email);

					if ($guest_email != $donar_email)
					{
						$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
						$link   = JRoute::_('index.php?option=com_jgive&view=donations&layout=my&Itemid=' . $itemid, false);
						$msg    = JText::_('COM_JGIVE_NO_ACCESS_MSG');
						$mainframe->enqueueMessage($msg, 'notice');
						$mainframe->redirect($link, '');
					}
				}
				elseif ($this->logged_userid != $donation_details['donor']->user_id)
				{
					$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
					$link   = JRoute::_('index.php?option=com_jgive&view=donations&layout=my&Itemid=' . $itemid, false);
					$msg    = JText::_('COM_JGIVE_NO_ACCESS_MSG');
					$mainframe->enqueueMessage($msg, 'notice');
					$mainframe->redirect($link, '');
				}
			}
			elseif ($this->logged_userid != $donation_details['donor']->user_id)
			{
				$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
				$link   = JRoute::_('index.php?option=com_jgive&view=donations&layout=my&Itemid=' . $itemid, false);
				$msg    = JText::_('COM_JGIVE_NO_ACCESS_MSG');
				$mainframe->enqueueMessage($msg, 'notice');
				$mainframe->redirect($link, '');
			}

			// PAYMENT
			$dispatcher = JDispatcher::getInstance();
			JPluginHelper::importPlugin('payment');

			$params = JComponentHelper::getParams('com_jgive');

			if (!is_array($params->get('gateways')))
			{
				$gateway_param[] = $params->get('gateways');
			}
			else
			{
				$gateway_param = $params->get('gateways');
			}

			if (!empty($gateway_param))
			{
				$gateways = $dispatcher->trigger('onTP_GetInfo', array($gateway_param));
			}

			$this->gateways = $gateways;

			// If recurring is 1 pass only paypal
			$recure_gateway[0]['name'] = 'paypal';
			$recure_gateway[0]['id'] = 'paypal';

			$this->recure_gateway = $recure_gateway;

			$this->assignRef('donation_details', $donation_details);
		}

		if ($layout == 'my' || $layout == 'all')
		{
			if (!$this->logged_userid AND $layout == 'my')
			{
				$msg = JText::_('COM_JGIVE_LOGIN_MSG');
				$uri = $_SERVER["REQUEST_URI"];
				$url = base64_encode($uri);
				$mainframe->redirect(JRoute::_('index.php?option=com_users&view=login&return=' . $url, false), $msg);
			}

			$donations = $this->get('Donations');
			$this->donations = $donations;

			// Get ordering filter
			$filter_order_Dir = $mainframe->getUserStateFromRequest('com_jgive.filter_order_Dir', 'filter_order_Dir', 'desc', 'word');
			$filter_type      = $mainframe->getUserStateFromRequest('com_jgive.filter_order', 'filter_order', 'id', 'string');

			// Payment status filter
			$payment_status = $mainframe->getUserStateFromRequest('com_jgive' . 'payment_status', 'payment_status', '', 'string');

			if ($payment_status == null)
			{
				$payment_status = '-1';
			}

			// Set filters
			$lists['payment_status'] = $payment_status;
			$lists['order_Dir']      = $filter_order_Dir;
			$lists['order']          = $filter_type;
			$this->lists             = $lists;

			$total       = $this->get('Total');
			$this->total = $total;

			$pagination       = $this->get('Pagination');
			$this->pagination = $pagination;

			$itemid       = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations&layout=my');
			$this->Itemid = $itemid;
		}

		parent::display($tpl);
	}

	/**
	 * Function to set Donor Data
	 *
	 * @param   array  $profiledata  The Profile Data contain donor information.
	 *
	 * @return  void
	 */
	public function _setDonorData($profiledata)
	{
		if (!empty($profiledata))
		{
			$session = JFactory::getSession();

			foreach ($profiledata as $field => $value)
			{
				if (!empty($value))
				{
					$session->set('JGIVE_' . $field, $value);
				}
			}

			if (!empty($profiledata['first_name']) && !empty($profiledata['last_name']))
			{
				$session->set('JGIVE_user_first_last_name', $profiledata['first_name'] . ' ' . $profiledata['last_name']);
			}

			$session->set('No_first_donation', 1);
		}
	}
}
