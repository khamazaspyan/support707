<?php
/**
* @version		1.0.0 jgive $
* @package		jgive
* @copyright	Copyright © 2012 - All rights reserved.
* @license		GNU/GPL
* @author		TechJoomla
* @author mail	extensions@techjoomla.com
* @website		http://techjoomla.com
*/

// No direct access
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Load jgive js
// Backend
$document->addScript(JUri::root(true) . '/media/com_jgive/javascript/donations.js');
$cdata=$this->donation_details;
$status = $this->retryPayment->status;
$msg = $this->retryPayment->msg;
$retryCondition_show= (isset($this->retryPayment)) ? $this->retryPayment : 1;

$core_js = JUri::root(true).'/media/system/js/core.js';
$flg = 0;

foreach ($document->_scripts as $name => $ar)
{
	if($name == $core_js )
	{
		$flg = 1;
	}
}

if ($flg == 0)
{
	echo "<script type='text/javascript' src='" . $core_js . "'></script>";
}

$params = JComponentHelper::getParams( 'com_jgive' );

// Check if goal amount present or not
$show_goal_field = 0;
$goal_amount = 0;
$show_selected_fields = $params->get('show_selected_fields');

if ($show_selected_fields)
{
	$creatorfield=$params->get('creatorfield');

	if (isset($creatorfield))
	{
		foreach ($creatorfield as $tmp)
		{
			switch ($tmp)
			{
				case 'goal_amount':
					$goal_amount=1;
				break;
			}
		}
	}
}
else
{
	$show_goal_field = 1;
}

// End Addded by Sneha
$donations_site = (isset($this->donations_site)) ? $this->donations_site : 0;
$donations_email= (isset($this->donations_email)) ? $this->donations_email : 0;

// Showing Retry Donation Button is is not set
$retryPayment_show= (isset($this->retryPayment_show)) ? $this->retryPayment_show : 1;

// Url for retry donation
$url = JUri::root() . "index.php?option=com_jgive&tmpl=component&task=donations.retryPayment&order=" . $cdata['payment']->id . "&gateway_name=";

// Load Payment gateway form html
$ajax =
<<<EOT
techjoomla.jQuery(document).ready(function()
{
	techjoomla.jQuery("input[name='gateways']").change(function()
	{
		var url1 = '{$url}'+techjoomla.jQuery("input[name='gateways']:checked").val();
		techjoomla.jQuery('#html-container').empty().html('Loading...');
		techjoomla.jQuery.ajax({
								url: url1,
								type: 'GET',
								dataType: 'json',
								success: function(response)
								{
									techjoomla.jQuery('#html-container').removeClass('ajax-loading').html( response );
								}
								});
	});
});
EOT;

$document->addScriptDeclaration($ajax);
?>
<script type="text/javascript">

	// Hide/Show Payment gateways option
	function jgive_showpaymentgetways()
	{
		var status = "<?php echo $status;?>";
		var msg = "<?php echo $msg;?>";

		if (document.getElementById('gatewaysContent').style.display=='none')
		{
			if(status == '1')
			{
				alert(msg);
			}
			else if(status == '0')
			{
				document.getElementById('gatewaysContent').style.display='block';
			}
		}

		return false;
	}
</script>

<div class="page-header">
	<h2>
		<?php
		if($donations_site)
		{
			echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_DETAILS') : JText::_('COM_JGIVE_INVESTMENT_DETAILS'));
		}
		?>
	</h2>
</div>

<div class="tjBs3 container-fluid">
	<div class="row">
		<?php
		if($donations_email)
		{?>
			<h4 style="background-color: #cccccc" ><?php
				echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_DETAILS_INFO') : JText::_('COM_JGIVE_INVESTMENT_DETAILS_INFO'));
			?></h4>
		<?php
		}

		if($donations_site)
		{?>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
				<h3><?php echo JText::_('COM_JGIVE_PAYMENT_DETAILS_SHORT'); ?></h3>
		<?php
		}
		else
		{?>
			<fieldset>
			<legend><?php echo JText::_('COM_JGIVE_PAYMENT_DETAILS_SHORT'); ?></legend>
		<?php
		}?>
			<table class="table table-condensed adminlist table-striped table-bordered">
				<tr>
					<td>
						<?php
							echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONATION_ID') : JText::_('COM_JGIVE_INVETMENT_ID'));
							?>
					</td>
					<td>
						<?php
							if(!$cdata['payment']->order_id)
							{
								$cdata['payment']->order_id=$cdata['payment']->id;
							}
							echo $cdata['payment']->order_id;
						?>
					</td>
				</tr>
				<?php if ($cdata['payment']->giveback_id)
				{?>
				<tr>
					<td>
						<?php echo JText::_('COM_JGIVE_BACK_ID'); ?></td>
					<td>
						<?php
						if ($cdata['payment']->giveback_id)
						{
							echo $cdata['payment']->giveback_id;
						}
						else
						{
							echo JText::_('COM_JGIVE_NO_GIVEBACK');
						}
						?>
					</td>
				</tr>
				<?php }
				if ($cdata['payment']->giveback_id)
				{?>
				<tr>
					<td>
						<?php echo JText::_('COM_JGIVE_ORDER_GIVEBACK_DESC'); ?>
					</td>

					<td>
						<?php

						if($cdata['payment']->giveback_desc)
						{
							echo $cdata['payment']->giveback_desc;
						}
						else
						{
							echo JText::_('COM_JGIVE_NO_GIVEBACK');
						}
						?>
					</td>
				</tr>
				<?php }?>

				<tr>
					<td><?php echo JText::_('COM_JGIVE_DATE');?></td>
					<td>
						<?php echo JHtml::_('date', $cdata['payment']->cdate, JText::_('COM_JGIVE_DATE_FORMAT_JOOMLA3'));?>
					</td>
				</tr>
				<?php if($cdata['payment']->is_recurring): ?>
					<tr>
						<td><?php echo JText::_("COM_JGIVE_DONATATION_TYPE");?></td>
						<td><?php echo JTEXT::_("COM_JGIVE_RECURRING"); ?></td>
					</tr>
				<?php else: ?>
					<tr>
						<td><?php echo JText::_("COM_JGIVE_DONATATION_TYPE");?></td>
						<td><?php echo JTEXT::_("COM_JGIVE_ONE_TIME"); ?></td>
					</tr>
				<?php endif; ?>

				<?php if($cdata['payment']->is_recurring): ?>
					<tr>
						<td><?php echo JText::_("COM_JGIVE_TERMS");?></td>

						<?php
							$jgiveFrontendHelper=new jgiveFrontendHelper();
							$diplay_amount_with_format = $jgiveFrontendHelper->getFormattedPrice($cdata['payment']->amount);

							$recurring_terms = str_replace("{AMOUNT}",$diplay_amount_with_format, JText::_("COM_JGIVE_RECURRING_DONATION_TERMS"));

							$recurring_terms = str_replace("{RECURRING_FREQ}",strtolower($cdata['payment']->recurring_frequency), $recurring_terms);

							$recurring_terms = str_replace("{RECURRING_TIMES}",$cdata['payment']->recurring_count, $recurring_terms);

						?>
						<td><?php echo $recurring_terms; ?></td>
					</tr>
				<?php endif; ?>

				<tr>
					<td><?php echo JText::_('COM_JGIVE_AMOUNT');?></td>
					<td>
						<?php
							$jgiveFrontendHelper=new jgiveFrontendHelper();
							$diplay_amount_with_format=$jgiveFrontendHelper->getFormattedPrice($cdata['payment']->amount);
							echo $diplay_amount_with_format;
						 ?>
					</td>
				</tr>

				<?php

				if($params->get('vat_for_donor'))
				{
				 ?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_VAT_NUMBER');?></td>
					<td><?php echo $cdata['payment']->vat_number;?></td>
				</tr>
					<?php
				} ?>

				<tr>
					<td><?php echo JText::_('COM_JGIVE_IP_ADDRESS');?></td>
					<td><?php echo $cdata['payment']->ip_address;?></td>
				</tr>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_PAYMENT_METHOD');?></td>
					<td><?php
						$donationsHelper= new donationsHelper();
						// gettng plugin name which is set in plugin option
						$plgname=$donationsHelper->getPluginName($cdata['payment']->processor);
						$plgname=!empty($plgname)?$plgname:$cdata['payment']->processor;
						echo $plgname;?>
					</td>
				</tr>
				<?php if(!empty($cdata['payment']->transaction_id))
				{?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_TRANSACTION_ID');?></td>
					<td><?php echo $cdata['payment']->transaction_id;?></td>
				</tr>
				<?php }?>
				<?php
					$annonymous_donation = '';
					switch($cdata['payment']->annonymous_donation)
					{
						case 0 :
							$annonymous_donation =  JText::_('COM_JGIVE_NO');
						break;
						case 1 :
							$annonymous_donation = JText::_('COM_JGIVE_YES') ;
						break;
					}
				?>
				<tr>
					<td><?php
					echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_ANNONYMOUS_DONATION') : JText::_('COM_JGIVE_ANNONYMOUS_INVESTMENT'));
					?></td>
					<td><?php echo $annonymous_donation;?></td>
				</tr>
			</table>

			<?php if($donations_site)
			{ ?>
				</div>
			<?php
			}
			else
			{ ?>
				</fieldset>
			<?php
			} ?>

		<?php
			$whichever = '';
			$info_text = '';
			switch($cdata['payment']->status)
			{
				case 'C' :
					$whichever =  JText::_('COM_JGIVE_CONFIRMED');
				break;

				case 'RF' :
					$whichever = JText::_('COM_JGIVE_REFUND') ;
				break;

				case 'P' :
					if($donations_site)
					{
						$whichever       = JText::_('COM_JGIVE_PENDING') ;
						$this->params    = JComponentHelper::getParams('com_jgive');
						$mail_recipients = $this->params->get("mail_recipients_order_status_change");

						if (isset($mail_recipients) && !$donations_email)
						{
							if (in_array('donor', $mail_recipients))
							{
								$info_text = JText::_("COM_JGIVE_ORDER_STATUS_NOTE_MSG");
							}
						}
					}
				break;

				case 'E' :
					$whichever = JText::_('COM_JGIVE_CANCELED') ;
				break;

				case 'D' :
					$whichever = JText::_('COM_JGIVE_DENIED') ;
				break;
			}
		?>

			<?php
			if($donations_site)
			{ ?>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<h3><?php echo JText::_('COM_JGIVE_PAYMENT_STATUS'); ?></h3>

						<!-- To do not show note while checkout, checked layout -->
						<?php if (!empty($info_text) && !empty($this->layout)): ?>
							<div class="alert alert-info">
							  <button type="button" class="close" data-dismiss="alert">&times;</button>
							  <?php echo $info_text; ?>
							</div>
						<?php endif; ?>
					<?php
			}
			else{ ?>
				<fieldset>
				<legend><?php echo JText::_('COM_JGIVE_PAYMENT_STATUS'); ?></legend>
			<?php } ?>

				<form action="" name="adminForm" id="adminForm" class="form-validate" method="post">
					<table class="table table-condensed adminlist table-striped table-bordered" >
						<tr>
							<td><?php echo JText::_('COM_JGIVE_PAYMENT_STATUS');?></td>
							<td>
							<?php
								if( ($cdata['payment']->status == 'P' || $cdata['payment']->status == 'C' || $cdata['payment']->status == 'E') && !($donations_site))
								{
									echo JHtml::_('select.genericlist',$this->pstatus,"pstatus",'class="pad_status" size="1" onChange="selectstatusorder('.$cdata['payment']->id.',this);"',"value","text",$cdata['payment']->status);
								}
								else
								{
									echo $whichever;
								}
								 ?>
							</td>
						</tr>
						<?php if($cdata['payment']->status == 'P' || $cdata['payment']->status == 'E')
						{
							if($retryPayment_show== 0)
							{?>
								<tr>
									<td colspan="2">
										<button class="btn btn-primary btn-xs" name="show_getways" id="show_getways"
											onclick="return jgive_showpaymentgetways();">
											<?php echo JText::_('COM_JGIVE_RETRY_DONATION');?>
										</button>
										<div id="gatewaysContent" style="display:none;">
											<div class="form-group">
												<label for="gatewaysContent" class="col-lg-8 col-md-8 col-sm-8 col-xs-12 control-label">
													<?php echo JText::_('COM_PAY_METHODS');?>
												</label>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-right">
												<?php
													if (empty($this->gateways))
													{
														echo JText::_('NO_PAYMENT_GATEWAY');
													}
													else
													{
														if($cdata['payment']->is_recurring == 1)
														{
															$gateways = $this->recure_gateway;

															$plg_html = '';
																foreach($gateways as $gateway)
																{
																	$plg_html .=
																	'<div class="radio">
																		<label>
																			<input type="radio" name="gateways" id="'.$gateway['id'].'" value="'.$gateway['id'].'" aria-label="..." autocomplete="off">
																				'.$gateway['name'].'
																		  </label>
																		</div>';
																}

																echo $plg_html;
														}
														else
														{
															$gateways = $this->gateways;

															$plg_html = '';
																foreach($gateways as $gateway)
																{
																	$plg_html .=
																	'<div class="radio">
																		<label>
																			<input type="radio" name="gateways" id="'.$gateway->id.'" value="'.$gateway->id.'" aria-label="..." autocomplete="off">
																				'.$gateway->name.'
																		  </label>
																		</div>';
																}

																echo $plg_html;
														}
													}?>
												</div>
											</div>
										</div>
									</td>
								</tr>
							<?php
							}
						}?>
						<?php if(!$donations_site){ ?>
							<tr>
								<td><?php echo JText::_('COM_JGIVE_NOTIFY');?></td>
								<td>
									<input type="checkbox" id = "notify_chk"  name = "notify_chk" value="1" size= "10" checked />
								</td>
							</tr>
							<tr>
								<td><?php echo JText::_('COM_JGIVE_COMMENT');?></td>
								<td><textarea id="" name="comment" rows="3" size="28" value=""></textarea></td>
							</tr>
						<?php } ?>
					</table>
					<?php if(!isset($this->mailContent)) { ?>
					<input type="hidden" name="option" value="com_jgive" />
					<input type="hidden" id='hidid' name="id" value="" />
					<input type="hidden" id='hidstat' name="status" value="" />
					<input type="hidden" name="task" id="task" value="" />
					<input type="hidden" name="view" value="donations" />
					<!--<input type="hidden" name="controller" value="donations" />-->
					<?php } ?>
				</form>

			<?php if($donations_site) { ?>
			</div>
			<?php }
			else{ ?>
			</fieldset>
			<?php } ?>


	<div style="clear:both;"></div>

	<?php if($donations_site)
	{ ?>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>
				<?php
					echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONOR_DETAILS_SHORT') : JText::_('COM_JGIVE_INVESTOR_DETAILS_SHORT'));
				?>
			</h3>
	<?php
	}
	else
	{ ?>
	<fieldset>

		<legend>
			<?php
			echo (($cdata['campaign']->type=='donation') ? JText::_('COM_JGIVE_DONOR_DETAILS_SHORT') : JText::_('COM_JGIVE_INVESTOR_DETAILS_SHORT')); ?>
		</legend>

	<?php
	} ?>
		<table class="table table-condensed adminlist table-striped table-bordered" >
			<?php if( $cdata['donor']->first_name OR  $cdata['donor']->last_name): ?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_NAME');?></td>
					<td><?php echo $cdata['donor']->first_name.' '.$cdata['donor']->last_name;?></td>
				</tr>
			<?php endif; ?>

			<?php if( $cdata['donor']->address OR  $cdata['donor']->address2): ?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_ADDRESS');?></td>
					<td>
						<?php
							echo $cdata['donor']->address;
							echo "<br/>";
							echo $cdata['donor']->address2;
						?>
					</td>
				</tr>
			<?php endif; ?>

			<?php if( $cdata['donor']->zip): ?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_ZIP');?></td>
					<td><?php echo $cdata['donor']->zip;?></td>
				</tr>
			<?php endif; ?>


			<?php if( $cdata['donor']->country_name): ?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_COUNTRY');?></td>
					<td><?php echo $cdata['donor']->country_name;?></td>
				</tr>
			<?php endif; ?>

			<?php if( $cdata['donor']->state_name): ?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_STATE');?></td>
					<td><?php echo $cdata['donor']->state_name;?></td>
				</tr>
			<?php endif; ?>


			<?php if( $cdata['donor']->city_name): ?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_CITY');?></td>
					<td><?php echo $cdata['donor']->city_name;?></td>
				</tr>
			<?php endif;?>

			<?php if( $cdata['donor']->phone): ?>
				<tr>
					<td><?php echo JText::_('COM_JGIVE_PHONE');?></td>
					<td><?php echo $cdata['donor']->phone;?></td>
				</tr>
			<?php endif; ?>

			<tr>
				<td><?php echo JText::_('COM_JGIVE_EMAIL');?></td>
				<td><?php echo $cdata['donor']->email;?></td>
			</tr>
		</table>

	<?php if($donations_site)
	{ ?>
		</div>
		<?php
	}
	else
	{ ?>
		</fieldset>
		<?php
	} ?>

	<?php if($donations_site)
	{ ?>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3><?php echo JText::_('COM_JGIVE_CAMPAIGN_DETAILS_SHORT'); ?></h3>
		<?php
	}
	else
	{ ?>
	<fieldset>
		<legend><?php echo JText::_('COM_JGIVE_CAMPAIGN_DETAILS_SHORT'); ?></legend>
		<?php
	} ?>
		<table class="table table-condensed adminlist table-striped table-bordered" width="50">
			<tr>
				<td><?php echo JText::_('COM_JGIVE_TITLE');?></td>
				<td><?php echo $cdata['campaign']->title;?></td>
			</tr>

			<?php if($show_goal_field==1 OR $goal_amount==0 ): ?>
			<tr>
				<td><?php echo JText::_('COM_JGIVE_GOAL_AMOUNT');?></td>
				<td><?php
					$jgiveFrontendHelper=new jgiveFrontendHelper();
					$diplay_amount_with_format=$jgiveFrontendHelper->getFormattedPrice($cdata['campaign']->goal_amount);
					echo $diplay_amount_with_format;
					?>
				</td>
			</tr>
			<?php endif; ?>

			<tr>
				<td><?php echo JText::_('COM_JGIVE_AMOUNT_RECEIVED');?></td>
				<td><?php
					$jgiveFrontendHelper=new jgiveFrontendHelper();
					$diplay_amount_with_format=$jgiveFrontendHelper->getFormattedPrice($cdata['campaign']->amount_received);
					echo $diplay_amount_with_format;
					?>
				</td>
			</tr>

			<?php if($show_goal_field==1 OR $goal_amount==0 ): ?>
			<tr>
				<td><?php echo JText::_('COM_JGIVE_REMAINING_AMOUNT');?></td>
				<td>
					<?php
					if($cdata['campaign']->amount_received>$cdata['campaign']->goal_amount){
						echo JText::_('COM_JGIVE_GOAL_ACHIEVED');
					}
					else{
						$jgiveFrontendHelper=new jgiveFrontendHelper();
						$diplay_amount_with_format=$jgiveFrontendHelper->getFormattedPrice($cdata['campaign']->remaining_amount);
						echo $diplay_amount_with_format;
					}
					?>
				</td>
			</tr>
			<?php endif; ?>
		</table>
		<?php
		if($cdata['payment']->status == 'P' || $cdata['payment']->status == 'E')
		{
			if($retryPayment_show== 0)
			{
				?>
				<button
					class="pull-right"
					onclick="jgive.donations.cancelRetryDonation()"
					type="button" class="btn btn-mini col-xs-2"
					id="cancelRetryDonation"
					data-original-title="Clear"
					title="<?php echo JText::_('COM_JGIVE_CLEAR_TOOLTIP');?>">
					<?php echo JText::_('COM_JGIVE_CANCEL')?>
				</button>
		<?php
			}
		}?>

	<?php if($donations_site)
	{ ?>
		</div>
		<?php
	}
	else
	{ ?>
		</fieldset>
		<?php
	} ?>
	</div>
</div>

<div style="clear:both;"></div>
<!--PAYMENT HIDDEN DATA WILL COME HERE -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"id="html-container" name=""></div>
<script type="text/javascript">
	var jgive_baseurl = "<?php echo JUri::root(); ?>";
</script>
