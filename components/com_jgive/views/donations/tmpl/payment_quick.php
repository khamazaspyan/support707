<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

$document = JFactory::getDocument();
$document->addScript(JUri::root(true) . '/media/com_jgive/javascript/donations.js');
$jgiveFrontendHelper = new jgiveFrontendHelper;

$input = JFactory::getApplication()->input;
$show_selected_fields_on_donate = $this->params->get('show_selected_fields_on_donation');
$donationfield = array();
$show_field = 0;
$donation_anonym = 0;

if ($show_selected_fields_on_donate)
{
	$donationfield = $this->params->get('donationfield');

	if (isset($donationfield))
	{
		if (in_array("donation_anonym", $donationfield))
		{
			$donation_anonym = 1;
		}
	}
}
else
{
	$show_field = 1;
}
// Fetched data
$cdata = $this->cdata;
?>
<div class="tjBs3">
	<div id="jgive_content" class="container-fluid">
		<header>
			<div class="page-header center">
				<h2 itemprop="name">
					<?php echo ($this->cdata['campaign']->type === 'donation') ? JText::_("COM_JGIVE_DONATE_TO"): JText::_("COM_JGIVE_INVEST_TO"); ?>
					&nbsp;<?php echo $this->cdata['campaign']->title; ?>
				</h2>
			</div>
		</header>

		<form action="" method="post" id="payment_quick" name="payment_quick" class="form-validate form-vertical" enctype="multipart/form-data">
			<!-- Edit order button -->
			<button id="jgive_edit_button" type="button" class="btn btn-defaultbtn-medium btn-primary jgive_edit_button pull-right" onclick="editDetails()">
				<span ><i class="fa fa-pencil-square-o"></i> <?php echo JText::_("COM_JGIVE_EDIT"); ?></span>
			</button>

			<div class="clearfix">&nbsp;</div>

			<!-- Edit amount & other details fields -->
			<div id="jgive_form_field">
				<div class="row" >
					<div id="jgive_disabled_fields" >
						<aside class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label class="control-label">
									<strong><?php echo JHtml::tooltip(
									JText::_("COM_JGIVE_ENTER_AMOUNT_TP"),
									JText::_("COM_JGIVE_ENTER_AMOUNT"),
									'',
									JText::_("COM_JGIVE_ENTER_AMOUNT")
									);?></strong>
								</label>
								<div class="controls">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">
											<?php
											$currency = $this->params->get('currency_symbol');

											// If currency symbol is not available then use currency code
											if (empty($currency))
											{
												$currency = $this->currency_code;
											}

											echo $currency;
											?>
										</span>
										<?php
											// Minimum donation amount
											$predefineAmount = !empty( $cdata['campaign']->minimum_amount) ? $cdata['campaign']->minimum_amount : 0;

											// Get selected giveback amount
											if (!empty($this->giveback_id))
											{
												foreach ($this->campaignGivebacks as $giveback)
												{
													if ($this->giveback_id == $giveback->id)
													{
														$predefineAmount = $giveback->amount;
														break;
													}
												}
											}
										?>
										<input type="number"
											class="jgive-amount-field"
											id="donation_amount"
											name="donation_amount"
											title="<?php echo JText::_("COM_JGIVE_ENTER_AMOUNT_TP");?>"
											min="<?php echo !empty($this->cdata['campaign']->minimum_amount) ? $this->cdata['campaign']->minimum_amount : 0; ?>"
											value="<?php echo $predefineAmount; ?>"
											class="required"
											required />
									</div>
								</div>
							</div>
							<div class="form-group">
								<?php
								if ($cdata['campaign']->type == "donation")
								{
									if ($show_field == 1 || $donation_anonym == 0 )
									{
									?>
									<label class="control-label">
										<strong>
											<?php echo JHtml::tooltip(
											JText::_("COM_JGIVE_DONATE_ANONYMOUSLY_TOOLTIP"),
											JText::_("COM_JGIVE_DONATE_ANONYMOUSLY"), '',
											JText::_("COM_JGIVE_DONATE_ANONYMOUSLY")
											);?>
										</strong>
									</label>
									<div class="controls radio">
										<label class="radio inline">
											<input type="radio" name="annonymousDonation" id="annonymousDonation1" value="1" >
												<?php echo JText::_('COM_JGIVE_YES');?>
										</label>
										<label class="radio inline">
											<input type="radio" name="annonymousDonation" id="annonymousDonation2" value="0" checked>
												<?php echo JText::_('COM_JGIVE_NO');?>
										</label>
									</div>
									<?php
									}
								}
								?>
							</div>
						</aside>

						<aside class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label label-default class="control-label" title="<?php echo JText::_("COM_JGIVE_YOUR_DETAILS_TP");?>">
									<strong><?php echo JHtml::tooltip(
									JText::_("COM_JGIVE_YOUR_DETAILS_TP"),
									JText::_("COM_JGIVE_YOUR_DETAILS"),
									'',
									JText::_("COM_JGIVE_YOUR_DETAILS")
									);?></strong>
								</label>
								<div class="controls">
									<input type="text"
										id="user_first_last_name"
										name="user_first_last_name"
										title="<?php echo JText::_("COM_JGIVE_FIRST_LAST_NAME");?>"
										placeholder="<?php echo JText::_("COM_JGIVE_FIRST_LAST_NAME");?>"
										value="<?php echo $this->session->get('JGIVE_user_first_last_name');?>"
										class="required"
										required />
								</div>
								&nbsp;
								<div class="controls">
									<input type="email"
										id="paypal_email"
										name="paypal_email"
										title="<?php echo JText::_("COM_JGIVE_EMAIL");?>"
										placeholder="<?php echo JText::_("COM_JGIVE_EMAIL");?>"
										value="<?php echo $this->session->get('JGIVE_paypal_email');?>"
										class="required"
										required />
								</div>
							</div>
						</aside>
						<?php
						if (count($this->campaignGivebacks))
						{
						?>
							<div id="jgive_givebacks" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h4><?php echo JText::_("COM_JGIVE_SELECT_YOUR_GIVEBACK");?></h4>

								<div class="radio">
									<label>
										<input alt="0.00"
											type="radio"
											name="givebacks"
											value="0"
											title="<?php echo JText::_("COM_JGIVE_NO_GIVEBACK_TITLE"); ?>"
											onclick="populateSelected_GivebackAmount(<?php echo $cdata['campaign']->minimum_amount; ?>, this)" />
											<?php echo JText::_("COM_JGIVE_THANKS_MSG"); ?>
									</label>
								</div>
								<?php
								foreach ($this->campaignGivebacks as $giveback)
								{
									$sold_givebacks = $giveback->sold_giveback;

									if ($sold_givebacks < $giveback->total_quantity)
									{
										$checked = "";
										$class = "";

										// Highlight selected giveback
										if ($this->giveback_id == $giveback->id)
										{
											$checked = ' checked="checked" ';
											$class = " jgive_active ";
										} ?>
										<div class="radio">
											<label>
												<input alt="<?php echo $giveback->amount; ?>"
													type="radio"
													name="givebacks"
													value="<?php echo $giveback->id; ?>"
													<?php echo $checked; ?>
													onclick="populateSelected_GivebackAmount(<?php echo $giveback->amount; ?>, this)"
													 />
												<strong> <?php echo $jgiveFrontendHelper->getFormattedPrice($giveback->amount); ?> + </strong> &nbsp;

												<?php echo !empty($giveback->description) ? $giveback->description : JText::_("COM_JGIVE_NO_DESC_AVAILABLE"); ?>
											</label>
										</div>
									<?php
									}?>

								<?php
								} ?>
							</div>

						<?php
						}
						?>
					</div>
				</div>
				<div class="clear-fix">&nbsp;</div>
				<hr/>
			</div>
			<footer id="jgive_footer">
				<div class="form-group">
					<label label-default class="col-lg-2 col-md-2 col-sm-3 col-xs-12 control-label"
					title="<?php echo JText::_("COM_JGIVE_SELECT_PAYMENT_METHOD_TP");?>">
						<strong><?php echo JHtml::tooltip(
						JText::_("COM_JGIVE_SELECT_PAYMENT_METHOD_TP"),
						JText::_("COM_JGIVE_SELECT_PAYMENT_METHOD"),
						'',
						JText::_("COM_JGIVE_SELECT_PAYMENT_METHOD")
						);?> &nbsp;</strong>
					</label>

					<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 radio">
						<?php
						$select = array();
						$gateways = array_merge($select, $this->gateways);
						$gateways = array_filter($gateways);

						// If only one geteway then keep it as selected
						if (count($gateways) >= 1)
						{
							// Id and value is same
							$default = $gateways[0]->id;
						}

						if (empty($this->gateways))
						{
							echo JText::_('COM_JGIVE_NO_PAYMENT_GATEWAY');
						}
						else
						{
							foreach ($this->gateways as $gateway)
							{
							?>
								<label class="radio-inline">
									<input type="radio" name="gateways"
									id="qtc_<?php echo $gateway->id; ?>"
									value="<?php echo $gateway->id; ?>" onclick="placeQuickPayment()">
									<?php echo $gateway->name; ?>
								</label>
							<?php
							}
						}
						?>
					</div>
				</div>
				<div class="clearfix"></div>

			</footer>

			<input type="hidden" name="option" value="com_jgive" />
			<input type="hidden" name="view" value="donations" />
			<!--<input type="hidden" name="controller" value="donations" />-->
			<input type="hidden" name="task" value="donations.placeOrder" />

			<input type="hidden" name="cid" value="<?php echo $this->cid;?>" />
			<input type="hidden" name="Itemid" value="<?php echo $input->get('Itemid', '', 'INT');?>"/>
			<input type="hidden" name="userid" id="userid" value="<?php echo !empty($user->id) ? $user->id : 0; ?>">
			<input type="hidden" name="order_id" id="order_id" value="0" />
			<input type="hidden" name="account" value="guest" />
		</form>
		<div id="payment_tab_table_html"></div>
	</div>
</div>

<!--- Javascript global declaration here -->
<script>
	var jgive_baseurl = "<?php echo JUri::root(); ?>";

	var minimum_amount = "<?php echo $cdata['campaign']->minimum_amount;?>";
	minimum_amount = parseInt(minimum_amount);

	var send_payments_to_owner = "<?php echo $this->params->get('send_payments_to_owner');?>";
	var commission_fee = "<?php echo $this->params->get('commission_fee');?>";
	var fixed_commissionfee = "<?php echo $this->params->get('fixed_commissionfee');?>";

	var givebackDetails = <?php echo !empty($this->campaignGivebacks) ? json_encode($this->campaignGivebacks, 1) : 0;?>;
</script>
