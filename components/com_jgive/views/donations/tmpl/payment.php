<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();

// Load Quick donation template
if ($this->params->get('quick_donation') == 1)
{
	echo $this->loadTemplate('quick');
}
else
{
	echo $this->loadTemplate('paymentform');
}
