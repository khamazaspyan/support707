<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * JgiveViewregistration view class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveViewregistration extends JViewLegacy
{
	/**
	 * Function dispaly
	 *
	 * @param   string  $tpl  The name of the template file to parse.
	 *
	 * @return  void
	 *
	 * @since   1.8.1
	 */
	public function display($tpl = null)
	{
		parent::display($tpl);
	}
}
