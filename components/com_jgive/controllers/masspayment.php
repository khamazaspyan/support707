<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
require_once JPATH_COMPONENT . '/controller.php';

jimport('joomla.application.component.controller');

/**
 * JgiveControllermasspayment controller class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveControllermasspayment extends JControllerLegacy
{
	/**
	 * Method performmasspay .
	 *
	 * @return boolean
	 *
	 * @since    1.8.1
	 */
	public function performmasspay()
	{
		// Get Params
		$params = JComponentHelper::getParams('com_jgive');
		$send_payments_to_owner = $params->get('send_payments_to_owner');

		if (!$send_payments_to_owner)
		{
			$input = JFactory::getApplication()->input;
			$pkey = $input->get('pkey', '');
			$params->get('private_key_cronjob');

			if ($pkey != $params->get('private_key_cronjob'))
			{
				echo JText::_('COM_JGIVE_SECRET_KEY_ERROR');

				return false;
			}

			if ($params->get('commission_fee') == 0)
			{
				echo '<b>' . JText::_('COM_JGIVE_COMMISSION_ZERO_ERROR') . '</b>';

				return false;
			}

			$model = $this->getModel('masspayment');
			$msg   = $model->performmasspay();
			echo $msg;
		}
	}
}
