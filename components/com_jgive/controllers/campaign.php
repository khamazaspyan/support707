<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');
jimport('techjoomla.tjcsv.csv');
jimport('joomla.filesystem.file');
jimport('techjoomla.common');

/**
 * Supporting a campaign records.
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.7
 */
class JgiveControllerCampaign extends JgiveController
{
	/**
	 * Class constructor.
	 *
	 * @since   1.6
	 */
	public function __construct()
	{
		$this->techjoomlacommon = new TechjoomlaCommon;

		parent::__construct();
	}

	/**
	 * Method to saves a new campaign details
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.7
	 */
	public function save()
	{
		$app = JFactory::getApplication();

		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$result              = false;

		// Total length of post back data in bytes.
		$contentLength = (int) $_SERVER['CONTENT_LENGTH'];

		// Maximum allowed size of post back data in MB.
		$postMaxSize = (int) ini_get('post_max_size');

		// Maximum allowed size of script execution in MB.
		$memoryLimit = (int) ini_get('memory_limit');

		// Check for the total size of post back data.
		if (($postMaxSize > 0 && $contentLength > $postMaxSize * 1024 * 1024) || ($memoryLimit != -1 && $contentLength > $memoryLimit * 1024 * 1024))
		{
			return JError::raiseWarning(100, JText::_('COM_JGIVE_ERROR_WARNUPLOADTOOLARGE'));
		}

		JSession::checkToken() or jexit('Invalid Token');
		$session = JFactory::getSession();

		$extra_jform_data = JFactory::getApplication()->input->get('jform', array(), 'array');

		$ccat = JFactory::getApplication()->input->get('campaign_category', '', 'INT');

		$input = $app->input;
		$post  = $input->post;

		// Get model
		$model  = $this->getModel('campaign');

		if (!empty($ccat))
		{
			// Validate the posted data.
			$formExtra = $model->getFormExtra(
			array("category" => $ccat,
					"clientComponent" => 'com_jgive',
					"client" => 'com_jgive.campaign',
					"view" => 'campaign',
					"layout" => 'create')
					);

			if (!$formExtra)
			{
				JError::raiseError(500, $model->getError());

				return false;
			}

			// Validate the posted extra data.
			if (!empty($formExtra[0]))
			{
				$extra_jform_data = $model->validateExtra($formExtra[0], $extra_jform_data);
			}

			if (!empty($formExtra[1]))
			{
				$extra_jform_data = $model->validateExtra($formExtra[1], $extra_jform_data);
			}

			// Check for errors.
			if ($extra_jform_data === false)
			{
				// Get the validation messages.
				$errors	= $model->getErrors();

				// Push up to three validation messages out to the user.
				for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
				{
					if ($errors[$i] instanceof Exception)
					{
						$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
					}
					else
					{
						$app->enqueueMessage($errors[$i], 'warning');
					}
				}

				// Save the data in the session.
				// Tweak.
				$app->setUserState('com_jgive.edit.campaign.data', $extra_jform_data);

				// Tweak *important
				$app->setUserState('com_jgive.edit.campaign.id', $extra_jform_data['id']);

				// Redirect back to the edit screen.
				$id = (int) $app->getUserState('com_jgive.edit.campaign.id');
				$this->setRedirect(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $id, false));
			}
		}

		$campaignhelperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $helperPath);
			JLoader::load('campaignHelper');
		}

		$campaignHelper = new campaignHelper;

		$formattedTime = $campaignHelper->getFormattedTime($post);

		$data = array();

		// Append formatted start time & end time for event to startdate & enddate.
		$data['startdate']  = $post->get('start_date', '', 'STRING') . " " . $formattedTime['campaign_start_time'];
		$data['enddate']    = $post->get('end_date', '', 'STRING') . " " . $formattedTime['campaign_end_time'];
		$start_dt_timestamp = strtotime($data['startdate']);
		$end_dt_timestamp   = strtotime($data['enddate']);

		// Validate if start-date-time <= end-date-time.
		if ($start_dt_timestamp > $end_dt_timestamp)
		{
			// Save the data in the session.
			// Tweak.
			$app->setUserState('com_jgive.edit.campaign.data', $post);

			// Tweak *important
			$app->setUserState('com_jgive.edit.campaign.id', $post->get('cid'));

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_jgive.edit.campaign.id');
			$this->setMessage(JText::_('COM_JGIVE_DATE_ERROR'), 'warning');

			$this->setRedirect(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $id, false));

			return false;
		}

		$result = $model->save($extra_jform_data);

		$campaignid = $session->get('camapign_id');
		$session->set('camapign_id', '');

		if ($result)
		{
			$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');

			if (empty($itemid))
			{
				$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
				$redirect = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&Itemid=' . $itemid, true);
			}
			else
			{
				$redirect = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=my&Itemid=' . $itemid, false);
			}

			$msg      = JText::_('COM_JGIVE_CAMPAIGN_SAVED');
		}
		else
		{
			$itemid   = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=create');
			$redirect = JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $campaignid . '&Itemid=' . $itemid, false);
			$msg      = JText::_('COM_JGIVE_CAMPAIGN_ERROR_SAVING');
		}

		$this->setRedirect($redirect, $msg);
	}

	/**
	 * Lets you edit campaign
	 *
	 * @return  void
	 */
	public function edit()
	{
		// Total length of post back data in bytes.
		$contentLength = (int) $_SERVER['CONTENT_LENGTH'];
		$app = JFactory::getApplication();

		// Maximum allowed size of post back data in MB.
		$postMaxSize = (int) ini_get('post_max_size');

		// Maximum allowed size of script execution in MB.
		$memoryLimit = (int) ini_get('memory_limit');

		// Check for the total size of post back data.
		if (($postMaxSize > 0 && $contentLength > $postMaxSize * 1024 * 1024) || ($memoryLimit != -1 && $contentLength > $memoryLimit * 1024 * 1024))
		{
			return JError::raiseWarning(100, JText::_('COM_JGIVE_ERROR_WARNUPLOADTOOLARGE'));
		}

		JSession::checkToken() or jexit('Invalid Token');

		$jgiveFrontendHelper = new jgiveFrontendHelper;

		$input = $app->input;
		$post  = $input->post;
		$campaignid = $post->get('cid');
		$extra_jform_data = $app->input->get('jform', array(), 'array');

		$ccat = $app->input->get('campaign_category', '', 'INT');

		// Get model
		$model = $this->getModel('campaign');

		if (!empty($ccat))
		{
			// Validate the posted data.
			$formExtra = $model->getFormExtra(
			array("category" => $ccat,
					"clientComponent" => 'com_jgive',
					"client" => 'com_jgive.campaign',
					"view" => 'campaign',
					"layout" => 'create')
					);

			if (!$formExtra)
			{
				JError::raiseError(500, $model->getError());

				return false;
			}

			// Check for errors.
			if ($extra_jform_data === false)
			{
				// Get the validation messages.
				$errors	= $model->getErrors();

				// Push up to three validation messages out to the user.
				for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
				{
					if ($errors[$i] instanceof Exception)
					{
						$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
					}
					else
					{
						$app->enqueueMessage($errors[$i], 'warning');
					}
				}

				// Save the data in the session.
				// Tweak.
				$app->setUserState('com_jgive.edit.campaign.data', $all_jform_data);

				// Tweak *important
				$app->setUserState('com_jgive.edit.campaign.id', $campaignid);

				$this->setRedirect(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $campaignid, false));
			}
		}

		$campaignhelperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $helperPath);
			JLoader::load('campaignHelper');
		}

		$campaignHelper = new campaignHelper;

		$formattedTime = $campaignHelper->getFormattedTime($post);

		$data = array();

		// Append formatted start time & end time for event to startdate & enddate.
		$data['startdate']  = $post->get('start_date', '', 'STRING') . " " . $formattedTime['campaign_start_time'];
		$data['enddate']    = $post->get('end_date', '', 'STRING') . " " . $formattedTime['campaign_end_time'];
		$start_dt_timestamp = strtotime($data['startdate']);
		$end_dt_timestamp   = strtotime($data['enddate']);

		// Validate if start-date-time <= end-date-time.
		if ($start_dt_timestamp > $end_dt_timestamp)
		{
			// Save the data in the session.
			// Tweak.
			$app->setUserState('com_jgive.edit.campaign.data', $post);

			// Tweak *important
			$app->setUserState('com_jgive.edit.campaign.id', $post->get('cid'));

			// Redirect back to the edit screen.
			$id = (int) $app->getUserState('com_jgive.edit.campaign.id');
			$this->setMessage(JText::_('COM_JGIVE_DATE_ERROR'), 'warning');

			$this->setRedirect(JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $id, false));

			return false;
		}

		if ($campaignid)
		{
			$result = $model->edit($extra_jform_data);
		}
		else
		{
			$campaignid = $input->get('cid');
			JError::raiseWarning(100, JText::_('COM_JGIVE_INVALID_REQUES'));
		}

		if ($result)
		{
			$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=my');

			if (empty($itemid))
			{
				$itemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaigns&layout=all');
				$redirect = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=all&Itemid=' . $itemid, true);
			}
			else
			{
				$redirect = JRoute::_('index.php?option=com_jgive&view=campaigns&layout=my&Itemid=' . $itemid, false);
			}

			$msg      = JText::_('COM_JGIVE_CAMPAIGN_SAVED');
		}
		else
		{
			$itemid   = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=create');
			$redirect = JRoute::_('index.php?option=com_jgive&view=campaign&layout=create&cid=' . $campaignid . '&Itemid=' . $itemid, false);
			$msg      = JText::_('COM_JGIVE_CAMPAIGN_ERROR_SAVING');
		}

		$this->setRedirect($redirect, $msg);
	}

	/**
	 * Data campaign donor
	 *
	 * @return  Donor data
	 *
	 * since 1.7
	 */
	public function viewMoreDonorReports()
	{
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$cid         = $post->get('cid', '', 'INT');
		$jgive_index = $post->get('jgive_index', '', 'INT');

		$model  = $this->getModel('campaign');
		$result = $model->viewMoreDonorReports($cid, $jgive_index);

		echo json_encode($result);
		jexit();
	}

	/**
	 * View more donor profile picture
	 *
	 * @return  donor profile pic
	 *
	 * since 1.7
	 */
	public function viewMoreDonorProPic()
	{
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$cid         = $post->get('cid', '', 'INT');
		$jgive_index = $post->get('jgive_index', '', 'INT');

		$model  = $this->getModel('campaign');
		$result = $model->viewMoreDonorProPic($cid, $jgive_index);

		echo json_encode($result);
		jexit();
	}

	/**
	 * CSV data campaign donor
	 *
	 * @return   Object  Donor data
	 *
	 * since 1.7
	 */
	public function getdonorCsvData()
	{
		$input  = JFactory::getApplication()->input;
		$post   = $input->post;
		$params = JComponentHelper::getParams('com_jgive');
		$model  = $this->getModel('campaign');

		$cid    = $post->get('cid', '', 'INT');
		$result = $model->getCsvDataCampaignDonor($cid);

		$filename = JText::_('COM_JGIVE_DONOR_REPORT') . date("Y-m-d");

		$config['csv_filename']   = $filename;
		$config['csv_colHeading'] = JText::_("COM_JGIVE_DONOR_CSV_COLUMN_HEADER");
		$config['csv_fields']     = JText::_("COM_JGIVE_DONOR_CSV_FIELDS");

		$TjCsv = new TjCsv($config);
		$TjCsv->CsvExport($result);
	}

	/**
	 * Upload video
	 *
	 * @return void
	 * since 1.7
	 */
	public function videoUpload()
	{
		$model    = $this->getModel('campaign');
		$response = $model->videoUpload();

		// Output json response
		header('Content-type: application/json');
		echo json_encode($response);

		jexit();
	}

	/**
	 * Check if all the parts exist, and gather all the parts of the file together
	 *
	 * @param   String  $temp_dir   The temporary directory holding all the parts of the file
	 * @param   String  $fileName   The original file name
	 * @param   String  $chunkSize  Each chunk size (in bytes)
	 * @param   String  $totalSize  Original file size (in bytes)
	 *
	 * @return void
	 * since 1.7
	 */
	public function createFileFromChunks($temp_dir, $fileName, $chunkSize, $totalSize)
	{
		// Count all the parts of this file
		$total_files = 0;

		foreach (scandir($temp_dir) as $file)
		{
			if (stripos($file, $fileName) !== false)
			{
				$total_files++;
			}
		}

		// Check that all the parts are present the size of the last part is between chunkSize and 2*$chunkSize
		if ($total_files * $chunkSize >= ($totalSize - $chunkSize + 1))
		{
			// Create the final destination file
			if (($fp = fopen(JPATH_SITE . '/tmp/' . $fileName, 'w')) !== false)
			{
				for ($i = 1; $i <= $total_files; $i++)
				{
					fwrite($fp, file_get_contents($temp_dir . '/' . $fileName . '.part' . $i));
				}

				fclose($fp);
			}
			else
			{
				return false;
			}

			// Rename the temporary directory (to avoid access from other concurrent chunks uploads) and than delete it
			if (rename($temp_dir, $temp_dir . '_UNUSED'))
			{
				$this->rrmdir($temp_dir . '_UNUSED');
			}
			else
			{
				$this->rrmdir($temp_dir);
			}
		}

		// Lets make a unique safe file name for each upload
		$name     = JPATH_SITE . '/tmp/' . $fileName;
		$fileInfo = pathinfo($name);

		// File extension
		$fileExt  = $fileInfo['extension'];

		// Base name
		$fileBase = $fileInfo['filename'];

		// Add logggedin userid to file name
		$fileBase = JFactory::getUser()->id . '_' . $fileBase;

		/*
		 Add timestamp to file name
		 http://www.php.net/manual/en/function.microtime.php
		 http://php.net/manual/en/function.uniqid.php
		 microtime â�� Return current Unix timestamp with microseconds
		 uniqid â�� Generate a unique ID
		 */

		$timestamp = microtime();

		$fileBase = $fileBase . '_' . $timestamp;

		// Clean up filename to get rid of strange characters like spaces etc
		$fileBase = JFile::makeSafe($fileBase);

		// Lose any special characters in the filename
		$fileBase = preg_replace("/[^A-Za-z0-9]/i", "_", $fileBase);

		// Use lowercase
		$fileBase = strtolower($fileBase);

		$fileName = $fileBase . '.' . $fileExt;

		rename($name, JPATH_SITE . '/tmp/' . $fileName);

		return $fileName;
	}

	/**
	 * Delete a directory RECURSIVELY
	 *
	 * @param   String  $dir  Directory path
	 *
	 * @link http://php.net/manual/en/function.rmdir.php
	 *
	 * @return   boolean
	 *
	 * since 1.7
	 */
	public function rrmdir($dir)
	{
		if (is_dir($dir))
		{
			$objects = scandir($dir);

			foreach ($objects as $object)
			{
				if ($object != "." && $object != "..")
				{
					if (filetype($dir . "/" . $object) == "dir")
					{
						$this->rrmdir($dir . "/" . $object);
					}
					else
					{
						unlink($dir . "/" . $object);
					}
				}
			}

			reset($objects);
			rmdir($dir);
		}
	}

	/**
	 * Delete video
	 *
	 * @return   boolean
	 *
	 * since 1.7
	 */
	public function deleteVideo()
	{
		$app = JFactory::getApplication();

		// Fetch value from ajax call
		$videoid = $app->input->get('videoid');
		$type    = $app->input->get('type');

		$model = $this->getModel('campaign');
		echo $deleteGalleryVideos = $model->deleteGalleryVideos($videoid, $type);

		jexit();
	}

	/**
	 * Mark video as campaign default video
	 *
	 * @return   boolean
	 *
	 * since 1.7
	 */
	public function setDefaultVideo()
	{
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		// Fetch value from ajax call
		$videoid = $post->get('videoid', '', 'INT');
		$camp_id = $post->get('camp_id', '', 'INT');
		$type    = $post->get('type', '', 'STRING');

		$model = $this->getModel('campaign');

		echo $model->setDefaultVideo($videoid, $camp_id, $type);

		jexit();
	}

	/**
	 * Method for getting Campaign specific donation and average donation data for showing graph
	 *
	 * @return   json
	 *
	 * since 2.0
	 */
	public function getCampaignGraphData()
	{
		$params                     = JComponentHelper::getParams('com_jgive');
		$currency                   = $params->get('currency_symbol');
		$input = JFactory::getApplication()->input;

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$this->techjoomlacommon = new TechjoomlaCommon;
		$lastTwelveMonth = $this->techjoomlacommon->getLastTwelveMonths();

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;

		// Creating Object of FrontendHelper class
		$jgiveFrontendHelper = new jgiveFrontendHelper;

		$duration = $input->get('filtervalue');
		$id = $input->get('cid');

		$campDetailData = $campaignHelper->getCampaignDetails($id);

		$model = $this->getModel('campaign');

		$results = $model->getGarphData($duration, $id);

		if ($duration == 0)
		{
			$graphDuration = 7;
		}
		elseif ($duration == 1)
		{
			$graphDuration = 30;
			$arraychunkvar = 7;
		}
		elseif ($duration == 2)
		{
			$arraychunkvar = 30;

			$todate = JFactory::getDate(date('Y-m-d'))->Format(JText::_('Y-m-d'));
			$backdate = date('Y-m-d', strtotime(date('Y-m-d') . ' - 1 year'));
			$graphDuration  = $campaignHelper->getDateDiffInDays($backdate, $todate);
		}

		$totalDonationAmt = 0;

		foreach ($results as $result)
		{
			$totalDonationAmt += $result->donation_amount;
		}

		if ($duration == 0 || $duration == 1)
		{
			for ($i = 0; $i < $graphDuration; $i++)
			{
				$graphDataArr['donationDate'][$i] = date("Y-m-d", strtotime($i . " days ago"));
				$graphDataArr['donationAvg'][$i] = $totalDonationAmt / $graphDuration;

				if (!empty($results))
				{
					for ($j = 0; $j < count($results); $j++)
					{
						if ($graphDataArr['donationDate'][$i] == $results[$j]->cdate)
						{
							$graphDataArr['donationAmount'][$i] = $results[$j]->donation_amount;

							break;
						}
						else
						{
							$graphDataArr['donationAmount'][$i] = "0";
						}
					}
				}
				else
				{
					$graphDataArr['donationAmount'][$i] = "0";
				}
			}
		}
		elseif ($duration == 2)
		{
			for ($i = 0; $i < count($lastTwelveMonth); $i++)
			{
				$graphDataArr['donationDate'][$i] = $lastTwelveMonth[$i]['month'];
				$graphDataArr['donationAvg'][$i] = $totalDonationAmt / $graphDuration;

				if (!empty($results))
				{
					for ($j = 0; $j < count($results); $j++)
					{
						$monthNum  = $results[$j]->MONTHSNAME;
						$dateObj   = DateTime::createFromFormat('!m', $monthNum);
						$monthName = $dateObj->format('F');

						if ($lastTwelveMonth[$i]['month'] == $monthName)
						{
							$graphDataArr['donationAmount'][$i] = $results[$j]->donation_amount;
							break;
						}
						else
						{
							$graphDataArr['donationAmount'][$i] = "0";
						}
					}
				}
				else
				{
					$graphDataArr['donationAmount'][$i] = "0";
				}
			}
		}

		$avgDonation = $totalDonationAmt / $graphDuration;

		if ($campDetailData->type == 'donation')
		{
			$graphDataArr['totalDonation'] = JText::_("COM_JGIVE_CAMPAIGN_SINGLE_TOTAL_DONATION") . $currency . @number_format($totalDonationAmt, 2, '.', ',');
			$graphDataArr['avgDonation'] = JText::_("COM_JGIVE_CAMPAIGN_SINGLE_AVG_DONATION") . $currency . @number_format($avgDonation, 2, '.', ',');
		}
		else
		{
			$graphDataArr['totalDonation'] = JText::_("COM_JGIVE_CAMPAIGN_SINGLE_TOTAL_INVESTMENT") . $currency .
			@number_format($totalDonationAmt, 2, '.', ',');
			$graphDataArr['avgDonation'] = JText::_("COM_JGIVE_CAMPAIGN_SINGLE_AVG_INVESTMENT") . $currency . @number_format($avgDonation, 2, '.', ',');
		}

		if ($duration == 1)
		{
			$graphDonationAmount = array_chunk($graphDataArr['donationAmount'], $arraychunkvar);

			$graphDonationAmountNewArr = [];

			$graphDonationAvgAmount = array_chunk($graphDataArr['donationAvg'], $arraychunkvar);
			$graphDonationAvgAmountNewArr = [];

			for ($i = 0; $i < count($graphDonationAmount); $i++)
			{
				$graphDonationAmountNewArr[] = array_sum($graphDonationAmount[$i]);
				$graphDataArr['donationAmount'] = $graphDonationAmountNewArr;

				// Avg Donation divide in chunk
				$graphDonationAvgAmountNewArr[] = array_sum($graphDonationAvgAmount[$i]);
				$graphDataArr['donationAvg'] = $graphDonationAvgAmountNewArr;
			}

			$graphDonationDate = array_chunk($graphDataArr['donationDate'], $arraychunkvar);
			$graphDonationDateNewArr = [];

			for ($i = 0; $i < count($graphDonationDate); $i++)
			{
				$graphDonationDateNewArr[] = reset($graphDonationDate[$i]);
				$graphDataArr['donationDate'] = $graphDonationDateNewArr;
			}
		}

		if ($duration == 0)
		{
			for ($k = 0; $k < count($graphDataArr['donationDate']); $k++)
			{
				$graphDataArr['donationDate'][$k] = date("D", strtotime($graphDataArr['donationDate'][$k]));
			}
		}
		elseif ($duration == 1)
		{
			for ($k = 0; $k < count($graphDataArr['donationDate']); $k++)
			{
				$graphDataArr['donationDate'][$k] = date("d/m", strtotime($graphDataArr['donationDate'][$k]));
			}
		}

		if ($duration == 0 || $duration == 1)
		{
			$graphDataArr['donationAvg'] = array_reverse($graphDataArr['donationAvg']);
			$graphDataArr['donationAmount'] = array_reverse($graphDataArr['donationAmount']);
			$graphDataArr['donationDate'] = array_reverse($graphDataArr['donationDate']);
		}

		echo json_encode($graphDataArr);
		jexit();
	}

	/**
	 * Method for Changing the feature camp
	 *
	 * @return   json
	 *
	 * since 2.0
	 */
	public function changeCampFeatureStatus()
	{
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$model = $this->getModel('campaign');
		$result = $model->campFeatureStatusChanged($post);

		echo json_encode($result);
		jexit();
	}

	/**
	 * Function to save text activity
	 *
	 * @return  object  activities
	 *
	 * @since   1.6
	 */
	public function addPostedActivity()
	{
		$input = JFactory::getApplication()->input;

		$activityData = array();
		$postText = $input->get('activity-post-text', '', 'STRING');
		$cid = $input->get('cid', '0', 'INT');
		$activityData['postData'] = $postText;
		$activityData['type'] = 'text';
		$activityData['cid'] = $cid;

		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$itemid   = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=campaign&layout=single');
		$redirect = JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cid . '&Itemid=' . $itemid, false);

		if (!empty($activityData['postData']))
		{
			// Trigger jgiveactivity plugin to add test activity
			$dispatcher = JDispatcher::getInstance();
			JPluginHelper::importPlugin('system');

			$result = $dispatcher->trigger('postActivity', array($activityData));

			if (empty($result[0]['error']))
			{
				$msg = JText::_("COM_JGIVE_TEXT_ACTIVITY_POST_SUCCESS_MSG");
				$this->setRedirect($redirect, $msg);
			}
			else
			{
				$msg = JText::_("COM_JGIVE_TEXT_ACTIVITY_POST_GUEST_ERROR_MSG");
				$this->setRedirect($redirect, $msg, 'error');
			}
		}
	}
}
