<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * JgiveControllerDonors controller class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveControllerDonors extends JgiveController
{
	/**
	 * Method &getModel.
	 *
	 * @param   String  $name    Name
	 * @param   String  $prefix  Prefix
	 * @param   Array   $config  Config
	 *
	 * @return model
	 *
	 * @since    1.8.1
	 */
	public function &getModel($name = 'Donors', $prefix = 'JgiveModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}

	/**
	 * Method to take id and redirect to contact us view
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function redirectforEmail()
	{
		$mainframe = JFactory::getApplication();
		$input = $mainframe->input;
		$cids = $input->get('cid', '', 'POST', 'ARRAY');
		$itemid = $input->get('Itemid', '', 'POST', 'INT');

		$session = JFactory::getSession();
		$session->set('selected_donor_item_ids', $cids);
		$contact_ink = JRoute::_('index.php?option=com_jgive&view=donors&layout=contact_us&Itemid=' . $itemid, false);
		$mainframe->redirect($contact_ink);
	}

	/**
	 * Method to get email for send mail to users
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function emailtoSelected()
	{
		$mainframe     = JFactory::getApplication();
		$input         = $mainframe->input;
		$selected_ids  = $input->get('selected_emails', '', 'POST', 'STRING');
		$subject       = $input->get('jgive_subject', '', 'POST', 'STRING');
		$body          = JRequest::getVar('jgive_message', '', 'post', 'string', JREQUEST_ALLOWHTML);
		$itemid        = $input->get('Itemid', '', 'POST', 'INT');
		$img_path      = 'img src="' . JUri::root();

		$res           = new stdClass;
		$res->content  = str_replace('img src="' . JUri::root(), 'img src="', $body);
		$res->content  = str_replace('img src="', $img_path, $res->content);
		$res->content  = str_replace("background: url('" . JUri::root(), "background: url('", $res->content);
		$res->content  = str_replace("background: url('", "background: url('" . JUri::root(), $res->content);

		$cid = explode(",", $selected_ids);

		$email_id = array_unique($cid);

		$model = $this->getModel('donors');
		$msg = JText::_('COM_JGIVE_EMAIL_SUCCESSFUL');

		$result = $model->emailtoSelected($email_id, $subject, $body, $attachmentPath = '');

		if ($result == 1)
		{
			$msg = JText::_('COM_JGIVE_EMAIL_SUCCESSFUL');
		}
		else
		{
			$msg = $model->getError();
		}

		$contact_ink = JRoute::_(JUri::base() . 'index.php?option=com_jgive&view=donors&layout=default&Itemid=' . $itemid);
		$mainframe->redirect($contact_ink, $msg);
	}

	/**
	 * Method to Cancel mail 
	 *
	 * @return  void
	 *
	 * @since   1.0
	 */
	public function cancelToMail()
	{
		$mainframe     = JFactory::getApplication();
		$input         = $mainframe->input;
		$itemid        = $input->get('Itemid', '', 'POST', 'INT');

		$contact_ink = 'index.php?option=com_jgive&view=donors&Itemid=' . $itemid;

		$contact_ink = JUri::root() . substr(JRoute::_($contact_ink), strlen(JUri::base(true)) + 1);
		$mainframe->redirect($contact_ink);
	}
}
