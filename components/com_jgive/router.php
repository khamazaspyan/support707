<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2017 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

if (!class_exists('campaignHelper'))
{
	JLoader::register('campaignHelper', $helperPath);
	JLoader::load('campaignHelper');
}

/**
 * Routing class from com_jgive
 *
 * @subpackage  com_jgive
 *
 * @since       2.0
 */
class JGiveRouter extends JComponentRouterBase
{
	private $views = array('campaigns', 'dashboard', 'donations', 'donors', 'reports');

	private $views_needing_campaignId = array('campaign');

	private $views_needing_donationId = array('donations');

	/**
	 * Build the route for the com_content component
	 *
	 * @param   array  &$query  An array of URL arguments
	 *
	 * @return  array  The URL arguments to use to assemble the subsequent URL.
	 *
	 * @since   1.7.1
	 */
	public function build(&$query)
	{
		$segments = array();

		// Get a menu item based on Itemid or currently active
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$params = JComponentHelper::getParams('com_jgive');
		$db = JFactory::getDbo();

		// We need a menu item.  Either the one specified in the query, or the current active one if none specified
		if (empty($query['Itemid']))
		{
			$menuItem = $menu->getActive();
			$menuItemGiven = false;
		}
		else
		{
			$menuItem = $menu->getItem($query['Itemid']);
			$menuItemGiven = true;
		}

		// Check again
		if ($menuItemGiven && isset($menuItem) && $menuItem->component != 'com_jgive')
		{
			$menuItemGiven = false;
			unset($query['Itemid']);
		}

		// Check if view is set.
		if (isset($query['view']))
		{
			$view = $query['view'];
		}
		else
		{
			// We need to have a view in the query or it is an invalid URL
			return $segments;
		}

		if ($view == 'dashboard')
		{
			$segments[] = $query['view'];
			unset($query['view']);
		}
		elseif ($view == 'reports')
		{
			unset($query['view']);
		}
		elseif ($view == 'donors')
		{
			if (isset($query['layout']))
			{
				$segments[] = $query['layout'];
				unset($query['view']);
				unset($query['layout']);
			}
			else
			{
				unset($query['view']);
			}
		}
		elseif($view == 'campaign')
		{
			if (isset($query['cid']))
			{
				$campaignHelper = new campaignHelper;

				if ($query['layout'] == 'single' || $query['layout'] == 'create')
				{
					$db = JFactory::getDbo();

					$dbQuery = $db->getQuery(true)->select('alias')->from('#__jg_campaigns')->where('id=' . (int) $query['cid']);
					$db->setQuery($dbQuery);
					$alias = $db->loadResult();

					if (!empty($alias))
					{
						$segments[] = $query['cid'] . ':' . $alias;
					}
					else
					{
						$ctitle     = $campaignHelper->getCampaignTitleFromCid($query['cid']);
						$string     = preg_replace('/\s+/', '', $ctitle);
						$segments[] = $query['cid'] . ':' . strtolower($string);
					}

					unset($query['cid']);
				}

				if ($query['layout'] == 'single')
				{
					$segments[] = 'details';
				}

				if ($query['layout'] == 'create')
				{
					$segments[] = 'edit';
				}

				$segments[] = $query['view'];
				unset($query['layout']);
				unset($query['view']);
			}
			else
			{
				$segments[] = $query['layout'];
				$segments[] = $query['view'];
				unset($query['layout']);
				unset($query['view']);
			}
		}
		elseif($view == 'donations')
		{
			if (isset($query['layout']))
			{
				$segments[] = $query['view'];
				unset($query['view']);

				if ($query['layout'] == 'payment')
				{
					$segments[] = $query['layout'];
					$segments[] = $query['cid'];
					unset ($query['layout']);
					unset ($query['cid']);
				}
				elseif ($query['layout'] == 'details')
				{
					$segments[] = $query['layout'];
					$segments[] = $query['donationid'];
					unset ($query['layout']);
					unset ($query['donationid']);
				}
			}
			else
			{
				unset($query['view']);
			}
		}
		elseif($view == 'campaigns')
		{
			if ($query['layout'] == 'all')
			{
				$segments[] = $query['view'];
				$segments[] = $query['layout'];
			}

			if (!empty($query['filter_campaign_cat']))
			{
				$catId = (int) $query['filter_campaign_cat'];

				if ($catId)
				{
					$campaignHelper = new campaignHelper;
					$alias          = $campaignHelper->getCatalias($query['filter_campaign_cat']);
					$segments[]     = $alias;
					unset($query['filter_campaign_cat']);
					unset($query['view']);
				}
				else
				{
					$segments[] = '';
					unset($query['filter_campaign_cat']);
					unset($query['view']);
				}
			}

			unset($query['view']);
			unset ($query['layout']);
		}

		return $segments;
	}

	/**
	 * Parse the segments of a URL.
	 *
	 * @param   array  &$segments  The segments of the URL to parse.
	 *
	 * @return  array  The URL attributes to be used by the application.
	 *
	 * @since   2.0
	 */
	public function parse(&$segments)
	{
		$vars = array();
		$db = JFactory::getDbo();

		// Count route segments
		$count = count($segments);

		if ($count == 1)
		{
			if ($segments[0] == 'contact_us')
			{
				$vars['view'] = 'donors';
				$vars['layout'] = $segments[0];
			}

			if ($segments[0] == 'dashboard')
			{
				$vars['view'] = 'dashboard';
			}
		}
		else
		{
			if ($segments[0] == 'donations')
			{
				$vars['view'] = 'donations';
				$vars['layout'] = $segments[1];

				if ($segments[1] == 'details')
				{
					$vars['donationid'] = $segments[2];
				}

				if ($segments[1] == 'payment')
				{
					$vars['cid'] = $segments[2];
				}
			}
			elseif (in_array('campaign', $segments))
			{
				if ($segments[1] == 'details' || $segments[1] == 'edit')
				{
					$vars['view'] = 'campaign';

					if ($segments[1] == 'details')
					{
						$vars['layout'] = 'single';
					}
					else
					{
						$vars['layout'] = 'create';
					}

					$camp_det_arr = explode(':', $segments[0]);
					$vars['cid']  = $camp_det_arr[0];
				}
				else
				{
					$vars['view'] = 'campaign';
					$vars['layout'] = 'create';
				}
			}
			elseif ($segments[0] == 'campaigns')
			{
				$vars['view']   = 'campaigns';
				$vars['layout'] = 'all';

				if (isset($segments[2]))
				{
					$categoryTable = JTable::getInstance('Category', 'JTable', array('dbo', $db));
					$categoryTable->load(array('alias' => $segments[2], 'extension' => 'com_jgive'));

					if ($categoryTable->id)
					{
						$vars['filter_campaign_cat'] = $categoryTable->id;
					}
					else
					{
						$vars['filter_campaign_cat'] = '';
					}
				}
			}
		}

		return $vars;
	}
}
