<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
$jinput = JFactory::getApplication()->input;

require_once JPATH_SITE . '/components/com_jgive/helpers/campaign.php';
require_once JPATH_SITE . '/components/com_jgive/models/campaigns.php';

$campaignHelper = new campaignHelper;
$campaignsModel = new JgiveModelCampaigns;
$jgiveParams = $campaignsModel->getState();
$campaigns_to_show = $campaignHelper->campaignsToShowOptions();
$campaign_type_filter_options = $campaignsModel->getCampaignTypeFilterOptions();
$filter_org_ind_type = $campaignHelper->organization_individual_type();

$mainframe = JFactory::getApplication();

// Get itemids
$menu = $mainframe->getMenu();
$activeMenu = $menu->getActive();

if (!empty($activeMenu))
{
	$menuItemId = $activeMenu->id;
}

$singleCampaignItemid = !empty($menuItemId)?$menuItemId:'';

// Take option value
$com_jgive_option = $mainframe->input->get('option');
$lists['campaigns_to_show']      = $mainframe->getUserStateFromRequest("$com_jgive_option.campaigns_to_show", 'campaigns_to_show', '');
$lists['campaign_countries']     = $mainframe->getUserStateFromRequest("$com_jgive_option.campaign_countries", 'campaign_countries');
$lists['campaign_states']        = $mainframe->getUserStateFromRequest("$com_jgive_option.campaign_states", 'campaign_states');
$lists['campaign_city']          = $mainframe->getUserStateFromRequest("$com_jgive_option.campaign_city", 'campaign_city');
$lists['filter_org_ind_type']    = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_org_ind_type", 'filter_org_ind_type');
$lists['filter_org_ind_type_my'] = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_org_ind_type_my", 'filter_org_ind_type_my');
$lists['filter_campaign_type']   = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_campaign_type", 'filter_campaign_type');

$campaignsModel->user_filter_options        = $campaignsModel->getUserFilterOptions();
$campaignsModel->ordering_options           = $campaignsModel->getOrderingOptions();
$campaignsModel->ordering_direction_options = $campaignsModel->getOrderingDirectionOptions();

// For countries
$countryarray = array();
$countryarray[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_SELONE_COUNTRY'));
$campaign_countries = $campaignsModel->getcountries();

if (!empty ($campaign_countries))
{
	foreach ($campaign_countries  as $tmp)
	{
		$value  = $tmp->country_id;
		$option = $tmp->country;
		$countryarray[] = JHtml::_('select.option', $value, $option);
	}
}

$countryoption = $countryarray;

// For state
$statearray   = array();
$statearray[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_SELECT_STATE'));

// Get states
$campaign_states = $campaignsModel->getCampaignStates();

if (isset($campaign_states))
{
	foreach ($campaign_states  as $tmp)
	{
		$value        = $tmp->id;
		$option       = $tmp->region;
		$statearray[] = JHtml::_('select.option', $value, $option);
	}
}

$campaign_states = $statearray;

// For city
$cityarray   = array();
$cityarray[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_SELECT_CITY'));

// Get city
$campaign_city = $campaignsModel->getCampaignCity();

if (isset($campaign_city))
{
	foreach ($campaign_city  as $tmp)
	{
		if ($tmp->id )
		{
			$value       = $tmp->id;
			$option      = $tmp->city;
			$cityarray[] = JHtml::_('select.option', $value, $option);
		}
		elseif (empty($tmp->id) && $tmp->othercity)
		{
			$value       = $tmp->othercity;
			$option      = $tmp->othercity;
			$cityarray[] = JHtml::_('select.option', $value, $option);
		}
	}
}

$campaign_city = $cityarray;

?>
	<div class="jgive_filters">
		<form action="" method="post" name="adminForm3" id="adminForm3">
			<input type="hidden" name="option" value="com_jgive" />
			<input type="hidden" name="view" value="campaigns" />
			<input type="hidden" name="layout" value="all" />

			<div class="panel-group" id="accordion">
				<!-- Quick Search -->
				<div>
					<div><b><?php echo JText::_('COM_JGIVE_CAMPAIGNS_TO_SHOW'); ?></b></div>
					<ul class="inline">
						<?php
							$selected = $jinput->get('campaigns_to_show', '', 'string');

							$camps_quick = 'index.php?option=com_jgive&view=campaigns&layout=all&campaigns_to_show=&Itemid=' . $singleCampaignItemid;

							$camps_quick = JUri::root() . substr(JRoute::_($camps_quick), strlen(JUri::base(true)) + 1);

						?>

						<li>
							<label>
							<input type="radio"
							class="<?php echo empty($selected) ? 'active': ''; ?>"
							name="<?php echo 'quick_search[]';?>"
							id="quicksearchfields"
							<?php echo empty($selected) ? 'checked': ''; ?>
							value="<?php echo JText::_('COM_JGIVE_RESET_FILTER_TO_ALL');?>"
							onclick='window.location.assign("<?php echo $camps_quick;?>")'/>
						<?php echo JText::_('COM_JGIVE_RESET_FILTER_TO_ALL'); ?></label></li></br>

						<?php
							for ($i = 1; $i < count($campaigns_to_show); $i ++)
							{
								$check = "";
								$selected = $campaigns_to_show[$i]->value;

								$camps_quick = 'index.php?option=com_jgive&view=campaigns&layout=all&campaigns_to_show=' . $selected . '&Itemid=' . $singleCampaignItemid;

								$camps_quick = JUri::root() . substr(JRoute::_($camps_quick), strlen(JUri::base(true)) + 1);

								if ($lists['campaigns_to_show'] == $selected)
								{
									$class = "active";
									$check = "checked";
								}
								else
								{
									$class = "";
								}
								?>
								<li>
									<label>
										<input type="radio" class="<?php echo $class; ?>" name="<?php echo 'quick_search[]';?>" id="quicksearchfields" <?php echo $check;?>
										value="<?php echo $campaigns_to_show[$i]->text; ?>" onclick='window.location.assign("<?php echo $camps_quick;?>")'/>
										<?php echo $campaigns_to_show[$i]->text; ?>
									</label>
								</li></br>
							<?php
							}
						?>
					</ul>
				</div>
				<!-- Quick Search E-->

				<!--  Filters s-->
				<?php
				$show_place_filter        = $jgiveParams->params->get('show_place_filter');
				$show_org_ind_type_filter = $jgiveParams->params->get('show_org_ind_type_filter');
				$filter_user              = $jinput->get('filter_user', '', 'INT');

				if ($show_place_filter || $show_org_ind_type_filter || $filter_user)
				{
				?>
					<div><b><?php echo JText::_('COM_JGIVE_FILTER_CAMPAIGNS');?></b></div>
					<div class="control-group">
						<?php if($show_org_ind_type_filter)
						{
							echo JHtml::_(
							'select.genericlist', $filter_org_ind_type, "filter_org_ind_type",
							'class="input-medium" size="1" onchange="this.form.submit();" name="filter_org_ind_type"',
							"value", "text", $lists['filter_org_ind_type']
							);
						} ?>

						<?php
						if($show_place_filter)
						{
							echo JHtml::_(
							'select.genericlist', $countryoption, "campaign_countries",
							'size="1" onchange="this.form.submit();" class="input-medium" name="campaign_countries"',
							"value", "text", $lists['campaign_countries']
							); ?>

							<?php echo JHtml::_(
							'select.genericlist', $campaign_states, "campaign_states",
							'size="1" onchange="this.form.submit();" class="input-medium" name="campaign_states"',
							"value", "text", $lists['campaign_states']
							);?>

							<?php echo JHtml::_(
							'select.genericlist', $campaign_city, "campaign_city",
							'size="1" onchange="this.form.submit();" class="input-medium" name="campaign_city"',
							"value", "text", $lists['campaign_city']
							);
						}?>
					</div>

				<?php
				}?>
				<!-- Filters More options e--->

				<!-- Campaign Type s-->
				<?php
				$campaignHelper = new campaignHelper;
				$campaign_type = $campaignHelper->filedToShowOrHide('campaign_type');

				if ($jgiveParams->params->get('show_type_filter') AND $campaign_type)
				{?>
					<div><b><?php echo JText::_('COM_JGIVE_CAMP_TYPE'); ?></b></div>
					<ul class="inline">
						<?php
						$camp_type = 'index.php?option=com_jgive&view=campaigns&layout=all&filter_campaign_type=&Itemid=' . $singleCampaignItemid;

						$camp_type = JUri::root() . substr(JRoute::_($camp_type), strlen(JUri::base(true)) + 1);
						$selectedType = $jinput->get('filter_campaign_type', '', 'string');?>
						<li>
							<label>
								<input type="radio"
								class="<?php echo empty($selectedType) ? 'active': ''; ?>"
								name="<?php echo 'camp_type[]';?>"
								id="campaign_type" value="" <?php echo empty($selectedType) ? 'checked': '';?>
								onclick='window.location.assign("<?php echo $camp_type;?>")'/>
								<?php echo JText::_('COM_JGIVE_RESET_FILTER_TO_ALL'); ?>
							</label>
						</li></br>

						<?php
						for ($i = 1; $i < count($campaign_type_filter_options); $i ++)
						{
							$chec = "";
							$selected = $campaign_type_filter_options[$i]->value;

							$camp_type = 'index.php?option=com_jgive&view=campaigns&layout=all&filter_campaign_type=' . $selected . '&Itemid=' . $singleCampaignItemid;

							$camp_type = JUri::root() . substr(JRoute::_($camp_type), strlen(JUri::base(true)) + 1);

							if ($selectedType == $campaign_type_filter_options[$i]->value)
							{
								$chec = 'checked';
							}

							if ($lists['filter_campaign_type'] == $selected)
							{
								$class = "active";
								$chec = "checked";
							}
							else
							{
								$class = "";
							}
							?>

							<li>
								<label>
									<input type="radio" class="<?php echo $class;?>" name="<?php echo 'camp_type[]';?>"
									id="campaign_type" value="" <?php echo $chec;?>
									onclick='window.location.assign("<?php echo $camp_type;?>")'/>
									<?php echo $campaign_type_filter_options[$i]->text; ?>
								</label>
							</li></br>
						<?php
						}
						?>
						</ul>
				<?php
				}?>
				<!-- Campaign Type e-->
			</div>
		</form>
	</div>

