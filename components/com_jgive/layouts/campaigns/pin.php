<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

JHtml::_('bootstrap.tooltip');

$campaign = $displayData['campaign'];
$images = $displayData['images'];
$otherData = $displayData['otherData'];

require_once JPATH_SITE . '/components/com_jgive/models/campaigns.php';
$jgiveModelCampaigns = new JgiveModelCampaigns;
$totalNoOfDonorsPerCamp = $jgiveModelCampaigns->getDonorsCountPerCamp($campaign->id);

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root(true) . '/media/com_jgive/css/jgive_bs3.css');

$campaignDetailUrl = JUri::root() . substr(
JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $campaign->id . '&Itemid=' . $otherData['allCampaignsItemid']),
strlen(JUri::base(true)) + 1
);

foreach ($images as $image)
{
	if ($image->gallery == 0)
	{
		$path      = 'images/jGive/';
		$fileParts = pathinfo($image->path);

		// If loop for old version compability (where img resize not available means no L , M ,S before image name)
		if (file_exists($path . $fileParts['basename']))
		{
			$campImg = JUri::root() . $path . $fileParts['basename'];
			break;
		}
		else
		{
			$campImg = JUri::root() . $path . 'M_' . $fileParts['basename'];
			break;
		}
	}
}
?>

<div class="jgive_pin_img">
	<a href="<?php echo $campaignDetailUrl;?>" title="<?php echo $campaign->title;?>" style="background:url('<?php echo $campImg;?>'); background-position: center center; background-size: cover; background-repeat: no-repeat;">
	</a>
</div>
<?php
if ($campaign->success_status == 1)
{
?>
	<div class="rubber_stamp">
		<span class="jgive_rubber_stamp_text">
			<?php echo JText::_('COM_JGIVE_SUCCESS_STAMP'); ?>
		</span>
	</div>
<?php
}?>
<div class="thumbnail">
	<div class="caption single_pin">
		<ul class="list-unstyled">
			<li>
				<?php
				if (strlen($campaign->title) > 20)
				{
				?>
					<a href="<?php echo  $campaignDetailUrl;?>" title="<?php echo $campaign->title;?>">
						<?php echo substr($campaign->title, 0, 20) . '...';?>
						<?php
						if ($campaign->featured == 1)
						{?>
							<span><i class="fa fa-star pull-right" aria-hidden="true"></i></span>
						<?php
						}?>
					</a>
				<?php
				}
				else
				{
				?>
					<a href="<?php echo  $campaignDetailUrl;?>" title="<?php echo $campaign->title;?>">
						<?php echo $campaign->title;?>
						<?php
						if ($campaign->featured == 1)
						{?>
							<span><i class="fa fa-star pull-right" aria-hidden="true"></i></span>
						<?php
						}?>
					</a>
				<?php
				}
				?>
			</li>
			<li class="text-muted">
				<?php echo ($campaign->type == 'donation') ? JText::_('COM_JGIVE_DONATION') : JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT');?>
				<a href="<?php echo  $campaignDetailUrl;?>" title="<?php echo $campaign->title;?>">
					<span><i class="fa fa-chevron-right pull-right" aria-hidden="true"></i></span>
				</a>

			</li>

			<?php
				$donated_per = 0;

				$goal_amount = (float) $campaign->goal_amount;

				if (!empty($campaign->amount_received) && $goal_amount > 0)
				{
					$donated_per = ($campaign->amount_received / $campaign->goal_amount) * 100;
				}

				$donated_per = number_format((float) $donated_per, 2, '.', '');
			?>
			<li><h4><?php echo $donated_per;?>%</h4></li>
			<li>
				<div class="progress" style="margin-bottom: 10px;">
					<div class="progress-bar progress-bar-success "
					role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="50" style="width: <?php echo $donated_per . '%';?>">
					</div>
				</div>
			</li>
			<li>
				<?php
					// Calculate days left
						$date_expire = 0;
						$curr_date = JFactory::getDate()->Format(JText::_('Y-m-d'));
						$end_date  = JFactory::getDate($campaign->end_date)->Format(JText::_('Y-m-d'));

						if ($curr_date > $end_date)
						{
							$date_expire = 1;
						}

						$time_curr_date  = strtotime($curr_date);
						$time_end_date   = strtotime($campaign->end_date);
						$interval        = ($time_end_date - $time_curr_date);

						$days_left = floor($interval / (60 * 60 * 24));

						$days = '';
						$lable = JText::_('COM_JGIVE_DAYS_LEFT');

						if ((int) ($time_curr_date) && (int) ($time_end_date))
						{
							$days = JText::_('COM_JGIVE_DAYS_LEFT');
						}

						if ($date_expire)
						{
							$days = JText::_('COM_JGIVE_NA');
						}
						elseif((int) ($time_curr_date) && (int) ($time_end_date))
						{
							if ((int) $days_left == 0 )
							{
								// Only one day left
								$days = 1;
							}
							else
							{
								$days = $days_left > 0 ?  $days_left: JText::_('COM_JGIVE_NA');
							}
						}
				?>
				<?php
				if ($days === "No" || $days === "NA")
				{
					echo JText::_('COM_JGIVE_CAMPAIGN_CLOSED');
				}
				else
				{
				?>
				<?php echo $days;?>&nbsp;
				<?php echo JText::_('COM_JGIVE_LAYOUT_DAYS'); ?>&nbsp;
				<?php echo JText::_('COM_JGIVE_DAYS__REMAINING');
				}?>
			</li>

			<li><h4><?php echo $totalNoOfDonorsPerCamp;?></h4></li>
			<li>
				<span class="text-capitalize text-muted">
					<?php echo ($campaign->type == 'donation') ? JText::_('COM_JGIVE_NO_OF_DONORS') : JText::_('COM_JGIVE_INVESTORS');?>
				</span>
			</li>
			<?php

			if ($campaign->donteButtonStatusFlag == 0)
			{
			?>
				<input type="button"
					class="btn btn-default btn-md form-control pull-right disabled"
					value="<?php echo (($campaign->type == 'donation') ? JText::_('COM_JGIVE_DONATIONS_CLOSED') : JText::_('COM_JGIVE_INVESTMENTS_CLOSED'));?>"/>
			<?php
			}
			elseif($campaign->donteButtonStatusFlag == -1)
			{
			?>
				<input type="button"
					class="btn btn-default btn-md disabled pull-right"
					value="<?php echo JText::_("COM_JGIVE_WILL_START_SOON"); ?>"/>
			<?php
			}
			elseif($campaign->donteButtonStatusFlag == 1)
			{
			?>
				<form action="" method="post" name="donationform" id="donationform">
					<input type="hidden" name="cid" id="cid" value="<?php echo $campaign->id;?>">
					<li>
						<button type="submit" class="btn btn-primary btn-md pull-right" id="allcampdonate"
							title="<?php echo ($campaign->type == 'donation') ? JText::_('COM_JGIVE_BUTTON_DONATE_TOOLTIP') : JText::_('COM_JGIVE_BUTTON_INVEST_TOOPTIP');?>">
							<?php echo ($campaign->type == 'donation') ? JText::_('COM_JGIVE_BUTTON_DONATE') : JText::_('COM_JGIVE_BUTTON_INVEST');?>
						</button>
					</li>
					<input type="hidden" name="option" value="com_jgive">
					<input type="hidden" name="task" value="donations.donate">
				</form>
			<?php
			}
			?>
		</ul>
		<div class="clearfix"></div>
	</div>
</div>















