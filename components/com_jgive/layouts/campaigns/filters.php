<?php
/**
 * @package    Jgive
 * @author     TechJoomla <extensions@techjoomla.com>
 * @website    http://techjoomla.com*
 * @copyright  Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
$campaignHelper = new campaignHelper;
$campaign_type = $campaignHelper->filedToShowOrHide('campaign_type');
$show_org_ind_type_filter = $displayData['params']['show_org_ind_type_filter'];
jimport('joomla.application.module.helper');
jimport( 'joomla.html.parameter' );

$document = JFactory::getDocument();
$renderer = $document->loadRenderer('module');
$com_params   = JComponentHelper::getParams('com_jgive');
$modules  = JModuleHelper::getModules('tj-filters-mod-pos');

if ($modules)
{
	$moduleParams = new JRegistry($modules['0']->params);
	$params   = array();
	if ($displayData['params']['show_filters'] == 1)
	{
		if ($moduleParams->get('client_type') == "com_jgive.campaign")
		{
			foreach ($modules as $module)
			{
				echo $renderer->render($module, $params);
			}
		}
		else
		{
			echo JText::_('COM_JGIVE_CAMP_NOTICE');
		}
	}
}
elseif ($displayData['params']['show_filters'] == 1)
{
?>
	<form action="" name="campaignFilterform" method="post" id="campaignFilterform">
<?php
	if ($displayData['params']['show_category_filter'] == 1)
	{?>
		<div class="col-xs-12 col-sm-3 campaignsCatFilterwrapper">
			<p class="text-muted"><?php echo JText::_('COM_JGIVE_CATEGORY');?></p>
			<?php
			$cat_url = 'index.php?option=com_jgive&view=campaigns&layout=all&filter_campaign_cat=&Itemid=' . $displayData['otherData']['allCampaignsItemid'];
			$cat_url = JUri::root() . substr(JRoute::_($cat_url), strlen(JUri::base(true)) + 1);
			if($displayData['list']['filter_campaign_cat'] == 0)
			{
				$class = "active";
				$checkVal = "checked";
			}
			else
			{
				$class = "";
				$checkVal = "";
			}
			?>

			<div class="<?php echo $class; ?>">
				<label>
					<input type="radio" class=""
					name="<?php echo 'filter_campaign_cat';?>"
					id="filter_campaign_cat" <?php echo $checkVal;?>
					value=""
					onclick='window.location.assign("<?php echo $cat_url;?>")'/>
					<?php echo JText::_('COM_JGIVE_ALL');?>
				</label>
			</div>
			<?php
			foreach ($displayData['categories'] as $category)
			{
				$check = "";
				$selected = $category->value;

				$camps_quick = 'index.php?option=com_jgive&view=campaigns&layout=all&filter_campaign_cat=' . $selected . '&Itemid=' . $displayData['otherData']['allCampaignsItemid'];
				$camps_quick = JUri::root() . substr(JRoute::_($camps_quick), strlen(JUri::base(true)) + 1);

				if ($displayData['list']['filter_campaign_cat'] == $selected)
				{
					$class = "active";
					$check = "checked";
				}
				else
				{
					$class = "";
				}
			?>
				<div class="<?php echo $class; ?>">
					<label>
						<input type="radio" class=""
						name="<?php echo 'filter_campaign_cat';?>"
						id="filter_campaign_cat" <?php echo $check;?>
						value="<?php echo $category->value; ?>"
						onclick='window.location.assign("<?php echo $camps_quick;?>")'/>
						<?php echo $category->text; ?>
					</label>
				</div>
			<?php
			}
			?>
		</div>
	<?php
	}

	$basePath = JPATH_SITE . '/components/com_jgive/layouts/corefilters/horizontal/bs3';
	$layout = new JLayoutFile('corefilters', $basePath);
	$data = "";
	echo $layout->render($data);
	$selected = null;
	?>
	<div class="clearfix"></div>
	<div class="row">
		<?php

		$campaignsResetFilterUrl ='index.php?option=com_jgive&view=campaigns&layout=all&filter_campaign_cat=' . $selected . '&campaigns_to_show=' . $selected . '&filter_org_ind_type=' . $selected . '&filter_campaign_type=' . $selected . '&campaign_countries' . $selected . '&campaign_states' . $selected . '&campaign_city=' . $selected .'&Itemid=' . $displayData['otherData']['allCampaignsItemid'];

		$campaignsResetFilterUrl = JUri::root() . substr(JRoute::_($campaignsResetFilterUrl), strlen(JUri::base(true)) + 1);
		?>
		<div class="col-xs-12 col-sm-12">
			<a class="pull-right" onclick='window.location.assign("<?php echo $campaignsResetFilterUrl;?>")' href="javascript:void(0);">
				<i class="fa fa-repeat" aria-hidden="true"></i>
				<?php echo JText::_('COM_JGIVE_REST_FILTERS');?>
			</a>
		</div>
<!--
		<div class="col-xs-12 col-sm-12">
			<button type="button" onclick="document.campaignFilterform.submit();" class="btn btn-mini btn-primary pull-right">
				<?php echo JText::_('COM_JGIVE_DASHBOARD_APPLY_FILTER'); ?>
			</button>
		</div>
-->
	</div>
	<div class="clearfix"></div>
<?php
}?>
