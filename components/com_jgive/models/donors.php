<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * JgiveModelDonors model class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveModelDonors extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'user_id', 'a.user_id',
				'campaign_id', 'a.campaign_id',
				'email', 'a.email',
				'first_name', 'a.first_name',
				'last_name', 'a.last_name',
				'address', 'a.address',
				'address2', 'a.address2',
				'city', 'a.city',
				'state', 'a.state',
				'country', 'a.country',
				'zip', 'a.zip',
				'phone', 'a.phone',
				'created_by', 'a.created_by',
				'giveback_id', 'g.id',
				'donation_amount', 'o.amount',
				'cdate', 'o.cdate',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * @param   String  $ordering   Sorting Order
	 * @param   String  $direction  Direction
	 *
	 * @return void
	 *
	 * @since    1.8.1
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		parent::populateState('a.campaign_id', 'DESC');
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return    JDatabaseQuery
	 *
	 * @since    1.8.1
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		$user    = JFactory::getUser();
		$user_id = $user->id;

		// Select the required fields from the table.
		$query
			->select(
				$this->getState(
					'list.select', 'DISTINCT a.*'
				)
			);

		$query->from('`#__jg_donors` AS a');

		// Join over the foreign key 'campaign_id'
		$query->select('#__jg_campaigns_1525992.title AS campaigns_title_1525992');
		$query->select('g.id AS gid');
		$query->select('g.description AS gdesc');
		$query->select('sum(o.amount) AS donation_amount');
		$query->select('o.cdate AS cdate');
		$query->join('LEFT', '#__jg_campaigns AS #__jg_campaigns_1525992 ON #__jg_campaigns_1525992.id = a.campaign_id');
		$query->join('LEFT', '#__jg_donations AS d ON d.donor_id = a.id');
		$query->join('LEFT', '#__jg_campaigns_givebacks AS g ON g.id = d.giveback_id');
		$query->join('LEFT', '#__jg_orders AS o ON o.donor_id = a.id');

		$query->where('#__jg_campaigns_1525992.creator_id =' . $user_id);
		$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));
		$query->group($db->qn('a.email'));

		if (!JFactory::getUser()->authorise('core.edit.state', 'com_jgive'))
		{
			$query->where('a.state = 1');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				// Search record
				$search = $db->Quote('%' . $db->escape($search, true) . '%');

				$query->where('( a.first_name LIKE ' . $search .
						'  OR  a.last_name LIKE ' . $search .
						'  OR  a.email LIKE ' . $search .
						'  OR  CONCAT(a.first_name, " ", a.last_name )' . ' LIKE ' . $search . ' )'
				);
			}
		}

		// Filtering campaign_id
		$filter_campaign_id = $this->state->get("filter.campaign_id");

		if ($filter_campaign_id)
		{
			// Code for filter record
			$query->where("a.campaign_id = '" . $db->escape($filter_campaign_id) . "'");
		}

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Method used to get item data for displaying on donor list view page
	 *
	 * @return $items Data
	 *
	 * @since	1.8.1
	 */
	public function getItems()
	{
		$items   = parent::getItems();
		$user    = JFactory::getUser();
		$user_id = $user->id;
		$db = JFactory::getDbo();

		foreach ($items as $item)
		{
			if (isset($item->campaign_id) && $item->campaign_id != '')
			{
				if (is_object($item->campaign_id))
				{
					$item->campaign_id = JArrayHelper::fromObject($item->campaign_id);
				}

				$values = (is_array($item->campaign_id)) ? $item->campaign_id : explode(',', $item->campaign_id);
				$textValue = array();

				foreach ($values as $value)
				{
					// Fetch campaign title by id
					$query = $db->getQuery(true);
					$query->select($db->quoteName('title'))
							->from('`#__jg_campaigns`')
							->where($db->quoteName('id') . ' = ' . $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();

					if ($results)
					{
						$textValue[] = $results->title;
					}
				}

				$item->campaign_title = !empty($textValue) ? implode(', ', $textValue) : $item->campaign_id;
			}

			if (isset($item->country) && $item->country != '')
			{
				if (is_object($item->country))
				{
					$item->country = JArrayHelper::fromObject($item->country);
				}

				$values = (is_array($item->country)) ? $item->country : explode(',', $item->country);
				$textValue = array();

				foreach ($values as $value)
				{
					// Fetch country by id
					$query = $db->getQuery(true);
					$query->select($db->quoteName('country'))
							->from('`#__tj_country`')
							->where($db->quoteName('id') . ' = ' . $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();

					if ($results)
					{
						$textValue[] = $results->country;
					}
				}

				$item->country = !empty($textValue) ? implode(', ', $textValue) : $item->country;
			}

			if (isset($item->city) && $item->city != '')
			{
				if (is_object($item->city))
				{
					$item->city = JArrayHelper::fromObject($item->city);
				}

				$values = (is_array($item->city)) ? $item->city : explode(',', $item->city);
				$textValue = array();

				foreach ($values as $value)
				{
					// Fetch city by id
					$query = $db->getQuery(true);
					$query->select($db->quoteName('city'))
							->from('`#__tj_city`')
							->where($db->quoteName('id') . ' = ' . $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();

					if ($results)
					{
						$textValue[] = $results->city;
					}
				}

				$item->city = !empty($textValue) ? implode(', ', $textValue) : $item->city;
			}

			if (isset($item->state) && $item->state != '')
			{
				if (is_object($item->state))
				{
					$item->state = JArrayHelper::fromObject($item->state);
				}

				$values = (is_array($item->state)) ? $item->state : explode(',', $item->state);
				$textValue = array();

				foreach ($values as $value)
				{
					// Fetch city by id
					$query = $db->getQuery(true);
					$query->select($db->quoteName('region'))
							->from('`#__tj_region`')
							->where($db->quoteName('id') . ' = ' . $db->quote($db->escape($value)));
					$db->setQuery($query);
					$results = $db->loadObject();

					if ($results)
					{
						$textValue[] = $results->region;
					}
				}

				$item->state = !empty($textValue) ? implode(', ', $textValue) : $item->state;
			}
		}

		return $items;
	}

	/**
	 * Method loadFormData
	 *
	 * @return loadForm Data
	 *
	 * @since	1.8.1
	 */
	protected function loadFormData()
	{
		$app              = JFactory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;

		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && !$this->isValidDate($value))
			{
				$filters[ $key ]  = '';
				$error_dateformat = true;
			}
		}

		if ($error_dateformat)
		{
			$app->enqueueMessage(JText::_("COM_JGIVE_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	/**
	 * Method isValidDate
	 *
	 * @param   String  $date  Date
	 *
	 * @return void
	 *
	 * @since	1.8.1
	 */
	private function isValidDate($date)
	{
		return preg_match("/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/", $date) && date_create($date);
	}

	/**
	 * Method used to find out count of campaign whach has created by logged user
	 *
	 * @return campaignsId
	 *
	 * @since	1.8.1
	 */
	public function getCampaignId()
	{
		// Fetching getCampaignIds function for campaign id
		if (!class_exists('JgiveModelDashboard'))
		{
			JLoader::register('JgiveModelDashboard', JPATH_SITE . '/components/com_jgive/models/dashboard.php');
			JLoader::load('JgiveModelDashboard');
		}

		$campaignsId = new JgiveModelDashboard;
		$campaignsId = $campaignsId->getCampaignIds();

		return $campaignsId;
	}

	/**
	 * Method to get email-id from donors table by using id.
	 *
	 * @param   String  $donors_id  Pass the Donor Id for getting email
	 *
	 * @return result
	 *
	 * @since    1.8.1
	 */
	public function getDonorsEmail($donors_id)
	{
		$db = JFactory::getDBO();
		$email_array = array();

		foreach ($donors_id AS $donor_id)
		{
			$query = $db->getQuery(true);
			$query = "SELECT id from #__jg_donors where id=" . $donor_id;
			$db->setQuery($query);
			$donor_id = $db->loadResult($query);

			if (!$donor_id)
			{
				continue;
			}

			$query = "SELECT email from #__jg_donors where  id=" . $donor_id;
			$db->setQuery($query);
			$email = $db->loadResult($query);

			if ($email)
			{
				$email_array[] = $email;
			}
		}

		return array_unique($email_array);
	}

	/**
	 * Method to get mail from controller for send mail to user.
	 *
	 * @param   String  $email_id        Email-Id
	 * @param   String  $subject         Email Subject
	 * @param   String  $message         Email Message
	 * @param   String  $attachmentPath  Attachment FIle
	 *
	 * @return result
	 *
	 * @since    1.8.1
	 */
	public function emailtoSelected($email_id, $subject, $message, $attachmentPath = '')
	{
		$donationhelper = new DonationsHelper;
		$com_params     = JComponentHelper::getParams('com_jgive');
		$replytoemail   = $com_params->get('reply_to');

		$where     = '';
		$app       = JFactory::getApplication();
		$mailfrom  = $app->getCfg('mailfrom');
		$fromname  = $app->getCfg('fromname');
		$sitename  = $app->getCfg('sitename');

		if (isset($replytoemail))
		{
			$replytoemail = explode(",", $replytoemail);
		}

		if (is_array($email_id))
		{
			foreach ($email_id AS $email)
			{
				// If donor is deleted dont send reminder
				if (!$email)
				{
					continue;
				}

				$result = $donationhelper->sendmail($email, $subject, $message, $html = 1);
			}
		}

		return $result;
	}

	/**
	 * Method to get number donors per campaign
	 *
	 * @param   Integer  $camp_id  Campaign Id
	 *
	 * @return array
	 *
	 * @since    2.0
	 */
	public function getDonorsPerCamp($camp_id)
	{
		$db      = JFactory::getDBO();
		$query   = $db->getQuery(true);

		// Donor Count who has registered
		$query->select('COUNT(DISTINCT d.user_id)');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->join('LEFT', $db->qn('#__jg_donors', 'd') . ' ON (' . $db->qn('d.id') . ' = ' . $db->qn('o.donor_id') . ')');
		$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('o.campaign_id') . ' = ' . $db->qn('c.id') . ')');
		$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));
		$query->where($db->qn('d.user_id') . '<>' . $db->quote('0'));
		$query->where($db->qn('c.id') . ' = ' . $db->quote($camp_id));

		$db->setQuery($query);
		$donarCountreg = $db->loadResult();

		$query   = $db->getQuery(true);

		// Donor Count who has guest
		$query->select('COUNT(DISTINCT d.email)');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->join('LEFT', $db->qn('#__jg_donors', 'd') . ' ON (' . $db->qn('d.id') . ' = ' . $db->qn('o.donor_id') . ')');
		$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('o.campaign_id') . ' = ' . $db->qn('c.id') . ')');
		$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));
		$query->where($db->qn('d.user_id') . '=' . $db->quote('0'));
		$query->where($db->qn('c.id') . ' = ' . $db->quote($camp_id));

		$db->setQuery($query);
		$donarCountguest = $db->loadResult();

		return $donarCountreg + $donarCountguest;
	}
}
