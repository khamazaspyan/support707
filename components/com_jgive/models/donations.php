<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die();
jimport('joomla.application.component.model');
jimport('techjoomla.common');

// Load videos class
require_once JPATH_SITE . '/components/com_jgive/models/donatevalidate.php';

/**
 * Donations form model class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveModelDonations extends JgiveModelDonateValidation
{
	protected $data;

	protected $total = null;

	protected $pagination = null;

	/**
	 * Class constructor.
	 *
	 * @since   1.8
	 */
	public function __construct()
	{
		$this->techjoomlacommon = new TechjoomlaCommon;
		parent::__construct();

		$mainframe  = JFactory::getApplication();
		$option = $mainframe->input->get('option');

		// Get the pagination request variables
		$limit      = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest($option . 'limitstart', 'limitstart', 0, 'int');

		// Set the limit variable for query later on
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}

	/**
	 * Method confirmpayment.
	 *
	 * @param   String  $pg_plugin  Plugin name.
	 * @param   Int     $oid        Order Id.
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function confirmpayment($pg_plugin, $oid)
	{
		$post            = JRequest::get('post');
		$comment_present = array_key_exists('comment', $post);

		if ($comment_present)
		{
			$this->saveComment($pg_plugin, $oid, $post['comment']);
		}

		$vars = $this->getPaymentVars($pg_plugin, $oid);

		if (!empty($post) && !empty($vars))
		{
			JPluginHelper::importPlugin('payment', $pg_plugin);
			$dispatcher = JDispatcher::getInstance();

			if ($vars->is_recurring == 1)
			{
				$result = $dispatcher->trigger('onTP_ProcessSubmitRecurring', array($post, $vars));
			}
			else
			{
				$result = $dispatcher->trigger('onTP_ProcessSubmit', array($post, $vars));
			}
		}
		else
		{
			JFactory::getApplication()->enqueueMessage(JText::_('COM_JGIVE_SOME_ERROR_OCCURRED'), 'error');
		}

		return true;
	}

	/**
	 * Method getDonations.
	 *
	 * @param   String  $pg_plugin  Plugin name.
	 * @param   Int     $oid        Order Id.
	 * @param   String  $comment    Comment.
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function saveComment($pg_plugin, $oid, $comment)
	{
		if ($oid)
		{
			$obj   = new stdClass;
			$db    = JFactory::getDBO();
			$query = "SELECT donation_id FROM #__jg_orders WHERE id =" . $oid;
			$db->setQuery($query);

			$obj->id      = $db->loadResult();
			$obj->comment = $comment;

			if ($obj->id)
			{
				if (!$db->updateObject('#__jg_donations', $obj, 'id'))
				{
					echo $db->stderr();
				}
			}
		}
	}

	/**
	 * Method getDonations.
	 *
	 * @return  Data Array.
	 *
	 * @since	1.8
	 */
	public function getDonations()
	{
		if (empty($this->data))
		{
			$query       = $this->_buildQuery();
			$this->data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->data;
	}

	/**
	 * Method _buildQuery.
	 *
	 * @return  query.
	 *
	 * @since	1.8
	 */
	public function _buildQuery()
	{
		$db = JFactory::getDBO();

		$mainframe = JFactory::getApplication();
		$option    = $mainframe->input->get('option');

		// Get the WHERE and ORDER BY clauses for the query
		$where = '';
		$where = $this->_buildContentWhere();

		$query = "SELECT i.id, i.order_id, i.fund_holder, i.status, i.processor,
		i.amount, i.fee, i.cdate, d.user_id AS donor_id, c.id AS cid, c.title,dona.comment
		FROM #__jg_orders AS i
		LEFT JOIN #__jg_campaigns AS c ON c.id=i.campaign_id
		LEFT JOIN #__categories as cat ON c.category_id=cat.id
		LEFT JOIN #__jg_donors AS d on d.id=i.donor_id
		LEFT JOIN #__jg_donations AS dona ON dona.id=i.donation_id" . $where;

		$filter_order     = $mainframe->getUserStateFromRequest($option . 'filter_order', 'filter_order', 'cdate', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'filter_order_Dir', 'filter_order_Dir', 'desc', 'word');

		if ($filter_order)
		{
			$qry1 = "SHOW COLUMNS FROM #__jg_orders";
			$db->setQuery($qry1);
			$exists1 = $db->loadobjectlist();

			foreach ($exists1 as $key1 => $value1)
			{
				$allowed_fields[] = $value1->Field;
			}

			if (in_array($filter_order, $allowed_fields))
			{
				$query .= " ORDER BY i.$filter_order $filter_order_Dir";
			}
		}

		return $query;
	}

	/**
	 * Method _buildContentWhere.
	 *
	 * @return  query.
	 *
	 * @since	1.8
	 */
	public function _buildContentWhere()
	{
		$mainframe = JFactory::getApplication();
		$option    = $mainframe->input->get('option');
		$layout    = $mainframe->input->get('layout', 'all');
		$cid       = $mainframe->input->get('cid', 0);

		$db             = JFactory::getDBO();
		$payment_status = $mainframe->getUserStateFromRequest($option . 'payment_status', 'payment_status', '', 'string');
		$where          = array();

		// Add filter for showing only logged in users donations
		if ($layout == 'my')
		{
			$me      = JFactory::getuser();
			$where[] = ' d.user_id=' . $me->id;
		}

		if ($payment_status != '-1' && !empty($payment_status))
		{
			$where[] = ' i.status = ' . $this->_db->Quote($payment_status);
		}

		if ($layout == 'all')
		{
			if ($cid != 0) // This is used when redirected from other view to this view
			{
				$where[] = " c.id=" . $cid;
			}
			else
			{
				$filter_campaign = $mainframe->getUserStateFromRequest($option . 'filter_campaign', 'filter_campaign', '', 'string');

				if ($filter_campaign != 0)
				{
					$where[] = " c.id=" . $filter_campaign;
				}

				$filter_campaign_type = $mainframe->getUserStateFromRequest($option . 'filter_campaign_type', 'filter_campaign_type', '', 'string');

				if (!empty($filter_campaign_type))
				{
					$where[] = " c.type='$filter_campaign_type'";
				}
			}
		}

		$campaign_cat = $mainframe->getUserStateFromRequest('com_jgive.campaign_cat', 'campaign_cat', '', 'INT');

		if (!empty($campaign_cat))
		{
			if (is_numeric($campaign_cat))
			{
				$cat_tbl = JTable::getInstance('Category', 'JTable');
				$cat_tbl->load($campaign_cat);
				$rgt       = $cat_tbl->rgt;
				$lft       = $cat_tbl->lft;
				$baselevel = (int) $cat_tbl->level;
				$where[]   = 'cat.lft >= ' . (int) $lft;
				$where[]   = 'cat.rgt <= ' . (int) $rgt;
			}
		}

		return $where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');
	}

	/**
	 * Method getTotal.
	 *
	 * @return  total.
	 *
	 * @since	1.8
	 */
	public function getTotal()
	{
		// Lets load the content if it doesn’t already exist
		if (empty($this->total))
		{
			$query        = $this->_buildQuery();
			$this->total = $this->_getListCount($query);
		}

		return $this->total;
	}

	/**
	 * Method getPagination.
	 *
	 * @return  pagination.
	 *
	 * @since	1.8
	 */
	public function getPagination()
	{
		// Lets load the content if it doesn’t already exist
		if (empty($this->pagination))
		{
			jimport('joomla.html.pagination');
			$this->pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->pagination;
	}

	/**
	 * Method getSingleDonationInfo.
	 *
	 * @return  donation_details Array.
	 *
	 * @since	1.8
	 */
	public function getSingleDonationInfo()
	{
		$order_id_key = JFactory::getApplication()->input->get('donationid');
		$path         = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$donationsHelper  = new donationsHelper;
		$donation_details = $donationsHelper->getSingleDonationInfo($order_id_key);

		return $donation_details;
	}

	/**
	 * Method setSessionCampaignId.
	 *
	 * @param   Integer  $cid          CID.
	 * @param   Integer  $giveback_id  GIVEN BACK ID
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function setSessionCampaignId($cid, $giveback_id = '')
	{
		$session = JFactory::getSession();
		$this->clearSessionCampaignId();
		$session->set('JGIVE_cid', $cid);

		if (!empty($giveback_id))
		{
			$session->set('JGIVE_giveback_id', $giveback_id);
		}

		return true;
	}

	/**
	 * Method clearSessionCampaignId.
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function clearSessionCampaignId()
	{
		$session = JFactory::getSession();
		$session->set('JGIVE_cid', '');

		return true;
	}

	/**
	 * Method getCampaignId.
	 *
	 * @return  cid.
	 *
	 * @since	1.8
	 */
	public function getCampaignId()
	{
		$input   = JFactory::getApplication()->input;
		$session = JFactory::getSession();
		$post    = JRequest::get('post');

		if (empty($post['cid']))
		{
			$cid = $session->get('JGIVE_cid');
		}
		else
		{
			$cid = $post['cid'];
		}

		if (empty($cid))
		{
			$cid = $input->get('cid', '', 'INT');
		}

		return $cid;
	}

	/**
	 * Method setSessionDonorData.
	 *
	 * @param   String  $post  Post
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function setSessionDonorData($post)
	{
		$session = JFactory::getSession();
		$session->set('JGIVE_cid', $post->get('cid', '', 'INT'));

		// Donor data
		$session->set('JGIVE_first_name', $post->get('first_name', '', 'STRING'));
		$session->set('JGIVE_last_name', $post->get('last_name', '', 'STRING'));
		$session->set('JGIVE_paypal_email', $post->get('paypal_email', '', 'STRING'));
		$session->set('JGIVE_address', $post->get('address', '', 'STRING'));
		$session->set('JGIVE_address2', $post->get('address2', '', 'STRING'));
		$session->set('JGIVE_city', $post->get('city', '', 'STRING'));
		$session->set('JGIVE_other_city', $post->get('other_city', '', 'STRING'));
		$session->set('JGIVE_state', $post->get('state', '', 'STRING'));
		$session->set('JGIVE_country', $post->get('country', '', 'STRING'));
		$session->set('JGIVE_zip', $post->get('zip', '', 'STRING'));
		$session->set('JGIVE_phone', $post->get('phone', '', 'STRING'));
		$session->set('JGIVE_donation_amount', $post->get('donation_amount', '', 'STRING'));
		$session->set('No_first_donation', 1);
		$session->set('JGIVE_user_first_last_name', $post->get('user_first_last_name', '', 'STRING'));

		return true;
	}

	/**
	 * Method addOrder.
	 *
	 * @param   String  $post  Post
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function addOrder($post)
	{
		// Get params
		$session              = JFactory::getSession();
		$campaignHelper       = new campaignHelper;
		$jgiveFrontendHelper  = new jgiveFrontendHelper;
		$params               = JComponentHelper::getParams('com_jgive');
		$commission_fee       = $params->get('commission_fee');
		$fixed_commission_fee = $params->get('fixed_commissionfee');

		if (empty($fixed_commission_fee))
		{
			$fixed_commission_fee = 0;
		}

		$send_payments_to_owner = $params->get('send_payments_to_owner');
		$db                     = JFactory::getDBO();
		$user                   = JFactory::getUser();

		// Get the user groupwise commision form params
		$params_usergroup = $params->get('usergroup');

		// Get campaign type donation/Investment & its creator
		$camp_details            = $campaignHelper->getCampaignType($post->get('cid', '', 'INT'));
		$campaign_creator        = JFactory::getUser($camp_details->creator_id);
		$camp_creator_groups_ids = $campaign_creator->groups;

		// Get logged user id
		$userid = $user->id;

		$this->guest_donation = $params->get('guest_donation');

		if ($this->guest_donation)
		{
			if (!$userid)
			{
				$userid = 0;

				$donor_registration = $post->get('account', '', 'STRING');

				if ($donor_registration == 'register')
				{
					$regdata['user_name']  = $post->get('paypal_email', '', 'STRING');
					$regdata['user_email'] = $post->get('paypal_email', '', 'STRING');

					$cb_params   = JComponentHelper::getParams('com_jgive');
					$integration = $cb_params->get('integration');

					if ($integration == 'cb')
					{
						$regdata['first_name'] = $post->get('first_name', '', 'STRING');
						$regdata['last_name']  = $post->get('last_name', '', 'STRING');
					}

					JLoader::import('registration', JPATH_SITE . '/components/com_jgive/models');
					$jgiveModelregistration = new jgiveModelregistration;
					$mesage                 = $jgiveModelregistration->store($regdata);

					if ($mesage)
					{
						$user   = JFactory::getUser();
						$userid = $user->id;
					}
					else
					{
						return -1;
					}
				}
				elseif (!($donor_registration == 'guest'))
				{
					return false;
				}

				$session->set('quick_reg_no_login', '1');
			}
		}
		elseif (!$userid)
		{
			$userid = 0;

			return false;
		}

		// Save donor details
		$obj = new stdClass;

		$JGIVE_order_id = $session->get('JGIVE_order_id');

		$obj->id = '';

		if (!empty($JGIVE_order_id))
		{
			$db    = JFactory::getDBO();
			$query = "SELECT donor_id
				FROM #__jg_orders
				WHERE id=" . $JGIVE_order_id;

			$db->setQuery($query);
			$obj->id = $db->loadResult();
		}

		$obj->user_id     = $userid;
		$obj->campaign_id = $post->get('cid', '', 'INT');

		$obj->email      = $post->get('paypal_email', '', 'STRING');
		$obj->first_name = $post->get('first_name', '', 'STRING');
		$obj->last_name  = $post->get('last_name', '', 'STRING');

		$user_first_last_name = $post->get('user_first_last_name', '', 'STRING');

		if (!empty($user_first_last_name))
		{
			$obj->first_name = $post->get('user_first_last_name', '', 'STRING');
		}

		$obj->address  = $post->get('address', '', '', 'STRING');
		$obj->address2 = $post->get('address2', '', 'STRING');

		$other_city_check = $post->get('other_city_check', '', 'STRING');

		if (!empty($other_city_check))
		{
			$obj->city = $post->get('other_city', '', 'STRING');
		}
		elseif (($post->get('city', '', 'STRING')) && ($post->get('city', '', 'STRING') != ''))
		{
			$obj->city = $post->get('city');
		}

		$obj->country = $post->get('country');

		$obj->state = $post->get('state');

		$obj->zip   = $post->get('zip', '', 'STRING');
		$obj->phone = $post->get('phone', '', 'STRING');

		if ($obj->id)
		{
			if (!$db->updateObject('#__jg_donors', $obj, 'id'))
			{
				echo $db->stderr();

				return false;
			}
		}
		elseif (!$db->insertObject('#__jg_donors', $obj, 'id'))
		{
			echo $db->stderr();

			return false;
		}

		// Get last insert id
		if ($obj->id)
		{
			$donors_key = $obj->id;
		}
		else
		{
			$donors_key = $db->insertid();
		}

		$obj     = new stdClass;
		$obj->id = '';

		if ($JGIVE_order_id)
		{
			$db    = JFactory::getDBO();
			$query = "SELECT donation_id FROM #__jg_orders WHERE id =" . $JGIVE_order_id;
			$db->setQuery($query);
			$obj->id = $db->loadResult();
		}

		$obj->campaign_id = $post->get('cid');
		$obj->donor_id    = $donors_key;

		if ($post->get('donation_type', '', 'INT'))
		{
			$obj->is_recurring        = 1;
			$obj->recurring_frequency = $post->get('recurring_freq', '', 'STRING');
			$obj->recurring_count     = $post->get('recurring_count', '', 'INT');
		}
		else
		{
			$obj->is_recurring        = 0;
			$obj->recurring_frequency = '';
			$obj->recurring_count     = '';
		}

		$obj->annonymous_donation = $post->get('annonymousDonation', '', 'INT');
		$no_giveback = $post->get('no_giveback', '', 'INT');

		// Check donor not checked no giveback option
		if (!$no_giveback)
		{
			$giveback_id = $post->get('givebacks', '', 'INT');

			$ds_amount        = $post->get('donation_amount', '', 'STRING');
			$amount_separator = $params->get('amount_separator');

			if (!empty($amount_separator))
			{
				$ds_amount = str_replace($amount_separator, '.', $ds_amount);
			}

			$iscorrect = $this->vaildateGiveback($JGIVE_order_id, $giveback_id, $ds_amount);

			if (!$iscorrect)
			{
				return false;
			}

			$obj->giveback_id = $giveback_id;
		}
		else
		{
			$obj->giveback_id = 0;
		}

		if ($obj->id)
		{
			if (!$db->updateObject('#__jg_donations', $obj, 'id'))
			{
				echo $db->stderr();

				return false;
			}
		}
		elseif (!$db->insertObject('#__jg_donations', $obj, 'id'))
		{
			echo $db->stderr();

			return false;
		}

		if ($obj->id)
		{
			$donation_id = $obj->id;
		}
		else
		{
			$donation_id = $db->insertid();
		}

		// Save order details
		$obj     = new stdClass;
		$obj->id = '';

		if ($JGIVE_order_id)
		{
			$obj->id = $JGIVE_order_id;
		}

		// Lets make a random char for this order
		// Take order prefix set by admin
		$order_prefix       = (string) $params->get('order_prefix');

		// String length should not be more than 5
		$order_prefix       = substr($order_prefix, 0, 5);

		// Take separator set by admin
		$separator          = (string) $params->get('separator');
		$obj->order_id      = $order_prefix . $separator;

		// Check if we have to add random number to order id
		$use_random_orderid = (int) $params->get('random_orderid');

		if ($use_random_orderid)
		{
			$random_numer = $this->_random(5);
			$obj->order_id .= $random_numer . $separator;

			/*This length shud be such that it matches the column lenth of primary key
			It is used to add pading
			order_id_column_field_length - prefix_length - no_of_underscores - length_of_random number*/
			$len = (23 - 5 - 2 - 5);
		}
		else
		{
			/*This length shud be such that it matches the column lenth of primary key
			It is used to add pading
			order_id_column_field_length - prefix_length - no_of_underscores*/
			$len = (23 - 5 - 2);
		}

		$obj->campaign_id    = $post->get('cid', '', 'INT');
		$obj->donor_id       = $donors_key;
		$obj->donation_id    = $donation_id;

		$obj->cdate = JFactory::getDate()->Format(JText::_('Y-m-d H:i:s'));
		$obj->mdate = JFactory::getDate()->Format(JText::_('Y-m-d H:i:s'));

		$obj->transaction_id = '';

		$donation_amount = $post->get('donation_amount', '', 'STRING');

		$amount_separator = $params->get('amount_separator');

		if (!empty($amount_separator))
		{
			$donation_amount = str_replace($amount_separator, '.', $donation_amount);
		}

		$obj->original_amount = $donation_amount;
		$obj->amount          = $donation_amount;

		$obj->fee = 0;

		if (!$send_payments_to_owner)
		{
			if (!empty($params_usergroup))
			{
				$count = count($params_usergroup);

				for ($l = 0; $l < $count; $l = $l + 3)
				{
					if (in_array($params_usergroup[$l], $camp_creator_groups_ids))
					{
						if ($camp_details->type == 'donation')
						{
							$commission_fee = (int) ($params_usergroup[$l + 1]);
						}
						elseif ($camp_details->type == 'investment')
						{
							$commission_fee = (int) ($params_usergroup[$l + 2]);
						}

						break;
					}
				}
			}

			if ($commission_fee > 0)
			{
				$obj->fee = (($obj->amount * $commission_fee) / 100) + $fixed_commission_fee;
			}
		}

		$obj->fund_holder = 0;
		$obj->vat_number  = $post->get('vat_number', '', 'STRING');

		if ($send_payments_to_owner)
		{
			// Money for this order will goto campaign promoters account
			$obj->fund_holder = 1;
		}

		// By default pending status
		$obj->status    = 'P';
		$obj->processor = $post->get('gateways', '', 'STRING');

		// Get the IP Address
		if (!empty($_SERVER['REMOTE_ADDR']))
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		else
		{
			$ip = 'unknown';
		}

		$obj->ip_address = $ip;

		if ($obj->id)
		{
			if (!$db->updateObject('#__jg_orders', $obj, 'id'))
			{
				echo $db->stderr();
			}
		}
		elseif (!$db->insertObject('#__jg_orders', $obj, 'id'))
		{
			echo $db->stderr();
		}

		// Get last insert id
		if ($JGIVE_order_id)
		{
			$orders_key = $JGIVE_order_id;
		}
		else
		{
			$orders_key = $db->insertid();
		}

		// Set order id in session
		$session = JFactory::getSession();
		$session->set('JGIVE_order_id', $orders_key);

		$db->setQuery('SELECT order_id FROM #__jg_orders WHERE id=' . $orders_key);
		$order_id      = (string) $db->loadResult();
		$maxlen        = 23 - strlen($order_id) - strlen($orders_key);
		$padding_count = (int) $params->get('padding_count');

		// Use padding length set by admin only if it is les than allowed(calculate) length
		if ($padding_count > $maxlen)
		{
			$padding_count = $maxlen;
		}

		if (strlen((string) $orders_key) <= $len)
		{
			$append = '';

			for ($z = 0; $z < $padding_count; $z++)
			{
				$append .= '0';
			}

			$append = $append . $orders_key;
		}

		$res      = new stdClass;
		$res->id  = $orders_key;

		// Imp
		$order_id = $res->order_id = $order_id . $append;

		if (!$db->updateObject('#__jg_orders', $res, 'id'))
		{
		}

		// Check if email is to be sent for new orders.
		$send_mail_new_order = $params->get('send_mail_new_order', 1);

		if ($send_mail_new_order)
		{
			require_once JPATH_SITE . "/components/com_jgive/helpers/donations.php";
			$donationsHelper = new donationsHelper;
			$donationsHelper->sendOrderEmail($orders_key, $post->get('cid', '', 'INT'));
		}

		return true;
	}

	/**
	 * Method getReceiverList.
	 *
	 * @param   String   $pg_plugin  PLUGIN Name
	 * @param   Integer  $tid        Order ID Primary Key.
	 *
	 * @return  vars.
	 *
	 * @since	1.8
	 */
	public function getPaymentVars($pg_plugin, $tid)
	{
		$campaignHelper         = new campaignHelper;
		$vars                   = new stdclass;
		$params                 = JComponentHelper::getParams('com_jgive');
		$currency_code          = $params->get('currency');
		$send_payments_to_owner = $params->get('send_payments_to_owner');

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";

		$pass_data = $this->getdetails($tid);

		$vars->order_id           = $pass_data->order_id;

		// Get the campaign promoter paypal email id
		$CampaignPromoterPaypalId = $campaignHelper->getCampaignPromoterPaypalId($tid);

		$session = JFactory::getSession();
		$session->set('order_id', $tid);

		$vars->client = 'com_jgive';

		if ($pg_plugin == 'paypal')
		{
			// Lets set the paypal email if admin is not handling transactions
			if ($send_payments_to_owner)
			{
				$vars->business = $campaignHelper->getCampaignPromoterPaypalId($tid);
			}

			// In case of donations
			if ($pass_data->is_recurring == 1)
			{
				$vars->cmd = '_xclick-subscriptions';
			}
			else
			{
				$vars->cmd = '_donations';
			}
		}

		$vars->user_firstname = $pass_data->first_name;
		$vars->user_id        = JFactory::getUser()->id;
		$vars->user_email     = $pass_data->email;

		$this->guest_donation = $params->get('guest_donation');
		$guest_email          = '';

		if ($this->guest_donation)
		{
			if (!$vars->user_id)
			{
				$vars->user_id = 0;
				$session       = JFactory::getSession();
				$session->set('quick_reg_no_login', '1');
				$guest_email = '';
				$guest_email = md5($vars->user_email);
				$session     = JFactory::getSession();
				$session->set('guest_email', $guest_email);
			}
		}

		$vars->item_name = $campaignHelper->getCampaignTitle($tid);

		// Added for payment description.
		$donationsHelper           = new donationsHelper;
		$cid                       = $donationsHelper->getCidFromOrderId($tid);
		$link                      = '<a href="' . JUri::root() . substr(
		JRoute::_('index.php?option=com_jgive&view=campaign&layout=single&cid=' . $cid), strlen(JUri::base(true)) + 1
		) . '">' . $vars->item_name . '</a>';

		$vars->payment_description = JText::sprintf('COM_JGIVE_PAYMENT_DESCRIPTION', $link);

		$vars->submiturl         = JRoute::_(
		"index.php?option=com_jgive&task=donations.confirmpayment&processor={$pg_plugin}&order_id=" . $vars->order_id
		);

		$vars->return            = JRoute::_(
		JUri::root() . "index.php?option=com_jgive&view=donations&layout=details&donationid=" .
		$pass_data->id . "&processor=" . $pg_plugin . "&email=" . $guest_email
		);

		$vars->cancel_return     = JRoute::_(
		JUri::root() . "index.php?option=com_jgive&view=donations&layout=details&donationid=" .
		$pass_data->id . "&processor=" . $pg_plugin . "&email=" . $guest_email
		);

		$vars->url               = $vars->notify_url = JRoute::_(
		JUri::root() . "index.php?option=com_jgive&task=donations.processPayment&processor={$pg_plugin}&order_id=" . $vars->order_id
		);



		$vars->campaign_promoter = $CampaignPromoterPaypalId;
		$vars->currency_code     = $currency_code;

		$amount_separator = $params->get('amount_separator');

		if (!empty($amount_separator))
		{
			$donation_amount = $session->get('JGIVE_donation_amount');
			$donation_amount = str_replace($amount_separator, '.', $donation_amount);
			$vars->amount    = $donation_amount;
		}
		else
		{
			$vars->amount = $session->get('JGIVE_donation_amount');
		}

		$vars->is_recurring        = $pass_data->is_recurring;
		$vars->recurring_frequency = $pass_data->recurring_frequency;
		$vars->recurring_count     = $pass_data->recurring_count;

		// For language specific paypal
		$user_data          = JFactory::getUser();
		$vars->country_code = '';
		$user_language      = $user_data->getParam('language');

		if (!empty($user_language)) // User language available in db
		{
			$user_language      = str_replace('-', '_', $user_language);
			$vars->country_code = $user_language;
		}
		else // Pass location if language not available for user in db
		{
			$vars->country_code = $pass_data->country_code;
		}

		$vars->adaptiveReceiverList = $this->getReceiverList($vars, $pg_plugin);

		return $vars;
	}


    public function getPaymentVarsSocial($pg_plugin, $tid)
    {
        $campaignHelper         = new campaignHelper;
        $vars                   = new stdclass;
        $params                 = JComponentHelper::getParams('com_jgive');
        $currency_code          = $params->get('currency');
        $send_payments_to_owner = $params->get('send_payments_to_owner');

        require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";

        $pass_data = $this->getdetails($tid);

        $vars->order_id           = $pass_data->order_id;

        // Get the campaign promoter paypal email id
        $CampaignPromoterPaypalId = $campaignHelper->getCampaignPromoterPaypalId($tid);

        $session = JFactory::getSession();
        $session->set('order_id', $tid);

        $vars->client = 'com_jgive';

        if ($pg_plugin == 'paypal')
        {
            // Lets set the paypal email if admin is not handling transactions
            if ($send_payments_to_owner)
            {
                $vars->business = $campaignHelper->getCampaignPromoterPaypalId($tid);
            }

            // In case of donations
            if ($pass_data->is_recurring == 1)
            {
                $vars->cmd = '_xclick-subscriptions';
            }
            else
            {
                $vars->cmd = '_donations';
            }
        }

        $vars->user_firstname = $pass_data->first_name;
        $vars->user_id        = JFactory::getUser()->id;
        $vars->user_email     = $pass_data->email;

        $this->guest_donation = $params->get('guest_donation');
        $guest_email          = '';

        if ($this->guest_donation)
        {
            if (!$vars->user_id)
            {
                $vars->user_id = 0;
                $session       = JFactory::getSession();
                $session->set('quick_reg_no_login', '1');
                $guest_email = '';
                $guest_email = md5($vars->user_email);
                $session     = JFactory::getSession();
                $session->set('guest_email', $guest_email);
            }
        }

        $vars->item_name = $campaignHelper->getCampaignTitle($tid);

        // Added for payment description.
        $donationsHelper           = new donationsHelper;
        $cid                       = $donationsHelper->getCidFromOrderId($tid);
        $link                      = '<a href="' . JUri::root() . substr(
                JRoute::_('index.php/community'), strlen(JUri::base(true)) + 1
            ) . '">' . $vars->item_name . '</a>';

        $vars->payment_description = JText::sprintf('COM_JGIVE_PAYMENT_DESCRIPTION', $link);

        $vars->submiturl         = JRoute::_(
            "index.php/community"
        );

        $vars->return            = JRoute::_(
            JUri::root() . "index.php/community"
        );

        $vars->cancel_return     = JRoute::_(
            JUri::root() . "index.php/community"
        );

        $vars->url               = $vars->notify_url = JRoute::_(
            JUri::root() . "index.php/community"
        );

        $vars->campaign_promoter = $CampaignPromoterPaypalId;
        $vars->currency_code     = $currency_code;

        $amount_separator = $params->get('amount_separator');

        if (!empty($amount_separator))
        {
            $donation_amount = $session->get('JGIVE_donation_amount');
            $donation_amount = str_replace($amount_separator, '.', $donation_amount);
            $vars->amount    = $donation_amount;
        }
        else
        {
            $vars->amount = $session->get('JGIVE_donation_amount');
        }

        $vars->is_recurring        = $pass_data->is_recurring;
        $vars->recurring_frequency = $pass_data->recurring_frequency;
        $vars->recurring_count     = $pass_data->recurring_count;

        // For language specific paypal
        $user_data          = JFactory::getUser();
        $vars->country_code = '';
        $user_language      = $user_data->getParam('language');

        if (!empty($user_language)) // User language available in db
        {
            $user_language      = str_replace('-', '_', $user_language);
            $vars->country_code = $user_language;
        }
        else // Pass location if language not available for user in db
        {
            $vars->country_code = $pass_data->country_code;
        }

        $vars->adaptiveReceiverList = $this->getReceiverList($vars, $pg_plugin);

        return $vars;
    }

	/**
	 * Method getReceiverList.
	 *
	 * @param   String  $vars       Vars.
	 * @param   String  $pg_plugin  PG PLUGIN.
	 *
	 * @return  receiverList Array.
	 *
	 * @since	1.8
	 */
	public function getReceiverList($vars, $pg_plugin)
	{
		// GET BUSINESS EMAIL
		$plugin           = JPluginHelper::getPlugin('payment', $pg_plugin);
		$pluginParams     = json_decode($plugin->params);
		$businessPayEmial = "";

		if (property_exists($pluginParams, 'business'))
		{
			$businessPayEmial = trim($pluginParams->business);
		}
		else
		{
			return array();
		}

		$params = JComponentHelper::getParams('com_jgive');

		$paymentsToOwnerWithoutApplyCommission = $params->get('send_payments_to_owner', 0);

		if ($pg_plugin == 'adaptive_paypal')
		{
			// Send payment to campaign promoter without charging any commision

			if ($paymentsToOwnerWithoutApplyCommission)
			{
				$AmountToPayToPromoter = $vars->amount;
			}
			else
			{
				$donationsHelper       = new donationsHelper;
				$fee                   = $donationsHelper->getFee($vars->order_id);
				$AmountToPayToPromoter = $vars->amount - $fee;
			}

			// Get site admin paypal bussiness account email address
			$plugin                         = JPluginHelper::getPlugin('payment', $pg_plugin);
			$pluginParams                   = json_decode($plugin->params);
			$siteAdminBussineessAcountEmail = "";

			if (property_exists($pluginParams, 'business'))
			{
				$siteAdminBussineessAcountEmail = trim($pluginParams->business);
			}

			$receiverList                = array();
			$receiverList[0]             = array();
			$receiverList[1]             = array();

			// Admin has his own products
			$receiverList[0]['receiver'] = $siteAdminBussineessAcountEmail;

			$receiverList[0]['amount']   = $fee;
			$receiverList[0]['primary']  = false;

			// Add other receivers
			$receiverList[1]['receiver'] = $vars->campaign_promoter;
			$receiverList[1]['amount']   = $vars->amount;
			$receiverList[1]['primary']  = true;

			return $receiverList;
		}

		return;
	}

	/**
	 * Method getdetails.
	 *
	 * @param   String   $pg_plugin  PG PLUGIN.
	 * @param   Integer  $tid        Id.
	 *
	 * @return  html.
	 *
	 * @since	1.8
	 */
	public function getHTML($pg_plugin, $tid)
	{
		$vars       = $this->getPaymentVars($pg_plugin, $tid);
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('payment', $pg_plugin);
		$html = $dispatcher->trigger('onTP_GetHTML', array($vars));

		return $html;
	}

    public function getHTMLSocial($pg_plugin, $tid)
    {
        $vars       = $this->getPaymentVarsSocial($pg_plugin, $tid);
        $dispatcher = JDispatcher::getInstance();
        JPluginHelper::importPlugin('payment', $pg_plugin);
        $html = $dispatcher->trigger('onTP_GetHTML', array($vars));

        return $html;
    }

	/**
	 * Method getdetails.
	 *
	 * @param   Integer  $tid  Id.
	 *
	 * @return  orderdetails  Array.
	 *
	 * @since	1.8
	 */
	public function getdetails($tid)
	{
		$user_id = JFactory::getUser()->id;

		if (!$user_id)
		{
			$query = "SELECT o.id, o.order_id, d.first_name, d.email, d.phone,
			 ds.is_recurring, ds.recurring_frequency, ds.recurring_count,
			 country.country_code
			 FROM #__jg_orders AS o
			 LEFT JOIN #__jg_donors AS d ON d.id=o.donor_id
			 LEFT JOIN #__jg_donations as ds ON ds.id=o.donation_id
			 LEFT JOIN #__tj_country as country ON country.country=d.country
			 WHERE o.id='" . $tid . "'";

			$this->_db->setQuery($query);
			$orderdetails = $this->_db->loadObjectlist();

			return $orderdetails['0'];
		}

		$query = "SELECT o.id, o.order_id, d.first_name, d.email, d.phone,
		 ds.is_recurring, ds.recurring_frequency, ds.recurring_count,
		 country.country_code
		 FROM #__jg_orders AS o
		 LEFT JOIN #__jg_donors AS d ON d.id=o.donor_id
		 LEFT JOIN #__jg_donations as ds ON ds.id=o.donation_id
		 LEFT JOIN #__tj_country as country ON country.country=d.country
		 WHERE o.id='" . $tid . "' AND d.user_id=" . $user_id;

		$this->_db->setQuery($query);
		$orderdetails = $this->_db->loadObjectlist();

		return $orderdetails['0'];
	}

	/**
	 * Method getCampaign.
	 *
	 * @return  Array.
	 *
	 * @since	1.8
	 */
	public function getCampaign()
	{
		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;
		$cid            = $this->getCampaignId();

		$query = "SELECT c.*
		FROM `#__jg_campaigns` AS c
		WHERE c.id=" . $cid;
		$this->_db->setQuery($query);
		$campaign          = $this->_db->loadObject();
		$cdata['campaign'] = $campaign;

		// Get campaign amounts
		$amounts                             = $campaignHelper->getCampaignAmounts($cid);
		$cdata['campaign']->amount_received  = $amounts['amount_received'];
		$cdata['campaign']->remaining_amount = $amounts['remaining_amount'];

		// Get campaign images
		$cdata['images']    = $campaignHelper->getCampaignImages($cid);

		// Get campaign givebacks
		$cdata['givebacks'] = $campaignHelper->getCampaignGivebacks($cid);

		return $cdata;
	}

	/**
	 * Method processPayment.
	 *
	 * @param   String   $post       Post.
	 * @param   Array    $pg_plugin  Data.
	 * @param   Integer  $order_id   Order Id.
	 *
	 * @return  data Array.
	 *
	 * @since	1.8
	 */
	public function processPayment($post, $pg_plugin, $order_id)
	{
		require_once JPATH_SITE . '/components/com_jgive/helpers/donations.php';
		$donationsHelper = new donationsHelper;

		// Get id for orders table using order_id
		$order_id_key = $donationsHelper->getOrderIdKeyFromOrderId($order_id);

		$comment_present = array_key_exists('comment', $post);

		if ($comment_present)
		{
			$this->saveComment($pg_plugin, $order_id_key, $post['comment']);
		}

		$jgiveFrontendHelper = new jgiveFrontendHelper;
		$session             = JFactory::getSession();
		$guest_email         = '';
		$guest_email         = $session->get('guest_email');
		$session->clear('guest_email');
		$session->set('order_link_guestemail', $guest_email);

		// Load donations helper

		JRequest::setVar('remote', 1);
		$donationItemid = $jgiveFrontendHelper->getItemId('index.php?option=com_jgive&view=donations');

		$return_resp = array();

		// Authorise Post Data
		if ($post['plugin_payment_method'] == 'onsite')
		{
			$plugin_payment_method = $post['plugin_payment_method'];
		}

		$vars = $this->getPaymentVars($pg_plugin, $order_id_key);

		// Trigger payment plugins- onTP_Processpayment
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('payment', $pg_plugin);

		$data = $dispatcher->trigger('onTP_Processpayment', array($post, $vars));

		$data = $data[0];

		// Store log
		$res = @$this->storelog($pg_plugin, $data);

		// Get order id
		if (empty($order_id))
		{
			$order_id = $data['order_id'];
		}

		// Get id for orders table using order_id
		$order_id_key                = $donationsHelper->getOrderIdKeyFromOrderId($order_id);
		$OrderStatusAndTransactoinId = $donationsHelper->getOrderTransactoionIdAndStatus($order_id_key);

		// Check is repetative same response from paypal & if yes then don't send email notification
		$duplicate_response = 0;

		if ($OrderStatusAndTransactoinId)
		{
			if (($OrderStatusAndTransactoinId->status == $data['status']) AND ($OrderStatusAndTransactoinId->transaction_id == $data['transaction_id']))
			{
				$duplicate_response = 1;
			}
		}

		// Gateway used
		$data['processor'] = $pg_plugin;

		// Payment status
		$data['status']    = trim($data['status']);

		// Get order amount
		$query = "SELECT o.original_amount
				FROM #__jg_orders as o
				where o.id=" . $order_id_key;
		$this->_db->setQuery($query);
		$order_amount = $this->_db->loadResult();

		// Return url
		$return_resp['return'] = $data['return'];

		// If payment status is confirmed
		if ($data['status'] == 'C' && $order_amount == $data['total_paid_amt'])
		{
			$this->updateOrder($data);

			// Update order status, send email,
			$donationsHelper->updatestatus($order_id_key, $data['status'], $comment = '', $notify_chk = 1, $duplicate_response);

			if ($data['status'] == 'C')
			{
				$donationsHelper->getSoldGivebacks($order_id_key);
			}

			// Trigger plugins
			// OnAfterJGivePaymentSuccess
			$dispatcher = JDispatcher::getInstance();
			JPluginHelper::importPlugin('system');
			$result = $dispatcher->trigger('OnAfterJGivePaymentSuccess', array($order_id_key));

			if ($result === false)
			{
			}
			// END plugin triggers

			// Added guest email in url for payment processs on site
			$return_resp['return'] = JUri::root() . substr(
			JRoute::_("index.php?option=com_jgive&view=donations&layout=details&donationid=" .
			$order_id_key . "&processor={$pg_plugin}&email=" . $guest_email . "&Itemid=" .
			$donationItemid, false
			), strlen(JUri::base(true)) + 1
			);

			$return_resp['status'] = '1';
		}
		elseif ($order_amount != $data['total_paid_amt'])
		{
			$data['status']        = 'E';
			$return_resp['status'] = '0';
		}
		elseif (!empty($data['status']))
		{
			// Added guest email in url for payment processs on site
			if ($plugin_payment_method && $data['status'] == 'P')
			{
				$return_resp['return'] = JUri::root() . substr(
				JRoute::_("index.php?option=com_jgive&view=donations&layout=details&donationid=" .
				$order_id_key . "&processor={$pg_plugin}&email=" . $guest_email .
				"&Itemid=" . $donationItemid, false
				), strlen(JUri::base(true)) + 1
				);
			}

			if ($data['status'] != 'C')
			{
				$this->updateOrder($data);
			}
			elseif ($data['status'] == 'C')
			{
				// Added guest email in url for payment processs on site
				$return_resp['return'] = JUri::root() . substr(
				JRoute::_(
				"index.php?option=com_jgive&view=donations&layout=cancel&processor={$pg_plugin}&email=" . $guest_email . "&Itemid=" . $chkoutItemid
				), strlen(JUri::base(true)) + 1
				);
			}

			$return_resp['status'] = '0';

			// TODO where is this  used ???
			$res->processor        = $data['processor'];
			$return_resp['msg']    = $data['error']['code'] . " " . $data['error']['desc'];
		}

		$res = trim($return_resp['msg']);

		if (!$res AND $pg_plugin == 'bycheck')
		{
			$return_resp['msg'] = JText::_('COM_JGIVE_ORDER_PLACED_NOTIFICATION');
		}

		// Update campaign success status.
		$campaignHelper = new campaignHelper;
		$campaignHelper->updateCampaignSuccessStatus($cid = 0, $campaignSuccessStatus = null, $order_id_key);

		// Update campaign processed flag.
		$campaignHelper->updateCampaignProcessedFlag($cid = 0, $campaignProcessedFlag = null, $order_id_key);

		$cid = $donationsHelper->getCidFromOrderId($order_id_key);

		if ($cid)
		{
			$this->_MailExceedingGoalAmount($cid);
		}

		return $return_resp;
	}

	/**
	 * Method storelog.
	 *
	 * @param   String  $name  Name.
	 * @param   Array   $data  Data.
	 *
	 * @return  data Array.
	 *
	 * @since	1.8
	 */
	public function storelog($name, $data)
	{
		$data1              = array();
		$data1['raw_data']  = $data['raw_data'];
		$data1['JT_CLIENT'] = "com_jgive";
		$dispatcher         = JDispatcher::getInstance();
		JPluginHelper::importPlugin('payment', $name);
		$data = $dispatcher->trigger('onTP_Storelog', array($data1));
	}

	/**
	 * Method updateOrder.
	 *
	 * @param   Array  $data  Data.
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function updateOrder($data)
	{
		// Load donations helper
		require_once JPATH_SITE . '/components/com_jgive/helpers/donations.php';
		$donationsHelper = new donationsHelper;

		// Get id for orders table using order_id
		$order_id_key = $donationsHelper->getOrderIdKeyFromOrderId($data['order_id']);

		// Get donation id
		$db    = JFactory::getDBO();
		$query = "SELECT donation_id
		FROM #__jg_orders where id=" . $order_id_key;
		$db->setQuery($query);
		$donation_id = $db->loadResult();

		$db    = JFactory::getDBO();
		$query = "SELECT subscr_id,is_recurring
		FROM #__jg_donations where id=" . $donation_id;
		$db->setQuery($query);
		$donation_details = $db->loadObject();

		// If subscriber id is not exist then it is first response from paypal
		// Hence update donation table insert subsriber id & also update transaction id

		if ($donation_details->is_recurring)
		{
			// For recurring payment
			if ($data['txn_type'] == 'subscr_payment')
			{
				// Insert subscriber id in donation table if not exits (First response of Recurring donations)
				if (empty($donation_details->subscr_id))
				{
					$db             = JFactory::getDBO();
					$res            = new stdClass;
					$res->id        = $donation_id;
					$res->subscr_id = $data['subscriber_id'];

					if (!$db->updateObject('#__jg_donations', $res, 'id'))
					{
					}

					// Update First order status & transaction id

					$db                  = JFactory::getDBO();
					$res                 = new stdClass;
					$res->id             = $order_id_key;
					$res->mdate          = date("Y-m-d H:i:s");
					$res->transaction_id = $data['transaction_id'];
					$res->status         = $data['status'];
					$res->processor      = $data['processor'];
					$res->extra          = json_encode($data['raw_data']);

					if (!$db->updateObject('#__jg_orders', $res, 'id'))
					{
					}
				}
				else // For recurring payment for more responses other than first
				{
					// Check the transaction id is present in the order table
					// Get transaction id

					$db    = JFactory::getDBO();
					$query = "SELECT transaction_id
					FROM #__jg_orders where donation_id=" . $donation_id;
					$db->setQuery($query);
					$transaction_ids = $db->loadColumn();

					// Check is transaction id exist in array
					if ($transaction_ids[0])
					{
						$flag = 0;

						for ($i = 0; $i < count($transaction_ids); $i++)
						{
							// If same transaction id then update the order
							if ($transaction_ids[$i] == $data['transaction_id'])
							{
								// Transaction id already in table
								$flag = 1;
								break;
							}
						}

						// New transaction
						if ($flag == 0)
						{
							// Order_id campaign_id  donor_id donation_id fund_holder cdate mdate
							// Transaction_id transaction_id original_amount amount  fee  status processor ip_address extra
							$this->_addNewRecurringOrder($data, $order_id_key, $donation_id);
						}
						else
						{
							$db                  = JFactory::getDBO();
							$res                 = new stdClass;
							$res->mdate          = date("Y-m-d H:i:s");
							$res->transaction_id = $data['transaction_id'];
							$res->status         = $data['status'];
							$res->processor      = $data['processor'];
							$res->extra          = json_encode($data['raw_data']);

							if (!$db->updateObject('#__jg_orders', $res, 'transaction_id'))
							{
							}
						}
					}
				}
			}

			// Check that user subscription is expired
			if ($data['processor'] == 'stripe')
			{
				$this->_CheckToCancelSubscriptions($donation_id, $data, $order_id_key);
			}
		}
		else
		{
			$db                  = JFactory::getDBO();
			$res                 = new stdClass;
			$res->id             = $order_id_key;
			$res->mdate          = date("Y-m-d H:i:s");
			$res->transaction_id = $data['transaction_id'];
			$res->status         = $data['status'];
			$res->processor      = $data['processor'];
			$res->extra          = json_encode($data['raw_data']);

			if (!$db->updateObject('#__jg_orders', $res, 'id'))
			{
			}

			// For adaptive payment add entry in payout report
			if ($data['txn_type'] == 'Adaptive Payment PAY')
			{
				$this->addPayout($data['raw_data']['paymentInfoList']['paymentInfo'][1], $data['order_id']);
			}
		}
	}

	/**
	 * Method addPayout.
	 *
	 * @param   Array    $data      Data.
	 * @param   Integer  $order_id  Order Id.
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function addPayout($data, $order_id)
	{
		$camp_promoter_email = $data['receiver']['email'];

		if (!$camp_promoter_email)
		{
			return;
		}

		$db    = JFactory::getDBO();
		$query = "SELECT creator_id FROM #__jg_campaigns WHERE paypal_email='" . $camp_promoter_email . "'";
		$db->setQuery($query);
		$camp_promoter_id = $db->loadResult();

		$db    = JFactory::getDBO();
		$query = "SELECT id FROM #__jg_payouts WHERE transaction_id='" . $data['transactionId'] . "'";
		$db->setQuery($query);
		$payout_id = $db->loadResult();

		$res = new stdClass;

		$db = JFactory::getDBO();

		if ($payout_id)
		{
			$res->id = $payout_id;
		}
		else
		{
			$res->id = '';
		}

		$res->user_id        = $camp_promoter_id;
		$res->payee_name     = JFactory::getUser($camp_promoter_id)->name;
		$res->date           = date("Y-m-d H:i:s");
		$res->transaction_id = $data['transactionId'];
		$res->email_id       = $camp_promoter_email;

		require_once JPATH_SITE . '/components/com_jgive/helpers/donations.php';
		$donationsHelper = new donationsHelper;
		$fee             = $donationsHelper->getFee($order_id);
		$amountPaid      = ($data['receiver']['amount']) - $fee;

		$res->amount = $amountPaid;

		if ($data['transactionStatus'] == 'COMPLETED')
		{
			$res->status = 1;
		}
		else
		{
			$res->status = 0;
		}

		$res->ip_address = '';
		$res->type       = 'adaptive_paypal';

		if ($res->id)
		{
			if (!$db->updateObject('#__jg_payouts', $res, 'id'))
			{
			}
		}
		else
		{
			if (!$db->insertObject('#__jg_payouts', $res, 'id'))
			{
			}
		}

		return true;
	}

	/**
	 * Method _addNewRecurringOrder.
	 *
	 * @param   Array    $data          Data  .
	 * @param   Integer  $order_id_key  Order Id Key  .
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function _addNewRecurringOrder($data, $order_id_key)
	{
		$donationsHelper = new donationsHelper;
		$donationInfo    = $donationsHelper->getSingleDonationInfo($order_id_key);

		/*Order_id campaign_id  donor_id donation_id fund_holder cdate mdate
		Transaction_id transaction_id original_amount amount  fee  status processor ip_address extra
		Save order details*/
		$db              = JFactory::getDBO();
		$obj             = new stdClass;
		$obj->id         = '';

		// Lets make a random char for this order
		// Take order prefix set by admin
		$obj->id            = '';
		$params             = JComponentHelper::getParams('com_jgive');
		$order_prefix       = (string) $params->get('order_prefix');

		// String length should not be more than 5
		$order_prefix       = substr($order_prefix, 0, 5);

		// Take separator set by admin
		$separator          = (string) $params->get('separator');
		$obj->order_id      = $order_prefix . $separator;

		// Check if we have to add random number to order id
		$use_random_orderid = (int) $params->get('random_orderid');

		if ($use_random_orderid)
		{
			$random_numer = $this->_random(5);
			$obj->order_id .= $random_numer . $separator;

			/*This length shud be such that it matches the column lenth of primary key
			It is used to add pading
			Order_id_column_field_length - prefix_length - no_of_underscores - length_of_random number*/
			$len = (23 - 5 - 2 - 5);
		}
		else
		{
			/*This length shud be such that it matches the column lenth of primary key
			 It is used to add pading
			 Order_id_column_field_length - prefix_length - no_of_underscores*/
			$len = (23 - 5 - 2);
		}

		$obj->campaign_id     = $donationInfo['campaign']->id;
		$obj->donor_id        = $donationInfo['donor']->id;
		$obj->donation_id     = $donationInfo['payment']->donation_id;
		$obj->cdate           = date("Y-m-d H:i:s");
		$obj->mdate           = date("Y-m-d H:i:s");
		$obj->original_amount = $donationInfo['payment']->original_amount;
		$obj->amount          = $donationInfo['payment']->amount;

		// Need To Modify
		$obj->fee = $donationInfo['payment']->fee;

		$obj->fund_holder = $donationInfo['payment']->fund_holder;

		$obj->processor = $donationInfo['payment']->processor;

		// Get the IP Address
		$obj->ip_address = $donationInfo['payment']->ip_address;

		$obj->transaction_id = $data['transaction_id'];
		$obj->status         = $data['status'];
		$obj->extra          = json_encode($data['raw_data']);

		if (!$db->insertObject('#__jg_orders', $obj, 'id'))
		{
			echo $db->stderr();
		}

		$orders_key = $db->insertid();

		if (!$orders_key)
		{
			return 'Error in saving order details';
		}

		$db->setQuery('SELECT order_id FROM #__jg_orders WHERE id=' . $orders_key);
		$order_id      = (string) $db->loadResult();
		$maxlen        = 23 - strlen($order_id) - strlen($orders_key);
		$padding_count = (int) $params->get('padding_count');

		// Use padding length set by admin only if it is les than allowed(calculate) length
		if ($padding_count > $maxlen)
		{
			$padding_count = $maxlen;
		}

		if (strlen((string) $orders_key) <= $len)
		{
			$append = '';

			for ($z = 0; $z < $padding_count; $z++)
			{
				$append .= '0';
			}

			$append = $append . $orders_key;
		}

		$res           = new stdClass;
		$res->id       = $orders_key;

		// Imp
		$res->order_id = $order_id . $append;

		if (!$db->updateObject('#__jg_orders', $res, 'id'))
		{
		}
	}

	/**
	 * Method changeOrderStatus.
	 *
	 * @return  value.
	 *
	 * @since	1.8
	 */
	public function changeOrderStatus()
	{
		$returnvaule = 1;
		$data        = JRequest::get('post');

		// For email
		if (isset($data['notify_chk']))
		{
			$notify_chk = 1;
		}
		else
		{
			$notify_chk = 0;
		}

		if (isset($data['comment']) && $data['comment'])
		{
			$comment = $data['comment'];
		}
		else
		{
			$comment = '';
		}

		$path = JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		if (!class_exists('donationsHelper'))
		{
			JLoader::register('donationsHelper', $path);
			JLoader::load('donationsHelper');
		}

		$donationsHelper = new donationsHelper;
		$donationsHelper->updatestatus($data['id'], $data['status'], $comment, $notify_chk, 0);

		// Assign order status
		$status = $data['status'];

		// Send mail when goal amount for campaign has been reached
		$db = JFactory::getDBO();

		$query = "SELECT campaign_id FROM `#__jg_orders` WHERE id = " . $data['id'];

		$db->setQuery($query);

		$campaign_id = $db->loadResult();

		$this->_MailExceedingGoalAmount($campaign_id);

		if ($data['status'] == 'C')
		{
			$donationsHelper->getSoldGivebacks($data['id']);
		}

		if ($status == 'RF')
		{
			$returnvaule = 3;
		}

		// Update campaign success status.
		$campaignHelper = new campaignHelper;
		$campaignHelper->updateCampaignSuccessStatus($cid = 0, $campaignSuccessStatus = null, $data['id']);

		return $returnvaule;
	}

	/**
	 * Method deleteDonations.
	 *
	 * @param   Integer  $odid  order id  .
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function deleteDonations($odid)
	{
		// Check which orders is recurring
		for ($i = 0; $i < (count($odid)); $i++)
		{
			$q = "SELECT donation_id FROM  #__jg_orders WHERE id=" . $odid[$i];
			$this->_db->setQuery($q);
			$donation_id = $this->_db->loadResult();

			if ($donation_id)
			{
				$q = "SELECT is_recurring FROM  #__jg_donations WHERE id=" . $donation_id;
				$this->_db->setQuery($q);
				$is_recurring = $this->_db->loadResult();

				if ($is_recurring) // For recurring donations delete only order entry
				{
					// Delete from  orders
					$query = "DELETE FROM #__jg_orders where id =" . $odid[$i];
					$this->_db->setQuery($query);

					if (!$this->_db->execute())
					{
						$this->setError($this->_db->getErrorMsg());

						return false;
					}
				}
				else // Delete donor, Order, donations
				{
					// Delete from donors
					$q = "SELECT donor_id FROM  #__jg_donations WHERE id=" . $donation_id;
					$this->_db->setQuery($q);
					$donor_id = $this->_db->loadResult();

					$query = "DELETE FROM #__jg_donors where id =" . $donor_id;
					$this->_db->setQuery($query);

					if (!$this->_db->execute())
					{
						$this->setError($this->_db->getErrorMsg());

						return false;
					}

					// Delete from  orders
					$query = "DELETE FROM #__jg_orders where id =" . $odid[$i];
					$this->_db->setQuery($query);

					if (!$this->_db->execute())
					{
						$this->setError($this->_db->getErrorMsg());

						return false;
					}

					// Delete from donations
					$query = "DELETE FROM #__jg_donations where id=" . $donation_id;
					$this->_db->setQuery($query);

					if (!$this->_db->execute())
					{
						$this->setError($this->_db->getErrorMsg());

						return false;
					}
				}
			}
			else // Support the version of jGive before 1.6 (Recurring payment & One page checkout) release
			{
				// Delete from donors
				$q = "SELECT donor_id FROM  #__jg_orders WHERE id=" . $odid[$i];
				$this->_db->setQuery($q);
				$donor_id = $this->_db->loadResult();

				$query = "DELETE FROM #__jg_donors where id =" . $donor_id;
				$this->_db->setQuery($query);

				if (!$this->_db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}

				// Delete from  orders
				$query = "DELETE FROM #__jg_orders where id =" . $odid[$i];
				$this->_db->setQuery($query);

				if (!$this->_db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}

				// Delete from donations
				$query = "DELETE FROM #__jg_donations where order_id=" . $odid[$i];
				$this->_db->setQuery($query);

				if (!$this->_db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method _random.
	 *
	 * @param   Integer  $length  Length  .
	 *
	 * @return  string.
	 *
	 * @since	1.8
	 */
	public function _random($length = 17)
	{
		$salt   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$len    = strlen($salt);
		$random = '';

		$stat = @stat(__FILE__);

		if (empty($stat) || !is_array($stat))
		{
			$stat = array(php_uname());
		}

		mt_srand(crc32(microtime() . implode('|', $stat)));

		for ($i = 0; $i < $length; $i++)
		{
			$random .= $salt[mt_rand(0, $len - 1)];
		}

		return $random;
	}

	/**
	 * Method getMinimumAmount.
	 *
	 * @param   Integer  $cid  campaign id  .
	 *
	 * @return  row result.
	 *
	 * @since	1.8
	 */
	public function getMinimumAmount($cid)
	{
		$query = "SELECT c.minimum_amount
			FROM #__jg_campaigns AS c
			WHERE c.id=" . $cid;
		$this->_db->setQuery($query);

		return $this->_db->loadResult();
	}

	/**
	 * Method checkMailExists.
	 *
	 * @param   string  $mail  The mail.
	 *
	 * @return  boolean.
	 *
	 * @since	1.8
	 */
	public function checkMailExists($mail)
	{
		$mailexist = 0;
		$query     = "SELECT id FROM #__users where email  LIKE '" . $mail . "'";
		$this->_db->setQuery($query);
		$result = $this->_db->loadResult();

		if ($result)
		{
			$mailexist = 1;
		}
		else
		{
			$mailexist = 0;
		}

		return $mailexist;
	}

	/**
	 * Method _MailExceedingGoalAmount.
	 *
	 * @param   integer  $camp_id  The id of the campaign.
	 *
	 * @return  void.
	 *
	 * @since	1.8
	 */
	public function _MailExceedingGoalAmount($camp_id)
	{
		$app = JFactory::getApplication();

		require_once JPATH_SITE . '/components/com_jgive/helpers/donations.php';

		$jg_params = JComponentHelper::getParams('com_jgive');
		$send_mail = $jg_params->get('goal_amount_reach');

		if ($send_mail == 1)
		{
			$query = " SELECT SUM( amount)
			FROM `#__jg_orders`
			WHERE status = 'C'
			AND campaign_id = " . $camp_id;
			$this->_db->setQuery($query);
			$total = $this->_db->loadResult();

			$query = " SELECT `goal_amount` FROM `#__jg_campaigns` WHERE `id` = " . $camp_id;
			$this->_db->setQuery($query);
			$goal_amount = $this->_db->loadResult();

			if ($total >= $goal_amount)
			{
				$query = "SELECT `title` FROM #__jg_campaigns WHERE id = " . $camp_id;
				$this->_db->setQuery($query);

				$campaign_title = $this->_db->loadResult();

				// Get campaign creator email
				$query = "SELECT DISTINCT (u.`email`)
				FROM`#__users` AS u
				LEFT JOIN #__jg_campaigns AS c ON u.id = c.creator_id
				WHERE c.id = " . $camp_id;
				$this->_db->setQuery($query);

				$creator_email = $this->_db->loadResult();

				// Send mail
				$mailfrom = $app->getCfg('mailfrom');
				$fromname = $app->getCfg('fromname');
				$sitename = $app->getCfg('sitename');

				$bcc_string = $jg_params->get('email');
				$subject    = JText::_('GOAL_AMOUNT_EXCEED_SUBJECT');
				$subject    = str_replace('{fundraiser_name}', $campaign_title, $subject);

				$body = JText::_('EXCEED_GOAL_AMOUNT');
				$body = str_replace('{fundraiser_name}', $campaign_title, $body);
				$body = str_replace('{sitename}', $sitename, $body);

				$donationsHelper = new donationsHelper;
				$res             = $donationsHelper->sendmail($creator_email, $subject, $body, $bcc_string, 'campaign.goalreached');
			}
		}
	}

	/**
	 * Method _CheckToCancelSubscriptions.
	 *
	 * @param   integer  $donation_id   The donation id .
	 * @param   array    $data          The data.
	 * @param   integer  $order_id_key  The Order Id Key.
	 *
	 * @return  Array result.
	 *
	 * @since	1.8
	 */
	public function _CheckToCancelSubscriptions($donation_id, $data, $order_id_key)
	{
		$db = JFactory::getDBO();

		$query = "SELECT COUNT(id) as orders_count
			FROM #__jg_orders
			WHERE donation_id =" . $donation_id . "
			AND status = 'C'
			GROUP BY donation_id";

		$db->setQuery($query);
		$orders_count = $db->loadResult();

		$query = "SELECT recurring_count
			FROM #__jg_donations as d
			WHERE d.id =" . $donation_id;

		$db->setQuery($query);
		$recurring_count = $db->loadResult();

		if (($orders_count == $recurring_count) || ($orders_count > $recurring_count))
		{
			$query = "SELECT `extra`
				FROM `#__jg_orders`
				WHERE `id`=" . $order_id_key . "
				AND `status`='C'
				";

			$db->setQuery($query);
			$extra = $db->loadResult();

			JPluginHelper::importPlugin('payment', $pg_plugin);
			$dispatcher = JDispatcher::getInstance();

			$result = $dispatcher->trigger('onTP_cancelSubscription', array($extra));
		}
	}

	/**
	 * Method donationsCsvexport.
	 *
	 * @param   integer  $campaign_cat  default set .
	 *
	 * @return  Array result.
	 *
	 * @since	1.8
	 */
	public function donationsCsvexport($campaign_cat = 1)
	{
		// Get the WHERE and ORDER BY clauses for the query
		$where = '';
		$where = $this->_buildContentWhere();

		$query = "SELECT i.id, i.order_id, i.fund_holder, i.status, i.processor, i.amount, i.fee, i.cdate, i.transaction_id,
		d.user_id AS donor_id, d.first_name, d.first_name, d.last_name, d.email, d.address, d.address2, d.city as othercity, d.zip,
		c.id AS cid, c.title,dona.comment, g.description as giveback_desc,
		dona.giveback_id,
		city.city, region.region as state, country.country
		FROM #__jg_orders AS i
		LEFT JOIN #__jg_campaigns AS c ON c.id=i.campaign_id
		LEFT JOIN #__categories as cat ON c.category_id=cat.id
		LEFT JOIN #__jg_donors AS d on d.id=i.donor_id
		LEFT JOIN #__jg_donations AS dona ON dona.id=i.donation_id
		LEFT JOIN #__jg_campaigns_givebacks AS g ON dona.giveback_id=g.id
		LEFT JOIN #__tj_city as city ON city.id = d.city
		LEFT JOIN #__tj_region as region ON region.id = d.state
		LEFT JOIN #__tj_country as country ON country.id = d.country
		 " . $where;

		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectlist();

		return $result;
	}

	/**
	 * Get the donor data if donor is recurring
	 *
	 * @return  Array Donor data
	 *
	 * @since  1.7.3
	 */
	public function getRecurringDonorInfo()
	{
		$user_id = JFactory::getUser()->id;

		if (!$user_id)
		{
			return;
		}

		$db          = JFactory::getDbo();
		$nestedQuery = $db->getQuery(true);
		$nestedQuery->select('MAX(id)');
		$nestedQuery->from($db->quoteName('#__jg_donors'));
		$nestedQuery->where($db->quoteName('user_id') . ' = ' . $user_id);

		$query = $db->getQuery(true);
		$query->select('*, email as paypal_email, country as country_id');
		$query->from($db->quoteName('#__jg_donors'));
		$query->where($db->quoteName('id') . ' = (' . $nestedQuery . ')');

		$db->setQuery($query);

		return $results = $db->loadAssoc();
	}

	/**
	 * Get the donors email id by using donation id
	 *
	 * @param   Array  $donation_ids  Donation id
	 *
	 * @return  Array Donor data
	 *
	 * @since  1.8.1
	 */
	public function getDonorsEmailByDonationId($donation_ids)
	{
		$db = JFactory::getDBO();
		$email_array = array();

		foreach ($donation_ids AS $donation_id)
		{
			$query = $db->getQuery(true);

			$query->select('dona.donor_id AS donor_id');
			$query->from($db->qn('#__jg_donations', 'dona'));
			$query->join('INNER', $db->qn('#__jg_donors', 'd') . ' ON (' . $db->qn('d.id') . ' = ' . $db->qn('dona.donor_id') . ')');
			$query->where($db->qn('dona.id') . ' = ' . $db->quote($donation_id));

			$db->setQuery($query);
			$donor_id = $db->loadResult($query);

			if (!$donor_id)
			{
				continue;
			}

			$query = "SELECT email from #__jg_donors where  id=" . $donor_id;
			$db->setQuery($query);
			$email = $db->loadResult($query);

			if ($email)
			{
				$email_array[] = $email;
			}
		}

		return array_unique($email_array);
	}

	/**
	 * Method to get mail from controller for send mail to user.
	 *
	 * @param   String  $email_id        Email-Id
	 * @param   String  $subject         Email Subject
	 * @param   String  $message         Email Message
	 * @param   String  $attachmentPath  Attachment FIle
	 *
	 * @return result
	 *
	 * @since    1.8.1
	 */
	public function emailtoSelected($email_id, $subject, $message, $attachmentPath = '')
	{
		$donationhelper = new DonationsHelper;
		$com_params     = JComponentHelper::getParams('com_jgive');
		$replytoemail   = $com_params->get('reply_to');

		$where     = '';
		$app       = JFactory::getApplication();
		$mailfrom  = $app->getCfg('mailfrom');
		$fromname  = $app->getCfg('fromname');
		$sitename  = $app->getCfg('sitename');

		if (isset($replytoemail))
		{
			$replytoemail = explode(",", $replytoemail);
		}

		if (is_array($email_id))
		{
			foreach ($email_id AS $email)
			{
				// If donor is deleted dont send reminder
				if (!$email)
				{
					continue;
				}

				$result = $donationhelper->sendmail($email, $subject, $message);
			}
		}

		return $result;
	}

	/**
	 * Method to Call Campaign Details
	 *
	 * @param   Integer  $giveback_id  Giveback ID
	 *
	 * @return array
	 *
	 * @since    1.8.1
	 */
	public function getCompareGiveBack($giveback_id)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select(array('g.*'))
			->from($db->qn('#__jg_campaigns_givebacks', 'g'))
			->where($db->qn('g.id') . ' = ' . $db->quote($giveback_id));
		$db->setQuery($query);
		$giveback_data = $db->loadAssoc();

		return $giveback_data;
	}

	/**
	 * Method to Check to allowed for Complete Payment or not
	 *
	 * @return array
	 *
	 * @since    1.8.1
	 */
	public function getAllowedRetryPayment()
	{
		$order_id_key = JFactory::getApplication()->input->get('donationid');
		$donation_details = $this->getSingleDonationInfo();
		$giveback_id = $donation_details['payment']->giveback_id;
		$giveback_data = $this->getCompareGiveBack($giveback_id);
		$donation_details['giveback'] = $giveback_data;

		$remaining_amount = $donation_details['campaign']->remaining_amount;
		$allow_exceed = $donation_details['campaign']->allow_exceed;
		$minimum_amount = $donation_details['campaign']->minimum_amount;
		$donation_amount = $donation_details['payment']->amount;
		$giveback_amount = $donation_details['giveback']['amount'];
		$camp_end_date = strtotime($donation_details['campaign']->end_date);
		$current_date = strtotime(date('Y-m-d'));

		$retryPayment = new StdClass;

		if ($remaining_amount <= 0 && $allow_exceed == 0)
		{
			$remaining_amount_flag = 1;
			$retryPayment->status = $remaining_amount_flag;
			$retryPayment->msg = JText::_("COM_JGIVE_GOAL_ACHIEVED_MSG");
		}
		elseif ($donation_amount < $giveback_amount)
		{
			$donation_amt_flag = 1;
			$retryPayment->status = $donation_amt_flag;
			$retryPayment->msg = JText::_("COM_JGIVE_DONATION_AMT_GREATER_MSG");
		}
		elseif ($minimum_amount > $donation_amount)
		{
			$minimum_amt_flag = 1;
			$retryPayment->status = $minimum_amt_flag;
			$retryPayment->msg = JText::_("COM_JGIVE_MIN_AMT_GREATER_MSG");
		}
		elseif ($camp_end_date < $current_date)
		{
			$camp_end_date_flag = 1;
			$retryPayment->status = $camp_end_date_flag;
			$retryPayment->msg = JText::_("COM_JGIVE_CAMPAIGN_CLOSED_MSG");
		}
		else
		{
			$allowedRetryPayment_option = 0;
			$retryPayment->status = $allowedRetryPayment_option;
			$retryPayment->msg = JText::_('COM_JGIVE_ALLOWED_COMPLETE_DONATION');
		}

		return $retryPayment;
	}
}
