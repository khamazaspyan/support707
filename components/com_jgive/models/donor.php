<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

/**
 * JgiveModelDonors model class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveModelDonor extends JModelItem
{
	/**
	 * Method &populateState.
	 *
	 * @return void.
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_jgive');

		// Load state from the request userState on edit or from the passed variable on default
		if (JFactory::getApplication()->input->get('layout') == 'edit')
		{
			$id = JFactory::getApplication()->getUserState('com_jgive.edit.donor.id');
		}
		else
		{
			$id = JFactory::getApplication()->input->get('id');
			JFactory::getApplication()->setUserState('com_jgive.edit.donor.id', $id);
		}

		$this->setState('donor.id', $id);

		// Load the parameters.
		$params       = $app->getParams();
		$params_array = $params->toArray();

		if (isset($params_array['item_id']))
		{
			$this->setState('donor.id', $params_array['item_id']);
		}

		$this->setState('params', $params);
	}

	/**
	 * Method &getData.
	 *
	 * @param   integer  $id  The id of the object to get.
	 *
	 * @return item.
	 */
	public function &getData($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id))
			{
				$id = $this->getState('donor.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published)
					{
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties  = $table->getProperties(1);
				$this->_item = JArrayHelper::toObject($properties, 'JObject');
			}
		}

		if (isset($this->_item->user_id))
		{
			$this->_item->user_id_name = JFactory::getUser($this->_item->user_id)->name;
		}

		if (isset($this->_item->campaign_id) && $this->_item->campaign_id != '')
		{
			if (is_object($this->_item->campaign_id))
			{
				$this->_item->campaign_id = JArrayHelper::fromObject($this->_item->campaign_id);
			}

			$values = (is_array($this->_item->campaign_id)) ? $this->_item->campaign_id : explode(',', $this->_item->campaign_id);

			$textValue = array();

			foreach ($values as $value)
			{
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->select('title')->from('`#__jg_campaigns`')->where('id = ' . $db->quote($db->escape($value)));
				$db->setQuery($query);
				$results = $db->loadObject();

				if ($results)
				{
					$textValue[] = $results->title;
				}
			}

			$this->_item->campaign_id = !empty($textValue) ? implode(', ', $textValue) : $this->_item->campaign_id;
		}

		return $this->_item;
	}

	/**
	 * Method getTable.
	 *
	 * @param   String  $type    Type
	 * @param   String  $prefix  Prefix
	 * @param   Array   $config  Config
	 *
	 * @return Id
	 *
	 * @since    1.8.1
	 */
	public function getTable($type = 'Donor', $prefix = 'JgiveTable', $config = array())
	{
		$this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_jgive/tables');

		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method checkin.
	 *
	 * @param   String  $alias  Alias
	 *
	 * @return Id
	 *
	 * @since    1.8.1
	 */
	public function getItemIdByAlias($alias)
	{
		$table = $this->getTable();

		$table->load(array('alias' => $alias));

		return $table->id;
	}

	/**
	 * Method checkin.
	 *
	 * @param   Integer  $id  Id
	 *
	 * @return Boolean
	 *
	 * @since    1.8.1
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int) $this->getState('donor.id');

		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
			if (method_exists($table, 'checkin'))
			{
				if (!$table->checkin($id))
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method checkout.
	 *
	 * @param   Integer  $id  Id
	 *
	 * @return Boolean
	 *
	 * @since    1.8.1
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int) $this->getState('donor.id');

		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
			if (method_exists($table, 'checkout'))
			{
				if (!$table->checkout($user->get('id'), $id))
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method getCategoryName.
	 *
	 * @param   Integer  $id  Id
	 *
	 * @return Object
	 *
	 * @since    1.8.1
	 */
	public function getCategoryName($id)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('title')->from('#__categories')->where('id = ' . $id);
		$db->setQuery($query);

		return $db->loadObject();
	}

	/**
	 * Method publish.
	 *
	 * @param   Integer  $id     Id
	 * @param   String   $state  State
	 *
	 * @return void
	 *
	 * @since    1.8.1
	 */
	public function publish($id, $state)
	{
		$table = $this->getTable();
		$table->load($id);
		$table->state = $state;

		return $table->store();
	}

	/**
	 * Method delete.
	 *
	 * @param   Integer  $id  Id
	 *
	 * @return void
	 *
	 * @since    1.8.1
	 */
	public function delete($id)
	{
		$table = $this->getTable();

		return $table->delete($id);
	}
}
