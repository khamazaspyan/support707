<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.modelform');
jimport('joomla.filesystem.file');
jimport('techjoomla.common');

// Load videos class
require_once JPATH_SITE . '/components/com_jgive/helpers/video/videos.php';

// Load TJ-Fields trait
require_once JPATH_SITE . "/components/com_tjfields/filterFields.php";

/**
 * JgiveModelCampaign for campaign
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JgiveModelCampaign extends JModelForm
{
	use TjfieldsFilterField;

	protected $params;

	/**
	 * Class constructor.
	 *
	 * @since   1.6
	 */
	public function __construct()
	{
		$this->params = JComponentHelper::getParams('com_jgive');
		$this->techjoomlacommon = new TechjoomlaCommon;

		parent::__construct();
	}

	/**
	 * Method to saves a new campaign details
	 *
	 * @param   Object  $extra_jform_data  Form data
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.7
	 */
	public function save($extra_jform_data)
	{
		$is_error_flag_set = 0;

		require_once JPATH_SITE . "/components/com_jgive/helpers/media.php";
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$session = JFactory::getSession();

		$user    = JFactory::getUser();
		$userid  = $user->id;

		if (!$userid)
		{
			$userid = 0;

			return false;
		}

		// Save campaign details
		// Prepare object for insert
		$obj          = $this->createCampaignObject($post, $userid, null);

		// @TODO use lang constant for date
		$obj->created = date(JText::_('Y-m-d H:i:s'));

		// Insert object
		if (!$this->_db->insertObject('#__jg_campaigns', $obj, 'id'))
		{
			echo $this->_db->stderr();

			return false;
		}

		// Get last insert id
		$camp_id = $this->_db->insertid();
		$session->set('camapign_id', $camp_id);

		// Save giveback details
		$params               = JComponentHelper::getParams('com_jgive');
		$creatorfield         = array();
		$show_selected_fields = $params->get('show_selected_fields');
		$show_field           = 0;
		$give_back_cnf        = 0;

		if ($show_selected_fields)
		{
			$creatorfield = $params->get('creatorfield');

			if (isset($creatorfield))
			{
				if (in_array('give_back', $creatorfield))
				{
					$give_back_cnf = 1;
				}
			}
		}
		else
		{
			$show_field = 1;
		}

		// Save give back detail only when show give back is yes (When editing campaign)
		if ($show_field == 1 OR $give_back_cnf == 0)
		{
			$givebackSaveSuccess = $this->saveGivebacks($post, $camp_id);

			if (!$givebackSaveSuccess)
			{
				return false;
			}
		}

		// Save Campaign Updates here
		$updatesSaveSucess = $this->saveUpdates($post, $camp_id);

		if (!$updatesSaveSucess)
		{
			return false;
		}

		// Upload campaign image
		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;

		$jgivemediaHelper = new jgivemediaHelper;
		$img_dimensions   = array();
		$img_dimensions[] = 'small';
		$img_dimensions[] = 'medium';
		$img_dimensions[] = 'large';
		$image_path       = array();

		// @Params filed_name,image dimensions,resize=0 or not_resize=1(upload original)
		$img_org_name = $jgivemediaHelper->imageupload('camp_image', $img_dimensions, 0);

		if (empty($img_org_name))
		{
			if ($img_org_name)
			{
				$uploadSuccess = $campaignHelper->uploadImage($camp_id, 'camp_image', 0, '');

				if (!$uploadSuccess)
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			$db               = JFactory::getDBO();
			$obj              = new stdClass;
			$obj->id          = '';
			$obj->campaign_id = $camp_id;

			$image_upload_path_for_db = 'images/jGive';
			$image_upload_path_for_db .= '/' . $img_org_name;

			$obj->path    = $image_upload_path_for_db;
			$obj->gallery = 0;
			$obj->order   = '';

			if (!$this->_db->insertObject('#__jg_campaigns_images', $obj, 'id'))
			{
				echo $this->_db->stderr();

				return false;
			}

			$uploadSuccess = $this->_db->insertid();
		}

		// @Amol Add new images in gallery
		$file_field  = 'jgive_img_gallery';
		$file_errors = $_FILES[$file_field]['error'];

		foreach ($file_errors as $key => $file_error)
		{
			// If file field is not empty
			if (!$file_error == 4)
			{
				$uploadSuccess = $campaignHelper->uploadImage($camp_id, 'jgive_img_gallery', 0, $key);
			}

			// If file field is not empty
			if ($file_error == 1)
			{
				$app     = JFactory::getApplication();
				$size    = ini_get("upload_max_filesize");
				$message = JText::sprintf('COM_JGIVE_FILE_SIZE_EXCEEDS', $size);
				$app->enqueueMessage($message, 'error');
				$is_error_flag_set = 1;
			}
			elseif($uploadSuccess === false)
			{
				$is_error_flag_set = 1;
			}
		}

		// Call function to save videos
		$video_error_flag_set = JgiveVideos::videoGallery($post, $camp_id);

		// Send campaigns created email to creator & site admin
		if ($params->get('admin_approval'))
		{
			// Email to site admin
			$campaignHelper->sendCmap_create_mail($post, $camp_id);

			// Email to campaign creator
			$campaignHelper->sendCmapCreateMailToPromoter($post, $camp_id);
		}

		// Trigger plugins
		// OnAfterJGiveCampaignSave
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');
		$result = $dispatcher->trigger('OnAfterJGiveCampaignSave', array($camp_id, $post));

		// Trigger after save campaign
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('content');
		$dispatcher->trigger('onAfterCampaignSave', array($post));

		// The error flag is set
		if ($is_error_flag_set)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * Lets you edit campaign
	 *
	 * @param   Object  $extra_jform_data  Form data
	 *
	 * @return  void
	 */
	public function edit($extra_jform_data)
	{
		$is_error_flag_set = 0;
		$oldDetails      = '';
		$oldDetailsImage = '';
		$newDetails      = '';
		$newDetailsImage = '';

		require_once JPATH_SITE . "/components/com_jgive/helpers/media.php";
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		$cid = $post->get('cid');

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;

		$oldDetails      = $this->getCampaignDetails($cid);
		$oldDetailsImage = $campaignHelper->getCampaignImages($cid);

		$promoter_id = $post->get('promoter_id');
		$user        = JFactory::getUser($promoter_id);
		$userid      = $user->id;

		if (!$userid)
		{
			$userid = 0;

			return false;
		}

		$give_back_ids         = $post->get('ids', '', 'Array');
		$givebackDeleteSuccess = $this->deleteGivebacks($give_back_ids, $cid);

		$updatesids = $post->get('updatesids', '', 'Array');
		$updatesDeleteSuccess = $this->deleteUpdates($updatesids, $cid);

		// Save campaign details
		$obj           = $this->createCampaignObject($post, $userid, $cid);
		$obj->modified = date(JText::_('Y-m-d H:i:s'));

		if (!$this->_db->updateObject('#__jg_campaigns', $obj, 'id'))
		{
			echo $this->_db->stderr();

			return false;
		}

		// Save giveback details
		// For edit , delete all existing givebacks for this campaign and re-add new
		$params               = JComponentHelper::getParams('com_jgive');
		$creatorfield         = array();
		$show_selected_fields = $params->get('show_selected_fields');
		$show_field           = 0;
		$give_back_cnf        = 0;

		if ($show_selected_fields)
		{
			$creatorfield = $params->get('creatorfield');

			if (isset($creatorfield))
			{
				if (in_array('give_back', $creatorfield))
				{
					$give_back_cnf = 1;
				}
			}
		}
		else
		{
			$show_field = 1;
		}

		// Save giveback details
		$params               = JComponentHelper::getParams('com_jgive');
		$creatorfield         = array();
		$show_selected_fields = $params->get('show_selected_fields');
		$show_field           = 0;
		$give_back_cnf        = 0;

		if ($show_selected_fields)
		{
			$creatorfield = $params->get('creatorfield');

			if (isset($creatorfield))
			{
				if (in_array('give_back', $creatorfield))
				{
					$give_back_cnf = 1;
				}
			}
		}
		else
		{
			$show_field = 1;
		}

		// Save give back detail only when show give back is yes (When editing campaign)
		if ($show_field == 1 OR $give_back_cnf == 0)
		{
			$givebackSaveSuccess = $this->saveGivebacks($post, $cid);

			if (!$givebackSaveSuccess)
			{
				return false;
			}
		}

		// Save Campaign Updates here
		$updatesSaveSucess = $this->saveUpdates($post, $cid);

		if (!$updatesSaveSucess)
		{
			return false;
		}

		// For edit , delete all existing images for this campaign and re-add new
		// Delete only images so set isvideo to it is not a video
		$isvideo = 0;

		// Delete only gallery image
		$gallery = array(1);

		// @params  campaign Id, existing image ids (not to delete), delete images only, delete gallery images only
		$existing_imgs_ids = $post->get('existing_imgs_ids', '', 'Array');
		$campaignHelper->deleteGalleryRecords($cid, $existing_imgs_ids, $isvideo, $gallery);

		// Delete video records
		$isvideo = 1;

		// Delete default & gallery videos
		$gallery = array(0,1);

		// @params  campaign Id, existing image ids (not to delete), delete images only, delete videos
		$campaignHelper->deleteGalleryRecords($cid, $existing_imgs_ids, $isvideo, $gallery);

		// Function added to delete videos - Added by Nidhi
		$upload_option = $post->get('upload_options', '', 'Array');
		$video_ids = $post->get('video_ids', '', 'Array');

		// Upload image
		$file_field = 'camp_image';
		$file_error = $_FILES[$file_field]['error'];

		$jgivemediaHelper = new jgivemediaHelper;
		$img_dimensions   = array();
		$img_dimensions[] = 'small';
		$img_dimensions[] = 'medium';
		$img_dimensions[] = 'large';
		$image_path       = array();

		// If file field is not empty
		if (!$file_error == 4)
		{
			$main_img_id  = $post->get('main_img_id');

			// @params filed_name,image dimensions,resize=0 or not_resize=1(upload original)
			$img_org_name = $jgivemediaHelper->imageupload('camp_image', $img_dimensions, 0);

			// Before deleting image get image path to delete image from folder
			$campaignHelper->deleteCampaignMainImage($cid);

			// Handling error Not Valid Extension
			if (empty($img_org_name))
			{
				if ($img_org_name)
				{
					$uploadSuccess = $campaignHelper->uploadImage($cid, 'camp_image', $main_img_id, 0);
				}
				else
				{
					return false;
				}
			}
			else
			{
				$obj              = new stdClass;
				$obj->id          = $main_img_id;
				$obj->campaign_id = $cid;

				$image_upload_path_for_db = 'images/jGive';
				$image_upload_path_for_db .= '/' . $img_org_name;

				$obj->path    = $image_upload_path_for_db;
				$obj->gallery = 0;
				$obj->order   = '';

				if ($main_img_id)
				{
					if (!$this->_db->updateObject('#__jg_campaigns_images', $obj, 'id'))
					{
						echo $this->_db->stderr();

						return false;
					}
				}
				else
				{
					if (!$this->_db->insertObject('#__jg_campaigns_images', $obj, 'id'))
					{
						echo $this->_db->stderr();

						return false;
					}

					$main_img_id = $this->_db->insertid();
				}
			}
		}

		// @Amol Add new images in gallery
		$file_field  = 'jgive_img_gallery';
		$file_errors = $_FILES[$file_field]['error'];

		foreach ($file_errors as $key => $file_error)
		{
			// If file field is not empty
			if (!$file_error == 4)
			{
				$uploadSuccess = $campaignHelper->uploadImage($cid, 'jgive_img_gallery', 0, $key);
			}

			// If file field is not empty
			if ($file_error == 1 )
			{
				$app     = JFactory::getApplication();
				$size    = ini_get("upload_max_filesize");
				$message = JText::sprintf('COM_JGIVE_FILE_SIZE_EXCEEDS', $size);
				$app->enqueueMessage($message, 'error');
				$is_error_flag_set = 1;
			}
			elseif($uploadSuccess === false)
			{
				$is_error_flag_set = 1;
			}
		}

		// Video Gallery
		$video_error_flag_set = JgiveVideos::videoGallery($post, $cid);

		// Save extra fields information
		$data = array();
		$data['content_id'] = $cid;
		$data['client'] = 'com_jgive.campaign';
		$data['fieldsvalue'] = $extra_jform_data;

		$this->saveExtraFields($data);

		/** Delete
		 Trigger plugins
		 OnAfterJGiveCampaignSave */

		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('system');

		$newDetails      = $this->getCampaignDetails($cid);
		$newDetailsImage = $campaignHelper->getCampaignImages($cid);

		$result = $dispatcher->trigger('OnAfterJGiveCampaignEdit', array($cid,$post,$newDetails, $oldDetails));

		// Trigger after edit campaign
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('content');
		$dispatcher->trigger('onAfterCampaignEdit', array($post));

		// Added by Sneha
		// Option for sending mail on edit
		$params  = JComponentHelper::getParams('com_jgive');
		$on_edit = $params->get('mail_on_edit');

		if ($on_edit == 1)
		{
			// Email to site admin
			$campaignHelper->send_mail_on_edit($post, $cid);
		}

		// The error flag is set
		if ($is_error_flag_set)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * Used when creating/editing a campaign
	 *
	 * @param   Object  $post    Form data
	 * @param   INT     $userid  User ID
	 * @param   INT     $cid     Campaign ID
	 *
	 * @return  void
	 */
	public function createCampaignObject($post, $userid, $cid)
	{
		$obj    = new stdClass;
		$params = JComponentHelper::getParams('com_jgive');

		$helperPath = JPATH_SITE . '/components/com_jgive/helpers/campaign.php';

		if (!class_exists('campaignHelper'))
		{
			JLoader::register('campaignHelper', $helperPath);
			JLoader::load('campaignHelper');
		}

		$campaignHelper = new campaignHelper;
		$jgiveFrontendHelper = new jgiveFrontendHelper;

		// Editing campaign
		$obj->id          = $cid;
		$obj->creator_id  = $userid;
		$obj->category_id = '';
		$obj->title       = $post->get('title', '', 'STRING');
		$alias            = $post->get('alias', '', 'RAW');

		if (empty($alias))
		{
			$alias = $obj->title;
		}

		$alias      = $campaignHelper->formatttedTitle($alias, $obj->id);
		$obj->alias = $campaignHelper->formatttedVanityURL($alias, $alias, $obj->id);

		if ($post->get('type'))
		{
			$obj->type = $post->get('type', '', 'STRING');
		}
		else
		{
			// Get the campaign set by admin to be created & save in db
			$camp_type = $params->get('camp_type');

			if (is_array($camp_type))
			{
				if (isset($camp_type[0]))
				{
					$obj->type = $camp_type[0];
				}
			}
			elseif (!empty($camp_type))
			{
				$obj->type = $camp_type;
			}
			else
			{
				$obj->type = 'donation';
			}
		}

		$obj->category_id  = $post->get('campaign_category', '', 'INT');

		// Org_ind_type since version 1.5.1
		$obj->org_ind_type = $post->get('org_ind_type', '', 'STRING');

		// Getting Meta Data
		$obj->meta_data = $post->get('meta_data', '', 'STRING');
		$obj->meta_desc = $post->get('meta_desc', '', 'STRING');

		if ($post->get('max_donors'))
		{
			$obj->max_donors = (int) $post->get('max_donors', '', 'INT');
		}

		if ($post->get('minimum_amount'))
		{
			$obj->minimum_amount = (int) $post->get('minimum_amount', '', 'FLOAT');
		}

		$obj->short_description = $post->get('short_desc', '', 'STRING');
		$obj->long_description = JRequest::getVar('long_desc', '', 'post', 'string', JREQUEST_ALLOWRAW);

		$obj->goal_amount  = $post->get('goal_amount', '', 'FLOAT');
		$obj->paypal_email = $post->get('paypal_email', '', 'STRING');

		$obj->first_name = $post->get('first_name', '', 'STRING');
		$obj->last_name  = $post->get('last_name', '', 'STRING');

		if ($post->get('address'))
		{
			$obj->address = $post->get('address', '', 'STRING');
		}

		if ($post->get('address2'))
		{
			$obj->address2 = $post->get('address2', '', 'STRING');
		}

		// For city since version 1.6
		$other_city_check = $post->get('other_city_check', '', 'STRING');

		if (!empty($other_city_check))
		{
			$obj->city       = $post->get('other_city', '', 'STRING');
			$obj->other_city = 1;
		}
		elseif (($post->get('city')) && $post->get('city') != '')
		{
			$obj->city       = $post->get('city');
			$obj->other_city = 0;
		}

		$obj->country = $post->get('country');
		$obj->state   = $post->get('state');

		if ($post->get('zip'))
		{
			$obj->zip = $post->get('zip', '', 'STRING');
		}

		if ($post->get('phone'))
		{
			$obj->phone = $post->get('phone', '', 'STRING');
		}

		if ($post->get('group_name'))
		{
			$obj->group_name = $post->get('group_name', '', 'STRING');
		}

		if ($post->get('website_address'))
		{
			$obj->website_address = $post->get('website_address', '', 'STRING');
		}

		// Get formatted start time & end time for campaign.

		$formattedTime = $campaignHelper->getFormattedTime($post);

		$obj->start_date = $post->get('start_date', '', 'STRING') . " " . $formattedTime['campaign_start_time'];

		$campaign_period_in_days = $params->get('campaign_period_in_days');

		if (empty($campaign_period_in_days) || $campaign_period_in_days == 0)
		{
			$obj->end_date   = $post->get('end_date', '', 'STRING') . " " . $formattedTime['campaign_end_time'];
		}
		else
		{
			$days_limit    = $post->get('days_limit', '', 'STRING');
			$obj->end_date = date('Y-m-d H:i:s', strtotime($obj->start_date) + (24 * 3600 * $days_limit));
		}

		$obj->start_date = $this->techjoomlacommon->getDateInUtc($obj->start_date);
		$obj->end_date = $this->techjoomlacommon->getDateInUtc($obj->end_date);

		if ($params->get('admin_approval'))
		{
			$obj->published = 0;
		}
		else
		{
			$obj->published = $post->get('publish', '', 'STRING');
		}

		$obj->allow_exceed         = $post->get('allow_exceed', '', 'INT');
		$obj->allow_view_donations = $post->get('show_public', '', 'INT');
		$obj->internal_use         = $post->get('internal_use', '', 'STRING');

		// Jomsocial groupid
		$js_group = $post->get('js_group', '', 'INT');

		if ($js_group)
		{
			$obj->js_groupid = $post->get('js_group', '', 'INT');
		}

		$obj->video_on_details_page = $post->get('video_on_details_page', '0', 'INT');

		return $obj;
	}

	/**
	 * Save campaign givebacks
	 *
	 * @param   Array  $post  Form data
	 * @param   INT    $cid   Campaign ID
	 *
	 * @return  Object
	 */
	public function saveGivebacks($post, $cid)
	{
		$give_back_value    = $post->get('give_back_value', '', 'Array');
		$give_back_details  = $post->get('give_back_details', '', 'Array');
		$giveback_count     = count($post->get('give_back_value', '', 'Array'));
		$give_back_ids      = $post->get('ids', '', 'Array');
		$give_back_order    = $post->get('give_back_order', '', 'Array');
		$give_back_quantity = $post->get('give_back_quantity', '', 'Array');

		$i = 0;

		foreach ($give_back_ids as $key => $value)
		{
			$coupon = new stdClass;

			if ($value)
			{
				$coupon->id = $value;
			}
			else
			{
				$coupon->id = '';
			}

			$coupon->campaign_id = $cid;

			if (!empty($coupon->campaign_id))
			{
				$coupon->amount         = $give_back_value[$key];
				$coupon->description    = $give_back_details[$key];
				$coupon->order          = $i++;
				$coupon->total_quantity = $give_back_quantity[$key];

				if ($value)
				{
					if (!$this->_db->updateObject('#__jg_campaigns_givebacks', $coupon, 'id'))
					{
						return false;
					}
				}
				elseif (!empty($coupon->amount))
				{
					$coupon->quantity = 0;

					if (!$this->_db->insertObject('#__jg_campaigns_givebacks', $coupon, 'id'))
					{
						echo $this->_db->stderr();

						return false;
					}
				}

				if ($value)
				{
					$giveback_id = $value;
				}
				else
				{
					$giveback_id = $this->_db->insertid();
				}

				require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
				$campaignHelper   = new campaignHelper;
				$couponimgSuccess = $campaignHelper->uploadImageForGiveback($key, $giveback_id, $cid);
			}
		}

		return true;
	}

	/**
	 * Function Save Campaign Updates Details
	 *
	 * @param   Array  $post  Form data
	 * @param   INT    $cid   Campaign ID
	 *
	 * @return  Object
	 */
	public function saveUpdates($post, $cid)
	{
		$updates_title = $post->get('updates_title', '', 'Array');
		$updates_desc  = $post->get('updates_desc', '', 'Array');
		$updates_cdate  = date('Y-m-d H:i:s');
		$updates_mdate  = date('Y-m-d H:i:s');
		$campaign_updates_count = count($post->get('updates_title', '', 'Array'));
		$updatesids = $post->get('updatesids', '', 'Array');

		$i = 0;

		foreach ($updatesids as $key => $value)
		{
			$updated_news = new stdClass;

			if ($value)
			{
				$updated_news->id = $value;
				$updated_news->mdate = $updates_mdate;
			}
			else
			{
				$updated_news->id = '';
				$updated_news->cdate = $updates_cdate;
			}

			$updated_news->campaign_id = $cid;

			if (!empty($updated_news->campaign_id))
			{
				$updated_news->title = $updates_title[$key];
				$updated_news->description = $updates_desc[$key];

				if ($value)
				{
					if (!$this->_db->updateObject('#__jg_updates', $updated_news, 'id'))
					{
						return false;
					}
				}
				elseif (!empty($updated_news->title))
				{
					if (!$this->_db->insertObject('#__jg_updates', $updated_news, 'id'))
					{
						echo $this->_db->stderr();

						return false;
					}
				}
			}
		}

		return true;
	}

	/**
	 * Delete entries from giveback table
	 *
	 * @param   INT  $give_back_ids  Giveback ID
	 * @param   INT  $cid            Campaign ID
	 *
	 * @return  void
	 */
	public function deleteGivebacks($give_back_ids, $cid)
	{
		$give_back_idsarr = (array) $give_back_ids;
		$query            = "SELECT id FROM #__jg_campaigns_givebacks WHERE campaign_id=" . $cid;
		$this->_db->setQuery($query);
		$type_ids = $this->_db->loadColumn();
		$diff     = array_diff($type_ids, $give_back_idsarr);
		$diffids  = implode("','", $diff);
		$query    = "DELETE FROM #__jg_campaigns_givebacks WHERE id IN ('" . $diffids . "') AND campaign_id=" . $cid;
		$this->_db->setQuery($query);
		$this->_db->execute();
	}

	/**
	 * Delete entries from Updates table
	 *
	 * @param   INT  $updatesids  Updates ID
	 * @param   INT  $cid         Campaign ID
	 *
	 * @return  void
	 */
	public function deleteUpdates($updatesids, $cid)
	{
		// Camapign Updates ids Array
		$updates_idarr = (array) $updatesids;
		$query            = "SELECT id FROM #__jg_updates WHERE campaign_id=" . $cid;
		$this->_db->setQuery($query);

		// Fetch Array of existed Campaign Updates
		$type_ids = $this->_db->loadColumn();

		// Finding difference between Existed Array of updates and edited data records
		$diff     = array_diff($type_ids, $updates_idarr);

		$diffids  = implode("','", $diff);

		$query    = "DELETE FROM #__jg_updates WHERE id IN ('" . $diffids . "') AND campaign_id=" . $cid;
		$this->_db->setQuery($query);
		$this->_db->execute();
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $cid  The id of the primary key.
	 *
	 * @return  mixed  $cdata  Object on success, false on failure.
	 *
	 * @since	1.7
	 */
	public function getCampaign($cid = '')
	{
		if (empty($cid))
		{
			$cid = JRequest::getInt('cid');

			// For single campaign menu
			if (empty($cid))
			{
				$cid = JRequest::getInt('id');
			}
		}

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;
		$query = "SELECT c.*
		FROM `#__jg_campaigns` AS c
		WHERE c.id=" . $cid;
		$this->_db->setQuery($query);
		$campaign          = $this->_db->loadObject();
		$cdata['campaign'] = $campaign;

		// Needed for loading states
		require_once JPATH_SITE . "/components/com_jgive/helper.php";
		$jgiveFrontendHelper = new jgiveFrontendHelper;

		$cdata['campaign']->country_name = $jgiveFrontendHelper->getCountryNameFromId($cdata['campaign']->country);
		$cdata['campaign']->state_name   = $jgiveFrontendHelper->getRegionNameFromId($cdata['campaign']->state, $cdata['campaign']->country);

		if ($cdata['campaign']->other_city == 0)
		{
			$cdata['campaign']->city_name = $jgiveFrontendHelper->getCityNameFromId($cdata['campaign']->city, $cdata['campaign']->country);
		}
		else
		{
			$cdata['campaign']->city_name = $cdata['campaign']->city;
		}

		// Get campaign images
		$cdata['images'] = $campaignHelper->getCampaignImages($cid);
		$cdata['video']  = $this->_getCampaignMedia($cid);

		// Get default video path
		if ($cdata['campaign']->video_on_details_page)
		{
			$cdata['campaign']->videoPlgParams    = $this->_getDefaultVideo($cid);
		}

		// Get campaign givebacks
		$cdata['givebacks'] = $campaignHelper->getCampaignGivebacks($cid);

		// Get campaign updates
		$cdata['updates'] = $campaignHelper->getCampaignUpdates($cid);

		// Get orders count
		$cdata['orders_count'] = $campaignHelper->getCampaignOrdersCount($cid);

		// Get campaign donors
		$params = JComponentHelper::getParams('com_jgive');

		$limit = 5;

		$cdata['donors'] = $campaignHelper->getCampaignDonors($cid, 0, $limit);

		// Get campaign donors count according to country
		$cdata['areawise_donors'] = $campaignHelper->getCampaignDonorsCountAreaWise($cid);

		$layout = JFactory::getApplication()->input->get('layout', 'create');

		if ($layout == 'single')
		{
			// Get campaign amounts
			$amounts                             = $campaignHelper->getCampaignAmounts($cid);
			$cdata['campaign']->amount_received  = $amounts['amount_received'];
			$cdata['campaign']->remaining_amount = $amounts['remaining_amount'];

			// Get campaign promoter info
			require_once JPATH_SITE . "/components/com_jgive/helpers/integrations.php";
			$JgiveIntegrationsHelper                     = new JgiveIntegrationsHelper;
			$cdata['campaign']->creator_avatar      = $JgiveIntegrationsHelper->getUserAvatar($cdata['campaign']->creator_id);
			$cdata['campaign']->creator_profile_url = $JgiveIntegrationsHelper->getUserProfileUrl($cdata['campaign']->creator_id);

			// Get campaign categories
			if (!empty($cdata['campaign']->category_id))
			{
				$cdata['campaign']->catname = $campaignHelper->getCatname($campaign->category_id);
			}

			$camp_startdate = JFactory::getDate($cdata['campaign']->start_date)->Format(JText::_('Y-m-d H:i:s'));
			$camp_enddate = JFactory::getDate($cdata['campaign']->end_date)->Format(JText::_('Y-m-d H:i:s'));
			$curr_date = JFactory::getDate()->Format(JText::_('Y-m-d H:i:s'));

			if ($curr_date < $camp_startdate)
			{
				// Campaign Not yet started
				$donteButtonStatusFlag = -1;
			}
			elseif ($curr_date > $camp_enddate)
			{
				// Camp closed
				$donteButtonStatusFlag = 0;
			}
			else
			{
				$donteButtonStatusFlag = 1;
			}

			$cdata['campaign']->donateBtnShowStatus = $donteButtonStatusFlag;
		}

		// Getting total no. of donors per campaign
		require_once JPATH_SITE . '/components/com_jgive/models/campaigns.php';
		$jgiveModelCampaigns = new JgiveModelCampaigns;
		$cdata['campaign']->totalNoOfDonors = $jgiveModelCampaigns->getDonorsCountPerCamp($cdata['campaign']->id);

		$cdata['campaign']->goneDays = $campaignHelper->getDateDiffInDays(date("Y-m-d"), $cdata['campaign']->start_date);
		$cdata['campaign']->days_limit = $campaignHelper->getDateDiffInDays($cdata['campaign']->start_date, $cdata['campaign']->end_date);

		return $cdata;
	}

	/**
	 * Get Thumbnail video
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  Video Thumbnail
	 */
	public function _getCampaignMedia($cid)
	{
		// Create a new query object.
		try
		{
			$query = $this->_db->getQuery(true);

			$query->select(array('id','filename','path','type','`default`','content_id','thumb_filename','thumb_path'));
			$query->select(array('url'));

			$query->from('#__jg_campaigns_media');
			$query->where('content_id  = ' . $cid);
			$query->order('type ASC');

			$this->_db->setQuery($query);
			$media = $this->_db->loadObjectList();

			return $media;
		}
		catch (Exception $e)
		{
			$app->enqueueMessage($e->getMessage(), 'error');
			$this->setError($e->getMessage());

			throw new Exception($e->getMessage());
		}
	}

	/**
	 * Get Default Video
	 *
	 * @param   INT  $cid  Campaign Id
	 *
	 * @return  Default video path
	 */
	public function _getDefaultVideo($cid)
	{
		$app = JFactory::getApplication();

		// Load media helper
		$helperPath = JPATH_SITE . '/components/com_jgive/helpers/media.php';

		if (!class_exists('jgivemediaHelper'))
		{
			JLoader::register('jgivemediaHelper', $helperPath);
			JLoader::load('jgivemediaHelper');
		}

		if (!empty($cid))
		{
			try
			{
				$video_params = array();

				// Create a new query object.
				$query = $this->_db->getQuery(true);

				$query->select(array('id','type'));

				$query->from("#__jg_campaigns_media");
				$query->where("content_id  = " . $cid);
				$query->where("`default` = 1");

				$this->_db->setQuery($query);
				$videoDetails = $this->_db->loadObject();

				if (!empty($videoDetails->id))
				{
					$videoDetails = $this->getVideoData($videoDetails->id, $videoDetails->type);

					if (!empty($videoDetails))
					{
						switch ($videoDetails->type)
						{
							// For uploaded files
							case 'video':

								// File path
								$video_params['file'] = JUri::root() . $videoDetails->path;

								// Plugin to call to pay video
								$video_params['plugin'] = 'jwplayer';
							break;

							// Video provider youtube
							case 'youtube':
								// Get youtube video ID from embed url
								$videoId  = JgiveMediaHelper::videoId($videoDetails->type, $videoDetails->url);

								if (!empty($videoId))
								{
									$video_params['file']    = 'https://www.youtube.com/watch?v=' . $videoId;
									$video_params['videoId'] = $videoId;

									// Plugin to call to pay video
									$video_params['plugin'] = 'jwplayer';
								}
							break;

							// Video provider vimeo
							case 'vimeo':
								$videoId  = JgiveMediaHelper::videoId($videoDetails->type, $videoDetails->url);

								if (!empty($videoId))
								{
									// Get youtube video ID from embed url
									$video_params['videoId'] = $videoId;

									// Plugin to call to pay video
									$video_params['plugin'] = 'vimeo';
								}
							break;

							// Other video provider than above
							default:
								// For future
							break;
						}
					}
				}
			}
			catch (Exception $e)
			{
				$app->enqueueMessage($e->getMessage(), 'error');
				$this->setError($e->getMessage());

				throw new Exception($e->getMessage());
			}

			return $video_params;
		}
	}

	/**
	 * Method to get a Campaign Category.
	 *
	 * @return  mixed  $item  Object on success, false on failure.
	 *
	 * @since	1.7
	 */
	public function getCampaignsCats()
	{
		$lang = JFactory::getLanguage();
		$tag  = $lang->gettag();

		if (JFactory::getApplication()->input->get('cid'))
		{
			$details  = $this->getCampaignDetails(JFactory::getApplication()->input->get('cid'));
			$camp_cat = $details->category_id;
			$config = array('filter.published' => array(1),'filter.language' => array('*',$tag));

			$categoryoptions = JHtml::_('category.options', 'com_jgive', $config);
			$dropdown        = JHtml::_(
			'select.genericlist', $categoryoptions, 'campaign_category', 'size="1" class="required"', 'value', 'text', intval($camp_cat)
			);
		}
		else
		{
			$categories = JHtml::_('category.options', 'com_jgive', $config = array('filter.published' => array(1),'filter.language' => array('*', $tag)));

			if (!empty($categories))
			{
				$dropdown = JHtml::_('select.genericlist', $categories, 'campaign_category', 'size="1" class="required"');
			}
			else
			{
				$msg = '<p class="text-warning">' . JText::_('COM_JGIVE_NO_CATEGORY') . '</p>';

				$drop = JHtml::_('select.genericlist', $categories, 'campaign_category', 'size="1" class="required"');
				$dropdown = $drop . $msg;
			}
		}

		return $dropdown;
	}

	/**
	 * Get Campaign details
	 *
	 * @param   integer  $c_id  Campaign id
	 *
	 * @return   INT  Js group
	 *
	 * since 1.7
	 */
	public function getCampaignDetails($c_id)
	{
		$query = "SELECT * FROM #__jg_campaigns WHERE id='" . $c_id . "'";
		$this->_db->setQuery($query);

		$item = $this->_db->loadObject();

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;

		// Get campaign images
		$item->images = $campaignHelper->getCampaignImages($item->id);

		// Get campaign videos
		$item->video = $this->_getCampaignMedia($item->id);

		// Get campaign givebacks
		$item->givebacks = $campaignHelper->getCampaignGivebacks($item->id);

		// Get Campaign Updates
		$item->updates = $campaignHelper->getCampaignUpdates($item->id);

		$item->days_limit = $campaignHelper->getDateDiffInDays($item->start_date, $item->end_date);

		return $item;
	}

	/**
	 * Get JS usergroup
	 *
	 * @return   INT  Js group
	 *
	 * since 1.7
	 */
	public function getJS_usergroup()
	{
		require_once JPATH_SITE . "/components/com_jgive/helpers/integrations.php";
		$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;

		return $result = $JgiveIntegrationsHelper->getJS_usergroup();
	}

	/**
	 * CSV data campaign donor
	 *
	 * @param   integer  $cid          Campaign id
	 * @param   integer  $jgive_index  Limit start
	 *
	 * @return   Object  Donor data
	 *
	 * since 1.7
	 */
	public function viewMoreDonorReports($cid, $jgive_index)
	{
		$campaignHelper = new campaignHelper;
		$params         = JComponentHelper::getParams('com_jgive');

		$limit_start = $jgive_index;

		$donors              = $campaignHelper->getCampaignDonors($cid, $limit_start - 1, 10);
		$this->currency_code = $params->get('currency');

		$html = "";

		foreach ($donors as $donor)
		{
			$html .= "<tr> <td data-title=" . JText::_('COM_JGIVE_GIVEBACK_NUMBER') . " >" . $jgive_index . "</td>

				<td data-title=" . JText::_('COM_JGIVE_DONOR_NAME') . ">";

			// If no avatar, use default avatar
			if (!$donor->avatar)
			{
				$donor->avatar = JUri::root(true) . '/media/com_jgive/images/default_avatar.png';
			}

			$title = $donor->first_name . " " . $donor->last_name;

			if (!empty($donor->profile_url) && $donor->user_id != 0)
			{
				$html .= "<a href=" . $donor->profile_url . " target='_blank'>" . $title . "
						</a>";
			}
			else
			{
				$html .= $title;
			}

			$html .= "<br/>
					<img class='com_jgive_img_48_48' src=" . $donor->avatar . " />
					<br/>";

			if ($donor->annonymous_donation)
			{
				$html .= JText::_("COM_JGIVE_ANNONYMOUS_DONATION_MSG_OWNER") . "
						<br/>";
			}

			$html .= "</td>";
			$html .= "<td data-title=" . JText::_('COM_JGIVE_DONATED_AMOUNUT') . ">

						" . JText::_('COM_JGIVE_DONATED_AMT') . ":" .
						$donor->amount . " " . $this->currency_code . "<br>" .
						JText::_('COM_JGIVE_GIVEBACK_SELECTED') . ":";

			if ($donor->giveback_id)
			{
				$html .= JText::_('COM_JGIVE_YES') . "<br>" .
					JText::_('COM_JGIVE_GIVEBACK_MIN_VALUE') . ":" . $donor->giveback_value . " " .
					$this->currency_code . "<br>" . JText::_('COM_JGIVE_GIVEBACK_DESC') . ":";

				if (strlen($donor->gb_description > 50))
				{
					$html .= substr($donor->gb_description, 0, 50) . "...";
				}
				else
				{
					$html .= $donor->gb_description;
				}
			}
			else
			{
				$html .= JText::_('COM_JGIVE_NO');
			}

			$html .= "</td>";
			$html .= "<td data-title=" . JText::_('COM_JGIVE_DONATION_DATE') . ">" . $donor->cdate . "</td> ";

			$html .= "</tr>";
			$jgive_index++;
		}

		$result = array();

		$result['jgive_index'] = $jgive_index;
		$result['records']     = $html;

		return $result;
	}

	/**
	 * CSV data campaign donor
	 *
	 * @param   integer  $cid          Campaign id
	 * @param   integer  $limit_start  Limit start
	 *
	 * @return   Object  Donor data
	 *
	 * since 1.7
	 */
	public function viewMoreDonorProPic($cid, $limit_start)
	{
		$campaignHelper = new campaignHelper;
		$result = array();

		JLoader::import('JgiveFrontendHelper', JPATH_SITE . '/components/com_jgive/helpers');
		$jgiveFrontendHelper  = new jgiveFrontendHelper;

		$params = JComponentHelper::getParams('com_jgive');
		$donors              = $campaignHelper->getCampaignDonors($cid, $limit_start - 1, 10);
		$this->currency_code = $params->get('currency');

		$html = "";

		$donors_html_view = $jgiveFrontendHelper->getViewpath('campaign', 'single_donorslist');

		foreach ($donors as $this->donor)
		{
			ob_start();
			include $donors_html_view;
			$html .= ob_get_contents();
			ob_end_clean();

			$limit_start ++;
		}

		$result = array();
		$result['jgive_index'] = $limit_start;
		$result['records']     = $html;

		return $result;
	}

	/**
	 * CSV data campaign donor
	 *
	 * @param   integer  $cid  Campaign id
	 *
	 * @return   Object  Donor data
	 *
	 * since 1.7
	 */
	public function getCsvDataCampaignDonor($cid)
	{
		$campaignHelper = new campaignHelper;

		$limit_start = 0;
		$limit       = 10;
		$getCsvData  = 1;

		return $donors = $campaignHelper->getCampaignDonors($cid, $limit_start, $limit, $getCsvData);
	}

	/**
	 * Get video data - Added by Nidhi
	 *
	 * @param   integer  $vid   Video id
	 * @param   string   $type  Video provider e.g youtube, vimeo, upload
	 *
	 * @return   Object   video data
	 *
	 * since 1.7
	 */
	public function getVideoData($vid, $type)
	{
		$app = JFactory::getApplication();

		if (!empty($vid) && !empty($type))
		{
			try
			{
				// Create a new query object.
				$query = $this->_db->getQuery(true);

				$query->select(array('id','filename','path','url','type','content_id','thumb_filename','thumb_path'));
				$query->from('#__jg_campaigns_media');
				$query->where('id  = ' . $vid);

				$this->_db->setQuery($query);
				$results = $this->_db->loadObject();
			}
			catch (Exception $e)
			{
				$app->enqueueMessage($e->getMessage(), 'error');
				$this->setError($e->getMessage());

				return false;
			}

			return $results;
		}
	}

	/**
	 * Upload video
	 *
	 * @return void
	 * since 1.7
	 */
	public function videoUpload()
	{
		return JgiveVideos::videoUpload();
	}

	/**
	 * Delete video
	 *
	 * @param   integer  $videoid  Video id
	 * @param   string   $type     Type of video
	 *
	 * @return   boolean
	 *
	 * since 1.7
	 */
	public function deleteGalleryVideos($videoid, $type)
	{
		$app = JFactory::getApplication();

		try
		{
			if ($videoid)
			{
				if ($type == "video")
				{
					$query = $this->_db->getQuery(true);

					$query->select(array('path','thumb_path'));
					$query->from('#__jg_campaigns_media');
					$query->where('id  = ' . $videoid);

					$this->_db->setQuery($query);
					$galleryVideoToDelPath = $this->_db->loadObject();

					if ($galleryVideoToDelPath)
					{
						$this->deleteFile($galleryVideoToDelPath);
					}
				}

				// Jgive plugin onCampaignVideoDelete
				JPluginHelper::importPlugin('system');
				$dispatcher = JDispatcher::getInstance();
				$dispatcher->trigger('onAfterCampaignVideoDelete', array($videoid, $type));

				// Create a new query object.
				$query = $this->_db->getQuery(true);

				$query->delete($this->_db->quoteName('#__jg_campaigns_media'));
				$query->where($this->_db->quoteName('id') . '=' . $this->_db->quote($videoid));

				$this->_db->setQuery($query);
				$result = $this->_db->execute();
			}

			return true;
		}
		catch (Exception $e)
		{
			$app->enqueueMessage($e->getMessage(), 'error');
			$this->setError($e->getMessage());

			return false;
		}
	}

	/**
	 * Delete Video files
	 *
	 * @param   array  $galleryVideoToDelPath  Path to thumbnail file
	 *
	 * @return   void
	 *
	 * @since 1.7
	 */
	public function deleteFile($galleryVideoToDelPath)
	{
		// If to delete video file then we will need to specify video file location
		$thumbpath = JPATH_SITE . $galleryVideoToDelPath->thumb_path;
		$videopath = JPATH_SITE . $galleryVideoToDelPath->path;

		if (JFile::exists($thumbpath) && JFile::exists($videopath))
		{
			JFile::delete($thumbpath);
			JFile::delete($videopath);
		}
	}

	/**
	 * Mark video as default
	 *
	 * @param   integer  $videoid  Video Id
	 * @param   integer  $camp_id  Campaign Id
	 * @param   string   $type     Type of video
	 *
	 * @return  boolean
	 *
	 * since 1.7
	 */
	public function setDefaultVideo($videoid, $camp_id, $type)
	{
		if ($videoid)
		{
			try
			{
				// Reset existing default marked video
				$query = $this->_db->getQuery(true);

				// Fields to update.
				$fields = array(
					$this->_db->quoteName('default') . ' = 0'
				);

				// Conditions for which records should be updated.
				$conditions = array(
					$this->_db->quoteName('content_id') . ' = ' . $camp_id
				);

				$query->update($this->_db->quoteName('#__jg_campaigns_media'))->set($fields)->where($conditions);

				$this->_db->setQuery($query);

				$result = $this->_db->execute();

				$obj          = new stdClass;
				$obj->id      = $videoid;
				$obj->default = 1;

				$this->_db->updateObject('#__jg_campaigns_media', $obj, 'id');
			}
			catch (Exception $e)
			{
				$app->enqueueMessage($e->getMessage(), 'error');
				$this->setError($e->getMessage());

				return false;
			}
		}

		return true;
	}

	/**
	 * Method to get the profile form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since  1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_jgive.campaign', 'campaignform', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get Graph Data
	 *
	 * @param   Integer  $duration  This show the duration for graph data
	 * @param   Integer  $id        This show the camp id or user id
	 *
	 * @return  Json data
	 *
	 * @since  2.0
	 */
	public function getGarphData($duration, $id)
	{
		if ($duration == 0)
		{
			$graphDuration = 7;
		}
		elseif ($duration == 1)
		{
			$graphDuration = 30;
		}

		$todate = JFactory::getDate(date('Y-m-d'))->Format(JText::_('Y-m-d'));

		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$query = $db->getQuery(true);

		if ($duration == 0 || $duration == 1)
		{
			$backdate = date('Y-m-d', strtotime(date('Y-m-d') . ' - ' . $graphDuration . ' days'));

			$query->select('SUM(o.amount) AS donation_amount');
			$query->select('DATE(o.cdate) AS cdate');
			$query->select('COUNT(o.id) AS orders_count');
			$query->from($db->qn('#__jg_orders', 'o'));
			$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('c.id') . ' = ' . $db->qn('o.campaign_id') . ')');
			$query->where($db->qn('c.id') . ' = ' . $db->quote($id) . ' AND ' . $db->qn('o.status') . ' = ' . $db->quote('C'));
			$query->where('DATE(' . $db->qn('o.cdate') . ')' . ' >= ' . $db->quote($backdate) . ' AND ' . 'DATE(' .
			$db->qn('o.cdate') . ')' . ' <= ' . $db->quote($todate)
			);
			$query->group('DATE(' . $db->qn('o.cdate') . ')');
			$query->order($db->qn('o.cdate') . 'DESC');

			$db->setQuery($query);
			$results = $db->loadObjectList();
		}
		elseif ($duration == 2)
		{
			$curdate    = date('Y-m-d');
			$back_year  = date('Y') - 1;
			$back_month = date('m') + 1;
			$backdate   = $back_year . '-' . $back_month . '-' . '01';

			$query->select('SUM(o.amount) AS donation_amount');
			$query->select('MONTH(o.cdate) AS MONTHSNAME');
			$query->select('YEAR(o.cdate) AS YEARNAME');
			$query->select('COUNT(o.id) AS orders_count');
			$query->from($db->qn('#__jg_orders', 'o'));
			$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('c.id') . ' = ' . $db->qn('o.campaign_id') . ')');
			$query->where($db->qn('c.id') . ' = ' . $db->quote($id) . ' AND ' . $db->qn('o.status') . ' = ' . $db->quote('C'));
			$query->where('DATE(' . $db->qn('o.cdate') . ')' . ' >= ' . $db->quote($backdate) . ' AND ' . 'DATE(' .
			$db->qn('o.cdate') . ')' . ' <= ' . $db->quote($todate)
			);
			$query->group($db->quote('YEARNAME'));
			$query->group('MONTHSNAME');
			$query->order($db->quote('YEAR( o.cdate )') . 'DESC');
			$query->order($db->quote('MONTH( o.cdate )') . 'DESC');

			$db->setQuery($query);
			$results = $db->loadObjectList();
		}

		return $results;
	}

	/**
	 * Method to campFeatureStatusChanged
	 *
	 * @return boolean
	 *
	 * @since  2.0
	 */
	public function campFeatureStatusChanged()
	{
		$input = JFactory::getApplication()->input;
		$post  = $input->post;

		try
		{
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			$fields = array($db->qn('featured') . ' = ' . $post->get('featured'));
			$conditions = array($db->qn('id') . ' = ' . $post->get('cid'));

			$query->update($db->quoteName('#__jg_campaigns'))->set($fields)->where($conditions);

			$db->setQuery($query);
			$db->execute();

			return true;
		}
		catch (Exception $e)
		{
			$app->enqueueMessage($e->getMessage(), 'error');
			$this->setError($e->getMessage());

			return false;
		}
	}

	/**
	 * Method to Get Campaign Video Id
	 *
	 * @param   String  $path  Videopath
	 * @param   String  $type  type
	 *
	 * @return boolean
	 *
	 * @since  2.0
	 */
	public function getCampaignVideoId($path, $type)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select('id');
		$query->select('type');
		$query->from($db->qn('#__jg_campaigns_media', 'm'));

		if ($type == "path")
		{
			$query->where($db->qn('m.path') . ' = ' . $db->quote($path));
		}
		elseif ($type == "url")
		{
			$query->where($db->qn('m.url') . ' = ' . $db->quote($path));
		}

		$db->setQuery($query);

		$results = $db->loadAssoc();

		return $results;
	}
}
