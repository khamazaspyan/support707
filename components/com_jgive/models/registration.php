<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die(';)');

jimport('joomla.application.component.model');
jimport('joomla.database.table.user');

/**
 * JgiveModelregistration model class.
 *
 * @package  JGive
 * @since    1.8.1
 */
class JgiveModelregistration extends JModelLegacy
{
	/**
	 * Constructor.
	 *
	 * @since      1.6
	 */
	public function __construct()
	{
		parent::__construct();
		global $mainframe, $option;
		$mainframe = JFactory::getApplication();
	}

	/**
	 * Method to Stor Client Data.
	 *
	 * @param   Array  $data  Data
	 *
	 * @return boolean
	 *
	 * @since    1.8.1
	 */
	public function store($data)
	{
		global $mainframe;
		$mainframe = JFactory::getApplication();
		$jinput    = $mainframe->input;
		$id        = $jinput->get('cid');
		$db        = JFactory::getDBO();

		$user = JFactory::getUser();

		if (!$user->id)
		{
			$jgiveModelregistration = new jgiveModelregistration;

			if (!$data['user_email'])
			{
				return false;
			}

			$query = "SELECT id FROM #__users WHERE email = '" . $data['user_email'] . "' or username = '" . $data['user_name'] . "'";
			$this->_db->setQuery($query);
			$userexist = $this->_db->loadResult();
			$userid    = "";
			$randpass  = "";

			if (!$userexist)
			{
				// Generate the random password & create a new user
				$randpass = $jgiveModelregistration->rand_str(6);
				$userid   = $jgiveModelregistration->createnewuser($data, $randpass);
			}
			else
			{
				$message = JText::_('COM_JGIVE_USER_EXIST');
				$jinput->set('message', $message);

				return false;
			}

			if ($userid)
			{
				JPluginHelper::importPlugin('user');

				if (!$userexist)
				{
					$jgiveModelregistration->SendMailNewUser($data, $randpass);
				}

				$cb_params   = JComponentHelper::getParams('com_jgive');
				$integration = $cb_params->get('integration');

				if ($integration == 'cb')
				{
					$cbobj            = new stdClass;
					$cbobj->user_id   = $userid;
					$cbobj->id        = $userid;
					$cbobj->firstname = $data['first_name'];
					$cbobj->lastname  = $data['last_name'];
					$cbobj->confirmed = 1;
					$db->insertObject('#__comprofiler', $cbobj, 'user_id');
				}

				$user           = array();
				$options        = array('remember' => JRequest::getBool('remember', false));

				// Tmp user details
				$user                    = array();
				$user['username']        = $data['user_name'];
				$options['autoregister'] = 0;
				$user['email']           = $data['user_email'];
				$user['password']        = $randpass;
				$mainframe->login(
									array('username' => $data['user_name'],'password' => $randpass),
									array('silent' => true)
								);
			}
		}

		return true;
	}

	/**
	 * Method to Create a new User
	 *
	 * @param   Array   $data      Data
	 * @param   String  $randpass  Random Password
	 *
	 * @return id
	 *
	 * @since    1.8.1
	 */
	public function createnewuser($data, $randpass)
	{
		global $message;
		jimport('joomla.user.helper');
		$authorize = JFactory::getACL();
		$user      = clone JFactory::getUser();
		$user->set('username', $data['user_name']);
		$user->set('password1', $randpass);
		$user->set('name', $data['user_name']);
		$user->set('email', $data['user_email']);

		// Password encryption
		$salt           = JUserHelper::genRandomPassword(32);
		$crypt          = JUserHelper::getCryptedPassword($user->password1, $salt);
		$user->password = "$crypt:$salt";

		// User group/type
		$user->set('id', '');
		$user->set('usertype', 'Registered');

		$userConfig       = JComponentHelper::getParams('com_users');

		// Default to Registered.
		$defaultUserGroup = $userConfig->get('new_usertype', 2);
		$user->set('groups', array($defaultUserGroup));

		$date = JFactory::getDate();
		$user->set('registerDate', $date->toSQL());

		// True on success, false otherwise
		if (!$user->save())
		{
			echo $message = "not created because of " . $user->getError();

			return false;
		}
		else
		{
			$message = "created of username-" . $user->username . "and send mail of details please check";
		}

		return $user->id;
	}

	/**
	 * Method to Create a random character generator for password
	 *
	 * @param   INT     $length  Length
	 * @param   String  $chars   Characters and Numbers
	 *
	 * @return string
	 *
	 * @since    1.8.1
	 */
	public function rand_str($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
	{
		// Length of character list
		$chars_length = (strlen($chars) - 1);

		// Start our string
		$string = $chars{rand(0, $chars_length)};

		// Generate random string
		for ($i = 1; $i < $length; $i = strlen($string))
		{
			// Grab a random character from our list
			$r = $chars{rand(0, $chars_length)};

			// Make sure the same two characters don't appear next to each other
			if ($r != $string{$i - 1})
			{
				$string .= $r;
			}
		}

		// Return the string
		return $string;
	}

	/**
	 * Method to Sending mail to user
	 *
	 * @param   Array   $data      Data
	 * @param   String  $randpass  Random Password
	 *
	 * @return id
	 *
	 * @since    1.8.1
	 */
	public function SendMailNewUser($data, $randpass)
	{
		$app      = JFactory::getApplication();
		$mailfrom = $app->getCfg('mailfrom');
		$fromname = $app->getCfg('fromname');
		$sitename = $app->getCfg('sitename');

		$email    = $data['user_email'];
		$subject  = JText::_('COM_JGIVE_SA_REGISTRATION_SUBJECT');
		$find1    = array(
			'{sitename}'
		);
		$replace1 = array(
			$sitename
		);
		$subject  = str_replace($find1, $replace1, $subject);

		$message = JText::_('COM_JGIVE_SA_REGISTRATION_USER');
		$find    = array(
			'{firstname}',
			'{sitename}',
			'{register_url}',
			'{username}',
			'{password}'
		);
		$replace = array(
			$data['user_name'],
			$sitename,
			JUri::root(),
			$data['user_name'],
			$randpass
		);
		$message = str_replace($find, $replace, $message);

		JFactory::getMailer()->sendMail($mailfrom, $fromname, $email, $subject, $message);
		$messageadmin = JText::_('COM_JGIVE_SA_REGISTRATION_ADMIN');
		$find2        = array(
			'{sitename}',
			'{username}'
		);
		$replace2     = array(
			$sitename,
			$data['user_name']
		);
		$messageadmin = str_replace($find2, $replace2, $messageadmin);

		JFactory::getMailer()->sendMail($mailfrom, $fromname, $mailfrom, $subject, $messageadmin);

		return true;
	}
}
