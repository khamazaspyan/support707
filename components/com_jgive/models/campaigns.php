<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

/**
 * Campaigns form model class.
 *
 * @package  JGive
 * @since    1.8
 */
class JgiveModelCampaigns extends JModelList
{
	protected $total = null;

	protected $pagination = null;

	protected $clearFilters = 0;

	/**
	 * Class constructor.
	 *
	 * @since   1.8
	 */
	public function __construct()
	{
		parent::__construct();
		global $mainframe;
		$input      = JFactory::getApplication()->input;
		$mainframe  = JFactory::getApplication();

		// Get pagination request variables
		$limit      = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = JFactory::getApplication()->input->get('limitstart', 0, '', 'int');

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
		$this->clearFilters = $input->get('clear', '0', 'INT');
	}

	/**
	 * Method populateState
	 *
	 * @param   String  $ordering   Ordering
	 * @param   String  $direction  Direction
	 *
	 * @return void
	 *
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		if ($app->isAdmin())
		{
			return;
		}

		// Load the parameters. Merge Global and Menu Item params into new object
		$params     = $app->getParams();
		$menuParams = new JRegistry;

		if ($menu = $app->getMenu()->getActive())
		{
			$menuParams->loadString($menu->params);
		}

		$mergedParams = clone $menuParams;
		$mergedParams->merge($params);

		$this->setState('params', $mergedParams);
	}

	/**
	 * Function getData
	 *
	 * @param   Array  $mod_data  Module Data Array.
	 *
	 * @return  data
	 *
	 * @since   1.8
	 */
	public function getData($mod_data = '')
	{
		global $mainframe;

		$mainframe = JFactory::getApplication();
		$option = $mainframe->input->get('option');

		require_once JPATH_SITE . "/components/com_jgive/helper.php";

		// If data hasn't already been obtained, load it
		$jgiveFrontendHelper = new jgiveFrontendHelper;

		if (empty($this->_data))
		{
			$query       = $this->_buildQuery($mod_data);

			if (isset($mod_data['no_of_camp_show']))
			{
				$this->_data = $this->_getList($query, $this->getState('limitstart'), $mod_data['no_of_camp_show']);
			}
			else
			{
				$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
			}
		}

		// Modifiy the data
		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;

		$cdata = array();
		$i     = 0;

		foreach ($this->_data as $d)
		{
			// Get campaign amounts
			$amounts             = $campaignHelper->getCampaignAmounts($d->id);
			$d->amount_received  = $amounts['amount_received'];
			$d->remaining_amount = $amounts['remaining_amount'];

			// Count donors(donations)
			$d->donor_count      = $campaignHelper->getCampaignDonorsCount($d->id);

			$camp_startdate = JFactory::getDate($d->start_date)->Format(JText::_('Y-m-d H:i:s'));
			$camp_enddate = JFactory::getDate($d->end_date)->Format(JText::_('Y-m-d H:i:s'));
			$curr_date = JFactory::getDate()->Format(JText::_('Y-m-d H:i:s'));

			if ($curr_date < $camp_startdate)
			{
				// Campaign Not yet started
				$d->donteButtonStatusFlag = -1;
			}
			elseif ($curr_date > $camp_enddate)
			{
				// Camp closed
				$d->donteButtonStatusFlag = 0;
			}
			else
			{
				$d->donteButtonStatusFlag = 1;
			}
		}

		$filter_order = '';

		if ($mainframe->isAdmin())
		{
			$filter_order = $mainframe->getUserStateFromRequest("$option.filter_order", 'filter_order', 'created', 'cmd');

			$filter_order_Dir = $mainframe->getUserStateFromRequest(
			$option . 'filter_order_Dir', 'filter_order_Dir', 'desc', 'cmd');
		}
		else
		{
			if ($this->clearFilters == 0)
			{
				$filter_order = $mainframe->getUserStateFromRequest(
				"$option.filter_order", 'filter_order',
				$mainframe->getParams()->get('default_sort_by_option'), 'cmd');

				$filter_order_Dir = $mainframe->getUserStateFromRequest(
				"$option.filter_order_Dir", 'filter_order_Dir',
				$mainframe->getParams()->get('filter_order_Dir'), 'cmd'
				);
			}
		}

		if ($filter_order == 'donor_count' || $filter_order == 'amount_received' || $filter_order == 'remaining_amount')
		{
			$this->_data = $jgiveFrontendHelper->multi_d_sort($this->_data, $filter_order, $filter_order_Dir);
		}

		foreach ($this->_data as $d)
		{
			$cdata[$d->id]['campaign'] = $d;

			// Get campaign images
			$cdata[$d->id]['images'] = $campaignHelper->getCampaignImages($d->id);
		}

		// Add Mark to successful campaigns & get date difference , & Get campaign country, city & state from id
		foreach ($cdata as $key)
		{
			$key['campaign']->successful = 0;

			if (($key['campaign']->amount_received >= $key['campaign']->goal_amount) && ($key['campaign']->amount_received > 0))
			{
				$key['campaign']->successful = 1;
			}

			// Get campaign duration in days
			$key['campaign']->days_limit = $campaignHelper->getDateDiffInDays($key['campaign']->start_date, $key['campaign']->end_date);

			$key['campaign']->country_name = $jgiveFrontendHelper->getCountryNameFromId($key['campaign']->country);
			$key['campaign']->state_name   = $jgiveFrontendHelper->getRegionNameFromId($key['campaign']->state, $key['campaign']->country);

			if ($key['campaign']->other_city == 0)
			{
				$key['campaign']->city_name = $jgiveFrontendHelper->getCityNameFromId($key['campaign']->city, $key['campaign']->country);
			}
			else
			{
				$key['campaign']->city_name = $key['campaign']->city;
			}
		}

		$this->_data = $cdata;

		return $this->_data;
	}

	/**
	 * Function _buildContentWhere
	 *
	 * @param   Array  $mod_data  Module Data Array.
	 *
	 * @return  query
	 *
	 * @since   1.8
	 */
	public function _buildQuery($mod_data = '')
	{
		// Build query as you want
		$db = JFactory::getDBO();
		global $mainframe;
		$mainframe = JFactory::getApplication();
		$option = $mainframe->input->get('option');

		// Get the WHERE and ORDER BY clauses for the query
		$where = '';
		$where = $this->_buildContentWhere($mod_data);

		// Modified by SNeha, bug id:24903
		$query = "SELECT c.*,cat.title as cat_name
		FROM #__jg_campaigns AS c
		LEFT JOIN #__categories as cat ON c.category_id=cat.id
		LEFT JOIN #__users AS u ON c.creator_id = u.id
		" . $where;

		$filter_order = '';
		$filter_order_Dir = '';

		if ($mainframe->isAdmin())
		{
			$filter_order = $mainframe->getUserStateFromRequest("$option.campaigns.filter_order", 'filter_order', 'created', 'cmd');

			$filter_order_Dir = $mainframe->getUserStateFromRequest("$option.campaigns.filter_order_Dir", 'filter_order_Dir', 'desc', 'cmd');
		}
		else
		{
			if ($this->clearFilters == 0)
			{
				if (isset($mod_data['campaigns_sort_by']))
				{
					$filter_order = $mod_data['campaigns_sort_by'];

					$filter_order_Dir = $mod_data['order_dir'];
				}
				else
				{
					$filter_order = $mainframe->getUserStateFromRequest(
					"$option.filter_order", 'filter_order',
					$mainframe->getParams()->get('default_sort_by_option'), 'cmd');

					$filter_order_Dir = $mainframe->getUserStateFromRequest(
					"$option.filter_order_Dir", 'filter_order_Dir',
					$mainframe->getParams()->get('filter_order_Dir'), 'cmd'
					);
				}
			}
		}

		if (!empty($filter_order))
		{
			$qry1 = "SHOW COLUMNS FROM #__jg_campaigns";
			$db->setQuery($qry1);
			$exists1 = $db->loadobjectlist();

			foreach ($exists1 as $key1 => $value1)
			{
				$allowed_fields[] = $value1->Field;
			}

			if (in_array($filter_order, $allowed_fields))
			{
				$query .= " ORDER BY c.$filter_order $filter_order_Dir";
			}
		}

		return $query;
	}

	/**
	 * Function _buildContentWhere
	 *
	 * @param   Array  $mod_data  Module Data Array.
	 *
	 * @return  query condition
	 *
	 * @since   1.8
	 */
	public function _buildContentWhere($mod_data = '')
	{
		global $mainframe;
		$mainframe = JFactory::getApplication();

		$option = $mainframe->input->get('option');
		$layout    = JFactory::getApplication()->input->get('layout', 'all');

		$db   = JFactory::getDBO();
		$user = JFactory::getUser();

		if ($mainframe->isAdmin())
		{
			$filter_campaign_cat = $mainframe->getUserStateFromRequest("$option.filter_campaign_cat", 'filter_campaign_cat', '', 'INT');
		}
		else
		{
			if ($this->clearFilters == 0)
			{
				$filter_campaign_cat = $mainframe->getUserStateFromRequest(
				"$option.filter_campaign_cat", 'filter_campaign_cat',
				$mainframe->getParams()->get('defualtCatid'), 'INT'
				);
			}
		}

		if (empty($filter_campaign_cat))
		{
			$filter_campaign_cat = JFactory::getApplication()->input->get('filter_campaign_cat', '', 'INT');
		}

		$where = array();

		$TjfieldsHelperPath = JPATH_SITE . '/components/com_tjfields/helpers/tjfields.php';

		if (!class_exists('TjfieldsHelper'))
		{
			JLoader::register('TjfieldsHelper', $TjfieldsHelperPath);
			JLoader::load('TjfieldsHelper');
		}

		$TjfieldsHelper = new TjfieldsHelper;
		$tjfieldItem_ids = $TjfieldsHelper->getFilterResults();

		$client = JFactory::getApplication()->input->get('client', '', 'string');

		if (!empty($client))
		{
			if ($tjfieldItem_ids != '-2')
			{
				$where[] = " c.id IN (" . $tjfieldItem_ids . ") ";
			}
		}

		// Added by SNeha
		$where[] = ' c.creator_id = u.id ';

		if (!empty($filter_campaign_cat))
		{
			if (is_numeric($filter_campaign_cat))
			{
				$cat_tbl = JTable::getInstance('Category', 'JTable');
				$cat_tbl->load($filter_campaign_cat);
				$rgt       = $cat_tbl->rgt;
				$lft       = $cat_tbl->lft;
				$baselevel = (int) $cat_tbl->level;
				$where[]   = 'cat.lft >= ' . (int) $lft;
				$where[]   = 'cat.rgt <= ' . (int) $rgt;
			}
		}

		if ($this->clearFilters == 0)
		{
			$filter_campaign_type = $mainframe->getUserStateFromRequest("$option.filter_campaign_type", 'filter_campaign_type', '', 'string');

			if (!empty($filter_campaign_type))
			{
				$where[] = " c.type='$filter_campaign_type'";
			}
		}

		// Added by Sneha
		// Add text filter on all campaigns
		$filter_state = $mainframe->getUserStateFromRequest("$option.search_list", 'search_list', '', 'string');
		$filter_state = $db->escape($filter_state);

		if ($filter_state)
		{
			$where[] = " ((c.title like '%" . $filter_state . "%' ) OR (c.short_description like '%" .
			$filter_state . "%' ) OR (c.group_name like '%" .
			$filter_state . "%' ) OR (c.first_name like '%" .
			$filter_state . "%' ) OR (c.last_name like '%" .
			$filter_state . "%' ) OR (c.address like '%" .
			$filter_state . "%' ) )";
		}

		if ($layout == 'all')
		{
			// Show only publisgehed
			$where[] = ' c.published=1';

			// Load the parameters. Merge Global and Menu Item params into new object
			$app        = JFactory::getApplication();
			$params     = $app->getParams();
			$menuParams = new JRegistry;

			if ($menu = $app->getMenu()->getActive())
			{
				$menuParams->loadString($menu->params);
			}

			$mergedParams = clone $menuParams;

			$filter_user = 0;

			if ($mergedParams->get('show_filters') == 1 && $mergedParams->get('show_promoter_filter'))
			{
				if ($this->clearFilters == 0)
				{
					// Show campaigns created by selected user
					$filter_user = $mainframe->getUserStateFromRequest("$option.filter_user", 'filter_user');
				}
			}
			elseif (JFactory::getApplication()->input->get('filter_user'))
			{
				$filter_user = JFactory::getApplication()->input->get('filter_user');
			}

			if ($filter_user > 0)
			{
				$where[] = ' c.creator_id=' . $filter_user;
			}

			if ($this->clearFilters == 0)
			{
				// Show campaigns from selected type
				$filter_campaign_type = $mainframe->getUserStateFromRequest("$option.filter_campaign_type", 'filter_campaign_type');

				if ($filter_campaign_type)
				{
					$where[] = " c.type='" . $filter_campaign_type . "'";
				}
				// Show campaign for selected country
				$countries_filter = $mainframe->getUserStateFromRequest("$option.campaign_countries", 'campaign_countries');

				if ($countries_filter)
				{
					$where[] = " c.country='" . $countries_filter . "'";
				}
				// Show campaign for selected state
				$state_filter = $mainframe->getUserStateFromRequest("$option.campaign_states", 'campaign_states');

				if ($state_filter)
				{
					$where[] = " c.state='" . $state_filter . "'";
				}
				// Show campaign for selected city
				$city_filter = $mainframe->getUserStateFromRequest("$option.campaign_city", 'campaign_city');

				if ($city_filter)
				{
					$where[] = " c.city='" . $city_filter . "'";
				}

				// Organization_individual_type filter since version 1.5.1
				$filter_org_ind_type = $mainframe->getUserStateFromRequest("$option.filter_org_ind_type", 'filter_org_ind_type');

				if ($filter_org_ind_type)
				{
					$where[] = " c.org_ind_type='" . $filter_org_ind_type . "'";
				}

				// Campaigns to show filter since jGive version 1.6

				/*if (isset($mod_data['campaigns_to_show']))
				{
					$filter_campaigns_to_show = $mod_data['campaigns_to_show'];
				}
				else
				{
					$filter_campaigns_to_show = $mainframe->getUserStateFromRequest("$option.campaigns_to_show", 'campaigns_to_show', '');
				}

				if ($filter_campaigns_to_show == 'featured')
				{
					$where[] = " c.featured=1 ";
				}
				elseif (isset($filter_campaigns_to_show) && $filter_campaigns_to_show != '')
				{
					$where[] = " c.success_status=" . $filter_campaigns_to_show . " ";
				}*/
			}
		}
		elseif ($layout == 'my')
		{
			$filter_org_ind_type_my = $mainframe->getUserStateFromRequest("$option.filter_org_ind_type_my", 'filter_org_ind_type_my');

			if ($filter_org_ind_type_my)
			{
				$where[] = " c.org_ind_type='" . $filter_org_ind_type_my . "'";
			}

			if ($user->id > 0)
			{
				$where[] = ' c.creator_id=' . $user->id;
			}
		}
		elseif ($layout == 'all_list')
		{
			if ($user->id > 0)
			{
				$filter_campaign_approve = $mainframe->getUserStateFromRequest("$option.filter_campaign_approve", 'filter_campaign_approve', '', 'INT');

				// Check the email link to apporove if it is then set pending value for filter
				$approve                 = JFactory::getApplication()->input->get('approve', '', 'INT');

				if ($filter_campaign_approve == 1)
				{
					$where[] = 'c.published=1';
				}
				elseif ($filter_campaign_approve == 2 or $approve)
				{
					$where[] = 'c.published=0';
				}

				// Organization_individual_type filter since version 1.5.1
				$filter_org_ind_type = $mainframe->getUserStateFromRequest("$option.filter_org_ind_type", 'filter_org_ind_type');

				if ($filter_org_ind_type)
				{
					$where[] = " c.org_ind_type='" . $filter_org_ind_type . "'";
				}
			}
		}

		if (isset($mod_data['campaigns_to_show']))
		{
			$filter_campaigns_to_show = $mod_data['campaigns_to_show'];
		}
		else
		{
			$filter_campaigns_to_show = $mainframe->getUserStateFromRequest("$option.campaigns_to_show", 'campaigns_to_show', '');
		}

		if ($filter_campaigns_to_show == 'featured')
		{
			$where[] = " c.featured=1 ";
		}
		elseif (isset($filter_campaigns_to_show) && $filter_campaigns_to_show != '')
		{
			$where[] = " c.success_status=" . $filter_campaigns_to_show . " ";
		}

		return $where = (count($where) ? ' WHERE ' . implode(' AND ', $where) : '');
	}

	/**
	 * Function getTotal
	 *
	 * @return  total
	 *
	 * @since   1.8
	 */
	public function getTotal()
	{
		// Load the content if it doesn't already exist
		if (empty($this->total))
		{
			$query        = $this->_buildQuery();
			$this->total = $this->_getListCount($query);
		}

		return $this->total;
	}

	/**
	 * Function getPagination
	 *
	 * @return  pagination
	 *
	 * @since   1.8
	 */
	public function getPagination()
	{
		// Load the content if it doesn't already exist
		if (empty($this->pagination))
		{
			jimport('joomla.html.pagination');
			$this->pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->pagination;
	}

	/**
	 * Function _loadData
	 *
	 * @return  Boolean
	 *
	 * @since   1.8
	 */
	public function _loadData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			// Get the pagination request variables
			$limitstart  = JFactory::getApplication()->input->get('limitstart', 0, '', 'int');
			$limit       = JFactory::getApplication()->input->get('limit', 20, '', 'int');
			$query       = $this->_buildQuery();
			$Arows       = $this->_getList($query, $limitstart, $limit);
			$this->_data = $Arows;
		}

		return true;
	}

	/**
	 * Function getUserFilterOptions
	 *
	 * @return  Array
	 *
	 * @since   1.8
	 */
	public function getUserFilterOptions()
	{
		$mainframe = JFactory::getApplication();
		$com_jgive_option = $mainframe->input->get('option');
		$query     = "SELECT DISTINCT (c.creator_id) AS id, u.username as name
			FROM `#__jg_campaigns` AS c
			LEFT JOIN `#__users` AS u ON u.id = c.creator_id
			ORDER BY u.username";
		$this->_db->setQuery($query);
		$users = $this->_db->loadObjectList();

		$filter_user = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_user", 'filter_user');
		$this->setState('filter_user', $filter_user);

		$options   = array();
		$options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_SELECT_USER_FILTER'));

		foreach ($users AS $user)
		{
			$options[] = JHtml::_('select.option', $user->id, $user->name);
		}

		return $options;
	}

	/**
	 * Function getCampaignTypeFilterOptions
	 *
	 * @return  Array
	 *
	 * @since	1.8
	 */
	public function getCampaignTypeFilterOptions()
	{
		$mainframe            = JFactory::getApplication();
		$com_jgive_option = $mainframe->input->get('option');
		$filter_campaign_type = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_campaign_type", 'filter_campaign_type');
		$this->setState('filter_campaign_type', $filter_campaign_type);
		$apps    = JFactory::getApplication();
		$options = array();

		if ($apps->issite() OR JVERSION < 3.0)
		{
			$options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_FILTER_SELECT_TYPE'));
		}

		$options[] = JHtml::_('select.option', 'donation', JText::_('COM_JGIVE_CAMPAIGN_TYPE_DONATION'));
		$options[] = JHtml::_('select.option', 'investment', JText::_('COM_JGIVE_CAMPAIGN_TYPE_INVESTMENT'));

		return $options;
	}

	/**
	 * Function getOrderingOptions
	 *
	 * @return  Array
	 *
	 * @since   1.8
	 */
	public function getOrderingOptions()
	{
		$mainframe = JFactory::getApplication();
		$com_jgive_option = $mainframe->input->get('option');

		if ($mainframe->isAdmin())
		{
			$filter_order = $mainframe->getUserStateFromRequest(
			"$com_jgive_option.filter_order", 'filter_order', 'created', 'string');
		}
		else
		{
			$filter_order = $mainframe->getUserStateFromRequest(
			"$com_jgive_option.filter_order", 'filter_order',
			$mainframe->getParams()->get('default_sort_by_option'), 'string');
		}

		$this->setState('filter_order', $filter_order);

		// Get the data to idetify which field to show on donation view
		$params               = JComponentHelper::getParams('com_jgive');
		$show_selected_fields = $params->get('show_selected_fields');
		$creatorfield         = array();
		$show_field           = 0;
		$goal_amount          = 0;

		if ($show_selected_fields)
		{
			$creatorfield = $params->get('creatorfield');

			if (isset($creatorfield))
			{
				foreach ($creatorfield as $tmp)
				{
					switch ($tmp)
					{
						case 'goal_amount':
							$goal_amount = 1;
							break;
					}
				}
			}
		}
		else
		{
			$show_field = 1;
		}

		$options   = array();
		$options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_FILTER_SELECT_OREDERING'));

		$options[] = JHtml::_('select.option', 'title', JText::_('COM_JGIVE_TITLE'));
		$options[] = JHtml::_('select.option', 'created', JText::_('COM_JGIVE_CREATED'));
		$options[] = JHtml::_('select.option', 'modified', JText::_('COM_JGIVE_MODIFIED'));
		$options[] = JHtml::_('select.option', 'start_date', JText::_('COM_JGIVE_START_DATE'));
		$options[] = JHtml::_('select.option', 'end_date', JText::_('COM_JGIVE_END_DATE'));

		if ($show_field == 1 OR $goal_amount == 0)
		{
			$options[] = JHtml::_('select.option', 'goal_amount', JText::_('COM_JGIVE_GOAL_AMOUNT'));
			$options[] = JHtml::_('select.option', 'amount_received', JText::_('COM_JGIVE_AMOUNT_RECEIVED'));
			$options[] = JHtml::_('select.option', 'remaining_amount', JText::_('COM_JGIVE_REMAINING_AMOUNT'));
			$options[] = JHtml::_('select.option', 'donor_count', JText::_('COM_JGIVE_TOTAL_DONORS_INVESTORS'));
		}

		return $options;
	}

	/**
	 * Function getOrderingDirectionOptions
	 *
	 * @return  Array
	 *
	 * @since   1.8
	 */
	public function getOrderingDirectionOptions()
	{
		$mainframe = JFactory::getApplication();
		$com_jgive_option = $mainframe->input->get('option');

		if ($mainframe->isAdmin())
		{
			$filter_order_Dir = $mainframe->getUserStateFromRequest(
			"$com_jgive_option.filter_order_Dir", 'filter_order_Dir', 'desc', 'string');
		}
		else
		{
				$filter_order_Dir = $mainframe->getUserStateFromRequest(
				"$com_jgive_option.filter_order_Dir", 'filter_order_Dir',
				$mainframe->getParams()->get('filter_order_Dir'), 'string');
		}

		$this->setState('filter_order_Dir', $filter_order_Dir);
		$options   = array();
		$options[] = JHtml::_('select.option', '', JText::_('COM_JGIVE_FILTER_SELECT_OREDERING_DIRECTION'));
		$options[] = JHtml::_('select.option', 'asc', JText::_('COM_JGIVE_ASCENDING'));
		$options[] = JHtml::_('select.option', 'desc', JText::_('COM_JGIVE_DESCENDING'));

		return $options;
	}

	/**
	 * Function setItemState
	 *
	 * @param   Interger  $items  Items
	 * @param   String    $state  State
	 *
	 * @return  Boolean
	 *
	 * @since   1.8
	 */
	public function setItemState($items, $state)
	{
		$campaignHelper = new campaignHelper;
		$db             = JFactory::getDBO();

		if (is_array($items))
		{
			foreach ($items as $id)
			{
				$db    = JFactory::getDBO();
				$query = "UPDATE #__jg_campaigns SET published=" . $state . " WHERE id=" . $id;
				$db->setQuery($query);

				if (!$db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}
			}

			// Get data to for email
			$ids = implode(',', $items);

			// Get creator ids
			$query = "SELECT camp.creator_id
					FROM #__jg_campaigns as camp
					WHERE camp.id IN(" . $ids . ") GROUP BY camp.creator_id";

			$db->setQuery($query);
			$creator_ids = $db->loadColumn();

			// Get campaign infor for each creator
			$camp_info = array();
			$i         = 0;

			foreach ($creator_ids as $creator)
			{
				$query = "SELECT camp.id,camp.title,u.email
							FROM #__jg_campaigns as camp
							LEFT JOIN #__users as u ON camp.creator_id=u.id
							WHERE camp.id IN(" . $ids . ") AND camp.creator_id=" . $creator;

				$db->setQuery($query);
				$camp_info[$i++] = $db->LoadObjectList();
			}
		}

		// Call function to send email admins/promoters to inform campaign is approved / reject
		$campaignHelper->sendemailCampaignApprovedReject($camp_info, $state);

		return true;
	}

	/**
	 * Function delete_campaigns
	 *
	 * @param   Interger  $camp_id  Campaign Id
	 *
	 * @return  Boolean
	 *
	 * @since   1.8
	 */
	public function delete_campaigns($camp_id)
	{
		$campaignHelper = new campaignHelper;
		$db             = JFactory::getDBO();

		// Camp creator info to send notification email
		if (is_array($camp_id))
		{
			// Get data to for email
			$ids = implode(',', $camp_id);

			// Get creator ids
			$query = "SELECT camp.creator_id
					FROM #__jg_campaigns as camp
					WHERE camp.id IN(" . $ids . ") GROUP BY camp.creator_id";

			$db->setQuery($query);
			$creator_ids = $db->loadColumn();

			// Get campaign infor for each creator
			$camp_info = array();
			$i         = 0;

			foreach ($creator_ids as $creator)
			{
				$query = "SELECT camp.id,camp.title,u.email
							FROM #__jg_campaigns as camp
							LEFT JOIN #__users as u ON camp.creator_id=u.id
							WHERE camp.id IN(" . $ids . ") AND camp.creator_id=" . $creator;

				$db->setQuery($query);
				$camp_info[$i++] = $db->LoadObjectList();
			}

			// End of camp creator function

			// Campaign deletion function start

			// Get Image Directory
			$dir = '../images/jGive/';

			foreach ($camp_id as $id)
			{
				// Get the Image Path
				$query = "Select path FROM #__jg_campaigns_images where campaign_id=$id";
				$db->setQuery($query);
				$result = $db->loadResult();

				// Get the image name & delete image
				$file = str_replace('images/jGive/', '', $result);

				if (file_exists($dir . $file))
				{
					if (!@unlink($dir . $file))
					{
					}
				}

				$query = "Delete from #__jg_campaigns_images where campaign_id=$id";
				$db->setQuery($query);

				if (!$db->execute())
				{
					$this->setError($this->_db->getErrorMsg());
				}

				// Delete give details
				$query = "Delete from #__jg_campaigns_givebacks where campaign_id=$id";
				$db->setQuery($query);

				if (!$db->execute())
				{
					$this->setError($this->_db->getErrorMsg());
				}

				// Delete the donors details
				$query = "Delete from #__jg_donors where campaign_id=$id";
				$db->setQuery($query);

				if (!$db->execute())
				{
					$this->setError($this->_db->getErrorMsg());
				}

				// Delete the order details
				$query = "Delete from #__jg_orders where campaign_id=$id";
				$db->setQuery($query);

				if (!$db->execute())
				{
					$this->setError($this->_db->getErrorMsg());
				}

				// Delete campaigns
				$query = "Delete from #__jg_campaigns where id=$id";
				$db->setQuery($query);

				if (!$db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}
			}
		}

		// Call function to send email admins/promoters to inform campaign is rejected
		$campaignHelper->sendemailCampaignApprovedReject($camp_info, 2);

		// Trigger after delete campaign
		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin('content');
		$dispatcher->trigger('onAfterCampaignDelete', array($camp_id));
		JPluginHelper::importPlugin('system');
		$dispatcher->trigger('OnAfterJGiveCampaignDelete', array($camp_id));

		// END plugin triggers

		return true;
	}

	/**
	 * Function setFeatureUnfreature
	 *
	 * @param   Interger  $items  Items
	 * @param   String    $state  Featured
	 *
	 * @return  Boolean
	 *
	 * @since   1.8
	 */
	public function setFeatureUnfreature($items, $state)
	{
		$db = JFactory::getDBO();

		if (is_array($items))
		{
			foreach ($items as $id)
			{
				$db    = JFactory::getDBO();
				$query = "UPDATE #__jg_campaigns SET featured=" . $state . " WHERE id=" . $id;
				$db->setQuery($query);

				if (!$db->execute())
				{
					$this->setError($this->_db->getErrorMsg());

					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Function getCampaignApproveFilterOptions
	 *
	 * @return  Option html
	 *
	 * @since   1.8
	 */
	public function getCampaignApproveFilterOptions()
	{
		$mainframe               = JFactory::getApplication();
		$com_jgive_option = $mainframe->input->get('option');
		$filter_campaign_approve = $mainframe->getUserStateFromRequest("$com_jgive_option.filter_campaign_approve", 'filter_campaign_approve');
		$options                 = array();

		return $options;
	}

	/**
	 * Function getcountries
	 *
	 * @return  Objectlist
	 *
	 * @since   1.8
	 */
	public function getcountries()
	{
		$db    = JFactory::getDBO();
		$query = "SELECT con.id as country_id, con.country
		FROM #__tj_country as con
		INNER JOIN #__jg_campaigns as camp ON camp.country = con.id
		WHERE camp.published = 1
		GROUP BY camp.country";
		$db->setQuery($query);

		return $db->loadobjectlist();
	}

	/**
	 * Function getCampaignStates
	 *
	 * @return  Objectlist
	 *
	 * @since   1.8
	 */
	public function getCampaignStates()
	{
		$db                 = JFactory::getDBO();
		$mainframe          = JFactory::getApplication();
		$com_jgive_option = $mainframe->input->get('option');

		// Get country to generate state
		$campaign_countries = $mainframe->getUserStateFromRequest("$com_jgive_option.campaign_countries", 'campaign_countries');

		if ($campaign_countries)
		{
			$query = "SELECT r.id, r.region
			FROM #__tj_region as r
			LEFT JOIN #__jg_campaigns AS camp ON r.id = camp.state
			WHERE camp.country='" . $campaign_countries . "'
			GROUP BY camp.state";

			$db->setquery($query);

			return $db->loadobjectlist();
		}
	}

	/**
	 * Function getCampaignCity
	 *
	 * @return  Objectlist
	 *
	 * @since   1.8
	 */
	public function getCampaignCity()
	{
		$db                     = JFactory::getDBO();
		$mainframe              = JFactory::getApplication();
		$com_jgive_option = $mainframe->input->get('option');

		// Get country to generate state
		$campaign_states_filter = $mainframe->getUserStateFromRequest("$com_jgive_option.campaign_states", 'campaign_states');

		if ($campaign_states_filter)
		{
			$query = "
			SELECT city.id, city.city, camp.city as othercity
			FROM #__tj_city as city
			RIGHT JOIN #__jg_campaigns AS camp ON city.id = camp.city
			WHERE camp.state='" . $campaign_states_filter . "'
			GROUP BY camp.city";

			$db->setquery($query);

			return $db->loadobjectlist();
		}
	}

	/**
	 * Function changeSuccessState
	 *
	 * @param   Integer  $cid            CID
	 * @param   Integer  $successStatus  Status
	 *
	 * @return  boolean
	 *
	 * @since   1.8
	 */
	public function changeSuccessState($cid, $successStatus)
	{
		$campaignHelper = new campaignHelper;
		$result         = $campaignHelper->updateCampaignSuccessStatus($cid, $successStatus, $orderId = 0);

		if ($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Function changeSuccessState
	 *
	 * @param   Integer  $cid  Campaign Id
	 *
	 * @return  Total donor count
	 *
	 * @since   2.0
	 */
	public function getDonorsCountPerCamp($cid)
	{
		if (!class_exists('JgiveModelDonors'))
		{
			JLoader::register('JgiveModelDonors', JPATH_SITE . '/components/com_jgive/models/donors.php');
			JLoader::load('JgiveModelDonors');
		}

		$jgiveModelDonorsObj = new JgiveModelDonors;

		$totalNoDonorsPerCamp = $jgiveModelDonorsObj->getDonorsPerCamp($cid);

		return $totalNoDonorsPerCamp;
	}
}
