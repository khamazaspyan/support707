<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('FFF');
jimport('joomla.application.component.model');

/**
 * Dashboard form model class.
 *
 * @package  JGive
 * @since    1.8
 */

class JgiveModelDashboard extends JModelLegacy
{
	/**
	 * Function getCampaignIds
	 *
	 * @return  list of campaign id
	 *
	 * @since   1.8
	 */
	public function getCampaignIds()
	{
		$db = JFactory::getDBO();

		// Get Login user id
		$user    = JFactory::getUser();
		$user_id = $user->id;

		$query = $db->getQuery(true);
		$query->select($db->qn(array('id', 'creator_id', 'title')))
			->from($db->qn('#__jg_campaigns'))
			->where($db->qn('#__jg_campaigns.creator_id') . ' = ' . $db->quote($user_id));

		$db->setQuery($query);
		$campainIds = $db->loadAssocList();

		return $campainIds;
	}

	/**
	 * Function getDashboardData
	 *
	 * @return  array of campaign id
	 *
	 * @since   1.8
	 */
	public function getDashboardData()
	{
		$db      = JFactory::getDBO();

		// Get Login user id
		$user    = JFactory::getUser();
		$user_id = $user->id;

		$query = $db->getQuery(true);
		$query->select('SUM(o.amount) as alltimedonationamount');
		$query->select('COUNT(o.id) as countofdonar');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . 'ON (' . $db->qn('c.id') . ' = ' . $db->qn('o.campaign_id') . ')');
		$query->where($db->qn('c.creator_id') . ' = ' . $db->quote($user_id));
		$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));

		$db->setQuery($query);

		$dashboardData = $db->loadAssoc();

		return $dashboardData;
	}

	/**
	 * Function getDonarCount
	 *
	 * @return  donar count
	 *
	 * @since   1.8
	 */
	public function getDonarCount()
	{
		$db      = JFactory::getDBO();

		// Get Login user id
		$user    = JFactory::getUser();
		$user_id = $user->id;
		$query   = $db->getQuery(true);

		// Donor Count who has registered
		$query->select('COUNT(DISTINCT d.user_id)');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->join('LEFT', $db->qn('#__jg_donors', 'd') . ' ON (' . $db->qn('d.id') . ' = ' . $db->qn('o.donor_id') . ')');
		$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('o.campaign_id') . ' = ' . $db->qn('c.id') . ')');
		$query->where($db->qn('c.creator_id') . ' = ' . $db->quote($user_id));
		$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));
		$query->where($db->qn('d.user_id') . '<>' . $db->quote('0'));

		$db->setQuery($query);
		$donarCountreg = $db->loadResult();

		$query   = $db->getQuery(true);

		// Donor Count whi has guest
		$query->select('COUNT(DISTINCT d.email)');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->join('LEFT', $db->qn('#__jg_donors', 'd') . ' ON (' . $db->qn('d.id') . ' = ' . $db->qn('o.donor_id') . ')');
		$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('o.campaign_id') . ' = ' . $db->qn('c.id') . ')');
		$query->where($db->qn('c.creator_id') . ' = ' . $db->quote($user_id));
		$query->where($db->qn('o.status') . ' = ' . $db->quote('C'));
		$query->where($db->qn('d.user_id') . '=' . $db->quote('0'));

		$db->setQuery($query);
		$donarCountguest = $db->loadResult();

		return $donarCountreg + $donarCountguest;
	}

	/**
	 * Function for Recent 5 donation Details
	 *
	 * @return  Object List of Recent periodic donation
	 *
	 * @since   1.8
	 */
	public function getRecent5DonationDetails()
	{
		$db      = JFactory::getDBO();
		$user    = JFactory::getUser();

		// Get Login user id
		$user_id = $user->id;
		$query   = $db->getQuery(true);

		$query->select($db->qn(array('o.id', 'o.order_id', 'o.cdate', 'o.amount', 'o.status', 'd.first_name', 'd.last_name')))
			->from($db->qn('#__jg_orders', 'o'))
			->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('o.campaign_id') . ' = ' . $db->qn('c.id') . ')')
			->join('INNER', $db->qn('#__jg_donors', 'd') . ' ON (' . $db->qn('d.id') . ' = ' . $db->qn('o.donor_id') . ')')
			->where($db->qn('c.creator_id') . ' = ' . $db->quote($user_id))
			->order($db->qn('o.cdate') . ' DESC')
			->setLimit(5);

		$db->setQuery($query);
		$recentDonations = $db->loadAssocList();

		return $recentDonations;
	}

	/**
	 * Function for Top 5 donors
	 *
	 * @return  Object List of Recent periodic donation
	 *
	 * @since   1.8
	 */
	public function getTop5Donors()
	{
		$db = JFactory::getDBO();

		// Get Login user id
		$user    = JFactory::getUser();
		$user_id = $user->id;

		$query   = $db->getQuery(true);

		$query->select('o.campaign_id');
		$query->select('d.first_name');
		$query->select('d.last_name');
		$query->select('SUM(o.amount) as donorwise_total_donation_amount');
		$query->from($db->qn('#__jg_donors', 'd'));
		$query->join('INNER', $db->qn('#__jg_orders', 'o') . ' ON (' . $db->qn('o.donor_id') . ' = ' . $db->qn('d.id') . ')');
		$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('c.id') . ' = ' . $db->qn('o.campaign_id') . ')');
		$query->where($db->qn('c.creator_id') . ' = ' . $db->quote($user_id) . ' AND ' . $db->qn('o.status') . ' = ' . $db->quote('C'));
		$query->group($db->qn('d.user_id'));
		$query->order($db->qn('donorwise_total_donation_amount') . ' DESC');

		$db->setQuery($query);
		$top5Donors = $db->loadAssocList();

		return $top5Donors;
	}

	/**
	 * Function for get user created campaign Details
	 *
	 * @return  array of campaign Details
	 *
	 * @since   1.8
	 */
	public function getMyCampaign()
	{
		$user = JFactory::getUser();
		$user_id = $user->id;

		$mainframe = JFactory::getApplication();
		$option = $mainframe->input->get('option');

		$filter_searchCamp = $mainframe->getUserStateFromRequest("$option.searchMyCamp", 'searchMyCamp', '', 'string');
		$filter_dashboard_filters = $mainframe->getUserStateFromRequest("$option.dashboard_filters", 'dashboard_filters');

		$filter_dashboard_categories = $mainframe->input->get('cat');

		$filter_categories = $filter_dashboard_categories;
		$filter_dashboard_campStatus = $mainframe->input->get('campStatus');
		$filter_dashboard_campType = $mainframe->input->get('campType');
		$filter_dashboard_orgType = $mainframe->input->get('orgType');

		if ($filter_dashboard_filters)
		{
			$todate = JFactory::getDate(date('Y-m-d'))->Format(JText::_('Y-m-d'));

			if ($filter_dashboard_filters == 1)
			{
				$previousDuration = 7;
				$backdate = date('Y-m-d', strtotime(date('Y-m-d') . ' - ' . $previousDuration . ' days'));
			}
			elseif ($filter_dashboard_filters == 2)
			{
				$previousDuration = 30;
				$backdate = date('Y-m-d', strtotime(date('Y-m-d') . ' - ' . $previousDuration . ' days'));
			}
			elseif ($filter_dashboard_filters == 3)
			{
				$previousDuration = 365;
				$backdate = date('Y-m-d', strtotime(date('Y-m-d') . ' - ' . $previousDuration . ' days'));
			}
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('c.*');
		$query->from($db->quoteName('#__jg_campaigns', 'c'));
		$query->where($db->quoteName('c.creator_id') . ' = ' . $db->quote($user_id));

		if ($filter_searchCamp)
		{
			$search = $db->Quote('%' . $db->escape($filter_searchCamp, true) . '%');
			$query->where($db->quoteName('c.title') . ' LIKE ' . $search);
		}

		if ($filter_dashboard_filters != 0)
		{
			$query->where('DATE(' . $db->quoteName('c.start_date') . ')' . ' >= ' . $db->quote($backdate));
		}

		if ($filter_categories != null)
		{
			$query->where($db->quoteName('c.category_id') . ' = ' . $filter_categories);
		}

		if ($filter_dashboard_campStatus != null)
		{
			$query->where($db->quoteName('c.published') . ' = ' . $db->quote($filter_dashboard_campStatus));
		}

		if ($filter_dashboard_campType != null)
		{
			$query->where($db->quoteName('c.type') . ' = ' . $db->quote($filter_dashboard_campType));
		}

		if ($filter_dashboard_orgType != null)
		{
			$query->where($db->quoteName('c.org_ind_type') . ' = ' . $db->quote($filter_dashboard_orgType));
		}

		$db->setQuery($query);
		$my_camp_data = $db->loadAssocList();

		require_once JPATH_SITE . "/components/com_jgive/helpers/campaign.php";
		$campaignHelper = new campaignHelper;

		foreach ($my_camp_data as $key => $camp_data)
		{
			// Get campaign amounts
			$amounts             = $campaignHelper->getCampaignAmounts($camp_data['id']);

			$my_camp_data[$key]['amount_received'] = $amounts['amount_received'];
			$my_camp_data[$key]['remaining_amount'] = $amounts['remaining_amount'];
		}

		return $my_camp_data;
	}

	/**
	 * Function for get donors Details Details
	 *
	 * @return  array of list
	 *
	 * @since   1.8
	 */

	public function getDonorsDetails()
	{
		$db = JFactory::getDBO();

		$user = JFactory::getUser();
		$user_id = $user->id;

		$query = $db->getQuery(true);

		$query->select('SUM(o.amount) as donation_amount');
		$query->select('d.*');
		$query->select('ds.annonymous_donation');
		$query->select('ds.giveback_id');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('c.id') . ' = ' . $db->qn('o.campaign_id') . ')');
		$query->join('INNER', $db->qn('#__users', 'u') . ' ON (' . $db->qn('u.id') . ' = ' . $db->qn('c.creator_id') . ')');
		$query->join('INNER', $db->qn('#__jg_donors', 'd') . ' ON (' . $db->qn('d.id') . ' = ' . $db->qn('o.donor_id') . ')');
		$query->join('INNER', $db->qn('#__jg_donations', 'ds') . ' ON (' . $db->qn('ds.donor_id') . ' = ' . $db->qn('d.id') . ')');
		$query->where($db->qn('c.creator_id') . ' = ' . $db->quote($user_id) . ' AND ' . $db->qn('o.status') . ' = ' . $db->quote('C'));
		$query->group($db->qn('d.email'));
		$query->order($db->qn('donation_amount') . ' DESC');

		$db->setQuery($query);
		$donorsDetails = $db->loadObjectList();

		// Getting Donor Avatar profile
		if (isset($donorsDetails))
		{
			foreach ($donorsDetails as $donorsDetail)
			{
				$helperPath = JPATH_SITE . '/components/com_jgive/helpers/integrations.php';

				if (!class_exists('JgiveIntegrationsHelper'))
				{
					JLoader::register('JgiveIntegrationsHelper', $helperPath);
					JLoader::load('JgiveIntegrationsHelper');
				}

				$JgiveIntegrationsHelper = new JgiveIntegrationsHelper;
				$donorsDetail->avatar      = $JgiveIntegrationsHelper->getUserAvatar($donorsDetail->user_id);
				$donorsDetail->profile_url = $JgiveIntegrationsHelper->getUserProfileUrl($donorsDetail->user_id);
			}
		}

		return $donorsDetails;
	}

	/**
	 * Function for get periodic donation graph data
	 *
	 * @return  array of list
	 *
	 * @since   1.8
	 */
	public function getPeriodicDonationGraphData()
	{
		$db       = JFactory::getDBO();

		// Get Login user id
		$user     = JFactory::getUser();
		$user_id  = $user->id;

		$app      = JFactory::getApplication();
		$backdate = $app->getUserStateFromRequest('from', 'from', '', 'string');
		$todate = $app->getUserStateFromRequest('to', 'to', '', 'string');

		// Get date for 30 days before, in Y-m-d H:i:s format
		$thirtyDaysBefore = date('Y-m-d', strtotime(date('Y-m-d') . ' - 30 days'));
		$backdate         = !empty($backdate) ? $backdate : JFactory::getDate($thirtyDaysBefore)->Format(JText::_('Y-m-d'));

		// Get current date, in Y-m-d H:i:s format
		$todate         = !empty($todate) ? $todate : JFactory::getDate(date('Y-m-d'))->Format(JText::_('Y-m-d'));

		$query = $db->getQuery(true);

		$query->select('SUM(o.amount) AS donation_amount');
		$query->select('DATE(o.cdate) AS cdate');
		$query->select('COUNT(o.id) AS orders_count');
		$query->from($db->qn('#__jg_orders', 'o'));
		$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('c.id') . ' = ' . $db->qn('o.campaign_id') . ')'
		);
		$query->where(
		$db->qn('c.creator_id') . ' = ' . $db->quote($user_id) . ' AND ' . $db->qn('o.status') . ' = ' .
		$db->quote('C')
		);
		$query->where(
		'DATE(' . $db->qn('o.cdate') . ')' . ' >= ' . $db->quote($backdate) . ' AND ' . 'DATE(' . $db->qn('o.cdate') . ')' . ' <= ' . $db->quote($todate)
		);
		$query->group('DATE(' . $db->qn('o.cdate') . ')');

		$db->setQuery($query);

		return $db->loadObjectList("cdate");
	}

	/**
	 * Method to get Dashboard Graph Data
	 *
	 * @param   Integer  $duration  This show the duration for graph data
	 * @param   Integer  $userid    This show the camp promoter id
	 *
	 * @return  Json data
	 *
	 * @since  2.0
	 */
	public function getDashboardGraphData($duration, $userid)
	{
		if ($duration == 0)
		{
			$graphDuration = 7;
		}
		elseif ($duration == 1)
		{
			$graphDuration = 30;
		}

		$todate = JFactory::getDate(date('Y-m-d'))->Format(JText::_('Y-m-d'));

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		if ($duration == 0 || $duration == 1)
		{
			$backdate = date('Y-m-d', strtotime(date('Y-m-d') . ' - ' . $graphDuration . ' days'));
			$query->select('SUM(o.amount) AS donation_amount');
			$query->select('DATE(o.cdate) AS cdate');
			$query->select('COUNT(o.id) AS orders_count');
			$query->from($db->qn('#__jg_orders', 'o'));
			$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('c.id') . ' = ' . $db->qn('o.campaign_id') . ')');
			$query->where($db->qn('c.creator_id') . ' = ' . $db->quote($userid) . ' AND ' . $db->qn('o.status') . ' = ' . $db->quote('C'));
			$query->where('DATE(' . $db->qn('o.cdate') . ')' . ' >= ' . $db->quote($backdate) . ' AND ' . 'DATE(' . $db->qn('o.cdate') . ')' . ' <= ' .
			$db->quote($todate)
			);
			$query->group('DATE(' . $db->qn('o.cdate') . ')');
			$query->order($db->qn('o.cdate') . 'DESC');

			$db->setQuery($query);

			$results = $db->loadObjectList();
		}
		elseif ($duration == 2)
		{
			$curdate    = date('Y-m-d');
			$back_year  = date('Y') - 1;
			$back_month = date('m') + 1;
			$backdate   = $back_year . '-' . $back_month . '-' . '01';

			$query->select('SUM(o.amount) AS donation_amount');
			$query->select('MONTH(o.cdate) AS MONTHSNAME');
			$query->select('YEAR(o.cdate) AS YEARNAME');
			$query->select('COUNT(o.id) AS orders_count');
			$query->from($db->qn('#__jg_orders', 'o'));
			$query->join('INNER', $db->qn('#__jg_campaigns', 'c') . ' ON (' . $db->qn('c.id') . ' = ' . $db->qn('o.campaign_id') . ')');
			$query->where($db->qn('c.creator_id') . ' = ' . $db->quote($userid) . ' AND ' . $db->qn('o.status') . ' = ' . $db->quote('C'));
			$query->where('DATE(' . $db->qn('o.cdate') . ')' . ' >= ' . $db->quote($backdate) . ' AND ' . 'DATE(' . $db->qn('o.cdate') . ')' . ' <= ' .
			$db->quote($todate)
			);
			$query->group($db->quote('YEARNAME'));
			$query->group('MONTHSNAME');
			$query->order($db->quote('YEAR( o.cdate )') . 'DESC');
			$query->order($db->quote('MONTH( o.cdate )') . 'DESC');

			$db->setQuery($query);

			$results = $db->loadObjectList();
		}

		return $results;
	}

	/**
	 * Function DashboardDropDownFilter
	 *
	 * @return Options
	 */
	public function dashboardDropDownOption()
	{
		$options   = array();
		$options[] = JHtml::_('select.option', '0', JText::_('COM_JGIVE_FILTER_SELECT_OPTION'));
		$options[] = JHtml::_('select.option', '1', JText::_('COM_JGIVE_FILTER_LATEST'));
		$options[] = JHtml::_('select.option', '2', JText::_('COM_JGIVE_FILTER_LAST_MONTH'));
		$options[] = JHtml::_('select.option', '3', JText::_('COM_JGIVE_FILTER_LAST_YEAR'));

		return $options;
	}
}
