<?php
/**
 * @version    SVN: <svn_id>
 * @package    JGive
 * @author     Techjoomla <extensions@techjoomla.com>
 * @copyright  Copyright (c) 2009-2015 TechJoomla. All rights reserved.
 * @license    GNU General Public License version 2 or later.
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

/**
 * JgiveModelDonateValidation for donation validation
 *
 * @package     JGive
 * @subpackage  com_jgive
 * @since       1.6.7
 */
class JgiveModelDonateValidation extends JModelLegacy
{
	/**
	 * Method to validate Giveback against amount entered for donation
	 *
	 * @param   INT  $order_id         Order Id
	 * @param   INT  $giveback_id      Giveback Id
	 * @param   INT  $donation_amount  Donation Amoun
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.7
	 */
	public function vaildateGiveback($order_id = null, $giveback_id = 0, $donation_amount = 0)
	{
		if (!empty($giveback_id))
		{
			try
			{
				// Create a new query object.
				$query = $this->_db->getQuery(true);

				$query->select(array('amount'));

				$query->from('#__jg_campaigns_givebacks');
				$query->where('id = ' . $giveback_id);

				$this->_db->setQuery($query);
				$giveback_minimum_amount = $this->_db->loadResult();

				if (((float) $giveback_minimum_amount) > ((float) $donation_amount))
				{
					return false;
				}
			}
			catch (Exception $e)
			{
				$this->setError($e->getMessage());

				return false;
			}
		}

		return true;
	}
}
